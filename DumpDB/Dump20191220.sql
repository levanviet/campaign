-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 207.148.116.202    Database: vsa
-- ------------------------------------------------------
-- Server version	5.7.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CAMPAIGN`
--

DROP TABLE IF EXISTS `CAMPAIGN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `CAMPAIGN` (
  `CAMPAIGN_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CAMPAIGN_NAME` varchar(255) NOT NULL,
  `CAMPAIGN_TYPE` int(11) NOT NULL,
  `IS_DEFAULT` tinyint(1) NOT NULL,
  `ACT_DATE` datetime DEFAULT NULL,
  `EXP_DATE` datetime DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `STATUS` int(11) NOT NULL,
  `SPECIAL_PROM_ID` bigint(20) DEFAULT NULL,
  `BLACKLIST_ID` bigint(45) DEFAULT NULL,
  PRIMARY KEY (`CAMPAIGN_ID`),
  KEY `FK_Category_idx` (`CATEGORY_ID`),
  CONSTRAINT `FK_Category` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `CATEGORY` (`CATEGORY_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10432 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CAMPAIGN`
--

LOCK TABLES `CAMPAIGN` WRITE;
/*!40000 ALTER TABLE `CAMPAIGN` DISABLE KEYS */;
INSERT INTO `CAMPAIGN` VALUES (10425,'1812',1,0,'2019-12-18 00:00:00','2019-12-19 00:00:00',0,'',4,1,NULL,NULL),(10426,'offline 1812',2,0,'2019-12-17 00:00:00','2019-12-18 00:00:00',0,'',6,1,NULL,NULL),(10427,'1912xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',1,0,'2019-12-19 00:00:00','2019-12-20 00:00:00',0,'',4,1,NULL,NULL),(10428,'1912fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff',2,0,'2019-12-19 00:00:00','2019-12-20 00:00:00',0,'',6,1,NULL,NULL),(10429,'1912',1,0,'2019-12-19 00:00:00','2019-12-20 00:00:00',0,'',4,1,NULL,NULL),(10430,'xxxxxx',1,0,'2019-12-20 00:00:00','2019-12-27 00:00:00',0,'',4,1,NULL,NULL),(10431,'8765',1,0,'2019-12-20 00:00:00','2019-12-21 00:00:00',0,'',4,1,NULL,NULL);
/*!40000 ALTER TABLE `CAMPAIGN` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CATEGORY`
--

DROP TABLE IF EXISTS `CATEGORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `CATEGORY` (
  `CATEGORY_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id của bản ghi',
  `CATEGORY_NAME` varchar(255) NOT NULL COMMENT 'Tên của category',
  `CATEGORY_TYPE` int(2) NOT NULL COMMENT 'Kiểu của category',
  `PARENT_ID` int(11) DEFAULT NULL COMMENT 'Category cha, có thể chọn để thay đổi nếu muốn',
  PRIMARY KEY (`CATEGORY_ID`),
  UNIQUE KEY `CATEGORY_ID_UNIQUE` (`CATEGORY_ID`),
  UNIQUE KEY `CATEGORY_NAME_UNIQUE` (`CATEGORY_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=12182 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CATEGORY`
--

LOCK TABLES `CATEGORY` WRITE;
/*!40000 ALTER TABLE `CATEGORY` DISABLE KEYS */;
INSERT INTO `CATEGORY` VALUES (4,'Online',1,NULL),(6,'Offline',2,NULL),(10,'connv111111111111111111111111111111111111111111',12,24),(16,'sdfdfg',12,24),(20,'connv1',15,58),(24,'Segment',12,NULL),(58,'Backlist',15,NULL),(59,'Root rule offline category',16,NULL),(60,'Root special offer',16,59),(61,'truongbx',15,58),(101,'Category Special offer  1',16,60),(102,'Category Special offer  2',16,60),(103,'Category Special offer  3',16,60),(104,'Category Special offer  4',16,60),(105,'1                            111111111111111111111111111111111111111111111111111111111111123',16,60),(106,'!@#$',16,60),(113,'Cat_Pre_Process',3,NULL),(151,'Root promotion block ',6,NULL),(152,'Root OCS behaviour template',11,NULL),(153,'OCS behaviour',9,NULL),(157,'Root ussd scenario cat',0,NULL),(166,'Category Root',10,NULL),(169,'Root Scenario',8,NULL),(175,'Root notify trigger',7,NULL),(207,'PPU-Number',18,NULL),(227,'PPU - Compare number',22,NULL),(234,'PPU - Same element',25,NULL),(236,'PPU - Time',19,NULL),(240,'PPU - Date',20,NULL),(242,'Parameter',29,NULL),(243,'Parameter Number',29,242),(244,'Parameter String',29,242),(246,'root rule online',13,NULL),(251,'PPU-Number range',21,NULL),(252,'PPU-Number range 2',21,251),(255,'PPU - Exist Element',23,NULL),(256,'PPU - Zone',24,NULL),(257,'Cat Zone',26,NULL),(258,'Cat Zone Map',27,NULL),(259,'Cat Geo Home Zone',28,NULL),(272,'Root PPU string',17,NULL),(278,'987654321',0,157),(282,'Root category result table',5,NULL),(283,'Root ConditionTable',4,NULL),(308,'test 123',7,175),(310,'test01',0,157),(316,'aaa',20,240),(337,'number test1231',18,207),(338,'Same element',25,234),(339,'Time',19,236),(340,'Datett',20,240),(341,'range',21,251),(342,'compare',22,227),(343,'exist',23,255),(344,'zone6',24,256),(345,'condition ',4,283),(346,'campaign online',1,4),(347,'campaign offline',2,6),(348,'Category result table 1',5,282),(349,'notify 1',10,166),(350,'PPU string test',17,272),(353,'99',25,234),(363,'dd',1,4),(391,'XX134',2,347),(392,'XXX2',2,6),(393,'ZZ223',2,347),(441,'hdhd',25,338),(444,'lkjhgkjh',25,353),(452,'XXX FFF 11',6,151),(459,'CDFa',6,452),(468,'rtyui',7,308),(472,'XXXXXXs',6,459),(473,'DFTRE',6,452),(482,'Statistic',30,NULL),(510,'xaxaxaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbddddddddddd',17,350),(511,'test12321',17,350),(519,'adaadd',9,153),(520,'ca1',5,348),(525,'ca2',5,520),(526,'ca3',5,525),(527,'rule online1',13,564),(529,'5432',0,310),(530,'43w2',0,529),(536,'ada',17,559),(537,'345ty',8,169),(538,'4567899',8,537),(539,'jhgf',8,538),(540,'8765',8,539),(542,'a',19,236),(544,'a23',24,256),(547,'76t54',0,278),(548,'654',0,310),(552,'sasasa11223',17,272),(553,'1111112223',17,272),(554,'1111111',17,350),(555,'qqqqqq1',17,511),(556,'a1',17,552),(557,'a1111',17,553),(558,'kkkkkkk',17,553),(559,'22222',17,553),(560,'12121212121212',17,553),(561,'232332',17,553),(562,'151515151',17,553),(563,'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh',5,348),(564,'test',13,246),(565,'454',13,527),(566,'45y',13,565),(567,'876',13,566),(570,'xxxxxxx',2,6),(572,'jjjjjjjjjjj11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111',7,175),(573,'1 kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kk kkk kkkk kkk kkk kkk kkk kkk kkkk kkkk kkkkk kkkk kkk kkk kkk',9,153),(577,'nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn',11,152),(580,'rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr',18,207),(581,'dddd 3',25,234),(582,'666666666666666666666666666666666666666666666666666666666666666666666666666666',19,236),(583,'7777777777777777777777777777777777777777777777777777777777777777777777777777777777777',20,240),(584,'9999999999999999999999999999999999999999999999999999999999999999',21,251),(585,'2222222222222222222222222222222222222222222222222222222222',22,227),(586,'1111111111111111111111111111111111111111111111111111111111111111111111111',23,255),(587,'6555555555555555555555555555555555555555555555555555555555555555555555555555555555555',24,256),(588,'1212121',5,282),(590,'141414141',5,588),(591,'kkkkk',17,553),(593,'222221111',17,553),(594,'test121212121',5,348),(597,'dâdadada',17,511),(602,'ggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg',0,157),(603,'11111111112222222222222222222222222222',8,169),(613,'aa',24,544),(616,'661',22,227),(642,'fdd',13,246),(645,'d',13,642),(672,'S61',9,519),(694,'S68',0,157),(710,'C24a',18,207),(712,'tam test 09',2,6),(718,'tam 56778',24,256),(719,'1qaa',19,236),(724,'2qaa',20,240),(726,'1xaa',21,251),(728,'adadad1d',22,227),(730,'dddd',23,255),(731,'adadada',23,730),(732,'vcvcvcvda',23,255),(735,'3frtrtrt9898aa',24,256),(742,'Scenario 124',8,169),(752,'time1112333',19,236),(766,'12we',13,645),(768,'abcd1',9,153),(769,'rwerwe',9,153),(777,'12121212qqq121 21212qqq1212 1212qqq 12121212qqq12121212qqqqqq12121212qqq12121212qqqq qq12121212 qqq12121212qqq',10,349),(778,'22331',10,349),(779,'99999912',10,349),(782,'cat con02',4,283),(784,'cat con033',4,283),(788,'S699',0,157),(795,'rule onlines',13,246),(797,'condition table111',4,283),(800,'offline 1',2,6),(801,'notify trigger',7,175),(802,'ocs behaviour 1',9,153),(803,'scenario trigger1',8,169),(806,'tamtest',5,282),(807,'ocs behaviour template 1',11,152),(808,'ussd scenario template',0,157),(811,'ppu number 1',18,207),(813,'ppu time 1',19,236),(814,'ppu date',20,240),(815,'ppu number range 1',21,251),(816,'ppu compare number 1',22,227),(817,'ppu exist element 1',23,255),(819,'ppu zone 1',24,256),(830,'result table 001',5,282),(839,'ddadadad1',7,175),(846,'dxad122f',9,153),(851,'2121212341',13,246),(852,'cccccccccvvv',2,6),(875,'trigger aa',8,169),(890,'uioy',19,236),(924,'q111kk',6,151),(929,'a141',0,157),(930,'q151',11,152),(942,'g11',6,151),(943,'3456789',4,283),(944,'ad',5,282),(955,'asasasd',4,345),(956,'11111111111111111111111111111111111111111111',13,851),(957,'12',12,24),(958,'13',12,24),(959,'14',12,24),(960,'17',12,24),(961,'1',7,468),(964,'OCS Behaviour Template ',11,152),(12131,'Truongbx232',14,NULL),(12132,'Huyenln_Notify_Template_111 ',10,166),(12133,'Template 131 ',11,152),(12134,'xxxxx',11,930),(12135,'ha test 1',11,12134),(12136,'yyyyy',11,930),(12137,'ha test 2',11,12136),(12138,'ha test 123',11,12135),(12139,'abcd',10,12139),(12140,'vbn',10,12132),(12141,'vbn1',10,12140),(12142,'g1',6,942),(12143,'A43567',5,282),(12145,'1111111111111111111111111111111111111111111111111111111111111',1,4),(12146,'2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222',1,4),(12147,'55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555',5,563),(12148,'6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666',14,12131),(12149,'444444444444444444444444444444444444444444444444444444444444444444',13,246),(12150,'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',4,283),(12151,'sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss',1,4),(12152,'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh',2,6),(12153,'jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj',7,175),(12154,'kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kkk kk kkk kkkk kkk kkk kkk kkk kkk kkkk kkkk kkkkk kkkk kkk kkk kkk',9,153),(12155,'yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy',8,169),(12156,'CASjhdf',6,151),(12157,'rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr',10,12157),(12158,'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq',11,152),(12159,'vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv',0,157),(12160,'ppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppp',17,272),(12161,'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',18,207),(12162,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',25,234),(12163,'wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww',19,236),(12164,'iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiitttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt',20,240),(12165,'nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn',21,251),(12166,'xxxxxxxxxxxxxxxzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',22,227),(12167,'jjjjjjjjjjjjjjjjjjjjjkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkggggggggggggggggggggggg',23,255),(12168,'aaaaaaaaaaaaaaaaaasssssssssssssssssssssssssdddddddddddddddd',24,256),(12169,'sdfghj',10,166),(12170,'asdfgh',10,349),(12171,'Huyenln_level_1',7,175),(12172,'111',9,768),(12173,'112',9,768),(12174,'113',9,768),(12175,'114',9,768),(12176,'115',9,768),(12177,'bbbbb',10,349),(12178,'template',11,152),(12179,'hatest',17,272),(12180,'                                                xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',19,582),(12181,'xxxxaaa',2,6);
/*!40000 ALTER TABLE `CATEGORY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OBJECTS`
--

DROP TABLE IF EXISTS `OBJECTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `OBJECTS` (
  `OBJECT_ID` int(10) NOT NULL AUTO_INCREMENT,
  `PARENT_ID` int(10) DEFAULT NULL,
  `STATUS` int(1) NOT NULL,
  `ORD` int(10) DEFAULT NULL,
  `OBJECT_URL` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `OBJECT_NAME` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `DECRIPTION` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `OBJECT_TYPE_ID` int(10) DEFAULT NULL,
  `OBJECT_CODE` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `CREATE_USER` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `PATH_IMG` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `UPDATE_USER` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`OBJECT_ID`),
  UNIQUE KEY `OBJECT_ID` (`OBJECT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1127 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OBJECTS`
--

LOCK TABLES `OBJECTS` WRITE;
/*!40000 ALTER TABLE `OBJECTS` DISABLE KEYS */;
INSERT INTO `OBJECTS` VALUES (1,NULL,1,0,'','Dashboard','Dashboard',1,'Dashboard',NULL,NULL,'fa fa-fw fa-life-saver','admin','2019-09-06 00:00:00'),(2,1,1,0,'/campaign/dashboard/home.jsf','Home','',1,'ON',NULL,NULL,'','admin','2019-07-29 00:00:00'),(3,87,1,2,'/campaign/campaign/campaignOnline.jsf','Campaign Online','',2,'OFF','admin','2019-07-29 00:00:00','','admin','2019-09-05 00:00:00'),(4,1,1,3,'/campaign/request/creationRequest.jsf','Request Creation','',NULL,'REQ','admin','2019-07-30 00:00:00','','admin','2019-10-09 00:00:00'),(7,1122,1,3,'/campaign/admin/changePass.jsf','Change password','',NULL,'CHANGEPASS','admin','2019-08-02 00:00:00','fa fa-fw fa-circle-o-notch','admin','2019-10-08 00:00:00'),(8,1124,1,4,'/campaign/ruleEngine/conditionTable/conditionTable.jsf','Condition Table','',NULL,'CONDITION_TABLE','admin','2019-09-10 00:00:00','fa fa-fw fa-table','admin','2019-10-21 00:00:00'),(86,1122,1,1,'/campaign/admin/roleManagement.jsf','Role management','',NULL,'ROLE MANAGEMENT','admin','2019-09-10 00:00:00','fa fa-fw fa-life-saver','admin','2019-10-08 00:00:00'),(87,NULL,1,3,NULL,'Campaign ',NULL,NULL,'CAMPAIGN ','admin',NULL,'fa fa-fw fa-life-saver',NULL,NULL),(88,1122,1,4,'/campaign/admin/menu-management/menu-management.jsf','Menu management','',NULL,'MENU MANAGEMENT','amin',NULL,'fa fa-fw fa-table','admin','2019-10-08 00:00:00'),(89,1122,1,5,'/campaign/admin/usersManagement.jsf','Users management','',NULL,'USERS MANAGEMENT','admin',NULL,'fa fa-fw fa-table','admin','2019-10-09 00:00:00'),(90,87,1,2,'/campaign/campaignoffline/campaignOffline.jsf','Campaign Offline','',NULL,'CAMPAIGN_OFFLINE','admin','2019-07-29 00:00:00','','admin','2019-09-05 00:00:00'),(91,NULL,1,6,NULL,'Promotion','',NULL,'PROMOTION','admin','2019-09-11 00:00:00','fa fa-fw fa-life-saver','admin','2019-09-11 00:00:00'),(92,91,1,6,'/campaign/promotion/promotion-block.jsf','Promotion block','',NULL,'PROMOTION_BLOCK','admin','2019-09-11 00:00:00','fa fa-fw fa-table','admin','2019-10-21 00:00:00'),(93,NULL,1,7,NULL,'Template',NULL,NULL,'Template','admin','2019-09-15 20:07:17','fa fa-fw fa-life-saver',NULL,'2019-09-15 20:13:37'),(94,93,1,2,'/campaign/OCSTemplate/OCSTemplate.jsf','OCS Behaviour Template','',2,'OFFFFSSS','admin','2019-07-29 00:00:00','fa fa-fw fa-table','admin','2019-10-21 00:00:00'),(95,91,0,6,'/campaign/promotion/notification.jsf','Notification','manage notification trigger',NULL,'NOTIFICATION','admin','2019-09-15 00:00:00','fa fa-fw fa-life-saver','admin','2019-09-15 00:00:00'),(96,93,1,10,'/campaign/template/ussdScenario/ussdScenario.jsf','USSD Scenario Template','',NULL,'USSD_SCENARIO','admin','2019-09-15 00:00:00','fa fa-fw fa-table','admin','2019-10-21 00:00:00'),(97,1124,1,1,'/campaign/ruleEngine/resultTable/resultTable.jsf','Result table','',NULL,'RESULT TABLE','admin','2019-09-16 00:00:00','fa fa-fw fa-table','admin','2019-10-21 00:00:00'),(98,93,1,1,'/campaign/template/notify/notify.jsf','Notify Template','',NULL,'NOTIFY_TEMPLATE','admin','2019-09-17 00:00:00','fa fa-fw fa-table','admin','2019-10-21 00:00:00'),(99,91,1,3,'/campaign/scenarioTrigger/scenarioTrigger.jsf','Scenario trigger','',NULL,'SCENARIO_TRIGGER','admin','2019-09-18 00:00:00','fa fa-fw fa-table','admin','2019-10-21 00:00:00'),(105,91,1,2,'/campaign/OCSBehaviour/OCSBehaviour.jsf','OCS Behaviour','',2,'OFFFF','admin','2019-07-29 00:00:00','fa fa-fw fa-table','admin','2019-10-21 00:00:00'),(111,91,1,1,'/campaign/notifyTrigger/notifyTrigger.jsf','Notify Trigger','',NULL,'NOTIFY_TRIGGER','admin','2019-09-23 00:00:00','fa fa-fw fa-table','admin','2019-10-21 00:00:00'),(115,1126,1,1,'/campaign/preProcessString/PreProcessString.jsf','PPU - String','',NULL,'PPU - String',NULL,NULL,'','admin','2019-10-06 00:00:00'),(117,1126,1,2,'/campaign/preProcessNumber/PreProcessNumber.jsf','PPU - Number','',NULL,'PPU - Number',NULL,NULL,'','admin','2019-10-06 00:00:00'),(118,1126,0,3,'/campaign/preProcessCompareNumber/PreProcessCompareNumber.jsf','PPU - Compare number','',NULL,'PPU_COMPARE_NUMBER','admin','2019-10-02 00:00:00','','admin','2019-10-02 00:00:00'),(120,1126,1,4,'/campaign/preProcessSameElement/PreProcessSameElement.jsf','PPU - Same element','',NULL,'PPU_SE','admin','2019-10-03 00:00:00','','admin','2019-10-03 00:00:00'),(121,1126,1,5,'/campaign/preProcessTime/PreProcessTime.jsf','PPU - Time','',NULL,'PPU_TIME','admin','2019-10-03 00:00:00','','admin','2019-10-03 00:00:00'),(122,1126,1,6,'/campaign/preProcessDate/PreProcessDate.jsf','PPU - Date','',NULL,'PPU_DATE','admin','2019-10-03 00:00:00','','admin','2019-10-03 00:00:00'),(124,1126,1,7,'/campaign/preProcessNumberRange/PreProcessNumberRange.jsf','PPU - Number range','',NULL,'PPU - Number range','admin','2019-10-03 00:00:00','','admin','2019-10-03 00:00:00'),(125,1126,1,8,'/campaign/preProcessCompareNumber/PreProcessCompareNumber.jsf','PPU - Compare number','',NULL,'PPU - Compare number','admin','2019-10-03 00:00:00','','admin','2019-10-03 00:00:00'),(126,1126,1,9,'/campaign/preProcessExistElement/PreProcessExistElement.jsf','PPU - Exist Element','',NULL,'PPU_EE','admin','2019-10-06 00:00:00','','admin','2019-10-06 00:00:00'),(127,1126,1,10,'/campaign/preProcessZone/PreProcessZone.jsf','PPU - Zone','',NULL,'PPU_ZONE','admin','2019-10-06 00:00:00','','admin','2019-10-06 00:00:00'),(137,1125,1,3,'/campaign/ruleOnline/RuleOnline.jsf','Rule Online','',NULL,'Rule Online','admin','2019-09-16 00:00:00','fa fa-fw fa-table','admin','2019-10-21 00:00:00'),(138,1125,1,3,'/campaign/ruleOffline/RuleOffline.jsf','Rule Offline','',NULL,'Rule Offline','admin','2019-09-16 00:00:00','fa fa-fw fa-table','admin','2019-10-21 00:00:00'),(1122,NULL,1,10,'','Admin','',NULL,'Admin','admin','2019-12-20 00:00:00','fa fa-fw fa-life-saver','admin','2019-12-20 00:00:00'),(1124,NULL,1,1,'','Rule Engine','',NULL,'Rule Engine','admin','2019-12-20 00:00:00','fa fa-fw fa-life-saver','admin','2019-12-20 00:00:00'),(1125,NULL,1,1,'','Rule','',NULL,'Rule','admin','2019-12-20 00:00:00','fa fa-fw fa-life-saver','admin','2019-12-20 00:00:00'),(1126,NULL,1,1,NULL,'PreProcess Unit',NULL,NULL,'PreProcess Unit','admin','2019-12-20 00:00:00','fa fa-fw fa-life-saver','admin','2019-12-20 00:00:00');
/*!40000 ALTER TABLE `OBJECTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ROLE`
--

DROP TABLE IF EXISTS `ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ROLE` (
  `ROLE_ID` int(10) NOT NULL AUTO_INCREMENT,
  `ROLE_NAME` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `ROLE_CODE` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `CREATE_USER` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `CREATE_DATE` date DEFAULT NULL,
  `UPDATE_USER` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `UPDATE_DATE` timestamp NULL DEFAULT NULL,
  `STATUS` int(1) DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`),
  UNIQUE KEY `ROLE_ID` (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ROLE`
--

LOCK TABLES `ROLE` WRITE;
/*!40000 ALTER TABLE `ROLE` DISABLE KEYS */;
INSERT INTO `ROLE` VALUES (1,'Admin','Admin',NULL,NULL,'admin','2019-12-20 15:25:30',1),(82,'connv','connv','admin','2019-12-20','connv','2019-12-20 17:03:25',0),(83,'connv','connv','admin','2019-12-20','admin','2019-12-20 17:07:03',0),(84,'connv','connv','admin','2019-12-20','admin','2019-12-20 17:08:13',1),(85,'test role','123456789','admin','2019-12-20','admin','2019-12-20 18:04:20',1);
/*!40000 ALTER TABLE `ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ROLE_OBJECT`
--

DROP TABLE IF EXISTS `ROLE_OBJECT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ROLE_OBJECT` (
  `OBJECT_ID` int(10) NOT NULL,
  `ROLE_ID` int(10) NOT NULL,
  `IS_ACTIVE` int(1) NOT NULL,
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1293 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ROLE_OBJECT`
--

LOCK TABLES `ROLE_OBJECT` WRITE;
/*!40000 ALTER TABLE `ROLE_OBJECT` DISABLE KEYS */;
INSERT INTO `ROLE_OBJECT` VALUES (1,1,1,1207),(2,1,1,1208),(3,1,1,1209),(4,1,1,1210),(86,1,1,1211),(88,1,1,1212),(89,1,1,1213),(97,1,1,1214),(98,1,1,1215),(111,1,1,1216),(115,1,1,1217),(90,1,1,1218),(94,1,1,1219),(105,1,1,1220),(117,1,1,1221),(7,1,1,1222),(99,1,1,1223),(137,1,1,1224),(138,1,1,1225),(8,1,1,1226),(120,1,1,1227),(121,1,1,1228),(92,1,1,1229),(122,1,1,1230),(124,1,1,1231),(125,1,1,1232),(126,1,1,1233),(96,1,1,1234),(127,1,1,1235),(87,1,1,1236),(91,1,1,1237),(93,1,1,1238),(1,82,0,1240),(2,82,0,1241),(86,82,0,1242),(97,82,0,1243),(98,82,0,1244),(111,82,0,1245),(115,82,0,1246),(3,82,0,1247),(90,82,0,1248),(94,82,0,1249),(105,82,0,1250),(117,82,0,1251),(4,82,0,1252),(7,82,0,1253),(99,82,0,1254),(137,82,0,1255),(138,82,0,1256),(8,82,0,1257),(88,82,0,1258),(120,82,0,1259),(89,82,0,1260),(121,82,0,1261),(92,82,0,1262),(122,82,0,1263),(124,82,0,1264),(125,82,0,1265),(126,82,0,1266),(96,82,0,1267),(127,82,0,1268),(1122,82,0,1270),(1,83,0,1271),(2,83,0,1272),(86,83,0,1273),(88,83,0,1274),(89,83,0,1275),(7,83,0,1276),(1122,83,0,1277),(1,84,1,1278),(86,84,1,1279),(7,84,1,1280),(2,84,1,1281),(88,84,1,1282),(89,84,1,1283),(1122,84,1,1284),(1125,85,1,1285),(1,85,0,1286),(86,85,0,1287),(2,85,0,1288),(1122,1,1,1289),(1124,1,1,1290),(1125,1,1,1291),(1126,1,1,1292);
/*!40000 ALTER TABLE `ROLE_OBJECT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ROLE_USER`
--

DROP TABLE IF EXISTS `ROLE_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ROLE_USER` (
  `ROLE_ID` int(10) NOT NULL,
  `USER_ID` int(10) NOT NULL,
  `IS_ACTIVE` int(1) NOT NULL,
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=338 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ROLE_USER`
--

LOCK TABLES `ROLE_USER` WRITE;
/*!40000 ALTER TABLE `ROLE_USER` DISABLE KEYS */;
INSERT INTO `ROLE_USER` VALUES (1,1,1,264),(1,77,1,330),(1,96,0,331),(82,96,0,332),(82,97,0,333),(83,96,0,334),(84,96,1,335),(85,98,1,336),(85,99,0,337);
/*!40000 ALTER TABLE `ROLE_USER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RULE`
--

DROP TABLE IF EXISTS `RULE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `RULE` (
  `RULE_ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `RULE_NAME` varchar(255) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `CDR_SERVICE_ID` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`RULE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RULE`
--

LOCK TABLES `RULE` WRITE;
/*!40000 ALTER TABLE `RULE` DISABLE KEYS */;
INSERT INTO `RULE` VALUES (434,'zxczc','xxxxx',12148,''),(435,'cccc','xxxxxx',956,'1,2,3'),(437,'','xxx111',956,'all'),(438,'','xxx11',12148,'-1'),(439,'','rule online 1812',246,'1,2'),(440,'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz','rule offline 1812',12131,'-1'),(441,'','rule offline',12131,'-1'),(442,'','rule offline 1',12131,'-1'),(444,'','offline 1812',12131,'-1'),(453,'','987',246,'1');
/*!40000 ALTER TABLE `RULE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SEQ_TABLE`
--

DROP TABLE IF EXISTS `SEQ_TABLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `SEQ_TABLE` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TABLE_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TABLE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SEQ_TABLE`
--

LOCK TABLES `SEQ_TABLE` WRITE;
/*!40000 ALTER TABLE `SEQ_TABLE` DISABLE KEYS */;
INSERT INTO `SEQ_TABLE` VALUES (1,'RULE',453),(2,'SCHEDULE',495),(3,'OCS_TEMPLATE',254),(4,'SCENARIO_NOTE',937),(5,'EXTEND_FIELD',508),(6,'OCS_BEHAVIOUR',253),(7,'NOTIFY_CONTENT',673),(8,'NOTIFY_TEMPLATE',434),(9,'NOTIFY_TRIGGER',736),(10,'STATISTIC_CYCLE',201),(11,'PRE_PROCESS_UNIT',1309),(12,'RESULT_TABLE',493),(13,'CAMPAIGN',12724),(14,'SCENARIO_TRIGGER',601),(15,'USSD_SCENARIO',670),(16,'CONDITION_TABLE',860),(17,'NODE_CONTENT',5210);
/*!40000 ALTER TABLE `SEQ_TABLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USERS`
--

DROP TABLE IF EXISTS `USERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `USERS` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_NAME` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `FULL_NAME` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `PASS_WORD` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `EMAIL` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `DEPT_ID` int(11) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `GENDER` int(11) DEFAULT NULL,
  `DATE_OF_BIRTH` date DEFAULT NULL,
  `BIRTH_PLACE` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `IDENTITY_CARD` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `ISSUE_PLACE_IDENT` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `ISSUE_DATE_IDENT` date DEFAULT NULL,
  `DESCRIPTION` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `VALID_FROM` datetime DEFAULT NULL,
  `VALID_TO` datetime DEFAULT NULL,
  `CHECK_IP` int(11) DEFAULT NULL,
  `IP` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `LAST_CHANGE_PASSWORD` varchar(100) DEFAULT NULL,
  `CREATE_USER` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `CREATE_DATE` date DEFAULT NULL,
  `LOGIN_FAILURE_COUNT` int(11) DEFAULT NULL,
  `LAST_LOCK_DATE` datetime DEFAULT NULL,
  `UPDATE_USER` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `UPDATE_DATE` timestamp NULL DEFAULT NULL,
  `DATE_BIRTH` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USERS`
--

LOCK TABLES `USERS` WRITE;
/*!40000 ALTER TABLE `USERS` DISABLE KEYS */;
INSERT INTO `USERS` VALUES (1,'admin','Admin','$2a$10$/anBAU2Fr4/JKNTFZPj7yu8kVjOXAPX4jirYYMU2sNTKg2TXVfy2W','',NULL,1,1,'2019-08-02',NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,'$2a$10$/anBAU2Fr4/JKNTFZPj7yu8kVjOXAPX4jirYYMU2sNTKg2TXVfy2W',NULL,NULL,NULL,NULL,'admin','2019-12-20 14:18:22','2019-12-20 14:18:19'),(57,'thao','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,1,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-09-19',NULL,NULL,'admin','2019-09-19 00:00:00','2019-09-19 09:04:47'),(58,'test','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,0,'2019-09-12',NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-09-19',NULL,NULL,'admin','2019-09-19 00:00:00','2019-09-19 09:23:43'),(59,'tests','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,1,NULL,NULL,'',NULL,NULL,'222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222',NULL,NULL,NULL,NULL,NULL,'admin','2019-09-19',NULL,NULL,'admin','2019-09-19 00:00:00','2019-09-19 09:23:59'),(60,'Nguyễn Hà','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,1,'2019-09-24',NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-09-20',NULL,NULL,'admin','2019-09-24 00:00:00','2019-09-24 16:28:23'),(61,'phạm thảo','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,1,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-09-20',NULL,NULL,'admin','2019-09-24 00:00:00','2019-09-24 16:10:46'),(62,'Tạo mới','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,1,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-09-23',NULL,NULL,'admin','2019-09-24 00:00:00','2019-09-24 16:11:01'),(63,'123','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,1,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,'$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa','admin','2019-09-23',NULL,NULL,'admin','2019-12-09 16:25:28','2019-12-09 16:25:27'),(64,'75','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,1,'2019-09-11',NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-09-23',NULL,NULL,'admin','2019-09-23 00:00:00','2019-09-23 02:57:36'),(65,'980','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,0,'2019-09-29',NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,'$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa','admin','2019-09-23',NULL,NULL,'admin','2019-12-08 15:41:11','2019-12-08 15:40:42'),(66,'8765','222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,2,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-09-23',NULL,NULL,'admin','2019-09-23 00:00:00','2019-10-03 07:08:39'),(67,'222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,1,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-09-23',NULL,NULL,'admin','2019-09-23 00:00:00','2019-09-23 03:59:25'),(68,'tt','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,1,NULL,NULL,'222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-09-23',NULL,NULL,'admin','2019-09-23 00:00:00','2019-09-23 03:58:15'),(69,'thảo phạm','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,1,'2019-09-26',NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-09-25',NULL,NULL,'admin','2019-09-25 00:00:00','2019-09-25 02:01:47'),(70,'việt nam','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,2,'2019-09-27',NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-09-25',NULL,NULL,'admin','2019-09-25 00:00:00','2019-09-25 02:02:32'),(71,'Test1','Nguyễn Thị Test1','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,1,'1989-09-19',NULL,'125786111',NULL,NULL,'nhân viên TEST1',NULL,NULL,NULL,NULL,'$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa','admin','2019-10-03',NULL,NULL,'admin','2019-12-12 15:51:29','2019-12-12 15:51:29'),(72,'ptt12','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-10-03',NULL,NULL,'admin','2019-10-03 00:00:00','2019-10-27 10:05:53'),(73,'connv','Nguyễn Văn Còn','$2a$10$/anBAU2Fr4/JKNTFZPj7yu8kVjOXAPX4jirYYMU2sNTKg2TXVfy2W',NULL,NULL,0,0,'1993-12-31',NULL,'0123456789',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-10-03',NULL,NULL,'admin','2019-10-03 00:00:00','2019-12-20 15:44:44'),(74,'346','1a','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-10-09',NULL,NULL,'admin','2019-10-21 00:00:00','2019-10-27 10:05:48'),(75,'Tâm','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,1,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-10-21',NULL,NULL,'admin','2019-10-21 00:00:00','2019-10-27 10:05:45'),(76,'95','95','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,1,'2019-10-23',NULL,'95',NULL,NULL,'95',NULL,NULL,NULL,NULL,NULL,'admin','2019-10-21',NULL,NULL,'admin','2019-10-21 00:00:00','2019-10-21 08:06:02'),(77,'phamthao','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-10-27',NULL,NULL,'admin','2019-12-18 08:42:51','2019-12-18 08:42:50'),(78,'yyyrrrrrrrrrryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy','hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,0,'2019-11-03',NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-11-13',NULL,NULL,'admin','2019-11-13 00:00:00','2019-11-13 04:28:21'),(79,'!@#$^','dfghggfds','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,0,'2019-11-01',NULL,'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-11-13',NULL,NULL,'admin','2019-11-13 00:00:00','2019-11-13 08:34:30'),(80,'test09','test','$2a$10$oqGCjG84Q.l9MB8n.8sdAeS2CjJMTxq0s81ofcpwsxTouRxBm0eGm',NULL,NULL,1,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-11-13',NULL,NULL,'admin','2019-11-13 00:00:00','2019-11-15 02:23:40'),(81,'ffffffffffff1fffffffffffff@!fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff','fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,2,'2019-10-01',NULL,'fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-11-13',NULL,NULL,'admin','2019-11-13 00:00:00','2019-11-13 07:51:55'),(82,'abc','Nguyen  Thi Thuy Linh','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,0,'2019-11-06',NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-11-15',NULL,NULL,'admin','2019-11-25 00:00:00','2019-11-25 08:28:52'),(83,'linh123','Nguyen Thi Thuy Linh','$2a$10$e3nsf8Nut2c0HClIF5wrneBG/wW9GiXuipIuUhKe.co/FrdAk2kU2',NULL,NULL,1,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-11-18',NULL,NULL,'linh123','2019-11-18 00:00:00','2019-11-18 03:15:12'),(84,'thao99','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,'$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa','admin','2019-11-19',NULL,NULL,'tam98','2019-12-17 03:30:52','2019-12-17 03:30:51'),(85,'tam','Tamdt','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-11-25',NULL,NULL,'admin','2019-11-25 00:00:00','2019-11-25 07:10:52'),(86,'tam98','tamdt','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-11-25',NULL,NULL,'admin','2019-11-25 00:00:00','2019-11-25 08:21:45'),(87,'test003','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-11-25',NULL,NULL,'admin','2019-11-25 00:00:00','2019-11-25 08:29:22'),(88,'test004','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-11-25',NULL,NULL,'admin','2019-11-25 00:00:00','2019-11-25 08:30:40'),(89,'testtest','connv','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,0,'2019-12-11',NULL,'1234567890',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-12-12',NULL,NULL,'admin','2019-12-12 15:54:23','2019-12-12 15:54:22'),(90,'test1712','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,0,'2019-12-16',NULL,'',NULL,NULL,'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttes',NULL,NULL,NULL,NULL,NULL,'admin','2019-12-17',NULL,NULL,'admin','2019-12-17 03:35:06','2019-12-17 03:35:06'),(91,'tests17','ghjj','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,1,'2019-12-18',NULL,'gggggggggggggggg',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-12-17',NULL,NULL,'admin','2019-12-17 03:34:01','2019-12-17 03:34:00'),(92,'thao@','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-12-17',NULL,NULL,'admin','2019-12-17 03:30:42','2019-12-17 03:30:41'),(93,'thaopham','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,'$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa','admin','2019-12-18',NULL,NULL,'admin','2019-12-18 08:37:50','2019-12-18 08:37:49'),(94,'1812','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,0,'1777-09-30',NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-12-18',NULL,NULL,'admin','2019-12-18 15:41:03','2019-12-18 15:41:01'),(95,'1912zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz','zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,0,NULL,NULL,'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',NULL,NULL,'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',NULL,NULL,NULL,NULL,NULL,'admin','2019-12-19',NULL,NULL,'admin','2019-12-19 02:32:51','2019-12-19 02:32:50'),(96,'connv','connv','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,'$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa','admin','2019-12-20',NULL,NULL,'admin','2019-12-20 17:08:39','2019-12-20 17:08:36'),(97,'hatest','','$2a$10$/anBAU2Fr4/JKNTFZPj7yu8kVjOXAPX4jirYYMU2sNTKg2TXVfy2W',NULL,NULL,1,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-12-20',NULL,NULL,'admin','2019-12-20 16:36:04','2019-12-20 16:36:04'),(98,'thaotest','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,1,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-12-20',NULL,NULL,'admin','2019-12-20 18:00:04','2019-12-20 18:00:03'),(99,'9887','','$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa',NULL,NULL,0,0,NULL,NULL,'',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin','2019-12-20',NULL,NULL,'admin','2019-12-20 18:06:06','2019-12-20 18:06:06');
/*!40000 ALTER TABLE `USERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accum_prop`
--

DROP TABLE IF EXISTS `accum_prop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `accum_prop` (
  `ACCUM_PROP_ID` int(11) unsigned NOT NULL,
  `ACCUM_PROP_NAME` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ACCUM_PROP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accum_prop`
--

LOCK TABLES `accum_prop` WRITE;
/*!40000 ALTER TABLE `accum_prop` DISABLE KEYS */;
/*!40000 ALTER TABLE `accum_prop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accumulate_trigger`
--

DROP TABLE IF EXISTS `accumulate_trigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `accumulate_trigger` (
  `ACCUM_TRIGGER_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `ACCUM_TYPE` int(11) NOT NULL,
  `CAMPAIGN_PROP_ID` int(11) NOT NULL,
  `VALUE` bigint(20) NOT NULL,
  `CYCLE` int(11) NOT NULL,
  `EFF_DATE` datetime DEFAULT NULL,
  `EXP_DATE` datetime DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ACCUM_TRIGGER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accumulate_trigger`
--

LOCK TABLES `accumulate_trigger` WRITE;
/*!40000 ALTER TABLE `accumulate_trigger` DISABLE KEYS */;
/*!40000 ALTER TABLE `accumulate_trigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `behaviour_template_param_map`
--

DROP TABLE IF EXISTS `behaviour_template_param_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `behaviour_template_param_map` (
  `BEHAVIOUR_TEMPLATE_PARAM_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BEHAVIOUR_TEMPLATE_ID` bigint(20) NOT NULL,
  `TEMPLATE_PARAM_ID` bigint(20) NOT NULL,
  `PARAM_INDEX` int(11) NOT NULL,
  PRIMARY KEY (`BEHAVIOUR_TEMPLATE_PARAM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `behaviour_template_param_map`
--

LOCK TABLES `behaviour_template_param_map` WRITE;
/*!40000 ALTER TABLE `behaviour_template_param_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `behaviour_template_param_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blacklist`
--

DROP TABLE IF EXISTS `blacklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `blacklist` (
  `BLACKLIST_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `BLACKLIST_NAME` varchar(200) NOT NULL,
  `CATEGORY_ID` int(11) unsigned DEFAULT NULL,
  `PATH` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `TEMP_PATH` varchar(200) NOT NULL,
  `HIST_PATH` varchar(200) NOT NULL,
  `FAILED_PATH` varchar(200) NOT NULL,
  PRIMARY KEY (`BLACKLIST_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blacklist`
--

LOCK TABLES `blacklist` WRITE;
/*!40000 ALTER TABLE `blacklist` DISABLE KEYS */;
INSERT INTO `blacklist` VALUES (1,'BlackList 1',58,'1','1','1','1','1'),(2,'Blacklist 2',58,'2','2','2','2','2'),(3,'Blacklist 3',58,'3','1','3','3','3'),(4,'11111111111111111111111111111111111111111111111111111111111111111111111111111122222222222222222222222222222222222222222222222222',58,'4','1','4','4','4'),(5,'!@#$',58,'5','1','5','5','5'),(6,'Hác',58,'6','1','6','6','6'),(7,'2',58,'/u01/file/blacklist/temp_path/zzz_zz.txt','1','7','7','7'),(8,'3',58,'       8    9','1','     8     9','         8    9','            8       9');
/*!40000 ALTER TABLE `blacklist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_container`
--

DROP TABLE IF EXISTS `block_container`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `block_container` (
  `BLOCK_CONTAINER_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PROMOTION_BLOCK_ID` bigint(20) NOT NULL,
  `PROM_TYPE` int(11) NOT NULL,
  `PROM_ID` bigint(20) NOT NULL,
  `OCS_PARENT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`BLOCK_CONTAINER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=453 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_container`
--

LOCK TABLES `block_container` WRITE;
/*!40000 ALTER TABLE `block_container` DISABLE KEYS */;
INSERT INTO `block_container` VALUES (440,120,1,249,NULL),(444,121,1,250,NULL),(445,121,2,725,NULL),(446,121,2,728,NULL),(447,121,2,725,NULL),(448,122,3,595,NULL),(452,123,1,250,NULL);
/*!40000 ALTER TABLE `block_container` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_info`
--

DROP TABLE IF EXISTS `campaign_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `campaign_info` (
  `CAMPAIGN_INFO_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CAMPAIGN_ID` bigint(20) unsigned NOT NULL,
  `SEGMENT_ID` bigint(20) NOT NULL,
  `RULE_ID` bigint(20) unsigned NOT NULL,
  `PRIORITY` int(11) unsigned NOT NULL,
  `PRIORITY_GROUP` int(11) NOT NULL,
  PRIMARY KEY (`CAMPAIGN_INFO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=514 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_info`
--

LOCK TABLES `campaign_info` WRITE;
/*!40000 ALTER TABLE `campaign_info` DISABLE KEYS */;
INSERT INTO `campaign_info` VALUES (503,10425,8,439,0,0),(505,10427,8,439,0,0),(506,10427,7,439,1,1),(507,10428,8,444,0,0),(508,10429,2,439,0,0),(509,10426,8,444,0,0),(510,10430,6,435,1,1),(511,10430,5,439,0,2),(512,10431,1,439,2,1),(513,10431,3,439,1,2);
/*!40000 ALTER TABLE `campaign_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_prop`
--

DROP TABLE IF EXISTS `campaign_prop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `campaign_prop` (
  `CAMPAIGN_PROP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CAMPAIGN_PROP_NAME` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`CAMPAIGN_PROP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_prop`
--

LOCK TABLES `campaign_prop` WRITE;
/*!40000 ALTER TABLE `campaign_prop` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_prop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_schedule`
--

DROP TABLE IF EXISTS `campaign_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `campaign_schedule` (
  `SCHEDULE_ID` bigint(20) NOT NULL,
  `SCHEDULE_NAME` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `SCHEDULE_TYPE` int(11) NOT NULL,
  `CAMPAIGN_ID` bigint(20) NOT NULL,
  `SCHEDULE_PATTERN` varchar(200) NOT NULL,
  `MODE` int(11) NOT NULL,
  PRIMARY KEY (`SCHEDULE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_schedule`
--

LOCK TABLES `campaign_schedule` WRITE;
/*!40000 ALTER TABLE `campaign_schedule` DISABLE KEYS */;
INSERT INTO `campaign_schedule` VALUES (478,'test','',1,10426,'0 0 0 19 12 * 2019',2),(487,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx','',1,10428,'0 0 0 20 12 * 2019',2),(493,'xxx','xxxx',1,10426,'0 0 0 20 12 * 2019',2);
/*!40000 ALTER TABLE `campaign_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdr_prop`
--

DROP TABLE IF EXISTS `cdr_prop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdr_prop` (
  `CDR_PROP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROP_NAME` varchar(50) NOT NULL,
  `DESCRIPTION` varchar(400) DEFAULT NULL,
  `SOURCE` varchar(50) DEFAULT NULL,
  `DATA_TYPE` varchar(20) DEFAULT NULL,
  `PARAM` varchar(400) DEFAULT NULL,
  `DEFAULT_VALUE` varchar(200) DEFAULT NULL,
  `DOMAIN_ID` bigint(20) DEFAULT NULL,
  `PROP_ID` bigint(20) DEFAULT NULL,
  `POS_INDEX` int(11) DEFAULT NULL,
  PRIMARY KEY (`CDR_PROP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdr_prop`
--

LOCK TABLES `cdr_prop` WRITE;
/*!40000 ALTER TABLE `cdr_prop` DISABLE KEYS */;
/*!40000 ALTER TABLE `cdr_prop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdr_service`
--

DROP TABLE IF EXISTS `cdr_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdr_service` (
  `CDR_SERVICE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CDR_SERVICE_CODE` varchar(40) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `REMARK` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`CDR_SERVICE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdr_service`
--

LOCK TABLES `cdr_service` WRITE;
/*!40000 ALTER TABLE `cdr_service` DISABLE KEYS */;
INSERT INTO `cdr_service` VALUES (1,'cdr1','cdr service 1',NULL),(2,'cdr2','cdr service 2',''),(3,'cdr3','cdr service 3',NULL),(4,'cdr4','cdr service 4',NULL),(5,'cdr5','cdr service 5',NULL),(6,'cdr6','cdr service 6',NULL),(7,'1','111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111',NULL);
/*!40000 ALTER TABLE `cdr_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdr_template`
--

DROP TABLE IF EXISTS `cdr_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdr_template` (
  `CDR_TEMPLATE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TEMPLATE_CODE` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(400) DEFAULT NULL,
  `CDR_SERVICE_ID` int(11) NOT NULL,
  `DELIMITER` varchar(10) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `DOMAIN_ID` bigint(20) DEFAULT NULL,
  `POS_INDEX` int(11) DEFAULT NULL,
  PRIMARY KEY (`CDR_TEMPLATE_ID`),
  KEY `FK_cdr_template_CATEGORY` (`CATEGORY_ID`),
  CONSTRAINT `CK_cdr_template_CATEGORY` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `category` (`CATEGORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdr_template`
--

LOCK TABLES `cdr_template` WRITE;
/*!40000 ALTER TABLE `cdr_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `cdr_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdr_template_prop`
--

DROP TABLE IF EXISTS `cdr_template_prop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdr_template_prop` (
  `CDR_TEMPLATE_PROP_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CDR_TEMPLATE_ID` bigint(20) NOT NULL,
  `ORDER_PROP` bigint(20) NOT NULL,
  `NULLABLE` bigint(20) DEFAULT NULL,
  `STATUS` bigint(20) DEFAULT NULL,
  `DOMAIN_ID` bigint(20) DEFAULT NULL,
  `CDR_PROP_ID` bigint(20) NOT NULL,
  `FORMAT` varchar(50) DEFAULT NULL,
  `DEFAULT_VALUE` varchar(255) DEFAULT NULL,
  `DISPLAYABLE` bit(1) DEFAULT NULL,
  PRIMARY KEY (`CDR_TEMPLATE_PROP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdr_template_prop`
--

LOCK TABLES `cdr_template_prop` WRITE;
/*!40000 ALTER TABLE `cdr_template_prop` DISABLE KEYS */;
/*!40000 ALTER TABLE `cdr_template_prop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clean_rule`
--

DROP TABLE IF EXISTS `clean_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `clean_rule` (
  `CLEAN_RULE_ID` int(12) NOT NULL,
  `OBJECT_ID` int(12) NOT NULL,
  `GROUP_ID` int(2) NOT NULL,
  `IS_SQL_OBJ` tinyint(1) NOT NULL,
  `ATT_NAME` varchar(50) NOT NULL,
  `EXPRESSION` varchar(50) NOT NULL,
  PRIMARY KEY (`CLEAN_RULE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clean_rule`
--

LOCK TABLES `clean_rule` WRITE;
/*!40000 ALTER TABLE `clean_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `clean_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `column_ct`
--

DROP TABLE IF EXISTS `column_ct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `column_ct` (
  `COLUMN_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `COLUMN_NAME` varchar(200) NOT NULL,
  `PRE_PROCESS_ID` bigint(20) unsigned NOT NULL,
  `COLUMN_INDEX` int(11) unsigned NOT NULL,
  PRIMARY KEY (`COLUMN_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2067 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `column_ct`
--

LOCK TABLES `column_ct` WRITE;
/*!40000 ALTER TABLE `column_ct` DISABLE KEYS */;
INSERT INTO `column_ct` VALUES (2056,'ppu string 1712',1224,1),(2057,'ppu zone 1712',1243,2),(2058,'time1',1238,3),(2062,'number 1712',1232,1),(2063,'same 1712',1233,2),(2064,'ppu string 1712',1224,3),(2065,'ppu string 1712',1224,1),(2066,'compare number 1712',1241,2);
/*!40000 ALTER TABLE `column_ct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `condition_table`
--

DROP TABLE IF EXISTS `condition_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `condition_table` (
  `CONDITION_TABLE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `CONDITION_TABLE_NAME` varchar(1000) NOT NULL,
  `DEFAULT_RESULT_INDEX` int(11) NOT NULL,
  PRIMARY KEY (`CONDITION_TABLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=857 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `condition_table`
--

LOCK TABLES `condition_table` WRITE;
/*!40000 ALTER TABLE `condition_table` DISABLE KEYS */;
INSERT INTO `condition_table` VALUES (854,'',283,'condition 1712xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',1),(855,'',283,'tt01',2),(856,'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',283,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',1);
/*!40000 ALTER TABLE `condition_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `condition_table_column_map`
--

DROP TABLE IF EXISTS `condition_table_column_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `condition_table_column_map` (
  `CONDITION_TABLE_COLUMN_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CONDITION_TABLE_ID` bigint(20) NOT NULL,
  `COLUMN_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`CONDITION_TABLE_COLUMN_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2066 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `condition_table_column_map`
--

LOCK TABLES `condition_table_column_map` WRITE;
/*!40000 ALTER TABLE `condition_table_column_map` DISABLE KEYS */;
INSERT INTO `condition_table_column_map` VALUES (2055,855,2056),(2056,855,2057),(2057,855,2058),(2061,854,2062),(2062,854,2063),(2063,854,2064),(2064,856,2065),(2065,856,2066);
/*!40000 ALTER TABLE `condition_table_column_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `condition_table_row_map`
--

DROP TABLE IF EXISTS `condition_table_row_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `condition_table_row_map` (
  `CONDITION_TABLE_ROW_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CONDITION_TABLE_ID` bigint(20) NOT NULL,
  `ROW_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`CONDITION_TABLE_ROW_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=986 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `condition_table_row_map`
--

LOCK TABLES `condition_table_row_map` WRITE;
/*!40000 ALTER TABLE `condition_table_row_map` DISABLE KEYS */;
INSERT INTO `condition_table_row_map` VALUES (978,855,978),(979,855,979),(982,854,982),(983,854,983),(984,856,984),(985,856,985);
/*!40000 ALTER TABLE `condition_table_row_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cycle_unit`
--

DROP TABLE IF EXISTS `cycle_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cycle_unit` (
  `ID` bigint(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `NOTE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cycle_unit`
--

LOCK TABLES `cycle_unit` WRITE;
/*!40000 ALTER TABLE `cycle_unit` DISABLE KEYS */;
INSERT INTO `cycle_unit` VALUES (1,'Cycle unit 1','Cycle unit 1'),(2,'Cycle unit 2','Cycle unit 2');
/*!40000 ALTER TABLE `cycle_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_type`
--

DROP TABLE IF EXISTS `data_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `data_type` (
  `data_type_id` bigint(20) NOT NULL,
  `data_type_name` varchar(255) NOT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`data_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_type`
--

LOCK TABLES `data_type` WRITE;
/*!40000 ALTER TABLE `data_type` DISABLE KEYS */;
INSERT INTO `data_type` VALUES (1,'String',NULL,NULL),(2,'Int',NULL,NULL),(3,'Long',NULL,NULL),(4,'Float',NULL,NULL);
/*!40000 ALTER TABLE `data_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extend_field`
--

DROP TABLE IF EXISTS `extend_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `extend_field` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(1000) NOT NULL,
  `DESCRIPTION` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extend_field`
--

LOCK TABLES `extend_field` WRITE;
/*!40000 ALTER TABLE `extend_field` DISABLE KEYS */;
INSERT INTO `extend_field` VALUES (1,'extend field 1',NULL),(2,'extend field 2',NULL),(3,'extend field 3',NULL),(4,'extend field 4',NULL),(5,'extend field 5',NULL),(22,'hhh',NULL),(25,'kkk',NULL),(34,'dfsdf',NULL),(35,'1111',NULL),(36,'1234',NULL),(37,'12121',NULL),(48,'aaaa','fsd'),(49,'aaa',''),(50,'aaadd','ds'),(51,'test',''),(52,'12','%'),(53,'54','2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222'),(58,'fsdf sdfsdf sdf sdf sdf sdfsdf sdfsdf sdf sdf sdf sdfsdf sdfsdf sdf sdf sdfsdf sdfsdf sdf sdf sdf sdfsdf sdfsdf sdf sdf sdf sdf sd342434','%'),(62,'xx','x'),(63,'TESTaaa',''),(65,'fffffffffffffffffffffffffffffffffffffffffff','g'),(66,'asd','a');
/*!40000 ALTER TABLE `extend_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_id_ocs`
--

DROP TABLE IF EXISTS `field_id_ocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `field_id_ocs` (
  `FIELD_ID` bigint(20) NOT NULL,
  `DATA_TYPE` int(11) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FIELD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_id_ocs`
--

LOCK TABLES `field_id_ocs` WRITE;
/*!40000 ALTER TABLE `field_id_ocs` DISABLE KEYS */;
INSERT INTO `field_id_ocs` VALUES (1,1,'Field 1'),(2,1,'Field 2'),(3,1,'Field 3'),(4,1,'Field 4'),(5,1,'Field 5'),(6,1,'Field 6'),(7,1,'Filed 7Filed 7Filed 7Filed 7Filed 7Filed 7Filed 7Filed 7Filed 7Filed 7Filed 7Filed 7Filed 7Filed 7');
/*!40000 ALTER TABLE `field_id_ocs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `function`
--

DROP TABLE IF EXISTS `function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `function` (
  `FUNCTION_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FUNCTION_NAME` varchar(200) NOT NULL,
  `FUNCTION_DISPLAY` varchar(255) NOT NULL,
  `NUMBER_PARAMETER` int(11) NOT NULL,
  `ALIAS` int(11) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  PRIMARY KEY (`FUNCTION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `function`
--

LOCK TABLES `function` WRITE;
/*!40000 ALTER TABLE `function` DISABLE KEYS */;
INSERT INTO `function` VALUES (1,'getAccumulateExactDay','getAccumulateExactDay(long time, String propName)',2,10001,NULL,NULL,1),(2,'getActionAccumulatePeriodbyDay','getActionAccumulatePeriodbyDay(String fromTime, String toTime, String propName, int actionType)',4,10002,NULL,NULL,1),(3,'getActionAccumulateLastNumberMonth','getActionAccumulateLastNumberMonth(String propName,int cycle, int actionType)',3,10003,NULL,NULL,1),(4,'getActionAccumulateCurrentCycleMonthbyDay','getActionAccumulateCurrentCycleMonthbyDay(String propName, int cycle, int actionType)',3,10004,NULL,NULL,1),(5,'getActionAccumulateLastCycleMonthbyDay','getActionAccumulateLastCycleMonthbyDay(String propName, int cycle, int actionType)',3,10005,NULL,NULL,1),(6,'getActionAccumulateLastCycleMonthbyMonth','getActionAccumulateLastCycleMonthbyMonth(String propName, int cycle, int actionType)',3,10006,NULL,NULL,1),(7,'getActionAccumulatePeriodbyMonth','getActionAccumulatePeriodbyMonth(String fromTime, String toTime, String propName, int actionType)',4,10007,NULL,NULL,1),(8,'getListSize','getListSize()',0,NULL,NULL,NULL,2),(9,'prefix','prefix(int endIndex)',1,NULL,NULL,NULL,2),(10,'suffix','suffix(int endIndex)',1,NULL,NULL,NULL,2),(11,'subString','subString(int startIndex,int endIndex)',2,NULL,NULL,NULL,2),(12,'standardMsisdn','standardMsisdn()',0,NULL,NULL,NULL,2),(13,'isOffnetNumberWithMNPPrefix','isOffnetNumberWithMNPPrefix()',0,NULL,NULL,NULL,2),(14,'standardMsisdnWithoutMNP','standardMsisdnWithoutMNP()',0,NULL,NULL,NULL,2),(15,'getStringLength','getStringLength()',0,NULL,NULL,NULL,2),(16,'getCellsFromListZoneString','getCellsFromListZoneString()',0,NULL,NULL,NULL,2),(17,'getCellsFromListCellString','getCellsFromListCellString()',0,NULL,NULL,NULL,2),(18,'getZonesFromListZoneString','getZonesFromListZoneString()',0,NULL,NULL,NULL,2),(19,'getZone','getZone(int zoneId)',1,NULL,NULL,NULL,2),(20,'getCellsFromListZoneStringByZoneId','getCellsFromListZoneStringByZoneId(int zoneId)',1,NULL,NULL,NULL,2),(21,'searchLongestMatchingZoneDataByZoneId','searchLongestMatchingZoneDataByZoneId(int zoneId)',1,NULL,NULL,NULL,2),(22,'searchLongestMatchingZoneDataByZoneMapId','searchLongestMatchingZoneDataByZoneMapId(int zoneMapId)',1,NULL,NULL,NULL,2),(23,'searchZoneCodeInZoneMapId','searchZoneCodeInZoneMapId(int zoneMapId)',1,NULL,NULL,NULL,2),(24,'searchZoneIdInZoneMapId','searchZoneIdInZoneMapId(int zoneMapId)',1,NULL,NULL,NULL,2),(25,'searchMatchingZoneIdInZoneMapId','searchMatchingZoneIdInZoneMapId(int zoneMapId)',1,NULL,NULL,NULL,2),(26,'getNumberDaysOfMonth','getNumberDaysOfMonth()',0,NULL,NULL,NULL,2),(27,'getDayOfMonth','getDayOfMonth()',0,NULL,NULL,NULL,2),(28,'addXDay','addXDay(int delta)',1,NULL,NULL,NULL,2),(29,'getStartOfDay','getStartOfDay()',0,NULL,NULL,NULL,2),(30,'getMonthOfYear','getMonthOfYear()',0,NULL,NULL,NULL,2),(31,'addXMonth','addXMonth(int delta)',1,NULL,NULL,NULL,2),(32,'getStartOfMonth','getStartOfMonth()',0,NULL,NULL,NULL,2),(33,'getTimeFromString','getTimeFromString()',0,NULL,NULL,NULL,2),(34,'multipleWith','multipleWith(double value)',1,NULL,NULL,NULL,2),(35,'divideWith','divideWith(double value)',1,NULL,NULL,NULL,2),(36,'roundUp','roundUp(int roundValue)',1,NULL,NULL,NULL,2),(37,'roundDown','roundDown(int roundValue)',1,NULL,NULL,NULL,2),(38,'getMinWithValue','getMinWithValue(double value)',1,NULL,NULL,NULL,2),(39,'getMaxWithValue','getMaxWithValue(double value)',1,NULL,NULL,NULL,2),(40,'getMinWithFieldNameInCdr','getMinWithFieldNameInCdr(String propName)',1,NULL,NULL,NULL,2),(41,'getMaxWithFieldNameInCdr','getMaxWithFieldNameInCdr(String propName)',1,NULL,NULL,NULL,2),(42,'getActionAccumulateCurrentCycleMonthbyMonth','getActionAccumulateCurrentCycleMonthbyMonth(String propName, int cycle, int actionType)',3,10008,NULL,NULL,1),(43,'getActionAccumulateLastCycleDay','getActionAccumulateLastCycleDay(String propName, int cycle, int actionType)',3,10009,NULL,NULL,1),(44,'getActionAccumulateCurrentCycleDay','getActionAccumulateCurrentCycleDay(String propName, int cycle, int actionType)',3,10010,NULL,NULL,1),(45,'getCurrentTime','getCurrentTime()',0,NULL,NULL,NULL,2),(46,'getStartOfYear','getStartOfYear()',0,NULL,NULL,NULL,2),(47,'inCurrentDay','inCurrentDay()',0,NULL,NULL,NULL,2),(48,'inCurrentMonth','inCurrentMonth()',0,NULL,NULL,NULL,2),(49,'inCurrentYear','inCurrentYear()',0,NULL,NULL,NULL,2),(50,'inCurrentCycleDay','inCurrentCycleDay(int cycle)',1,NULL,NULL,NULL,2),(51,'inCurrentCycleMonth','inCurrentCycleMonth(int cycle)',1,NULL,NULL,NULL,2),(52,'inCurrentCycleYear','inCurrentCycleYear(int cycle)',1,NULL,NULL,NULL,2),(53,'inLastCycleDay','inLastCycleDay(int cycle)',1,NULL,NULL,NULL,2),(54,'inLastCycleMonth','inLastCycleMonth(int cycle)',1,NULL,NULL,NULL,2),(55,'inLastCycleYear','inLastCycleYear(int cycle)',1,NULL,NULL,NULL,2),(56,'yenTest','yenTest(Date d)',1,NULL,NULL,NULL,NULL),(57,'yenTest','yenTest(Date d)',1,NULL,NULL,NULL,2),(58,'yenTest','yenTest(List a)',1,NULL,NULL,NULL,2),(59,'yenTest','yenTest(List b)',1,NULL,NULL,NULL,2),(60,'thaoTest','thaoTest(String string, Date date, int so, List list1, List list2)',5,NULL,NULL,NULL,2),(63,'thao','thao(long time,String string)',2,NULL,NULL,NULL,2);
/*!40000 ALTER TABLE `function` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `function_container`
--

DROP TABLE IF EXISTS `function_container`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `function_container` (
  `FUNCTION_CONTAINER_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FUNCTION_ID` bigint(20) NOT NULL,
  `FUNCTION_PARAM_ID` bigint(20) NOT NULL,
  `FUNCTION_PARAM_INDEX` int(11) NOT NULL,
  PRIMARY KEY (`FUNCTION_CONTAINER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `function_container`
--

LOCK TABLES `function_container` WRITE;
/*!40000 ALTER TABLE `function_container` DISABLE KEYS */;
INSERT INTO `function_container` VALUES (1,1,1,1),(2,1,4,2),(3,2,4,1),(4,2,5,2),(5,2,6,3),(6,3,2,1),(7,3,3,2),(8,3,4,3),(9,3,6,4),(10,4,4,1),(11,4,5,2),(12,4,6,3),(13,5,4,1),(14,5,5,2),(15,5,6,3),(16,6,4,1),(17,6,5,2),(18,6,6,3),(19,7,2,1),(20,7,3,2),(21,7,4,3),(22,7,6,4),(23,42,4,1),(24,42,5,2),(25,42,6,3),(26,9,7,1),(27,10,7,1),(28,11,8,1),(29,11,7,2),(30,20,9,1),(31,21,9,1),(32,22,10,1),(33,23,10,1),(34,24,10,1),(35,25,10,1),(36,28,11,1),(37,31,11,1),(38,34,12,1),(39,35,12,1),(40,36,13,1),(41,37,13,1),(42,38,12,1),(43,39,12,1),(44,40,4,1),(45,41,4,1),(46,43,4,1),(47,43,5,2),(48,43,6,3),(49,44,4,1),(50,44,5,2),(51,44,6,3),(52,50,5,1),(53,51,5,1),(54,52,5,1),(55,53,5,1),(56,54,5,1),(57,55,5,1),(58,2,7,4),(59,57,14,1),(60,58,15,1),(61,59,16,1),(62,60,17,1),(63,60,18,2),(64,60,19,3),(65,60,20,4),(66,60,21,5),(67,63,22,1),(68,63,23,2);
/*!40000 ALTER TABLE `function_container` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `function_param`
--

DROP TABLE IF EXISTS `function_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `function_param` (
  `FUNCTION_PARAM_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FUNCTION_PARAM_NAME` varchar(200) NOT NULL,
  `VALUE_TYPE` int(11) NOT NULL,
  PRIMARY KEY (`FUNCTION_PARAM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `function_param`
--

LOCK TABLES `function_param` WRITE;
/*!40000 ALTER TABLE `function_param` DISABLE KEYS */;
INSERT INTO `function_param` VALUES (1,'time',4),(2,'fromTime',1),(3,'toTime',1),(4,'propName',1),(5,'cycle',2),(6,'actionType',2),(7,'endIndex',2),(8,'startIndex',2),(9,'zoneId',2),(10,'zoneMapId',3),(11,'delta',2),(12,'value',5),(13,'roundValue',2),(14,'d',3),(15,'a',4),(16,'b',5),(17,'string',1),(18,'date',2),(19,'so',3),(20,'list1',4),(21,'list2',5),(22,'time',2),(23,'string',1);
/*!40000 ALTER TABLE `function_param` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `geo_home_zone`
--

DROP TABLE IF EXISTS `geo_home_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `geo_home_zone` (
  `GEO_HOME_ZONE_ID` bigint(20) NOT NULL,
  `GEO_HOME_ZONE_TYPE` int(11) DEFAULT NULL,
  `GEO_HOME_ZONE_NAME` varchar(200) NOT NULL,
  `GEO_HOME_ZONE_CODE` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`GEO_HOME_ZONE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `geo_home_zone`
--

LOCK TABLES `geo_home_zone` WRITE;
/*!40000 ALTER TABLE `geo_home_zone` DISABLE KEYS */;
INSERT INTO `geo_home_zone` VALUES (1,1,'geo_home_zone campaign 1065','1',NULL,259),(2,1,'geo_home_zone campaign 1065','1',NULL,259),(3,1,'geo_home_zone campaign 1066','1',NULL,259),(4,1,'geo_home_zone campaign 1069','1',NULL,259);
/*!40000 ALTER TABLE `geo_home_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `geo_net_zone`
--

DROP TABLE IF EXISTS `geo_net_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `geo_net_zone` (
  `GEO_NET_ZONE_ID` bigint(20) NOT NULL,
  `CELL_ID` bigint(20) NOT NULL,
  `GEO_HOME_ZONE_ID` bigint(20) NOT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`GEO_NET_ZONE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `geo_net_zone`
--

LOCK TABLES `geo_net_zone` WRITE;
/*!40000 ALTER TABLE `geo_net_zone` DISABLE KEYS */;
/*!40000 ALTER TABLE `geo_net_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ignite_cache`
--

DROP TABLE IF EXISTS `ignite_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ignite_cache` (
  `CACHE_ID` int(12) NOT NULL,
  `CACHE_NAME` varchar(50) NOT NULL,
  `OBJ_CLASS` varchar(50) NOT NULL,
  `CRONTAB_EXPRESSION` varchar(50) DEFAULT NULL,
  `NUMBER_CLEAN_THREAD` int(5) DEFAULT NULL,
  `KEY_ATTRIBUTE` varchar(50) NOT NULL,
  PRIMARY KEY (`CACHE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ignite_cache`
--

LOCK TABLES `ignite_cache` WRITE;
/*!40000 ALTER TABLE `ignite_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `ignite_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `input_object`
--

DROP TABLE IF EXISTS `input_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `input_object` (
  `OBJECT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `OBJECT_NAME` varchar(255) CHARACTER SET utf8 NOT NULL,
  `OBJECT_TYPE` tinyint(4) NOT NULL,
  `OBJECT_DATA_TYPE` tinyint(4) NOT NULL,
  `OBJECT_PARENT_ID` bigint(20) DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`OBJECT_ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2086 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `input_object`
--

LOCK TABLES `input_object` WRITE;
/*!40000 ALTER TABLE `input_object` DISABLE KEYS */;
INSERT INTO `input_object` VALUES (1,'campaignMsgxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',1,1,NULL,'root'),(2,'msisdn',1,2,1,'string'),(3,'campaignType',1,3,1,'int'),(4,'eventTimestamp',1,4,1,'long'),(5,'offlineCampaignId',1,4,1,'long'),(6,'segmentId',1,4,1,'long'),(7,'errorCode',1,3,1,'int'),(8,'detailErrorCode',1,2,1,'string'),(9,'custProfile',1,1,1,'object'),(10,'cbaCustomer',1,1,1,'object'),(11,'accumInfo',1,1,1,'object'),(12,'cdrEvent',1,1,1,'object'),(13,'currResponse',1,1,1,'object'),(14,'campaignPropMap',3,1,1,'map - object'),(15,'responseList',2,1,1,'list - object'),(16,'msisdn',1,2,9,'string'),(17,'totalNotify',1,3,9,'int'),(18,'languageId',1,4,9,'long'),(19,'lastRetryTime',1,4,9,'long'),(20,'lastNotifyTime',1,4,9,'long'),(21,'lastRegisterOffer',1,4,9,'long'),(22,'lastRegisterTime',1,4,9,'long'),(23,'isBlockedPromotion',1,6,9,'boolean'),(24,'blockNotifyIdList',2,3,9,'list - long'),(25,'lastNotifyOfferList',2,3,9,'list - long'),(26,'currSegmentList',2,3,9,'list - long'),(27,'blacklistIdList',2,3,9,'list - long'),(28,'totalChannelMap',3,1,9,'map - object'),(29,'statisticNotifyMap',3,1,9,'map - object'),(30,'specialOfferMap',4,1,9,'map - list - object'),(31,'campaignInfoMap',3,1,9,'map - object'),(32,'parameterInsMap',3,1,9,'map - object'),(33,'specialPromId',1,4,30,'long'),(34,'offerExtId',1,4,30,'long'),(35,'priority',1,3,30,'int'),(36,'effDate',1,4,32,'long'),(37,'expDate',1,4,32,'long'),(38,'parameterId',1,4,32,'long'),(39,'longParam',1,4,32,'long'),(40,'stringParam',1,2,32,'string'),(41,'parameterInsId',1,4,32,'long'),(42,'stringList',2,2,32,'list - string'),(43,'longList',2,4,32,'list - long'),(44,'statisticId',1,4,29,'long'),(45,'effDate',1,4,29,'long'),(46,'expDate',1,4,29,'long'),(47,'count',1,3,29,'int'),(48,'channelId',1,4,28,'long'),(49,'value',1,3,28,'int'),(50,'campaignId',1,4,31,'long'),(51,'lastRegisterOffer',1,4,31,'long'),(52,'lastRegisterTime',1,4,31,'long'),(53,'lastNotifyTime',1,4,31,'long'),(54,'totalNotify',1,3,31,'int'),(55,'totalRegister',1,3,31,'int'),(56,'isBlockedPromotion',1,6,31,'boolean'),(57,'lastNotifyOfferList',2,4,31,'list - long'),(58,'totalChannelMap',3,1,31,'map - object'),(59,'historyInfoMap',3,1,31,'map - map - object'),(60,'id',1,4,59,'long'),(61,'expDate',1,4,59,'long'),(62,'lastOccurTime',1,4,59,'long'),(63,'offerExtId',1,4,59,'long'),(64,'currSegmentId',1,4,59,'long'),(65,'currNotifyId',1,4,59,'long'),(66,'type',1,4,59,'long'),(67,'numberNotify',1,3,59,'int'),(68,'numberRegister',1,3,59,'int'),(69,'isRetry',1,6,59,'boolean'),(70,'syntaxValue',1,2,59,'string'),(71,'segmentIdList',2,4,59,'list - long'),(72,'scenarioId',1,4,59,'long'),(73,'channelId',1,4,58,'long'),(74,'value',1,3,58,'int'),(75,'custId',1,4,10,'long'),(76,'mapCust',3,1,10,'map - object'),(77,'mapMemberShip',3,1,10,'map - object'),(78,'mapShareTime',3,1,10,'map - object'),(79,'mapGroup',3,1,10,'map - object'),(80,'mapOfferInstance',3,1,10,'map - object'),(81,'mapBundleOfferInstance',3,1,10,'map - object'),(82,'mapSub',3,1,10,'map - object'),(83,'mapBalance',3,1,10,'map - object'),(84,'mapAcmBalance',3,1,10,'map - object'),(85,'mapParameterInstance',3,1,10,'map - object'),(86,'mapHistory',3,1,10,'map - object'),(87,'mapCustDetail',3,1,10,'map - object'),(88,'mapSubQuota',3,1,10,'map - object'),(89,'mapCtmThreshold',3,1,10,'map - object'),(90,'mapSession',3,1,10,'map - object'),(91,'mapSessionRG',3,1,10,'map - object'),(92,'lstGroupId',2,4,76,'list - long'),(93,'lstMemberShipId',2,4,76,'list - long'),(94,'lstBundleOfferInstanceId',2,4,76,'list - long'),(95,'lstSubId',2,4,76,'list - long'),(96,'lstBalanceId',2,4,76,'list - long'),(97,'lstAcmBalanceId',2,4,76,'list - long'),(98,'lstHistoryId',2,4,76,'list - long'),(99,'lstOfferInstanceId',2,4,76,'list - long'),(100,'lstParameterInstanceId',2,4,76,'list - long'),(101,'level',1,3,77,'int'),(102,'currentAcmBalLstUsed',1,3,77,'int'),(103,'memberShipOf',1,3,77,'int'),(104,'effDate',1,4,77,'long'),(105,'shareTimeId',1,4,77,'long'),(106,'masterGroupId',1,4,77,'long'),(107,'expDate',1,4,77,'long'),(108,'state',1,4,77,'long'),(109,'recurringDay',1,4,77,'long'),(110,'memberShipId',1,4,77,'long'),(111,'masterCustId',1,4,77,'long'),(112,'lstAcmBalanceId',2,4,77,'list - long'),(113,'shareTimeType',1,3,78,'int'),(114,'shareTimeId',1,4,78,'long'),(115,'monthly',1,4,78,'long'),(116,'dayInMonth',1,4,78,'long'),(117,'hour',1,4,78,'long'),(118,'dayInWeek',1,4,78,'long'),(119,'groupName',1,2,79,''),(120,'masterMsisdn',1,2,79,''),(121,'groupOf',1,4,79,''),(122,'maxMember',1,3,79,''),(123,'shareType',1,3,79,''),(124,'effDate',1,4,79,''),(125,'lstMemberId',1,2,79,''),(126,'groupSize',1,3,79,''),(127,'shareBalPolicy',1,3,79,''),(128,'groupId',1,4,79,''),(129,'billingCycleTypeId',1,4,79,''),(130,'groupType',1,4,79,''),(131,'expDate',1,4,79,''),(132,'state',1,4,79,''),(133,'memberShipId',1,4,79,''),(134,'recurringDay',1,4,79,''),(135,'level',1,3,79,''),(136,'lstGroupMemberId',2,4,79,''),(137,'lstBundleOfferInstanceId',2,4,79,''),(138,'lstAcmBalanceId',2,4,79,''),(139,'lstParameterInsId',2,4,79,''),(140,'lstBalanceId',2,4,79,''),(141,'lstOfferInstanceId',2,4,79,''),(142,'offerInstanceId',1,4,80,''),(143,'upgradeType',1,4,80,''),(144,'suspendTime',1,4,80,''),(145,'effDate',1,4,80,''),(146,'upgradeOrNot',1,4,80,''),(147,'offerId',1,4,80,''),(148,'updateDate',1,4,80,''),(149,'offerOf',1,4,80,''),(150,'expDate',1,4,80,''),(151,'lstMember',1,4,80,''),(152,'state',1,4,80,''),(153,'recurringDay',1,4,80,''),(154,'level',1,3,80,''),(155,'downgradeTime',1,3,80,''),(156,'extBillingCycleTypeId',1,3,80,''),(157,'upgradeTime',1,3,80,''),(158,'lstParameterInsId',2,4,80,''),(159,'bundleOfferInstanceId',1,4,81,''),(160,'expDate',1,4,81,''),(161,'recurringDate',1,4,81,''),(162,'state',1,4,81,''),(163,'effDate',1,4,81,''),(164,'level',1,3,81,''),(165,'bundleOfferOf',1,4,81,''),(166,'bundleOfferId',2,3,81,''),(167,'effDate',1,4,82,''),(168,'regTime',1,4,82,''),(169,'bccsAcctId',1,2,82,''),(170,'endNotifyTime',1,4,82,''),(171,'msisdn',1,2,82,''),(172,'updateDate',1,4,82,''),(173,'subId',1,4,82,''),(174,'custId',1,4,82,''),(175,'expDate',1,4,82,''),(176,'lstVpnGroupName',1,2,82,''),(177,'stateSet',1,2,82,''),(178,'lstZone',1,2,82,''),(179,'password',1,2,82,''),(180,'countTopupTotal',1,3,82,''),(181,'precharge',1,4,82,''),(182,'subType',1,3,82,''),(183,'regType',1,2,82,''),(184,'mainOfferId',1,4,82,''),(185,'stateDate',1,4,82,''),(186,'contractId',1,2,82,''),(187,'bccsSubId',1,2,82,''),(188,'province',1,2,82,''),(189,'lstProp',1,2,82,''),(190,'completedDate',1,4,82,''),(191,'countTopupFailure',1,3,82,''),(192,'langId',1,4,82,''),(193,'lstSessionId',1,2,82,''),(194,'countTopupSuccess',1,3,82,''),(195,'lstCell',1,2,82,''),(196,'iccid',1,2,82,''),(197,'imsi',1,2,82,''),(198,'subCategory',1,2,82,''),(199,'lstPromotion',1,2,82,''),(200,'startNotifyTime',2,4,82,''),(201,'lstGroupId',2,4,82,''),(202,'lstAcmBalanceId',2,4,82,''),(203,'lstParameterInsId',2,4,82,''),(204,'lstBundleOfferInstanceId',2,4,82,''),(205,'lstHistoryId',2,4,82,''),(206,'lstQuotaId',2,4,82,''),(207,'lstMemberShipId',2,4,82,''),(208,'lstBalanceId',2,4,82,''),(209,'lstOfferInstanceId',2,4,82,''),(210,'reserve',1,4,83,''),(211,'deltaThreshold',1,4,83,''),(212,'balId',1,4,83,''),(213,'effDate',1,4,83,''),(214,'balOf',1,4,83,''),(215,'balTypeId',1,4,83,''),(216,'updateDate',1,4,83,''),(217,'consume',1,4,83,''),(218,'expDate',1,4,83,''),(219,'state',1,4,83,''),(220,'recurringDay',1,4,83,''),(221,'level',1,3,83,''),(222,'gross',1,4,83,''),(223,'lastReserve',1,4,83,''),(224,'quotaMax',1,4,83,''),(225,'lstCtmThresholdId',2,4,83,''),(226,'deltaThreshold',1,4,84,''),(227,'effDate',1,4,84,''),(228,'balTypeId',1,4,84,''),(229,'updateDate',1,4,84,''),(230,'expDate',1,4,84,''),(231,'state',1,4,84,''),(232,'acmBalId',1,4,84,''),(233,'value',1,4,84,''),(234,'recurringDay',1,4,84,''),(235,'level',1,3,84,''),(236,'limit',1,4,84,''),(237,'acmBalOf',1,4,84,''),(238,'billingCycleId',1,4,84,''),(239,'lastReserve',1,4,84,''),(240,'lstCtmThresholdId',2,4,84,''),(241,'level',1,3,85,''),(242,'paramOf',1,4,85,''),(243,'longParam',1,4,85,''),(244,'effDate',1,4,85,''),(245,'parameterId',1,4,85,''),(246,'updateDate',1,4,85,''),(247,'expDate',1,4,85,''),(248,'state',1,4,85,''),(249,'billingCycleId',1,4,85,''),(250,'parameterInsId',1,4,85,''),(251,'lstString',1,2,85,''),(252,'stringParam',1,2,85,''),(253,'lstLong',2,4,85,''),(254,'hisOf',1,4,86,''),(255,'hisId',1,4,86,''),(256,'expDate',1,4,86,''),(257,'hisType',1,4,86,''),(258,'hisContent',1,2,86,''),(259,'effDate',1,4,86,''),(260,'level',1,3,86,''),(261,'masterMsisdn',1,2,87,''),(262,'custType',1,2,87,''),(263,'email',1,2,87,''),(264,'address',1,2,87,''),(265,'firstName',1,2,87,''),(266,'sex',1,3,87,''),(267,'effDate',1,4,87,''),(268,'birthDay',1,4,87,''),(269,'totalSub',1,3,87,''),(270,'custId',1,4,87,''),(271,'expDate',1,4,87,''),(272,'state',1,4,87,''),(273,'custVip',1,2,87,''),(274,'lastName',1,2,87,''),(275,'bccsCustId',1,2,87,''),(276,'password',1,2,87,''),(277,'pepId',1,4,88,''),(278,'billingCycleTypeId',1,4,88,''),(279,'subId',1,4,88,''),(280,'ratingGroupId',1,2,88,''),(281,'quota',1,4,88,''),(282,'custId',1,4,88,''),(283,'expDate',1,4,88,''),(284,'state',1,3,88,''),(285,'subQuotaId',1,4,88,''),(286,'service',1,3,88,''),(287,'effDate',1,4,88,''),(288,'thresholdName',1,2,89,''),(289,'periodSent',1,4,89,''),(290,'thresholdId',1,4,89,''),(291,'timeSent',1,4,89,''),(292,'triggerId',1,4,89,''),(293,'externalId',1,4,89,''),(294,'value',1,4,89,''),(295,'ctmThresholdId',1,4,89,''),(296,'thresholdType',1,4,89,''),(297,'lstTriggerId',2,4,89,''),(298,'srvcExpTime',1,4,90,''),(299,'extAcmBal',1,2,90,''),(300,'sessionType',1,4,90,''),(301,'extBal',1,2,90,''),(302,'srcvBeginTime',1,4,90,''),(303,'extDiameter',1,2,90,''),(304,'cardInfo',1,2,90,''),(305,'sessionName',1,2,90,''),(306,'lstEventTrigger',1,2,90,''),(307,'redirectType',1,4,90,''),(308,'msisdn',1,2,90,''),(309,'subId',1,4,90,''),(310,'langId',1,4,90,''),(311,'totalUnitUse',1,4,90,''),(312,'custId',1,4,90,''),(313,'version',1,3,90,''),(314,'prodSpecId',1,4,90,''),(315,'srvcType',1,4,90,''),(316,'extProp',1,2,90,''),(317,'eventId',1,4,90,''),(318,'lstSessionRatingGroupId',2,3,90,''),(319,'extAcmBal',1,2,91,''),(320,'extBal',1,2,91,''),(321,'ratingGroupId',1,3,91,''),(322,'isMonitor',1,3,91,''),(323,'extDiameter',1,2,91,''),(324,'profilePepId',1,3,91,''),(325,'hashRG',1,3,91,''),(326,'expTime',1,4,91,''),(327,'redirectType',1,3,91,''),(328,'totalUnitUse',1,4,91,''),(329,'cellInfo',1,2,91,''),(330,'extProp',1,2,91,''),(331,'eventId',1,3,91,''),(332,'lstCalcPricePlan',2,4,91,''),(333,'lstReservePricePlan',2,4,91,''),(334,'propertyMap',3,2,11,''),(335,'campaignId',1,4,14,''),(336,'telecomServiceId',1,3,14,''),(337,'sessionId',1,6,14,''),(338,'propertyMap',3,2,14,''),(339,'campaignId',1,4,13,''),(340,'errorCode',1,3,13,''),(341,'segmentId',1,4,13,''),(342,'resultType',1,3,13,''),(343,'promotionBlockList',2,1,13,''),(344,'promotionBlockId',1,2,343,''),(345,'promotionBlockName',1,3,343,''),(346,'ocsBehaviourList',1,4,343,''),(347,'notifyTriggerList',1,4,343,''),(348,'scenarioTriggerList',1,6,343,''),(354,'campaignId',1,2,15,''),(355,'errorCode',1,3,15,''),(356,'segmentId',1,6,15,''),(357,'resultType',2,1,15,''),(358,'promotionBlockList',2,1,15,''),(359,'promotionBlockId',1,2,358,''),(360,'promotionBlockName',1,4,358,''),(361,'ocsBehaviourList',1,4,358,''),(362,'notifyTriggerList',1,6,358,''),(363,'scenarioTriggerList',1,4,358,''),(364,'propertiesMap',3,1,12,NULL),(365,'01',1,2,364,''),(366,'02',1,2,364,''),(367,'03',1,2,364,''),(368,'04',1,2,364,''),(369,'05',1,2,364,''),(370,'06',1,2,364,''),(371,'07',1,2,364,''),(372,'08',1,2,364,''),(373,'1146',1,2,364,''),(374,'271',1,2,364,''),(375,'853',1,2,364,''),(376,'873',1,2,364,''),(377,'9001',1,2,364,''),(378,'9002',1,2,364,''),(379,'9003',1,2,364,''),(380,'9004',1,2,364,''),(381,'9005',1,2,364,''),(382,'9006',1,2,364,''),(383,'9007',1,2,364,''),(384,'9008',1,2,364,''),(385,'9009',1,2,364,''),(386,'9010',1,2,364,''),(387,'90100',1,2,364,''),(388,'90100021',1,2,364,''),(389,'901001',1,2,364,''),(390,'90101',1,2,364,''),(391,'90102',1,2,364,''),(392,'90103',1,2,364,''),(393,'90104',1,2,364,''),(394,'90105',1,2,364,''),(395,'90106',1,2,364,''),(396,'90107',1,2,364,''),(397,'90108',1,2,364,''),(398,'90109',1,2,364,''),(399,'9011',1,2,364,''),(400,'90110',1,2,364,''),(401,'90111',1,2,364,''),(402,'90112',1,2,364,''),(403,'90113',1,2,364,''),(404,'90114',1,2,364,''),(405,'90115',1,2,364,''),(406,'90116',1,2,364,''),(407,'90117',1,2,364,''),(408,'90118',1,2,364,''),(409,'90119',1,2,364,''),(410,'9012',1,2,364,''),(411,'90120',1,2,364,''),(412,'90121',1,2,364,''),(413,'90122',1,2,364,''),(414,'90123',1,2,364,''),(415,'90124',1,2,364,''),(416,'9013',1,2,364,''),(417,'9014',1,2,364,''),(418,'9015',1,2,364,''),(419,'9016',1,2,364,''),(420,'9017',1,2,364,''),(421,'9018',1,2,364,''),(422,'9019',1,2,364,''),(423,'9020',1,2,364,''),(424,'902001',1,2,364,''),(425,'9021',1,2,364,''),(426,'90212',1,2,364,''),(427,'90213',1,2,364,''),(428,'90214',1,2,364,''),(429,'9022',1,2,364,''),(430,'9023',1,2,364,''),(431,'9024',1,2,364,''),(432,'9025',1,2,364,''),(433,'9026',1,2,364,''),(434,'9027',1,2,364,''),(435,'9028',1,2,364,''),(436,'9029',1,2,364,''),(437,'9030',1,2,364,''),(438,'903000',1,2,364,''),(439,'903001',1,2,364,''),(440,'903016',1,2,364,''),(441,'903017',1,2,364,''),(442,'903018',1,2,364,''),(443,'9031',1,2,364,''),(444,'9032',1,2,364,''),(445,'9033',1,2,364,''),(446,'9034',1,2,364,''),(447,'9035',1,2,364,''),(448,'9036',1,2,364,''),(449,'9037',1,2,364,''),(450,'9038',1,2,364,''),(451,'9039',1,2,364,''),(452,'9040',1,2,364,''),(453,'9041',1,2,364,''),(454,'9042',1,2,364,''),(455,'9043',1,2,364,''),(456,'9044',1,2,364,''),(457,'9045',1,2,364,''),(458,'9046',1,2,364,''),(459,'9047',1,2,364,''),(460,'9048',1,2,364,''),(461,'9049',1,2,364,''),(462,'9050',1,2,364,''),(463,'90500',1,2,364,''),(464,'90502',1,2,364,''),(465,'90503',1,2,364,''),(466,'90504',1,2,364,''),(467,'90505',1,2,364,''),(468,'90506',1,2,364,''),(469,'90507',1,2,364,''),(470,'90508',1,2,364,''),(471,'90509',1,2,364,''),(472,'9051',1,2,364,''),(473,'90510',1,2,364,''),(474,'90512',1,2,364,''),(475,'90513',1,2,364,''),(476,'90514',1,2,364,''),(477,'90515',1,2,364,''),(478,'90516',1,2,364,''),(479,'90517',1,2,364,''),(480,'9052',1,2,364,''),(481,'9053',1,2,364,''),(482,'9054',1,2,364,''),(483,'9055',1,2,364,''),(484,'9056',1,2,364,''),(485,'9057',1,2,364,''),(486,'9058',1,2,364,''),(487,'9059',1,2,364,''),(488,'9060',1,2,364,''),(489,'9061',1,2,364,''),(490,'9062',1,2,364,''),(491,'9063',1,2,364,''),(492,'9064',1,2,364,''),(493,'9065',1,2,364,''),(494,'9066',1,2,364,''),(495,'9067',1,2,364,''),(496,'9068',1,2,364,''),(497,'9069',1,2,364,''),(498,'9070',1,2,364,''),(499,'9071',1,2,364,''),(500,'9072',1,2,364,''),(501,'9073',1,2,364,''),(502,'9074',1,2,364,''),(503,'9075',1,2,364,''),(504,'9076',1,2,364,''),(505,'9077',1,2,364,''),(506,'9078',1,2,364,''),(507,'9079',1,2,364,''),(508,'9080',1,2,364,''),(509,'9081',1,2,364,''),(510,'9082',1,2,364,''),(511,'9083',1,2,364,''),(512,'9084',1,2,364,''),(513,'9085',1,2,364,''),(514,'9086',1,2,364,''),(515,'9087',1,2,364,''),(516,'9088',1,2,364,''),(517,'9089',1,2,364,''),(518,'9090',1,2,364,''),(519,'90900',1,2,364,''),(520,'9091',1,2,364,''),(521,'9092',1,2,364,''),(522,'9093',1,2,364,''),(523,'9094',1,2,364,''),(524,'9095',1,2,364,''),(525,'9096',1,2,364,''),(526,'9097',1,2,364,''),(527,'9098',1,2,364,''),(528,'9099',1,2,364,''),(529,'9101',1,2,364,''),(530,'9102',1,2,364,''),(531,'9103',1,2,364,''),(532,'9104',1,2,364,''),(533,'9105',1,2,364,''),(534,'9106',1,2,364,''),(535,'9107',1,2,364,''),(536,'9108',1,2,364,''),(537,'9109',1,2,364,''),(538,'9110',1,2,364,''),(539,'91100',1,2,364,''),(540,'91100021',1,2,364,''),(541,'911001',1,2,364,''),(542,'91101',1,2,364,''),(543,'91102',1,2,364,''),(544,'91103',1,2,364,''),(545,'91104',1,2,364,''),(546,'91105',1,2,364,''),(547,'91106',1,2,364,''),(548,'91107',1,2,364,''),(549,'91108',1,2,364,''),(550,'91109',1,2,364,''),(551,'9111',1,2,364,''),(552,'91110',1,2,364,''),(553,'91111',1,2,364,''),(554,'91112',1,2,364,''),(555,'91113',1,2,364,''),(556,'91114',1,2,364,''),(557,'91115',1,2,364,''),(558,'91116',1,2,364,''),(559,'91117',1,2,364,''),(560,'91118',1,2,364,''),(561,'91119',1,2,364,''),(562,'9112',1,2,364,''),(563,'91120',1,2,364,''),(564,'91121',1,2,364,''),(565,'91122',1,2,364,''),(566,'91123',1,2,364,''),(567,'91124',1,2,364,''),(568,'9113',1,2,364,''),(569,'9114',1,2,364,''),(570,'9115',1,2,364,''),(571,'9116',1,2,364,''),(572,'9117',1,2,364,''),(573,'9118',1,2,364,''),(574,'9119',1,2,364,''),(575,'9120',1,2,364,''),(576,'912001',1,2,364,''),(577,'9121',1,2,364,''),(578,'91212',1,2,364,''),(579,'91213',1,2,364,''),(580,'91214',1,2,364,''),(581,'9122',1,2,364,''),(582,'9123',1,2,364,''),(583,'9124',1,2,364,''),(584,'9125',1,2,364,''),(585,'9126',1,2,364,''),(586,'9127',1,2,364,''),(587,'9128',1,2,364,''),(588,'9129',1,2,364,''),(589,'9130',1,2,364,''),(590,'913000',1,2,364,''),(591,'913001',1,2,364,''),(592,'913016',1,2,364,''),(593,'913017',1,2,364,''),(594,'913018',1,2,364,''),(595,'9131',1,2,364,''),(596,'9132',1,2,364,''),(597,'9133',1,2,364,''),(598,'9134',1,2,364,''),(599,'9135',1,2,364,''),(600,'9136',1,2,364,''),(601,'9137',1,2,364,''),(602,'9138',1,2,364,''),(603,'9139',1,2,364,''),(604,'9140',1,2,364,''),(605,'9141',1,2,364,''),(606,'9142',1,2,364,''),(607,'9143',1,2,364,''),(608,'9144',1,2,364,''),(609,'9145',1,2,364,''),(610,'9146',1,2,364,''),(611,'9147',1,2,364,''),(612,'9148',1,2,364,''),(613,'9149',1,2,364,''),(614,'9150',1,2,364,''),(615,'91500',1,2,364,''),(616,'91502',1,2,364,''),(617,'91503',1,2,364,''),(618,'91504',1,2,364,''),(619,'91505',1,2,364,''),(620,'91506',1,2,364,''),(621,'91507',1,2,364,''),(622,'91508',1,2,364,''),(623,'91509',1,2,364,''),(624,'9151',1,2,364,''),(625,'91510',1,2,364,''),(626,'91512',1,2,364,''),(627,'91513',1,2,364,''),(628,'91514',1,2,364,''),(629,'91515',1,2,364,''),(630,'91516',1,2,364,''),(631,'91517',1,2,364,''),(632,'9152',1,2,364,''),(633,'9153',1,2,364,''),(634,'9154',1,2,364,''),(635,'9155',1,2,364,''),(636,'9156',1,2,364,''),(637,'9157',1,2,364,''),(638,'9158',1,2,364,''),(639,'9159',1,2,364,''),(640,'9160',1,2,364,''),(641,'9161',1,2,364,''),(642,'9162',1,2,364,''),(643,'9163',1,2,364,''),(644,'9164',1,2,364,''),(645,'9165',1,2,364,''),(646,'9166',1,2,364,''),(647,'9167',1,2,364,''),(648,'9168',1,2,364,''),(649,'9169',1,2,364,''),(650,'9170',1,2,364,''),(651,'9171',1,2,364,''),(652,'9172',1,2,364,''),(653,'9173',1,2,364,''),(654,'9174',1,2,364,''),(655,'9175',1,2,364,''),(656,'9176',1,2,364,''),(657,'9177',1,2,364,''),(658,'9178',1,2,364,''),(659,'9179',1,2,364,''),(660,'9180',1,2,364,''),(661,'9181',1,2,364,''),(662,'9182',1,2,364,''),(663,'9183',1,2,364,''),(664,'9184',1,2,364,''),(665,'9185',1,2,364,''),(666,'9186',1,2,364,''),(667,'9187',1,2,364,''),(668,'9188',1,2,364,''),(669,'9189',1,2,364,''),(670,'9190',1,2,364,''),(671,'91900',1,2,364,''),(672,'9191',1,2,364,''),(673,'9192',1,2,364,''),(674,'9193',1,2,364,''),(675,'9194',1,2,364,''),(676,'9195',1,2,364,''),(677,'9196',1,2,364,''),(678,'9197',1,2,364,''),(679,'9198',1,2,364,''),(680,'9199',1,2,364,''),(681,'9201',1,2,364,''),(682,'9202',1,2,364,''),(683,'9203',1,2,364,''),(684,'9204',1,2,364,''),(685,'9205',1,2,364,''),(686,'9206',1,2,364,''),(687,'9207',1,2,364,''),(688,'9208',1,2,364,''),(689,'9209',1,2,364,''),(690,'9210',1,2,364,''),(691,'92100',1,2,364,''),(692,'92100021',1,2,364,''),(693,'921001',1,2,364,''),(694,'92101',1,2,364,''),(695,'92102',1,2,364,''),(696,'92103',1,2,364,''),(697,'92104',1,2,364,''),(698,'92105',1,2,364,''),(699,'92106',1,2,364,''),(700,'92107',1,2,364,''),(701,'92108',1,2,364,''),(702,'92109',1,2,364,''),(703,'9211',1,2,364,''),(704,'92110',1,2,364,''),(705,'92111',1,2,364,''),(706,'92112',1,2,364,''),(707,'92113',1,2,364,''),(708,'92114',1,2,364,''),(709,'92115',1,2,364,''),(710,'92116',1,2,364,''),(711,'92117',1,2,364,''),(712,'92118',1,2,364,''),(713,'92119',1,2,364,''),(714,'9212',1,2,364,''),(715,'92120',1,2,364,''),(716,'92121',1,2,364,''),(717,'92122',1,2,364,''),(718,'92123',1,2,364,''),(719,'92124',1,2,364,''),(720,'9213',1,2,364,''),(721,'9214',1,2,364,''),(722,'9215',1,2,364,''),(723,'9216',1,2,364,''),(724,'9217',1,2,364,''),(725,'9218',1,2,364,''),(726,'9219',1,2,364,''),(727,'9220',1,2,364,''),(728,'922001',1,2,364,''),(729,'9221',1,2,364,''),(730,'92212',1,2,364,''),(731,'92213',1,2,364,''),(732,'92214',1,2,364,''),(733,'9222',1,2,364,''),(734,'9223',1,2,364,''),(735,'9224',1,2,364,''),(736,'9225',1,2,364,''),(737,'9226',1,2,364,''),(738,'9227',1,2,364,''),(739,'9228',1,2,364,''),(740,'9229',1,2,364,''),(741,'9230',1,2,364,''),(742,'923000',1,2,364,''),(743,'923001',1,2,364,''),(744,'923016',1,2,364,''),(745,'923017',1,2,364,''),(746,'923018',1,2,364,''),(747,'9231',1,2,364,''),(748,'9232',1,2,364,''),(749,'9233',1,2,364,''),(750,'9234',1,2,364,''),(751,'9235',1,2,364,''),(752,'9236',1,2,364,''),(753,'9237',1,2,364,''),(754,'9238',1,2,364,''),(755,'9239',1,2,364,''),(756,'9240',1,2,364,''),(757,'9241',1,2,364,''),(758,'9242',1,2,364,''),(759,'9243',1,2,364,''),(760,'9244',1,2,364,''),(761,'9245',1,2,364,''),(762,'9246',1,2,364,''),(763,'9247',1,2,364,''),(764,'9248',1,2,364,''),(765,'9249',1,2,364,''),(766,'9250',1,2,364,''),(767,'92500',1,2,364,''),(768,'92502',1,2,364,''),(769,'92503',1,2,364,''),(770,'92504',1,2,364,''),(771,'92505',1,2,364,''),(772,'92506',1,2,364,''),(773,'92507',1,2,364,''),(774,'92508',1,2,364,''),(775,'92509',1,2,364,''),(776,'9251',1,2,364,''),(777,'92510',1,2,364,''),(778,'92512',1,2,364,''),(779,'92513',1,2,364,''),(780,'92514',1,2,364,''),(781,'92515',1,2,364,''),(782,'92516',1,2,364,''),(783,'92517',1,2,364,''),(784,'9252',1,2,364,''),(785,'9253',1,2,364,''),(786,'9254',1,2,364,''),(787,'9255',1,2,364,''),(788,'9256',1,2,364,''),(789,'9257',1,2,364,''),(790,'9258',1,2,364,''),(791,'9259',1,2,364,''),(792,'9260',1,2,364,''),(793,'9261',1,2,364,''),(794,'9262',1,2,364,''),(795,'9263',1,2,364,''),(796,'9264',1,2,364,''),(797,'9265',1,2,364,''),(798,'9266',1,2,364,''),(799,'9267',1,2,364,''),(800,'9268',1,2,364,''),(801,'9269',1,2,364,''),(802,'9270',1,2,364,''),(803,'9271',1,2,364,''),(804,'9272',1,2,364,''),(805,'9273',1,2,364,''),(806,'9274',1,2,364,''),(807,'9275',1,2,364,''),(808,'9276',1,2,364,''),(809,'9277',1,2,364,''),(810,'9278',1,2,364,''),(811,'9279',1,2,364,''),(812,'9280',1,2,364,''),(813,'9281',1,2,364,''),(814,'9282',1,2,364,''),(815,'9283',1,2,364,''),(816,'9284',1,2,364,''),(817,'9285',1,2,364,''),(818,'9286',1,2,364,''),(819,'9287',1,2,364,''),(820,'9288',1,2,364,''),(821,'9289',1,2,364,''),(822,'9290',1,2,364,''),(823,'92900',1,2,364,''),(824,'9291',1,2,364,''),(825,'9292',1,2,364,''),(826,'9293',1,2,364,''),(827,'9294',1,2,364,''),(828,'9295',1,2,364,''),(829,'9296',1,2,364,''),(830,'9297',1,2,364,''),(831,'9298',1,2,364,''),(832,'9299',1,2,364,''),(833,'930',1,2,364,''),(834,'9300',1,2,364,''),(835,'959',1,2,364,''),(836,'976',1,2,364,''),(837,'977',1,2,364,''),(838,'978',1,2,364,''),(839,'980',1,2,364,''),(840,'ACCESS_PREFIX',1,2,364,''),(841,'ACCOUNT_CODE',1,2,364,''),(842,'AcctBookListPresent',1,2,364,''),(843,'ACCT_BOOK_TYPE',1,2,364,''),(844,'ACCT_ID',1,2,364,''),(845,'ACCT_ID1',1,2,364,''),(846,'ACCT_ID2',1,2,364,''),(847,'ACCT_ID3',1,2,364,''),(848,'ACCT_ID4',1,2,364,''),(849,'ACCT_ID_LIST',1,2,364,''),(850,'ACCT_ITEM_TYPE_ID1',1,2,364,''),(851,'ACCT_ITEM_TYPE_ID2',1,2,364,''),(852,'ACCT_ITEM_TYPE_ID3',1,2,364,''),(853,'ACCT_ITEM_TYPE_ID4',1,2,364,''),(854,'ACCT_ITEM_TYPE_ID_LIST',1,2,364,''),(855,'ACCT_KEY_1',1,2,364,''),(856,'ACCT_KEY_10',1,2,364,''),(857,'ACCT_KEY_2',1,2,364,''),(858,'ACCT_KEY_3',1,2,364,''),(859,'ACCT_KEY_4',1,2,364,''),(860,'ACCT_KEY_5',1,2,364,''),(861,'ACCT_KEY_6',1,2,364,''),(862,'ACCT_KEY_7',1,2,364,''),(863,'ACCT_KEY_8',1,2,364,''),(864,'ACCT_KEY_9',1,2,364,''),(865,'ACCT_RES_ID1',1,2,364,''),(866,'ACCT_RES_ID10',1,2,364,''),(867,'ACCT_RES_ID100',1,2,364,''),(868,'ACCT_RES_ID1001',1,2,364,''),(869,'ACCT_RES_ID101',1,2,364,''),(870,'ACCT_RES_ID102',1,2,364,''),(871,'ACCT_RES_ID103',1,2,364,''),(872,'ACCT_RES_ID11',1,2,364,''),(873,'ACCT_RES_ID12',1,2,364,''),(874,'ACCT_RES_ID13',1,2,364,''),(875,'ACCT_RES_ID14',1,2,364,''),(876,'ACCT_RES_ID15',1,2,364,''),(877,'ACCT_RES_ID16',1,2,364,''),(878,'ACCT_RES_ID17',1,2,364,''),(879,'ACCT_RES_ID18',1,2,364,''),(880,'ACCT_RES_ID1819665236',1,2,364,''),(881,'ACCT_RES_ID19',1,2,364,''),(882,'ACCT_RES_ID2',1,2,364,''),(883,'ACCT_RES_ID20',1,2,364,''),(884,'ACCT_RES_ID2001',1,2,364,''),(885,'ACCT_RES_ID21',1,2,364,''),(886,'ACCT_RES_ID212',1,2,364,''),(887,'ACCT_RES_ID22',1,2,364,''),(888,'ACCT_RES_ID23',1,2,364,''),(889,'ACCT_RES_ID24',1,2,364,''),(890,'ACCT_RES_ID25',1,2,364,''),(891,'ACCT_RES_ID26',1,2,364,''),(892,'ACCT_RES_ID27',1,2,364,''),(893,'ACCT_RES_ID28',1,2,364,''),(894,'ACCT_RES_ID29',1,2,364,''),(895,'ACCT_RES_ID3',1,2,364,''),(896,'ACCT_RES_ID30',1,2,364,''),(897,'ACCT_RES_ID31',1,2,364,''),(898,'ACCT_RES_ID32',1,2,364,''),(899,'ACCT_RES_ID33',1,2,364,''),(900,'ACCT_RES_ID34',1,2,364,''),(901,'ACCT_RES_ID35',1,2,364,''),(902,'ACCT_RES_ID36',1,2,364,''),(903,'ACCT_RES_ID37',1,2,364,''),(904,'ACCT_RES_ID38',1,2,364,''),(905,'ACCT_RES_ID39',1,2,364,''),(906,'ACCT_RES_ID4',1,2,364,''),(907,'ACCT_RES_ID40',1,2,364,''),(908,'ACCT_RES_ID41',1,2,364,''),(909,'ACCT_RES_ID42',1,2,364,''),(910,'ACCT_RES_ID43',1,2,364,''),(911,'ACCT_RES_ID44',1,2,364,''),(912,'ACCT_RES_ID45',1,2,364,''),(913,'ACCT_RES_ID46',1,2,364,''),(914,'ACCT_RES_ID47',1,2,364,''),(915,'ACCT_RES_ID48',1,2,364,''),(916,'ACCT_RES_ID49',1,2,364,''),(917,'ACCT_RES_ID5',1,2,364,''),(918,'ACCT_RES_ID50',1,2,364,''),(919,'ACCT_RES_ID500',1,2,364,''),(920,'ACCT_RES_ID51',1,2,364,''),(921,'ACCT_RES_ID6',1,2,364,''),(922,'ACCT_RES_ID7',1,2,364,''),(923,'ACCT_RES_ID8',1,2,364,''),(924,'ACCT_RES_ID9',1,2,364,''),(925,'ACCT_RES_ID900',1,2,364,''),(926,'ACCT_RES_ID_LIST',1,2,364,''),(927,'ACM_DATE',1,2,364,''),(928,'ACTION',1,2,364,''),(929,'ActionDate',1,2,364,''),(930,'ACTION_DATE',1,2,364,''),(931,'ActivateTime',1,2,364,''),(932,'ACTIVATE_TIME',1,2,364,''),(933,'ACTIVE_DATE',1,2,364,''),(934,'ACTUAL_USAGE',1,2,364,''),(935,'ACTUAL_USAGE2',1,2,364,''),(936,'Add_new_propety',1,2,364,''),(937,'ADVICE_FOR_CREDIT_LIMIT',1,2,364,''),(938,'AMOUNT',1,2,364,''),(939,'AOC_FLAG',1,2,364,''),(940,'APN',1,2,364,''),(941,'BAGPP_RAT_TYPE',1,2,364,''),(942,'BAGPP_SGSN_MCC_MNC',1,2,364,''),(943,'BAL',1,2,364,''),(944,'Balance',1,2,364,''),(945,'BALANCE1',1,2,364,''),(946,'BALANCE2',1,2,364,''),(947,'BALANCE3',1,2,364,''),(948,'BALANCE4',1,2,364,''),(949,'BALINFO',1,2,364,''),(950,'BALTYPEID',1,2,364,''),(951,'BAL_1',1,2,364,''),(952,'BAL_10',1,2,364,''),(953,'BAL_100',1,2,364,''),(954,'BAL_1001',1,2,364,''),(955,'BAL_101',1,2,364,''),(956,'BAL_102',1,2,364,''),(957,'BAL_103',1,2,364,''),(958,'BAL_11',1,2,364,''),(959,'BAL_12',1,2,364,''),(960,'BAL_13',1,2,364,''),(961,'BAL_14',1,2,364,''),(962,'BAL_15',1,2,364,''),(963,'BAL_16',1,2,364,''),(964,'BAL_17',1,2,364,''),(965,'BAL_18',1,2,364,''),(966,'BAL_1819665236',1,2,364,''),(967,'BAL_19',1,2,364,''),(968,'BAL_2',1,2,364,''),(969,'BAL_20',1,2,364,''),(970,'BAL_2001',1,2,364,''),(971,'BAL_21',1,2,364,''),(972,'BAL_212',1,2,364,''),(973,'BAL_22',1,2,364,''),(974,'BAL_23',1,2,364,''),(975,'BAL_24',1,2,364,''),(976,'BAL_25',1,2,364,''),(977,'BAL_26',1,2,364,''),(978,'BAL_27',1,2,364,''),(979,'BAL_28',1,2,364,''),(980,'BAL_29',1,2,364,''),(981,'BAL_3',1,2,364,''),(982,'BAL_30',1,2,364,''),(983,'BAL_31',1,2,364,''),(984,'BAL_32',1,2,364,''),(985,'BAL_33',1,2,364,''),(986,'BAL_34',1,2,364,''),(987,'BAL_35',1,2,364,''),(988,'BAL_36',1,2,364,''),(989,'BAL_37',1,2,364,''),(990,'BAL_38',1,2,364,''),(991,'BAL_39',1,2,364,''),(992,'BAL_4',1,2,364,''),(993,'BAL_40',1,2,364,''),(994,'BAL_41',1,2,364,''),(995,'BAL_42',1,2,364,''),(996,'BAL_43',1,2,364,''),(997,'BAL_44',1,2,364,''),(998,'BAL_45',1,2,364,''),(999,'BAL_46',1,2,364,''),(1000,'BAL_47',1,2,364,''),(1001,'BAL_48',1,2,364,''),(1002,'BAL_49',1,2,364,''),(1003,'BAL_5',1,2,364,''),(1004,'BAL_50',1,2,364,''),(1005,'BAL_500',1,2,364,''),(1006,'BAL_51',1,2,364,''),(1007,'BAL_55',1,2,364,''),(1008,'BAL_6',1,2,364,''),(1009,'BAL_61',1,2,364,''),(1010,'BAL_62',1,2,364,''),(1011,'BAL_63',1,2,364,''),(1012,'BAL_7',1,2,364,''),(1013,'BAL_8',1,2,364,''),(1014,'BAL_9',1,2,364,''),(1015,'BAL_900',1,2,364,''),(1016,'BAL_ID',1,2,364,''),(1017,'BAL_ID1',1,2,364,''),(1018,'BAL_ID10',1,2,364,''),(1019,'BAL_ID100',1,2,364,''),(1020,'BAL_ID1001',1,2,364,''),(1021,'BAL_ID101',1,2,364,''),(1022,'BAL_ID102',1,2,364,''),(1023,'BAL_ID103',1,2,364,''),(1024,'BAL_ID11',1,2,364,''),(1025,'BAL_ID12',1,2,364,''),(1026,'BAL_ID13',1,2,364,''),(1027,'BAL_ID14',1,2,364,''),(1028,'BAL_ID15',1,2,364,''),(1029,'BAL_ID16',1,2,364,''),(1030,'BAL_ID17',1,2,364,''),(1031,'BAL_ID18',1,2,364,''),(1032,'BAL_ID1819665236',1,2,364,''),(1033,'BAL_ID19',1,2,364,''),(1034,'BAL_ID2',1,2,364,''),(1035,'BAL_ID20',1,2,364,''),(1036,'BAL_ID2001',1,2,364,''),(1037,'BAL_ID21',1,2,364,''),(1038,'BAL_ID212',1,2,364,''),(1039,'BAL_ID22',1,2,364,''),(1040,'BAL_ID23',1,2,364,''),(1041,'BAL_ID24',1,2,364,''),(1042,'BAL_ID25',1,2,364,''),(1043,'BAL_ID26',1,2,364,''),(1044,'BAL_ID27',1,2,364,''),(1045,'BAL_ID28',1,2,364,''),(1046,'BAL_ID29',1,2,364,''),(1047,'BAL_ID3',1,2,364,''),(1048,'BAL_ID30',1,2,364,''),(1049,'BAL_ID31',1,2,364,''),(1050,'BAL_ID32',1,2,364,''),(1051,'BAL_ID33',1,2,364,''),(1052,'BAL_ID34',1,2,364,''),(1053,'BAL_ID35',1,2,364,''),(1054,'BAL_ID36',1,2,364,''),(1055,'BAL_ID37',1,2,364,''),(1056,'BAL_ID38',1,2,364,''),(1057,'BAL_ID39',1,2,364,''),(1058,'BAL_ID4',1,2,364,''),(1059,'BAL_ID40',1,2,364,''),(1060,'BAL_ID41',1,2,364,''),(1061,'BAL_ID42',1,2,364,''),(1062,'BAL_ID43',1,2,364,''),(1063,'BAL_ID44',1,2,364,''),(1064,'BAL_ID45',1,2,364,''),(1065,'BAL_ID46',1,2,364,''),(1066,'BAL_ID47',1,2,364,''),(1067,'BAL_ID48',1,2,364,''),(1068,'BAL_ID49',1,2,364,''),(1069,'BAL_ID5',1,2,364,''),(1070,'BAL_ID50',1,2,364,''),(1071,'BAL_ID500',1,2,364,''),(1072,'BAL_ID51',1,2,364,''),(1073,'BAL_ID55',1,2,364,''),(1074,'BAL_ID6',1,2,364,''),(1075,'BAL_ID61',1,2,364,''),(1076,'BAL_ID62',1,2,364,''),(1077,'BAL_ID63',1,2,364,''),(1078,'BAL_ID7',1,2,364,''),(1079,'BAL_ID8',1,2,364,''),(1080,'BAL_ID9',1,2,364,''),(1081,'BAL_ID900',1,2,364,''),(1082,'BAL_ID_LIST',1,2,364,''),(1083,'BAL_INFO',1,2,364,''),(1084,'BAL_TYPE_ID',1,2,364,''),(1085,'BAL_TYPE_ID_1',1,2,364,''),(1086,'BAL_TYPE_ID_10',1,2,364,''),(1087,'BAL_TYPE_ID_100',1,2,364,''),(1088,'BAL_TYPE_ID_1001',1,2,364,''),(1089,'BAL_TYPE_ID_101',1,2,364,''),(1090,'BAL_TYPE_ID_102',1,2,364,''),(1091,'BAL_TYPE_ID_103',1,2,364,''),(1092,'BAL_TYPE_ID_11',1,2,364,''),(1093,'BAL_TYPE_ID_12',1,2,364,''),(1094,'BAL_TYPE_ID_13',1,2,364,''),(1095,'BAL_TYPE_ID_14',1,2,364,''),(1096,'BAL_TYPE_ID_15',1,2,364,''),(1097,'BAL_TYPE_ID_16',1,2,364,''),(1098,'BAL_TYPE_ID_17',1,2,364,''),(1099,'BAL_TYPE_ID_18',1,2,364,''),(1100,'BAL_TYPE_ID_1819665236',1,2,364,''),(1101,'BAL_TYPE_ID_19',1,2,364,''),(1102,'BAL_TYPE_ID_20',1,2,364,''),(1103,'BAL_TYPE_ID_2001',1,2,364,''),(1104,'BAL_TYPE_ID_21',1,2,364,''),(1105,'BAL_TYPE_ID_212',1,2,364,''),(1106,'BAL_TYPE_ID_22',1,2,364,''),(1107,'BAL_TYPE_ID_23',1,2,364,''),(1108,'BAL_TYPE_ID_24',1,2,364,''),(1109,'BAL_TYPE_ID_25',1,2,364,''),(1110,'BAL_TYPE_ID_26',1,2,364,''),(1111,'BAL_TYPE_ID_27',1,2,364,''),(1112,'BAL_TYPE_ID_28',1,2,364,''),(1113,'BAL_TYPE_ID_29',1,2,364,''),(1114,'BAL_TYPE_ID_3',1,2,364,''),(1115,'BAL_TYPE_ID_30',1,2,364,''),(1116,'BAL_TYPE_ID_31',1,2,364,''),(1117,'BAL_TYPE_ID_32',1,2,364,''),(1118,'BAL_TYPE_ID_33',1,2,364,''),(1119,'BAL_TYPE_ID_34',1,2,364,''),(1120,'BAL_TYPE_ID_35',1,2,364,''),(1121,'BAL_TYPE_ID_36',1,2,364,''),(1122,'BAL_TYPE_ID_37',1,2,364,''),(1123,'BAL_TYPE_ID_38',1,2,364,''),(1124,'BAL_TYPE_ID_39',1,2,364,''),(1125,'BAL_TYPE_ID_40',1,2,364,''),(1126,'BAL_TYPE_ID_41',1,2,364,''),(1127,'BAL_TYPE_ID_42',1,2,364,''),(1128,'BAL_TYPE_ID_43',1,2,364,''),(1129,'BAL_TYPE_ID_44',1,2,364,''),(1130,'BAL_TYPE_ID_45',1,2,364,''),(1131,'BAL_TYPE_ID_46',1,2,364,''),(1132,'BAL_TYPE_ID_47',1,2,364,''),(1133,'BAL_TYPE_ID_48',1,2,364,''),(1134,'BAL_TYPE_ID_49',1,2,364,''),(1135,'BAL_TYPE_ID_50',1,2,364,''),(1136,'BAL_TYPE_ID_500',1,2,364,''),(1137,'BAL_TYPE_ID_51',1,2,364,''),(1138,'BAL_TYPE_ID_55',1,2,364,''),(1139,'BAL_TYPE_ID_6',1,2,364,''),(1140,'BAL_TYPE_ID_61',1,2,364,''),(1141,'BAL_TYPE_ID_62',1,2,364,''),(1142,'BAL_TYPE_ID_63',1,2,364,''),(1143,'BAL_TYPE_ID_8',1,2,364,''),(1144,'BAL_TYPE_ID_900',1,2,364,''),(1145,'BEARERCAPABILITY',1,2,364,''),(1146,'BEARERCAPABILITY2',1,2,364,''),(1147,'BEARER_PROTOCOL_TYPE',1,2,364,''),(1148,'BEFORE_STATE',1,2,364,''),(1149,'BEFORE_STATE_SET',1,2,364,''),(1150,'BEGIN_OF_CYCLE',1,2,364,''),(1151,'BENEFIT_RULE',1,2,364,''),(1152,'BENEFIT_TRIGGER_FLAG',1,2,364,''),(1153,'BE_ID',1,2,364,''),(1154,'BILLINGCYCLESPLITFLAG',1,2,364,''),(1155,'BILLING_CYCLE_BEGIN_TIME',1,2,364,''),(1156,'BILLING_CYCLE_END_TIME',1,2,364,''),(1157,'BILLING_CYCLE_ID',1,2,364,''),(1158,'BILLING_IMSI',1,2,364,''),(1159,'BILLING_NBR',1,2,364,''),(1160,'BILLING_NBR1',1,2,364,''),(1161,'BILLING_NUMBER',1,2,364,''),(1162,'BILL_CYCLE',1,2,364,''),(1163,'BLOCK_REASON',1,2,364,''),(1164,'Brand',1,2,364,''),(1165,'BRAND_NAME',1,2,364,''),(1166,'BYTES',1,2,364,''),(1167,'CALLED_CUG_NO',1,2,364,''),(1168,'CALLED_HOME_AREA_NUMBER',1,2,364,''),(1169,'CALLED_HOME_COUNTRY_CODE',1,2,364,''),(1170,'CALLED_HOME_NETWORK_CODE',1,2,364,''),(1171,'CALLED_IMEI',1,2,364,''),(1172,'CALLED_IMSI',1,2,364,''),(1173,'CALLED_NBR',1,2,364,''),(1174,'CALLED_NETWORK_TYPE',1,2,364,''),(1175,'CALLED_NUMBER',1,2,364,''),(1176,'CALLED_ROAM_AREA_NUMBER',1,2,364,''),(1177,'CALLED_ROAM_COUNTRY_CODE',1,2,364,''),(1178,'CALLED_ROAM_NETWORK_CODE',1,2,364,''),(1179,'CALLED_VLR_NBR',1,2,364,''),(1180,'CALLED_VPN_GROUP_NUMBER',1,2,364,''),(1181,'CALLED_VPN_SHORT_NUMBER',1,2,364,''),(1182,'CALLED_VPN_TOP_GROUP_NUMBER',1,2,364,''),(1183,'CALLING_CUG_NO',1,2,364,''),(1184,'CALLING_HOME_AREA_NUMBER',1,2,364,''),(1185,'CALLING_HOME_NETWORK_CODE',1,2,364,''),(1186,'CALLING_IMEI',1,2,364,''),(1187,'CALLING_IMSI',1,2,364,''),(1188,'CALLING_NBR',1,2,364,''),(1189,'CALLING_NETWORK_TYPE',1,2,364,''),(1190,'CALLING_NUMBER',1,2,364,''),(1191,'CALLING_ROAM_AREA_NUMBER',1,2,364,''),(1192,'CALLING_ROAM_COUNTRY_CODE',1,2,364,''),(1193,'CALLING_ROAM_NETWORK_CODE',1,2,364,''),(1194,'CALLING_STATION_ID',1,2,364,''),(1195,'CALLING_VLR_NBR',1,2,364,''),(1196,'CALLING_VPN_GROUP_NUMBER',1,2,364,''),(1197,'CALLING_VPN_SHORT_NUMBER',1,2,364,''),(1198,'CALLING_VPN_TOP_GROUP_NUMBER',1,2,364,''),(1199,'CALL_FORWARD_INDICATOR',1,2,364,''),(1200,'CALL_ID',1,2,364,''),(1201,'CALL_REF_NBR',1,2,364,''),(1202,'CALL_TYPE',1,2,364,''),(1203,'CALL_TYPE_STRING',1,2,364,''),(1204,'CARD_STATUS',1,2,364,''),(1205,'CATEGORY_ID',1,2,364,''),(1206,'CCR_ACCEPT_FLAG',1,2,364,''),(1207,'CCR_EVENT_TYPE',1,2,364,''),(1208,'CCR_RELEASE_CAUSE',1,2,364,''),(1209,'CCR_REQUEST_ACTION',1,2,364,''),(1210,'CCR_TERMINATION_CAUSE',1,2,364,''),(1211,'CCR_TYPE',1,2,364,''),(1212,'CC_REQUEST_NUMBER',1,2,364,''),(1213,'CC_REQUEST_TYPE',1,2,364,''),(1214,'CDR_BATCH_ID',1,2,364,''),(1215,'CDR_ERROR_NO',1,2,364,''),(1216,'CDR_ID',1,2,364,''),(1217,'CDR_SUB_ID',1,2,364,''),(1218,'CDR_TYPE',1,2,364,''),(1219,'CELL_A',1,2,364,''),(1220,'CELL_B',1,2,364,''),(1221,'CELL_ID',1,2,364,''),(1222,'CENTREX_GROUP_NUMBER',1,2,364,''),(1223,'CHANGE_LANG_DATE',1,2,364,''),(1224,'CHANGE_PASS_DATE',1,2,364,''),(1225,'CHANGE_STATE_DATE',1,2,364,''),(1226,'CHANNEL',1,2,364,''),(1227,'CHARGE',1,2,364,''),(1228,'CHARGE1',1,2,364,''),(1229,'CHARGE10',1,2,364,''),(1230,'CHARGE100',1,2,364,''),(1231,'CHARGE1001',1,2,364,''),(1232,'CHARGE101',1,2,364,''),(1233,'CHARGE102',1,2,364,''),(1234,'CHARGE103',1,2,364,''),(1235,'CHARGE11',1,2,364,''),(1236,'CHARGE12',1,2,364,''),(1237,'CHARGE13',1,2,364,''),(1238,'CHARGE14',1,2,364,''),(1239,'CHARGE15',1,2,364,''),(1240,'CHARGE16',1,2,364,''),(1241,'CHARGE17',1,2,364,''),(1242,'CHARGE18',1,2,364,''),(1243,'CHARGE1819665236',1,2,364,''),(1244,'CHARGE19',1,2,364,''),(1245,'CHARGE2',1,2,364,''),(1246,'CHARGE20',1,2,364,''),(1247,'CHARGE2001',1,2,364,''),(1248,'CHARGE21',1,2,364,''),(1249,'CHARGE212',1,2,364,''),(1250,'CHARGE22',1,2,364,''),(1251,'CHARGE23',1,2,364,''),(1252,'CHARGE24',1,2,364,''),(1253,'CHARGE25',1,2,364,''),(1254,'CHARGE26',1,2,364,''),(1255,'CHARGE27',1,2,364,''),(1256,'CHARGE28',1,2,364,''),(1257,'CHARGE29',1,2,364,''),(1258,'CHARGE3',1,2,364,''),(1259,'CHARGE30',1,2,364,''),(1260,'CHARGE31',1,2,364,''),(1261,'CHARGE32',1,2,364,''),(1262,'CHARGE33',1,2,364,''),(1263,'CHARGE34',1,2,364,''),(1264,'CHARGE35',1,2,364,''),(1265,'CHARGE36',1,2,364,''),(1266,'CHARGE37',1,2,364,''),(1267,'CHARGE38',1,2,364,''),(1268,'CHARGE39',1,2,364,''),(1269,'CHARGE4',1,2,364,''),(1270,'CHARGE40',1,2,364,''),(1271,'CHARGE41',1,2,364,''),(1272,'CHARGE42',1,2,364,''),(1273,'CHARGE43',1,2,364,''),(1274,'CHARGE44',1,2,364,''),(1275,'CHARGE45',1,2,364,''),(1276,'CHARGE46',1,2,364,''),(1277,'CHARGE47',1,2,364,''),(1278,'CHARGE48',1,2,364,''),(1279,'CHARGE49',1,2,364,''),(1280,'CHARGE5',1,2,364,''),(1281,'CHARGE50',1,2,364,''),(1282,'CHARGE500',1,2,364,''),(1283,'CHARGE51',1,2,364,''),(1284,'CHARGE6',1,2,364,''),(1285,'CHARGE7',1,2,364,''),(1286,'CHARGE8',1,2,364,''),(1287,'CHARGE9',1,2,364,''),(1288,'CHARGE900',1,2,364,''),(1289,'CHARGE_1',1,2,364,''),(1290,'CHARGE_10',1,2,364,''),(1291,'CHARGE_100',1,2,364,''),(1292,'CHARGE_1001',1,2,364,''),(1293,'CHARGE_101',1,2,364,''),(1294,'CHARGE_102',1,2,364,''),(1295,'CHARGE_103',1,2,364,''),(1296,'CHARGE_11',1,2,364,''),(1297,'CHARGE_12',1,2,364,''),(1298,'CHARGE_13',1,2,364,''),(1299,'CHARGE_14',1,2,364,''),(1300,'CHARGE_15',1,2,364,''),(1301,'CHARGE_16',1,2,364,''),(1302,'CHARGE_17',1,2,364,''),(1303,'CHARGE_18',1,2,364,''),(1304,'CHARGE_1819665236',1,2,364,''),(1305,'CHARGE_19',1,2,364,''),(1306,'CHARGE_2',1,2,364,''),(1307,'CHARGE_20',1,2,364,''),(1308,'CHARGE_2001',1,2,364,''),(1309,'CHARGE_21',1,2,364,''),(1310,'CHARGE_212',1,2,364,''),(1311,'CHARGE_22',1,2,364,''),(1312,'CHARGE_23',1,2,364,''),(1313,'CHARGE_24',1,2,364,''),(1314,'CHARGE_25',1,2,364,''),(1315,'CHARGE_26',1,2,364,''),(1316,'CHARGE_27',1,2,364,''),(1317,'CHARGE_28',1,2,364,''),(1318,'CHARGE_29',1,2,364,''),(1319,'CHARGE_3',1,2,364,''),(1320,'CHARGE_30',1,2,364,''),(1321,'CHARGE_31',1,2,364,''),(1322,'CHARGE_32',1,2,364,''),(1323,'CHARGE_33',1,2,364,''),(1324,'CHARGE_34',1,2,364,''),(1325,'CHARGE_35',1,2,364,''),(1326,'CHARGE_36',1,2,364,''),(1327,'CHARGE_37',1,2,364,''),(1328,'CHARGE_38',1,2,364,''),(1329,'CHARGE_39',1,2,364,''),(1330,'CHARGE_4',1,2,364,''),(1331,'CHARGE_40',1,2,364,''),(1332,'CHARGE_41',1,2,364,''),(1333,'CHARGE_42',1,2,364,''),(1334,'CHARGE_43',1,2,364,''),(1335,'CHARGE_44',1,2,364,''),(1336,'CHARGE_45',1,2,364,''),(1337,'CHARGE_46',1,2,364,''),(1338,'CHARGE_47',1,2,364,''),(1339,'CHARGE_48',1,2,364,''),(1340,'CHARGE_49',1,2,364,''),(1341,'CHARGE_5',1,2,364,''),(1342,'CHARGE_50',1,2,364,''),(1343,'CHARGE_500',1,2,364,''),(1344,'CHARGE_51',1,2,364,''),(1345,'CHARGE_6',1,2,364,''),(1346,'CHARGE_7',1,2,364,''),(1347,'CHARGE_8',1,2,364,''),(1348,'CHARGE_9',1,2,364,''),(1349,'CHARGE_900',1,2,364,''),(1350,'CHARGE_BALANCE_INFO',1,2,364,''),(1351,'CHARGE_CODE_INFO_FOR_GL',1,2,364,''),(1352,'CHARGE_DOWNLOAD',1,2,364,''),(1353,'CHARGE_FREEUNIT_INFO',1,2,364,''),(1354,'CHARGE_ID',1,2,364,''),(1355,'CHARGE_LIST',1,2,364,''),(1356,'CHARGE_PARTY',1,2,364,''),(1357,'CHARGE_PARTY_INDICATOR',1,2,364,''),(1358,'CHARGE_UPLOAD',1,2,364,''),(1359,'CHARGING_CALL_ID',1,2,364,''),(1360,'CHARGING_ID',1,2,364,''),(1361,'CHARGING_ITEM_ID',1,2,364,''),(1362,'CHARGING_TYPE',1,2,364,''),(1363,'COMMAND_ID',1,2,364,''),(1364,'COMMAND_NAME',1,2,364,''),(1365,'COMPLETED_DATE',1,2,364,''),(1366,'CONTENT',1,2,364,''),(1367,'CONTENT_ID',1,2,364,''),(1368,'CONTRACT_ID',1,2,364,''),(1369,'Country Name',1,2,364,''),(1370,'COUNTRY_CODE',1,2,364,''),(1371,'CP_ID',1,2,364,''),(1372,'CREATE_DATE',1,2,364,''),(1373,'CURRENCY',1,2,364,''),(1374,'CURRENCY_ID_1',1,2,364,''),(1375,'CURRENCY_ID_10',1,2,364,''),(1376,'CURRENCY_ID_15',1,2,364,''),(1377,'CURRENCY_ID_17',1,2,364,''),(1378,'CURRENCY_ID_21',1,2,364,''),(1379,'CURRENCY_ID_22',1,2,364,''),(1380,'CURRENCY_ID_24',1,2,364,''),(1381,'CURRENCY_ID_25',1,2,364,''),(1382,'CURRENCY_ID_26',1,2,364,''),(1383,'CURRENCY_ID_27',1,2,364,''),(1384,'CURRENCY_ID_34',1,2,364,''),(1385,'CURRENCY_ID_45',1,2,364,''),(1386,'CURRENCY_ID_46',1,2,364,''),(1387,'CURRENCY_ID_49',1,2,364,''),(1388,'CURRENCY_ID_50',1,2,364,''),(1389,'CUST_ID',1,2,364,''),(1390,'CYCLE_BEGIN_DATE',1,2,364,''),(1391,'CYCLE_BEGIN_TIME',1,2,364,''),(1392,'CYCLE_DAYS',1,2,364,''),(1393,'CYCLE_END_DATE',1,2,364,''),(1394,'CYCLE_END_TIME',1,2,364,''),(1395,'DATA_ZONE_ID',1,2,364,''),(1396,'DEAL_TIME',1,2,364,''),(1397,'DEBIT_AMOUNT',1,2,364,''),(1398,'DEBIT_FROM_ADVANCE_POSTPAID',1,2,364,''),(1399,'DEBIT_FROM_ADVANCE_PREPAID',1,2,364,''),(1400,'DEBIT_FROM_CREDIT_POSTPAID',1,2,364,''),(1401,'DEBIT_FROM_POSTPAID',1,2,364,''),(1402,'DEBIT_FROM_PREPAID',1,2,364,''),(1403,'DEFAULT_ACCT_FLAG1',1,2,364,''),(1404,'DEFAULT_ACCT_FLAG2',1,2,364,''),(1405,'DEFAULT_ACCT_FLAG3',1,2,364,''),(1406,'DELIVER_COUNT',1,2,364,''),(1407,'DESCRIPTION',1,2,364,''),(1408,'DISCOUNT',1,2,364,''),(1409,'DISCOUNT_CHARGE',1,2,364,''),(1410,'DIS_DOM_CHARGE',1,2,364,''),(1411,'DIS_INT_CHARGE',1,2,364,''),(1412,'DIS_SER_CHARGE',1,2,364,''),(1413,'DOM_CHARGE',1,2,364,''),(1414,'DOWN_DATA',1,2,364,''),(1415,'DT_DISCOUNT',1,2,364,''),(1416,'DURATION',1,2,364,''),(1417,'EFF_DATE',1,2,364,''),(1418,'END_DATE',1,2,364,''),(1419,'END_DATETIME',1,2,364,''),(1420,'EVENT_BEGIN_TIME',1,2,364,''),(1421,'EVENT_END_TIME',1,2,364,''),(1422,'EVENT_ID',1,2,364,''),(1423,'EVENT_INST_ID',1,2,364,''),(1424,'EVENT_SEQ',1,2,364,''),(1425,'EVENT_TIME',1,2,364,''),(1426,'EVENT_TYPE',1,2,364,''),(1427,'EVT_SOURCE_CATEGORY',1,2,364,''),(1428,'EXCHANGE_RATE',1,2,364,''),(1429,'EXPDATE',1,2,364,''),(1430,'ExpDate',1,2,364,''),(1431,'EXPIRE_DATE_1',1,2,364,''),(1432,'EXPIRE_DATE_10',1,2,364,''),(1433,'EXPIRE_DATE_100',1,2,364,''),(1434,'EXPIRE_DATE_1001',1,2,364,''),(1435,'EXPIRE_DATE_101',1,2,364,''),(1436,'EXPIRE_DATE_102',1,2,364,''),(1437,'EXPIRE_DATE_103',1,2,364,''),(1438,'EXPIRE_DATE_11',1,2,364,''),(1439,'EXPIRE_DATE_12',1,2,364,''),(1440,'EXPIRE_DATE_13',1,2,364,''),(1441,'EXPIRE_DATE_14',1,2,364,''),(1442,'EXPIRE_DATE_15',1,2,364,''),(1443,'EXPIRE_DATE_16',1,2,364,''),(1444,'EXPIRE_DATE_17',1,2,364,''),(1445,'EXPIRE_DATE_18',1,2,364,''),(1446,'EXPIRE_DATE_1819665236',1,2,364,''),(1447,'EXPIRE_DATE_19',1,2,364,''),(1448,'EXPIRE_DATE_2',1,2,364,''),(1449,'EXPIRE_DATE_20',1,2,364,''),(1450,'EXPIRE_DATE_2001',1,2,364,''),(1451,'EXPIRE_DATE_21',1,2,364,''),(1452,'EXPIRE_DATE_212',1,2,364,''),(1453,'EXPIRE_DATE_22',1,2,364,''),(1454,'EXPIRE_DATE_23',1,2,364,''),(1455,'EXPIRE_DATE_24',1,2,364,''),(1456,'EXPIRE_DATE_25',1,2,364,''),(1457,'EXPIRE_DATE_26',1,2,364,''),(1458,'EXPIRE_DATE_27',1,2,364,''),(1459,'EXPIRE_DATE_28',1,2,364,''),(1460,'EXPIRE_DATE_29',1,2,364,''),(1461,'EXPIRE_DATE_3',1,2,364,''),(1462,'EXPIRE_DATE_30',1,2,364,''),(1463,'EXPIRE_DATE_31',1,2,364,''),(1464,'EXPIRE_DATE_32',1,2,364,''),(1465,'EXPIRE_DATE_33',1,2,364,''),(1466,'EXPIRE_DATE_34',1,2,364,''),(1467,'EXPIRE_DATE_35',1,2,364,''),(1468,'EXPIRE_DATE_36',1,2,364,''),(1469,'EXPIRE_DATE_37',1,2,364,''),(1470,'EXPIRE_DATE_38',1,2,364,''),(1471,'EXPIRE_DATE_39',1,2,364,''),(1472,'EXPIRE_DATE_4',1,2,364,''),(1473,'EXPIRE_DATE_40',1,2,364,''),(1474,'EXPIRE_DATE_41',1,2,364,''),(1475,'EXPIRE_DATE_42',1,2,364,''),(1476,'EXPIRE_DATE_43',1,2,364,''),(1477,'EXPIRE_DATE_44',1,2,364,''),(1478,'EXPIRE_DATE_45',1,2,364,''),(1479,'EXPIRE_DATE_46',1,2,364,''),(1480,'EXPIRE_DATE_47',1,2,364,''),(1481,'EXPIRE_DATE_48',1,2,364,''),(1482,'EXPIRE_DATE_49',1,2,364,''),(1483,'EXPIRE_DATE_5',1,2,364,''),(1484,'EXPIRE_DATE_50',1,2,364,''),(1485,'EXPIRE_DATE_500',1,2,364,''),(1486,'EXPIRE_DATE_51',1,2,364,''),(1487,'EXPIRE_DATE_55',1,2,364,''),(1488,'EXPIRE_DATE_6',1,2,364,''),(1489,'EXPIRE_DATE_61',1,2,364,''),(1490,'EXPIRE_DATE_62',1,2,364,''),(1491,'EXPIRE_DATE_63',1,2,364,''),(1492,'EXPIRE_DATE_7',1,2,364,''),(1493,'EXPIRE_DATE_8',1,2,364,''),(1494,'EXPIRE_DATE_9',1,2,364,''),(1495,'EXPIRE_DATE_900',1,2,364,''),(1496,'EXPORT_TIME',1,2,364,''),(1497,'EXPROP1',1,2,364,''),(1498,'EXP_ACCT_CONTRACT',1,2,364,''),(1499,'EXP_DATE',1,2,364,''),(1500,'EXP_SUBS_CODE',1,2,364,''),(1501,'EXTEND_DAYS',1,2,364,''),(1502,'EXT_PROP2',1,2,364,''),(1503,'EX_PROPERTY1',1,2,364,''),(1504,'EX_PROPERTY2',1,2,364,''),(1505,'EX_PROPERTY3',1,2,364,''),(1506,'EX_PROPERTY4',1,2,364,''),(1507,'EX_PROPERTY5',1,2,364,''),(1508,'FILE_ID',1,2,364,''),(1509,'FORWARD_TYPE',1,2,364,''),(1510,'FORWORD_TYPE',1,2,364,''),(1511,'FRAMED_ID_ADDRESS',1,2,364,''),(1512,'FRAME_IP',1,2,364,''),(1513,'FREE_UNIT_AMOUNT_OF_DURATION',1,2,364,''),(1514,'FREE_UNIT_AMOUNT_OF_FLUX',1,2,364,''),(1515,'FREE_UNIT_AMOUNT_OF_TIMES',1,2,364,''),(1516,'FU_OWN_TYPE',1,2,364,''),(1517,'GGSN_ADDRESS',1,2,364,''),(1518,'GMSC_ADDRESS',1,2,364,''),(1519,'GROUP_CALL_TYPE',1,2,364,''),(1520,'GROUP_CURRENCY_BAL',1,2,364,''),(1521,'GROUP_ID',1,2,364,''),(1522,'GROUP_NONCURRENCY_BAL',1,2,364,''),(1523,'GROUP_PAY_FLAG',1,2,364,''),(1524,'HOME_ZONE_ID',1,2,364,''),(1525,'HOT_LINE_INDICATOR',1,2,364,''),(1526,'HOT_SEQ',1,2,364,''),(1527,'IB_SERVICE_TYPE',1,2,364,''),(1528,'ICCID',1,2,364,''),(1529,'IDENTIFY_TYPE',1,2,364,''),(1530,'IMEI',1,2,364,''),(1531,'IMSI',1,2,364,''),(1532,'IMS_CHARGING_IDENTIFIER',1,2,364,''),(1533,'INCOMING_ROAM_FLAG',1,2,364,''),(1534,'INDIVIDUAL_PRICE_PLAN_ID_LIST',1,2,364,''),(1535,'INFO',1,2,364,''),(1536,'INT_CHARGE',1,2,364,''),(1537,'IPADDRESS',1,2,364,''),(1538,'IPADRESS',1,2,364,''),(1539,'IR_CHARGE',1,2,364,''),(1540,'IR_CHARGE_LOCAL',1,2,364,''),(1541,'IR_CHARGE_VND',1,2,364,''),(1542,'ISDN',1,2,364,''),(1543,'ITEM_CODE',1,2,364,''),(1544,'ITEM_ID',1,2,364,''),(1545,'LAC_A',1,2,364,''),(1546,'LAC_B',1,2,364,''),(1547,'LANG_ID',1,2,364,''),(1548,'LANG_ID',1,2,364,''),(1549,'LANG_ID_AFTER',1,2,364,''),(1550,'LANG_ID_BEFORE',1,2,364,''),(1551,'LC_DATE_TIME',1,2,364,''),(1552,'LOAN_MONEY',1,2,364,''),(1553,'LOAN_TIME',1,2,364,''),(1554,'LOAN_TYPE',1,2,364,''),(1555,'LOST_AMOUNT',1,2,364,''),(1556,'MAC_ADDRESS',1,2,364,''),(1557,'MAIN_BALANCE_INFO',1,2,364,''),(1558,'MAIN_OFFER',1,2,364,''),(1559,'MAIN_PROD_SPEC_ID',1,2,364,''),(1560,'MARKUP_CHARGE',1,2,364,''),(1561,'MASTER_NBR',1,2,364,''),(1562,'MESSAGE',1,2,364,''),(1563,'MONEY_REMAIN',1,2,364,''),(1564,'MONITOR_INPUT_VOLUME_201',1,2,364,''),(1565,'MONITOR_INPUT_VOLUME_202',1,2,364,''),(1566,'MONITOR_INPUT_VOLUME_203',1,2,364,''),(1567,'MONITOR_INPUT_VOLUME_204',1,2,364,''),(1568,'MONITOR_INPUT_VOLUME_205',1,2,364,''),(1569,'MONITOR_INPUT_VOLUME_206',1,2,364,''),(1570,'MONITOR_INPUT_VOLUME_207',1,2,364,''),(1571,'MONITOR_INPUT_VOLUME_208',1,2,364,''),(1572,'MONITOR_KEY_201',1,2,364,''),(1573,'MONITOR_KEY_202',1,2,364,''),(1574,'MONITOR_KEY_203',1,2,364,''),(1575,'MONITOR_KEY_204',1,2,364,''),(1576,'MONITOR_KEY_205',1,2,364,''),(1577,'MONITOR_KEY_206',1,2,364,''),(1578,'MONITOR_KEY_207',1,2,364,''),(1579,'MONITOR_KEY_208',1,2,364,''),(1580,'MONITOR_OUTPUT_VOLUME_201',1,2,364,''),(1581,'MONITOR_OUTPUT_VOLUME_202',1,2,364,''),(1582,'MONITOR_OUTPUT_VOLUME_203',1,2,364,''),(1583,'MONITOR_OUTPUT_VOLUME_204',1,2,364,''),(1584,'MONITOR_OUTPUT_VOLUME_205',1,2,364,''),(1585,'MONITOR_OUTPUT_VOLUME_206',1,2,364,''),(1586,'MONITOR_OUTPUT_VOLUME_207',1,2,364,''),(1587,'MONITOR_OUTPUT_VOLUME_208',1,2,364,''),(1588,'MONITOR_RULE_NAME_201',1,2,364,''),(1589,'MONITOR_RULE_NAME_202',1,2,364,''),(1590,'MONITOR_RULE_NAME_203',1,2,364,''),(1591,'MONITOR_RULE_NAME_204',1,2,364,''),(1592,'MONITOR_RULE_NAME_205',1,2,364,''),(1593,'MONITOR_RULE_NAME_206',1,2,364,''),(1594,'MONITOR_RULE_NAME_207',1,2,364,''),(1595,'MONITOR_RULE_NAME_208',1,2,364,''),(1596,'MONITOR_VOLUME_201',1,2,364,''),(1597,'MONITOR_VOLUME_202',1,2,364,''),(1598,'MONITOR_VOLUME_203',1,2,364,''),(1599,'MONITOR_VOLUME_204',1,2,364,''),(1600,'MONITOR_VOLUME_205',1,2,364,''),(1601,'MONITOR_VOLUME_206',1,2,364,''),(1602,'MONITOR_VOLUME_207',1,2,364,''),(1603,'MONITOR_VOLUME_208',1,2,364,''),(1604,'MSC_ADDRESS',1,2,364,''),(1605,'MSISDN',1,2,364,''),(1606,'Msisdn',1,2,364,''),(1607,'MSISDN_',1,2,364,''),(1608,'MSRN',1,2,364,''),(1609,'NAS_IP_ADDRESS',1,2,364,''),(1610,'NETWORK_TYPE',1,2,364,''),(1611,'NewBalanceCalled',1,2,364,''),(1612,'NewBalanceCalling',1,2,364,''),(1613,'NewBlockReason',1,2,364,''),(1614,'NEWOFFEREXTERNALID',1,2,364,''),(1615,'NEWSTATE',1,2,364,''),(1616,'NEW_CONTRACT_ID',1,2,364,''),(1617,'NEW_CUST_ID',1,2,364,''),(1618,'NEW_ICCID',1,2,364,''),(1619,'NEW_IMSI',1,2,364,''),(1620,'NEW_MSISDN',1,2,364,''),(1621,'NEW_PASS',1,2,364,''),(1622,'NEW_PRICE_PLAN_ID',1,2,364,''),(1623,'NEW_SUB_ID',1,2,364,''),(1624,'NOFITY_CONTENT',1,2,364,''),(1625,'NOTIFYTIME',1,2,364,''),(1626,'NOTIFY_TIME',1,2,364,''),(1627,'NOW_STATE_SET',1,2,364,''),(1628,'NP_FLAG',1,2,364,''),(1629,'NP_PREFIX',1,2,364,''),(1630,'OBJ_ID',1,2,364,''),(1631,'OBJ_TYPE',1,2,364,''),(1632,'OCS_ID',1,2,364,''),(1633,'OCS_ONLINE_FLAG',1,2,364,''),(1634,'OCS_RESULT_CODE',1,2,364,''),(1635,'OFFER_EXTERNAL_ID',1,2,364,''),(1636,'OFFER_ID',1,2,364,''),(1637,'OFFER_NAME',1,2,364,''),(1638,'OldBlockReason',1,2,364,''),(1639,'OLDOFFEREXTERNALID',1,2,364,''),(1640,'OLDSTATE',1,2,364,''),(1641,'OLD_PASS',1,2,364,''),(1642,'ONLINE_CHARGING_FLAG',1,2,364,''),(1643,'ONNET_INDICATOR',1,2,364,''),(1644,'OPERATORFEE',1,2,364,''),(1645,'OPERATORRESULT',1,2,364,''),(1646,'OperFee',1,2,364,''),(1647,'OperResult',1,2,364,''),(1648,'OperResult1',1,2,364,''),(1649,'OPER_TYPE_1',1,2,364,''),(1650,'OPER_TYPE_10',1,2,364,''),(1651,'OPER_TYPE_100',1,2,364,''),(1652,'OPER_TYPE_1001',1,2,364,''),(1653,'OPER_TYPE_101',1,2,364,''),(1654,'OPER_TYPE_102',1,2,364,''),(1655,'OPER_TYPE_103',1,2,364,''),(1656,'OPER_TYPE_11',1,2,364,''),(1657,'OPER_TYPE_12',1,2,364,''),(1658,'OPER_TYPE_13',1,2,364,''),(1659,'OPER_TYPE_14',1,2,364,''),(1660,'OPER_TYPE_15',1,2,364,''),(1661,'OPER_TYPE_16',1,2,364,''),(1662,'OPER_TYPE_17',1,2,364,''),(1663,'OPER_TYPE_18',1,2,364,''),(1664,'OPER_TYPE_1819665236',1,2,364,''),(1665,'OPER_TYPE_19',1,2,364,''),(1666,'OPER_TYPE_20',1,2,364,''),(1667,'OPER_TYPE_2001',1,2,364,''),(1668,'OPER_TYPE_21',1,2,364,''),(1669,'OPER_TYPE_212',1,2,364,''),(1670,'OPER_TYPE_22',1,2,364,''),(1671,'OPER_TYPE_23',1,2,364,''),(1672,'OPER_TYPE_24',1,2,364,''),(1673,'OPER_TYPE_25',1,2,364,''),(1674,'OPER_TYPE_26',1,2,364,''),(1675,'OPER_TYPE_27',1,2,364,''),(1676,'OPER_TYPE_28',1,2,364,''),(1677,'OPER_TYPE_29',1,2,364,''),(1678,'OPER_TYPE_3',1,2,364,''),(1679,'OPER_TYPE_30',1,2,364,''),(1680,'OPER_TYPE_31',1,2,364,''),(1681,'OPER_TYPE_32',1,2,364,''),(1682,'OPER_TYPE_33',1,2,364,''),(1683,'OPER_TYPE_34',1,2,364,''),(1684,'OPER_TYPE_35',1,2,364,''),(1685,'OPER_TYPE_36',1,2,364,''),(1686,'OPER_TYPE_37',1,2,364,''),(1687,'OPER_TYPE_38',1,2,364,''),(1688,'OPER_TYPE_39',1,2,364,''),(1689,'OPER_TYPE_40',1,2,364,''),(1690,'OPER_TYPE_41',1,2,364,''),(1691,'OPER_TYPE_42',1,2,364,''),(1692,'OPER_TYPE_43',1,2,364,''),(1693,'OPER_TYPE_44',1,2,364,''),(1694,'OPER_TYPE_45',1,2,364,''),(1695,'OPER_TYPE_46',1,2,364,''),(1696,'OPER_TYPE_47',1,2,364,''),(1697,'OPER_TYPE_48',1,2,364,''),(1698,'OPER_TYPE_49',1,2,364,''),(1699,'OPER_TYPE_50',1,2,364,''),(1700,'OPER_TYPE_500',1,2,364,''),(1701,'OPER_TYPE_51',1,2,364,''),(1702,'OPER_TYPE_6',1,2,364,''),(1703,'OPER_TYPE_8',1,2,364,''),(1704,'OPER_TYPE_900',1,2,364,''),(1705,'OPPOSE_MAIN_OFFERING_ID',1,2,364,''),(1706,'OPPOSE_NETWORK_TYPE',1,2,364,''),(1707,'OPPOSE_NUMBER_TYPE',1,2,364,''),(1708,'OPPOSITE_CELL',1,2,364,''),(1709,'ORG_STA_DATETIME',1,2,364,''),(1710,'ORIGINAL_CALLED_PARTY',1,2,364,''),(1711,'ORIGINATINGIOI',1,2,364,''),(1712,'ORIGIN_HOST',1,2,364,''),(1713,'ORIGIN_REALM',1,2,364,''),(1714,'ORI_CHARGE1',1,2,364,''),(1715,'OtherMSISDN',1,2,364,''),(1716,'OWE_ACCT_RES_ID_LIST',1,2,364,''),(1717,'PACKAGE_TYPE',1,2,364,''),(1718,'PAGE_COUNT',1,2,364,''),(1719,'PAID',1,2,364,''),(1720,'PAID_CONTRACT_ID',1,2,364,''),(1721,'PAID_NUMBER',1,2,364,''),(1722,'PAID_SUB_ID',1,2,364,''),(1723,'PARTNER_ID',1,2,364,''),(1724,'PartyCode',1,2,364,''),(1725,'PARTY_CODE',1,2,364,''),(1726,'PAYMENT_COUNT',1,2,364,''),(1727,'PAY_TIME',1,2,364,''),(1728,'PAY_TYPE',1,2,364,''),(1729,'PCRF_BAL_1',1,2,364,''),(1730,'PCRF_BAL_2',1,2,364,''),(1731,'PCRF_BAL_3',1,2,364,''),(1732,'PCRF_CHARGE_1',1,2,364,''),(1733,'PCRF_CHARGE_2',1,2,364,''),(1734,'PCRF_CHARGE_3',1,2,364,''),(1735,'PCRF_PRE_BAL_1',1,2,364,''),(1736,'PCRF_PRE_BAL_2',1,2,364,''),(1737,'PCRF_PRE_BAL_3',1,2,364,''),(1738,'PEER_NBR',1,2,364,''),(1739,'PID_INFO',1,2,364,''),(1740,'PIN',1,2,364,''),(1741,'PORT',1,2,364,''),(1742,'PO_CODE',1,2,364,''),(1743,'PPS_PWD',1,2,364,''),(1744,'PREBAL',1,2,364,''),(1745,'PREPAY_FLAG',1,2,364,''),(1746,'PRESENT_INFO',1,2,364,''),(1747,'PRE_BALANCE',1,2,364,''),(1748,'PRE_BALANCE1',1,2,364,''),(1749,'PRE_BALANCE2',1,2,364,''),(1750,'PRE_BALANCE3',1,2,364,''),(1751,'PRE_BALANCE4',1,2,364,''),(1752,'PRE_EXPDATE',1,2,364,''),(1753,'PRICE_ID',1,2,364,''),(1754,'PRICE_ID1',1,2,364,''),(1755,'PRICE_ID10',1,2,364,''),(1756,'PRICE_ID100',1,2,364,''),(1757,'PRICE_ID1001',1,2,364,''),(1758,'PRICE_ID101',1,2,364,''),(1759,'PRICE_ID102',1,2,364,''),(1760,'PRICE_ID103',1,2,364,''),(1761,'PRICE_ID11',1,2,364,''),(1762,'PRICE_ID12',1,2,364,''),(1763,'PRICE_ID13',1,2,364,''),(1764,'PRICE_ID14',1,2,364,''),(1765,'PRICE_ID15',1,2,364,''),(1766,'PRICE_ID16',1,2,364,''),(1767,'PRICE_ID17',1,2,364,''),(1768,'PRICE_ID18',1,2,364,''),(1769,'PRICE_ID1819665236',1,2,364,''),(1770,'PRICE_ID19',1,2,364,''),(1771,'PRICE_ID2',1,2,364,''),(1772,'PRICE_ID20',1,2,364,''),(1773,'PRICE_ID2001',1,2,364,''),(1774,'PRICE_ID21',1,2,364,''),(1775,'PRICE_ID212',1,2,364,''),(1776,'PRICE_ID22',1,2,364,''),(1777,'PRICE_ID23',1,2,364,''),(1778,'PRICE_ID24',1,2,364,''),(1779,'PRICE_ID25',1,2,364,''),(1780,'PRICE_ID26',1,2,364,''),(1781,'PRICE_ID27',1,2,364,''),(1782,'PRICE_ID28',1,2,364,''),(1783,'PRICE_ID29',1,2,364,''),(1784,'PRICE_ID3',1,2,364,''),(1785,'PRICE_ID30',1,2,364,''),(1786,'PRICE_ID31',1,2,364,''),(1787,'PRICE_ID32',1,2,364,''),(1788,'PRICE_ID33',1,2,364,''),(1789,'PRICE_ID34',1,2,364,''),(1790,'PRICE_ID35',1,2,364,''),(1791,'PRICE_ID36',1,2,364,''),(1792,'PRICE_ID37',1,2,364,''),(1793,'PRICE_ID38',1,2,364,''),(1794,'PRICE_ID39',1,2,364,''),(1795,'PRICE_ID4',1,2,364,''),(1796,'PRICE_ID40',1,2,364,''),(1797,'PRICE_ID41',1,2,364,''),(1798,'PRICE_ID42',1,2,364,''),(1799,'PRICE_ID43',1,2,364,''),(1800,'PRICE_ID44',1,2,364,''),(1801,'PRICE_ID45',1,2,364,''),(1802,'PRICE_ID46',1,2,364,''),(1803,'PRICE_ID47',1,2,364,''),(1804,'PRICE_ID48',1,2,364,''),(1805,'PRICE_ID49',1,2,364,''),(1806,'PRICE_ID5',1,2,364,''),(1807,'PRICE_ID50',1,2,364,''),(1808,'PRICE_ID500',1,2,364,''),(1809,'PRICE_ID51',1,2,364,''),(1810,'PRICE_ID6',1,2,364,''),(1811,'PRICE_ID7',1,2,364,''),(1812,'PRICE_ID8',1,2,364,''),(1813,'PRICE_ID9',1,2,364,''),(1814,'PRICE_ID900',1,2,364,''),(1815,'PRICE_ID_LIST',1,2,364,''),(1816,'PRICE_LIST',1,2,364,''),(1817,'PRICE_PLAN_ID',1,2,364,''),(1818,'PRIMARY_OFFER_CHARGE_AMOUNT',1,2,364,''),(1819,'PROC_DATE',1,2,364,''),(1820,'PRODSPECID',1,2,364,''),(1821,'PRODUCT',1,2,364,''),(1822,'PRODUCT_CODE',1,2,364,''),(1823,'PRODUCT_QUANTITY',1,2,364,''),(1824,'PROD_NAME',1,2,364,''),(1825,'PROD_PRICE_PLAN_NAME',1,2,364,''),(1826,'PROD_SPEC',1,2,364,''),(1827,'PROD_SPEC_ID',1,2,364,''),(1828,'PROD_STATE',1,2,364,''),(1829,'PROMOTION_CODE',1,2,364,''),(1830,'PROPERTIES',1,2,364,''),(1831,'PS_CALLED_STATION_ID',1,2,364,''),(1832,'QOS',1,2,364,''),(1833,'QUANTITY',1,2,364,''),(1834,'RANK_UP_OFFSET',1,2,364,''),(1835,'RATE_USAGE',1,2,364,''),(1836,'RATE_USAGE2',1,2,364,''),(1837,'RATING_GROUP',1,2,364,''),(1838,'RATING_GROUP_BYTES',1,2,364,''),(1839,'RATING_GROUP_CDR',1,2,364,''),(1840,'RAT_TYPE',1,2,364,''),(1841,'REASON',1,2,364,''),(1842,'RECEIVABLE_CHARGE1',1,2,364,''),(1843,'RECEIVABLE_CHARGE2',1,2,364,''),(1844,'RECEIVABLE_CHARGE3',1,2,364,''),(1845,'RECEIVABLE_CHARGE4',1,2,364,''),(1846,'RECEIVABLE_CHARGE_LIST',1,2,364,''),(1847,'RECHARGE_AMOUNT',1,2,364,''),(1848,'RECHARGE_BLACKLIST',1,2,364,''),(1849,'RECIPIENT_NUMBER',1,2,364,''),(1850,'RECORD_SEQ',1,2,364,''),(1851,'RECURRING_SEGMENT_CALC_BEGIN_TIME',1,2,364,''),(1852,'RECURRING_SEGMENT_CALC_END_TIME',1,2,364,''),(1853,'RECURRING_TIME',1,2,364,''),(1854,'RECV_CHARGE1',1,2,364,''),(1855,'RECV_CUST_ID',1,2,364,''),(1856,'REC_TYPE',1,2,364,''),(1857,'REDIRECTING_PARTY',1,2,364,''),(1858,'REFUND_INDICATOR',1,2,364,''),(1859,'REGIONAL_ZONE_ID',1,2,364,''),(1860,'REMARK',1,2,364,''),(1861,'REMOTE_FLAG',1,2,364,''),(1862,'REPORTING_VOLUME_TYPE',1,2,364,''),(1863,'REPORT_REASON',1,2,364,''),(1864,'REROUTE_IDD',1,2,364,''),(1865,'RESERVE',1,2,364,''),(1866,'RESERVE1',1,2,364,''),(1867,'RESERVE2',1,2,364,''),(1868,'RESERVE4',1,2,364,''),(1869,'RESERVE5',1,2,364,''),(1870,'RESERVE6',1,2,364,''),(1871,'RESERVE7',1,2,364,''),(1872,'RESOURCE_CHARGE1',1,2,364,''),(1873,'RESOURCE_CHARGE10',1,2,364,''),(1874,'RESOURCE_CHARGE2',1,2,364,''),(1875,'RESOURCE_CHARGE3',1,2,364,''),(1876,'RESOURCE_CHARGE4',1,2,364,''),(1877,'RESOURCE_CHARGE5',1,2,364,''),(1878,'RESOURCE_CHARGE6',1,2,364,''),(1879,'RESOURCE_CHARGE7',1,2,364,''),(1880,'RESOURCE_CHARGE8',1,2,364,''),(1881,'RESOURCE_CHARGE9',1,2,364,''),(1882,'RESOURCE_TYPE1',1,2,364,''),(1883,'RESOURCE_TYPE10',1,2,364,''),(1884,'RESOURCE_TYPE2',1,2,364,''),(1885,'RESOURCE_TYPE3',1,2,364,''),(1886,'RESOURCE_TYPE4',1,2,364,''),(1887,'RESOURCE_TYPE5',1,2,364,''),(1888,'RESOURCE_TYPE6',1,2,364,''),(1889,'RESOURCE_TYPE7',1,2,364,''),(1890,'RESOURCE_TYPE8',1,2,364,''),(1891,'RESOURCE_TYPE9',1,2,364,''),(1892,'RESULT',1,2,364,''),(1893,'RESULT_CODE',1,2,364,''),(1894,'RESULT_REASON',1,2,364,''),(1895,'RETRY',1,2,364,''),(1896,'RE_ATTR',1,2,364,''),(1897,'RE_ATTR_FLAG',1,2,364,''),(1898,'RE_NAME',1,2,364,''),(1899,'RE_RATING_TIMES',1,2,364,''),(1900,'ROAMING_FLAG',1,2,364,''),(1901,'ROAMING_ZONE_ID',1,2,364,''),(1902,'ROAM_STATE',1,2,364,''),(1903,'ROUTEID',1,2,364,''),(1904,'ROUTING_ID1',1,2,364,''),(1905,'ROUTING_ID_LIST',1,2,364,''),(1906,'ROUTING_PREFIX',1,2,364,''),(1907,'SELF_ACCT_FLAG',1,2,364,''),(1908,'SEND_CUST_ID',1,2,364,''),(1909,'SEND_RESULT',1,2,364,''),(1910,'SEQ',1,2,364,''),(1911,'SEQUENCE',1,2,364,''),(1912,'SEQUENCE_NUMBER',1,2,364,''),(1913,'SERI_CARD',1,2,364,''),(1914,'SERVICE',1,2,364,''),(1915,'SERVICE_ID',1,2,364,''),(1916,'SERVICE_IDENTIFIER',1,2,364,''),(1917,'SERVICE_IDENTIFY',1,2,364,''),(1918,'SERVICE_KEY',1,2,364,''),(1919,'SERVICE_KEY',1,2,364,''),(1920,'SERVICE_LEVEL',1,2,364,''),(1921,'SERVICE_TYPE',1,2,364,''),(1922,'SERVICE_UNIT_TYPE',1,2,364,''),(1923,'SERVICE_UNIT_TYPE2',1,2,364,''),(1924,'SER_CHARGE',1,2,364,''),(1925,'SESSION_ID',1,2,364,''),(1926,'SGSN_ADDRESS',1,2,364,''),(1927,'SGSN_MCC_MNC',1,2,364,''),(1928,'SMSC_ADDRESS',1,2,364,''),(1929,'SMS_NOTIFY_DATE',1,2,364,''),(1930,'SMS_NOTIFY_ID',1,2,364,''),(1931,'SMS_RECEIVE_TIME',1,2,364,''),(1932,'SMS_ROUTE_ID',1,2,364,''),(1933,'SMS_TYPE',1,2,364,''),(1934,'SM_ID',1,2,364,''),(1935,'SM_LENGTH',1,2,364,''),(1936,'SPECIAL_NUMBER_INDICATOR',1,2,364,''),(1937,'SPECIAL_ZONE_ID',1,2,364,''),(1938,'SPID',1,2,364,''),(1939,'SPLIT_CDR_REASON',1,2,364,''),(1940,'SRC_CDR_ID',1,2,364,''),(1941,'SRC_CDR_NO',1,2,364,''),(1942,'SRC_ID',1,2,364,''),(1943,'SRC_REC_LINE_NO',1,2,364,''),(1944,'STATE',1,2,364,''),(1945,'STATEDATE',1,2,364,''),(1946,'STATE_DATE',1,2,364,''),(1947,'STATE_SET',1,2,364,''),(1948,'STATUS',1,2,364,''),(1949,'STA_DATE',1,2,364,''),(1950,'STA_DATETIME',1,2,364,''),(1951,'SUBID',1,2,364,''),(1952,'SUBS_DAILY_ACM',1,2,364,''),(1953,'SUBS_ID_DATA1',1,2,364,''),(1954,'SUBS_ID_DATA2',1,2,364,''),(1955,'SUBS_ID_TYPE1',1,2,364,''),(1956,'SUBS_ID_TYPE2',1,2,364,''),(1957,'SUB_ID',1,2,364,''),(1958,'SUB_ID_DATA_1',1,2,364,''),(1959,'SUB_ID_TYPE_1',1,2,364,''),(1960,'SUB_OFFER',1,2,364,''),(1961,'SUB_OFFER_CREATE_DATE_1',1,2,364,''),(1962,'SUB_OFFER_CREATE_DATE_10',1,2,364,''),(1963,'SUB_OFFER_CREATE_DATE_2',1,2,364,''),(1964,'SUB_OFFER_CREATE_DATE_3',1,2,364,''),(1965,'SUB_OFFER_CREATE_DATE_4',1,2,364,''),(1966,'SUB_OFFER_CREATE_DATE_5',1,2,364,''),(1967,'SUB_OFFER_CREATE_DATE_6',1,2,364,''),(1968,'SUB_OFFER_CREATE_DATE_7',1,2,364,''),(1969,'SUB_OFFER_CREATE_DATE_8',1,2,364,''),(1970,'SUB_OFFER_CREATE_DATE_9',1,2,364,''),(1971,'SUB_OFFER_EXTERNAL_ID_1',1,2,364,''),(1972,'SUB_OFFER_EXTERNAL_ID_10',1,2,364,''),(1973,'SUB_OFFER_EXTERNAL_ID_2',1,2,364,''),(1974,'SUB_OFFER_EXTERNAL_ID_3',1,2,364,''),(1975,'SUB_OFFER_EXTERNAL_ID_4',1,2,364,''),(1976,'SUB_OFFER_EXTERNAL_ID_5',1,2,364,''),(1977,'SUB_OFFER_EXTERNAL_ID_6',1,2,364,''),(1978,'SUB_OFFER_EXTERNAL_ID_7',1,2,364,''),(1979,'SUB_OFFER_EXTERNAL_ID_8',1,2,364,''),(1980,'SUB_OFFER_EXTERNAL_ID_9',1,2,364,''),(1981,'SUB_OFFER_NAME_1',1,2,364,''),(1982,'SUB_OFFER_NAME_10',1,2,364,''),(1983,'SUB_OFFER_NAME_2',1,2,364,''),(1984,'SUB_OFFER_NAME_3',1,2,364,''),(1985,'SUB_OFFER_NAME_4',1,2,364,''),(1986,'SUB_OFFER_NAME_5',1,2,364,''),(1987,'SUB_OFFER_NAME_6',1,2,364,''),(1988,'SUB_OFFER_NAME_7',1,2,364,''),(1989,'SUB_OFFER_NAME_8',1,2,364,''),(1990,'SUB_OFFER_NAME_9',1,2,364,''),(1991,'SUB_PRICE_PLAN_ID',1,2,364,''),(1992,'SUB_STATE',1,2,364,''),(1993,'SUP_OFFER_VERSION',1,2,364,''),(1994,'TARIFF_CHANGE_USAGE',1,2,364,''),(1995,'TelecomServiceID',1,2,364,''),(1996,'TELECOMSERVICE_ID',1,2,364,''),(1997,'TELECOM_SERVICE_ID',1,2,364,''),(1998,'TEMPLATE_ID',1,2,364,''),(1999,'TERMINAL_TYPE',1,2,364,''),(2000,'TERMINATINGIOI',1,2,364,''),(2001,'THIRD_PART_NBR',1,2,364,''),(2002,'THRESHOLD',1,2,364,''),(2003,'TIMESTAMP_OF_SSP',1,2,364,''),(2004,'TIME_NUMBER',1,2,364,''),(2005,'TIME_ZONE_OF_SSP',1,2,364,''),(2006,'TOPUP',1,2,364,''),(2007,'TOPUP_DATE',1,2,364,''),(2008,'TOTAL',1,2,364,''),(2009,'TOTAL_ACTIVE',1,2,364,''),(2010,'TOTAL_ACTIVE_DAILY',1,2,364,''),(2011,'TOTAL_DISABLE',1,2,364,''),(2012,'TOTAL_DISABLE_DAILY',1,2,364,''),(2013,'TOTAL_IDLE',1,2,364,''),(2014,'TOTAL_IDLE_DAILY',1,2,364,''),(2015,'TOTAL_POOL',1,2,364,''),(2016,'TOTAL_POOL_DAILY',1,2,364,''),(2017,'TOTAL_RECHARGE_DAILY',1,2,364,''),(2018,'TOTAL_SUSPEND',1,2,364,''),(2019,'TOTAL_SUSPEND_DAILY',1,2,364,''),(2020,'TOTAL_TAX',1,2,364,''),(2021,'TRADE_METHOD',1,2,364,''),(2022,'TRADE_TIME',1,2,364,''),(2023,'TRADE_TYPE',1,2,364,''),(2024,'TRANSACTION_ID',1,2,364,''),(2025,'TransferMoney',1,2,364,''),(2026,'TRANSITION_ID',1,2,364,''),(2027,'TRANSLATENBR',1,2,364,''),(2028,'TRIGGER_ACM_LIST',1,2,364,''),(2029,'TRIGGER_ID',1,2,364,''),(2030,'TRIGGER_PROCESS_EVENT',1,2,364,''),(2031,'TRUE_RE_ATTR',1,2,364,''),(2032,'TRUNK_CENTREX_SERVICE_TYPE',1,2,364,''),(2033,'TRUNK_GROUP_IN',1,2,364,''),(2034,'TRUNK_GROUP_OUT',1,2,364,''),(2035,'TYPE',1,2,364,''),(2036,'UCB_FLAG',1,2,364,''),(2037,'UNIT_ID_11',1,2,364,''),(2038,'UNIT_ID_12',1,2,364,''),(2039,'UNIT_ID_13',1,2,364,''),(2040,'UNIT_ID_14',1,2,364,''),(2041,'UNIT_ID_16',1,2,364,''),(2042,'UNIT_ID_18',1,2,364,''),(2043,'UNIT_ID_19',1,2,364,''),(2044,'UNIT_ID_20',1,2,364,''),(2045,'UNIT_ID_23',1,2,364,''),(2046,'UNIT_ID_28',1,2,364,''),(2047,'UNIT_ID_29',1,2,364,''),(2048,'UNIT_ID_30',1,2,364,''),(2049,'UNIT_ID_31',1,2,364,''),(2050,'UNIT_ID_36',1,2,364,''),(2051,'UNIT_ID_37',1,2,364,''),(2052,'UNIT_ID_40',1,2,364,''),(2053,'UNIT_ID_42',1,2,364,''),(2054,'UNIT_ID_43',1,2,364,''),(2055,'UNIT_ID_44',1,2,364,''),(2056,'UNIT_ID_47',1,2,364,''),(2057,'UNREGISTERED_FLAG',1,2,364,''),(2058,'UNTIL_DATE',1,2,364,''),(2059,'UNUSED_PROP',1,2,364,''),(2060,'UN_DEBIT_AMOUNT',1,2,364,''),(2061,'UP_DATA',1,2,364,''),(2062,'URL',1,2,364,''),(2063,'USAGE_MEASURE_ID',1,2,364,''),(2064,'USAGE_MEASURE_ID2',1,2,364,''),(2065,'USER_EQUIPMENT_INFO_TYPE',1,2,364,''),(2066,'USER_EQUIPMENT_INFO_VALUE',1,2,364,''),(2067,'USER_EQUIPMENT_INFO_VALUE_ORG',1,2,364,''),(2068,'USER_NAME',1,2,364,''),(2069,'USU_CC_INPUT_OCTETS',1,2,364,''),(2070,'USU_CC_OUTPUT_OCTETS',1,2,364,''),(2071,'USU_CC_TIME',1,2,364,''),(2072,'USU_CC_TOTAL_OCTETS',1,2,364,''),(2073,'VALUE',1,2,364,''),(2074,'VAS_SERVICE_TYPE',1,2,364,''),(2075,'VCPIN',1,2,364,''),(2076,'VCTRADE_SN',1,2,364,''),(2077,'VN_STA_DATETIME',1,2,364,''),(2078,'VOLUME',1,2,364,''),(2079,'VPLMN',1,2,364,''),(2080,'VPN_INFO',1,2,364,''),(2081,'VPN_NAME',1,2,364,''),(2082,'WAIT_DURATION',1,2,364,''),(2083,'ZONE_ADDRESS',1,2,364,''),(2084,'ZONE_CODE',1,2,364,''),(2085,'ZONE_TYPE',1,2,364,'');
/*!40000 ALTER TABLE `input_object` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invitation_priority`
--

DROP TABLE IF EXISTS `invitation_priority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `invitation_priority` (
  `INVITATION_PRIORITY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CAMPAIGN_ID` bigint(20) NOT NULL,
  `SEGMENT_ID` bigint(20) NOT NULL,
  `NOTIFY_ID` bigint(20) NOT NULL,
  `NOTIFY_TYPE` int(11) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  PRIMARY KEY (`INVITATION_PRIORITY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=780 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invitation_priority`
--

LOCK TABLES `invitation_priority` WRITE;
/*!40000 ALTER TABLE `invitation_priority` DISABLE KEYS */;
INSERT INTO `invitation_priority` VALUES (766,10425,8,725,1,0),(768,10427,7,725,1,0),(769,10427,8,725,1,1),(770,10428,8,725,1,0),(771,10429,2,725,1,0),(772,10429,2,728,1,1),(773,10426,8,725,1,0),(774,10430,5,728,1,1),(775,10430,5,725,1,0),(776,10431,1,725,1,2),(777,10431,3,725,1,1),(778,10431,1,728,1,3),(779,10431,3,728,1,0);
/*!40000 ALTER TABLE `invitation_priority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `language` (
  `LANGUAGE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `LANGUAGE_NAME` varchar(1000) NOT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`LANGUAGE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES (1,'VN','VN',1),(2,'EN','EN',1),(8,'xx','xx',1),(9,'yy','yy',1),(10,'zz','zz',1),(11,'tt','tt',1),(12,'ee','ee',1),(13,'qq','qq',1),(14,'rr','rr',1),(15,'hh','hh',1),(16,'dd','dd',1),(17,'A','uu',1),(19,'ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss',NULL,1),(20,'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq',NULL,1);
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `map_command`
--

DROP TABLE IF EXISTS `map_command`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `map_command` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESTINATION` varchar(255) DEFAULT NULL,
  `COMMAND_ID` int(11) DEFAULT NULL,
  `COMMAND_NAME` varchar(255) DEFAULT NULL,
  `PROTOCOL` int(11) DEFAULT NULL,
  `RESULT_CODE` int(11) DEFAULT NULL,
  `SMSC_CODE` int(11) DEFAULT NULL,
  `ACCT_BOOK_TYPE_FOR_CDR` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `map_command`
--

LOCK TABLES `map_command` WRITE;
/*!40000 ALTER TABLE `map_command` DISABLE KEYS */;
INSERT INTO `map_command` VALUES (1,NULL,1,'command 1',NULL,NULL,NULL,NULL),(2,NULL,2,'command 2',NULL,NULL,NULL,NULL),(3,NULL,3,'a',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `map_command` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note_content`
--

DROP TABLE IF EXISTS `note_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `note_content` (
  `NODE_CONTENT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NODE_ID` bigint(20) NOT NULL,
  `LANGUAGE_ID` bigint(20) NOT NULL,
  `CONTENT` varchar(200) NOT NULL,
  PRIMARY KEY (`NODE_CONTENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note_content`
--

LOCK TABLES `note_content` WRITE;
/*!40000 ALTER TABLE `note_content` DISABLE KEYS */;
INSERT INTO `note_content` VALUES (120,765,17,'{xxxxxxxxxxx}'),(121,769,17,'{xxx}'),(122,785,17,'{xxxxx}'),(124,817,17,'xxxx'),(125,865,17,'xxx'),(129,896,17,'xxxxxxxxxxxxxxxxxxxxxxx'),(132,900,17,'rtyuio'),(135,903,17,'hhhhhhh'),(138,907,17,'hhh'),(143,911,17,'rtyu'),(146,915,17,'rty'),(150,926,17,'xxxxxxxxxxx'),(152,936,17,'aaaaaa');
/*!40000 ALTER TABLE `note_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notify_content`
--

DROP TABLE IF EXISTS `notify_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notify_content` (
  `NOTIFY_CONTENT_ID` bigint(20) NOT NULL,
  `NOTIFY_TEMPLATE_ID` bigint(20) NOT NULL,
  `LANGUAGE_ID` int(11) NOT NULL,
  `CONTENT` varchar(500) NOT NULL,
  PRIMARY KEY (`NOTIFY_CONTENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notify_content`
--

LOCK TABLES `notify_content` WRITE;
/*!40000 ALTER TABLE `notify_content` DISABLE KEYS */;
INSERT INTO `notify_content` VALUES (664,428,17,'{xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx}'),(666,429,17,'{xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx}'),(667,430,17,'{xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx}'),(669,430,2,'{xxzzz}'),(673,434,17,'{xx}');
/*!40000 ALTER TABLE `notify_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notify_statistic_map`
--

DROP TABLE IF EXISTS `notify_statistic_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notify_statistic_map` (
  `ID` bigint(11) NOT NULL AUTO_INCREMENT,
  `NOTIFY_ID` bigint(11) NOT NULL,
  `STATISTIC_CYCLE_ID` bigint(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notify_statistic_map`
--

LOCK TABLES `notify_statistic_map` WRITE;
/*!40000 ALTER TABLE `notify_statistic_map` DISABLE KEYS */;
INSERT INTO `notify_statistic_map` VALUES (241,728,203),(242,729,203),(243,730,204);
/*!40000 ALTER TABLE `notify_statistic_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notify_tempalate_param_map`
--

DROP TABLE IF EXISTS `notify_tempalate_param_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notify_tempalate_param_map` (
  `NOTIFY_TEMPLATE_PARAM_ID` bigint(20) NOT NULL,
  `NOTIFY_TEMPLATE_ID` bigint(20) NOT NULL,
  `TEMPLATE_PARAM_ID` bigint(20) NOT NULL,
  `PARAM_INDEX` int(11) NOT NULL,
  PRIMARY KEY (`NOTIFY_TEMPLATE_PARAM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notify_tempalate_param_map`
--

LOCK TABLES `notify_tempalate_param_map` WRITE;
/*!40000 ALTER TABLE `notify_tempalate_param_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `notify_tempalate_param_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notify_template`
--

DROP TABLE IF EXISTS `notify_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notify_template` (
  `TEMPLATE_ID` bigint(20) NOT NULL,
  `TEMPLATE_NAME` varchar(1000) NOT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `TEMPLATE_TYPE` int(11) DEFAULT NULL,
  PRIMARY KEY (`TEMPLATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notify_template`
--

LOCK TABLES `notify_template` WRITE;
/*!40000 ALTER TABLE `notify_template` DISABLE KEYS */;
INSERT INTO `notify_template` VALUES (428,'template 1712','',166,1),(429,'template 1812','',166,1),(430,'notify template 1812','',166,NULL),(434,'67','',166,NULL);
/*!40000 ALTER TABLE `notify_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notify_trigger`
--

DROP TABLE IF EXISTS `notify_trigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notify_trigger` (
  `NOTIFY_TRIGGER_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TRIGGER_NAME` varchar(1000) NOT NULL,
  `NOTIFY_TYPE` int(11) NOT NULL,
  `ALIAS` varchar(1000) NOT NULL,
  `NOTIFY_TEMPLATE_ID` bigint(20) NOT NULL,
  `IS_INVITE` tinyint(1) DEFAULT NULL,
  `NUMBER_RETRY` int(11) DEFAULT NULL,
  `REPEAT_TIME` bigint(20) DEFAULT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `CHANNEL_TYPE` int(1) DEFAULT NULL,
  `RETRY_CYCLE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`NOTIFY_TRIGGER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=737 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notify_trigger`
--

LOCK TABLES `notify_trigger` WRITE;
/*!40000 ALTER TABLE `notify_trigger` DISABLE KEYS */;
INSERT INTO `notify_trigger` VALUES (725,'trigger 1712',1,'11',428,1,22,900000,'',175,NULL,1),(728,'1812',1,'112',430,1,NULL,NULL,'',175,NULL,NULL),(729,'1912',1,'12',430,0,NULL,NULL,'',175,NULL,NULL),(730,'1912s',1,'12',430,0,NULL,NULL,'',175,NULL,NULL),(736,'76',1,'55',430,0,NULL,NULL,'',175,NULL,NULL);
/*!40000 ALTER TABLE `notify_trigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notify_values`
--

DROP TABLE IF EXISTS `notify_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notify_values` (
  `NOTIFY_VALUE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TRIGGER_NOTIFY_ID` bigint(20) NOT NULL,
  `TEMPLATE_PARAM_ID` bigint(20) DEFAULT NULL,
  `SOURCE_TYPE` int(11) NOT NULL,
  `DATA_TYPE` int(11) NOT NULL,
  `VALUE` varchar(1000) NOT NULL,
  `PARAM_STRING` varchar(1000) NOT NULL,
  PRIMARY KEY (`NOTIFY_VALUE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=476 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notify_values`
--

LOCK TABLES `notify_values` WRITE;
/*!40000 ALTER TABLE `notify_values` DISABLE KEYS */;
INSERT INTO `notify_values` VALUES (465,725,428,1,1,'98','xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'),(466,728,430,1,1,'56','xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'),(467,728,430,1,1,'78','xxzzz'),(468,729,430,1,1,'67','xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'),(469,729,430,1,2,'89','xxzzz'),(470,730,430,1,1,'13','xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'),(471,730,430,1,1,'57','xxzzz'),(474,736,430,1,1,'7','xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'),(475,736,430,1,2,'9','xxzzz');
/*!40000 ALTER TABLE `notify_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `object_define`
--

DROP TABLE IF EXISTS `object_define`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `object_define` (
  `OBJ_ID` int(12) NOT NULL,
  `OBJ_CLASS` varchar(50) NOT NULL,
  `PARENT_OBJ_ID` int(12) DEFAULT NULL,
  `IS_CLEAN` tinyint(1) NOT NULL,
  `CHILD_ATT_NAME` varchar(50) DEFAULT NULL,
  `CHILD_STRUCTURE_TYPE` int(1) DEFAULT NULL,
  `LINK_SQL_OBJ_NAME` varchar(50) DEFAULT NULL,
  `OBJECT_ATT` varchar(50) DEFAULT NULL,
  `SQL_ATT` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`OBJ_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `object_define`
--

LOCK TABLES `object_define` WRITE;
/*!40000 ALTER TABLE `object_define` DISABLE KEYS */;
/*!40000 ALTER TABLE `object_define` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocs_behaviour`
--

DROP TABLE IF EXISTS `ocs_behaviour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ocs_behaviour` (
  `BEHAVIOUR_ID` bigint(20) NOT NULL,
  `BEHAVIOUR_NAME` varchar(1000) NOT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `TEMPLATE_ID` bigint(20) NOT NULL,
  `NOTIFY_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`BEHAVIOUR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocs_behaviour`
--

LOCK TABLES `ocs_behaviour` WRITE;
/*!40000 ALTER TABLE `ocs_behaviour` DISABLE KEYS */;
INSERT INTO `ocs_behaviour` VALUES (249,'xxxxxx','',153,252,NULL),(250,'ocs behaviour 1812','',153,252,NULL),(253,'865','',153,252,NULL);
/*!40000 ALTER TABLE `ocs_behaviour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocs_behaviour_extend_fields`
--

DROP TABLE IF EXISTS `ocs_behaviour_extend_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ocs_behaviour_extend_fields` (
  `ID` bigint(11) NOT NULL AUTO_INCREMENT,
  `BEHAVIOUR_ID` bigint(11) NOT NULL,
  `EXTEND_FIELD_ID` bigint(11) NOT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=583 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocs_behaviour_extend_fields`
--

LOCK TABLES `ocs_behaviour_extend_fields` WRITE;
/*!40000 ALTER TABLE `ocs_behaviour_extend_fields` DISABLE KEYS */;
INSERT INTO `ocs_behaviour_extend_fields` VALUES (575,249,35,'1'),(576,249,52,'2'),(577,250,35,'12'),(578,250,52,'11'),(581,253,35,'7'),(582,253,52,'9');
/*!40000 ALTER TABLE `ocs_behaviour_extend_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocs_behaviour_fields`
--

DROP TABLE IF EXISTS `ocs_behaviour_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ocs_behaviour_fields` (
  `BEHAVIOUR_FIELD_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `OCS_BEHAVIOUR_ID` int(11) unsigned NOT NULL,
  `CRA_FIELD_ID` int(11) unsigned NOT NULL,
  `DATA_TYPE` int(11) unsigned NOT NULL,
  `VALUE` varchar(45) DEFAULT NULL,
  `SOURCE_TYPE` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`BEHAVIOUR_FIELD_ID`),
  UNIQUE KEY `BEHAVIOUR_FIELD_ID` (`BEHAVIOUR_FIELD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=469 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocs_behaviour_fields`
--

LOCK TABLES `ocs_behaviour_fields` WRITE;
/*!40000 ALTER TABLE `ocs_behaviour_fields` DISABLE KEYS */;
INSERT INTO `ocs_behaviour_fields` VALUES (465,249,2,1,'22',1),(466,250,2,1,'12',1),(468,253,2,1,'78',1);
/*!40000 ALTER TABLE `ocs_behaviour_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocs_behaviour_template`
--

DROP TABLE IF EXISTS `ocs_behaviour_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ocs_behaviour_template` (
  `TEMPLATE_ID` bigint(20) NOT NULL,
  `TEMPLATE_NAME` varchar(1000) NOT NULL,
  `COMMAND_CONTENT` varchar(1000) DEFAULT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `COMMAND_CODE` int(11) NOT NULL,
  `COMMAND_NAME` varchar(45) NOT NULL,
  PRIMARY KEY (`TEMPLATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocs_behaviour_template`
--

LOCK TABLES `ocs_behaviour_template` WRITE;
/*!40000 ALTER TABLE `ocs_behaviour_template` DISABLE KEYS */;
INSERT INTO `ocs_behaviour_template` VALUES (252,'ocs behaviour template1712',NULL,'',152,1,'command 1'),(254,'987',NULL,'',152,1,'command 1');
/*!40000 ALTER TABLE `ocs_behaviour_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocs_behaviour_template_extend_fields`
--

DROP TABLE IF EXISTS `ocs_behaviour_template_extend_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ocs_behaviour_template_extend_fields` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BEHAVIOUR_TEMPLATE_ID` int(11) NOT NULL,
  `EXTEND_FIELD_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=787 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocs_behaviour_template_extend_fields`
--

LOCK TABLES `ocs_behaviour_template_extend_fields` WRITE;
/*!40000 ALTER TABLE `ocs_behaviour_template_extend_fields` DISABLE KEYS */;
INSERT INTO `ocs_behaviour_template_extend_fields` VALUES (785,252,35),(786,252,52);
/*!40000 ALTER TABLE `ocs_behaviour_template_extend_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocs_behaviour_template_fields`
--

DROP TABLE IF EXISTS `ocs_behaviour_template_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ocs_behaviour_template_fields` (
  `TEMPLATE_FIELD_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BEHAVIOUR_TEMPLATE_ID` int(11) NOT NULL,
  `CRA_FIELD_ID` int(11) NOT NULL,
  `DATA_TYPE` int(11) NOT NULL,
  `VALUE` varchar(45) DEFAULT NULL,
  `SOURCE_TYPE` int(11) NOT NULL,
  `REMARK` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`TEMPLATE_FIELD_ID`),
  UNIQUE KEY `TEMPALTE_FIELD_ID_UNIQUE` (`TEMPLATE_FIELD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=553 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocs_behaviour_template_fields`
--

LOCK TABLES `ocs_behaviour_template_fields` WRITE;
/*!40000 ALTER TABLE `ocs_behaviour_template_fields` DISABLE KEYS */;
INSERT INTO `ocs_behaviour_template_fields` VALUES (549,252,1,1,'67',1,NULL),(550,252,2,1,NULL,2,NULL),(552,254,1,1,'89',1,NULL);
/*!40000 ALTER TABLE `ocs_behaviour_template_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocs_behaviour_values`
--

DROP TABLE IF EXISTS `ocs_behaviour_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ocs_behaviour_values` (
  `OCS_BEHAVIOUR_VALUE_ID` bigint(20) NOT NULL,
  `OCS_BEHAVIOUR_ID` bigint(20) NOT NULL,
  `TEMPLATE_PARAM_ID` bigint(20) NOT NULL,
  `SOURCE_TYPE` int(11) NOT NULL,
  `DATA_TYPE` int(11) NOT NULL,
  `VALUE` varchar(200) NOT NULL,
  PRIMARY KEY (`OCS_BEHAVIOUR_VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocs_behaviour_values`
--

LOCK TABLES `ocs_behaviour_values` WRITE;
/*!40000 ALTER TABLE `ocs_behaviour_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `ocs_behaviour_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter`
--

DROP TABLE IF EXISTS `parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `parameter` (
  `PARAMETER_ID` int(11) NOT NULL,
  `OWNER_LEVEL` int(11) NOT NULL,
  `FOR_TEMPLATE` int(11) DEFAULT NULL,
  `PARAMETER_NAME` varchar(45) NOT NULL,
  `PARAMETER_VALUE` varchar(45) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PARAMETER_ID`),
  UNIQUE KEY `PARAMETER_ID_UNIQUE` (`PARAMETER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter`
--

LOCK TABLES `parameter` WRITE;
/*!40000 ALTER TABLE `parameter` DISABLE KEYS */;
INSERT INTO `parameter` VALUES (1,1,1,'Parameter Number 1','Parameter Number 1',243),(2,1,1,'Parameter Number 2','Parameter Number 2',243),(3,1,1,'Parameter String 1','Parameter String 1',244),(4,1,1,'Parameter String 2','Parameter String 2',244),(5,1,1,'Parameter','Parameter ',242);
/*!40000 ALTER TABLE `parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_notify_trigger`
--

DROP TABLE IF EXISTS `post_notify_trigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `post_notify_trigger` (
  `POST_NOTIFY_TRIGGER_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `OCS_BEHAVIOUR_ID` int(11) NOT NULL,
  `ERROR_CODE` int(11) NOT NULL,
  `COMMAND_ID` int(11) DEFAULT NULL,
  `NOTIFY_ID` int(11) NOT NULL,
  PRIMARY KEY (`POST_NOTIFY_TRIGGER_ID`),
  UNIQUE KEY `POST_TRIGGER_NOTIFY_ID` (`POST_NOTIFY_TRIGGER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_notify_trigger`
--

LOCK TABLES `post_notify_trigger` WRITE;
/*!40000 ALTER TABLE `post_notify_trigger` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_notify_trigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pre_process_param_map`
--

DROP TABLE IF EXISTS `pre_process_param_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pre_process_param_map` (
  `PRE_PROCESS_PARAM_MAP` bigint(20) NOT NULL AUTO_INCREMENT,
  `PROCESS_PARAM_ID` bigint(20) NOT NULL,
  `PRE_PROCESS_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`PRE_PROCESS_PARAM_MAP`)
) ENGINE=InnoDB AUTO_INCREMENT=1690 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pre_process_param_map`
--

LOCK TABLES `pre_process_param_map` WRITE;
/*!40000 ALTER TABLE `pre_process_param_map` DISABLE KEYS */;
INSERT INTO `pre_process_param_map` VALUES (1610,1612,1224),(1611,1613,1224),(1614,1616,1225),(1615,1617,1225),(1618,1620,1236),(1619,1621,1238),(1620,1622,1238),(1621,1623,1238),(1622,1624,1238),(1623,1625,1238),(1624,1626,1238),(1625,1627,1238),(1626,1628,1239),(1627,1629,1239),(1628,1630,1239),(1629,1631,1239),(1630,1632,1239),(1631,1633,1239),(1632,1634,1239),(1635,1637,1243),(1636,1638,1243),(1637,1639,1232),(1638,1640,1232),(1641,1643,1259),(1642,1644,1259),(1643,1645,1259),(1644,1646,1259),(1645,1647,1259),(1646,1648,1259),(1647,1649,1259),(1648,1650,1261),(1649,1651,1262),(1650,1652,1265),(1651,1653,1256),(1652,1654,1255),(1654,1656,1240),(1655,1657,1288),(1656,1658,1289),(1659,1661,1298),(1663,1665,1299),(1665,1667,1300),(1673,1675,1302),(1674,1676,1302),(1675,1677,1302),(1676,1678,1302),(1677,1679,1302),(1678,1680,1302),(1679,1681,1302),(1681,1683,1303),(1683,1685,1304),(1685,1687,1307),(1687,1689,1308),(1689,1691,1309);
/*!40000 ALTER TABLE `pre_process_param_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pre_process_unit`
--

DROP TABLE IF EXISTS `pre_process_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pre_process_unit` (
  `PRE_PROCESS_ID` bigint(20) NOT NULL,
  `PRE_PROCESS_NAME` varchar(1000) NOT NULL,
  `PRE_PROCESS_TYPE` int(11) NOT NULL,
  `DEFAULT_VALUE` int(11) NOT NULL,
  `INPUT_FIELDS` varchar(1000) NOT NULL,
  `SPECIAL_FIELDS` varchar(200) DEFAULT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PRE_PROCESS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pre_process_unit`
--

LOCK TABLES `pre_process_unit` WRITE;
/*!40000 ALTER TABLE `pre_process_unit` DISABLE KEYS */;
INSERT INTO `pre_process_unit` VALUES (1224,'ppu string 1712',1,1,'type:2|getAccumulateExactDay(1,555555555555555555555555555555555555555555)',NULL,'',272),(1225,'ppu number range',6,1,'type:1|campaignPropMap{telecomServiceId=-2&sessionId=true;subString(1,2)}','isUseParameter:true;isEqualAtEndValue:false','xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',251),(1232,'number 1712',2,1,'type:1|cbaCustomer{;prefix(1)}','isUseParameter:true','',207),(1233,'same 1712',9,1,'type:1|segmentId{;suffix(1)}%type:1|custProfile{;suffix(1)}',NULL,'',234),(1236,'11111111111111111111111111111111111111111111111',2,1,'type:1|custProfile{;prefix(1)}','isUseParameter:false','111111111111111111111111111111111111111111111111111111',207),(1238,'time1',4,2,'type:1|msisdn','isCurrentTimeUsing:false','time1',236),(1239,'ppu time 1712',4,1,'type:1|detailErrorCode{;prefix(1)}','isCurrentTimeUsing:false','',236),(1240,'ppu date 1712',5,1,'type:1|cdrEvent{;subString(1,2)}','isEqualAtEndValue:false','',240),(1241,'compare number 1712',7,0,'type:1|cdrEvent{;suffix(1)}%type:1|custProfile{;getZonesFromListZoneString()}','compareType:1','',227),(1242,'exist element 1712',3,1,'type:1|accumInfo{;suffix(1)}',NULL,'',255),(1243,'ppu zone 1712',8,1,'type:1|cbaCustomer{;subString(1,1):thaoTest(765,1,2019%12%17%04%25%30,1,1)}',NULL,'',256),(1255,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',1,1,'type:1|campaignPropMap{telecomServiceId=1;suffix(1)}',NULL,'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',272),(1256,'22222222222222222qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq',2,1,'type:1|custProfile{;subString(1,1)}','isUseParameter:true','fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff',207),(1257,'999999999999hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh',9,1,'type:1|cbaCustomer{;prefix(1)}%type:1|accumInfo{;subString(1,2)}',NULL,'gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg',234),(1259,'12222222222111111111111111111111112222222222222',4,1,'type:1|detailErrorCode{;prefix(1)}','isCurrentTimeUsing:false','tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt',236),(1261,'356zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',5,1,'type:1|cbaCustomer{;subString(1,2)}','isEqualAtEndValue:false','777777777777777777777777777777777777777hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh',240),(1262,'678ggggggggggggggggggggggggggggfffffffffffffffffffffffffffffffff',6,1,'type:1|detailErrorCode{;suffix(1)}','isUseParameter:true;isEqualAtEndValue:false','hhhhhhhhhhhhhhhhhhhhhhhhhhggggggggggggggggggggggggggggttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt',251),(1263,'999998888888888888ggggggggggggggghhhhhhhhhhhhhh',7,0,'type:1|segmentId{;standardMsisdnWithoutMNP()}%type:1|campaignPropMap{;isOffnetNumberWithMNPPrefix()}','compareType:1','fffffffffffffffffffffddddddddddddddddddddddddddddddddddhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh',227),(1264,'77777777777778888888888888888hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh',3,1,'type:1|cbaCustomer{;suffix(1)}',NULL,'xxxxxxxxxxxxxxxxxxxxxxxzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',255),(1265,'kkkkkkkkkkkkkkkkhhhhhhhhhhhhhhhhhhhhhhggggggggggggggggggggggggggggggg',8,1,'type:1|segmentId{;prefix(1)}',NULL,'lllllllllllllhhhhhhhhhhhhhhhhhhhhhhhggggggggggggggggggggggggggggfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff',256),(1288,'2012',5,1,'type:1|detailErrorCode{;prefix(1)}','isEqualAtEndValue:false','',240),(1289,'2012s',5,1,'type:1|custProfile{;suffix(1)}','isEqualAtEndValue:false','',240),(1298,'123',5,1,'type:1|segmentId{;suffix(1)}','isEqualAtEndValue:false','',240),(1299,'12367',1,1,'type:1|errorCode{;prefix(1)}',NULL,'',272),(1300,'201267',2,1,'type:1|offlineCampaignId{;prefix(1)}','isUseParameter:false','',207),(1301,'ggggffffffff',9,1,'type:1|detailErrorCode{;suffix(1)}%type:1|cbaCustomer{;suffix(1)}',NULL,'',234),(1302,'67',4,1,'type:1|errorCode{;suffix(1)}','isCurrentTimeUsing:false','',236),(1303,'67gh',5,1,'type:1|detailErrorCode','isEqualAtEndValue:false','',240),(1304,'98hgf',6,1,'type:1|detailErrorCode{;subString(1,2)}','isUseParameter:false;isEqualAtEndValue:false','',251),(1305,'678',7,0,'type:1|offlineCampaignId{;subString(1,2)}%type:1|campaignType{;getListSize()}','compareType:1','',227),(1306,'5tgh',3,1,'type:1|offlineCampaignId{;suffix(1)}',NULL,'',255),(1307,'78hgf',8,1,'type:1|detailErrorCode{;isOffnetNumberWithMNPPrefix()}',NULL,'',256),(1308,'jhgf',1,1,'type:1|segmentId{;prefix(1)}',NULL,'',272),(1309,'987654',5,1,'type:1|detailErrorCode{;prefix(1)}','isEqualAtEndValue:false','',240);
/*!40000 ALTER TABLE `pre_process_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pre_process_value_map`
--

DROP TABLE IF EXISTS `pre_process_value_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pre_process_value_map` (
  `PRE_PROCESS_VALUE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PRE_PROCESS_ID` bigint(20) NOT NULL,
  `PROCESS_VALUE_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`PRE_PROCESS_VALUE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1561 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pre_process_value_map`
--

LOCK TABLES `pre_process_value_map` WRITE;
/*!40000 ALTER TABLE `pre_process_value_map` DISABLE KEYS */;
INSERT INTO `pre_process_value_map` VALUES (1497,1224,1509),(1498,1224,1510),(1499,1225,1511),(1500,1225,1512),(1501,1232,1513),(1502,1232,1514),(1503,1233,1515),(1504,1233,1516),(1505,1236,1517),(1506,1238,1518),(1507,1238,1519),(1508,1239,1520),(1509,1239,1521),(1510,1240,1522),(1511,1240,1523),(1512,1241,1524),(1513,1241,1525),(1514,1242,1526),(1515,1242,1527),(1516,1243,1528),(1517,1243,1529),(1518,1255,1530),(1519,1255,1531),(1521,1257,1533),(1522,1257,1534),(1523,1259,1535),(1524,1261,1536),(1525,1262,1537),(1526,1263,1538),(1527,1263,1539),(1528,1264,1540),(1529,1264,1541),(1530,1265,1542),(1531,1256,1543),(1532,1288,1544),(1533,1289,1545),(1536,1298,1548),(1540,1299,1552),(1542,1300,1554),(1543,1301,1555),(1544,1301,1556),(1546,1302,1558),(1548,1303,1560),(1550,1304,1562),(1551,1305,1563),(1552,1305,1564),(1553,1306,1565),(1554,1306,1566),(1556,1307,1568),(1558,1308,1570),(1560,1309,1572);
/*!40000 ALTER TABLE `pre_process_value_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `process_param`
--

DROP TABLE IF EXISTS `process_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `process_param` (
  `PROCESS_PARAM_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PARAM_INDEX` int(11) NOT NULL,
  `CONFIG_INPUT` varchar(200) NOT NULL,
  PRIMARY KEY (`PROCESS_PARAM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1692 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `process_param`
--

LOCK TABLES `process_param` WRITE;
/*!40000 ALTER TABLE `process_param` DISABLE KEYS */;
INSERT INTO `process_param` VALUES (1612,1,'inputStr:111111111111111111111111111111111111111111111111111111111111111111111111111111111111;CompareType:1;priority:1'),(1613,2,'inputStr:2222222222222222222222222222222222222222222222222222222222222222222222222;CompareType:1;priority:2'),(1616,1,'start:1;startIsParameter:true;end:12;endIsParameter:false;priority:1'),(1617,2,'start:23;startIsParameter:false;end:56;endIsParameter:false;priority:2'),(1620,1,'inputNumber:1111111111111111111111111111111111111111;comparisionType:1;priority:1'),(1621,1,'day:1;hour:0;min:0;sec:0'),(1622,1,'day:2;hour:0;min:0;sec:0'),(1623,1,'day:3;hour:0;min:0;sec:0'),(1624,1,'day:4;hour:0;min:0;sec:0'),(1625,1,'day:5;hour:0;min:0;sec:0'),(1626,1,'day:6;hour:0;min:0;sec:0'),(1627,1,'day:7;hour:0;min:0;sec:0'),(1628,1,'day:1;hour:0;min:0;sec:0'),(1629,1,'day:2;hour:0;min:0;sec:0'),(1630,2,'day:3;hour:0;min:0;sec:0'),(1631,1,'day:4;hour:0;min:0;sec:0'),(1632,1,'day:5;hour:0;min:0;sec:0'),(1633,1,'day:6;hour:0;min:0;sec:0'),(1634,1,'day:7;hour:0;min:0;sec:0'),(1637,1,'zoneValueId:1;dataZoneType:0;priority:1'),(1638,2,'zoneValueId:5;dataZoneType:0;priority:2'),(1639,1,'inputNumber:5;isUseParameter:true;comparisionType:1;priority:1'),(1640,2,'inputNumber:13;isUseParameter:false;comparisionType:1;priority:2'),(1643,1,'day:1;hour:0;min:0;sec:0'),(1644,1,'day:2;hour:0;min:0;sec:0'),(1645,1,'day:3;hour:0;min:0;sec:0'),(1646,1,'day:4;hour:0;min:0;sec:0'),(1647,1,'day:5;hour:0;min:0;sec:0'),(1648,1,'day:6;hour:0;min:0;sec:0'),(1649,1,'day:7;hour:0;min:0;sec:0'),(1650,1,'startValue:1;startType:3;endValue:0;endType:2;priority:1'),(1651,1,'start:2;startIsParameter:true;end:4;endIsParameter:true;priority:1'),(1652,1,'zoneValueId:4;dataZoneType:2;priority:1'),(1653,1,'inputNumber:2;isUseParameter:true;comparisionType:1;priority:1'),(1654,2,'inputStr:11111;CompareType:1;priority:1'),(1656,2,'startValue:-2;startType:3;endValue:-1;endType:3;priority:2'),(1657,1,'startValue:-3;startType:3;endValue:-1;endType:3;priority:1'),(1658,1,'startValue:-3;startType:3;endValue:-2;endType:3;priority:1'),(1661,1,'startValue:2019/12/21/03/15/32;startType:1;endValue:2019/12/21/05/22/41;endType:1;priority:1'),(1665,1,'inputStr:67;CompareType:1;priority:1'),(1667,1,'inputNumber:45;comparisionType:1;priority:1'),(1675,1,'day:1;hour:0;min:0;sec:0'),(1676,1,'day:2;hour:0;min:0;sec:0'),(1677,1,'day:3;hour:0;min:0;sec:0'),(1678,1,'day:4;hour:0;min:0;sec:0'),(1679,1,'day:5;hour:0;min:0;sec:0'),(1680,1,'day:6;hour:0;min:0;sec:0'),(1681,1,'day:7;hour:0;min:0;sec:0'),(1683,1,'startValue:1;startType:3;endValue:2019/12/21/03/15/23;endType:1;priority:1'),(1685,1,'start:78;end:89;priority:1'),(1687,1,'zoneValueId:2;dataZoneType:0;priority:1'),(1689,1,'inputStr:98;CompareType:1;priority:1'),(1691,1,'startValue:0;startType:2;endValue:1;endType:3;priority:1');
/*!40000 ALTER TABLE `process_param` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `process_value`
--

DROP TABLE IF EXISTS `process_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `process_value` (
  `PROCESS_VALUE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE_ID` int(11) NOT NULL,
  `VALUE_NAME` varchar(1000) NOT NULL,
  `VALUE_INDEX` int(11) NOT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `VALUE_COLOR` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`PROCESS_VALUE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1573 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `process_value`
--

LOCK TABLES `process_value` WRITE;
/*!40000 ALTER TABLE `process_value` DISABLE KEYS */;
INSERT INTO `process_value` VALUES (1509,1,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',1,'','18a178'),(1510,2,'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',2,'','91274a'),(1511,1,'vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv',1,'','c4134e'),(1512,2,'nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn',2,'','1c991c'),(1513,1,'12',1,'','d62761'),(1514,2,'13',2,'','49b02a'),(1515,0,'false',0,'','c93c6b'),(1516,1,'true',1,'','910909'),(1517,1,'11111111111111111',1,'','de6f94'),(1518,1,'1',1,'','120107'),(1519,2,'2',2,'','fffcfd'),(1520,1,'121',1,'','2fabd4'),(1521,2,'13',2,'','2dc492'),(1522,1,'t5',1,'','bf4970'),(1523,2,'54',2,'','318f31'),(1524,0,'false',0,'','e32b68'),(1525,1,'true',1,'','876872'),(1526,0,'false',0,'','ab6c81'),(1527,1,'true',1,'','fc246c'),(1528,1,'78',1,'','73203c'),(1529,2,'66',2,'','ad808f'),(1530,1,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',1,'','b81e51'),(1531,2,'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',2,'','5e5ed9'),(1533,0,'false',0,NULL,'fffcfd'),(1534,1,'true',1,NULL,'fffcfd'),(1535,1,'kkkkkkkkkkkk',1,'','cf1956'),(1536,1,'888888888888777777777777777777',1,'','ab2c57'),(1537,1,'45eeeeeeeeeee',1,'','9e989a'),(1538,0,'false',0,'','c73a69'),(1539,1,'true',1,'','9c7884'),(1540,0,'false',0,'','e0dcdd'),(1541,1,'true',1,'','bd8296'),(1542,1,'xxxxzzzzzzzzzzzzzzzzz',1,'','ab3059'),(1543,1,'22222',1,'','87022e'),(1544,1,'test',1,'','7a1436'),(1545,1,'123',1,'','690828'),(1548,1,'87',1,'','94445f'),(1552,1,'76',1,'','853852'),(1554,1,'123',1,'','c74d76'),(1555,0,'false',0,NULL,'fffcfd'),(1556,1,'true',1,NULL,'fffcfd'),(1558,1,'56',1,'','690828'),(1560,1,'1sd',1,'','b56781'),(1562,1,'89hh',1,'','de316b'),(1563,0,'false',0,NULL,'fffcfd'),(1564,1,'true',1,NULL,'fffcfd'),(1565,0,'false',0,NULL,'fffcfd'),(1566,1,'true',1,NULL,'fffcfd'),(1568,1,'89',1,'','b54369'),(1570,1,'g65',1,'','7d2240'),(1572,1,'23',1,'','823e55');
/*!40000 ALTER TABLE `process_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promotion_block`
--

DROP TABLE IF EXISTS `promotion_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `promotion_block` (
  `PROMOTION_BLOCK_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `PROMOTION_BLOCK_NAME` varchar(1000) NOT NULL,
  PRIMARY KEY (`PROMOTION_BLOCK_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promotion_block`
--

LOCK TABLES `promotion_block` WRITE;
/*!40000 ALTER TABLE `promotion_block` DISABLE KEYS */;
INSERT INTO `promotion_block` VALUES (120,'',151,'block 1712'),(121,'',151,'block 1812'),(122,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',151,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'),(123,'',151,'9866');
/*!40000 ALTER TABLE `promotion_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request`
--

DROP TABLE IF EXISTS `request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `request` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `TRANSACTION_ID` varchar(200) NOT NULL,
  `FACTOR` int(11) DEFAULT NULL,
  `OBJECT_ID` bigint(20) unsigned NOT NULL,
  `OBJECT_TYPE` int(11) unsigned NOT NULL,
  `REQUEST_TYPE` int(11) unsigned NOT NULL,
  `SERVICE_ID` int(11) unsigned NOT NULL,
  `ERROR_CODE` int(11) unsigned NOT NULL,
  `ERROR_DESCRIPTION` varchar(200) DEFAULT NULL,
  `PERCENTAGE` float DEFAULT NULL,
  `REQUEST_TIME` datetime DEFAULT NULL,
  `FINISH_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9578 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request`
--

LOCK TABLES `request` WRITE;
/*!40000 ALTER TABLE `request` DISABLE KEYS */;
INSERT INTO `request` VALUES (9553,'123|1565170224436',10,6996996,1,21,31,1,NULL,0,'2019-08-07 16:30:24','2019-08-07 16:30:38'),(9554,'123|1565170382558',10,9696996,1,22,31,1,NULL,0,'2019-08-07 16:33:02','2019-08-07 16:33:15'),(9555,'123|1565170664445',10,996699,1,21,31,1,NULL,0,'2019-08-07 16:37:44','2019-08-07 16:37:58'),(9558,'123|1565334113154',10,6699666,1,21,31,4,NULL,0,'2019-08-09 14:01:53','2019-08-09 14:02:20'),(9559,'123|1565406536238',10,69696969,1,21,31,5,NULL,0,'2019-08-10 10:08:56','2019-08-10 10:09:21'),(9560,'123|1565427542768',10,169961,1,21,32,2,NULL,100,'2019-08-10 15:59:02','2019-08-10 16:03:49'),(9577,'123|15654285500505555555555555555555555555555',10,234567,1,21,31,4,NULL,99,'2019-09-19 16:52:02','2019-09-20 17:17:17');
/*!40000 ALTER TABLE `request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `result` (
  `RESULT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `RESULT_TYPE` int(11) NOT NULL,
  `STEP_INDEX` int(11) DEFAULT NULL,
  `PROMOTION_BLOCK_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`RESULT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result`
--

LOCK TABLES `result` WRITE;
/*!40000 ALTER TABLE `result` DISABLE KEYS */;
INSERT INTO `result` VALUES (949,1,0,120),(950,3,0,NULL),(951,3,0,NULL),(952,3,0,NULL),(961,3,NULL,NULL),(962,4,0,120),(966,3,0,NULL),(968,1,0,120),(969,2,1,120),(970,3,0,NULL),(972,1,0,120),(973,2,1,120),(974,3,0,NULL),(975,3,0,NULL),(976,1,0,120),(977,2,0,120),(978,3,0,NULL),(980,1,0,121),(989,1,0,120),(990,1,0,121),(991,2,51,120),(992,1,0,121),(993,2,0,121),(994,3,NULL,NULL),(998,1,0,122),(999,3,0,NULL),(1000,4,0,122);
/*!40000 ALTER TABLE `result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result_table`
--

DROP TABLE IF EXISTS `result_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `result_table` (
  `RESULT_TABLE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(500) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `RESULT_TABLE_NAME` varchar(500) NOT NULL,
  `DEFAULT_RESULT_ID` bigint(20) DEFAULT NULL,
  `CONDITION_TABLE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`RESULT_TABLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=494 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result_table`
--

LOCK TABLES `result_table` WRITE;
/*!40000 ALTER TABLE `result_table` DISABLE KEYS */;
INSERT INTO `result_table` VALUES (481,'cccc',806,'ccccc',949,854),(482,'rstt1',282,'rstt1',975,855),(483,'',282,'result table 1812',992,854),(486,'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',282,'1912zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',989,854),(493,'',282,'9876',998,854);
/*!40000 ALTER TABLE `result_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result_table_rule_result_map`
--

DROP TABLE IF EXISTS `result_table_rule_result_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `result_table_rule_result_map` (
  `RESULT_TABLE_RULE_RESULT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `RESULT_TABLE_ID` bigint(20) NOT NULL,
  `RULE_RESULT_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`RESULT_TABLE_RULE_RESULT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=541 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result_table_rule_result_map`
--

LOCK TABLES `result_table_rule_result_map` WRITE;
/*!40000 ALTER TABLE `result_table_rule_result_map` DISABLE KEYS */;
INSERT INTO `result_table_rule_result_map` VALUES (503,481,503),(504,481,504),(505,481,505),(523,482,523),(524,482,524),(525,482,525),(533,486,533),(534,486,534),(535,483,535),(536,483,536),(539,493,539),(540,493,540);
/*!40000 ALTER TABLE `result_table_rule_result_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retry_cycle`
--

DROP TABLE IF EXISTS `retry_cycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `retry_cycle` (
  `ID` bigint(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `NOTE` varchar(255) DEFAULT NULL,
  `CYCLE_UNIT_ID` bigint(11) NOT NULL,
  `CYCLE_UNIT_TIMES` bigint(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retry_cycle`
--

LOCK TABLES `retry_cycle` WRITE;
/*!40000 ALTER TABLE `retry_cycle` DISABLE KEYS */;
INSERT INTO `retry_cycle` VALUES (1,'By Day','By Day',1,1),(2,'By Month','By Month',2,2);
/*!40000 ALTER TABLE `retry_cycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `row_ct`
--

DROP TABLE IF EXISTS `row_ct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `row_ct` (
  `ROW_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(200) NOT NULL,
  `ROW_INDEX` int(11) unsigned NOT NULL,
  PRIMARY KEY (`ROW_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=986 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `row_ct`
--

LOCK TABLES `row_ct` WRITE;
/*!40000 ALTER TABLE `row_ct` DISABLE KEYS */;
INSERT INTO `row_ct` VALUES (978,'1/2/1',1),(979,'1/2/2',2),(982,'1/1/1',1),(983,'2/1/2',2),(984,'1/0',1),(985,'2/1',2);
/*!40000 ALTER TABLE `row_ct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rule_result`
--

DROP TABLE IF EXISTS `rule_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rule_result` (
  `RULE_RESULT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `RESULT_ID` bigint(20) NOT NULL,
  `ROW_INDEX` int(11) NOT NULL,
  PRIMARY KEY (`RULE_RESULT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=541 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rule_result`
--

LOCK TABLES `rule_result` WRITE;
/*!40000 ALTER TABLE `rule_result` DISABLE KEYS */;
INSERT INTO `rule_result` VALUES (504,951,1),(505,952,2),(524,977,1),(525,978,2),(533,990,1),(534,991,2),(535,993,1),(536,994,2),(539,999,1),(540,1000,2);
/*!40000 ALTER TABLE `rule_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rule_result_table_map`
--

DROP TABLE IF EXISTS `rule_result_table_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rule_result_table_map` (
  `RULE_RESULT_TABLE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `RULE_ID` bigint(20) NOT NULL,
  `RESULT_TABLE_ID` bigint(20) NOT NULL,
  `RESULT_TABLE_INDEX` int(11) NOT NULL,
  PRIMARY KEY (`RULE_RESULT_TABLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=732 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rule_result_table_map`
--

LOCK TABLES `rule_result_table_map` WRITE;
/*!40000 ALTER TABLE `rule_result_table_map` DISABLE KEYS */;
INSERT INTO `rule_result_table_map` VALUES (714,435,481,1),(715,434,481,1),(716,437,482,1),(717,438,482,1),(724,440,483,1),(725,441,482,1),(726,442,483,1),(728,444,483,1),(729,439,483,1),(731,453,486,1);
/*!40000 ALTER TABLE `rule_result_table_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scenario_node`
--

DROP TABLE IF EXISTS `scenario_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `scenario_node` (
  `NODE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NODE_NAME` varchar(200) NOT NULL,
  `IS_ROOT` tinyint(1) DEFAULT NULL,
  `PARENT_ID` bigint(20) DEFAULT NULL,
  `SYNTAX` varchar(200) DEFAULT NULL,
  `OCS_BEHAVIOUR_ID` bigint(20) DEFAULT NULL,
  `USSD_SCENARIO_ID` bigint(20) DEFAULT NULL,
  `IS_BACK` int(11) DEFAULT NULL,
  `IS_BACK_ROOT` int(11) DEFAULT NULL,
  PRIMARY KEY (`NODE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=937 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scenario_node`
--

LOCK TABLES `scenario_node` WRITE;
/*!40000 ALTER TABLE `scenario_node` DISABLE KEYS */;
INSERT INTO `scenario_node` VALUES (765,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',1,0,'',0,652,0,0),(769,'node',1,0,'',0,653,0,0),(775,'node1',0,773,'34',0,653,0,0),(777,'node',0,769,'11',0,653,0,0),(785,'nodeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff',1,0,'',0,655,0,0),(797,'node',0,785,'11',0,655,0,0),(799,'node 1',0,797,'12',0,655,0,0),(801,'1',0,799,'1',0,655,0,0),(803,'23333333333333333333333333333333333333333333333333333333',0,801,'2',0,655,0,0),(817,'node123',1,0,'',0,657,0,0),(819,'node 12654',0,817,'12',0,657,0,0),(824,'12',0,819,'34',0,657,0,0),(826,'6777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777',0,824,'545',0,657,0,0),(844,'12',0,765,'12',0,652,0,0),(846,'13',0,844,'123',0,652,0,0),(848,'14',0,846,'112',0,652,0,0),(850,'87',0,848,'45',0,652,0,0),(852,'67',0,850,'44',0,652,0,0),(854,'675',0,852,'44',0,652,0,0),(856,'890',0,854,'45',0,652,0,0),(858,'345',0,856,'33',0,652,0,0),(860,'56',0,858,'44',0,652,0,0),(865,'node9',1,0,'',0,660,0,0),(867,'node10',0,865,'11',0,660,0,0),(869,'node2',0,867,'12',0,660,0,0),(874,'node11',0,867,'15',0,660,0,0),(876,'nodes',0,865,'18',0,660,0,0),(878,'node32',0,874,'21',0,660,0,0),(880,'node45',0,876,'55',0,660,0,0),(882,'node3',0,880,'22',0,660,0,0),(884,'129',0,882,'33',0,660,0,0),(896,'121231111',1,0,'',0,663,0,0),(900,'876987',1,0,'',0,664,0,0),(903,'trrew987',1,0,'',0,665,0,0),(907,'5589',1,0,'',0,666,0,0),(911,'r567865466tr',1,0,'',0,667,0,0),(915,'erytr',1,0,'',0,668,0,0),(918,'1',0,777,'1',0,653,0,0),(920,'2',0,918,'2',0,653,0,0),(924,'11',0,920,'11',0,653,0,0),(926,'node',1,0,'',0,669,0,0),(928,'node1',0,926,'12',0,669,0,0),(930,'node21',0,928,'13',0,669,0,0),(933,'node 56',0,930,'55',0,669,0,0),(936,'98',1,0,'11',0,670,0,0);
/*!40000 ALTER TABLE `scenario_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scenario_trigger`
--

DROP TABLE IF EXISTS `scenario_trigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `scenario_trigger` (
  `SCENARIO_TRIGGER_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TRIGGER_NAME` varchar(500) NOT NULL,
  `ALIAS` varchar(500) NOT NULL,
  `DESCRIPTION` varchar(500) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `USSD_SCENARIO_ID` bigint(20) NOT NULL,
  `IS_RETRY` tinyint(11) NOT NULL,
  `CYCLE_ID` int(11) NOT NULL,
  PRIMARY KEY (`SCENARIO_TRIGGER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=602 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scenario_trigger`
--

LOCK TABLES `scenario_trigger` WRITE;
/*!40000 ALTER TABLE `scenario_trigger` DISABLE KEYS */;
INSERT INTO `scenario_trigger` VALUES (595,'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx','1111111111111111111111111111111111111111111111111111111','zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',169,652,0,0),(601,'9855','98','',169,670,0,0);
/*!40000 ALTER TABLE `scenario_trigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segment`
--

DROP TABLE IF EXISTS `segment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `segment` (
  `SEGMENT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SEGMENT_NAME` varchar(200) NOT NULL,
  `PARENT_ID` bigint(20) DEFAULT NULL,
  `SEGMENT_SIZE` bigint(20) NOT NULL,
  `PATH` varchar(200) NOT NULL,
  `CONTROL_PERCENTAGE` int(11) DEFAULT NULL,
  `APPLICATION_ID` int(11) DEFAULT NULL,
  `EXP_DATE` datetime DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `IS_ACTIVE` tinyint(1) DEFAULT NULL,
  `EFF_DATE` datetime DEFAULT NULL,
  `FAILED_PATH` varchar(500) DEFAULT NULL,
  `TEMP_PATH` varchar(500) DEFAULT NULL,
  `HIST_PATH` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`SEGMENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segment`
--

LOCK TABLES `segment` WRITE;
/*!40000 ALTER TABLE `segment` DISABLE KEYS */;
INSERT INTO `segment` VALUES (1,'segment 1',1,1234,'1',NULL,NULL,'2020-01-01 22:14:41','11111111111111111',10,1,'2019-08-25 22:15:08','1','1','1'),(2,'segment 2',2,1234,'2',NULL,NULL,'2020-01-01 22:14:41',NULL,10,1,'2019-08-25 22:15:08','2','2','2'),(3,'segment 3',NULL,1234,'3',NULL,NULL,'2020-01-01 22:14:41',NULL,10,1,'2019-08-25 22:15:08','3','3','3'),(4,'segment 4',NULL,4,'4',NULL,NULL,'2020-01-01 22:14:41',NULL,10,1,'2019-08-25 22:15:08','4','4','4'),(5,'segment 5',NULL,5,'5',NULL,NULL,'2020-01-01 22:14:41',NULL,16,1,'2019-08-25 22:15:08','5','5','5'),(6,'segment 6',NULL,6,'6',NULL,NULL,'2020-01-01 22:14:41',NULL,16,1,'2019-08-25 22:15:08','6','6','6'),(7,'segment 7',NULL,7,'7',NULL,NULL,'2020-01-01 22:14:41',NULL,10,1,'2019-08-25 22:15:08','7','7','7'),(8,'segment 81111111111111111111111111111111111111111111111111111112',NULL,5,'8',NULL,NULL,'2020-01-01 22:14:41',NULL,10,1,'2019-08-25 22:15:08','8','8','8'),(9,'1',NULL,9,'9',NULL,NULL,'2020-01-01 22:14:41',NULL,10,1,'2019-08-25 22:15:08','9','9','9'),(10,'2',NULL,10,'10',NULL,NULL,'2020-01-01 22:14:41',NULL,10,1,'2019-08-25 22:15:08','10','10','10'),(11,'3',NULL,11,'11',NULL,NULL,'2020-01-01 22:14:41',NULL,10,1,'2019-08-25 22:15:08','11','11','11'),(12,'4',NULL,12,'12',NULL,NULL,'2020-01-01 22:14:41',NULL,10,1,'2019-08-25 22:15:08','12','12','12'),(13,'5',NULL,12,'13',NULL,NULL,'2020-01-01 22:14:41',NULL,10,1,'2019-08-25 22:15:08','13','13','13'),(14,'segment cấp 1',NULL,13,'/u01/file/segment/current_path/ha_ha.txt',NULL,NULL,'2020-01-01 22:14:41',NULL,10,1,'2019-08-25 22:15:08','/u01/file/segment/fail_path/ha_ha1.txt','/u01/file/segment/temp_path/ha_haha.txt','/u01/file/segment/history_path/ha_ha2.txt');
/*!40000 ALTER TABLE `segment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specialprom`
--

DROP TABLE IF EXISTS `specialprom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `specialprom` (
  `SPECIAL_PROM_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SPECIAL_PROM_NAME` varchar(200) NOT NULL,
  `CATEGORY_ID` int(10) unsigned DEFAULT NULL,
  `PATH` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `HIST_PATH` varchar(200) NOT NULL,
  `FAILED_PATH` varchar(200) NOT NULL,
  `TEMP_PATH` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`SPECIAL_PROM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specialprom`
--

LOCK TABLES `specialprom` WRITE;
/*!40000 ALTER TABLE `specialprom` DISABLE KEYS */;
INSERT INTO `specialprom` VALUES (1,'Special offer 1',60,'1','1','1','1','1'),(2,'Special offer 2',60,'2','2','2','2','2'),(3,'        Spe   cial    offer 3',60,'3','3','3','3','3'),(4,'Condition Table',60,'4','4','4','4','4'),(5,'!@#$^',60,'5','5','5','5','5'),(6,'special offer cấp 1',60,'6','6','6','6','6');
/*!40000 ALTER TABLE `specialprom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statistic_cycle`
--

DROP TABLE IF EXISTS `statistic_cycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `statistic_cycle` (
  `ID` bigint(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(1000) NOT NULL,
  `CYCLE_UNIT_ID` bigint(11) NOT NULL,
  `CYCLE_UNIT_TIMES` bigint(11) NOT NULL,
  `MAX_NOTIFY` bigint(11) NOT NULL,
  `CATEGORY_ID` bigint(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statistic_cycle`
--

LOCK TABLES `statistic_cycle` WRITE;
/*!40000 ALTER TABLE `statistic_cycle` DISABLE KEYS */;
INSERT INTO `statistic_cycle` VALUES (203,'statictis123xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',1,1,1,482),(204,'statictzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',2,2,2,482),(217,'4',1,1,1,482),(218,'5',2,2,2,482),(219,'6',1,1,1,482),(220,'7',2,2,2,482),(221,'8',1,1,1,482),(222,'9',2,2,2,482),(223,'10',1,1,1,482),(224,'11',2,2,2,482);
/*!40000 ALTER TABLE `statistic_cycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_param`
--

DROP TABLE IF EXISTS `template_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `template_param` (
  `TEMPLATE_PARAM_ID` bigint(20) NOT NULL,
  `TEMPLATE_PARAM_NAME` varchar(200) NOT NULL,
  `PARAM_TYPE` int(11) NOT NULL,
  PRIMARY KEY (`TEMPLATE_PARAM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_param`
--

LOCK TABLES `template_param` WRITE;
/*!40000 ALTER TABLE `template_param` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_param` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ussd_scenario`
--

DROP TABLE IF EXISTS `ussd_scenario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ussd_scenario` (
  `SCENARIO_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SCENARIO_NAME` varchar(1000) NOT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`SCENARIO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=671 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ussd_scenario`
--

LOCK TABLES `ussd_scenario` WRITE;
/*!40000 ALTER TABLE `ussd_scenario` DISABLE KEYS */;
INSERT INTO `ussd_scenario` VALUES (652,'xxx123 3','xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',157),(653,'1712 123478','1',157),(655,'scenario 1712123','',157),(657,'1912','',157),(660,'2012','',157),(663,'20122019','',157),(664,'87654','',157),(665,'98765','',157),(666,'7654','',157),(667,'4567876875678','rty',157),(668,'4567iuyt','',157),(669,'201212112s','',157),(670,'9880','',157);
/*!40000 ALTER TABLE `ussd_scenario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zone`
--

DROP TABLE IF EXISTS `zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `zone` (
  `ZONE_ID` bigint(20) NOT NULL,
  `ZONE_MAP_ID` bigint(20) NOT NULL,
  `ZONE_NAME` varchar(200) NOT NULL,
  `ZONE_CODE` varchar(200) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `IS_MATCHING` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ZONE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zone`
--

LOCK TABLES `zone` WRITE;
/*!40000 ALTER TABLE `zone` DISABLE KEYS */;
INSERT INTO `zone` VALUES (1,1,'ZONE campaign 1058xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx','1',NULL,257,1),(2,2,'ZONE campaign 1059','2',NULL,257,1),(3,3,'ZONE campaign 1060','3',NULL,257,0),(4,4,'ZONE campaign 1061','4',NULL,257,1),(5,5,'ZONE campaign 1062','5',NULL,257,1),(6,6,'ZONE campaign 1063','6',NULL,257,1),(7,6,'ZONE campaign 1063','7',NULL,257,1),(8,7,'ZONE campaign 1064','6',NULL,257,0),(9,7,'ZONE campaign 1064','7',NULL,257,0),(10,8,'ZONE campaign 1067','8',NULL,257,1),(11,8,'ZONE campaign 1067','8',NULL,257,1),(12,8,'ZONE campaign 1067','8',NULL,257,1),(13,9,'ZONE campaign 1068','8',NULL,257,1),(14,9,'ZONE campaign 1068','8',NULL,257,1),(15,9,'ZONE campaign 1068','8',NULL,257,1),(16,10,'ZONE campaign 1069','8',NULL,257,1),(17,11,'ZONE campaign 1069','8',NULL,257,1);
/*!40000 ALTER TABLE `zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zone_data`
--

DROP TABLE IF EXISTS `zone_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `zone_data` (
  `ZONE_DATA_ID` bigint(20) NOT NULL,
  `ZONE_DATA_VALUE` varchar(200) NOT NULL,
  `ZONE_ID` bigint(20) NOT NULL,
  `UPDATE_DATE` datetime NOT NULL,
  PRIMARY KEY (`ZONE_DATA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zone_data`
--

LOCK TABLES `zone_data` WRITE;
/*!40000 ALTER TABLE `zone_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `zone_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zone_map`
--

DROP TABLE IF EXISTS `zone_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `zone_map` (
  `ZONE_MAP_ID` bigint(20) NOT NULL,
  `ZONE_MAP_NAME` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ZONE_MAP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zone_map`
--

LOCK TABLES `zone_map` WRITE;
/*!40000 ALTER TABLE `zone_map` DISABLE KEYS */;
INSERT INTO `zone_map` VALUES (1,'ZONE MAP CAMPAING 1058',NULL,258),(2,'ZONE MAP CAMPAING 1059',NULL,258),(3,'ZONE MAP CAMPAING 1060',NULL,258),(4,'ZONE MAP CAMPAING 1061',NULL,258),(5,'ZONE MAP CAMPAING 1062',NULL,258),(6,'ZONE MAP CAMPAING 1063',NULL,258),(7,'ZONE MAP CAMPAING 1064',NULL,258),(8,'ZONE MAP CAMPAING 1067',NULL,258),(9,'ZONE MAP CAMPAING 1068',NULL,258),(10,'ZONE MAP CAMPAING 1069',NULL,258),(11,'ZONE MAP CAMPAING 1069',NULL,258);
/*!40000 ALTER TABLE `zone_map` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-21  7:40:29
