package vn.viettel.campaign.entities;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "language")
@NoArgsConstructor
public class Language implements Comparable<Language>{

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "LANGUAGE_ID")
  private Long languageId;
  @Column(name = "LANGUAGE_NAME")
  private String languageName;
  @Column(name = "DESCRIPTION")
  private String description;
  @Column(name = "CATEGORY_ID")
  private long categoryId;

  public Language(Long languageId) {
    this.languageId = languageId;
  }

  public Long getLanguageId() {
    return languageId;
  }

  public void setLanguageId(Long languageId) {
    this.languageId = languageId;
  }


  public String getLanguageName() {
    return languageName;
  }

  public void setLanguageName(String languageName) {
    this.languageName = languageName;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(long categoryId) {
    this.categoryId = categoryId;
  }

  @Override
  public int compareTo(Language o) {
    if (getLanguageName() == null || o.getLanguageName() == null) {
      return 0;
    }
    System.out.println("getLanguageName():" + getLanguageName());
    System.out.println("sc:" + o.getLanguageName());
    return getLanguageName().toUpperCase().compareTo(o.getLanguageName().toUpperCase());
  }
}
