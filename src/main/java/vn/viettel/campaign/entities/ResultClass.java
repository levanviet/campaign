/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ABC
 */
@Entity
@Table(name = "result_class")
public class ResultClass implements Serializable  {
     private static final Long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CLASS_ID")
    private Long classId;
    @Basic(optional = false)
    @Column(name = "OWNER_ID")
    private Long ownerId;
    @Column(name = "CAMPAIGN_EVALUATION_INFO_ID")
    private Long campaignEvaluationInfoId;
    @Column(name = "CLASS_NAME")
    private String className;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CLASS_LEVEL")
    private Integer classLevel;

    public ResultClass() {
    }

    public ResultClass(Long classId, Long ownerId, Long campaignEvaluationInfoId, String className, String description, Integer classLevel) {
        this.classId = classId;
        this.ownerId = ownerId;
        this.campaignEvaluationInfoId = campaignEvaluationInfoId;
        this.className = className;
        this.description = description;
        this.classLevel = classLevel;
    }

    public Long getClassId() {
        return classId;
    }

    public void setClassId(Long classId) {
        this.classId = classId;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Long getCampaignEvaluationInfoId() {
        return campaignEvaluationInfoId;
    }

    public void setCampaignEvaluationInfoId(Long campaignEvaluationInfoId) {
        this.campaignEvaluationInfoId = campaignEvaluationInfoId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getClassLevel() {
        return classLevel;
    }

    public void setClassLevel(Integer classLevel) {
        this.classLevel = classLevel;
    }
    
    
}
