/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author SON
 */
@Entity
@Table(name = "kpi_processor")
public class KpiProcessor extends BaseCategory implements Serializable,TreeItemBase {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PROCESSOR_ID")
    private Long processorId;
    @Basic(optional = false)
    @Column(name = "PROCESSOR_NAME")
    private String processorName;
    @Basic(optional = false)
    @Column(name = "INPUT_FIELD")
    private String inputField;
    @Basic(optional = false)
    @Column(name = "CRITERIA_NAME")
    private String criteriaName;
    @Basic(optional = false)
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Transient
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    

    @Transient
    private String cloneInputFiled;

    public String getCloneInputFiled() {
        return cloneInputFiled;
    }

    public void setCloneInputFiled(String cloneInputFiled) {
        this.cloneInputFiled = cloneInputFiled;
    }

    public KpiProcessor() {
    }

    public KpiProcessor(Long processorId) {
        this.processorId = processorId;
    }

    public KpiProcessor(Long processorId, String processorName, String inputField, String criteriaName, String description, long categoryId) {
        this.processorId = processorId;
        this.processorName = processorName;
        this.inputField = inputField;
        this.criteriaName = criteriaName;
        this.description = description;
        this.categoryId = categoryId;
    }

    public Long getProcessorId() {
        return processorId;
    }

    public void setProcessorId(Long processorId) {
        this.processorId = processorId;
    }

    public String getProcessorName() {
        return processorName;
    }

    public void setProcessorName(String processorName) {
        this.processorName = processorName;
    }

    public String getInputField() {
        return inputField;
    }

    public void setInputField(String inputField) {
        this.inputField = inputField;
    }

    public String getCriteriaName() {
        return criteriaName;
    }

    public void setCriteriaName(String criteriaName) {
        this.criteriaName = criteriaName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Long getCategoryId() {
        return categoryId;
    }

    @Override
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (processorId != null ? processorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KpiProcessor)) {
            return false;
        }
        KpiProcessor other = (KpiProcessor) object;
        if ((this.processorId == null && other.processorId != null) || (this.processorId != null && !this.processorId.equals(other.processorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vn.viettel.campaign.entities.KpiProcessor[ processorId=" + processorId + " ]";
    }

    public String getFilter() {
        return getProcessorName();
    }

     @Override
    public long getParentId() {
        return this.getCategoryId();
    }

    @Override
    public void setParentId(Long id) {
        setCategoryId(id);
    }

    @Override
    public String getTreeType() {
        return "object";
    }

    @Override
    public String getName() {
        return this.processorName;
    }

    @Override
    public Class getParentType() {
        return Category.class;
    }

}
