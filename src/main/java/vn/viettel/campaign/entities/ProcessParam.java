package vn.viettel.campaign.entities;

import vn.viettel.campaign.common.DataUtil;

import javax.persistence.*;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "process_param")
public class ProcessParam {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PROCESS_PARAM_ID")
	private long processParamId;
	@Column(name = "PARAM_INDEX")
	private Long paramIndex;
	@Column(name = "CONFIG_INPUT")
	private String configInput;
	@Transient
	private String value;
	@Transient
	private Long parameterId;
	@Transient
	private Long parameterId2;
	@Transient
	private String value2;
	@Transient
	private Integer type;
	@Transient
	private Integer priority;
	@Transient
	boolean param = false;
	@Transient
	boolean param2 = false;
	@Transient
	private Long day;
	@Transient
	private Long hour;
	@Transient
	private Long min;
	@Transient
	private Long sec;
	@Transient
	private Long startType;
	@Transient
	private String startVal;
	@Transient
	private Long endType;
	@Transient
	private String endVal;
	@Transient
	private Date startValue;
	@Transient
	private Date endValue;
	@Transient
	private Long zoneType;
	@Transient
	private Long zoneId;
	@Transient
	private String zoneName;

	@Transient
	private String valueColor;

	@Transient
	private String valueName;

	@Transient
	private String zoneTypeName;

	public String getZoneTypeName() {
		return zoneTypeName;
	}

	public void setZoneTypeName(String zoneTypeName) {
		this.zoneTypeName = zoneTypeName;
	}

	public Long getZoneType() {
		return zoneType;
	}

	public void setZoneType(Long zoneType) {
		this.zoneType = zoneType;
	}

	public Long getZoneId() {
		return zoneId;
	}

	public void setZoneId(Long zoneId) {
		this.zoneId = zoneId;
	}

	public Long getDay() {
		return day;
	}

	public void setDay(Long day) {
		this.day = day;
	}

	public Long getHour() {
		return hour;
	}

	public void setHour(Long hour) {
		this.hour = hour;
	}

	public Long getMin() {
		return min;
	}

	public void setMin(Long min) {
		this.min = min;
	}

	public Long getSec() {
		return sec;
	}

	public void setSec(Long sec) {
		this.sec = sec;
	}

	public long getProcessParamId() {
		return processParamId;
	}

	public void setProcessParamId(long processParamId) {
		this.processParamId = processParamId;
	}


	public Long getParamIndex() {
		return paramIndex;
	}

	public void setParamIndex(Long paramIndex) {
		this.paramIndex = paramIndex;
	}


	public void calculateConfigInput() {
		this.configInput = "inputStr:" + (this.value == null ? "" : this.value.trim()) + ";type:" + (this.type == null ? "" : this.type) + ";priority:" + (this.priority == null ? "" : this.priority);
	}
	public void calculateConfigInputPPUString() {
		this.configInput = "inputStr:" + (this.value== null ? "" : this.value.trim()) + ";CompareType:" + (this.type == null ? "" : this.type) + ";priority:" + (this.priority == null ? "" : this.priority);
	}

	public void calculateConfigInputPPUNumber(boolean usingParameter) {
		String tempValue = this.value == null ? "" : this.value;
		if (isParam()){
			tempValue = this.parameterId == null ? "" : this.parameterId +"";
		}
		if (!usingParameter) {
			this.configInput = "inputNumber:" + tempValue +
					";comparisionType:" + (this.type == null ? "" : this.type) + ";priority:" + (this.priority == null ? "" : this.priority);
		} else {
			this.configInput = "inputNumber:" + tempValue +	";isUseParameter:" + this.param +
			";comparisionType:" + (this.type == null ? "" : this.type) + ";priority:" + (this.priority == null ? "" : this.priority);
		}
	}
	public void calculateConfigInputPPUNumberRange(boolean usingParameter) {
		String tempValue = this.value == null ? "" : this.value;
		String tempValue2 = this.value2 == null ? "" : this.value2;
		if (isParam()){
			tempValue = this.parameterId == null ? "" : this.parameterId +"";
		}
		if (isParam2()){
			tempValue2 = this.parameterId2 == null ? "" : this.parameterId2 +"";
		}

		if (!usingParameter) {
			this.configInput = "start:" + tempValue + ";end:" + tempValue2 + ";priority:" +
					(this.priority == null ? "" : this.priority);
		} else {
			this.configInput = "start:" + tempValue +	";startIsParameter:" + this.param +
					";end:" + tempValue2 + ";endIsParameter:"+this.param2 +";priority:" + (this.priority == null ? "" : this.priority);
		}
	}

	public void calculateConfigInputTime() {
		this.configInput = "day:" + (this.day == null ? "" : this.day) + ";hour:" + (this.hour == null ? "" : this.hour) + ";min:" + (this.min == null ? "" : this.min) + ";sec:" + (this.sec == null ? "" : this.sec);
	}

	public void setConfigInput(String configInput) {
		this.configInput = configInput;
	}

	public String getConfigInput() {
		return configInput;
	}

	public String getValue() {
		if (DataUtil.isStringNullOrEmpty(value)) {
			if (configInput == null || configInput.equalsIgnoreCase("")) {
				return null;
			}
			String[] data = configInput.split(";");
			this.value = data[0].split(":").length == 2 ? data[0].split(":")[1] : null;
		}
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getType() {
		if (type == null ) {
			if (configInput == null || configInput.equalsIgnoreCase("")) {
				return null;
			}
			String[] data = configInput.split(";");
			String temp = data[1].split(":").length == 2 ? data[1].split(":")[1] : null;
			if (data.length == 4){
				temp = data[2].split(":").length == 2 ? data[2].split(":")[1] : null;
			}
			if (temp == null) {
				return null;
			}
			this.type = Integer.parseInt(temp);
		}
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getPriority() {
		if (priority == null) {
			if (configInput == null || configInput.equalsIgnoreCase("")) {
				return null;
			}
			String[] data = configInput.split(";");
			String temp = data[2].split(":").length == 2 ? data[2].split(":")[1] : null;
			if (data.length == 4){
				temp = data[3].split(":").length == 2 ? data[3].split(":")[1] : null;
			}
			if (data.length == 5){
				temp = data[4].split(":").length == 2 ? data[4].split(":")[1] : null;
			}
			if (temp == null) {
				return null;
			}
			this.priority = Integer.parseInt(temp);
		}
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

    public boolean isParam() {
        return param;
    }

    public boolean getParam() {
        String[] data = configInput.split(";");
        this.param =  Boolean.valueOf(data[1].split(":")[1]);
        return this.param;
    }

	public void setParam(boolean param) {
		this.param = param;
	}

	public Long getStartType() {
		return startType;
	}

	public void setStartType(Long startType) {
		this.startType = startType;
	}

	public String getStartVal() {
		return startVal;
	}

	public void setStartVal(String startVal) {
		this.startVal = startVal;
	}

	public Long getEndType() {
		return endType;
	}

	public void setEndType(Long endType) {
		this.endType = endType;
	}

	public String getEndVal() {
		return endVal;
	}

	public void setEndVal(String endVal) {
		this.endVal = endVal;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

	public boolean isParam2() {
		return param2;
	}

	public void setParam2(boolean param2) {
		this.param2 = param2;
	}

    public Date getStartValue() {
        return startValue;
    }

    public void setStartValue(Date startValue) {
        this.startValue = startValue;
    }

    public Date getEndValue() {
        return endValue;
    }

    public void setEndValue(Date endValue) {
        this.endValue = endValue;
    }

	public String getValueColor() {
		return valueColor;
	}

	public void setValueColor(String valueColor) {
		this.valueColor = valueColor;
	}

	public String getZoneName() {
		return zoneName + " (" + zoneId + ")";
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getValueName() {
		return valueName;
	}

	public void setValueName(String valueName) {
		this.valueName = valueName;
	}

	public Long getParameterId() {
		if (this.parameterId == null){
			return  0L ;
		}
		return this.parameterId;
	}

	public void setParameterId(Long parameterId) {
		this.parameterId = parameterId;
	}

	public void setParameterId2(Long parameterId) {
		this.parameterId2 = parameterId;
	}
	public Long getParameterId2() {
		if (this.parameterId2 == null){
			return  0L ;
		}
		return this.parameterId2;
	}

}
