package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "rule_result_table_map")
public class RuleResultTableMap {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "RULE_RESULT_TABLE_ID")
  private long ruleResultTableId;
  @Column(name = "RULE_ID")
  private long ruleId;
  @Column(name = "RESULT_TABLE_ID")
  private long resultTableId;
  @Column(name = "RESULT_TABLE_INDEX")
  private long resultTableIndex;


  public long getRuleResultTableId() {
    return ruleResultTableId;
  }

  public void setRuleResultTableId(long ruleResultTableId) {
    this.ruleResultTableId = ruleResultTableId;
  }


  public long getRuleId() {
    return ruleId;
  }

  public void setRuleId(long ruleId) {
    this.ruleId = ruleId;
  }


  public long getResultTableId() {
    return resultTableId;
  }

  public void setResultTableId(long resultTableId) {
    this.resultTableId = resultTableId;
  }


  public long getResultTableIndex() {
    return resultTableIndex;
  }

  public void setResultTableIndex(long resultTableIndex) {
    this.resultTableIndex = resultTableIndex;
  }

}
