/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author SON
 */
@Entity
@Table(name = "pre_process_unit")
public class PpuEvaluation extends BaseCategory implements Serializable, TreeItemBase {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PRE_PROCESS_ID")
    private Long preProcessId;
    @Basic(optional = false)
    @Column(name = "PRE_PROCESS_NAME")
    private String preProcessName;
    @Basic(optional = false)
    @Column(name = "PRE_PROCESS_TYPE")
    private int preProcessType;
    @Basic(optional = false)
    @Column(name = "DEFAULT_VALUE")
    private int defaultValue;
    @Basic(optional = false)
    @Column(name = "INPUT_FIELDS")
    private String inputFields;
    @Column(name = "SPECIAL_FIELDS")
    private String specialFields;
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @Column(name = "CATEGORY_ID")
    private long categoryId;
    @Column(name = "TYPE")
    private Integer type;
    @Column(name = "CRITERIA_NAME")
    private String criteriaName;

    
    @Transient
    private String cloneInputFiled;

    public String getCloneInputFiled() {
        return cloneInputFiled;
    }

    public void setCloneInputFiled(String cloneInputFiled) {
        this.cloneInputFiled = cloneInputFiled;
    }
    public PpuEvaluation() {
    }

    public PpuEvaluation(Long preProcessId) {
        this.preProcessId = preProcessId;
    }

    public PpuEvaluation(Long preProcessId, String preProcessName, int preProcessType, int defaultValue, String inputFields, long categoryId) {
        this.preProcessId = preProcessId;
        this.preProcessName = preProcessName;
        this.preProcessType = preProcessType;
        this.defaultValue = defaultValue;
        this.inputFields = inputFields;
        this.categoryId = categoryId;
    }

    public Long getPreProcessId() {
        return preProcessId;
    }

    public void setPreProcessId(Long preProcessId) {
        this.preProcessId = preProcessId;
    }

    public String getPreProcessName() {
        return preProcessName;
    }

    public void setPreProcessName(String preProcessName) {
        this.preProcessName = preProcessName;
    }

    public int getPreProcessType() {
        return preProcessType;
    }

    public void setPreProcessType(int preProcessType) {
        this.preProcessType = preProcessType;
    }

    public int getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(int defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getInputFields() {
        return inputFields;
    }

    public void setInputFields(String inputFields) {
        this.inputFields = inputFields;
    }

    public String getSpecialFields() {
        return specialFields;
    }

    public void setSpecialFields(String specialFields) {
        this.specialFields = specialFields;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCriteriaName() {
        return criteriaName;
    }

    public void setCriteriaName(String criteriaName) {
        this.criteriaName = criteriaName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (preProcessId != null ? preProcessId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PpuEvaluation)) {
            return false;
        }
        PpuEvaluation other = (PpuEvaluation) object;
        if ((this.preProcessId == null && other.preProcessId != null) || (this.preProcessId != null && !this.preProcessId.equals(other.preProcessId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vn.viettel.campaign.entities.PpuEvaluation[ preProcessId=" + preProcessId + " ]";
    }

    @Override
    public void setCategoryId(Long category_id) {
        this.categoryId = category_id;
    }

    public String getFilter() {
        return getPreProcessName();
    }

    @Override
    public long getParentId() {
        return this.getCategoryId();
    }

    @Override
    public void setParentId(Long id) {
        setCategoryId(id);
    }

    @Override
    public String getTreeType() {
        return "object";
    }

    @Override
    public String getName() {
        return this.preProcessName;
    }

    @Override
    public Class getParentType() {
        return Category.class;
    }

}
