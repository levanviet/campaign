package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "notify_values")
public class NotifyValues {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NOTIFY_VALUE_ID")
    private long notifyValueId;
    @Column(name = "TRIGGER_NOTIFY_ID")
    private Long triggerNotifyId;
    @Column(name = "TEMPLATE_PARAM_ID")
    private Long templateParamId;
    @Column(name = "SOURCE_TYPE")
    private Long sourceType;
    @Column(name = "DATA_TYPE")
    private Long dataType;
    @Column(name = "VALUE")
    private String value;
    @Column(name = "PARAM_STRING")
    private String paramString;

    public long getNotifyValueId() {
        return notifyValueId;
    }

    public void setNotifyValueId(long notifyValueId) {
        this.notifyValueId = notifyValueId;
    }

    public Long getTriggerNotifyId() {
        return triggerNotifyId;
    }

    public void setTriggerNotifyId(Long triggerNotifyId) {
        this.triggerNotifyId = triggerNotifyId;
    }

    public Long getTemplateParamId() {
        return templateParamId;
    }

    public void setTemplateParamId(Long templateParamId) {
        this.templateParamId = templateParamId;
    }

    public Long getSourceType() {
        return sourceType;
    }

    public void setSourceType(Long sourceType) {
        this.sourceType = sourceType;
    }

    public Long getDataType() {
        return dataType;
    }

    public void setDataType(Long dataType) {
        this.dataType = dataType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getParamString() {
        return paramString;
    }

    public void setParamString(String paramString) {
        this.paramString = paramString;
    }
}
