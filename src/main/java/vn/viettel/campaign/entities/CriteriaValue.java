/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SON
 */
@Entity
@Table(name = "criteria_value")
public class CriteriaValue extends Object implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CRITERIA_VALUE_ID")
    private Long criteriaValueId;
    @Basic(optional = false)
    @Column(name = "CRITERIA_VALUE_NAME")
    private String criteriaValueName;
    @Column(name = "FUZZY_FUNCTION_TYPE")
    private Integer fuzzyFunctionType;
    @Column(name = "MEMBERSHIP_FUNCTION")
    private String membershipFunction;
    @Column(name = "DESCRIPTION")
    private String description;

    public CriteriaValue() {
    }

    public CriteriaValue(Long criteriaValueId) {
        this.criteriaValueId = criteriaValueId;
    }

    public CriteriaValue(Long criteriaValueId, String criteriaValueName) {
        this.criteriaValueId = criteriaValueId;
        this.criteriaValueName = criteriaValueName;
    }

    public Long getCriteriaValueId() {
        return criteriaValueId;
    }

    public void setCriteriaValueId(Long criteriaValueId) {
        this.criteriaValueId = criteriaValueId;
    }

    public String getCriteriaValueName() {
        return criteriaValueName;
    }

    public void setCriteriaValueName(String criteriaValueName) {
        this.criteriaValueName = criteriaValueName;
    }

    public Integer getFuzzyFunctionType() {
        return fuzzyFunctionType;
    }

    public void setFuzzyFunctionType(Integer fuzzyFunctionType) {
        this.fuzzyFunctionType = fuzzyFunctionType;
    }

    public String getMembershipFunction() {
        return membershipFunction;
    }

    public void setMembershipFunction(String membershipFunction) {
        this.membershipFunction = membershipFunction;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (criteriaValueId != null ? criteriaValueId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CriteriaValue)) {
            return false;
        }
        CriteriaValue other = (CriteriaValue) object;
        if ((this.criteriaValueId == null && other.criteriaValueId != null) || (this.criteriaValueId != null && !this.criteriaValueId.equals(other.criteriaValueId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vn.viettel.campaign.entities.CriteriaValue[ criteriaValueId=" + criteriaValueId + " ]";
    }
    
}
