/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author SON
 */
@Entity
@Table(name = "input_field_variable_map")
public class InputFieldVariableMap implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "OWNER_ID")
    private long ownerId;
    @Basic(optional = false)
    @Column(name = "TYPE")
    private int type;
    @Basic(optional = false)
    @Column(name = "VARIABLE_NAME")
    private String variableName;
    @Basic(optional = false)
    @Column(name = "VARIABLE_VALUE")
    private String variableValue;

    
    @Transient
    private int inputMode;
    
    @Transient
    private int isInputMode;

    public int getIsInputMode() {
        return isInputMode;
    }

    public void setIsInputMode(int isInputMode) {
        this.isInputMode = isInputMode;
    }
    
    public int getInputMode() {
        if(variableValue.equals("")){
            return 1;
        }
        
        return Integer.parseInt(this.variableValue.substring(5, 6));
    }

    public void setInputMode(int inputMode) {
        this.inputMode = inputMode;
    }
    public InputFieldVariableMap() {
    }

    public InputFieldVariableMap(Long id) {
        this.id = id;
    }

    public InputFieldVariableMap(Long id, long ownerId, int type, String variableName, String variableValue) {
        this.id = id;
        this.ownerId = ownerId;
        this.type = type;
        this.variableName = variableName;
        this.variableValue = variableValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getVariableName() {
        return variableName;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public String getVariableValue() {
        return variableValue;
    }

    public void setVariableValue(String variableValue) {
        this.variableValue = variableValue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InputFieldVariableMap)) {
            return false;
        }
        InputFieldVariableMap other = (InputFieldVariableMap) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vn.viettel.campaign.entities.InputFieldVariableMap[ id=" + id + " ]";
    }
    
}
