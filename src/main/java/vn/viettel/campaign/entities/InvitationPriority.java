package vn.viettel.campaign.entities;
import vn.viettel.campaign.common.NotifyType;

import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "invitation_priority")
public class InvitationPriority {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "INVITATION_PRIORITY_ID")
  private long invitationPriorityId;
  @Column(name = "CAMPAIGN_ID")
  private Long campaignId;
  @Column(name = "SEGMENT_ID")
  private Long segmentId;
  @Column(name = "NOTIFY_ID")
  private Long notifyId;
  @Column(name = "NOTIFY_TYPE")
  private Long notifyType;
  @Column(name = "PRIORITY")
  private Long priority;

  @Transient
  private String segmentName;

  @Transient
  private Long invite;

  @Transient
  private String triggerName;

  @Transient
  private String notifyName;
  @Transient
  private String objectKey;

  public long getInvitationPriorityId() {
    return invitationPriorityId;
  }

  public void setInvitationPriorityId(long invitationPriorityId) {
    this.invitationPriorityId = invitationPriorityId;
  }


  public Long getCampaignId() {
    return campaignId;
  }

  public void setCampaignId(Long campaignId) {
    this.campaignId = campaignId;
  }


  public Long getSegmentId() {
    return segmentId;
  }

  public void setSegmentId(Long segmentId) {
    this.segmentId = segmentId;
  }


  public Long getNotifyId() {
    return notifyId;
  }

  public void setNotifyId(Long notifyId) {
    this.notifyId = notifyId;
  }


  public Long getNotifyType() {
    return notifyType;
  }

  public void setNotifyType(Long notifyType) {
    this.notifyType = notifyType;
  }


  public Long getPriority() {
    return priority;
  }

  public void setPriority(Long priority) {
    this.priority = priority;
  }

  public String getSegmentName() {
    return segmentName;
  }

  public void setSegmentName(String segmentName) {
    this.segmentName = segmentName;
  }

  public String getTriggerName() {
    return triggerName;
  }

  public void setTriggerName(String triggerName) {
    this.triggerName = triggerName;
  }

  public String getNotifyName() {
    return NotifyType.asMap().get(this.getNotifyType());
  }

  public void setNotifyName(String notifyName) {
    this.notifyName = notifyName;
  }

  public String getObjectKey() {
    return this.notifyId + ""+this.segmentId;
  }

  public void setObjectKey(String objectKey) {
    this.objectKey = objectKey;
  }

  public Long getInvite() {
    return invite;
  }

  public void setInvite(Long invite) {
    this.invite = invite;
  }
}
