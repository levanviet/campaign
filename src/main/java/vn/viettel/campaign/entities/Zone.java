package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "zone")
public class Zone {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ZONE_ID")
  private long zoneId;
  @Column(name = "ZONE_MAP_ID")
  private Long zoneMapId;
  @Column(name = "ZONE_NAME")
  private String zoneName;
  @Column(name = "ZONE_CODE")
  private String zoneCode;
  @Column(name = "DESCRIPTION")
  private String description;
  @Column(name = "CATEGORY_ID")
  private Long categoryId;
  @Column(name = "IS_MATCHING")
  private Long isMatching;
  @Transient
  private String name;

  public String getName() {
    return zoneName;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getZoneId() {
    return zoneId;
  }

  public void setZoneId(long zoneId) {
    this.zoneId = zoneId;
  }


  public Long getZoneMapId() {
    return zoneMapId;
  }

  public void setZoneMapId(Long zoneMapId) {
    this.zoneMapId = zoneMapId;
  }


  public String getZoneName() {
    return zoneName;
  }

  public void setZoneName(String zoneName) {
    this.zoneName = zoneName;
  }


  public String getZoneCode() {
    return zoneCode;
  }

  public void setZoneCode(String zoneCode) {
    this.zoneCode = zoneCode;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }


  public Long getIsMatching() {
    return isMatching;
  }

  public void setIsMatching(Long isMatching) {
    this.isMatching = isMatching;
  }
  public String getFilter() {
    return getName();
  }
}
