package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ConKC
 */
@Entity
@Table(name = "block_container")
public class BlockContainer implements Serializable {

    @Id
    @Column(name = "BLOCK_CONTAINER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "PROMOTION_BLOCK_ID")
    private Long promotionBlockId;
    @Column(name = "PROM_TYPE")
    private Long promType;
    @Column(name = "PROM_ID")
    private Long promId;
    @Column(name = "OCS_PARENT_ID")
    private Long ocsParentId;

    public BlockContainer() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPromotionBlockId() {
        return promotionBlockId;
    }

    public void setPromotionBlockId(Long promotionBlockId) {
        this.promotionBlockId = promotionBlockId;
    }

    public Long getPromType() {
        return promType;
    }

    public void setPromType(Long promType) {
        this.promType = promType;
    }

    public Long getPromId() {
        return promId;
    }

    public void setPromId(Long promId) {
        this.promId = promId;
    }

    public Long getOcsParentId() {
        return ocsParentId;
    }

    public void setOcsParentId(Long ocsParentId) {
        this.ocsParentId = ocsParentId;
    }
}
