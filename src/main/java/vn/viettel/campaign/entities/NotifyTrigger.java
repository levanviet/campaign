package vn.viettel.campaign.entities;

import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dto.NotifyTemplateDetailDTO;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author truongbx
 */
@Entity
@Table(name = "notify_trigger")
public class NotifyTrigger extends BaseCategory {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NOTIFY_TRIGGER_ID")
    private long notifyTriggerId;
    @Column(name = "TRIGGER_NAME")
    private String triggerName;
    @Column(name = "NOTIFY_TYPE")
    private Long notifyType;
    @Column(name = "ALIAS")
    private String alias;
    @Column(name = "NOTIFY_TEMPLATE_ID")
    private Long notifyTemplateId;
    @Column(name = "IS_INVITE")
    private Long isInvite;
    @Column(name = "NUMBER_RETRY")
    private Long numberRetry;
    @Column(name = "REPEAT_TIME")
    private Long repeatTime;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CATEGORY_ID")
    private Long categoryId;
    @Column(name = "CHANNEL_TYPE")
    private Long channelType;
    @Column(name = "RETRY_CYCLE_ID")
    private Long retryCycleId;
    @Transient
    private String name;
    @Transient
    private boolean bInvite;
    @Transient
    private List<StatisticCycle> statisticCycles;
    @Transient
    private List<NotifyContent> notifyContents;
    @Transient
    private List<NotifyValues> notifyValues;
    @Transient
    private NotifyTemplate notifyTemplate;
    @Transient
    private NotifyTemplateDetailDTO notifyTemplateDetailDTO = NotifyTemplateDetailDTO.builder().build();;
    @Transient
    private String filter;

    public String getFilter() {
        return triggerName;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public long getNotifyTriggerId() {
        return notifyTriggerId;
    }

    public void setNotifyTriggerId(long notifyTriggerId) {
        this.notifyTriggerId = notifyTriggerId;
    }

    public String getTriggerName() {
        return triggerName;
    }

    public void setTriggerName(String triggerName) {
        this.triggerName = triggerName;
    }

    public Long getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(Long notifyType) {
        this.notifyType = notifyType;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Long getNotifyTemplateId() {
        return notifyTemplateId;
    }

    public void setNotifyTemplateId(Long notifyTemplateId) {
        this.notifyTemplateId = notifyTemplateId;
    }

    public Long getIsInvite() {
        return isInvite;
    }

    public void setIsInvite(Long isInvite) {
        this.isInvite = isInvite;
    }

    public Long getNumberRetry() {
        return numberRetry;
    }

    public void setNumberRetry(Long numberRetry) {
        this.numberRetry = numberRetry;
    }

    public Long getRepeatTime() {
        return repeatTime;
    }

    public void setRepeatTime(Long repeatTime) {
        this.repeatTime = repeatTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getChannelType() {
        return channelType;
    }

    public void setChannelType(Long channelType) {
        this.channelType = channelType;
    }

    public Long getRetryCycleId() {
        return retryCycleId;
    }

    public void setRetryCycleId(Long retryCycleId) {
        this.retryCycleId = retryCycleId;
    }


    public void setName(String name) {
        this.name = name;
    }

    public boolean getbInvite() {
        return bInvite;
    }

    public void setbInvite(boolean bInvite) {
        this.bInvite = bInvite;
    }

    public List<StatisticCycle> getStatisticCycles() {
        return statisticCycles;
    }

    public void setStatisticCycles(List<StatisticCycle> statisticCycles) {
        this.statisticCycles = statisticCycles;
    }

    public List<NotifyContent> getNotifyContents() {
        return notifyContents;
    }

    public void setNotifyContents(List<NotifyContent> notifyContents) {
        this.notifyContents = notifyContents;
    }

    public List<NotifyValues> getNotifyValues() {
        return notifyValues;
    }

    public void setNotifyValues(List<NotifyValues> notifyValues) {
        this.notifyValues = notifyValues;
    }

    public NotifyTemplate getNotifyTemplate() {
        return notifyTemplate;
    }

    public void setNotifyTemplate(NotifyTemplate notifyTemplate) {
        this.notifyTemplate = notifyTemplate;
    }

    public NotifyTemplateDetailDTO getNotifyTemplateDetailDTO() {
        return notifyTemplateDetailDTO;
    }

    public void setNotifyTemplateDetailDTO(NotifyTemplateDetailDTO notifyTemplateDetailDTO) {
        this.notifyTemplateDetailDTO = notifyTemplateDetailDTO;
    }
    @Override
    public long getParentId() {
        return getCategoryId();
    }

    @Override
    public String getTreeType() {
        return "notifyTrigger";
    }

    @Override
    public void setParentId(Long id) {
        setCategoryId(id);
    }

    @Override
    public String getName() {
        return this.triggerName;
    }

    @Override
    public Class getParentType() {
        return Category.class;
    }

}
