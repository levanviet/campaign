package vn.viettel.campaign.entities;

import javax.persistence.Transient;

/**
 * @author truongbx
 * @date 9/15/2019
 */
public abstract class BaseCategory {
//	private String filter = "";
	public abstract long getParentId();
	public abstract void setParentId(Long id);
	public abstract String getTreeType();
	public abstract String getName();
	public abstract Class getParentType();
	public String getFilter() {
		return getName();
	}

}
