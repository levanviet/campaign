package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "result_table")
public class ResultTable  extends BaseCategory {

  @Id
//  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "RESULT_TABLE_ID")
  private long resultTableId;
  @Column(name = "DESCRIPTION")
  private String description;
  @Column(name = "CATEGORY_ID")
  private Long categoryId;
  @Column(name = "RESULT_TABLE_NAME")
  private String resultTableName;
  @Column(name = "DEFAULT_RESULT_ID")
  private Long defaultResultId;
  @Column(name = "CONDITION_TABLE_ID")
  private Long conditionTableId;
  @Transient
  private Long ruleId;

  @Transient
  private String name ;
  @Transient
  private Long resultTableIndex;

  @Transient
  private String filter;

  public String getFilter() {
    return resultTableName;
  }

  public void setFilter(String filter) {
    this.filter = filter;
  }


  public long getResultTableId() {
    return resultTableId;
  }

  public void setResultTableId(long resultTableId) {
    this.resultTableId = resultTableId;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }


  public String getResultTableName() {
    return resultTableName;
  }

  public void setResultTableName(String resultTableName) {
    this.resultTableName = resultTableName;
  }


  public Long getDefaultResultId() {
    return defaultResultId;
  }

  public void setDefaultResultId(Long defaultResultId) {
    this.defaultResultId = defaultResultId;
  }


  public Long getConditionTableId() {
    return conditionTableId;
  }

  public void setConditionTableId(Long conditionTableId) {
    this.conditionTableId = conditionTableId;
  }

  public Long getRuleId() {
    return ruleId;
  }

  public void setRuleId(Long ruleId) {
    this.ruleId = ruleId;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getResultTableIndex() {
    return resultTableIndex;
  }

  public void setResultTableIndex(Long resultTableIndex) {
    this.resultTableIndex = resultTableIndex;
  }
  @Override
  public long getParentId() {
    return getCategoryId();
  }

  @Override
  public String getTreeType() {
    return "resultTable";
  }

  @Override
  public void setParentId(Long id) {
    setCategoryId(id);
  }

  @Override
  public String getName() {
    return this.resultTableName;
  }

  @Override
  public Class getParentType() {
    return Category.class;
  }
}
