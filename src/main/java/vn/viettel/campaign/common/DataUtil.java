package vn.viettel.campaign.common;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author truongbx
 * @date 9/4/2019
 */
public class DataUtil {
	public static String percentageRegix = "^[^%]*$";
	public static String REGEX_FILTER_TABLE = "^[^.,{}();:&]*$";

	@SuppressWarnings("unchecked")
	public static <T> T cloneBean(T source) {
		try {
			if (source == null) {
				return null;
			}
			T dto = (T) source.getClass().getConstructor().newInstance();
			BeanUtils.copyProperties(source, dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}
	public static boolean isNumber(String string) {
		return !isNullOrEmpty(string) && string.trim().matches("^\\d+$");
	}
	public static void trimObject(Object model) {
		if (isNullObject(model)) {
			return;
		}
		for (Field field : model.getClass().getDeclaredFields()) {
			try {
				field.setAccessible(true);
				Object value = field.get(model);
				if (value != null) {
					if (value instanceof String) {
						String trimmed = (String) value;
						field.set(model, trimmed.trim());
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static boolean isStringNullOrEmpty(String input) {
		if (input == null || input.trim().equalsIgnoreCase("")) {
			return true;
		}
		return false;
	}

	//	mac dinh string da duoc check null or empty
	public static boolean checkMaxlength(String input, int length) {
		if (input == null) {
			return true;
		}
		try {
			if (input.trim().length() > length) {
				return false;
			}
		} catch (Exception e) {
			return true;
		}
		return true;
	}

	public static boolean checkMaxlength(String input) {
		// default check do dai la 255 ky tu
		return checkMaxlength(input, 255);
	}

	//	mac dinh string da duoc check null or empty
	public static boolean checkNotContainPercentage(String input) {
		if (input == null) {
			return true;
		}
		return Pattern.compile(percentageRegix).matcher(input).matches();
	}
	//	mac dinh string da duoc check null or empty
	public static boolean validateInputFilter(String input) {
		if (input == null) {
			return true;
		}
		return Pattern.compile(REGEX_FILTER_TABLE).matcher(input).matches();
	}

	//	public static void main(String[] args) {
//		System.out.println(checkNotContainPercentage("%truongbx"));
//	}
	public static List<String> splitListFile(String strFiles) {
		List<String> lstFile = new ArrayList<>();
		if (!isNullOrEmpty(strFiles)) {
			String lst[] = strFiles.split(";");
			lstFile = Arrays.asList(lst);
		}
		return lstFile;
	}

	public static List<String> splitListFile(String strFiles, String seperator) {
		List<String> lstFile = new ArrayList<String>();
		if (!isNullOrEmpty(strFiles)) {
			String lst[] = strFiles.split(seperator);
			lstFile = Arrays.asList(lst);
		}
		return lstFile;
	}

	/**
	 * check null or empty Su dung ma nguon cua thu vien StringUtils trong apache
	 * common lang
	 *
	 * @param cs String
	 * @return boolean
	 */
	public static boolean isNullOrEmpty(CharSequence cs) {
		int strLen;
		if (cs == null || (strLen = cs.length()) == 0) {
			return true;
		}
		for (int i = 0; i < strLen; i++) {
			if (!Character.isWhitespace(cs.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	public static boolean isNullObject(Object obj1) {
		if (obj1 == null) {
			return true;
		}
		if (obj1 instanceof String) {
			return isNullOrEmpty(obj1.toString());
		}
		return false;
	}

	public static boolean isNullOrEmpty(final Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	public static boolean isNullOrEmpty(final Object[] collection) {
		return collection == null || collection.length == 0;
	}

	public static boolean isNullOrEmpty(final Map<?, ?> map) {
		return map == null || map.isEmpty();
	}

	/**
	 * safe equal
	 *
	 * @param obj1 Long
	 * @param obj2 Long
	 * @return boolean
	 */
	public static boolean safeEqual(Long obj1, Long obj2) {
		if (obj1 == obj2)
			return true;
		return ((obj1 != null) && (obj2 != null) && (obj1.compareTo(obj2) == 0));
	}

	/**
	 * safe equal
	 *
	 * @param obj1 Long
	 * @param obj2 Long
	 * @return boolean
	 */
	public static boolean safeEqual(BigInteger obj1, BigInteger obj2) {
		if (obj1 == obj2)
			return true;
		return (obj1 != null) && (obj2 != null) && obj1.equals(obj2);
	}

	/**
	 * @param obj1
	 * @param obj2
	 * @return
	 * @date 09-12-2015 17:43:20
	 * @author TuyenNT17
	 * @description
	 */
	public static boolean safeEqual(Short obj1, Short obj2) {
		if (obj1 == obj2)
			return true;
		return ((obj1 != null) && (obj2 != null) && (obj1.compareTo(obj2) == 0));
	}

	/**
	 * safe equal
	 *
	 * @param obj1 String
	 * @param obj2 String
	 * @return boolean
	 */
	public static boolean safeEqual(String obj1, String obj2) {
		if (obj1 == obj2)
			return true;
		return ((obj1 != null) && (obj2 != null) && obj1.equals(obj2));
	}

	public static String getDataBetweenCurlyBracesByIndex(String data, int index) {
		Pattern logEntry = Pattern.compile("\\{(.*?)\\}");
		String[] table = data.split("\\.");
		String filterData =table[index-1];
		Matcher matchPattern = logEntry.matcher(filterData);
		while (matchPattern.find()) {
				return matchPattern.group(1);
		}
		return "";
	}

	public static String getDataBetweenParenthesis(String data) {
		String str = data.substring(data.indexOf("(") + 1, data.indexOf(")"));
		return str;
	}


	public static String getDataBetweenCurlyBraces(String data) {
		Pattern logEntry = Pattern.compile("\\{(.*?)\\}");
		Matcher matchPattern = logEntry.matcher(data);
		while (matchPattern.find()) {
			System.out.println(matchPattern.group(1));
		}
		return "";
	}

	public static void main(String[] args) {
//		String parterm = "([^{]*?)(?=\\})";
//		String data ="type:1|campaignMsg{}.custProfile{msisdn=hj;getStartOfYear()}.parameterInsMap{}";
//		String data2 = "reset survey 888 ${d14322a2-bc13-4fcb-9da3-e2346a7d58ec} for the participant ${c45e9bc0-6043-4aa1-8de0-27f8f8aade82}";
//		System.out.println(getDataBetweenCurlyBraces(data,2));
		String abc = "bc{d";
		System.out.println(Pattern.compile(percentageRegix).matcher(abc).matches());
		System.out.println(validateInputFilter(abc));
	}
}
