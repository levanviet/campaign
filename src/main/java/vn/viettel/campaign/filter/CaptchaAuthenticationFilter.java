///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package vn.viettel.campaign.filter;
//
//import java.io.IOException;
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import org.springframework.security.authentication.InsufficientAuthenticationException;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
//import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
//
///**
// *
// * @author nguyen89bin_gmail
// */
//public class CaptchaAuthenticationFilter extends AbstractAuthenticationProcessingFilter{
//    
//    private String processUrl;
//
//    public CaptchaAuthenticationFilter(String defaultFilterProcessesUrl, String failureUrl) {
//        super(defaultFilterProcessesUrl);
//        this.processUrl = defaultFilterProcessesUrl;
//        setAuthenticationFailureHandler(new SimpleUrlAuthenticationFailureHandler(failureUrl));
//    }
//
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        HttpServletRequest req = (HttpServletRequest) request;
//        HttpServletResponse res=(HttpServletResponse)response;
//        if(processUrl.equals(req.getServletPath()) && "POST".equalsIgnoreCase(req.getMethod())){
//            String expect = req.getSession().getAttribute("YZM").toString();
//
//            //remove from session
//            req.getSession().removeAttribute("YZM");
//
//            if (expect != null && !expect.equals(req.getParameter("verifyCode"))){
//                unsuccessfulAuthentication(req, res, new InsufficientAuthenticationException("Wrong verification code."));
//                return;
//            }
//        }
//        chain.doFilter(request, response);
//    }
//
//
//    @Override
//    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
//        return null;
//    }
//    
//}
