package vn.viettel.campaign.config;

//import com.viettel.vocs.cba.camp.wapi.produce.RequestProducer;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import vn.viettel.campaign.service.UtilsService;
import vn.viettel.campaign.service.impl.UtilsServiceImpl;

/**
 *
 * @author habm2
 */
public class Startup implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
//        RequestProducer.buildConfiguration();
        UtilsServiceImpl utilsService = new UtilsServiceImpl();
        utilsService.loadProperty();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
