package vn.viettel.campaign.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dto.BehaviourDTO;
import vn.viettel.campaign.dto.PromotionBlockDTO;
import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.entities.PromotionBlock;
import vn.viettel.campaign.service.*;
import vn.viettel.campaign.service.impl.TreeServiceImpl;
import java.util.Arrays;
import javax.faces.bean.ManagedProperty;
import lombok.Getter;
import lombok.Setter;
import vn.viettel.campaign.dao.NotifyTriggerDAOInterface;
import vn.viettel.campaign.entities.NotifyTrigger;
import vn.viettel.campaign.entities.OcsBehaviour;
import vn.viettel.campaign.entities.ScenarioTrigger;
import vn.viettel.campaign.validate.CheckOnEdit;

/**
 *
 * @author ConKC
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
public class PromotionBlockController extends BaseController implements Serializable {

    private TreeNode rootNodeCategory;
    private TreeNode selectedNodeCategory;
    private TreeNode rootNodePrm;
    private TreeNode[] selectedNodePrm;
    private boolean isDisplay;
    private boolean displayBtnSelect;
    private Category category;
    private boolean update;
    private List<Category> categories;
    private List<PromotionBlock> promotionBlocks;
    private Boolean actonSwitch;
    private PromotionBlockDTO promotionBlockDTO;
    private List<BehaviourDTO> behaviourDTOs;
    private BehaviourDTO behaviourDTO;
    private List<BehaviourDTO> ocsBehaviours;
    private List<BehaviourDTO> notifyTriggers;
    private List<BehaviourDTO> scenarioTriggers;
    private Long promotionType;
    @Autowired
    private UssdScenarioService ussdScenarioService;
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PromotionBlockService promotionBlockService;

    @Autowired
    private TreeServiceImpl treeService;

    @Autowired
    private UtilsService utilsService;

    @Autowired
    OCSBehaviourInterface oCSBehaviourServiceImpl;

    @Autowired
    NotifyTriggerDAOInterface notifyTriggerDAO;

    @Autowired
    private ScenarioTriggerService scenarioTriggerService;

    @ManagedProperty(value = "#{notifyTriggerController}")
    NotifyTriggerController notifyTriggerController;

    @ManagedProperty(value = "#{oCSBehaviourController}")
    OCSBehaviourController oCSBehaviourController;

    @ManagedProperty(value = "#{scenarioTriggerController}")
    ScenarioTriggerController scenarioTriggerController;

    private String viewObject;
    private String objectDetail;

    @PostConstruct
    public void init() {
        this.promotionType = 1l;
        this.isDisplay = false;
        this.displayBtnSelect = true;
        this.actonSwitch = false;
        this.category = new Category();
        this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_PROMOTION_BLOCK_TYPE);
        initTreeNode();
    }

    public void initTreeNode() {
        List<Long> categoryIds = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> categoryIds.add(item.getCategoryId()));
        }
        this.promotionBlocks = promotionBlockService.findByCategory(categoryIds);
        List<Object> objectLst = new ArrayList<>(promotionBlocks);
        rootNodeCategory = treeService.createTreeCategoryAndComponentObj(categories, objectLst);
        addExpandedNode(rootNodeCategory.getChildren().get(0));
    }

    public void prepareCreateCategory() {
        this.update = false;
        Category parentCat = (Category) selectedNodeCategory.getData();
        this.category = new Category(null, null, Constants.CatagoryType.CATEGORY_PROMOTION_BLOCK_TYPE, parentCat.getCategoryId());
        Long id = ussdScenarioService.getNextSequense(Constants.TableName.CATEGORY);
        category.setCategoryId(id);
    }

    public boolean validateCategory() {
        if (DataUtil.isStringNullOrEmpty(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Category name");
            return false;
        }
        
        if (!DataUtil.checkNotContainPercentage(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Category name");
            return false;
        }
        boolean rs = true;
        if (!categoryService.checkDuplicate(category.getName(), category.getCategoryId(), category.getCategoryType())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.category.exist"));
            rs = false;
        }
        return rs;
//        boolean rs = true;
//        if (StringUtils.isNotBlank(category.getName()) && category.getName().length() > 255) {
//            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("maxlength.category.name"));
//            rs = false;
//        } else if (!categoryService.checkDuplicate(category.getName(), category.getCategoryId())) {
//            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Category name");
//            rs = false;
//        }
//        return rs;
    }

    public void doSaveOrUpdateCategory() {
        if (validateCategory()) {
            categoryService.save(category);
            refreshTree();
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public void prepareEditCategory() {
        this.update = true;
        if (Objects.nonNull(selectedNodeCategory)) {
            Category cat = (Category) selectedNodeCategory.getData();
            this.category = categoryService.getCategoryById(cat.getCategoryId());
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void doDeleteCategory() {
        if (Objects.nonNull(selectedNodeCategory)) {
            this.category = (Category) selectedNodeCategory.getData();

            if (DataUtil.isNullObject(category.getParentId())) {
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.parent.category"));
                return;
            }

            if (categoryService.checkDeleteCategory(this.category.getCategoryId())) {
                categoryService.deleteCategory(category);
                selectedNodeCategory.getChildren().clear();
                TreeNode parNode = selectedNodeCategory.getParent();

                selectedNodeCategory.getParent().getChildren().remove(selectedNodeCategory);
                if (parNode.getChildren().isEmpty()) {
                    removeExpandedNode(parNode);
                }
                selectedNodeCategory.setParent(null);
                selectedNodeCategory = null;
                categories.remove(category);
                updateCategoryInforPromotion();
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
            } else {
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    private void refreshTree() {
        if (update) {
            categories.remove((Category) selectedNodeCategory.getData());
            categories.add(this.category);
        } else {
            categories.add(this.category);
        }
        List<Object> objectLst = new ArrayList<>(promotionBlocks);
        rootNodeCategory = treeService.createTreeCategoryAndComponentObj(categories, objectLst);

        TreeNode note = findCategoryInTree(rootNodeCategory, category);
        processRefreshCategory(rootNodeCategory, selectedNodeCategory, category, update);
        updateCategoryInforPromotion();
    }

    public void prepareCreatePromotion() {
        this.actonSwitch = true;
        this.isDisplay = true;
        this.update = false;
        this.behaviourDTOs = new ArrayList<>();
        if (Objects.nonNull(selectedNodeCategory)) {
            this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_PROMOTION_BLOCK_TYPE);
            this.ocsBehaviours = promotionBlockService.findOcsBehaviour(null);
            this.notifyTriggers = promotionBlockService.findNotifyTrigger(null);
            this.scenarioTriggers = promotionBlockService.findScenarioTrigger(null);
            Category cat = (Category) selectedNodeCategory.getData();
            Long prmBlockId = this.promotionBlockService.genPrmBlockId();
            this.promotionBlockDTO = new PromotionBlockDTO(prmBlockId, cat.getCategoryId());
        } else {
            this.promotionBlockDTO = new PromotionBlockDTO();
        }
    }

    public void prepareEditPromotion(final PromotionBlock pb) {
        this.actonSwitch = false;
        this.isDisplay = true;
        this.update = true;
        this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_PROMOTION_BLOCK_TYPE);
        if (Objects.nonNull(pb)) {
            this.promotionBlockDTO = new PromotionBlockDTO();
            promotionBlockDTO.setBlockId(pb.getId());
            promotionBlockDTO.setBlockName(pb.getName());
            promotionBlockDTO.setCategoryId(pb.getCategoryId());
            promotionBlockDTO.setDescription(pb.getDescription());
            this.behaviourDTOs = promotionBlockService.findBehaviourByPromotionBlockId(pb.getId());
            this.ocsBehaviours = promotionBlockService.findOcsBehaviour(null);
            this.notifyTriggers = promotionBlockService.findNotifyTrigger(null);
            this.scenarioTriggers = promotionBlockService.findScenarioTrigger(null);
        } else {
            this.promotionBlockDTO = new PromotionBlockDTO();
        }
    }

    public PromotionBlock findPromotionBlock() {
        PromotionBlock promotionBlock = (PromotionBlock) selectedNodeCategory.getData();
        if (Objects.nonNull(selectedNodeCategory) && Objects.nonNull(promotionBlock)) {
            return (PromotionBlock) selectedNodeCategory.getData();
        }
        return new PromotionBlock();
    }

    public void prepareOpenDlgChoosePromotion() {
        this.promotionType = 1l;
        List<Long> ids = new ArrayList<>();
        if (behaviourDTOs != null && !behaviourDTOs.isEmpty()) {
            behaviourDTOs.stream()
                    .filter((behaviour) -> (Objects.nonNull(behaviour.getPromotionType()))
                            && Objects.equals(Constants.PromotionType.OCS_BEHAVIOUR, behaviour.getPromotionType()))
                    .forEach((behaviour) -> {
                        ids.add(behaviour.getPromotionId());
                    });
        }
        this.rootNodePrm = promotionBlockService.genTreeBehaviours(ids, ocsBehaviours, promotionType);
        treeService.expandAll(rootNodePrm);
    }

    public void onChangePrmType() {
        List<Long> ids = new ArrayList<>();
        if (Objects.equals(promotionType, Constants.PromotionType.OCS_BEHAVIOUR)) {
            if (behaviourDTOs != null && !behaviourDTOs.isEmpty()) {
                behaviourDTOs.stream()
                        .filter((behaviour) -> (Objects.nonNull(behaviour.getPromotionType()))
                        && Objects.equals(Constants.PromotionType.OCS_BEHAVIOUR, behaviour.getPromotionType()))
                        .forEach((behaviour) -> {
                            ids.add(behaviour.getPromotionId());
                        });
            }
            this.rootNodePrm = promotionBlockService.genTreeBehaviours(ids, ocsBehaviours, promotionType);
        } else if (Objects.equals(promotionType, Constants.PromotionType.NOTIFY_TRIGGER)) {
            if (behaviourDTOs != null && !behaviourDTOs.isEmpty()) {
                behaviourDTOs.stream()
                        .filter((behaviour) -> (Objects.nonNull(behaviour.getPromotionType()))
                        && Objects.equals(Constants.PromotionType.NOTIFY_TRIGGER, behaviour.getPromotionType()))
                        .forEach((behaviour) -> {
                            ids.add(behaviour.getPromotionId());
                        });
            }
            this.rootNodePrm = promotionBlockService.genTreeBehaviours(ids, notifyTriggers, promotionType);
        } else if (Objects.equals(promotionType, Constants.PromotionType.SCENARIO_TRIGGER)) {
            if (behaviourDTOs != null && !behaviourDTOs.isEmpty()) {
                behaviourDTOs.stream()
                        .filter((behaviour) -> (Objects.nonNull(behaviour.getPromotionType()))
                        && Objects.equals(Constants.PromotionType.SCENARIO_TRIGGER, behaviour.getPromotionType()))
                        .forEach((behaviour) -> {
                            ids.add(behaviour.getPromotionId());
                        });
            }
            this.rootNodePrm = promotionBlockService.genTreeBehaviours(ids, scenarioTriggers, promotionType);
        } else {
            this.rootNodePrm = new DefaultTreeNode();
        }
        treeService.expandAll(rootNodePrm);
    }

    public void onSelectedPrm(TreeNode[] nodes) {
        if (nodes != null && nodes.length > 0) {
            for (TreeNode node : nodes) {
                if ("behaviour".equals(node.getType())) {
                    BehaviourDTO bdto = (BehaviourDTO) node.getData();
                    if (DataUtil.safeEqual(bdto.getPromotionType(), 1L)
                            && Objects.nonNull(bdto.getNotifyId())) {
                        behaviourDTOs.add(bdto);
                        List<BehaviourDTO> notifyOs = promotionBlockService.findNotifyTrigger(Arrays.asList(bdto.getNotifyId()));
                        if (!notifyOs.isEmpty()) {
                            notifyOs.forEach(item -> {
                                item.setOcsParentId(bdto.getPromotionId());
                            });
                            behaviourDTOs.addAll(notifyOs);
                        }
                    } else {
                        behaviourDTOs.add(bdto);
                    }
                }
            }
        }
    }

    public void onViewPrm(BehaviourDTO behaviour) {
        if (Objects.nonNull(behaviour)) {
            this.behaviourDTO = behaviour;
            if (behaviourDTO.getPromotionType() == 1) {
                OcsBehaviour ocsBehaviour = oCSBehaviourServiceImpl.findById(behaviourDTO.getPromotionId());
                oCSBehaviourController.onViewObject(ocsBehaviour);
                this.viewObject = "ocs_behaviour";
                this.objectDetail = "Ocs Behaviour";
            }
            if (behaviourDTO.getPromotionType() == 2) {
                NotifyTrigger notifyTrigger = notifyTriggerDAO.findById(NotifyTrigger.class, behaviourDTO.getPromotionId());
                notifyTriggerController.prepareViewObject(notifyTrigger);
                this.viewObject = "notify";
                this.objectDetail = "Notify Trigger";
            }
            if (behaviourDTO.getPromotionType() == 3) {
                ScenarioTrigger scenarioTrigger = scenarioTriggerService.findById(behaviourDTO.getPromotionId());
                scenarioTriggerController.prepareViewObject(scenarioTrigger);
                this.viewObject = "scen";
                this.objectDetail = "Scenario Trigger";
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void onDeletePrm(BehaviourDTO behaviour) {
        if (Objects.nonNull(behaviour)) {
            this.behaviourDTOs.remove(behaviour);
            if (DataUtil.safeEqual(behaviour.getPromotionType(), 1L)) {
                BehaviourDTO dTO = behaviourDTOs.stream()
                        .filter(item -> Objects.nonNull(item.getOcsParentId()) && DataUtil.safeEqual(item.getOcsParentId(), behaviour.getPromotionId()))
                        .findFirst()
                        .orElse(null);
                this.behaviourDTOs.remove(dTO);
            }
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("promotion.block.success"));
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void eventSelectNode() {
        this.displayBtnSelect = !(selectedNodePrm != null && selectedNodePrm.length > 0);
    }

    public void onReset() {
        if (update) {
            PromotionBlock block = promotionBlockService.findById(promotionBlockDTO.getBlockId());
            this.prepareEditPromotion(block);
        } else {
            this.behaviourDTOs = new ArrayList<>();
            if (Objects.nonNull(selectedNodeCategory)) {
                this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_PROMOTION_BLOCK_TYPE);
                this.ocsBehaviours = promotionBlockService.findOcsBehaviour(null);
                this.notifyTriggers = promotionBlockService.findNotifyTrigger(null);
                this.scenarioTriggers = promotionBlockService.findScenarioTrigger(null);
                Category cat = (Category) selectedNodeCategory.getData();
                Long prmBlockId = this.promotionBlockService.genPrmBlockId();
                this.promotionBlockDTO = new PromotionBlockDTO(prmBlockId, cat.getCategoryId());
            } else {
                this.promotionBlockDTO = new PromotionBlockDTO();
            }
        }
        this.actonSwitch = true;
    }

    public void onSavePrmBlock() {
        if (validatePrm()) {
            try {
                boolean rs = promotionBlockService.savePrmBlock(promotionBlockDTO, behaviourDTOs, update);
                if (rs) {
                    this.update = true;
                    initTreeNode();
                    Category cat = categoryService.getCategoryById(promotionBlockDTO.getCategoryId());
                    TreeNode note = findCategoryInTree(rootNodeCategory, cat);
                    if (note != null) {
                        addExpandedNode(note);
                        expandCurrentNode(note);
                        mapTreeStatus(rootNodeCategory);
                    }
                    successMsg(Constants.REMOTE_GROWL, utilsService.getTex("promotion.block.success"));
                    this.actonSwitch = false;
                } else {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.fail"));
                }
            } catch (Exception ex) {
                Logger.getLogger(PromotionBlockController.class.getName()).log(Level.SEVERE, null, ex);
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.fail"));
            }
        }
    }

    public boolean validatePrm() {
        boolean rs = true;
        if (StringUtils.isNotBlank(promotionBlockDTO.getBlockName())
                && promotionBlockDTO.getBlockName().length() > 255) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("maxlength.promotion.block.name"));
            rs = false;
        } else if (!promotionBlockService.checkDuplicate(promotionBlockDTO.getBlockName(), promotionBlockDTO.getBlockId())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.promotion.block"));
            rs = false;
        } else if (StringUtils.isNotBlank(promotionBlockDTO.getDescription())
                && promotionBlockDTO.getDescription().length() > 255) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("maxlength.promotion.block.description"));
            rs = false;
        } else if (behaviourDTOs == null || behaviourDTOs.isEmpty()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("empty.promotion.behaviours"));
            rs = false;
        }
        return rs;
    }

    public void onDeletePrmBlock() {

        PromotionBlock promotionBlock = (PromotionBlock) selectedNodeCategory.getData();
        if (promotionBlockService.checkInUseInResult(promotionBlock.getId())){
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.promotionblock"));
            return;
        }
        if (promotionBlockService.deletePrmBlock(promotionBlock)) {
            TreeNode parent =   selectedNodeCategory.getParent();
            parent.getChildren().remove(selectedNodeCategory);
            if (parent != null && parent.getChildren().size() == 0) {
                removeExpandedNode(parent);
            }
            this.isDisplay = false;
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("promotion.block.success"));
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.fail"));
        }
    }

    public TreeNode getRootNodeCategory() {
        return rootNodeCategory;
    }

    public void setRootNodeCategory(TreeNode rootNodeCategory) {
        this.rootNodeCategory = rootNodeCategory;
    }

    public TreeNode getSelectedNodeCategory() {
        return selectedNodeCategory;
    }

    public void setSelectedNodeCategory(TreeNode selectedNodeCategory) {
        this.selectedNodeCategory = selectedNodeCategory;
    }

    public boolean isIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(boolean isDisplay) {
        this.isDisplay = isDisplay;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<PromotionBlock> getPromotionBlocks() {
        return promotionBlocks;
    }

    public void setPromotionBlocks(List<PromotionBlock> promotionBlocks) {
        this.promotionBlocks = promotionBlocks;
    }

    public Boolean getActonSwitch() {
        return actonSwitch;
    }

    public void setActonSwitch(Boolean actonSwitch) {
        this.actonSwitch = actonSwitch;
    }

    public PromotionBlockDTO getPromotionBlockDTO() {
        return promotionBlockDTO;
    }

    public void setPromotionBlockDTO(PromotionBlockDTO promotionBlockDTO) {
        this.promotionBlockDTO = promotionBlockDTO;
    }

    public List<BehaviourDTO> getBehaviourDTOs() {
        return behaviourDTOs;
    }

    public void setBehaviourDTOs(List<BehaviourDTO> behaviourDTOs) {
        this.behaviourDTOs = behaviourDTOs;
    }

    public BehaviourDTO getBehaviourDTO() {
        return behaviourDTO;
    }

    public void setBehaviourDTO(BehaviourDTO behaviourDTO) {
        this.behaviourDTO = behaviourDTO;
    }

    public TreeNode getRootNodePrm() {
        return rootNodePrm;
    }

    public void setRootNodePrm(TreeNode rootNodePrm) {
        this.rootNodePrm = rootNodePrm;
    }

    public List<BehaviourDTO> getOcsBehaviours() {
        return ocsBehaviours;
    }

    public void setOcsBehaviours(List<BehaviourDTO> ocsBehaviours) {
        this.ocsBehaviours = ocsBehaviours;
    }

    public Long getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(Long promotionType) {
        this.promotionType = promotionType;
    }

    public TreeNode[] getSelectedNodePrm() {
        return selectedNodePrm;
    }

    public void setSelectedNodePrm(TreeNode[] selectedNodePrm) {
        this.selectedNodePrm = selectedNodePrm;
    }

    public List<BehaviourDTO> getNotifyTriggers() {
        return notifyTriggers;
    }

    public void setNotifyTriggers(List<BehaviourDTO> notifyTriggers) {
        this.notifyTriggers = notifyTriggers;
    }

    public List<BehaviourDTO> getScenarioTriggers() {
        return scenarioTriggers;
    }

    public void setScenarioTriggers(List<BehaviourDTO> scenarioTriggers) {
        this.scenarioTriggers = scenarioTriggers;
    }

    public boolean isDisplayBtnSelect() {
        return displayBtnSelect;
    }

    public void setDisplayBtnSelect(boolean displayBtnSelect) {
        this.displayBtnSelect = displayBtnSelect;
    }
    
    public void prepareEditWhenClick() {
        CheckOnEdit.onEdit = "promotion";
        prepareEdit();
        this.actonSwitch = false;
    }

    public void prepareEditFromContextMenu() {
        CheckOnEdit.onEdit = "promotion";
        prepareEdit();
        this.actonSwitch = true;

    }

    public void prepareEdit() {
        if ("category".equals(selectedNodeCategory.getType())) {
            prepareEditCategory();
            PrimeFaces.current().executeScript("PF('carDialog').show();");
        }
        if ("promotionBlock".equals(selectedNodeCategory.getType())) {
            prepareEditPromotion(findPromotionBlock());
        }

    }

    public OCSBehaviourInterface getoCSBehaviourServiceImpl() {
        return oCSBehaviourServiceImpl;
    }

    public void setoCSBehaviourServiceImpl(OCSBehaviourInterface oCSBehaviourServiceImpl) {
        this.oCSBehaviourServiceImpl = oCSBehaviourServiceImpl;
    }

    public OCSBehaviourController getoCSBehaviourController() {
        return oCSBehaviourController;
    }

    public void setoCSBehaviourController(OCSBehaviourController oCSBehaviourController) {
        this.oCSBehaviourController = oCSBehaviourController;
    }

}
