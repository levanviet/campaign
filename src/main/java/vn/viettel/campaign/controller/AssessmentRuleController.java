/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.support.RequestContext;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.constants.Constants.CatagoryType;
import static vn.viettel.campaign.controller.BaseController.errorMsg;
import static vn.viettel.campaign.controller.BaseController.errorMsgParams;
import static vn.viettel.campaign.controller.BaseController.successMsg;
import vn.viettel.campaign.dto.Expression;
import vn.viettel.campaign.dto.NodeDefineDTO;
import vn.viettel.campaign.dto.NodeInfo;
import vn.viettel.campaign.entities.AssessmentRule;
import vn.viettel.campaign.entities.AssessmentRuleKpiProcessorMap;
import vn.viettel.campaign.entities.AssessmentRulePreProcessUnitMap;
import vn.viettel.campaign.entities.BaseCategory;
import vn.viettel.campaign.entities.CampaignEvaluationInfo;
import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.entities.CriteriaValue;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.KpiProcessor;
import vn.viettel.campaign.entities.PpuEvaluation;
import vn.viettel.campaign.entities.ResultClass;
import vn.viettel.campaign.entities.KpiProcessorCriteriaValueMap;
import vn.viettel.campaign.entities.PreProcessValueMap;
import vn.viettel.campaign.entities.ProcessValue;
import vn.viettel.campaign.entities.RuleEvaluationInfo;
import vn.viettel.campaign.entities.SegmentEvaluationInfo;
import vn.viettel.campaign.service.AssessmentRuleService;
import vn.viettel.campaign.service.CategoryService;
import vn.viettel.campaign.service.TreeUtilsService;
import vn.viettel.campaign.service.UtilsService;

/**
 *
 * @author ABC
 */
@ManagedBean(name = "assessmentRuleController")
@ViewScoped
@Getter
@Setter
public class AssessmentRuleController extends BaseController implements Serializable {

    private TreeNode rootNode;
    private TreeNode kpiNode;
    private TreeNode ppuNode;
    private TreeNode rootNodeFuzzy;
    private TreeNode selectedNode;
    private TreeNode selectedNodeAssessmentRule;
    private TreeNode currentNote;
    private TreeNode currenRootNode;

    private AssessmentRule assessmentRule;
    private AssessmentRule backupAssessmentRule;
    private Category category;
    private Category categoryBackUp;
    private AssessmentRulePreProcessUnitMap assessmentRulePreProcessUnitMap;
    private AssessmentRuleKpiProcessorMap assessmentRuleKpiProcessorMap;
    private KpiProcessorCriteriaValueMap kpiProcessorCriteriaValueMap;
    private PpuEvaluation ppuEvaluation;
    private PpuEvaluation ppuEvaluationMap;
    private KpiProcessor kpiProcessor;
    private KpiProcessor kpiProcessorMap;
    private ResultClass resultClass;
    private FuzzyRule fuzzyRule;
    private FuzzyRule fuzzyRuleEdit;
    private Expression expression;
    private NodeDefineDTO nodeDefineDTO;
    private NodeInfo nodeInfo;

    private List<AssessmentRule> listAssessmentRule;
    private List<AssessmentRule> listAssessmentRuleToDelete = new ArrayList<>();
    private List<AssessmentRulePreProcessUnitMap> listAssessmentRulePreProcessUnitMap;
    private List<AssessmentRuleKpiProcessorMap> listAssessmentRuleKpiProcessorMap;
    private List<CampaignEvaluationInfo> listCampaignEvaluationInfo;
    private List<SegmentEvaluationInfo> listSegmentEvaluationInfo;
    private List<Category> categories;
    private List<Category> categoriess = new ArrayList<>();
    private List<Category> categoryKpi = new ArrayList<>();
    private List<Category> categoryPPU = new ArrayList<>();
    private List<PpuEvaluation> listPpuEvaluation;
    private List<PpuEvaluation> listPpuEvaluationToDelete;
    private List<PpuEvaluation> listPpuEvaluationBuildTree = new ArrayList<>();
    private List<KpiProcessor> listKpiProcessor;
    private List<KpiProcessor> listKpiProcessorToDelete;
    private List<KpiProcessor> listKpiProcessorBuildTree = new ArrayList<>();
    private List<ResultClass> listResultClass;
    private List<ResultClass> listResultClassMap;
    private List<ResultClass> listResultClassToDelete = new ArrayList<>();
    private List<FuzzyRule> listFuzzyRule;
    private List<FuzzyRule> listFuzzyRuleToDelete = new ArrayList<>();
    private List<FuzzyRule> listFuzzyRuleToCheck = new ArrayList<>();
    private List<CriteriaValue> listCriteriaValue = new ArrayList<>();
    private List<KpiProcessorCriteriaValueMap> listkpiProcessorCriteriaValueMap = new ArrayList<>();
    private List<PreProcessValueMap> listPreProcessValueMap = new ArrayList<>();
    private List<ProcessValue> listProcessValue = new ArrayList<>();
    private List<RuleEvaluationInfo> listRuleEvaluationInfo = new ArrayList<>();

    public Map<Long, TreeNode> treeNodeExpanded = new HashMap<>();

    private List<NodeInfo> listNodeInfo = new ArrayList<>();
    private List<NodeInfo> listNodeInfoToDelete = new ArrayList<>();

    private boolean disPlays;
    private boolean action;
    private boolean assessmentRuleEditMoe;
    private boolean renderedCbb;
    private boolean fuzzyEditMode;
    private boolean expressionEditMode;
    private boolean expressionParentEditMode;
    private boolean groupEditMode;
    private boolean groupParentEditMode;
    private boolean showFuzzyMode;
    private boolean categoryEditMode;
    private boolean editMode;
    private boolean focusCombobox = false;
    private boolean focusValuEx = false;
    private boolean focusComboboxResult = false;

    private int checkOption;
    private int checkOptionClick;
    private int dem;
    private int count;

    private String objectTypeToEdit = "";
    private String ppuNameMap;
    private String kpiNameMap;

    @Autowired
    private TreeUtilsService treeUtilsService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AssessmentRuleService assessmentRuleService;
    @Autowired
    private UtilsService utilsService;

    @ManagedProperty(value = "#{ppuEvaluationController}")
    PPUEvaluationController ppuEvaluationController;

    @ManagedProperty(value = "#{kpiProcessorController}")
    KpiProcessorController kpiProcessorController;

    @PostConstruct
    public void init() {
        this.listAssessmentRule = new ArrayList<>();
        this.categories = new ArrayList<>();
        this.listAssessmentRulePreProcessUnitMap = new ArrayList<>();
        this.listAssessmentRuleKpiProcessorMap = new ArrayList<>();
        this.listPpuEvaluation = new ArrayList<>();
        this.listKpiProcessor = new ArrayList<>();
        this.listResultClass = new ArrayList<>();
        this.listFuzzyRule = new ArrayList<>();
        this.listPpuEvaluationToDelete = new ArrayList<>();
        this.listKpiProcessorToDelete = new ArrayList<>();
        this.listCampaignEvaluationInfo = new ArrayList<>();
        this.listSegmentEvaluationInfo = new ArrayList<>();
        initTreeNode();
    }

    public void initTreeNode() {
        this.categories = categoryService.getCategoryByType(CatagoryType.CATEGORY_ASSESSMENT_RULE_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.listAssessmentRule = categoryService.getDataFromCatagoryId(longs, Constants.ASSESSMENT_RULE);
        rootNode = treeUtilsService.createTreeCategoryAndComponentCampaignEvaluation(categories, listAssessmentRule);
        rootNode.getChildren().get(0).setExpanded(true);
        addExpandedNode(rootNode.getChildren().get(0));

    }

    // ASESSMENT_RULE
    //edit
    public void prepareUpdateAssessmentRule() {
        //initTreeNode();
        this.objectTypeToEdit = "object";
        this.disPlays = true;
        this.editMode = true;
        this.action = true;

        this.listKpiProcessorToDelete = new ArrayList<>();
        this.listPpuEvaluationToDelete = new ArrayList<>();
        this.listResultClassToDelete = new ArrayList<>();
        this.listFuzzyRuleToDelete = new ArrayList<>();
        this.assessmentRuleEditMoe = true;
        AssessmentRule asssessment = (AssessmentRule) selectedNode.getData();
        this.assessmentRule = assessmentRuleService.findAssessmentRuleById(asssessment.getRuleId());
        this.currenRootNode = this.selectedNode;
        this.backupAssessmentRule = new AssessmentRule();
        cloneAssessmentRule(backupAssessmentRule, assessmentRule);
        this.listPpuEvaluation = assessmentRuleService.getListPpuEvaluationByRuleId(assessmentRule.getRuleId());
        this.listKpiProcessor = assessmentRuleService.getListKpiProcessorByRuleId(assessmentRule.getRuleId());
        this.listResultClass = assessmentRuleService.getListResultClassByAssessmentRuleId(assessmentRule.getRuleId());
        this.listAssessmentRulePreProcessUnitMap = assessmentRuleService.getAllAssessmentRulePreProcessUnitMapById(assessmentRule.getRuleId());
        this.listAssessmentRuleKpiProcessorMap = assessmentRuleService.getAlllistAssessmentRuleKpiProcessorMapById(assessmentRule.getRuleId());
        this.listFuzzyRule = assessmentRuleService.getListFuzzyRuleByRuleId(assessmentRule.getRuleId());
        this.selectedNode.getChildren().clear();
        dequyAssessmentRule(this.rootNode);

    }

    public void dequyAssessmentRule(TreeNode rootNode) {
        List<TreeNode> lstChildrent = rootNode.getChildren();

        for (TreeNode it : lstChildrent) {

            if ("object".equals(it.getType())) {
                AssessmentRule a = (AssessmentRule) it.getData();
                AssessmentRule b = (AssessmentRule) this.selectedNode.getData();
                if (Objects.equals(a.getRuleId(), b.getRuleId())) {
                    TreeNode root = it;
                    KpiProcessor kpi = new KpiProcessor();
                    kpi.setProcessorName("KPI List");
                    TreeNode kpiNode = new DefaultTreeNode("folderkpi", kpi, root);
                    kpiNode.setExpanded(true);
                    for (KpiProcessor items : listKpiProcessor) {
                        TreeNode kpiN = new DefaultTreeNode("kpi", items, kpiNode);
                        kpiN.setExpanded(true);
                    }

                    PpuEvaluation ppu = new PpuEvaluation();
                    ppu.setPreProcessName("PPU List");
                    TreeNode ppuNode = new DefaultTreeNode("folderppu", ppu, root);
                    ppuNode.setExpanded(true);
                    for (PpuEvaluation itemm : listPpuEvaluation) {
                        TreeNode ppun = new DefaultTreeNode("ppu", itemm, ppuNode);
                        ppun.setExpanded(true);
                    }
                    root.setExpanded(true);

                    expandCurrentNode(root);
                    break;
                }
            }
            if ("category".equals(it.getType())) {
                dequyAssessmentRule(it);
            }

        }

    }
    //create

    public void prepareCreateAssessmentRule() {
        this.objectTypeToEdit = "object";
        this.disPlays = true;
        this.editMode = false;
        this.action = true;

        this.assessmentRuleEditMoe = false;
        this.category = (Category) selectedNode.getData();
        this.assessmentRule = new AssessmentRule();
        this.currenRootNode = null;
        this.assessmentRule = assessmentRuleService.getNextAssessmentRule();
        this.assessmentRule.setCategoryId(this.category.getCategoryId());
        this.listPpuEvaluation = new ArrayList<>();
        this.listKpiProcessor = new ArrayList<>();
        this.listResultClass = new ArrayList<>();
        this.listFuzzyRule = new ArrayList<>();
        this.listAssessmentRulePreProcessUnitMap = new ArrayList<>();
        this.listAssessmentRuleKpiProcessorMap = new ArrayList<>();
        this.listKpiProcessorToDelete = new ArrayList<>();
        this.listPpuEvaluationToDelete = new ArrayList<>();
        this.listResultClassToDelete = new ArrayList<>();
        this.listFuzzyRuleToDelete = new ArrayList<>();
        this.backupAssessmentRule = new AssessmentRule();
        cloneAssessmentRule(backupAssessmentRule, assessmentRule);
    }

    public void onResetAssessmentRule() {
        if (this.editMode == true) {
            this.assessmentRule = backupAssessmentRule;
            this.disPlays = true;
            this.editMode = true;
            this.action = true;

            this.assessmentRuleEditMoe = true;
            this.listPpuEvaluation = assessmentRuleService.getListPpuEvaluationByRuleId(assessmentRule.getRuleId());
            this.listKpiProcessor = assessmentRuleService.getListKpiProcessorByRuleId(assessmentRule.getRuleId());
            this.listResultClass = assessmentRuleService.getListResultClassByAssessmentRuleId(assessmentRule.getRuleId());
            this.listAssessmentRulePreProcessUnitMap = assessmentRuleService.getAllAssessmentRulePreProcessUnitMapById(assessmentRule.getRuleId());
            this.listAssessmentRuleKpiProcessorMap = assessmentRuleService.getAlllistAssessmentRuleKpiProcessorMapById(assessmentRule.getRuleId());
            this.listFuzzyRule = assessmentRuleService.getListFuzzyRuleByRuleId(assessmentRule.getRuleId());
            this.listKpiProcessorToDelete = new ArrayList<>();
            this.listPpuEvaluationToDelete = new ArrayList<>();
            this.listResultClassToDelete = new ArrayList<>();
            this.listFuzzyRuleToDelete = new ArrayList<>();
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        } else {
            cloneAssessmentRule(assessmentRule, backupAssessmentRule);
            this.listPpuEvaluation = new ArrayList<>();
            this.listKpiProcessor = new ArrayList<>();
            this.listResultClass = new ArrayList<>();
            this.listFuzzyRule = new ArrayList<>();
            this.listKpiProcessorToDelete = new ArrayList<>();
            this.listPpuEvaluationToDelete = new ArrayList<>();
            this.listResultClassToDelete = new ArrayList<>();
            this.listFuzzyRuleToDelete = new ArrayList<>();
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public void cloneAssessmentRule(AssessmentRule a, AssessmentRule b) {
        a.setRuleId(b.getRuleId());
        a.setRuleName(b.getRuleName());
        a.setCategoryId(b.getCategoryId());
        a.setDescription(b.getDescription());
    }

    public void editAssessmentRuleName() {
        this.assessmentRule.setRuleName(assessmentRule.getRuleName().replaceAll(" +", " "));

    }

    public void editAssessmentDescription() {
        this.assessmentRule.setDescription(assessmentRule.getDescription().replaceAll(" +", " "));
    }

    public boolean validateAssessmentRule() {
        AssessmentRule assessmentRuleCheck = this.assessmentRule;
        if (DataUtil.isStringNullOrEmpty(assessmentRuleCheck.getRuleName())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.assessment.name"));
            return false;
        }
        if (this.editMode == true) {
            for (AssessmentRule item : listAssessmentRule) {
                if (Objects.equals(assessmentRuleCheck.getRuleId(), item.getRuleId())) {
                    continue;
                } else if (assessmentRuleCheck.getRuleName().equals(item.getRuleName())) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.assessment.name.unique"));
                    return false;
                }
            }
        }
        if (this.editMode == false) {
            for (AssessmentRule items : listAssessmentRule) {

                if (assessmentRuleCheck.getRuleName().equals(items.getRuleName())) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.assessment.name.unique"));
                    return false;
                }

            }
        }
        if (!DataUtil.checkMaxlength(assessmentRuleCheck.getRuleName())) {

            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.assessment.maxlenght"), "Assessment Rule Name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(assessmentRuleCheck.getRuleName())) {
            //this.assessmentRule.setRuleName(backupAssessmentRule.getRuleName());
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.assessment.name.characters"));
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(assessmentRuleCheck.getDescription())) {
            //this.assessmentRule.setDescription(backupAssessmentRule.getDescription());
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.special.characters"), "Description");

            return false;
        }
        if (!DataUtil.checkMaxlength(assessmentRuleCheck.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.assessment.maxlenght"), "Description");
            return false;
        }
        if (validateResultClass() == false) {
            return false;
        }
        this.count = 0;
        for (FuzzyRule item : listFuzzyRule) {
            if (item != null) {
                Double value = item.getWeight() * 100;
                count = count + value.intValue();
            }

        }
        if (count != 100) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.count.weight"), "fuzzy");
            return false;
        }
        if (validateValuaExDoSaveFuzzy() == false) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.ValuaEx"));
            return false;
        }

        return true;
    }

    public void prepareEditWhenClick() {
        if ("object".equals(selectedNode.getType())) {
            this.objectTypeToEdit = "object";
            prepareUpdateAssessmentRule();
            this.action = false;

            // Load list kpi, ppu tạo node gán lên cây
            // ...............................
        }
        if ("category".equals(selectedNode.getType())) {
            this.objectTypeToEdit = "category";
            preapreUpdateCategory();
            PrimeFaces.current().executeScript("PF('carDialog').show();");
        }

        if ("kpi".equals(selectedNode.getType())) {
            this.objectTypeToEdit = "kpi";
            kpiProcessorController.setSelectedNode(selectedNode);
            kpiProcessorController.prepareEditWhenClick();
            kpiProcessorController.setDisplay(true);
        }
        if ("ppu".equals(selectedNode.getType())) {
            this.objectTypeToEdit = "ppu";
            ppuEvaluationController.setSelectedNode(selectedNode);
            ppuEvaluationController.prepareEditWhenClick();
            ppuEvaluationController.setDisplay(true);
        }
        if ("folderkpi".equals(selectedNode.getType())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.selectNode"), "KPI List");
        }
        if ("folderppu".equals(selectedNode.getType())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.selectNode"), "PPU List");
        }
    }

    // PPU
    public void buildTreePPUCreate() {
//        this.ppuEditMode = false;
        this.categoryPPU = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_PRE_PROCESS_UNIT_INFO_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categoryPPU.isEmpty()) {
            categoryPPU.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.listPpuEvaluationBuildTree = assessmentRuleService.getListPpuEvaluationTree(longs);
        ppuNode = treeUtilsService.createTreeCategoryAndComponentCampaignEvaluation(categoryPPU, listPpuEvaluationBuildTree);
        ppuNode.getChildren().get(0).setExpanded(true);
        addExpandedNode(rootNode.getChildren().get(0));
    }

    public void choosePPU() {
        this.ppuEvaluation = (PpuEvaluation) selectedNode.getData();
        this.listPpuEvaluation.add(this.ppuEvaluation);
        this.assessmentRulePreProcessUnitMap = new AssessmentRulePreProcessUnitMap();
        assessmentRulePreProcessUnitMap = assessmentRuleService.getNextAssessmentRulePreProcessUnitMap();
        assessmentRulePreProcessUnitMap.setPreProcessUnitId(this.ppuEvaluation.getPreProcessId());
        assessmentRulePreProcessUnitMap.setRuleId(assessmentRule.getRuleId());
        this.listAssessmentRulePreProcessUnitMap.add(assessmentRulePreProcessUnitMap);

    }

    public boolean validatePPU() {
        if (!Objects.nonNull(selectedNode)) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
            return false;
        }
        if (selectedNode.getType().equals("category")) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
            return false;
        } else {
            this.ppuEvaluation = (PpuEvaluation) selectedNode.getData();
            this.dem = 0;
            for (PpuEvaluation item : listPpuEvaluation) {
                if (Objects.equals(item.getPreProcessId(), this.ppuEvaluation.getPreProcessId())) {
                    dem++;
                }
            }
            if (dem != 0) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.ppu"));
                return false;
            }
        }

        return true;
    }

    public void deletePPU(PpuEvaluation ppuEvaluation) {
        this.ppuEvaluation = ppuEvaluation;
        if (validateDeletePPU()) {
            this.listPpuEvaluation.remove(ppuEvaluation);
            this.listPpuEvaluationToDelete.add(ppuEvaluation);

//        this.ppuNameMap = ppuEvaluation.getCriteriaName();
//        if (!this.listFuzzyRule.isEmpty()) {
//            List<Long> longs = checkChooseNameInFuzzyRule(ppuNameMap, this.listFuzzyRule);
//            for (Long id : longs) {
//                for (FuzzyRule items : listFuzzyRule) {
//                    if (Objects.equals(id, items.getFuzzyRuleId())) {
//                        this.listFuzzyRuleToDelete.add(items);
//                        this.listFuzzyRule.remove(items);
//                        if (this.listFuzzyRule.isEmpty()) {
//                            break;
//                        }
//                    }
//                }
//            }
//        }
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        } else {

            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.name.ppu"));
        }

    }

    public List<Long> checkChooseNameInFuzzyRule(String name, List<FuzzyRule> lst) {
        List<Long> longs = new ArrayList<>();
        for (FuzzyRule item : lst) {
            List<NodeInfo> listDisplayExpression = new ArrayList<>();
            listDisplayExpression.addAll(getListNodeInfoFromDisplayExpress(item.getDisplayExpression()));
            for (NodeInfo items : listDisplayExpression) {
                String[] text = items.getDisplay().split(" ");
                for (int i = 0; i < text.length; i++) {
                    if ("is".equals(text[i])) {
                        if (items.getDisplay().substring(0, items.getDisplay().indexOf(text[i]) - 1).trim().equals(name)) {
                            longs.add(item.getFuzzyRuleId());
                        }
                    }
                    if ("not".equals(text[i])) {
                        if (items.getDisplay().substring(0, items.getDisplay().indexOf(text[i]) - 1).trim().equals(name)) {
                            longs.add(item.getFuzzyRuleId());
                        }
                    }
                }
            }
        }
        return longs;
    }

    public boolean validateDeletePPU() {
        List<NodeInfo> allListNodeInfor = getAllNodeInfoOfListfuzzyRule();
        Boolean validate = true;
        validate = checkNameIsUsedWithContentExInExpressionOfListFuzzyRule(this.ppuEvaluation.getCriteriaName(), allListNodeInfor);
        return validate;
    }

    public boolean validateDeleteKPI() {
        List<NodeInfo> allListNodeInfor = getAllNodeInfoOfListfuzzyRule();
        Boolean validate = true;
        validate = checkNameIsUsedWithContentExInExpressionOfListFuzzyRule(this.kpiProcessor.getCriteriaName(), allListNodeInfor);
        return validate;
    }

    public Boolean checkNameIsUsedWithContentExInExpressionOfListFuzzyRule(String name, List<NodeInfo> allListNodeInfor) {
        if (!"".equals(name)) {
            for (NodeInfo item : allListNodeInfor) {
                String[] text = item.getDisplay().split(" ");
                for (int i = 0; i < text.length; i++) {
                    if ("is".equals(text[i])) {
                        if (item.getDisplay().substring(0, item.getDisplay().indexOf(text[i]) - 1).trim().equals(name)) {
                            return false;
                        }
                    }
                    if ("not".equals(text[i])) {
                        if (item.getDisplay().substring(0, item.getDisplay().indexOf(text[i]) - 1).trim().equals(name)) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public boolean validateValuaExDoSaveFuzzy() {
        List<NodeInfo> allListNodeInfor = getAllNodeInfoOfListfuzzyRule();
        Boolean validate = true;
        validate = checkNameClassNameNotIsUsedWithContextExInExpressionOfListFuzzyRule(allListNodeInfor);
        return validate;
    }

    public boolean validateDeleteResultClassNameInCampaignEAndSegmentE() {
        List<NodeInfo> allListNodeInfor = getAllNodeInfoOfListfuzzyRuleOfCampaignEvaluationNotDefault();
        Boolean validate = true;
        validate = checkResultClassNameNotIsUsedWithValueExInExpressionOfListFuzzyRule(this.resultClass.getClassName(), allListNodeInfor);
        return validate;
    }

    public List<NodeInfo> getAllNodeInfoOfListfuzzyRule() {
        List<NodeInfo> allListNodeInfor = new ArrayList<>();
        for (FuzzyRule item : this.listFuzzyRule) {
            allListNodeInfor.addAll(getListNodeInfoFromDisplayExpress(item.getDisplayExpression()));
        }
        return allListNodeInfor;
    }

    public List<NodeInfo> getAllNodeInfoOfListfuzzyRuleOfCampaignEvaluationNotDefault() {
        List<NodeInfo> allListNodeInfor = new ArrayList<>();
        for (FuzzyRule item : this.listFuzzyRuleToCheck) {
            allListNodeInfor.addAll(getListNodeInfoFromDisplayExpress(item.getDisplayExpression()));
        }
        return allListNodeInfor;
    }

    public Boolean checkResultClassNameNotIsUsedWithValueExInExpressionOfListFuzzyRule(String name, List<NodeInfo> allListNodeInfor) {
        if (!"".equals(name)) {
            for (NodeInfo item : allListNodeInfor) {
                String[] text = item.getDisplay().split(" ");
                for (int i = 0; i < text.length; i++) {
                    if ("is".equals(text[i])) {
                        if (item.getDisplay().substring(item.getDisplay().indexOf(text[i]) + 2, item.getDisplay().length()).trim().equals(name)) {
                            return false;
                        }
                    }
                    if ("not".equals(text[i])) {
                        if (item.getDisplay().substring(item.getDisplay().indexOf(text[i]) + 3, item.getDisplay().length()).trim().equals(name)) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public Boolean checkNameClassNameNotIsUsedWithContextExInExpressionOfListFuzzyRule(List<NodeInfo> allListNodeInfor) {
        for (NodeInfo item : allListNodeInfor) {
            String[] text = item.getDisplay().split(" ");
            for (int i = 0; i < text.length; i++) {
                if ("is".equals(text[i])) {
                    for (PpuEvaluation it : listPpuEvaluation) {
                        if (item.getDisplay().substring(0, item.getDisplay().indexOf(text[i]) - 1).trim().equals(it.getCriteriaName())) {
                            ProcessValue processValue = assessmentRuleService.checkValueName(item.getDisplay().substring(item.getDisplay().indexOf(text[i]) + 2, item.getDisplay().length()).trim(), it.getPreProcessId());
                            if (processValue == null) {
                                return false;
                            }
                        }
                    }
                    for (KpiProcessor it : listKpiProcessor) {
                        if (item.getDisplay().substring(0, item.getDisplay().indexOf(text[i]) - 1).trim().equals(it.getCriteriaName())) {
                            CriteriaValue criteriaValue = assessmentRuleService.chekCriteriaValueName(item.getDisplay().substring(item.getDisplay().indexOf(text[i]) + 2, item.getDisplay().length()).trim(), it.getProcessorId());
                            if (criteriaValue == null) {
                                return false;
                            }
                        }
                    }
                }
                if ("not".equals(text[i])) {
                    for (PpuEvaluation it : listPpuEvaluation) {
                        if (item.getDisplay().substring(0, item.getDisplay().indexOf(text[i]) - 1).trim().equals(it.getCriteriaName())) {
                            ProcessValue processValue = assessmentRuleService.checkValueName(item.getDisplay().substring(item.getDisplay().indexOf(text[i]) + 3, item.getDisplay().length()).trim(), it.getPreProcessId());
                            if (processValue == null) {
                                return false;
                            }
                        }
                    }
                    for (KpiProcessor it : listKpiProcessor) {
                        if (item.getDisplay().substring(0, item.getDisplay().indexOf(text[i]) - 1).trim().equals(it.getCriteriaName())) {
                            CriteriaValue criteriaValue = assessmentRuleService.chekCriteriaValueName(item.getDisplay().substring(item.getDisplay().indexOf(text[i]) + 3, item.getDisplay().length()).trim(), it.getProcessorId());
                            if (criteriaValue == null) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    // KPI
    public void buildKpiprocessorCreate() {
        this.categoryKpi = categoryService.getCategoryByType(Constants.CatagoryType.CATEORY_KPI_PROCESSOR_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categoryKpi.isEmpty()) {
            categoryKpi.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.listKpiProcessorBuildTree = categoryService.getDataFromCatagoryId(longs, Constants.KPI_PROCESSOR);
        kpiNode = treeUtilsService.createTreeCategoryAndComponentCampaignEvaluation(categoryKpi, listKpiProcessorBuildTree);
        kpiNode.getChildren().get(0).setExpanded(true);
        addExpandedNode(kpiNode.getChildren().get(0));
    }

    public void chooseKPI() {
        this.listKpiProcessor.add(this.kpiProcessor);
        this.assessmentRuleKpiProcessorMap = new AssessmentRuleKpiProcessorMap();
        this.assessmentRuleKpiProcessorMap = assessmentRuleService.getNextAssessmentRuleKpiProcessorMap();
        this.assessmentRuleKpiProcessorMap.setProcessorId(this.kpiProcessor.getProcessorId());
        this.assessmentRuleKpiProcessorMap.setRuleId(assessmentRule.getRuleId());
        this.listAssessmentRuleKpiProcessorMap.add(this.assessmentRuleKpiProcessorMap);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));

    }

    public void deleteKPI(KpiProcessor kpiProcessor) {
        this.kpiProcessor = kpiProcessor;

        if (validateDeleteKPI()) {
            this.listKpiProcessor.remove(kpiProcessor);
            this.listKpiProcessorToDelete.add(kpiProcessor);
//        this.kpiNameMap = kpiProcessor.getCriteriaName();
//        if (!this.listFuzzyRule.isEmpty()) {
//            List<Long> longs = checkChooseNameInFuzzyRule(kpiNameMap, this.listFuzzyRule);
//            for (Long id : longs) {
//                for (FuzzyRule items : listFuzzyRule) {
//                    if (Objects.equals(id, items.getFuzzyRuleId())) {
//                        this.listFuzzyRuleToDelete.add(items);
//                        this.listFuzzyRule.remove(items);
//                        if (this.listFuzzyRule.isEmpty()) {
//                            break;
//                        }
//                    }
//                }
//            }
//        }
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.name.kpi"));
        }

    }

    public boolean validateKPI() {
        if (!Objects.nonNull(selectedNode)) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
            return false;
        }
        if (selectedNode.getType().equals("category")) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
            return false;
        } else {
            this.kpiProcessor = (KpiProcessor) selectedNode.getData();
            this.dem = 0;
            for (KpiProcessor item : listKpiProcessor) {
                if (Objects.equals(item.getProcessorId(), this.kpiProcessor.getProcessorId())) {
                    dem++;
                }
            }
            if (dem != 0) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.kpi"));
                return false;
            }
        }
        return true;
    }

    // RESULT CLASS
    public void createResultClass() {
        this.resultClass = new ResultClass();
        this.resultClass = assessmentRuleService.getNextSequenceResultClass();
        this.resultClass.setClassLevel(1);
        this.resultClass.setOwnerId(this.assessmentRule.getRuleId());
        this.listResultClass.add(this.resultClass);

    }

    public void editResultClassName() {
        for (ResultClass item : listResultClass) {
            if (item != null) {
                item.setClassName(item.getClassName().replaceAll(" +", " "));
                item.setDescription(item.getDescription().replaceAll(" +", " "));
            }
        }
        for (FuzzyRule item : listFuzzyRule) {
            for (ResultClass items : listResultClass) {
                if (Objects.equals(item.getClassId(), items.getClassId())) {
                    item.setClassName(items.getClassName());
                }
            }
        }
    }

    public void deleteResultClass(ResultClass resultClass) {
        this.resultClass = resultClass;
        if (validateDeleteResult()) {
            this.listResultClass.remove(resultClass);
            this.listResultClassToDelete.add(resultClass);
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public boolean validateDeleteResult() {
        this.listCampaignEvaluationInfo = assessmentRuleService.getListCampaignEvalurationMapAssessment(this.assessmentRule.getRuleId());
        this.listSegmentEvaluationInfo = assessmentRuleService.getListSegmentEvaluationInfoMapAssessment(this.assessmentRule.getRuleId());
        this.listFuzzyRuleToCheck = new ArrayList<>();
        if (!listCampaignEvaluationInfo.isEmpty()) {
            for (CampaignEvaluationInfo item : listCampaignEvaluationInfo) {
                this.listFuzzyRuleToCheck.addAll(assessmentRuleService.getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(item.getCampaignId(), item.getCampaignEvaluationInfoId()));
            }
        }
        if (!listSegmentEvaluationInfo.isEmpty()) {
            for (SegmentEvaluationInfo item : listSegmentEvaluationInfo) {
                this.listFuzzyRuleToCheck.addAll(assessmentRuleService.getListFuzzyRuleOfSegment(item.getSegmentId(), item.getCampaignEvaluationInfoId()));
            }
        }
        if (validateDeleteResultClassNameInCampaignEAndSegmentE() == false) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.result.nameInCamapignESegmentE"));
            return false;
        }
        this.dem = 0;
        for (FuzzyRule item : listFuzzyRule) {
            if (Objects.equals(resultClass.getClassId(), item.getClassId())) {
                dem++;
            }
        }
        if (dem != 0) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.result.name"));
            return false;
        } else {
            return true;
        }
    }

    public boolean validateResultClass() {
        if (listResultClass.isEmpty()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "List Result Class");
            return false;
        }
        for (ResultClass item : listResultClass) {
            if (!DataUtil.checkNotContainPercentage(item.getClassName())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.special.characters"), "Result class");
                return false;
            }
            if (DataUtil.isStringNullOrEmpty(item.getClassName())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Result Class Name");
                return false;
            }
            if (!DataUtil.checkMaxlength(item.getClassName())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Result Class Name");
                return false;
            }
            if (!DataUtil.checkMaxlength(item.getDescription())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description Result class");
                return false;
            }
            if (!DataUtil.checkNotContainPercentage(item.getDescription())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.special.characters"), "Description Result class");
                return false;
            }
        }
        for (ResultClass item : listResultClass) {
            for (ResultClass items : listResultClass) {
                if (Objects.equals(item.getClassId(), items.getClassId())) {
                    continue;
                }
                if (item.getClassName().equals(items.getClassName())) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.resultName"));
                    return false;
                }
            }
        }

        return true;
    }

    // FUZZY RULE
    public void prepareEditFuzzyRule(FuzzyRule fuzzyRule) {
        if (DataUtil.isStringNullOrEmpty(this.assessmentRule.getRuleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Assessment Rule Name");
        } else {
            if (validateResultClass()) {
                this.showFuzzyMode = false;
                this.focusComboboxResult = false;
                this.fuzzyEditMode = true;
                this.listResultClassMap = new ArrayList<>();
                this.listResultClassMap.addAll(this.listResultClass);
                this.fuzzyRuleEdit = new FuzzyRule();
                setFuzzyRule(fuzzyRuleEdit, fuzzyRule);
                //this.fuzzyRule = fuzzyRule;
                this.listNodeInfo = getListNodeInfoFromDisplayExpress(this.fuzzyRuleEdit.getDisplayExpression());
                this.rootNodeFuzzy = genTreeFuzzy(listNodeInfo);
                for (NodeInfo item : listNodeInfo) {
                    if (item.getParentId() == null) {
                        this.nodeDefineDTO = new NodeDefineDTO();
                        nodeDefineDTO.setOperatorDT(item.getOperator());
                        this.dem = 0;
                        for (NodeInfo items : listNodeInfo) {
                            if (Objects.equals(items.getParentId(), item.getId())) {
                                dem++;
                            }
                        }
                        if (dem >= 2) {
                            this.renderedCbb = true;
                        } else {
                            this.renderedCbb = false;
                        }
                    }
                }
            }
        }

    }

    public void setFuzzyRule(FuzzyRule a, FuzzyRule b) {

        a.setCampaignEvaluationInfoId(b.getCampaignEvaluationInfoId());
        a.setClassId(b.getClassId());
        a.setClassName(b.getClassName());
        a.setDescription(b.getDescription());
        a.setDisplayExpression(b.getDisplayExpression());
        a.setFuzzyRuleId(b.getFuzzyRuleId());
        a.setFuzzyOperator(b.getFuzzyOperator());
        a.setExpression(b.getExpression());
        a.setOwnerId(b.getOwnerId());
        a.setWeight(b.getWeight());
        a.setRuleLevel(b.getRuleLevel());

    }

    public void chooseOperatorForParent() {
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                item.setOperator(this.nodeDefineDTO.getOperatorDT());
                item.setDisplay("Group" + " " + "-" + " " + this.nodeDefineDTO.getOperatorDT());
            }
        }
        this.rootNodeFuzzy = genTreeFuzzy(this.listNodeInfo);
        TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfo);
        genField(togenfield);
    }

    public void prepareCreateFuzzyRule() {
        if (DataUtil.isStringNullOrEmpty(this.assessmentRule.getRuleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Assessment Rule Name");
        } else {

            if (validateResultClass()) {
                this.showFuzzyMode = false;
                this.focusComboboxResult = false;
                this.fuzzyRuleEdit = new FuzzyRule();
                this.fuzzyRule = new FuzzyRule();
                this.listResultClassMap = new ArrayList<>();
                this.listResultClassMap.addAll(this.listResultClass);
                this.fuzzyRule = assessmentRuleService.getNextFuzzyRule();
                this.fuzzyRule.setOwnerId(assessmentRule.getRuleId());
                this.fuzzyRule.setRuleLevel(1L);
                this.fuzzyRule.setDisplayExpression("");
                setFuzzyRule(fuzzyRuleEdit, fuzzyRule);
                this.fuzzyEditMode = false;
                this.listNodeInfo = new ArrayList<>();
                NodeInfo item = new NodeInfo();
                item.setId(1L);
                item.setParentId(null);
                item.setType("group");
                item.setValue("");
                this.renderedCbb = false;
                this.listNodeInfo.add(item);
                nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT("and");
                item.setOperator(nodeDefineDTO.getOperatorDT());
                rootNodeFuzzy = genTreeFuzzy(this.listNodeInfo);
            }
        }

    }

    public void showFuzzyRule(FuzzyRule fuzzyRule) {
        this.showFuzzyMode = true;
        this.fuzzyEditMode = true;
        this.listResultClassMap = new ArrayList<>();
        this.listResultClassMap.addAll(this.listResultClass);
        this.fuzzyRuleEdit = new FuzzyRule();
        setFuzzyRule(fuzzyRuleEdit, fuzzyRule);
        this.listNodeInfo = getListNodeInfoFromDisplayExpress(this.fuzzyRuleEdit.getDisplayExpression());
        this.rootNodeFuzzy = genTreeFuzzy(listNodeInfo);
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                this.nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT(item.getOperator());
                this.dem = 0;
                for (NodeInfo items : listNodeInfo) {
                    if (Objects.equals(items.getParentId(), item.getId())) {
                        dem++;
                    }
                }
                if (dem >= 2) {
                    this.renderedCbb = true;
                } else {
                    this.renderedCbb = false;
                }
            }
        }
    }

    public void deleteFuzzyRule(FuzzyRule fuzzyRule) {
        this.listFuzzyRule.remove(fuzzyRule);
        this.listFuzzyRuleToDelete.add(fuzzyRule);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public void doSaveFuzzyRule() {
        if (this.fuzzyEditMode == false) {
            for (ResultClass item : listResultClass) {
                if (Objects.equals(fuzzyRuleEdit.getClassId(), item.getClassId())) {
                    this.fuzzyRuleEdit.setClassName(item.getClassName());
                }
            }

            setFuzzyRule(this.fuzzyRule, fuzzyRuleEdit);

            this.listFuzzyRule.add(this.fuzzyRule);
            for (FuzzyRule item : listFuzzyRule) {
                item.setExpression(item.getDisplayExpression().trim().toLowerCase()
                        .replace("and", "&")
                        .replace("not", "#")
                        .replace("or", "||")
                        .replace("is", "_").replaceAll(" +", "_"));

            }
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
        if (this.fuzzyEditMode == true) {
            for (ResultClass item : listResultClass) {
                if (Objects.equals(fuzzyRuleEdit.getClassId(), item.getClassId())) {
                    this.fuzzyRuleEdit.setClassName(item.getClassName());
                }
            }
            for (FuzzyRule item : listFuzzyRule) {
                if (Objects.equals(item.getFuzzyRuleId(), fuzzyRuleEdit.getFuzzyRuleId())) {
                    setFuzzyRule(item, fuzzyRuleEdit);
                }
                item.setExpression(item.getDisplayExpression().trim().toLowerCase()
                        .replace("and", "&")
                        .replace("not", "#")
                        .replace("or", "||")
                        .replace("is", "_").replaceAll(" +", "_"));
            }
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public boolean validateTreeFuzzy() {
        Boolean check = true;
        for (NodeInfo it : listNodeInfo) {
            if ("group".equals(it.getType())) {
                if (countChild(it, listNodeInfo) < 2) {
                    check = false;
                }
            }
        }
        return check;
    }

    public int countChild(NodeInfo nodeParent, List<NodeInfo> listNodeInfo) {
        int count = 0;
        for (NodeInfo item : listNodeInfo) {
            if (Objects.equals(item.getParentId(), nodeParent.getId())) {
                count += 1;
            }
        }
        return count;
    }

    public boolean validateFuzzy() {
        FuzzyRule fuzzyRule = this.fuzzyRuleEdit;
        if (fuzzyRule.getWeight() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.emptyNumber"), "Fuzzy Weight");
            return false;
        }
        if (fuzzyRule.getWeight() < 0 || fuzzyRule.getWeight() > 1) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.weight"), "Fuzzy Weight");
            return false;
        }
        if (fuzzyRule.getClassId() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Result class");
//            PrimeFaces.current().executeScript("PF('carDialog').show();");
            this.focusComboboxResult = true;
            PrimeFaces.current().executeScript("PF('resuleClassFuzzyId').focus()");
            return false;
        }
        if (!DataUtil.checkMaxlength(fuzzyRule.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(fuzzyRule.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.special.characters"), "Display Expression");
            return false;
        }
        if (!DataUtil.checkMaxlength(fuzzyRule.getDisplayExpression())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Display Expression");
            return false;
        }
        if (DataUtil.isStringNullOrEmpty(fuzzyRule.getDisplayExpression())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Expression Display");
            return false;
        }
//        if (DataUtil.isStringNullOrEmpty(this.assessmentRule.getRuleName())) {
//            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Owner Name");
//            return false;
//        }
        if (!validateTreeFuzzy()) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.group.tree"));
            return false;
        }
//        if (validateValuaExDoSaveFuzzy() == false) {
//            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.ValuaEx"));
//            return false;
//        }
        return true;
    }

    public boolean validateExpresstion() {
        Expression expression = this.expression;
        if ("".equals(expression.getContentEx())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.expresstion"), "ContextEx");
            this.focusCombobox = true;
            return false;
        }
        if ("".equals(expression.getValueEx())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.valueEx"), "ValueEx");
            this.focusValuEx = true;
            return false;
        }
        return true;
    }

    // EXPRESSION
    public void prepareEditExpression() {
        this.focusCombobox = false;
        this.focusValuEx = false;
        this.expressionEditMode = true;
        NodeInfo nodeInfo = (NodeInfo) selectedNode.getData();
        expression = new Expression();
        String[] text = nodeInfo.getDisplay().split(" ");

        for (int i = 0; i < text.length; i++) {
            if ("is".equals(text[i])) {
                expression.setContentEx(nodeInfo.getDisplay().substring(0, nodeInfo.getDisplay().indexOf(text[i]) - 1).trim());
                expression.setOperatorEx("is");
                expression.setValueEx(nodeInfo.getDisplay().substring(nodeInfo.getDisplay().indexOf(text[i]) + 2, nodeInfo.getDisplay().length()).trim());
            }
            if ("not".equals(text[i])) {
                expression.setContentEx(nodeInfo.getDisplay().substring(0, nodeInfo.getDisplay().indexOf(text[i]) - 1).trim());
                expression.setOperatorEx("not");
                expression.setValueEx(nodeInfo.getDisplay().substring(nodeInfo.getDisplay().indexOf(text[i]) + 3, nodeInfo.getDisplay().length()).trim());
            }
        }
        this.checkOptionClick = 0;
        for (KpiProcessor item : listKpiProcessor) {
            if (item.getCriteriaName() != null) {
                if (item.getCriteriaName().equals(expression.getContentEx())) {
                    checkOptionClick++;
                }
            }
        }
        if (this.checkOptionClick != 0) {
            this.checkOption = 1;
        } else {
            this.checkOption = 2;
        }
        //set valueEx vao combo box
        if (this.checkOption == 1) {
            for (KpiProcessor item : listKpiProcessor) {
                if (item.getCriteriaName() != null) {
                    if (item.getCriteriaName().equals(expression.getContentEx())) {
                        this.listkpiProcessorCriteriaValueMap = assessmentRuleService.getListkpiProcessorCriteriaValueMapByProsessorId(item.getProcessorId());
                    }
                }
            }
            List<Long> longs = new ArrayList<>();
            for (KpiProcessorCriteriaValueMap item : listkpiProcessorCriteriaValueMap) {
                longs.add(item.getCriteriaValueId());
            }
            this.listCriteriaValue = assessmentRuleService.getListCriteriaValueDAO(longs);
        }
        if (this.checkOption == 2) {
            for (PpuEvaluation item : listPpuEvaluation) {
                if (item.getCriteriaName() != null) {
                    if (item.getCriteriaName().equals(expression.getContentEx())) {
                        this.listPreProcessValueMap = assessmentRuleService.getListPreProcessValueMapByPreProcessId(item.getPreProcessId());
                    }
                }
            }
            List<Long> longs = new ArrayList<>();
            if (!this.listPreProcessValueMap.isEmpty()) {
                for (PreProcessValueMap item : listPreProcessValueMap) {
                    longs.add(item.getProcessValueId());
                }
                this.listProcessValue = assessmentRuleService.getListProcessValueByListId(longs);
            } else {
                this.listProcessValue = new ArrayList<>();
            }
        }
    }

    // CREATE EXPRESSION
    public void prepareCreateExpression() {
        this.focusCombobox = false;
        this.focusValuEx = false;
        this.expressionEditMode = false;
        this.expressionParentEditMode = false;
        this.checkOption = 1;
        expression = new Expression();
        this.listProcessValue = new ArrayList<>();
        this.listCriteriaValue = new ArrayList<>();

    }

    // careate cho nut goc
    public void prepareCreateExpressionParent() {
        this.focusCombobox = false;
        this.focusValuEx = false;
        this.expressionEditMode = false;
        this.expressionParentEditMode = true;
        this.checkOption = 1;
        expression = new Expression();
        this.listProcessValue = new ArrayList<>();
        this.listCriteriaValue = new ArrayList<>();
    }

    public void chooseKpiOrPPU() {
        this.focusCombobox = false;
        this.focusValuEx = false;
        this.expression = expression;
        if (this.checkOption == 1) {
            for (KpiProcessor item : listKpiProcessor) {
                if (item.getCriteriaName() != null) {
                    if (item.getCriteriaName().equals(expression.getContentEx())) {
                        this.listkpiProcessorCriteriaValueMap = assessmentRuleService.getListkpiProcessorCriteriaValueMapByProsessorId(item.getProcessorId());
                    }
                }
            }
            List<Long> longs = new ArrayList<>();
            for (KpiProcessorCriteriaValueMap item : listkpiProcessorCriteriaValueMap) {
                longs.add((Long) item.getCriteriaValueId());
            }
            if (longs.isEmpty()) {
                this.listCriteriaValue = new ArrayList<>();
            } else {
//                this.listCriteriaValue = assessmentRuleService.getListCriteriaValueDAO(longs);
                this.listCriteriaValue = new ArrayList<>();
            }
        }
        if (this.checkOption == 2) {
            for (PpuEvaluation item : listPpuEvaluation) {
                if (item.getCriteriaName() != null) {
                    if (item.getCriteriaName().equals(expression.getContentEx())) {
                        this.listPreProcessValueMap = assessmentRuleService.getListPreProcessValueMapByPreProcessId(item.getPreProcessId());
                    }
                }
            }
            List<Long> longs = new ArrayList<>();
            for (PreProcessValueMap item : listPreProcessValueMap) {
                longs.add(item.getProcessValueId());
            }
            if (longs.isEmpty()) {
                this.listProcessValue = new ArrayList<>();
            } else {
//                this.listProcessValue = assessmentRuleService.getListProcessValueByListId(longs);
                this.listProcessValue = new ArrayList<>();
            }
        }

    }

    public void chooseCriteria() {
        expression = this.expression;
        if (this.checkOption == 1) {
            if ("".equals(expression.getContentEx())) {
                this.listCriteriaValue = new ArrayList<>();
            } else {
                for (KpiProcessor item : listKpiProcessor) {
                    if (item.getCriteriaName() != null) {
                        if (item.getCriteriaName().equals(expression.getContentEx())) {
                            this.listkpiProcessorCriteriaValueMap = assessmentRuleService.getListkpiProcessorCriteriaValueMapByProsessorId(item.getProcessorId());
                        }
                    }
                }

                List<Long> longs = new ArrayList<>();
                for (KpiProcessorCriteriaValueMap item : listkpiProcessorCriteriaValueMap) {
                    longs.add(item.getCriteriaValueId());
                }
                if (longs.isEmpty()) {
                    this.listCriteriaValue = new ArrayList<>();
                } else {
                    this.listCriteriaValue = assessmentRuleService.getListCriteriaValueDAO(longs);
                }
            }
        }
        if (this.checkOption == 2) {
            if ("".equals(expression.getContentEx())) {
                this.listProcessValue = new ArrayList<>();
            } else {
                for (PpuEvaluation item : listPpuEvaluation) {
                    if (item.getCriteriaName() != null) {
                        if (item.getCriteriaName().equals(expression.getContentEx())) {
                            this.listPreProcessValueMap = assessmentRuleService.getListPreProcessValueMapByPreProcessId(item.getPreProcessId());
                        }
                    }
                }
                List<Long> longs = new ArrayList<>();
                for (PreProcessValueMap item : listPreProcessValueMap) {
                    longs.add(item.getProcessValueId());
                }
                if (longs.isEmpty()) {
                    this.listProcessValue = new ArrayList<>();
                } else {
                    this.listProcessValue = assessmentRuleService.getListProcessValueByListId(longs);
                }
            }
        }
    }

    // SAVE EDIT EXPRESSION 
    public void saveOrEditExpression() {
        if (this.expressionEditMode == false) {
            if (this.expressionParentEditMode == false) {
                NodeInfo nodeSelect = (NodeInfo) selectedNode.getData();
                this.nodeInfo = new NodeInfo();

                Long maxId = 1L;
                for (NodeInfo item : listNodeInfo) {

                    if (item.getId() > maxId) {
                        maxId = item.getId();

                    }
                }

                this.nodeInfo.setId(maxId + 1);
                this.nodeInfo.setParentId(nodeSelect.getId());
                this.nodeInfo.setDisplay(expression.getContentEx() + " " + expression.getOperatorEx() + " " + expression.getValueEx());
                this.nodeInfo.setValue(expression.getContentEx() + " " + expression.getOperatorEx() + " " + expression.getValueEx());
                this.nodeInfo.setOperator(expression.getOperatorEx());
                this.nodeInfo.setType("object");

            }
            if (this.expressionParentEditMode == true) {

                this.nodeInfo = new NodeInfo();

                Long maxId = 1L;
                for (NodeInfo item : listNodeInfo) {
                    if (item.getId() > maxId) {
                        maxId = item.getId();
                    }
                }
                Long minId = 1L;
                for (NodeInfo item : listNodeInfo) {
                    if (item.getId() < minId) {
                        minId = item.getId();
                    }
                }
                this.nodeInfo.setId(maxId + 1);
                this.nodeInfo.setParentId(minId);
                this.nodeInfo.setDisplay(expression.getContentEx() + " " + expression.getOperatorEx() + " " + expression.getValueEx());
                this.nodeInfo.setValue(expression.getContentEx() + " " + expression.getOperatorEx() + " " + expression.getValueEx());
                this.nodeInfo.setOperator(expression.getOperatorEx());
                this.nodeInfo.setType("object");

            }
        }
        if (this.expressionEditMode == true) {
            NodeInfo nodeInfo = (NodeInfo) selectedNode.getData();
            for (NodeInfo item : listNodeInfo) {
                if (Objects.equals(item.getId(), nodeInfo.getId())) {
                    item.setDisplay(expression.getContentEx() + " " + expression.getOperatorEx() + " " + expression.getValueEx());
                }
            }

        }

//        rootNodeFuzzy = genTreeFuzzy(listNodeInfo);
        refreshTreeNodeInfo();
        TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfo);
        genField(togenfield);
    }
    //DELETE EXPRESSION OR GROUP

    public void deleteExpressionOrGroup() {
        NodeInfo nodeInfo = (NodeInfo) selectedNode.getData();
        this.count = 0;
        for (NodeInfo item : listNodeInfo) {
            if (Objects.equals(item.getParentId(), nodeInfo.getId())) {
                this.count++;
            }
        }
        if (count >= 1) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.delete.group"));
        } else {
            listNodeInfo.remove(nodeInfo);
            rootNodeFuzzy = genTreeFuzzy(listNodeInfo);
            for (NodeInfo item : listNodeInfo) {
                if (item.getParentId() == null) {
                    this.nodeDefineDTO = new NodeDefineDTO();
                    nodeDefineDTO.setOperatorDT(item.getOperator());
                    this.dem = 0;
                    for (NodeInfo items : listNodeInfo) {
                        if (Objects.equals(items.getParentId(), item.getId())) {
                            dem++;
                        }
                    }
                    if (dem >= 2) {
                        this.renderedCbb = true;
                    } else {
                        this.renderedCbb = false;
                    }
                }
            }
            genField(genTreeFuzzyToGenDisplayExpression(listNodeInfo));
        }
    }

    public boolean validateDeleteGroup() {
        NodeInfo nodeInfo = (NodeInfo) selectedNode.getData();
        this.count = 0;
        for (NodeInfo item : listNodeInfo) {
            if (Objects.equals(item.getParentId(), nodeInfo.getId())) {
                this.count++;
            }
        }
        if (count >= 1) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.delete.group"));
            return false;
        }
        return true;
    }

    // GROUP
    public void prapreEditGroup() {
        this.groupEditMode = true;
        NodeInfo nodeInfo = (NodeInfo) selectedNode.getData();
        this.nodeDefineDTO = new NodeDefineDTO();
        nodeDefineDTO.setOperatorDT(nodeInfo.getDisplay().split(" ")[2]);
    }
//create group

    public void prepareCreateGroup() {
        this.groupEditMode = false;
        this.groupParentEditMode = false;

        this.nodeDefineDTO = new NodeDefineDTO();
    }
//crate group cho nut goc

    public void prepareCreateGroupForParent() {
        this.focusCombobox = false;
        this.focusValuEx = false;
        this.groupEditMode = false;
        this.groupParentEditMode = true;
        this.nodeDefineDTO = new NodeDefineDTO();
    }

    // save edit group
    public void saveOrEditGroup() {
        if (this.groupEditMode == false) {
            if (this.groupParentEditMode == false) {
                NodeInfo nodeInfoSelect = (NodeInfo) selectedNode.getData();
                this.nodeInfo = new NodeInfo();
                Long maxId = 1L;
                for (NodeInfo item : listNodeInfo) {
                    if (item.getId() > maxId) {
                        maxId = item.getId();
                    }
                }
                this.nodeInfo.setId(maxId + 1);
                this.nodeInfo.setParentId(nodeInfoSelect.getId());
                this.nodeInfo.setDisplay("Group" + " " + "-" + " " + nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setValue(nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setOperator(nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setType("group");

            }
            if (this.groupParentEditMode == true) {
                this.nodeInfo = new NodeInfo();
                Long maxId = 1L;
                for (NodeInfo item : listNodeInfo) {
                    if (item.getId() > maxId) {
                        maxId = item.getId();
                    }
                }
                Long minId = 1L;
                for (NodeInfo item : listNodeInfo) {
                    if (item.getId() < minId) {
                        minId = item.getId();
                    }
                }
                this.nodeInfo.setId(maxId + 1);
                this.nodeInfo.setParentId(minId);
                this.nodeInfo.setDisplay("Group" + " " + "-" + " " + nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setValue(nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setOperator(nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setType("group");

            }
        }
        if (this.groupEditMode == true) {
            NodeInfo nodeInfoSelect = (NodeInfo) selectedNode.getData();
            for (NodeInfo item : listNodeInfo) {
                if (Objects.equals(item.getId(), nodeInfoSelect.getId())) {
                    item.setDisplay("Group" + " " + "-" + " " + nodeDefineDTO.getOperatorDT());
                    item.setOperator(nodeDefineDTO.getOperatorDT());

                }
            }
        }

//        rootNodeFuzzy = genTreeFuzzy(this.listNodeInfo);
        refreshTreeNodeInfoGroup();
        TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfo);
        genField(togenfield);
    }

    // GEN CAY
    public TreeNode genTreeFuzzy(List<NodeInfo> listNodeInfo) {
        // root node of framework
        TreeNode fuzzyRootNode = new DefaultTreeNode(null, null);

//         lấy ra nodeInfo gốc nhưng khong gan vao goc framework
        NodeInfo nodeInfoRoot = new NodeInfo();
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                nodeInfoRoot = item;
            }
        }

        for (NodeInfo item : listNodeInfo) {
            if (Objects.equals(item.getParentId(), nodeInfoRoot.getId())) {
                if ("group".equals(item.getType())) {
                    TreeNode node = new DefaultTreeNode("group", item, fuzzyRootNode);
                    recursiveTreeFuzzy(node, item, listNodeInfo);
                }
                if ("object".equals(item.getType())) {
                    TreeNode node = new DefaultTreeNode("object", item, fuzzyRootNode);
                }
            }
        }

        fuzzyRootNode.setExpanded(true);

        return fuzzyRootNode;
    }

    public TreeNode genTreeFuzzyToGenDisplayExpression(List<NodeInfo> listNodeInfo) {
        // root node of framework
        TreeNode fuzzyRootNode = new DefaultTreeNode(null, null);

//        //Tạo root node của dữ liệu
        NodeInfo firstNodeInfo = new NodeInfo();
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                firstNodeInfo = item;
            }
        }
        TreeNode rootNode = new DefaultTreeNode("group", firstNodeInfo, fuzzyRootNode);

//        // Đệ quy để tạo các node con tiếp theo
        recursiveTreeFuzzy(rootNode, firstNodeInfo, listNodeInfo);
        fuzzyRootNode.setExpanded(true);
        //addExpandedNode(fuzzyRootNode.getChildren().get(0));

        return fuzzyRootNode;
    }

    public void recursiveTreeFuzzy(TreeNode rootNode, NodeInfo firstNodeInfo, List<NodeInfo> listNodeInfo) {
        for (NodeInfo item : listNodeInfo) {

            if (Objects.equals(item.getParentId(), firstNodeInfo.getId())) {
                if ("group".equals(item.getType())) {
                    TreeNode node = new DefaultTreeNode("group", item, rootNode);
                    recursiveTreeFuzzy(node, item, listNodeInfo);
                }

                if ("object".equals(item.getType())) {
                    TreeNode node = new DefaultTreeNode("object", item, rootNode);
                }
            }
        }
    }
    private Long nodeInfoId = 0L;

    public List<NodeInfo> getListNodeInfoFromDisplayExpress(String displayExpression) {

        List<NodeInfo> listNodeInfo = new ArrayList<>();
        this.nodeInfoId = 1L;

        NodeInfo rootNodeInfo = new NodeInfo();

        rootNodeInfo.setId(this.nodeInfoId);
        rootNodeInfo.setValue(displayExpression);
        rootNodeInfo.setType("group");
        rootNodeInfo.setDisplay(null);
        listNodeInfo.add(rootNodeInfo);

        getListNode(rootNodeInfo, listNodeInfo);

        return listNodeInfo;

    }

    public void getListNode(NodeInfo nodeInfo, List<NodeInfo> listNodeInfo) {
        // lấy chuỗi displayExpression of group
        String valueStr = nodeInfo.getValue();

        // Ký tự ngoặc đầu tiên (tính cả ban đầu hoăc sau khi reset nếu đủ bộ ngoặc)
        String startChar = "";

        // vị trí ngoặc mở đầu tiên
        int startIndex = 0;

        // để lưu số lượng ngoặc mở
        int numberOfStartChar = 0;

        // vị trí ngoặc đóng cuối cùng
        int endIndex = 0;

        // để lưu số lượng ngoặc dong
        int numberOfEndChar = 0;

//        ArrayList<String> listString = new ArrayList();
        ArrayList<String> listStringToSplit = new ArrayList();
        List<NodeInfo> listTodequy = new ArrayList();

        for (int i = 0; i < valueStr.length(); i++) {
            if ("".equals(startChar)) {

                if ("[".equals(Character.toString(valueStr.charAt(i))) || "(".equals(Character.toString(valueStr.charAt(i)))) {
                    startChar = Character.toString(valueStr.charAt(i));
                    numberOfStartChar = 1;
                    startIndex = i;
                }
            } else {

                if ("(".equals(startChar)) {

                    if (")".equals(Character.toString(valueStr.charAt(i)))) {
                        numberOfEndChar = numberOfEndChar + 1;

                        if (numberOfEndChar == numberOfStartChar) {
                            endIndex = i;
//                            listString.add(valueStr.substring(startIndex + 1, endIndex));
                            listStringToSplit.add(valueStr.substring(startIndex, endIndex + 1));

                            NodeInfo n = new NodeInfo();
                            this.nodeInfoId = this.nodeInfoId + 1;

                            n.setId(this.nodeInfoId);
                            n.setParentId(nodeInfo.getId());
                            n.setValue(valueStr.substring(startIndex + 1, endIndex));
                            n.setType("group");
//                            n.setDisplay(valueStr.substring(startIndex + 1, endIndex));

                            listNodeInfo.add(n);
                            listTodequy.add(n);

                            startChar = "";
                            numberOfStartChar = 0;
                            startIndex = 0;
                            numberOfEndChar = 0;
                            endIndex = 0;
//                            getListNode(n, nodeInfoId, listNodeInfo);
                        }

                    }
                    if ("(".equals(Character.toString(valueStr.charAt(i)))) {
                        numberOfStartChar = numberOfStartChar + 1;
                    }
                }

                if ("[".equals(startChar)) {
                    if ("]".equals(Character.toString(valueStr.charAt(i)))) {
                        numberOfEndChar = numberOfEndChar + 1;
                        if (numberOfEndChar == numberOfStartChar) {
                            endIndex = i;
//                            listString.add(valueStr.substring(startIndex + 1, endIndex));
                            listStringToSplit.add(valueStr.substring(startIndex, endIndex + 1));

                            NodeInfo n = new NodeInfo();
                            this.nodeInfoId = this.nodeInfoId + 1;

                            n.setId(this.nodeInfoId);
                            n.setParentId(nodeInfo.getId());
                            n.setValue(valueStr.substring(startIndex + 1, endIndex));
                            n.setType("object");
                            n.setDisplay(valueStr.substring(startIndex + 1, endIndex));

                            listNodeInfo.add(n);

                            startChar = "";
                            numberOfStartChar = 0;
                            numberOfEndChar = 0;
                            startIndex = 0;
                            endIndex = 0;
                        }
                    }
                    if ("[".equals(Character.toString(valueStr.charAt(i)))) {
                        numberOfStartChar = numberOfStartChar + 1;
                    }
                }

            }
        }

        String operator = valueStr.replace(listStringToSplit.get(0), "").trim().split(" ")[0];
        nodeInfo.setOperator(operator);
        nodeInfo.setDisplay("Group - " + operator);
        for (NodeInfo itm : listTodequy) {
            getListNode(itm, listNodeInfo);
        }
    }

    //GEN tu cay len field
    public void genField(TreeNode tree) {
        String valueStr = "";
        if (genTreeFuzzyToGenDisplayExpression(listNodeInfo).getChildren().size() > 0) {
            TreeNode nodeTogen = genTreeFuzzyToGenDisplayExpression(listNodeInfo).getChildren().get(0);
            valueStr = genToStringTree(nodeTogen);
            valueStr = valueStr.substring(1, valueStr.length() - 1);
        }

        // set valueString vào trường field
        fuzzyRuleEdit.setDisplayExpression(valueStr);
    }

    public String genToStringTree(TreeNode treeNode) {
        String valueString = "(";

        NodeInfo nodeInfo = (NodeInfo) treeNode.getData();

        List<TreeNode> listChildren = treeNode.getChildren();
        if (listChildren.size() > 0) {
            NodeInfo fistChildNodeInfo = (NodeInfo) listChildren.get(0).getData();
            if ("object".equals(fistChildNodeInfo.getType())) {
                valueString = valueString + "[" + fistChildNodeInfo.getDisplay() + "]";
            }
            if ("group".equals(fistChildNodeInfo.getType())) {
                valueString = valueString + genToStringTree(listChildren.get(0));
            }

            for (int i = 1; i < listChildren.size(); i++) {
                NodeInfo nodeInfos = (NodeInfo) listChildren.get(i).getData();
                if ("object".equals(nodeInfos.getType())) {
                    valueString = valueString + " " + nodeInfo.getOperator() + " " + "[" + nodeInfos.getDisplay() + "]";
                }
                if ("group".equals(nodeInfos.getType())) {
                    valueString = valueString + " " + nodeInfo.getOperator() + " " + genToStringTree(listChildren.get(i));
                }
            }
        }
        valueString = valueString + ")";
        return valueString;
    }

    //DELETE ALL TREE
    public void clearTreeFuzzy() {
        for (int i = 1; i < listNodeInfo.size(); i++) {
            if (listNodeInfo.get(i).getParentId() != null) {
                listNodeInfoToDelete.add(listNodeInfo.get(i));
            }
        }
        for (NodeInfo item : listNodeInfoToDelete) {
            if (item != null) {
                listNodeInfo.remove(item);
            }
        }
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                this.nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT(item.getOperator());
                this.dem = 0;
                for (NodeInfo items : listNodeInfo) {
                    if (Objects.equals(items.getParentId(), item.getId())) {
                        dem++;
                    }
                }
                if (dem >= 2) {
                    this.renderedCbb = true;
                } else {
                    this.renderedCbb = false;
                }
            }
        }

        rootNodeFuzzy = genTreeFuzzy(listNodeInfo);
        TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfo);
        genField(togenfield);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public void doSaveAll() {
        // update create assessment rule
//        for (AssessmentRule item : listAssessmentRule) {
//            if (item != null) {
//                assessmentRuleService.createOrUpdateAssessmenRule(item);
//            }
//        }
        if (this.editMode) {
            assessmentRuleService.createOrUpdateAssessmenRule(this.assessmentRule);
            for (AssessmentRule item : listAssessmentRule) {
                if (Objects.equals(item.getRuleId(), this.assessmentRule.getRuleId())) {
                    cloneAssessmentRule(item, this.assessmentRule);
                }
            }
            //create update ppu evaluatino
            for (AssessmentRulePreProcessUnitMap item : listAssessmentRulePreProcessUnitMap) {
                if (item != null) {
                    assessmentRuleService.createOrUpdateAssessmentRuleProcessUnitMap(item);
                }
            }

            // delete ppu evaluatino
            for (PpuEvaluation item : listPpuEvaluationToDelete) {
                for (AssessmentRulePreProcessUnitMap items : listAssessmentRulePreProcessUnitMap) {
                    if (Objects.equals(item.getPreProcessId(), items.getPreProcessUnitId()) && Objects.equals(items.getRuleId(), this.assessmentRule.getRuleId())) {
                        listAssessmentRulePreProcessUnitMap.remove(items);
                        assessmentRuleService.deleteAssessmentRulePreProcessUnitMap(items.getId());
                    }
                }
            }

            this.listPpuEvaluationToDelete = new ArrayList<>();
//        assessmentRuleService.deleteAssessmentRulePreProcessUnitMap(12L);

            // create update kpi processor
            for (AssessmentRuleKpiProcessorMap item : listAssessmentRuleKpiProcessorMap) {
                if (item != null) {
                    assessmentRuleService.createOrUpdateAssessmentRuleKpiProcessorMap(item);
                }
            }
            // delete kpi processor
            for (KpiProcessor item : listKpiProcessorToDelete) {
                for (AssessmentRuleKpiProcessorMap items : listAssessmentRuleKpiProcessorMap) {
                    if (Objects.equals(item.getProcessorId(), items.getProcessorId()) && Objects.equals(items.getRuleId(), this.assessmentRule.getRuleId())) {
                        listAssessmentRuleKpiProcessorMap.remove(items);
                        assessmentRuleService.deleteAssessmentRuleKpiProcessorMap(items.getId());
                    }
                }

            }
            this.listKpiProcessorToDelete = new ArrayList<>();

            //create update result Class 
            for (ResultClass item : listResultClass) {
                if (item != null) {
                    assessmentRuleService.doCreateOrUpdateResultClass(item);
                }
            }

            // delete result class
            for (ResultClass item : listResultClassToDelete) {
                if (item != null) {
                    assessmentRuleService.deleteResultClass(item.getClassId());
                }
            }

            // create update fuzzyRule
            for (FuzzyRule item : listFuzzyRule) {
                if (item != null) {
                    assessmentRuleService.doCreateOrUpdateFuzzyRule(item);
                }
            }
            //delete fuuzy
            for (FuzzyRule item : listFuzzyRuleToDelete) {
                if (item != null) {
                    assessmentRuleService.deleteFuzzyRule(item.getFuzzyRuleId());
                }
            }
            this.selectedNode = this.currenRootNode;
            //this.selectedNode = this.currenRootNode;
//            TreeNode parentNote = selectedNode.getParent();
//            parentNote.getChildren().remove(selectedNode);
//            if (parentNote.getChildren().isEmpty()) {
//                removeExpandedNode(parentNote);
//            }
//            rootNode.getChildren().get(0).setExpanded(true);
//            TreeNode treeNode = addNoteToCategoryTree(rootNode, this.assessmentRule);
//            this.currenRootNode = treeNode;
//            if (treeNode != null) {
//                treeNode.setSelected(true);
//            }

            initTreeNode();
            Category cat = categoryService.getCategoryById(this.assessmentRule.getCategoryId());
            TreeNode note = findCategoryInTree(rootNode, cat);
            if (note != null) {
                note.setExpanded(true);
                expandCurrentNode(note);
                mapTreeStatus(rootNode);
            }
            this.action = false;
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
        if (!this.editMode) {
            assessmentRuleService.createOrUpdateAssessmenRule(this.assessmentRule);
            this.listAssessmentRule.add(this.assessmentRule);

            //create update ppu evaluatino
            for (AssessmentRulePreProcessUnitMap item : listAssessmentRulePreProcessUnitMap) {
                if (item != null) {
                    assessmentRuleService.createOrUpdateAssessmentRuleProcessUnitMap(item);
                }
            }

            // delete ppu evaluatino
            for (PpuEvaluation item : listPpuEvaluationToDelete) {
                for (AssessmentRulePreProcessUnitMap items : listAssessmentRulePreProcessUnitMap) {
                    if (Objects.equals(item.getPreProcessId(), items.getPreProcessUnitId()) && Objects.equals(items.getRuleId(), this.assessmentRule.getRuleId())) {
                        listAssessmentRulePreProcessUnitMap.remove(items);
                        assessmentRuleService.deleteAssessmentRulePreProcessUnitMap(items.getId());
                    }
                }
            }

            this.listPpuEvaluationToDelete = new ArrayList<>();
//        assessmentRuleService.deleteAssessmentRulePreProcessUnitMap(12L);

            // create update kpi processor
            for (AssessmentRuleKpiProcessorMap item : listAssessmentRuleKpiProcessorMap) {
                if (item != null) {
                    assessmentRuleService.createOrUpdateAssessmentRuleKpiProcessorMap(item);
                }
            }
            // delete kpi processor
            for (KpiProcessor item : listKpiProcessorToDelete) {
                for (AssessmentRuleKpiProcessorMap items : listAssessmentRuleKpiProcessorMap) {
                    if (Objects.equals(item.getProcessorId(), items.getProcessorId()) && Objects.equals(items.getRuleId(), this.assessmentRule.getRuleId())) {
                        listAssessmentRuleKpiProcessorMap.remove(items);
                        assessmentRuleService.deleteAssessmentRuleKpiProcessorMap(items.getId());
                    }
                }

            }
            this.listKpiProcessorToDelete = new ArrayList<>();

            //create update result Class 
            for (ResultClass item : listResultClass) {
                if (item != null) {
                    assessmentRuleService.doCreateOrUpdateResultClass(item);
                }
            }

            // delete result class
            for (ResultClass item : listResultClassToDelete) {
                if (item != null) {
                    assessmentRuleService.deleteResultClass(item.getClassId());
                }
            }

            // create update fuzzyRule
            for (FuzzyRule item : listFuzzyRule) {
                if (item != null) {
                    assessmentRuleService.doCreateOrUpdateFuzzyRule(item);
                }
            }
            //delete fuuzy
            for (FuzzyRule item : listFuzzyRuleToDelete) {
                if (item != null) {
                    assessmentRuleService.deleteFuzzyRule(item.getFuzzyRuleId());
                }
            }

            rootNode.getChildren().get(0).setExpanded(true);
            TreeNode treeNode = addNoteToCategoryTree(rootNode, this.assessmentRule);
            this.currenRootNode = treeNode;
            if (treeNode != null) {
                treeNode.setSelected(true);
            }

            this.action = false;
            this.editMode = true;
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }

    }

    public void deleteAssessmentRule() {
        this.assessmentRule = (AssessmentRule) selectedNode.getData();
        this.listRuleEvaluationInfo = assessmentRuleService.findRuleEvaluationInfoById(this.assessmentRule.getRuleId());
        if (this.listRuleEvaluationInfo.isEmpty()) {

            // delete fuzzy rule
            this.listFuzzyRule = assessmentRuleService.getListFuzzyRuleByRuleId(assessmentRule.getRuleId());
            this.listFuzzyRuleToDelete = this.listFuzzyRule;

            for (FuzzyRule item : listFuzzyRuleToDelete) {
                if (item != null) {
                    assessmentRuleService.deleteFuzzyRule(item.getFuzzyRuleId());
                }
            }
            // delete result class
            this.listResultClass = assessmentRuleService.getListResultClassByAssessmentRuleId(assessmentRule.getRuleId());
            this.listResultClassToDelete = this.listResultClass;

            for (ResultClass item : listResultClassToDelete) {
                if (item != null) {
                    assessmentRuleService.deleteResultClass(item.getClassId());
                }
            }

            // delete kpi processor
            this.listAssessmentRuleKpiProcessorMap = assessmentRuleService.getAlllistAssessmentRuleKpiProcessorMapById(this.assessmentRule.getRuleId());
            for (AssessmentRuleKpiProcessorMap item : listAssessmentRuleKpiProcessorMap) {
                if (Objects.equals(item.getRuleId(), this.assessmentRule.getRuleId())) {
                    assessmentRuleService.deleteAssessmentRuleKpiProcessorMap(item.getId());
                }
            }

            // delete ppu evaluatino
            this.listAssessmentRulePreProcessUnitMap = assessmentRuleService.getAllAssessmentRulePreProcessUnitMapById(this.assessmentRule.getRuleId());
            for (AssessmentRulePreProcessUnitMap item : listAssessmentRulePreProcessUnitMap) {
                if (Objects.equals(item.getRuleId(), this.assessmentRule.getRuleId())) {
                    assessmentRuleService.deleteAssessmentRulePreProcessUnitMap(item.getId());
                }
            }

            // delet assessmentRule
            this.listAssessmentRule.remove(this.assessmentRule);
            this.listAssessmentRuleToDelete.add(this.assessmentRule);
            assessmentRuleService.deleteAssessmentRule(this.assessmentRule.getRuleId());
            //initTreeNode();
            TreeNode parentNode = selectedNode.getParent();
            parentNode.getChildren().remove(selectedNode);
            if (parentNode.getChildren().isEmpty()) {
                removeExpandedNode(parentNode);
            }
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.remove.assessment"));
        }

        this.disPlays = false;
    }

    //          CATEGORY
    public void preapreCreateCategory() {
        this.categoryEditMode = false;
        Category parentCat = (Category) selectedNode.getData();
        this.category = new Category();
        this.categoryBackUp = new Category();
        category.setCategoryType(parentCat.getCategoryType());
        Long id = assessmentRuleService.getNextSequense(Constants.TableName.CATEGORY);
        category.setCategoryId(id);
        this.category.setParentId(parentCat.getCategoryId());
        cloneCategory(categoryBackUp, category);
    }

    public void cloneCategory(Category a, Category b) {
        a.setCategoryId(b.getCategoryId());
        a.setCategoryType(b.getCategoryType());
        a.setName(b.getName());
        a.setParentId(b.getParentId());
    }

    public void preapreUpdateCategory() {
        this.categoryEditMode = true;
        this.categoryBackUp = new Category();
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();
            cloneCategory(categoryBackUp, category);
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void saveCategory() {
        cloneCategory(category, categoryBackUp);
        categoryService.save(category);
        refreshTree();
        //this.disPlays = false;
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    private void refreshTreeNodeInfo() {
        if (expressionEditMode) {

        } else {
            listNodeInfo.add(this.nodeInfo);
        }
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                this.nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT(item.getOperator());
                this.dem = 0;
                for (NodeInfo items : listNodeInfo) {
                    if (Objects.equals(items.getParentId(), item.getId())) {
                        dem++;
                    }
                }
                if (dem >= 2) {
                    this.renderedCbb = true;
                } else {
                    this.renderedCbb = false;
                }
            }
        }
        rootNodeFuzzy = genTreeFuzzy(listNodeInfo);
        processRefreshNodeInfo(rootNodeFuzzy, selectedNode, nodeInfo, expressionEditMode);
    }

    private void refreshTreeNodeInfoGroup() {
        if (groupEditMode) {

        } else {
            listNodeInfo.add(this.nodeInfo);
        }
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                this.nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT(item.getOperator());
                this.dem = 0;
                for (NodeInfo items : listNodeInfo) {
                    if (Objects.equals(items.getParentId(), item.getId())) {
                        dem++;
                    }
                }
                if (dem >= 2) {
                    this.renderedCbb = true;
                } else {
                    this.renderedCbb = false;
                }
            }
        }

        rootNodeFuzzy = genTreeFuzzy(listNodeInfo);
        processRefreshNodeInfo(rootNodeFuzzy, selectedNode, nodeInfo, groupEditMode);
    }

    private void refreshTree() {
        if (categoryEditMode) {
            categories.remove((Category) selectedNode.getData());
            categories.add(this.category);
        } else {
            categories.add(this.category);
        }

        rootNode = treeUtilsService.createTreeCategoryAndComponentCampaignEvaluation(categories, this.listAssessmentRule);
        processRefreshCategory(rootNode, selectedNode, category, categoryEditMode);
        //updateCategoryInformCampaign();
    }

    public void deleteCategory() {
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();
            if (inValidCategoryNode()) {
                return;
            }
            if (categoryService.checkDeleteCategory(this.category.getCategoryId())) {
                categoryService.deleteCategory(category);
                selectedNode.getChildren().clear();
                TreeNode parNode = selectedNode.getParent();
                selectedNode.getParent().getChildren().remove(selectedNode);
                if (parNode.getChildren().isEmpty()) {
                    removeExpandedNode(parNode);
                }
                selectedNode.setParent(null);
                selectedNode = null;
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
                categories.remove(category);
                parNode.setExpanded(true);
                updateCategoryInformCampaign();
            } else {
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public boolean inValidCategoryNode() {
        if (DataUtil.isNullObject(category.getParentId())) {
            errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.parent.category"));
            return true;
        }
        if (!selectedNode.getChildren().isEmpty()) {
            errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            return true;
        }
        return false;
    }

    public boolean validateCategory() {
        if (DataUtil.isStringNullOrEmpty(this.categoryBackUp.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.categoryBackUp.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Category name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.categoryBackUp.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Category name");
            return false;
        }
        if (Objects.equals(this.categoryBackUp.getCategoryId(), this.categoryBackUp.getParentId())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.category"));
            return false;
        }

        boolean rs = true;
        if (!categoryService.checkDuplicate(categoryBackUp.getName(), categoryBackUp.getCategoryId(), categoryBackUp.getCategoryType())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Category name");
            rs = false;
        }
        return rs;
    }

    public void expandCurrentNodeAssessmentRule(TreeNode selectedNode) {
        TreeNode parent = selectedNode.getParent();
        if (parent == null || parent.getData() == null) {
            return;
        } else {
            addExpandedNodeAssessmentRule(parent);
            expandCurrentNodeAssessmentRule(parent);
        }
    }

    public void addExpandedNodeAssessmentRule(TreeNode node) {
        Category category = (Category) node.getData();
        if (category == null) {
            return;
        }
        node.setExpanded(true);
        treeNodeExpanded.put(category.getCategoryId(), node);
    }

    public TreeNode addNoteToCategoryTree(TreeNode rootNode, BaseCategory baseCategory) {
        TreeNode note = findParentNoteInTree(rootNode, baseCategory);
        if (note != null) {
            TreeNode newNote = new DefaultTreeNode(baseCategory.getTreeType(), baseCategory, note);
            expandCurrentNode(newNote);
            this.selectedNode = newNote;
            this.currenRootNode = this.selectedNode;
            return newNote;
        }
        return null;
    }

    public TreeNode findParentNoteInTree(TreeNode root, BaseCategory baseCategory) {
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (baseCategory.getParentType().isInstance(note.getData())) {
                Category data = (Category) note.getData();
                if (data.getCategoryId() == baseCategory.getParentId()) {
                    return note;
                }
            }
            currentNote = findParentNoteInTree(note, baseCategory);
            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }
}
