package vn.viettel.campaign.controller;

import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.common.DateUtils;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dto.FilterTable;
import vn.viettel.campaign.dto.PathTable;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.PreprocessUnitInterface;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.*;

import static vn.viettel.campaign.common.DateUtils.DATETIME_PPU_SRING;
import static vn.viettel.campaign.common.DateUtils.DATETIME_PPU_ZONE;

import vn.viettel.campaign.validate.CheckOnEdit;

/**
 * @author truongbx
 * @date 9/15/2019
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
public class PreProcessCompareNumberController extends BaseCategoryController<PreProcessUnit> implements Serializable {

    @Autowired
    PreprocessUnitInterface preprocessUnitServiceImpl;
    private TreeNode pathRootNode;
    private TreeNode selectedPathNode = new DefaultTreeNode();
    private List<InputObject> lstInputObject = new ArrayList<>();
    private Map<Long, InputObject> mapInputObject = new HashMap<>();
    private Map<String, InputObject> mapInputNameObject = new HashMap<>();
    private Map<Long, Function> mapFunction = new HashMap<>();
    private Map<Long, Function> mapFunctionTypeOne = new HashMap<>();
    private List<PathTable> lstPathTable = new ArrayList<>();
    private List<PathTable> lstPathTableSecond = new ArrayList<>();
    private List<InputObject> lstConditionData = new ArrayList<>();
    private List<Function> lstFunctionData = new ArrayList<>();
    private List<Function> lstFunctionDataTypeOne = new ArrayList<>();
    private List<FilterTable> lstConditionTable = new ArrayList<>();
    private List<FilterTable> lstFunctionTable = new ArrayList<>();
    private List<FilterTable> lstFunctionPathTable = new ArrayList<>();
    private List<FilterTable> lstFunctionPathTableSecond = new ArrayList<>();
    private List<Zone> lstZone = new ArrayList<>();
    private List<ZoneMap> lstZoneMap = new ArrayList<>();
    private List<ProcessValue> lstProcessValue = new ArrayList<>();
    private List<ProcessValue> lstProcessValueTemp = new ArrayList<>();
    private List<ProcessParam> lstProcessParams = new ArrayList<>();
    private boolean disableConditiontable = true;
    private PathTable currentPathTable = new PathTable();
    private PathTable realPathTable;
    private Long indexField = 1L;

    @Override
    public void init() {
        this.categoryType = Constants.CatagoryType.CATEGORY_PRE_PROCESS_UNIT_COMPARE_NUMBER_TYPE;
        this.currentClass = PreProcessUnit.class;
        lstInputObject = preprocessUnitServiceImpl.getLstInputObject();
        for (InputObject inputObject : lstInputObject) {
            mapInputObject.put(inputObject.getObjectId(), inputObject);
            mapInputNameObject.put(inputObject.getObjectName() + "_" + inputObject.getObjectParentId(), inputObject);
        }
        initPathTree();
        lstFunctionData = preprocessUnitServiceImpl.getLstFunction();
        lstFunctionDataTypeOne = preprocessUnitServiceImpl.getLstFunctionTypeOne();
        lstFunctionData.forEach((e) -> mapFunction.put(e.getFunctionId(), e));
        lstFunctionDataTypeOne.forEach((e) -> mapFunction.put(e.getFunctionId(), e));
        lstZone = preprocessUnitServiceImpl.getLstZone();
        lstZoneMap = preprocessUnitServiceImpl.getLstZoneMap();
        //

    }

    @Override
    public void initCurrentValue() {
        currentValue = preprocessUnitServiceImpl.getNextSequense();
        currentValue.setPreProcessType(Constants.PRE_PROCESS_UNIT_COMPARE_NUMBER);
        //HaBM2: Filter for evaluation
        currentValue.setType(Constants.PPU_TYPE_RULE);
    }

    @Override
    public boolean onValidateObject() {
        if (!validInputField(this.currentValue.getPreProcessName(), "PreProcess unit name", true, true, true)) {
            return false;
        }
        if (preprocessUnitServiceImpl.checkExistPreProcessName(this.currentValue.getPreProcessName(), this.currentValue.getPreProcessId())) {
            duplidateMessage("PreProcess unit name");
            return false;

        }
        if (!validInputField(this.currentValue.getDescription(), "Description", false, true, true)) {
            return false;
        }
        if (this.currentValue.getDefaultValue() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Default value");
            return false;
        }
        if (!validRequireField(this.currentValue.getInputMode(), "Input mode")) {
            return false;
        }
        if (!validRequireField(this.currentValue.getInputModeSecond(), "Input mode")) {
            return false;
        }

        if (this.currentValue.getInputMode() == 1) {
            if (!validRequireFieldList(lstPathTable, "Path table")) {
                return false;
            }
            for (PathTable pathTable : lstPathTable) {
                if (pathTable.getObjectId() == -1) {
                    notCompleteMessage("Path table");
                    return false;
                }
            }

        } else if (this.currentValue.getInputMode() == 2) {
            if (!validRequireFieldList(lstFunctionPathTable, "Path table")) {
                return false;
            }
            boolean validFunction = buildDataFunction(lstFunctionPathTable, 1, 1L);
            if (!validFunction) {
                notCompleteMessage("Path table");
                return false;
            }
        }
        if (this.currentValue.getInputModeSecond() == 1) {
            if (!validRequireFieldList(lstPathTableSecond, "Path table")) {
                return false;
            }
            for (PathTable pathTable : lstPathTableSecond) {
                if (pathTable.getObjectId() == -1) {
                    notCompleteMessage("Path table");
                    return false;
                }
            }

        } else if (this.currentValue.getInputModeSecond() == 2) {
            if (!validRequireFieldList(lstFunctionPathTableSecond, "Path table")) {
                return false;
            }
            boolean validFunction = buildDataFunction(lstFunctionPathTableSecond, 1, 2L);
            if (!validFunction) {
                notCompleteMessage("Path table");
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean doDelete() {
        return preprocessUnitServiceImpl.onDeletePreprocessUnit(this.currentValue.getPreProcessId());
    }

    @Override
    public boolean onSaveObject() {
//		this.currentValue.setDefaultValue(1L);
        this.currentValue.setSpecialFields("compareType:" + currentValue.getSpecialFieldsTmp());
        this.currentValue.setInputFields(this.currentValue.getInputFieldsOne() + "%" + this.currentValue.getInputFieldsSecond());
        boolean result = preprocessUnitServiceImpl.onSavePreprocessUnit(this.currentValue, lstProcessParams, lstProcessValue);
        return result;
    }

    @Override
    public void doEdit() {
        lstProcessValue = preprocessUnitServiceImpl.getLstProcessValue(currentValue.getPreProcessId());
        lstProcessParams = preprocessUnitServiceImpl.getLstProcessParam(currentValue.getPreProcessId());
        lstPathTable = new ArrayList<>();
        lstFunctionPathTable = new ArrayList<>();
        lstPathTableSecond = new ArrayList<>();
        lstFunctionPathTableSecond = new ArrayList<>();
        Map<Long, ProcessValue> mapColor = new HashMap<>();
        for (ProcessValue processValue : lstProcessValue) {
            mapColor.put(processValue.getValueId(), processValue);
        }
        for (ProcessParam processParam : lstProcessParams) {
            ProcessValue processValue = mapColor.get(processParam.getParamIndex());
            if (processValue != null) {
                processParam.setValueColor(processValue.getValueColor());
            }
        }
        if (this.currentValue.getInputMode() == 1) {
            initDataForPathTable();
        } else {
            lstFunctionPathTable = convertFunctionFilterToList(this.currentValue.getOtherString(),1);
            for (int i = 0; i < lstFunctionPathTable.size(); i++) {
                if (i == 0) {
                    lstFunctionPathTable.get(0).setLstFunction(lstFunctionDataTypeOne);
                } else {
                    lstFunctionPathTable.get(i).setLstFunction(lstFunctionData);
                }
            }
        }
        if (this.currentValue.getInputModeSecond() == 1) {
            initDataForPathTableSecond();
        } else {
            lstFunctionPathTableSecond = convertFunctionFilterToList(this.currentValue.getOtherStringSecond(),1);
            for (int i = 0; i < lstFunctionPathTableSecond.size(); i++) {
                if (i == 0) {
                    lstFunctionPathTableSecond.get(0).setLstFunction(lstFunctionDataTypeOne);
                } else {
                    lstFunctionPathTableSecond.get(i).setLstFunction(lstFunctionData);
                }
            }
        }
    }

    public void initDataForPathTable() {
        if (!DataUtil.isStringNullOrEmpty(this.currentValue.getOtherString())) {
            String dataTable = this.currentValue.getOtherString();
            String[] table = dataTable.split("\\.");
            for (int i = 0; i < table.length; i++) {
                int toIndex = table[i].indexOf("{") == -1 ? table[i].length() : table[i].indexOf("{");
                String name = table[i].substring(0, toIndex);
                PathTable pathTable = new PathTable();
                PathTable parent = new PathTable();
                if (i == 0) {
                    TreeNode parentNode = pathRootNode.getChildren().get(0);
                    InputObject input = (InputObject) parentNode.getData();
                    parent.setObjectId(input.getObjectId());
                    parent.setNode(parentNode);
                } else {
                    parent = lstPathTable.get(lstPathTable.size() - 1);
                }
                pathTable.setObjectName(name);
                InputObject inputObject = mapInputNameObject.get(name + "_" + parent.getObjectId());
                if (inputObject != null) {
                    pathTable.setObjectId(inputObject.getObjectId());
                }
                findNoteByParent(parent.getNode(), pathTable);
                List<InputObject> lstInputObject = getChildDataOfNode(pathTable.getNode().getParent());
                pathTable.setLstInput(lstInputObject);
                lstPathTable.add(pathTable);
            }
            buildFilterData(lstPathTable);
        }
    }

    public void initDataForPathTableSecond() {
        if (!DataUtil.isStringNullOrEmpty(this.currentValue.getOtherStringSecond())) {
            String dataTable = this.currentValue.getOtherStringSecond();
            String[] table = dataTable.split("\\.");
            for (int i = 0; i < table.length; i++) {
                int toIndex = table[i].indexOf("{") == -1 ? table[i].length() : table[i].indexOf("{");
                String name = table[i].substring(0, toIndex);
                PathTable pathTable = new PathTable();
                PathTable parent = new PathTable();
                if (i == 0) {
                    TreeNode parentNode = pathRootNode.getChildren().get(0);
                    InputObject input = (InputObject) parentNode.getData();
                    parent.setObjectId(input.getObjectId());
                    parent.setNode(parentNode);
                } else {
                    parent = lstPathTableSecond.get(lstPathTableSecond.size() - 1);
                }
                pathTable.setObjectName(name);
                InputObject inputObject = mapInputNameObject.get(name + "_" + parent.getObjectId());
                if (inputObject != null) {
                    pathTable.setObjectId(inputObject.getObjectId());
                }
                findNoteByParent(parent.getNode(), pathTable);
                List<InputObject> lstInputObject = getChildDataOfNode(pathTable.getNode().getParent());
                pathTable.setLstInput(lstInputObject);
                lstPathTableSecond.add(pathTable);
            }
            buildFilterDataSecond(lstPathTableSecond);
        }
    }

    @Override
    public boolean onUpdateObject() {
        this.currentValue.setSpecialFields("compareType:" + currentValue.getSpecialFieldsTmp());
        this.currentValue.setInputFields(this.currentValue.getInputFieldsOne() + "%" + this.currentValue.getInputFieldsSecond());
        boolean result = preprocessUnitServiceImpl.onUpdatePreprocessUnit(this.currentValue, lstProcessParams, lstProcessValue);
        return result;
    }

    @Override
    public void rollbackData() {
        PreProcessUnit data = preprocessUnitServiceImpl.findById(this.currentValue.getPreProcessId());
        this.currentValue.asMapMultiFields(data);
    }

    @Override
    public void doAdd() {
        lstPathTable = new ArrayList<>();
        lstPathTableSecond = new ArrayList<>();
        lstProcessParams = new ArrayList<>();
        lstProcessValue = new ArrayList<>();
        ProcessValue pv = new ProcessValue();
        pv.setValueIndex(0);
        pv.setValueId(0);
        pv.setValueColor("fffcfd");
        pv.setValueName("false");
        lstProcessValue.add(pv);
        ProcessValue pv2 = new ProcessValue();
        pv2.setValueIndex(1);
        pv2.setValueId(1);
        pv2.setValueColor("fffcfd");
        pv2.setValueName("true");
        lstProcessValue.add(pv2);
        // nghiep vu: mac dinh defult value la false
        this.currentValue.setDefaultValue(0L);
    }

    //=============== Handle action ================
    public boolean validateChoosePath() {
        if (selectedPathNode == null) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.object.choose"));
            return false;
        }

        List<PathTable> data = new ArrayList<>();
        getDataToPathTable(data, selectedPathNode);
        if (this.indexField == 1) {
            lstPathTable = new ArrayList<>();
            lstPathTable.addAll(data);
        } else if (this.indexField == 2) {
            lstPathTableSecond = new ArrayList<>();
            lstPathTableSecond.addAll(data);
        }
        buildPathFromPathTable();
        return true;
    }

    public boolean validateApplyFilter() {

        boolean valid = buildDataCondition();
        if (!disableConditiontable && !valid) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notComplete"), "Condition");
            return false;
        }
        boolean checkCondition = validateInputCondition(lstConditionTable,mapInputObject);
        if (!disableConditiontable && !checkCondition){
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("valid.parameter"));
            return false;
        }
        valid = checkDuplicate(lstConditionTable);
        if (!disableConditiontable && !valid) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.table"), "field name", "condition");
            return false;
        }
        boolean validFunction = buildDataFunction(lstFunctionTable, 2);
        if (!validFunction) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notComplete"), "Function");
            return false;
        }
        boolean checkFunction = validateInputFunction(lstFunctionTable);
        if (!checkFunction){
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("valid.parameter"));
            return false;
        }
        validFunction = checkDuplicate(lstFunctionTable);
        if (!validFunction) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.table"), "function name", "Function");
            return false;
        }
        realPathTable.cloneData(currentPathTable);
        buildPathFromPathTable();
        return true;
    }

    public void getDataToPathTable(List<PathTable> data, TreeNode selectedPathNode) {
        InputObject inputObject = (InputObject) selectedPathNode.getData();
        if (inputObject.getObjectParentId() == null) {
            return;
        }
        PathTable pathTable = new PathTable();
        pathTable.setObjectId(inputObject.getObjectId());
        pathTable.setObjectName(inputObject.getObjectName());
        pathTable.setNode(selectedPathNode);
        List<InputObject> lstInputObject = getChildDataOfNode(selectedPathNode.getParent());
        pathTable.setLstInput(lstInputObject);
        data.add(0, pathTable);
        getDataToPathTable(data, selectedPathNode.getParent());

    }

    public List<InputObject> getChildDataOfNode(TreeNode node) {
        List<TreeNode> lstTreeNote = node.getChildren();
        List<InputObject> lstInputObject = new ArrayList<>();
        for (TreeNode treeNode : lstTreeNote) {
            lstInputObject.add((InputObject) treeNode.getData());
        }
        return lstInputObject;
    }

    public void buildPathFromPathTable() {
        StringBuilder stringBuilder = new StringBuilder();
        List<PathTable> lstPathTableTmp = null;
        if (this.indexField == 1) {
            lstPathTableTmp = lstPathTable;
        } else if (this.indexField == 2) {
            lstPathTableTmp = lstPathTableSecond;
        }
        if (lstPathTableTmp != null) {

            for (PathTable pathTable : lstPathTableTmp) {
                if (pathTable.getObjectId() > 0) {
                    if (DataUtil.isStringNullOrEmpty(pathTable.getDataCondition()) && DataUtil.isStringNullOrEmpty(pathTable.getDataFunction())) {
                        stringBuilder.append("." + pathTable.getObjectName() + "{}");
                    } else {
                        stringBuilder.append("." + pathTable.getObjectName()
                                + "{" + pathTable.getDataCondition() + ";" + pathTable.getDataFunction() + "}");
                    }
                }
            }
        }
        if (this.indexField == 1) {
            this.currentValue.setOtherString(stringBuilder.toString().replaceFirst(".", ""));
        } else if (this.indexField == 2) {
            this.currentValue.setOtherStringSecond(stringBuilder.toString().replaceFirst(".", ""));
        }
        buildFilterData(lstPathTable);
        buildFilterDataSecond(lstPathTableSecond);

    }

    public boolean addChildPath(Long indexField) {
        this.indexField = indexField;
        if (this.indexField == 1) {
            if (lstPathTable.isEmpty()) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.dont.parent"));
                return false;
            }
            PathTable pathTable = lstPathTable.get(lstPathTable.size() - 1);
            if (pathTable.getObjectId() == -1) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.dont.child"));
                return false;
            }
            List<InputObject> lstValue = getChildDataOfNode(pathTable.getNode());
            if (lstValue == null || lstValue.isEmpty()) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.dont.child"));
                return false;
            }
            PathTable newPath = new PathTable();
            newPath.setLstInput(lstValue);
            lstPathTable.add(newPath);
        }
        if (this.indexField == 2) {
            if (lstPathTableSecond.isEmpty()) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.dont.parent"));
                return false;
            }
            PathTable pathTable = lstPathTableSecond.get(lstPathTableSecond.size() - 1);
            if (pathTable.getObjectId() == -1) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.dont.child"));
                return false;
            }
            List<InputObject> lstValue = getChildDataOfNode(pathTable.getNode());
            if (lstValue == null || lstValue.isEmpty()) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.dont.child"));
                return false;
            }
            PathTable newPath = new PathTable();
            newPath.setLstInput(lstValue);
            lstPathTableSecond.add(newPath);
        }
        return true;
    }

    public void addNomalizerTable() {
        ProcessParam param = new ProcessParam();
        lstProcessParams.add(0,param);
    }

    public void onRemovePath(PathTable pathTable, Long indexField) {
        this.indexField = indexField;
        int index = 0;
        if (indexField == 1) {
            index = lstPathTable.indexOf(pathTable);
        }
        if (indexField == 2) {
            index = lstPathTableSecond.indexOf(pathTable);
        }
        removeFromIndexToLatest(index);
        buildPathFromPathTable();
    }

    public void removeFromIndexToLatest(int index) {
        if (indexField == 1) {
            if (index >= lstPathTable.size()) {
                return;
            }
            List<PathTable> temp = new ArrayList<>();
            for (int i = index; i < lstPathTable.size(); i++) {
                temp.add(lstPathTable.get(i));
            }
            lstPathTable.removeAll(temp);
        }
        if (indexField == 2) {
            if (index >= lstPathTableSecond.size()) {
                return;
            }
            List<PathTable> temp = new ArrayList<>();
            for (int i = index; i < lstPathTableSecond.size(); i++) {
                temp.add(lstPathTableSecond.get(i));
            }
            lstPathTableSecond.removeAll(temp);
        }
    }

    public void onChangePath(PathTable pathTable, Long indexField) {
        this.indexField = indexField;
        if (this.indexField == 1) {
            int index = lstPathTable.indexOf(pathTable);
            removeFromIndexToLatest(index + 1);
            if (pathTable.getObjectId() > 0) {
                TreeNode treenode;
                if (index == 0) {
                    treenode = lstPathTable.get(index).getNode().getParent();
                } else {
                    treenode = lstPathTable.get(index - 1).getNode();
                }
                InputObject inputObject = findNoteByParent(treenode, pathTable);
                if (inputObject != null) {

                    pathTable.setObjectName(inputObject.getObjectName());
                }
                pathTable.setDataCondition("");
                pathTable.setDataFunction("");
                pathTable.setData("");
            }

            buildPathFromPathTable();
        }
        if (this.indexField == 2) {
            int index = lstPathTableSecond.indexOf(pathTable);
            removeFromIndexToLatest(index + 1);
            if (pathTable.getObjectId() > 0) {
                TreeNode treenode;
                if (index == 0) {
                    treenode = lstPathTableSecond.get(index).getNode().getParent();
                } else {
                    treenode = lstPathTableSecond.get(index - 1).getNode();
                }
                InputObject inputObject = findNoteByParent(treenode, pathTable);
                if (inputObject != null) {

                    pathTable.setObjectName(inputObject.getObjectName());
                }
                pathTable.setDataCondition("");
                pathTable.setDataFunction("");
                pathTable.setData("");
            }

            buildPathFromPathTable();
        }
    }

    public void onChangeInputMode(Long index) {
        if (index == 1) {
            this.currentValue.setOtherString("");
            lstPathTable = new ArrayList<>();
            lstFunctionPathTable = new ArrayList<>();
        }
        if (index == 2) {
            this.currentValue.setOtherStringSecond("");
            lstPathTableSecond = new ArrayList<>();
            lstFunctionPathTableSecond = new ArrayList<>();
        }
    }

    public void onRemoveNomalizerTable(ProcessParam processParam) {
        lstProcessParams.remove(processParam);
    }

    public void onAddDefineValue() {
        ProcessValue pv = new ProcessValue();
        Long valueId = getMaxValueId(lstProcessValue);
        pv.setValueIndex(valueId);
        pv.setValueId(valueId);
        pv.setValueColor("fffcfd");
        lstProcessValue.add(pv);
    }

    public void onChangeValueName(ProcessParam processParam) {
        processParam.setValueName("");
        processParam.setValueColor("fffcfd");
        for (ProcessValue processValue : lstProcessValue) {
            if (processValue.getValueId() == processParam.getParamIndex()) {
                processParam.setValueColor(processValue.getValueColor());
            }
        }

    }

    public void onRemoveDefineValue(ProcessValue pv) {
        if (!DataUtil.isNullOrEmpty(lstProcessParams)) {
            for (ProcessParam processParam : lstProcessParams) {
                if (processParam.getParamIndex() == null){
                    continue;
                }
                if (pv.getValueId() == processParam.getParamIndex()) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("validate.remove.processparam"));
                    return;
                }
            }
        }
        lstProcessValue.remove(pv);

    }

    public void onAddCondition() {
        lstConditionTable.add(new FilterTable());
    }

    public void onAddFunction() {
        lstFunctionTable.add(new FilterTable());
    }

    public void onAddFunctionPath(Long indexField) {
        this.indexField = indexField;
        FilterTable filterTable = new FilterTable();
        if (indexField == 1) {
            if (lstFunctionPathTable.size() == 0) {
                filterTable.setLstFunction(lstFunctionDataTypeOne);
                lstFunctionPathTable.add(filterTable);
            } else {
                filterTable.setLstFunction(lstFunctionData);
                lstFunctionPathTable.add(filterTable);
            }
        }
        if (indexField == 2) {
            if (lstFunctionPathTableSecond.size() == 0) {
                filterTable.setLstFunction(lstFunctionDataTypeOne);
                lstFunctionPathTableSecond.add(filterTable);
            } else {
                filterTable.setLstFunction(lstFunctionData);
                lstFunctionPathTableSecond.add(filterTable);
            }
        }

    }

    public boolean onSaveProcessValue() {
        String previousName = null;

        if (!validRequireFieldList(lstProcessValue, "Default value")) {
            return false;
        }
        for (ProcessValue processValue : lstProcessValue) {
//            if (processValue.getValueId() == 0) {
//                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.positiveInteger"), "Value id");
//                return false;
//            }
            if (!validInputField(processValue.getValueName(), "Value name", true, true, true)) {
                return false;
            }
            if (!validInputField(processValue.getDescription(), "Description", false, true, true)) {
                return false;
            }

        }
        List<ProcessValue> lstData = new ArrayList<>();
        lstData.addAll(lstProcessValue);
        Collections.sort(lstData, new ProcessValueComparator());
        for (ProcessValue processValue : lstData) {
            if (processValue.getValueName().equalsIgnoreCase(previousName)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.valuename"));
                return false;
            } else {
                previousName = processValue.getValueName();
            }
        }
        long previousId = -1;
        Collections.sort(lstData, new ProcessValueIdComparator());
        for (ProcessValue processValue : lstData) {
            if (processValue.getValueId() == previousId) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Value id");
                return false;
            } else {
                previousId = processValue.getValueId();
            }
        }
//TODO save here
        return true;
    }

    public void onFieldConditionChange(FilterTable filterTable) {
        InputObject input = mapInputObject.get(filterTable.getFieldId());
        if (input != null) {

            filterTable.setFieldType(input.getObjectDataType());
            filterTable.setFieldName(input.getObjectName());
        }
        buildDataCondition();
    }

    public boolean buildDataCondition() {
        String str = "";
        for (FilterTable filterTable : lstConditionTable) {
            InputObject input = mapInputObject.get(filterTable.getFieldId());
            if (input == null) {
                return false;
            }
            String value = "";
            switch (input.getObjectDataType().intValue()) {
                case 2:
                    value = filterTable.getFieldValueString();
                    break;
                case 3:
                case 4:
                    value = filterTable.getFieldValueNumber() == null ? "" : filterTable.getFieldValueNumber() + "";
                    break;
                case 5:
                    value = filterTable.getFieldValueDouble() == null ? "" : filterTable.getFieldValueDouble().intValue() + "";
                    break;
                case 6:
                    if (filterTable.getFieldValueBoolean() == 1) {
                        value = "true";
                    } else {
                        value = "false";
                    }
                    break;

            }
            if (DataUtil.isStringNullOrEmpty(value)) {
                return false;
            }
            str += "&" + filterTable.getFieldName() + "=" + value;
        }
        this.currentPathTable.setDataCondition(str.replaceFirst("&", ""));
        this.currentPathTable.setDataByCondition(str.replaceFirst("&", ""));
        return true;
    }

    public void onConditionChange() {
        buildDataCondition();
    }

    public void onFunctionChange() {
        buildDataFunction(lstFunctionTable, 2);
    }

    public void onFunctionPathChange(Long indexField) {
        this.indexField = indexField;
        if (indexField == 1) {
            buildDataFunction(lstFunctionPathTable, 1);
        }
        if (indexField == 2) {
            buildDataFunction(lstFunctionPathTableSecond, 1);
        }
    }

    public boolean buildDataFunction(List<FilterTable> lstFunctionTable, long type, Long indexField) {
        this.indexField = indexField;
        return buildDataFunction(lstFunctionTable, type);
    }

    //	type = 2 is for add filter popup
    public boolean buildDataFunction(List<FilterTable> lstFunctionTable, long type) {
        String str = "";
        for (FilterTable filterTable : lstFunctionTable) {
            Function input = mapFunction.get(filterTable.getFieldId());
            if (input == null) {
                return false;
            }
            String value = "";
            for (FunctionParam functionParam : filterTable.getLstFunctionParams()) {
                switch (functionParam.getValueType().intValue()) {
                    case 1:
                        String valuetemp = functionParam.getFieldValueString() == null ? "" :
                                functionParam.getFieldValueString().trim();
                        if (DataUtil.isStringNullOrEmpty(valuetemp)) {
                            return false;
                        }
                        value += "," + valuetemp;
                        break;
                    case 2:
                        Long valuetemp2 = functionParam.getFieldValueNumber();
                        if (valuetemp2 == null) {
                            return false;
                        }
                        value += "," + valuetemp2;
                        break;
                    case 3:
                        String valuetempDate = DateUtils.formatDatetoString(functionParam.getFieldValueDate(), DATETIME_PPU_ZONE);
                        if (DataUtil.isStringNullOrEmpty(valuetempDate)) {
                            return false;
                        }
                        value += "," + valuetempDate;
                        break;
                    case 4:
                        Long zone = functionParam.getFieldZoneId();
                        if (zone == null||zone == 0L) {
                            return false;
                        }
                        value += "," + zone;
                        break;
                    case 5:
                        Long zoneMapId = functionParam.getFieldZoneMapId();
                        if (zoneMapId == null) {
                            return false;
                        }
                        value += "," + zoneMapId;
                        break;
                    case 6:
                        if (type ==1){
                            Double valuetemp3  = functionParam.getFieldValueDouble() ;
                            if (valuetemp3 == null) {
                                return false;
                            }
                            value += "," + String.format("%.2f", valuetemp3);
                        } else if (type == 2){
                            Long valuetemp3  = functionParam.getFieldValueNumber() ;
                            if (valuetemp3 == null) {
                                return false;
                            }
                            value += "," + valuetemp3;
                        }
                        break;
                }
            }
            str += ":" + filterTable.getFieldName().split("\\(")[0] + "(" + value.replaceFirst(",", "") + ")";
        }
        if (type == 2) {
            this.currentPathTable.setDataFunction(str.replaceFirst(":", ""));
            this.currentPathTable.setDataByFunction(str.replaceFirst(":", ""));
        } else {
            if (indexField == 1) {
                this.currentValue.setOtherString(str.replaceFirst(":", ""));
            }
            if (indexField == 2) {
                this.currentValue.setOtherStringSecond(str.replaceFirst(":", ""));
            }
        }
        return true;
    }

    public void onFieldFunctionChange(FilterTable filterTable) {
        functionChange(filterTable);
        buildDataFunction(lstFunctionTable, 2);
    }

    public void onFieldFunctionPathChange(FilterTable filterTable, Long indexField) {
        this.indexField = indexField;
        functionChange(filterTable);
        if (indexField == 1) {
            buildDataFunction(lstFunctionPathTable, 1);
        }
        if (indexField == 2) {
            buildDataFunction(lstFunctionPathTableSecond, 1);
        }
    }

    public void functionChange(FilterTable filterTable) {
        Function input = mapFunction.get(filterTable.getFieldId());
        if (input == null) {
            filterTable.setLstFunctionParams(new ArrayList<>());
            return;
        }
        filterTable.setFieldType(input.getType());
        filterTable.setFieldName(input.getFunctionDisplay());
        filterTable.resetValue();
        filterTable.setLstFunctionParams(preprocessUnitServiceImpl.getLstFunctionParam(input.getFunctionId()));
    }

    public void onRemoveCondition(FilterTable filterTable) {
        lstConditionTable.remove(filterTable);
        buildDataCondition();
    }

    public void onRemoveFuntion(FilterTable filterTable) {
        lstFunctionTable.remove(filterTable);
        buildDataFunction(lstFunctionTable, 2);
    }

    public void onRemovePathFuntion(FilterTable filterTable, Long indexField) {
        this.indexField = indexField;
        if (indexField == 1) {
            if (lstFunctionPathTable.indexOf(filterTable) == 0) {
                lstFunctionPathTable.clear();
            } else {
                lstFunctionPathTable.remove(filterTable);
            }
            buildDataFunction(lstFunctionPathTable, 1);
        }
        if (indexField == 2) {
            if (lstFunctionPathTableSecond.indexOf(filterTable) == 0) {
                lstFunctionPathTableSecond.clear();
            } else {
                lstFunctionPathTableSecond.remove(filterTable);
            }
            buildDataFunction(lstFunctionPathTableSecond, 1);
        }
    }

    //	================ Build data ================
    public void prepareDataToShowFilterTable(PathTable pathTable, Long indexField) {
        this.indexField = indexField;
        if (indexField == 1) {
            if (pathTable.getObjectId() < 1) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.path"));
                return;
            }
            currentPathTable.cloneData(pathTable);
            realPathTable = pathTable;
            Integer index = lstPathTable.indexOf(pathTable);
            String data = DataUtil.getDataBetweenCurlyBracesByIndex(this.currentValue.getInputFieldsOne(), index + 1);
            pathTable.setData(data);

            InputObject object = (InputObject) pathTable.getNode().getData();
            if ((object.getObjectType() == 2 || object.getObjectType() == 3)
                    && object.getObjectDataType() == 1 && !pathTable.getNode().getChildren().isEmpty()) {
                disableConditiontable = false;
            } else {
                disableConditiontable = true;
            }
            lstConditionData = preprocessUnitServiceImpl.getLstCondition(pathTable.getObjectId());
            lstConditionTable = convertConditionFilterToList(pathTable.getDataCondition());
            lstFunctionTable = convertFunctionFilterToList(pathTable.getDataFunction(),2);
        }
        if (indexField == 2) {
            if (pathTable.getObjectId() < 1) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.path"));
                return;
            }
            currentPathTable.cloneData(pathTable);
            realPathTable = pathTable;
            Integer index = lstPathTableSecond.indexOf(pathTable);
            String data = DataUtil.getDataBetweenCurlyBracesByIndex(this.currentValue.getInputFieldsSecond(), index + 1);
            pathTable.setData(data);

            InputObject object = (InputObject) pathTable.getNode().getData();
            if ((object.getObjectType() == 2 || object.getObjectType() == 3)
                    && object.getObjectDataType() == 1 && !pathTable.getNode().getChildren().isEmpty()) {
                disableConditiontable = false;
            } else {
                disableConditiontable = true;
            }
            lstConditionData = preprocessUnitServiceImpl.getLstCondition(pathTable.getObjectId());
            lstConditionTable = convertConditionFilterToList(pathTable.getDataCondition());
            lstFunctionTable = convertFunctionFilterToList(pathTable.getDataFunction(),2);
        }

    }

    public List<FilterTable> convertConditionFilterToList(String condition) {

        List<FilterTable> lstDataCondition = new ArrayList<>();
        if (!DataUtil.isStringNullOrEmpty(condition)) {
            Map<String, InputObject> map = new HashMap<>();
            for (InputObject inputObject : lstConditionData) {
                map.put(inputObject.getObjectName(), inputObject);
            }
            String[] rows = condition.split("&");
            for (int i = 0; i < rows.length; i++) {
                String row = rows[i];
                String[] fieldAndData = row.split("=");
                InputObject inputObject = map.get(fieldAndData[0]);
                if (inputObject != null) {

                    FilterTable filterTable = new FilterTable();
                    filterTable.setFieldId(inputObject.getObjectId());
                    filterTable.setFieldName(inputObject.getObjectName());
                    filterTable.setFieldType(inputObject.getObjectDataType());
                    String value = fieldAndData[1];
                    switch (inputObject.getObjectDataType().intValue()) {
                        case 2:
                            filterTable.setFieldValueString(value);
                            break;
                        case 3:
                        case 4:
                            filterTable.setFieldValueNumber(Long.parseLong(value));
                            break;
                        case 5:
                            filterTable.setFieldValueDouble(Double.parseDouble(value));
                            break;
                        case 6:
                            if (value.equalsIgnoreCase("true")) {
                                filterTable.setFieldValueBoolean(1);
                            } else {
                                filterTable.setFieldValueBoolean(0);
                            }
                            break;

                    }
                    lstDataCondition.add(filterTable);
                }
            }

        }
        return lstDataCondition;
    }

    public List<FilterTable> convertFunctionFilterToList(String functionData,  Integer type) {
        List<FilterTable> lstDataFunction = new ArrayList<>();
        if (!DataUtil.isStringNullOrEmpty(functionData)) {
            Map<String, Function> map = new HashMap<>();
            for (Function function : lstFunctionData) {
                map.put(function.getFunctionDisplay().substring(0, function.getFunctionDisplay().indexOf("(")) + "_" + function.getNumberParameter(), function);
            }
            for (Function function : lstFunctionDataTypeOne) {
                map.put(function.getFunctionDisplay().substring(0, function.getFunctionDisplay().indexOf("(")) + "_" + function.getNumberParameter(), function);
            }

            String[] rows = functionData.split(":");
            for (int i = 0; i < rows.length; i++) {
                String row = rows[i];
                String functionName = row.split("\\(")[0];
                String argumentStr = DataUtil.getDataBetweenParenthesis(row);
                int paramNum = 0;
                String[] arg = argumentStr.split(",");
                if (!DataUtil.isStringNullOrEmpty(argumentStr)) {
                    paramNum = argumentStr.split(",").length;
                }
                Function function = map.get(functionName + "_" + paramNum);
                if (function == null) {
                    return lstDataFunction;
                }
                FilterTable filterTable = new FilterTable();
                filterTable.setFieldId(function.getFunctionId());
                filterTable.setFieldName(function.getFunctionDisplay());

                List<FunctionParam> lstParams = preprocessUnitServiceImpl.getLstFunctionParam(function.getFunctionId());
                for (FunctionParam functionParam : lstParams) {
                    String value = arg[lstParams.indexOf(functionParam)];
                    switch (functionParam.getValueType().intValue()) {
                        case 1:
                            functionParam.setFieldValueString(value);
                            break;
                        case 2:
                            functionParam.setFieldValueNumber(Long.parseLong(value));
                            break;
                        case 3:
                            functionParam.setFieldValueDate(DateUtils.stringToDate(value, DATETIME_PPU_ZONE));
                            break;
                        case 4:
                            functionParam.setFieldZoneId(Long.parseLong(value));
                            break;
                        case 5:
                            functionParam.setFieldZoneMapId(Long.parseLong(value));
                            break;
                        case 6:
                            if (type == 2){
                                functionParam.setFieldValueNumber(Long.parseLong(value));
                            }else if (type == 1){
                                functionParam.setFieldValueDouble(Double.parseDouble(value));
                            }
                            break;
                    }
                }
                filterTable.setLstFunctionParams(lstParams);
                lstDataFunction.add(filterTable);
            }
        }
        return lstDataFunction;
    }

    public void initPathTree() {
        TreeNode node = getRootNote();
        buildTree(node, lstInputObject);
    }

    public void buildTree(TreeNode node, List<InputObject> lstInputObject) {
        InputObject parentNote = (InputObject) node.getData();
        for (InputObject input : lstInputObject) {
            if (input.getObjectParentId() != null && input.getObjectParentId() == parentNote.getObjectId()) {
                TreeNode childNode = new DefaultTreeNode("object", input, node);
                buildTree(childNode, lstInputObject);
            }
        }
    }

    public TreeNode getRootNote() {
        // root node off tree (parent id is null)
        InputObject rootObject = null;
        for (InputObject it : lstInputObject) {
            if (it.getObjectParentId() == null) {
                rootObject = it;
            }
        }       // root node in framework (not show in view)
        pathRootNode = new DefaultTreeNode(null, null);
        TreeNode note = new DefaultTreeNode("root", rootObject, pathRootNode);
        note.setSelectable(false);
        note.setExpanded(true);
        return note;
    }

    public InputObject findNoteByParent(TreeNode node, PathTable pathTable) {
        List<TreeNode> lstTreeNote = node.getChildren();
        for (TreeNode treeNode : lstTreeNote) {
            InputObject inputObject = (InputObject) treeNode.getData();
            if (inputObject.getObjectId() == pathTable.getObjectId()) {
                pathTable.setNode(treeNode);
                return inputObject;
            }
        }
        return null;
    }

    public void prepateToShowChoosePathPopup(Long index) {
        this.indexField = index;
//		lstPathTable = new ArrayList<>();
    }

    public TreeNode getPathRootNode() {
        return pathRootNode;
    }

    public void setPathRootNode(TreeNode pathRootNode) {
        this.pathRootNode = pathRootNode;
    }

    public class ProcessValueComparator implements Comparator<ProcessValue> {

        @Override
        public int compare(ProcessValue o1, ProcessValue o2) {
            return o1.getValueName().compareTo(o2.getValueName());
        }
    }

    public class ProcessValueIdComparator implements Comparator<ProcessValue> {

        @Override
        public int compare(ProcessValue o1, ProcessValue o2) {
            if (o1.getValueId() - o2.getValueId() >= 0) {
                return 1;
            }
            return -1;
        }
    }

    class SortTable implements Comparator<FilterTable> {

        public int compare(FilterTable a, FilterTable b) {
            return a.getFieldId() - b.getFieldId() > 0 ? 1 : -1;
        }
    }

    class ProcessParamsComparator implements Comparator<ProcessParam> {

        public int compare(ProcessParam a, ProcessParam b) {
            if (a.getPriority() == null && b.getPriority() == null) {
                return 0;
            } else if (a.getPriority() == null && b.getPriority() != null) {
                return -1;
            } else if (a.getPriority() != null && b.getPriority() == null) {
                return 1;
            } else {
                return a.getPriority() - b.getPriority();
            }
        }
    }

    public boolean checkDuplicate(List<FilterTable> lstFilterTable) {
        List<FilterTable> newList = new ArrayList<>();
        newList.addAll(lstFilterTable);
        Collections.sort(newList, new SortTable());
        long previousId = -1;
        for (FilterTable filterTable : newList) {
            if (previousId == filterTable.getFieldId()) {
                return false;
            }
            previousId = filterTable.getFieldId();
        }
        return true;
    }

    public void buildFilterData(List<PathTable> lstPathTable) {
        for (PathTable pathTable : lstPathTable) {
            if (pathTable.getObjectId() > 0) {
                String data = DataUtil.getDataBetweenCurlyBracesByIndex(this.currentValue.getInputFieldsOne(), lstPathTable.indexOf(pathTable) + 1);
                pathTable.setData(data);
            }
        }
    }

    public void buildFilterDataSecond(List<PathTable> lstPathTable) {
        for (PathTable pathTable : lstPathTable) {
            if (pathTable.getObjectId() > 0) {
                String data = DataUtil.getDataBetweenCurlyBracesByIndex(this.currentValue.getInputFieldsSecond(), lstPathTable.indexOf(pathTable) + 1);
                pathTable.setData(data);
            }
        }
    }

    public void prepareEditWhenClick() {
        CheckOnEdit.onEdit = "ppu";
        prepareEdit();
        this.action = false;
    }

    public void prepareEditFromContextMenu() {
        CheckOnEdit.onEdit = "ppu";
        prepareEdit();
        this.action = true;

    }
    
    public void switchOn(){
//        do Nothing
        int x = 5;
        int y = 6;
    }
}
