/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.TreeNode;
import org.primefaces.poseidon.view.data.datatable.ColumnsView;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dto.ConditionTableDTO;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.*;
import vn.viettel.campaign.service.impl.TreeServiceImpl;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Objects;
import java.util.*;
import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import vn.viettel.campaign.dao.ConditionTableDAOInterface;
import vn.viettel.campaign.dto.ResultStringDTO;
import vn.viettel.campaign.service.impl.ResultServiceImpl;
import vn.viettel.campaign.service.impl.ResultTableServiceImpl;
import vn.viettel.campaign.validate.CheckOnEdit;

/**
 * @author ConKC
 */
@ManagedBean(name = "resultTableController")
@ViewScoped
@Getter
@Setter
public class ResultTableController extends BaseController implements Serializable {

    TreeNode parentNode;

    private Result defaultResult;
    private Map<String, String> mapType;

    private Boolean chooseBlockForResultString;
    private Boolean chooseBlockForResult;

    private Boolean test;
    private HashMap resultType;
    private ResultStringDTO resultStringDTO;

    private String radioValue;
    private boolean isDisplay;
    private boolean actonSwitch = false;

    private TreeNode rootNode;
    private TreeNode preprocessUnitRootNode;
    private TreeNode conditionRootNode;
    private TreeNode blockRootNode;

    private TreeNode selectedNode;
    private TreeNode selectedNodeTmp;
    private TreeNode selectedPreprocessUnitNode;
    private TreeNode selectedConditionNode;
    private TreeNode selectedBlockNode;

    private List<Category> categories;
    private List<ConditionTable> conditionTables;
    private List<ResultTable> resultTables;
    private Category category;
    private ConditionTable conditionTable;
    private ResultTable resultTable;
    private Boolean isUpdate;
    private Boolean isDefault;
    private String messageConfirm = "Are you sure to create this Category?";
    private String datePattern = "dd/MM/yyyy";

    private List<ResultTable> resultTableAllData = new ArrayList<>();
    private DualListModel<ResultTable> resultTablePickingList = new DualListModel<>();
    private List<ColumnsView.ColumnModel> columns;
    private List<Long> preProcessIds;
    private List<PreProcessUnit> preProcessUnits;
    private List<ColumnCt> lstColumnCt = new ArrayList<>();
    private List<RowCt> lstRowCt;
    private List<ProcessValue> lstProcessValue;
    private List<Object> lstProcessValueObject;
    private Map<String, String> mapProcessValue = new HashMap<>();
    private List<ConditionTableDTO> conditionTableDTOS;
    private ConditionTableDTO conditionTableDTO;
    private ConditionTableDTO conditionTableDefault;
    @Autowired
    private UssdScenarioService ussdScenarioService;
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private TreeServiceImpl treeService;

    @Autowired
    private CampaignOnlineService campaignOnlineService;

    @Autowired
    SpecialPromService specialPromServiceImpl;

    @Autowired
    private UtilsService utilsService;

    @Autowired
    private ConditionTableService conditionTableService;

    @Autowired
    ConditionTableDAOInterface conditionTableDAO;

    @Autowired
    ResultServiceImpl resultServiceImpl;

    @Autowired
    ResultTableServiceImpl resultTableServiceImpl;

    @Autowired
    PromotionBlockService promotionBlockService;

    @Autowired
    PreprocessUnitInterface preprocessUnitServiceImpl;

    private String editObject;

    @ManagedProperty(value = "#{conditionTableController}")
    ConditionTableController conditionTableController;

    @ManagedProperty(value = "#{preProcessController}")
    PreProcessController preProcessController;

    @ManagedProperty(value = "#{preProcessNumberController}")
    PreProcessNumberController preProcessNumberController;

    @ManagedProperty(value = "#{preProcessExistElementController}")
    PreProcessExistElementController preProcessExistElementController;

    @ManagedProperty(value = "#{preProcessTimeController}")
    PreProcessTimeController preProcessTimeController;

    @ManagedProperty(value = "#{preProcessDateController}")
    PreProcessDateController preProcessDateController;

    @ManagedProperty(value = "#{preProcessNumberRangeController}")
    PreProcessNumberRangeController preProcessNumberRangeController;

    @ManagedProperty(value = "#{preProcessCompareNumberController}")
    PreProcessCompareNumberController preProcessCompareNumberController;

    @ManagedProperty(value = "#{preProcessZoneController}")
    PreProcessZoneController preProcessZoneController;

    @ManagedProperty(value = "#{preProcessSameElementController}")
    PreProcessSameElementController preProcessSameElementController;

    @PostConstruct
    public void init() {
        BigInteger object = (BigInteger) resultServiceImpl.getSequence();
        int value = object.intValueExact();
        int value1 = object.intValue();
        mapType = new HashMap<String, String>();
        mapType.put("1", "NORMAL");
        mapType.put("2", "SKIP");
        mapType.put("3", "NO ACTION");
        mapType.put("4", "EXIT ALL");
        mapType.put("5", "EXIT RULE");
        mapType.put("6", "EXIT SEGMENT");
        mapType.put("7", "EXIT CAMPAIGN");

        this.isDisplay = false;
        this.category = new Category();
        this.categories = new ArrayList<>();
        this.isUpdate = false;
        initTreeNode();

    }

    public void initTreeNode() {
        this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_RESULT_TABLE_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.conditionTables = categoryService.getConditionTableByCategory(longs);
        this.resultTables = categoryService.getResultTableByCategory(longs);
        rootNode = treeService.createResultTableTree(categories, resultTables);
        addExpandedNode(rootNode.getChildren().get(0));
    }

    public void prepareCreateCategory() {
        this.isUpdate = false;
        this.category = new Category();
        this.category.setCategoryType(Constants.CatagoryType.CATEGORY_RESULT_TABLE_TYPE);
        Category parentCat = (Category) selectedNode.getData();
        this.category.setParentId(parentCat.getCategoryId());
        Long id = ussdScenarioService.getNextSequense(Constants.TableName.CATEGORY);
        category.setCategoryId(id);
    }

    public void prepareEditCategory() {
        this.isUpdate = true;
//        messageConfirm = "Are you sure to edit this Category?";
        if (Objects.nonNull(selectedNode)) {
            Category cat = (Category) selectedNode.getData();
            this.category = categoryService.getCategoryById(cat.getCategoryId());
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void doSaveOrUpdateCategory() {
        if (validateCategory()) {
            categoryService.save(category);
            refreshTree();
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public boolean validateCategory() {

        if (DataUtil.isStringNullOrEmpty(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Category name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Category name");
            return false;
        }
        boolean rs = true;
        if (!categoryService.checkDuplicate(category.getName(), category.getCategoryId(), category.getCategoryType())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.category.exist"));
            rs = false;
        }
        return rs;
    }

    public boolean validateCondition() {
        if (DataUtil.isStringNullOrEmpty(this.conditionTable.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Condition Table name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.conditionTable.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Condition Table name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.conditionTable.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Condition Table name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.conditionTable.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.conditionTable.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Description");
            return false;
        }
        // validate nghiep vu
        // valid
        if (DataUtil.isNullOrEmpty(this.preProcessIds)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.condition.column"));
            return false;
        }
        if (DataUtil.isNullOrEmpty(this.conditionTableDTOS)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.condition.row"));
            return false;
        }
        boolean chooseDefault = false;
        boolean isDuplicateCombine = false;
        Map<String, String> mapsCombine = new HashMap<>();
        for (ConditionTableDTO item : this.conditionTableDTOS) {
            if (DataUtil.safeEqual("1", item.getIsDefault())) {
                chooseDefault = true;
            }
            String combine = item.getCombine();
            if (!mapsCombine.containsKey(combine)) {
                mapsCombine.put(combine, combine);
            } else {
                isDuplicateCombine = true;
            }
        }
        if (!chooseDefault) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.condition.default"));
            return false;
        }
        if (isDuplicateCombine) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.condition.combine"));
            return false;
        }
        return true;
    }

    private void refreshTree() {
//        this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_RESULT_TABLE_TYPE);
//        rootNode = treeService.createConditionTree(categories, conditionTables);
//        rootNode.setSelected(true);

        if (isUpdate) {
//            categories.remove((Category) selectedNode.getData());
//            categories.add(this.category);

            int i = categories.indexOf(selectedNode.getData());
            categories.remove(selectedNode.getData());
            categories.add(i, this.category);
        } else {
            categories.add(this.category);
        }

        rootNode = treeService.createResultTableTree(categories, resultTables);
        processRefreshCategory(rootNode, selectedNode, category, isUpdate);
        updateCategoryInformCondition();
    }

    public void refreshData() {
        System.out.println("refreshData");
        TreeNode treeNode = this.selectedNode;
        if (treeNode != null) {
            this.selectedNodeTmp = this.selectedNode;
        }
        if (treeNode != null && treeNode.getData() != null && treeNode.getData() instanceof Category) {
            prepareAddObject();
        } else {
            initTreeNode();
            mapTreeStatus(rootNode);
            prepareEditObject();
        }
    }

    public void doDeleteCategory() {
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();
            if (DataUtil.isNullObject(category.getParentId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.parent.category"));
                return;
            }
            if (categoryService.checkDeleteCategory(this.category.getCategoryId(), ResultTable.class.getSimpleName())) {
                categoryService.deleteCategory(category);
                selectedNode.getChildren().clear();
                TreeNode parNode = selectedNode.getParent();
                selectedNode.getParent().getChildren().remove(selectedNode);
                if (parNode.getChildren().isEmpty()) {
                    removeExpandedNode(parNode);
                }
                selectedNode.setParent(null);
                selectedNode = null;
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
                categories.remove(category);
//                parNode.setExpanded(true);
                updateCategoryInformCondition();
            } else {
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    //    public void prepareEditObject() {
//        this.isDisplay = true;
//        this.isUpdate = true;
//        try {
//            this.conditionTable = (ConditionTable) selectedNode.getData();
//        } catch (Exception e) {
//            this.conditionTable = (ConditionTable) this.selectedNodeTmp.getData();
//        }
//        ConditionTableDTO conditionTableDTO = conditionTableService.getDataConditionTable(conditionTable.getConditionTableId());
//        initDataCondition(conditionTableDTO);      
//    }
//
    public void prepareEditObject() {

        this.conditionTable = new ConditionTable();
        this.conditionTableDTOS = new ArrayList<>();
        this.columns = new ArrayList<>();
        this.lstColumnCt = new ArrayList<>();
        this.lstProcessValue = new ArrayList<>();
        this.preProcessIds = new ArrayList<>();
        this.resultTable = new ResultTable();
        conditionTableDefault = new ConditionTableDTO();

        actonSwitch = false;
        this.isDisplay = true;
        this.isUpdate = true;
        try {
            selectedNode.getChildren().clear();
            doAddConditionToResultTable(selectedNode);
            selectedNode.setExpanded(true);
            this.resultTable = (ResultTable) selectedNode.getData();
        } catch (Exception e) {
            this.resultTable = (ResultTable) this.selectedNodeTmp.getData();
        }

//        if (resultTable.getConditionTableId() != null) {
        ResultTable resultTableTmp = resultTableServiceImpl.getResultTableById(resultTable.getResultTableId());
        resultTable.setCategoryId(resultTableTmp.getCategoryId());
        resultTable.setResultTableId(resultTableTmp.getResultTableId());
        resultTable.setResultTableName(resultTableTmp.getResultTableName());
        resultTable.setDefaultResultId(resultTableTmp.getDefaultResultId());
        resultTable.setConditionTableId(resultTableTmp.getConditionTableId());
        resultTable.setDescription(resultTableTmp.getDescription());
        this.radioValue = "0";
        if (!DataUtil.isNullObject(resultTableTmp.getConditionTableId())) {
            ConditionTableDTO conditionTableDTO = conditionTableService.getDataConditionTable(resultTableTmp.getConditionTableId());
            this.conditionTable = conditionTableDAO.findById(ConditionTable.class, resultTableTmp.getConditionTableId());
            initDataCondition(conditionTableDTO, true);
        } else {

        }
        if (!DataUtil.isNullObject(resultTableTmp.getDefaultResultId())) {
            defaultResult = resultServiceImpl.getResultById(resultTableTmp.getDefaultResultId());
            conditionTableDefault = new ConditionTableDTO();
            String step = DataUtil.isNullObject(defaultResult.getStepIndex()) ? "" : defaultResult.getStepIndex() + "";
            String blockId = DataUtil.isNullObject(defaultResult.getPromotionBlockId()) ? "" : defaultResult.getPromotionBlockId() + "";
            String resultString = mapType.get(defaultResult.getResultType() + "") + "|PromotionId=" + blockId + "|Step=" + step;
            conditionTableDefault.setResultString(resultString);
            conditionTableDefault.setResultType(Long.parseLong(defaultResult.getResultType() + ""));
            if (!DataUtil.isNullObject(defaultResult.getPromotionBlockId())) {
                conditionTableDefault.setPromotionBlockId(defaultResult.getPromotionBlockId().longValue());
            }
            conditionTableDefault.setStepIndex(defaultResult.getStepIndex());
        }
    }

    public void doAddConditionToResultTable(TreeNode resultNote) {
        ResultTable resultTable = (ResultTable) resultNote.getData();
        if (resultTable.getConditionTableId() == null) {
            return;
        }
        ConditionTable conditionTable = campaignOnlineService.getListConditionById(resultTable.getConditionTableId());
        if (conditionTable != null) {
            TreeNode conditionNote = new DefaultTreeNode("condition", conditionTable, resultNote);
            doAddColumnToCondition(conditionNote);
        }
    }

    public void doAddColumnToCondition(TreeNode conditionNote) {
        ConditionTable conditionTable = (ConditionTable) conditionNote.getData();
        List<PreProcessUnit> lstPPU = campaignOnlineService.getListPPUByConditionId(conditionTable.getConditionTableId());
        if (lstPPU != null && !lstPPU.isEmpty()) {
            for (PreProcessUnit preProcessUnit : lstPPU) {
                TreeNode columnNote = new DefaultTreeNode("ppu", preProcessUnit, conditionNote);
            }
        }
    }

    public void initDataCondition(ConditionTableDTO conditionTableDTO, boolean isGetResult) {
        columns = new ArrayList<ColumnsView.ColumnModel>();
//        columns.add(new ColumnsView.ColumnModel("Index", "index"));
//        columns.add(new ColumnsView.ColumnModel("Default", "isDefault"));
        lstColumnCt = conditionTableDTO.getLstColumnCt();
        lstRowCt = conditionTableDTO.getLstRowCt();
//        List<ProcessValue> lstProcessValue = conditionTableDTO.getLstProcessValue();
        Map<String, ProcessValue> mapProcessValue = new HashMap<>();
//        if (!DataUtil.isNullOrEmpty(lstProcessValue)) {
//            for (ProcessValue processValue : lstProcessValue) {
//                mapProcessValue.put(processValue.getValueId(), processValue);
//            }
//        }
        this.preProcessIds = new ArrayList<>();
        this.preProcessUnits = new ArrayList<>();
        if (!DataUtil.isNullOrEmpty(lstColumnCt)) {
            for (ColumnCt columnCt : lstColumnCt) {
                columns.add(new ColumnsView.ColumnModel(columnCt.getColumnName(), "value"));
                preProcessIds.add(columnCt.getPreProcessId());
                PreProcessUnit ppu = preprocessUnitServiceImpl.findById(columnCt.getPreProcessId());
                ppu.setColumnName(columnCt.getColumnName());
                preProcessUnits.add(ppu);
                List<Long> ids = new ArrayList<>();
                ids.add(columnCt.getPreProcessId());
                List<ProcessValue> lstProcessValue = conditionTableService.getProcessValueByValuePreProcessIds(ids);
                for (ProcessValue processValue : lstProcessValue) {
                    mapProcessValue.put(processValue.getValueId() + "_" + columnCt.getPreProcessId(), processValue);
                }
            }
        }
        getDataProcessValue();
//        columns.add(new ColumnsView.ColumnModel("Combine", "combine"));
        // init data conditionTableDTOS
        this.conditionTableDTOS = new ArrayList<>();
        long defaultResultIndex = conditionTableDTO.getConditionTable().getDefaultResultIndex();
        if (!DataUtil.isNullOrEmpty(lstRowCt)) {
            for (RowCt rowCt : lstRowCt) {
                ConditionTableDTO item = new ConditionTableDTO();
                item.setIndex(rowCt.getRowIndex() + "");
                if (defaultResultIndex == rowCt.getRowIndex()) {
                    item.setIsDefault("1");
                }
                String rowValue = rowCt.getValue();
                List<String> values = new ArrayList<>();
                List<String> valueIds = new ArrayList<>();
                List<String> valueColor = new ArrayList<>();
                item.setCombine(rowValue);
                if (!DataUtil.isNullOrEmpty(rowValue)) {
                    List<String> lstIds = DataUtil.splitListFile(rowValue, "/");
                    if (!DataUtil.isNullOrEmpty(lstIds)) {
                        int indexCol = 0;
                        for (String valueId : lstIds) {
                            Long id = preProcessIds.get(indexCol++);
                            ProcessValue processValue = mapProcessValue.get(Long.parseLong(valueId) + "_" + id);
                            if (processValue != null) {
                                values.add(processValue.getValueName());
                                valueIds.add(processValue.getValueId() + "");
                                valueColor.add(processValue.getValueColor() + "");
                            }
                        }
                    }
                }
                item.setValues(values);
                item.setValueIds(valueIds);
                item.setValueColor(valueColor);
//                 get resultString here1
                String resultString = null;

//                List<Result> listResult1 = resultServiceImpl.finAllResult();
                try {
                    if (isGetResult) {
                        long rowIndex = rowCt.getRowIndex();
                        List<Result> listResult = resultServiceImpl.getResultByRowIndexAndResultTableId(rowIndex, resultTable.getResultTableId());
                        Result result = listResult.get(0);
                        String step = DataUtil.isNullObject(result.getStepIndex()) ? "" : result.getStepIndex() + "";
                        String blockId = DataUtil.isNullObject(result.getPromotionBlockId()) ? "" : result.getPromotionBlockId() + "";
                        resultString = mapType.get(result.getResultType() + "") + "|PromotionId=" + blockId + "|Step=" + step;
                        item.setResultString(resultString);
                        item.setResultType(Long.parseLong(result.getResultType() + ""));
                        item.setPromotionBlockId(result.getPromotionBlockId().longValue());
                        item.setStepIndex(result.getStepIndex());
                    }
                } catch (Exception e) {

                }
                conditionTableDTOS.add(item);
            }
        }
        //
        if (!DataUtil.isNullOrEmpty(conditionTableDTOS)) {
            for (ConditionTableDTO dto : conditionTableDTOS) {
                if ("1".equalsIgnoreCase(dto.getIsDefault())) {
                    this.conditionTableDTO = dto;
                    break;
                }
            }
        }
    }

    public void doSaveConditionTable() {
        try {
            conditionTableDTOS.get(0).setPreProcessIds(this.preProcessIds);
        } catch (Exception e) {

        }
        conditionTableService.doSave(this.conditionTable, this.conditionTableDTOS, lstColumnCt);
        initTreeNode();
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public void onChooseCondition() {

        List<Category> categoriesCondition = categoryService.getCategoryByType(Constants.CONDITION_TABLE_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categoriesCondition.isEmpty()) {
            categoriesCondition.forEach(item -> longs.add(item.getCategoryId()));
        }
        List<ConditionTable> listCond = categoryService.getConditionTableByCategory(longs);
        this.conditionRootNode = treeService.createConditionTree(categoriesCondition, listCond);
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('conditionTreePopup').show();");
    }

    public void onRemoveCondition() {
        conditionTable = new ConditionTable();
        columns = new ArrayList<ColumnsView.ColumnModel>();
        lstColumnCt = new ArrayList<>();
        conditionTableDTOS = new ArrayList<>();
        resultTable.setConditionTableId(null);
    }

    public void onChooseBlock() {

        List<Category> categoriesBlock = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_PROMOTION_BLOCK_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categoriesBlock.isEmpty()) {
            categoriesBlock.forEach(item -> longs.add(item.getCategoryId()));
        }
        List<PromotionBlock> listPB = categoryService.getPromotionBlockByCategory(longs);
        this.blockRootNode = treeService.createPromotionBlockTree(categoriesBlock, listPB);
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('promotionBlockTreePopup').show();");
        this.chooseBlockForResult = true;
        this.chooseBlockForResultString = false;
    }

    public void onChooseBlockForResultString() {

        List<Category> categoriesBlock = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_PROMOTION_BLOCK_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categoriesBlock.isEmpty()) {
            categoriesBlock.forEach(item -> longs.add(item.getCategoryId()));
        }
        List<PromotionBlock> listPB = categoryService.getPromotionBlockByCategory(longs);
        this.blockRootNode = treeService.createPromotionBlockTree(categoriesBlock, listPB);
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('promotionBlockTreePopup').show();");
        this.chooseBlockForResult = false;
        this.chooseBlockForResultString = true;
    }

    public boolean onChooseResultString(ConditionTableDTO item) {
        isDefault = DataUtil.isNullObject(item);
        conditionTableDefault = DataUtil.isNullObject(conditionTableDefault) ? new ConditionTableDTO() : conditionTableDefault;
        item = DataUtil.isNullObject(item) ? conditionTableDefault : item;
        this.resultStringDTO = new ResultStringDTO();
        resultStringDTO.setTypeName("Normal");
        resultStringDTO.setTypeValue(item.getResultType());

        if (!DataUtil.isNullObject(item.getPromotionBlockId()) && item.getPromotionBlockId() != 0) {
            resultStringDTO.setPromotionBlockName(promotionBlockService.findById(item.getPromotionBlockId()).getName());
            resultStringDTO.setPromotionBlockId(item.getPromotionBlockId());
        }

        if (!DataUtil.isNullObject(item.getStepIndex()) && item.getResultType() == 2L) {
            resultStringDTO.setStep(Long.parseLong(item.getStepIndex() + ""));
        } else {
            resultStringDTO.setStep(null);
        }
//        PrimeFaces current = PrimeFaces.current();
//        current.executeScript("PF('resultStringPopup').show();");
        conditionTableDTO = DataUtil.isNullObject(item) ? conditionTableDefault : item;
        return true;
    }

    public void onChangeType() {
        this.test = true;

    }

    public void onSelectCondition() {
        if (selectedConditionNode.getData() instanceof ConditionTable) {
            conditionTable = (ConditionTable) selectedConditionNode.getData();
            this.radioValue = "0";
            ConditionTableDTO conditionTableDTO = conditionTableService.getDataConditionTable(conditionTable.getConditionTableId());
            resultTable.setConditionTableId(conditionTable.getConditionTableId());
//            this.conditionTable = conditionTableDAO.findById(ConditionTable.class, conditionTable.getConditionTableId());
            initDataCondition(conditionTableDTO, false);
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.condition.table"));
            return;
        }
    }

    public boolean validatePreprocessUnit() {
        if (DataUtil.isNullObject(resultStringDTO.getTypeValue())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.type.result"));
            return false;
        } else {
            // skip, normal bat buoc chon promotion block
            if (DataUtil.isNullObject(resultStringDTO.getPromotionBlockId())
                    && (resultStringDTO.getTypeValue() == Constants.ResultType.SKIP || resultStringDTO.getTypeValue() == Constants.ResultType.NORMAL)) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.pro.block.result"));
                return false;
            }
//            if (resultStringDTO.getTypeValue() == 2) {
//                if (DataUtil.isNullObject(resultStringDTO.getStep())) {
//                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("required.step"));
//                }
//            }
        }
        //
        if (resultStringDTO.getTypeValue() == 1 || resultStringDTO.getTypeValue() == 3
                || resultStringDTO.getTypeValue() == 4
                || resultStringDTO.getTypeValue() == 5
                || resultStringDTO.getTypeValue() == 6
                || resultStringDTO.getTypeValue() == 7) {
            resultStringDTO.setStep(0L);
        } else if (resultStringDTO.getTypeValue() == 2) {
            if (DataUtil.isNullObject(resultStringDTO.getStep())) {
                resultStringDTO.setStep(0L);
            }
        }
        if (!DataUtil.isNullObject(resultStringDTO.getStep())) {
            conditionTableDTO.setStepIndex(Integer.parseInt(resultStringDTO.getStep() + ""));
        } else {

        }
        if (resultStringDTO.getTypeValue() != 1) {
            conditionTableDTO.setPromotionBlockId(DataUtil.isNullObject(resultStringDTO.getPromotionBlockId()) ? 0L : resultStringDTO.getPromotionBlockId());
        } else {
            conditionTableDTO.setPromotionBlockId(resultStringDTO.getPromotionBlockId());
        }
        conditionTableDTO.setResultType(resultStringDTO.getTypeValue());
        String blockId = DataUtil.isNullObject(resultStringDTO.getPromotionBlockId()) || resultStringDTO.getTypeValue() == 3 ? "" : resultStringDTO.getPromotionBlockId() + "";
        String step = DataUtil.isNullObject(resultStringDTO.getStep()) ? "" : resultStringDTO.getStep() + "";
        String str = mapType.get(resultStringDTO.getTypeValue().toString()) + "|PromotionId=" + blockId + "|Step=" + step;
        conditionTableDTO.setResultString(str);
        if (isDefault) {
            conditionTableDefault = DataUtil.cloneBean(conditionTableDTO);
        }
//        if (this.selectedPreprocessUnitNode == null) {
//            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.preprocess.choose"));
//            return false;
//        }
//        if (this.selectedPreprocessUnitNode.getData() instanceof PreProcessUnit) {
//
//        } else {
//            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.preprocess"));
//            return false;
//        }
//        this.preProcessIds = DataUtil.isNullOrEmpty(this.preProcessIds) ? new ArrayList<>() : this.preProcessIds;
//        long preprocessId = ((PreProcessUnit) this.selectedPreprocessUnitNode.getData()).getPreProcessId();
//        if (preProcessIds.contains(preprocessId)) {
//            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.preprocess.duplicate"));
//            return false;
//        } else {
//            preProcessIds.add(preprocessId);
//        }
//        getDataProcessValue();
//        columns = DataUtil.isNullOrEmpty(this.columns) ? new ArrayList<>() : this.columns;
//        ColumnCt itemAdd = new ColumnCt();
//        itemAdd.setColumnName("PPU" + (lstColumnCt.size() + 1));
//        lstColumnCt.add(itemAdd);
//        columns.add(new ColumnsView.ColumnModel(itemAdd.getColumnName(), "value"));
//        // update lai cac row cu
//        if (!DataUtil.isNullOrEmpty(this.conditionTableDTOS)) {
//            for (ConditionTableDTO dto : conditionTableDTOS) {
//                List<String> valueIds = dto.getValueIds();
//                String combine = dto.getCombine();
//                valueIds.add(this.lstProcessValue.get(0).getValueId() + "");
//                combine = "Combine".equalsIgnoreCase(combine) ? "" : combine;
//                combine = DataUtil.isNullOrEmpty(combine) ? combine + this.lstProcessValue.get(0).getValueId() + "" : combine + "/" + this.lstProcessValue.get(0).getValueId();
//                dto.setCombine(combine);
//                dto.setValueIds(valueIds);
//            }
//        }
        return true;
    }

    public void getDataProcessValue() {
        this.lstProcessValueObject = new ArrayList<>();
//        this.lstProcessValue = conditionTableService.getProcessValueByValuePreProcessIds(this.preProcessIds);
        for (PreProcessUnit unit : preProcessUnits) {
            List<Long> ids = new ArrayList<>();
            ids.add(unit.getPreProcessId());
            List<ProcessValue> lstProcessValue = conditionTableService.getProcessValueByValuePreProcessIds(ids);
            if (DataUtil.isNullOrEmpty(unit.getLstProcessValue())) {
                unit.setLstProcessValue(lstProcessValue);
            }
            this.lstProcessValueObject.add(lstProcessValue);
        }
//        this.mapProcessValue = new HashMap<>();
//        if (!DataUtil.isNullOrEmpty(lstProcessValue)) {
//            for (ProcessValue processValue : lstProcessValue) {
//                this.mapProcessValue.put(processValue.getValueId() + "", processValue.getValueName());
//            }
//        }
    }

    public void prepareAddObject() {
        this.editObject = "result";
        this.isDisplay = true;
        this.isUpdate = false;
        actonSwitch = true;
        this.conditionTable = new ConditionTable();
        this.conditionTableDTOS = new ArrayList<>();
        Category category = (Category) this.selectedNode.getData();
        this.conditionTableDTO = new ConditionTableDTO();
        conditionTable.setCategoryId(category.getCategoryId());
        this.columns = new ArrayList<>();
        this.lstColumnCt = new ArrayList<>();
        this.lstProcessValue = new ArrayList<>();
        this.preProcessIds = new ArrayList<>();
        this.resultTable = new ResultTable();
        long id = ussdScenarioService.getNextSequense(Constants.TableName.RESULT_TABLE);
        this.resultTable.setCategoryId(category.getCategoryId());
        resultTable.setResultTableId(id);
        this.radioValue = "";
        conditionTableDefault = new ConditionTableDTO();
    }

    public void doDeleteObject() {
//        System.out.println("doDeleteObject:" + ((ConditionTable) this.selectedNode.getData()).getConditionTableId());
        resultTableServiceImpl.doDelete((ResultTable) this.selectedNode.getData());
        TreeNode parentNode = selectedNode.getParent();
        parentNode.getChildren().remove(selectedNode);
        if (parentNode.getChildren().size() == 0) {
            removeExpandedNode(parentNode);
        }
        initTreeNode();
        mapTreeStatus(rootNode);
        this.isDisplay = false;
        resultTables.remove(selectedNode.getData());
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public boolean validateDeleteCondition() {
        Long id = ((ResultTable) this.selectedNode.getData()).getResultTableId();
        long count = resultTableServiceImpl.checkUseRule(id);
        if (count > 0) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.rs.used"));
            return false;
        }
        return true;
    }

    public boolean validateForm() {
        String a = "xxx";
        if (resultTable.getName() == null || "".equals(resultTable.getName().trim())) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Result table name is required!", null));
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Result table name");
            return false;
        } else {
            List lst = resultTableServiceImpl.getListToCheckDuplicate(resultTable.getName(), resultTable.getResultTableId() != 0 ? resultTable.getResultTableId() : null);
            if (!DataUtil.isNullOrEmpty(lst)) {
//                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "This result table name is exist in system!", null));
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.rs.exist"));
                return false;
            }
            if (resultTable.getName().contains("%")) {
//                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Result table name not contains % character!", null));
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Result table name");
                return false;
            }
            if (!DataUtil.checkMaxlength(this.resultTable.getName())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Result table name");
                return false;
            }
//            if (resultTable.getName().length() > 255) {
////                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Result table name must be a maximum of 255 characters!", null));
//                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Result table name");
//                return false;
//            }
        }
        //
        // desc
        if (!DataUtil.checkMaxlength(this.resultTable.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.resultTable.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Description");
            return false;
        }
        if (resultTable.getConditionTableId() == null && DataUtil.isNullObject(conditionTableDefault.getResultString())) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Result table must have condition table or default result!", null));
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.rs.exist.condition"));
            return false;
        }
        //
        // conditiontable
//        resultTable.setDefaultResultId(null);
        if (DataUtil.isNullOrEmpty(conditionTableDTOS)) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Result table must have content!", null));
//            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.rs.exist.condition.content"));
//            return false;
        } else {
            for (ConditionTableDTO item : conditionTableDTOS) {
                if (DataUtil.isNullObject(item.getResultString())) {
//                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Result is required!", null));
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.rs.result"));
                    return false;
                }
            }
        }

        // default
//        resultTable.setConditionTableId(null);
//        if (DataUtil.isNullObject(defaultResult)) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Default result is required!", null));
//            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.rs.default.result"));
//            return false;
//        }
        // dosave
//        saveOrUpdate();
        if (!validateChangeCondition()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.condition.change"));
            return false;
        }
        return true;
    }

    public boolean validateChangeCondition() {
        if (!DataUtil.isNullObject(resultTable.getConditionTableId())) {
            ConditionTableDTO conditionTableDTO = conditionTableService.getDataConditionTable(resultTable.getConditionTableId());
            //
            List<ColumnCt> lstColumnCt = conditionTableDTO.getLstColumnCt();
            List<RowCt> lstRowCt = conditionTableDTO.getLstRowCt();
            if (!compareListColumnCt(this.lstColumnCt, lstColumnCt)) {
                initDataCondition(conditionTableDTO, true);
                return false;
            }
            if (!compareListRowCt(this.lstRowCt, lstRowCt)) {
                initDataCondition(conditionTableDTO, true);
                return false;
            }
        }
//        resultTable.setConditionTableId(conditionTable.getConditionTableId());
        return true;
    }

    /**
     * false neu list ko trung
     *
     * @param prevList
     * @param modelList
     * @return
     */
    public boolean compareListColumnCt(List<ColumnCt> prevList, List<ColumnCt> modelList) {
        if (prevList.size() == modelList.size()) {
            int i = 0;
            for (ColumnCt modelListdata : modelList) {
                boolean isOk = false;
//                for (ColumnCt prevListdata : prevList) {
                if (prevList.get(i++).getPreProcessId() == modelListdata.getPreProcessId()) {
                    isOk = true;
                }
//                }
                if (!isOk) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    public boolean compareListRowCt(List<RowCt> prevList, List<RowCt> modelList) {
        if (prevList.size() == modelList.size()) {
            int i = 0;
            for (RowCt modelListdata : modelList) {
                boolean isOk = false;
//                for (RowCt prevListdata : prevList) {
                if (prevList.get(i++).getValue().equalsIgnoreCase(modelListdata.getValue())) {
                    isOk = true;
                }
//                }
                if (!isOk) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    public boolean saveOrUpdate() {
//        if (!validateForm()) {
//            return;
//        }
//        if (!DataUtil.isNullObject(resultTable.getConditionTableId())) {
//            defaultResult = null;
//            resultTable.setDefaultResultId(null);
//        }
//        if (!DataUtil.isNullObject(resultTable.getDefaultResultId())) {
//
//            columns = new ArrayList<ColumnsView.ColumnModel>();
//
//            conditionTableDTOS = new ArrayList<>();
//            conditionTable = new ConditionTable();
//        }

        if (!validateChangeCondition()) {
            return false;
        }

        actonSwitch = false;
        DataUtil.trimObject(resultTable);
        resultTableServiceImpl.updateOrSave(resultTable, defaultResult, conditionTableDTOS, conditionTableDefault);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));

        if ("rule_online".equals(CheckOnEdit.onEdit)) {
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            RuleOnlineController ruleOnlineController = (RuleOnlineController) elContext.getELResolver().getValue(elContext, null, "ruleOnlineController");

            ruleOnlineController.getParentNode().getChildren().remove(ruleOnlineController.getSelectedNode());
            ruleOnlineController.setSelectedNode(new DefaultTreeNode("result", this.resultTable, ruleOnlineController.getParentNode()));
            ruleOnlineController.getSelectedNode().setSelected(true);

            ruleOnlineController.prepareEdit();
            ruleOnlineController.setEditObject("result");
            return true;
        }

        if ("rule_offline".equals(CheckOnEdit.onEdit)) {
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            RuleOfflineController ruleOfflineController = (RuleOfflineController) elContext.getELResolver().getValue(elContext, null, "ruleOfflineController");

            ruleOfflineController.getParentNode().getChildren().remove(ruleOfflineController.getSelectedNode());
            ruleOfflineController.setSelectedNode(new DefaultTreeNode("result", this.resultTable, ruleOfflineController.getParentNode()));
            ruleOfflineController.getSelectedNode().setSelected(true);

            ruleOfflineController.prepareEdit();
            ruleOfflineController.setEditObject("result");
            return true;
        }

        if ("cam_online".equals(CheckOnEdit.onEdit)) {
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            CampaignOnlineController campaignOnlineController = (CampaignOnlineController) elContext.getELResolver().getValue(elContext, null, "campaignOnlineController");

            campaignOnlineController.getParentNode().getChildren().remove(campaignOnlineController.getSelectedNode());
            campaignOnlineController.setSelectedNode(new DefaultTreeNode("result", this.resultTable, campaignOnlineController.getParentNode()));
            campaignOnlineController.getSelectedNode().setSelected(true);

            campaignOnlineController.prepareEdit();
            campaignOnlineController.setEditObject("result");
            return true;
        }

        if ("cam_offline".equals(CheckOnEdit.onEdit)) {
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            CampaignOfflineController campaignOfflineController = (CampaignOfflineController) elContext.getELResolver().getValue(elContext, null, "campaignOfflineController");

            campaignOfflineController.getParentNode().getChildren().remove(campaignOfflineController.getSelectedNode());
            campaignOfflineController.setSelectedNode(new DefaultTreeNode("result", this.resultTable, campaignOfflineController.getParentNode()));
            campaignOfflineController.getSelectedNode().setSelected(true);

            campaignOfflineController.prepareEdit();
            campaignOfflineController.setEditObject("result");
            return true;
        }

//        initTreeNode();
        if (isUpdate) {
            TreeNode parentNote = selectedNode.getParent();
            if (parentNote != null) {
                parentNote.getChildren().remove(selectedNode);
                if (parentNote.getChildren().size() == 0) {
                    removeExpandedNode(parentNote);
                }
            }
        }
        if (!isUpdate) {
            resultTables = DataUtil.isNullOrEmpty(resultTables) ? new ArrayList<>() : resultTables;
            resultTables.add(this.resultTable);
        }
        addNoteToCategoryTree(rootNode, resultTable);
        mapTreeStatus(rootNode);
        isUpdate = true;
        return true;

    }

    public TreeNode addNoteToCategoryTree(TreeNode rootNode, ResultTable conditionTable) {
        TreeNode note = findParentNoteInTree(rootNode, conditionTable);
        if (note != null) {
            TreeNode newNote = new DefaultTreeNode("result", conditionTable, note);
            expandCurrentNode(newNote);
            this.selectedNode = newNote;
            return newNote;
        }
        return null;
    }

    public TreeNode findParentNoteInTree(TreeNode root, ResultTable conditionTable) {
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (Category.class.isInstance(note.getData())) {
                Category data = (Category) note.getData();
                if (DataUtil.safeEqual(data.getCategoryId(), conditionTable.getCategoryId())) {
                    return note;
                }
            }
            currentNote = findParentNoteInTree(note, conditionTable);
            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }

    public void onSelectBlock() {

        if ("block".equals(selectedBlockNode.getType())) {
            PromotionBlock ProBlock = (PromotionBlock) selectedBlockNode.getData();
            if (chooseBlockForResultString == true) {
                resultStringDTO.setPromotionBlockId(ProBlock.getId());
                resultStringDTO.setPromotionBlockName(ProBlock.getName());

            }

            if (chooseBlockForResult == true) {

                if (resultTable.getDefaultResultId() == null) {
                    defaultResult = new Result();
                    Result rs = new Result();
                    rs.setResultType(1);
                    defaultResult = rs;
                    Object obj = resultServiceImpl.getSequence();
                    defaultResult.setResultId(Long.parseLong(obj.toString()));
                    resultTable.setDefaultResultId(defaultResult.getResultId());

                } else {
                    defaultResult = resultServiceImpl.getResultById(resultTable.getDefaultResultId());
                }
                defaultResult.setPromotionBlockId(BigInteger.valueOf(ProBlock.getId()));
                defaultResult = resultServiceImpl.updateOrSave(defaultResult);
                resultTable.setDefaultResultId(defaultResult.getResultId());

            }
        } else {
            // Bạn chưa chọn Block
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Must choose Promotion block!", null));
        }
    }

    public void onRemoveDefault() {
        defaultResult = new Result();
        conditionTableDefault = new ConditionTableDTO();
//        resultTable.setDefaultResultId(null);
    }

    public void onChangeRadio() {

        String xxx = this.radioValue;
        xxx = this.radioValue;

    }

    public TreeNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode rootNode) {
        this.rootNode = rootNode;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(Boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    public List<ConditionTable> getConditionTables() {
        return conditionTables;
    }

    public void setConditionTables(List<ConditionTable> conditionTables) {
        this.conditionTables = conditionTables;
    }

    public ConditionTable getConditionTable() {
        return conditionTable;
    }

    public void setConditionTable(ConditionTable conditionTable) {
        this.conditionTable = conditionTable;
    }

    public boolean getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(boolean isDisplay) {
        this.isDisplay = isDisplay;
    }

    public DualListModel<ResultTable> getResultTablePickingList() {
        return resultTablePickingList;
    }

    public void setResultTablePickingList(DualListModel<ResultTable> resultTablePickingList) {
        this.resultTablePickingList = resultTablePickingList;
    }

    public String getDatePattern() {
        return datePattern;
    }

    public void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
    }

    public void eventListener(ValueChangeEvent event) {
        System.out.println(event.toString());
    }

    public void valueChangeMethod(ValueChangeEvent e) {
        System.out.println(e.getNewValue().toString());
    }

    public String getMessageConfirm() {
        return messageConfirm;
    }

    public void setMessageConfirm(String messageConfirm) {
        this.messageConfirm = messageConfirm;
    }

    public List<ColumnsView.ColumnModel> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnsView.ColumnModel> columns) {
        this.columns = columns;
    }

    public List<ConditionTableDTO> getConditionTableDTOS() {
        return conditionTableDTOS;
    }

    public void setConditionTableDTOS(List<ConditionTableDTO> conditionTableDTOS) {
        this.conditionTableDTOS = conditionTableDTOS;
    }

    public ConditionTableDTO getConditionTableDTO() {
        return conditionTableDTO;
    }

    public void setConditionTableDTO(ConditionTableDTO conditionTableDTO) {
        this.conditionTableDTO = conditionTableDTO;
    }

    public List<Long> getPreProcessIds() {
        return preProcessIds;
    }

    public void setPreProcessIds(List<Long> preProcessIds) {
        this.preProcessIds = preProcessIds;
    }

    public List<ProcessValue> getLstProcessValue() {
        return lstProcessValue;
    }

    public void setLstProcessValue(List<ProcessValue> lstProcessValue) {
        this.lstProcessValue = lstProcessValue;
    }

    public Map<String, String> getMapProcessValue() {
        return mapProcessValue;
    }

    public void setMapProcessValue(Map<String, String> mapProcessValue) {
        this.mapProcessValue = mapProcessValue;
    }

    public TreeNode getPreprocessUnitRootNode() {
        return preprocessUnitRootNode;
    }

    public void setPreprocessUnitRootNode(TreeNode preprocessUnitRootNode) {
        this.preprocessUnitRootNode = preprocessUnitRootNode;
    }

    public TreeNode getSelectedPreprocessUnitNode() {
        return selectedPreprocessUnitNode;
    }

    public void setSelectedPreprocessUnitNode(TreeNode selectedPreprocessUnitNode) {
        this.selectedPreprocessUnitNode = selectedPreprocessUnitNode;
    }

    public boolean getActonSwitch() {
//        System.out.println(actonSwitch);
        return actonSwitch;
    }

    public TreeNode getSelectedNodeTmp() {
        return selectedNodeTmp;
    }

    public void setSelectedNodeTmp(TreeNode selectedNodeTmp) {
        this.selectedNodeTmp = selectedNodeTmp;
    }

    public void setActonSwitch(boolean actonSwitch) {
        this.actonSwitch = actonSwitch;
    }

    public List<Object> getLstProcessValueObject() {
        return lstProcessValueObject;
    }

    public void setLstProcessValueObject(List<Object> lstProcessValueObject) {
        this.lstProcessValueObject = lstProcessValueObject;
    }

    public TreeNode getConditionRootNode() {
        return conditionRootNode;
    }

    public void setConditionRootNode(TreeNode conditionRootNode) {
        this.conditionRootNode = conditionRootNode;
    }

    public String getRadioValue() {
        return radioValue;
    }

    public void setRadioValue(String radioValue) {
        this.radioValue = radioValue;
    }

    public List<ResultTable> getResultTables() {
        return resultTables;
    }

    public void setResultTables(List<ResultTable> resultTables) {
        this.resultTables = resultTables;
    }

    public ResultTable getResultTable() {
        return resultTable;
    }

    public void setResultTable(ResultTable resultTable) {
        this.resultTable = resultTable;
    }

    public List<ColumnCt> getLstColumnCt() {
        return lstColumnCt;
    }

    public void setLstColumnCt(List<ColumnCt> lstColumnCt) {
        this.lstColumnCt = lstColumnCt;
    }

    public TreeNode getBlockRootNode() {
        return blockRootNode;
    }

    public void setBlockRootNode(TreeNode blockRootNode) {
        this.blockRootNode = blockRootNode;
    }

    public TreeNode getSelectedConditionNode() {
        return selectedConditionNode;
    }

    public void setSelectedConditionNode(TreeNode selectedConditionNode) {
        this.selectedConditionNode = selectedConditionNode;
    }

    public TreeNode getSelectedBlockNode() {
        return selectedBlockNode;
    }

    public void setSelectedBlockNode(TreeNode selectedBlockNode) {
        this.selectedBlockNode = selectedBlockNode;
    }

    public HashMap getResultType() {
        return resultType;
    }

    public void setResultType(HashMap resultType) {
        this.resultType = resultType;
    }

    public ResultStringDTO getResultStringDTO() {
        return resultStringDTO;
    }

    public void setResultStringDTO(ResultStringDTO resultStringDTO) {
        this.resultStringDTO = resultStringDTO;
    }

    public Boolean getTest() {
        return test;
    }

    public void setTest(Boolean test) {
        this.test = test;
    }

    public Boolean getChooseBlockForResultString() {
        return chooseBlockForResultString;
    }

    public void setChooseBlockForResultString(Boolean chooseBlockForResultString) {
        this.chooseBlockForResultString = chooseBlockForResultString;
    }

    public Boolean getChooseBlockForResult() {
        return chooseBlockForResult;
    }

    public void setChooseBlockForResult(Boolean chooseBlockForResult) {
        this.chooseBlockForResult = chooseBlockForResult;
    }

    public void prepareEditWhenClick() {
        CheckOnEdit.onEdit = "result";
        prepareEdit();
        this.actonSwitch = false;
    }

    public void prepareEditFromContextMenu() {
        CheckOnEdit.onEdit = "result";
        prepareEdit();
        this.actonSwitch = true;

    }

    public void prepareEdit() {

        this.parentNode = selectedNode.getParent();

        if ("category".equals(selectedNode.getType())) {
            prepareEditCategory();
            PrimeFaces.current().executeScript("PF('carDialog').show();");
        }
        if ("result".equals(selectedNode.getType())) {
            this.editObject = "result";
            prepareEditObject();
        }

        if ("condition".equals(selectedNode.getType())) {
            this.editObject = "condition";
            conditionTableController.setSelectedNode(selectedNode);
            conditionTableController.prepareEditObject();
        }

        if ("ppu".equals(selectedNode.getType())) {
//            ColumnCt columnCt = (ColumnCt) selectedNode.getData();
//            PreProcessUnit ppu = preprocessUnitServiceImpl.findById(columnCt.getPreProcessId());
//            Category cat = categoryService.getCategoryById(ppu.getCategoryId());
            PreProcessUnit ppu = (PreProcessUnit) selectedNode.getData();

            if (Constants.PRE_PROCESS_UNIT_STRING == ppu.getPreProcessType()) {
                this.editObject = "ppu_string";
                preProcessController.setSelectedNode(selectedNode);
                preProcessController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_NUMBER == ppu.getPreProcessType()) {
                this.editObject = "ppu_number";
                preProcessNumberController.setSelectedNode(selectedNode);
                preProcessNumberController.onEditObject();
            }

            if (Constants.PROCESS_UNIT_EXIST_ELEMENT == ppu.getPreProcessType()) {
                this.editObject = "ppu_exist_element";
                preProcessExistElementController.setSelectedNode(selectedNode);
                preProcessExistElementController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_TIME == ppu.getPreProcessType()) {
                this.editObject = "ppu_time";
                preProcessTimeController.setSelectedNode(selectedNode);
                preProcessTimeController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_DATE == ppu.getPreProcessType()) {
                this.editObject = "ppu_date";
                preProcessDateController.setSelectedNode(selectedNode);
                preProcessDateController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_NUMBER_RANGE == ppu.getPreProcessType()) {
                this.editObject = "ppu_number_range";
                preProcessNumberRangeController.setSelectedNode(selectedNode);
                preProcessNumberRangeController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_COMPARE_NUMBER == ppu.getPreProcessType()) {
                this.editObject = "ppu_compare_nember";
                preProcessCompareNumberController.setSelectedNode(selectedNode);
                preProcessCompareNumberController.onEditObject();
            }

            if (Constants.PROCESS_UNIT_ZONE == ppu.getPreProcessType()) {
                this.editObject = "ppu_zone";
                preProcessZoneController.setSelectedNode(selectedNode);
                preProcessZoneController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_SAME_ELEMENT == ppu.getPreProcessType()) {
                this.editObject = "ppu_same_element";
                preProcessSameElementController.setSelectedNode(selectedNode);
                preProcessSameElementController.onEditObject();
            }

        }

    }

    public List<PreProcessUnit> getPreProcessUnits() {
        return preProcessUnits;
    }

    public void setPreProcessUnits(List<PreProcessUnit> preProcessUnits) {
        this.preProcessUnits = preProcessUnits;
    }

    public ConditionTableDTO getConditionTableDefault() {
        return conditionTableDefault;
    }

    public void setConditionTableDefault(ConditionTableDTO conditionTableDefault) {
        this.conditionTableDefault = conditionTableDefault;
    }
}
