package vn.viettel.campaign.controller;

import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.common.DateUtils;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dto.FilterTable;
import vn.viettel.campaign.dto.PathTable;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.PreprocessUnitInterface;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.*;

import static vn.viettel.campaign.common.DateUtils.DATETIME_PPU_ZONE;
import vn.viettel.campaign.validate.CheckOnEdit;

/**
 * @author truongbx
 * @date 9/15/2019
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
public class PreProcessNumberController extends BaseCategoryController<PreProcessUnit> implements Serializable {

    @Autowired
    PreprocessUnitInterface preprocessUnitServiceImpl;
    private TreeNode pathRootNode;
    private TreeNode parameterNode;
    private TreeNode selectedPathNode = new DefaultTreeNode();
    private List<InputObject> lstInputObject = new ArrayList<>();
    private List<Category> lstCategoryParameter = new ArrayList<>();
    private Map<Long, InputObject> mapInputObject = new HashMap<>();
    private Map<String, InputObject> mapInputNameObject = new HashMap<>();
    private Map<Long, Function> mapFunction = new HashMap<>();
    private Map<Long, Function> mapFunctionTypeOne = new HashMap<>();
    private List<PathTable> lstPathTable = new ArrayList<>();
    private List<InputObject> lstConditionData = new ArrayList<>();
    private List<Function> lstFunctionData = new ArrayList<>();
    private List<Function> lstFunctionDataTypeOne = new ArrayList<>();
    private List<FilterTable> lstConditionTable = new ArrayList<>();
    private List<FilterTable> lstFunctionTable = new ArrayList<>();
    private List<FilterTable> lstFunctionPathTable = new ArrayList<>();
    private List<Zone> lstZone = new ArrayList<>();
    private List<ZoneMap> lstZoneMap = new ArrayList<>();
    private List<ProcessValue> lstProcessValue = new ArrayList<>();
    private List<ProcessValue> lstProcessValueTemp = new ArrayList<>();
    private List<ProcessParam> lstProcessParams = new ArrayList<>();
    private List<Parameter> lstParameter = new ArrayList<>();
    private Map<Long, Parameter> mapParameter = new HashMap<>();

    private TreeNode parameterSelectedNode = new DefaultTreeNode();
    private boolean disableConditiontable = true;
    private PathTable currentPathTable = new PathTable();
    private PathTable realPathTable;
    private ProcessParam currentProcessParam;
    private TreeNode zoneRootNode;
    private TreeNode selectedZoneNode = new DefaultTreeNode();
    private FunctionParam currentFunctionParam;
    Map<Long, Zone> mapZone = new HashMap<>();

    @Override
    public void init() {
        this.categoryType = Constants.CatagoryType.CATEGORY_PRE_PROCESS_UNIT_NUMBER_TYPE;
        this.currentClass = PreProcessUnit.class;
        lstInputObject = preprocessUnitServiceImpl.getLstInputObject();
        for (InputObject inputObject : lstInputObject) {
            mapInputObject.put(inputObject.getObjectId(), inputObject);
            mapInputNameObject.put(inputObject.getObjectName() + "_" + inputObject.getObjectParentId(), inputObject);
        }
        initPathTree();
        lstFunctionData = preprocessUnitServiceImpl.getLstFunction();
        lstFunctionDataTypeOne = preprocessUnitServiceImpl.getLstFunctionTypeOne();
        lstFunctionData.forEach((e) -> mapFunction.put(e.getFunctionId(), e));
        lstFunctionDataTypeOne.forEach((e) -> mapFunction.put(e.getFunctionId(), e));
        lstZone = preprocessUnitServiceImpl.getLstZone();
        lstZoneMap = preprocessUnitServiceImpl.getLstZoneMap();
        initTreeParameter();
        initZoneTree();
    }

    public void initTreeParameter() {
        lstCategoryParameter = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_PARAMETER_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!lstCategoryParameter.isEmpty()) {
            lstCategoryParameter.forEach(item -> longs.add(item.getCategoryId()));
        }
        lstParameter = categoryService.getDataFromCatagoryId(longs, Parameter.class.getSimpleName());
        mapParameter = new HashMap<>();
        lstParameter.forEach((e) -> {
            mapParameter.put(e.getParameterId(), e);
        });
//		parameterNode = getRootNoteCatagory(lstTreeCategory);
        initCategoryTree();
//		buildCatagoryAndChildrentTree(parameterNode.getChildren().get(0),lstTreeCategory,lstParameter);
    }

    public void buildCatagoryAndChildrentTree(TreeNode parentNodeCat, List<Category> listCat, List<Parameter> data) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (parentCat.getCategoryId().equals(cat.getParentId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
                buildCatagoryAndChildrentTree(childNodeCat, listCat, data);
            }
        }
        for (Parameter baseCategory : data) {
            if (baseCategory.getCategoryId().equals(parentCat.getCategoryId())) {
                new DefaultTreeNode("parameter", baseCategory, parentNodeCat);
            }
        }
    }

    @Override
    public void initCurrentValue() {
        currentValue = preprocessUnitServiceImpl.getNextSequense();
        currentValue.setPreProcessType(Constants.PRE_PROCESS_UNIT_NUMBER);
        currentValue.setSpecialFields("isUseParameter:" + currentValue.isUsingParameter());
        //HaBM2: Filter for evaluation
        currentValue.setType(Constants.PPU_TYPE_RULE);
    }

    @Override
    public boolean onValidateObject() {
        if (!validInputField(this.currentValue.getPreProcessName(), "PreProcess unit name", true, true, true)) {
            return false;
        }
        if (preprocessUnitServiceImpl.checkExistPreProcessName(this.currentValue.getPreProcessName(), this.currentValue.getPreProcessId())) {
            duplidateMessage("PreProcess unit name");
            return false;

        }
        if (!validInputField(this.currentValue.getDescription(), "Description", false, true, true)) {
            return false;
        }
        if (!validRequireField(this.currentValue.getDefaultValue(), "Default value")) {
            return false;
        }
        if (!validRequireField(this.currentValue.getInputMode(), "Input mode")) {
            return false;
        }

        if (this.currentValue.getInputMode() == 1) {
            if (!validRequireFieldList(lstPathTable, "Path table")) {
                return false;
            }
            for (PathTable pathTable : lstPathTable) {
                if (pathTable.getObjectId() == -1) {
                    notCompleteMessage("Path table");
                    return false;
                }
            }

        } else if (this.currentValue.getInputMode() == 2) {
            if (!validRequireFieldList(lstFunctionPathTable, "Path table")) {
                return false;
            }
            boolean validFunction = buildDataFunction(lstFunctionPathTable, 1);
            if (!validFunction) {
                notCompleteMessage("Path table");
                return false;
            }
        }
        if (!validRequireFieldList(lstProcessParams, "Normalizer table")) {
            return false;
        }
        List<ProcessParam> lstData = new ArrayList<>();
        lstData.addAll(lstProcessParams);
        Collections.sort(lstData, new ProcessParamsComparator());
        Integer i = -1;
        Map<Long, String> map = new HashMap<>();
        for (ProcessParam processParam : lstData) {
            map.put(processParam.getParamIndex(), "1");
            processParam.calculateConfigInputPPUNumber(this.currentValue.isUsingParameter());
            if (!validRequireField(processParam.getParamIndex(), "Value name in normalizer table")) {
                return false;
            }
            if (!validRequireField(processParam.getValue(), "Parameter value in normalizer table")) {
                return false;
            }
            if (!DataUtil.checkMaxlength(processParam.getValue())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "parameter value");
                return false;
            }
            if (!validRequireField(processParam.getPriority(), "Priority in normalizer table")) {
                return false;
            }

            if (i == processParam.getPriority()) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.table"), "priority", "normalizer");
                return false;
            }
            i = processParam.getPriority();
        }
        map.put(this.currentValue.getDefaultValue(), "");
        for (ProcessValue processValue : lstProcessValue) {
            if (map.get(processValue.getValueId()) == null) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.ppu.processValue"));
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean doDelete() {
        return preprocessUnitServiceImpl.onDeletePreprocessUnit(this.currentValue.getPreProcessId());
    }

    @Override
    public boolean onSaveObject() {
        boolean result = preprocessUnitServiceImpl.onSavePreprocessUnit(this.currentValue, lstProcessParams, lstProcessValue);
        return result;
    }

    @Override
    public void doEdit() {
        lstProcessValue = preprocessUnitServiceImpl.getLstProcessValue(currentValue.getPreProcessId());
        lstProcessParams = preprocessUnitServiceImpl.getLstProcessParam(currentValue.getPreProcessId());
        parseProcessParam();
        lstPathTable = new ArrayList<>();
        lstFunctionPathTable = new ArrayList<>();
        Map<Long, ProcessValue> mapColor = new HashMap<>();
        for (ProcessValue processValue : lstProcessValue) {
            mapColor.put(processValue.getValueId(), processValue);
        }
        for (ProcessParam processParam : lstProcessParams) {
            ProcessValue processValue = mapColor.get(processParam.getParamIndex());
            if (processValue != null) {
                processParam.setValueColor(processValue.getValueColor());
                processParam.setValueName(processValue.getValueName());
            }
            if (processParam.isParam()) {
                Long id = processParam.getParameterId();
                if (id == null || id.equals(0) || mapParameter.get(id) == null) {
                    processParam.setValue("");
                } else {
                    Parameter parameter = mapParameter.get(id);
                    if (parameter != null) {
                        processParam.setValue(parameter.getParameterName());
                    }
                }
            }
        }
        if (this.currentValue.getInputMode() == 1) {
            initDataForPathTable();
        } else {
            lstFunctionPathTable = convertFunctionFilterToList(this.currentValue.getOtherString(), 1);
            for (int i = 0; i < lstFunctionPathTable.size(); i++) {
                if (i == 0) {
                    lstFunctionPathTable.get(0).setLstFunction(lstFunctionDataTypeOne);
                } else {
                    lstFunctionPathTable.get(i).setLstFunction(lstFunctionData);
                }
            }
        }
//
    }

    public void parseProcessParam() {
        if (lstProcessParams.isEmpty()) {
            return;
        }
        for (ProcessParam processParam : lstProcessParams) {
            String[] data = processParam.getConfigInput().split(";");
            if (data.length != 4 && this.currentValue.isUsingParameter()) {
                return;
            }
            if (data.length != 3 && !this.currentValue.isUsingParameter()) {
                return;
            }
            int i = 0;
            String paramId = data[i].substring(data[i].indexOf(":") + 1);
            i++;
            if (this.currentValue.isUsingParameter()) {
                String booleanValue = data[i].substring(data[i].indexOf(":") + 1);
                if (booleanValue.equalsIgnoreCase("true")) {
                    processParam.setParam(true);
                } else {
                    processParam.setParam(false);
                }
                i++;
            }
            if (processParam.isParam()) {
                if (DataUtil.isStringNullOrEmpty(paramId)) {
                    paramId = "0";
                }
                processParam.setParameterId(Long.parseLong(paramId));
            } else {
                processParam.setValue(paramId);
            }
            processParam.setType(Integer.parseInt(data[i].substring(data[i].indexOf(":") + 1)));
            i++;
            processParam.setPriority(Integer.parseInt(data[i].substring(data[i].indexOf(":") + 1)));
            processParam.setConfigInput("");
        }
    }

    public void initDataForPathTable() {
        if (!DataUtil.isStringNullOrEmpty(this.currentValue.getOtherString())) {
            String dataTable = this.currentValue.getOtherString();
            String[] table = dataTable.split("\\.");
            for (int i = 0; i < table.length; i++) {
                int toIndex = table[i].indexOf("{") == -1 ? table[i].length() : table[i].indexOf("{");
                String name = table[i].substring(0, toIndex);
                PathTable pathTable = new PathTable();
                PathTable parent = new PathTable();
                if (i == 0) {
                    TreeNode parentNode = pathRootNode.getChildren().get(0);
                    InputObject input = (InputObject) parentNode.getData();
                    parent.setObjectId(input.getObjectId());
                    parent.setNode(parentNode);
                } else {
                    parent = lstPathTable.get(lstPathTable.size() - 1);
                }
                pathTable.setObjectName(name);
                InputObject inputObject = mapInputNameObject.get(name + "_" + parent.getObjectId());
                if (inputObject != null) {
                    pathTable.setObjectId(inputObject.getObjectId());
                }
                findNoteByParent(parent.getNode(), pathTable);
                List<InputObject> lstInputObject = getChildDataOfNode(pathTable.getNode().getParent());
                pathTable.setLstInput(lstInputObject);
                lstPathTable.add(pathTable);
            }
            buildFilterData();
        }

    }

    @Override
    public boolean onUpdateObject() {
        boolean result = preprocessUnitServiceImpl.onUpdatePreprocessUnit(this.currentValue, lstProcessParams, lstProcessValue);
        return result;
    }

    @Override
    public void rollbackData() {
        PreProcessUnit data = preprocessUnitServiceImpl.findById(this.currentValue.getPreProcessId());
        this.currentValue.asMap(data);
    }

    @Override
    public void doAdd() {
        lstPathTable = new ArrayList<>();
        lstProcessParams = new ArrayList<>();
        lstProcessValue = new ArrayList<>();
    }

    //=============== Handle action ================
    public boolean validateChoosePath() {
        if (selectedPathNode == null) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.object.choose"));
            return false;
        }

        List<PathTable> data = new ArrayList<>();
        getDataToPathTable(data, selectedPathNode);
        lstPathTable = new ArrayList<>();
        lstPathTable.addAll(data);
        buildPathFromPathTable();
        return true;
    }

    public boolean validateApplyFilter() {

        boolean valid = buildDataCondition();
        if (!disableConditiontable && !valid) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notComplete"), "Condition");
            return false;
        }
        boolean checkCondition = validateInputCondition(lstConditionTable, mapInputObject);
        if (!disableConditiontable && !checkCondition) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("valid.parameter"));
            return false;
        }

        valid = checkDuplicate(lstConditionTable);
        if (!disableConditiontable && !valid) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.table"), "field name", "condition");
            return false;
        }
        boolean validFunction = buildDataFunction(lstFunctionTable, 2);
        if (!validFunction) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notComplete"), "Function");
            return false;
        }
        boolean checkFunction = validateInputFunction(lstFunctionTable);
        if (!checkFunction) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("valid.parameter"));
            return false;
        }
        validFunction = checkDuplicate(lstFunctionTable);
        if (!validFunction) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.table"), "function name", "Function");
            return false;
        }
        realPathTable.cloneData(currentPathTable);
        buildPathFromPathTable();
        return true;
    }

    public void getDataToPathTable(List<PathTable> data, TreeNode selectedPathNode) {
        InputObject inputObject = (InputObject) selectedPathNode.getData();
        if (inputObject.getObjectParentId() == null) {
            return;
        }
        PathTable pathTable = new PathTable();
        pathTable.setObjectId(inputObject.getObjectId());
        pathTable.setObjectName(inputObject.getObjectName());
        pathTable.setNode(selectedPathNode);
        List<InputObject> lstInputObject = getChildDataOfNode(selectedPathNode.getParent());
        pathTable.setLstInput(lstInputObject);
        data.add(0, pathTable);
        getDataToPathTable(data, selectedPathNode.getParent());

    }

    public List<InputObject> getChildDataOfNode(TreeNode node) {
        List<TreeNode> lstTreeNote = node.getChildren();
        List<InputObject> lstInputObject = new ArrayList<>();
        for (TreeNode treeNode : lstTreeNote) {
            lstInputObject.add((InputObject) treeNode.getData());
        }
        return lstInputObject;
    }

    public void buildPathFromPathTable() {
        StringBuilder stringBuilder = new StringBuilder();
        for (PathTable pathTable : lstPathTable) {
            if (pathTable.getObjectId() > 0) {
                if (DataUtil.isStringNullOrEmpty(pathTable.getDataCondition()) && DataUtil.isStringNullOrEmpty(pathTable.getDataFunction())) {
                    stringBuilder.append("." + pathTable.getObjectName() + "{}");
                } else {
                    stringBuilder.append("." + pathTable.getObjectName()
                            + "{" + pathTable.getDataCondition() + ";" + pathTable.getDataFunction() + "}");
                }
            }
        }
        this.currentValue.setOtherString(stringBuilder.toString().replaceFirst(".", ""));
        buildFilterData();
    }

    public boolean addChildPath() {
        if (lstPathTable.isEmpty()) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.dont.parent"));
            return false;
        }
        PathTable pathTable = lstPathTable.get(lstPathTable.size() - 1);
        if (pathTable.getObjectId() == -1) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.dont.child"));
            return false;
        }
        List<InputObject> lstValue = getChildDataOfNode(pathTable.getNode());
        if (lstValue == null || lstValue.isEmpty()) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.dont.child"));
            return false;
        }
        PathTable newPath = new PathTable();
        newPath.setLstInput(lstValue);
        lstPathTable.add(newPath);
        return true;
    }

    public void addNomalizerTable() {
        ProcessParam param = new ProcessParam();
        param.setPriority(lstProcessParams.size() + 1);
        lstProcessParams.add(0, param);
    }

    public void onRemovePath(PathTable pathTable) {
        int index = lstPathTable.indexOf(pathTable);
        removeFromIndexToLatest(index);
        buildPathFromPathTable();
    }

    public void removeFromIndexToLatest(int index) {
        if (index >= lstPathTable.size()) {
            return;
        }
        List<PathTable> temp = new ArrayList<>();
        for (int i = index; i < lstPathTable.size(); i++) {
            temp.add(lstPathTable.get(i));
        }
        lstPathTable.removeAll(temp);
    }

    public void onChangePath(PathTable pathTable) {
        int index = lstPathTable.indexOf(pathTable);
        removeFromIndexToLatest(index + 1);
        if (pathTable.getObjectId() > 0) {
            TreeNode treenode;
            if (index == 0) {
                treenode = lstPathTable.get(index).getNode().getParent();
            } else {
                treenode = lstPathTable.get(index - 1).getNode();
            }
            InputObject inputObject = findNoteByParent(treenode, pathTable);
            if (inputObject != null) {
                pathTable.setObjectName(inputObject.getObjectName());
            }
            pathTable.setDataCondition("");
            pathTable.setDataFunction("");
            pathTable.setData("");
        }

        buildPathFromPathTable();
    }

    public void onChangeInputMode() {
        this.currentValue.setOtherString("");
        lstPathTable = new ArrayList<>();
        lstFunctionPathTable = new ArrayList<>();
    }

    public void onRemoveNomalizerTable(ProcessParam processParam) {
        lstProcessParams.remove(processParam);
    }

    public void onAddDefineValue() {
        ProcessValue pv = new ProcessValue();
        Long valueId = getMaxValueId(lstProcessValueTemp);
        pv.setValueIndex(valueId);
        pv.setValueId(valueId);
        pv.setValueColor("fffcfd");
        lstProcessValueTemp.add(0, pv);
    }

    public void onChangeValueName(ProcessParam processParam) {
        processParam.setValueName("");
        processParam.setValueColor("fffcfd");
        for (ProcessValue processValue : lstProcessValue) {
            if (processValue.getValueId() == processParam.getParamIndex()) {
                processParam.setValueColor(processValue.getValueColor());
                processParam.setValueName(processValue.getValueName());
            }
        }

    }

    public void onRemoveDefineValue(ProcessValue pv) {
        if (!DataUtil.isNullOrEmpty(lstProcessParams)) {
            for (ProcessParam processParam : lstProcessParams) {
                if (processParam.getParamIndex() == null) {
                    continue;
                }
                if (pv.getValueId() == processParam.getParamIndex()) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("validate.remove.processparam"));
                    return;
                }
                if (pv.getValueId() == this.currentValue.getDefaultValue()) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("validate.remove.defaulValue"));
                    return;
                }
            }
        }
        lstProcessValueTemp.remove(pv);
//        int i = 1;
//        for (ProcessValue processValue : lstProcessValueTemp) {
//            processValue.setValueId(i);
//            processValue.setValueIndex(i);
//            i++;
//        }
    }

    public void onAddCondition() {
        lstConditionTable.add(new FilterTable());
    }

    public void onAddFunction() {
        lstFunctionTable.add(new FilterTable());
    }

    public void onAddFunctionPath() {
        FilterTable filterTable = new FilterTable();
        if (lstFunctionPathTable.size() == 0) {
            filterTable.setLstFunction(lstFunctionDataTypeOne);
            lstFunctionPathTable.add(filterTable);
        } else {
            filterTable.setLstFunction(lstFunctionData);
            lstFunctionPathTable.add(filterTable);
        }

    }

    public boolean onSaveProcessValue() {
        String previousName = null;

        if (!validRequireFieldList(lstProcessValueTemp, "Default value")) {
            return false;
        }
        for (ProcessValue processValue : lstProcessValueTemp) {
            if (processValue.getValueId() == 0) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.positiveInteger"), "Value id");
                return false;
            }
            if (!validInputField(processValue.getValueName(), "Value name", true, true, true)) {
                lstProcessValue = lstProcessValueOld;
                return false;
            }
            if (!validInputField(processValue.getDescription(), "Description", false, true, true)) {
                return false;
            }

        }
        List<ProcessValue> lstData = new ArrayList<>();
        lstData.addAll(lstProcessValueTemp);
        Collections.sort(lstData, new ProcessValueComparator());
        for (ProcessValue processValue : lstData) {
            if (processValue.getValueName().equalsIgnoreCase(previousName)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.valuename"));
                return false;
            } else {
                previousName = processValue.getValueName();
            }
        }
        long previousId = -1;
        Collections.sort(lstData, new ProcessValueIdComparator());
        for (ProcessValue processValue : lstData) {
            if (processValue.getValueId() == previousId) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Value id");
                return false;
            } else {
                previousId = processValue.getValueId();
            }
        }
        lstProcessValue = new ArrayList<>();
        lstProcessValue.addAll(lstProcessValueTemp);
        // chekc doi mau khi sua value table
        if (!DataUtil.isNullOrEmpty(lstProcessParams)) {
            for (ProcessParam processParam : lstProcessParams) {
                Long index = processParam.getParamIndex();
                for (ProcessValue processValue : lstProcessValue) {
                    if (processValue.getValueId() == index) {
                        processParam.setValueColor(processValue.getValueColor());
                        break;
                    }
                }
            }
        } // end chekc doi mau khi sua value table
//TODO save here
        return true;
    }

    public void onFieldConditionChange(FilterTable filterTable) {
        InputObject input = mapInputObject.get(filterTable.getFieldId());
        if (input != null) {
            filterTable.setFieldType(input.getObjectDataType());
            filterTable.setFieldName(input.getObjectName());
            filterTable.resetValue();
            buildDataCondition();
        }
    }

    public boolean buildDataCondition() {
        String str = "";
        for (FilterTable filterTable : lstConditionTable) {
            InputObject input = mapInputObject.get(filterTable.getFieldId());
            if (input == null) {
                return false;
            }
            String value = "";
            switch (input.getObjectDataType().intValue()) {
                case 2:
                    value = filterTable.getFieldValueString() == null ? "" : filterTable.getFieldValueString().trim();
                    break;
                case 3:
                case 4:
                    value = filterTable.getFieldValueNumber() == null ? "" : filterTable.getFieldValueNumber() + "";
                    break;
                case 5:
                    value = filterTable.getFieldValueDouble() == null ? "" : filterTable.getFieldValueDouble().intValue() + "";
                    break;
                case 6:
                    if (filterTable.getFieldValueBoolean() == 1) {
                        value = "true";
                    } else {
                        value = "false";
                    }
                    break;

            }
            if (DataUtil.isStringNullOrEmpty(value)) {
                return false;
            }
            str += "&" + filterTable.getFieldName() + "=" + value;
        }
        this.currentPathTable.setDataCondition(str.replaceFirst("&", ""));
        this.currentPathTable.setDataByCondition(str.replaceFirst("&", ""));
        return true;
    }

    public void onConditionChange() {
        buildDataCondition();
    }

    public void onFunctionChange() {
        buildDataFunction(lstFunctionTable, 2);
    }

    public void onFunctionPathChange() {
        buildDataFunction(lstFunctionPathTable, 1);
    }

    //	type = 2 is for add filter popup
    public boolean buildDataFunction(List<FilterTable> lstFunctionTable, long type) {
        String str = "";
        for (FilterTable filterTable : lstFunctionTable) {
            Function input = mapFunction.get(filterTable.getFieldId());
            if (input == null) {
                return false;
            }
            String value = "";
            for (FunctionParam functionParam : filterTable.getLstFunctionParams()) {
                switch (functionParam.getValueType().intValue()) {
                    case 1:
                        String valuetemp = functionParam.getFieldValueString() == null ? ""
                                : functionParam.getFieldValueString().trim();
                        if (DataUtil.isStringNullOrEmpty(valuetemp)) {
                            return false;
                        }
                        value += "," + valuetemp;
                        break;
                    case 2:
                        Long valuetemp2 = functionParam.getFieldValueNumber();
                        if (valuetemp2 == null) {
                            return false;
                        }
                        value += "," + valuetemp2;
                        break;
                    case 3:
                        String valuetempDate = DateUtils.formatDatetoString(functionParam.getFieldValueDate(), DATETIME_PPU_ZONE);
                        if (DataUtil.isStringNullOrEmpty(valuetempDate)) {
                            return false;
                        }
                        value += "," + valuetempDate;
                        break;
                    case 4:
                        Long zone = functionParam.getFieldZoneId();
                        if (zone == null || zone == 0L) {
                            return false;
                        }
                        value += "," + zone;
                        break;
                    case 5:
                        Long zoneMapId = functionParam.getFieldZoneMapId();
                        if (zoneMapId == null) {
                            return false;
                        }
                        value += "," + zoneMapId;
                        break;
                    case 6:
                        if (type == 1) {
                            Double valuetemp3 = functionParam.getFieldValueDouble();
                            if (valuetemp3 == null) {
                                return false;
                            }
                            value += "," + String.format("%.2f", valuetemp3);
                        } else if (type == 2) {
                            Long valuetemp3 = functionParam.getFieldValueNumber();
                            if (valuetemp3 == null) {
                                return false;
                            }
                            value += "," + valuetemp3;
                        }
                        break;
                }
            }
            str += ":" + filterTable.getFieldName().split("\\(")[0] + "(" + value.replaceFirst(",", "") + ")";
        }
        if (type == 2) {
            this.currentPathTable.setDataFunction(str.replaceFirst(":", ""));
            this.currentPathTable.setDataByFunction(str.replaceFirst(":", ""));
        } else {
            this.currentValue.setOtherString(str.replaceFirst(":", ""));
        }
        return true;
    }

    public void onFieldFunctionChange(FilterTable filterTable) {
        functionChange(filterTable);
        buildDataFunction(lstFunctionTable, 2);
    }

    public void onFieldFunctionPathChange(FilterTable filterTable) {
        functionChange(filterTable);
        buildDataFunction(lstFunctionPathTable, 1);
    }

    public void functionChange(FilterTable filterTable) {
        Function input = mapFunction.get(filterTable.getFieldId());
        if (input == null) {
            filterTable.setLstFunctionParams(new ArrayList<>());
            return;
        }
        filterTable.setFieldType(input.getType());
        filterTable.setFieldName(input.getFunctionDisplay());
        filterTable.resetValue();
        filterTable.setLstFunctionParams(preprocessUnitServiceImpl.getLstFunctionParam(input.getFunctionId()));
    }

    public void onRemoveCondition(FilterTable filterTable) {
        lstConditionTable.remove(filterTable);
        buildDataCondition();
    }

    public void onRemoveFuntion(FilterTable filterTable) {
        lstFunctionTable.remove(filterTable);
        buildDataFunction(lstFunctionTable, 2);
    }

    public void onRemovePathFuntion(FilterTable filterTable) {
        if (lstFunctionPathTable.indexOf(filterTable) == 0) {
            lstFunctionPathTable.clear();
        } else {
            lstFunctionPathTable.remove(filterTable);
        }
        buildDataFunction(lstFunctionPathTable, 1);
    }

    public void onParameterChange() {
        lstProcessParams = new ArrayList<>();
    }

    //	================ Build data ================
    public void prepareDataToShowFilterTable(PathTable pathTable) {
        if (pathTable.getObjectId() < 1) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.path"));
            return;
        }
        currentPathTable.cloneData(pathTable);
        realPathTable = pathTable;
        Integer index = lstPathTable.indexOf(pathTable);
        String data = DataUtil.getDataBetweenCurlyBracesByIndex(this.currentValue.getInputFields(), index + 1);
        pathTable.setData(data);

        InputObject object = (InputObject) pathTable.getNode().getData();
        if ((object.getObjectType() == 2 || object.getObjectType() == 3)
                && object.getObjectDataType() == 1 && !pathTable.getNode().getChildren().isEmpty()) {
            disableConditiontable = false;
        } else {
            disableConditiontable = true;
        }
        lstConditionData = preprocessUnitServiceImpl.getLstCondition(pathTable.getObjectId());
        lstConditionTable = convertConditionFilterToList(pathTable.getDataCondition());
        lstFunctionTable = convertFunctionFilterToList(pathTable.getDataFunction(), 2);

    }

    public List<FilterTable> convertConditionFilterToList(String condition) {

        List<FilterTable> lstDataCondition = new ArrayList<>();
        if (!DataUtil.isStringNullOrEmpty(condition)) {
            Map<String, InputObject> map = new HashMap<>();
            for (InputObject inputObject : lstConditionData) {
                map.put(inputObject.getObjectName(), inputObject);
            }
            String[] rows = condition.split("&");
            for (int i = 0; i < rows.length; i++) {
                String row = rows[i];
                String[] fieldAndData = row.split("=");
                InputObject inputObject = map.get(fieldAndData[0]);
                FilterTable filterTable = new FilterTable();
                if (filterTable != null) {
                    if (inputObject != null) {
                        filterTable.setFieldId(inputObject.getObjectId());
                        filterTable.setFieldName(inputObject.getObjectName());
                        filterTable.setFieldType(inputObject.getObjectDataType());
                        String value = fieldAndData[1];
                        switch (inputObject.getObjectDataType().intValue()) {
                            case 2:
                                filterTable.setFieldValueString(value);
                                break;
                            case 3:
                            case 4:
                                filterTable.setFieldValueNumber(Long.parseLong(value));
                                break;
                            case 5:
                                filterTable.setFieldValueDouble(Double.parseDouble(value));
                                break;
                            case 6:
                                if (value.equalsIgnoreCase("true")) {
                                    filterTable.setFieldValueBoolean(1);
                                } else {
                                    filterTable.setFieldValueBoolean(0);
                                }
                                break;

                        }
                        lstDataCondition.add(filterTable);
                    }
                }
            }

        }
        return lstDataCondition;
    }

    public List<FilterTable> convertFunctionFilterToList(String functionData, Integer type) {
        List<FilterTable> lstDataFunction = new ArrayList<>();
        if (!DataUtil.isStringNullOrEmpty(functionData)) {
            Map<String, Function> map = new HashMap<>();
            for (Function function : lstFunctionData) {
                map.put(function.getFunctionDisplay().substring(0, function.getFunctionDisplay().indexOf("(")) + "_" + function.getNumberParameter(), function);
            }
            for (Function function : lstFunctionDataTypeOne) {
                map.put(function.getFunctionDisplay().substring(0, function.getFunctionDisplay().indexOf("(")) + "_" + function.getNumberParameter(), function);
            }

            String[] rows = functionData.split(":");
            for (int i = 0; i < rows.length; i++) {
                String row = rows[i];
                String functionName = row.split("\\(")[0];
                String argumentStr = DataUtil.getDataBetweenParenthesis(row);
                int paramNum = 0;
                String[] arg = argumentStr.split(",");
                if (!DataUtil.isStringNullOrEmpty(argumentStr)) {
                    paramNum = argumentStr.split(",").length;
                }
                Function function = map.get(functionName + "_" + paramNum);
                if (function == null) {
                    return lstDataFunction;
                }
                FilterTable filterTable = new FilterTable();
                filterTable.setFieldId(function.getFunctionId());
                filterTable.setFieldName(function.getFunctionDisplay());

                List<FunctionParam> lstParams = preprocessUnitServiceImpl.getLstFunctionParam(function.getFunctionId());
                for (FunctionParam functionParam : lstParams) {
                    String value = arg[lstParams.indexOf(functionParam)];
                    switch (functionParam.getValueType().intValue()) {
                        case 1:
                            functionParam.setFieldValueString(value);
                            break;
                        case 2:
                            functionParam.setFieldValueNumber(Long.parseLong(value));
                            break;
                        case 3:
                            functionParam.setFieldValueDate(DateUtils.stringToDate(value, DATETIME_PPU_ZONE));
                            break;
                        case 4:
                            functionParam.setFieldZoneId(Long.parseLong(value));
                            Zone zone = mapZone.get(functionParam.getFieldZoneId());
                            if (zone != null) {
                                functionParam.setFieldZoneName(zone.getZoneName());
                            }
                            break;
                        case 5:
                            functionParam.setFieldZoneMapId(Long.parseLong(value));
                            break;
                        case 6:
                            if (type == 2) {
                                functionParam.setFieldValueNumber(Long.parseLong(value));
                            } else if (type == 1) {
                                functionParam.setFieldValueDouble(Double.parseDouble(value));
                            }
                            break;
                    }
                }
                filterTable.setLstFunctionParams(lstParams);
                lstDataFunction.add(filterTable);
            }
        }
        return lstDataFunction;
    }

    public void initPathTree() {
        TreeNode node = getRootNote();
        buildTree(node, lstInputObject);
    }

    public void initCategoryTree() {
        TreeNode node = getRootParameterNote();
        buildCatagoryAndChildrentTree(node, lstCategoryParameter, lstParameter);
        parameterNode.getChildren().get(0).setExpanded(true);
    }

    public void buildTree(TreeNode node, List<InputObject> lstInputObject) {
        InputObject parentNote = (InputObject) node.getData();
        for (InputObject input : lstInputObject) {
            if (input.getObjectParentId() != null && input.getObjectParentId() == parentNote.getObjectId()) {
                TreeNode childNode = new DefaultTreeNode("object", input, node);
                buildTree(childNode, lstInputObject);
            }
        }
    }

    public TreeNode getRootNote() {
        // root node off tree (parent id is null)
        InputObject rootObject = null;
        for (InputObject it : lstInputObject) {
            if (it.getObjectParentId() == null) {
                rootObject = it;
            }
        }       // root node in framework (not show in view)
        pathRootNode = new DefaultTreeNode(null, null);
        TreeNode note = new DefaultTreeNode("root", rootObject, pathRootNode);
        note.setSelectable(false);
        note.setExpanded(true);
        return note;
    }

    public TreeNode getRootParameterNote() {
        // root node off tree (parent id is null)
        Category rootObject = null;
        for (Category it : lstCategoryParameter) {
            if (it.getParentId() == null) {
                rootObject = it;
            }
        }       // root node in framework (not show in view)
        parameterNode = new DefaultTreeNode(null, null);
        TreeNode note = new DefaultTreeNode("category", rootObject, parameterNode);
        note.setSelectable(false);
        return note;
    }

    public InputObject findNoteByParent(TreeNode node, PathTable pathTable) {
        List<TreeNode> lstTreeNote = node.getChildren();
        for (TreeNode treeNode : lstTreeNote) {
            InputObject inputObject = (InputObject) treeNode.getData();
            if (inputObject.getObjectId() == pathTable.getObjectId()) {
                pathTable.setNode(treeNode);
                return inputObject;
            }
        }
        return null;
    }

    public void prepateToShowChoosePathPopup() {
//		lstPathTable = new ArrayList<>();
    }

    public TreeNode getPathRootNode() {
        return pathRootNode;
    }

    public void setPathRootNode(TreeNode pathRootNode) {
        this.pathRootNode = pathRootNode;
    }

    public class ProcessValueComparator implements Comparator<ProcessValue> {

        @Override
        public int compare(ProcessValue o1, ProcessValue o2) {
            return o1.getValueName().compareTo(o2.getValueName());
        }
    }

    class SortTable implements Comparator<FilterTable> {

        public int compare(FilterTable a, FilterTable b) {
            return a.getFieldId() - b.getFieldId() > 0 ? 1 : -1;
        }
    }

    class ProcessParamsComparator implements Comparator<ProcessParam> {

        public int compare(ProcessParam a, ProcessParam b) {
            if (a.getPriority() == null && b.getPriority() == null) {
                return 0;
            } else if (a.getPriority() == null && b.getPriority() != null) {
                return -1;
            } else if (a.getPriority() != null && b.getPriority() == null) {
                return 1;
            } else {
                return a.getPriority() - b.getPriority();
            }
        }
    }

    public boolean checkDuplicate(List<FilterTable> lstFilterTable) {
        List<FilterTable> newList = new ArrayList<>();
        newList.addAll(lstFilterTable);
        Collections.sort(newList, new SortTable());
        long previousId = -1;
        for (FilterTable filterTable : newList) {
            if (previousId == filterTable.getFieldId()) {
                return false;
            }
            previousId = filterTable.getFieldId();
        }
        return true;
    }

    public boolean validateChooseParameter() {
        if (parameterSelectedNode == null || parameterSelectedNode.getType().equalsIgnoreCase("category")) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.parameter.choose"));
            return false;
        }
        Parameter parameter = (Parameter) parameterSelectedNode.getData();
        currentProcessParam.setValue(parameter.getParameterName());
        currentProcessParam.setParameterId(parameter.getParameterId());
        return true;
    }

    public void prepareToShowParameterDialog(ProcessParam processParam) {
        this.currentProcessParam = processParam;
        parameterSelectedNode = null;
        initCategoryTree();
        parameterSelectedNode = findParameterNoteInTree(parameterNode, processParam.getParameterId());
        if (parameterSelectedNode != null) {
            parameterSelectedNode.setSelected(true);
        }
    }

    public TreeNode getParameterNode() {
        return parameterNode;
    }

    public void setParameterNode(TreeNode parameterNode) {
        this.parameterNode = parameterNode;
    }

    public void changeParameter(ProcessParam processParam) {
        processParam.setValue(null);
        processParam.setConfigInput("");
        checkUssingParamStatus();
    }

    public class ProcessValueIdComparator implements Comparator<ProcessValue> {

        @Override
        public int compare(ProcessValue o1, ProcessValue o2) {
            if (o1.getValueId() - o2.getValueId() >= 0) {
                return 1;
            }
            return -1;
        }
    }

    private List<ProcessValue> lstProcessValueOld = new ArrayList<>();

    public void prepareToShowDefineValuePopup() {
        lstProcessValueTemp = new ArrayList<>();
        lstProcessValueTemp.addAll(lstProcessValue);
        lstProcessValueOld = cloneListProcessValue(lstProcessValue);
    }

    public void checkUssingParamStatus() {
        for (ProcessParam processParam : lstProcessParams) {
            if (processParam.isParam()) {
                this.currentValue.setUsingParameter(true);
                currentValue.setSpecialFields("isUseParameter:" + currentValue.isUsingParameter());
                return;
            }
        }
        this.currentValue.setUsingParameter(false);
        currentValue.setSpecialFields("isUseParameter:" + currentValue.isUsingParameter());
    }

    public void buildFilterData() {
        for (PathTable pathTable : lstPathTable) {
            if (pathTable.getObjectId() > 0) {
                String data = DataUtil.getDataBetweenCurlyBracesByIndex(this.currentValue.getInputFields(), lstPathTable.indexOf(pathTable) + 1);
                pathTable.setData(data);
            }
        }

    }

    public void prepareEditWhenClick() {
        CheckOnEdit.onEdit = "ppu";
        prepareEdit();
        this.action = false;
    }

    public void prepareEditFromContextMenu() {
        CheckOnEdit.onEdit = "ppu";
        prepareEdit();
        this.action = true;

    }

    public void initZoneTree() {
        List<ZoneMap> lstZoneMap = preprocessUnitServiceImpl.getLstZoneMap();
        List<Zone> lstZone = preprocessUnitServiceImpl.getLstZone();
        mapZone = new HashMap<>();
        lstZone.forEach((e) -> {
            mapZone.put(e.getZoneId(), e);
        });
        zoneRootNode = treeService.createZoneZonMapTree(lstZoneMap, lstZone);
    }

    public boolean validateChooseZone() {
        if (selectedZoneNode == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("not.valid.choose.zone"));
            return false;
        }
        if (selectedZoneNode.getData() instanceof Category) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("not.valid.choose.cat"));
            return false;
        }
        Zone zone = (Zone) selectedZoneNode.getData();
        currentFunctionParam.setFieldZoneId(zone.getZoneId());
        currentFunctionParam.setFieldZoneName(zone.getZoneName());
        buildDataFunction(lstFunctionPathTable, 1);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        return true;
    }

    public void prepareChooseZone(FunctionParam functionParam) {
        initZoneTree();
        currentFunctionParam = functionParam;
        selectedZoneNode = null;
    }
}
