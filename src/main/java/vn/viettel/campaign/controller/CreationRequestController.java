package vn.viettel.campaign.controller;

//import com.viettel.vocs.cba.camp.common.config.Constant;
//import com.viettel.vocs.cba.camp.common.message.RequestMessage;
//import com.viettel.vocs.cba.camp.wapi.builder.BlacklistBuilder;
//import com.viettel.vocs.cba.camp.wapi.builder.RequestBuilder;
//import com.viettel.vocs.cba.camp.wapi.builder.SegmentBuilder;
//import com.viettel.vocs.cba.camp.wapi.builder.SpecialPromBuilder;
//import com.viettel.vocs.cba.camp.wapi.common.RequestFuture;
//import com.viettel.vocs.cba.camp.wapi.hdfs.HDFSProcessCommon;
//import com.viettel.vocs.cba.camp.wapi.obj.ValidateResponse;
//import com.viettel.vocs.cba.camp.wapi.produce.RequestProducer;
//import com.viettel.vocs.cba.camp.wapi.utils.WebAPIUtils;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.common.DateUtils;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dto.ComboboxDTO;
import vn.viettel.campaign.dto.CreateFileDTO;
import vn.viettel.campaign.dto.RequestBuildDTO;
import vn.viettel.campaign.dto.RequestCountDTO;
import vn.viettel.campaign.dto.RequestDTO;
import vn.viettel.campaign.dto.RequestStatusDTO;
import vn.viettel.campaign.dto.SpecialOfferDTO;
import vn.viettel.campaign.entities.BlackList;
import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.entities.Request;
import vn.viettel.campaign.entities.Segment;
import vn.viettel.campaign.entities.SpecialProm;
import vn.viettel.campaign.service.CategoryService;
import vn.viettel.campaign.service.RequestService;
import vn.viettel.campaign.service.SegmentService;
import vn.viettel.campaign.service.UtilsService;
import vn.viettel.campaign.service.impl.TreeServiceImpl;
import vn.viettel.campaign.service.SpecialPromService;
import vn.viettel.campaign.service.BlacklistService;
import vn.viettel.campaign.service.impl.RequestServiceImpl;
import vn.viettel.campaign.service.impl.UtilsServiceImpl;

/**
 *
 * @author ConKC
 */
@ManagedBean
@ViewScoped
public class CreationRequestController extends BaseController implements Serializable {

    public static long TIMEOUT_SEND = 30000L;
    public final static String NOT_FOUND_ROOT_CATEGORY = "Not found root category ^^";
    public final static String SEND_SUCCESS = "Success send ^^!";
    public final static String SEND_ERROR = "Create request fail, please try again ^^!";
    public final static String SEND_TIMEOUT = "Timeout or Exception send, please try agaign ^^!";
    public final static String VALIDATE_EXCEPTION = "Have exception, please try again ^^!";
    public final static String DUPLICATE_EXCEPTION = "Duplicate msisdn, please check row number ^^!";
    public final static String VALIDATE_ERROR = "Fail validate, please check row number ^^!";
    public final static String FILE_EMPTY = "File empty, please check again ^^!";
    public final static String SUCCESS = "Success validate ^^!";

    private int serviceId;
    private RequestStatusDTO requestStatusDTO;
    private List<RequestDTO> requests;
    private List<Category> categorys;
    private List<Segment> segments;
    private RequestDTO request;
    private Request selectedRequest;
    private int stepsIndex;
    private List<ComboboxDTO> objects;
    private List<ComboboxDTO> actions;
    private boolean displayBtnNext;
    private boolean displayDetailSeg;
    private boolean displayDetailBL;
    private boolean displayDetailSO;
    private int object_action;
    private TreeNode rootNodeSegment;
    private TreeNode selectedNodeSegment;
    private TreeNode rootNodeBlacklist;
    private TreeNode selectedNodeBlacklist;
    private TreeNode rootNodeSpecialOffer;
    private TreeNode selectedNodeSpecialOffer;
    private RequestBuildDTO requestBuildDTO;
    private boolean disableBLName;
    private boolean disableBLCat;
    private boolean disableOSName;
    private boolean disableOSCat;
    private int interval;
    private int chooseFileStep3;
    private List<CreateFileDTO> createFileDTOs;
    private List<SpecialOfferDTO> specialOfferDTOs;
    private CreateFileDTO createFileDTO;
    private boolean disableBtnCreate;
    private boolean disableBtnValidate;
    private boolean disableBtnBuild;
    private Long countRequestNew;
    private String titlePopup;
    private Long categoryId;

    @Autowired
    private RequestService requestService;

    @Autowired
    private UtilsService utilsService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SegmentService segmentService;

    @Autowired
    private BlacklistService blacklistService;

    @Autowired
    private SpecialPromService specialPromService;

    @Autowired
    private TreeServiceImpl treeService;

    @PostConstruct
    public void init() {
        final int timeAuto = Integer.valueOf((String) utilsService.getObject("time.auto.update.request.interval", "600"));
        this.interval = timeAuto;
        this.requestBuildDTO = new RequestBuildDTO();
        this.stepsIndex++;
        this.displayBtnNext = true;
        this.displayDetailSO = false;
        this.disableBLName = false;
        this.disableBLCat = false;
        this.requestStatusDTO = requestService.getRequestStatus();
        this.requests = requestService.getAllRequestScalar();
        displayDetailBL = false;
        displayDetailSeg = false;
        chooseFileStep3 = 1;
        createFileDTOs = new ArrayList<>();
        titlePopup = StringUtils.EMPTY;
    }

    public void doReload() {
        this.requestStatusDTO = requestService.getRequestStatus();
        this.requests = requestService.getAllRequestScalar();
        RequestCountDTO countDTO = requestService.countRequestUpdate(countRequestNew);
        this.countRequestNew = countDTO.getId();
        Long count = countDTO.getCountRequest();
        if (count != null && count > 0) {
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("notification.update.request.new"), countDTO.getCountRequest());
        }
    }

    public void prepareCreateRequest() {
        this.titlePopup = utilsService.getTex("title.popup.create.request");
        this.requestBuildDTO = new RequestBuildDTO();
        this.stepsIndex = 0;
        this.displayBtnNext = true;
        this.objects = requestService.generateObjects();
    }

    public void initTreeNodeSegment() {
        List<Category> catLst = categoryService.getCategoryByType(Constants.SEGMENT, false);
        List<Long> integers = new ArrayList<>();
        if (!catLst.isEmpty()) {
            catLst.forEach(item -> integers.add(item.getCategoryId()));
        }
        List<Segment> segmentLst = segmentService.findSegmentByCategory(integers);
        List<Object> objectLst = new ArrayList<>(segmentLst);
        rootNodeSegment = treeService.createTreeCategoryAndComponentObj(catLst, objectLst);
        treeService.expandAll(rootNodeSegment);
    }

    public void onSelectSegment() {
        if (Objects.nonNull(selectedNodeSegment)
                && selectedNodeSegment.getData() instanceof Segment) {
            this.displayDetailSeg = true;
            this.displayBtnNext = false;
            Segment segment = (Segment) selectedNodeSegment.getData();
            this.requestBuildDTO.setSegmentName(StringUtils.isEmpty(segment.getName()) ? StringUtils.EMPTY : segment.getName());
            this.requestBuildDTO.setSegmentId(Objects.isNull(segment.getSegmentId()) ? null : segment.getSegmentId());
            this.requestBuildDTO.setTempPath(StringUtils.isEmpty(segment.getTempPath()) ? StringUtils.EMPTY : segment.getTempPath());
            this.requestBuildDTO.setFailedPath(StringUtils.isEmpty(segment.getFailedPath()) ? StringUtils.EMPTY : segment.getFailedPath());
            this.requestBuildDTO.setHistPath(StringUtils.isEmpty(segment.getHistPath()) ? StringUtils.EMPTY : segment.getHistPath());
            this.requestBuildDTO.setPath(StringUtils.isEmpty(segment.getPath()) ? StringUtils.EMPTY : segment.getPath());
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void onChangeSegmentName() {
        String segmentName = requestBuildDTO.getSegmentName().trim().toLowerCase();
        segmentName = segmentName.replaceAll("\\s{2,}", " ").replaceAll(" ", "_");
        if (segmentName.isEmpty()) {
            this.requestBuildDTO.setPath(StringUtils.EMPTY);
            this.requestBuildDTO.setTempPath(StringUtils.EMPTY);
            this.requestBuildDTO.setHistPath(StringUtils.EMPTY);
            this.requestBuildDTO.setFailedPath(StringUtils.EMPTY);
        } else {
            this.requestBuildDTO.setPath(UtilsServiceImpl.getProperty(Constants.SpringProperty.SEGMENT_PATH) + segmentName + ".txt");
            this.requestBuildDTO.setTempPath(UtilsServiceImpl.getProperty(Constants.SpringProperty.SEGMENT_TEMP_PATH) + segmentName + ".txt");
            this.requestBuildDTO.setHistPath(UtilsServiceImpl.getProperty(Constants.SpringProperty.SEGMENT_HIST_PATH) + segmentName + ".txt");
            this.requestBuildDTO.setFailedPath(UtilsServiceImpl.getProperty(Constants.SpringProperty.SEGMENT_FAILED_PATH) + segmentName + ".txt");
        }
    }

    public void onChangeBlacklistName() {
        String blacklistName = requestBuildDTO.getBlacklistName().trim().toLowerCase();
        blacklistName = blacklistName.replaceAll("\\s{2,}", " ").replaceAll(" ", "_");
        if (blacklistName.isEmpty()) {
            this.requestBuildDTO.setPath(StringUtils.EMPTY);
            this.requestBuildDTO.setTempPath(StringUtils.EMPTY);
            this.requestBuildDTO.setHistPath(StringUtils.EMPTY);
            this.requestBuildDTO.setFailedPath(StringUtils.EMPTY);
        } else {
            this.requestBuildDTO.setPath(UtilsServiceImpl.getProperty(Constants.SpringProperty.BLACKLIST_PATH) + blacklistName + ".txt");
            this.requestBuildDTO.setTempPath(UtilsServiceImpl.getProperty(Constants.SpringProperty.BLACKLIST_TEMP_PATH) + blacklistName + ".txt");
            this.requestBuildDTO.setHistPath(UtilsServiceImpl.getProperty(Constants.SpringProperty.BLACKLIST_HIST_PATH) + blacklistName + ".txt");
            this.requestBuildDTO.setFailedPath(UtilsServiceImpl.getProperty(Constants.SpringProperty.BLACKLIST_FAILED_PATH) + blacklistName + ".txt");
        }
    }

    public void onChangeSpecialOfferName() {
        String specialPromName = requestBuildDTO.getSpecialOfferName().trim().toLowerCase();
        specialPromName = specialPromName.replaceAll("\\s{2,}", " ").replaceAll(" ", "_");
        if (specialPromName.isEmpty()) {
            this.requestBuildDTO.setPath(StringUtils.EMPTY);
            this.requestBuildDTO.setTempPath(StringUtils.EMPTY);
            this.requestBuildDTO.setHistPath(StringUtils.EMPTY);
            this.requestBuildDTO.setFailedPath(StringUtils.EMPTY);
        } else {
            this.requestBuildDTO.setPath(UtilsServiceImpl.getProperty(Constants.SpringProperty.SPECIAL_OFFER_PATH) + specialPromName + ".txt");
            this.requestBuildDTO.setTempPath(UtilsServiceImpl.getProperty(Constants.SpringProperty.SPECIAL_OFFER_TEMP_PATH) + specialPromName + ".txt");
            this.requestBuildDTO.setHistPath(UtilsServiceImpl.getProperty(Constants.SpringProperty.SPECIAL_OFFER_HIST_PATH) + specialPromName + ".txt");
            this.requestBuildDTO.setFailedPath(UtilsServiceImpl.getProperty(Constants.SpringProperty.SPECIAL_OFFER_FAILED_PATH) + specialPromName + ".txt");
        }
    }

    public void onChangeCheckBoxBlacklistName() {
        if (!disableBLName) {
            this.requestBuildDTO.setBlacklistName(StringUtils.isEmpty(requestBuildDTO.getBlacklistNameOld())
                    ? StringUtils.EMPTY : requestBuildDTO.getBlacklistNameOld());
            this.requestBuildDTO.setTempPath(StringUtils.isEmpty(requestBuildDTO.getTempPathOld())
                    ? StringUtils.EMPTY : requestBuildDTO.getTempPathOld());
            this.requestBuildDTO.setHistPath(StringUtils.isEmpty(requestBuildDTO.getHistPathOld())
                    ? StringUtils.EMPTY : requestBuildDTO.getHistPathOld());
            this.requestBuildDTO.setFailedPath(StringUtils.isEmpty(requestBuildDTO.getFailedPathOld())
                    ? StringUtils.EMPTY : requestBuildDTO.getFailedPathOld());
            this.requestBuildDTO.setCurrentPath(StringUtils.isEmpty(requestBuildDTO.getCurrentPathOld())
                    ? StringUtils.EMPTY : requestBuildDTO.getCurrentPathOld());

        } else {
            this.requestBuildDTO.setBlacklistName(StringUtils.EMPTY);
        }
    }

    public void onChangeCheckboxSpecialOfferName() {
        if (!disableOSName) {
            this.requestBuildDTO.setSpecialOfferName(StringUtils.isEmpty(requestBuildDTO.getSpecialOfferNameOld())
                    ? StringUtils.EMPTY : requestBuildDTO.getSpecialOfferNameOld());
            this.requestBuildDTO.setTempPath(StringUtils.isEmpty(requestBuildDTO.getTempPathOld())
                    ? StringUtils.EMPTY : requestBuildDTO.getTempPathOld());
            this.requestBuildDTO.setHistPath(StringUtils.isEmpty(requestBuildDTO.getHistPathOld())
                    ? StringUtils.EMPTY : requestBuildDTO.getHistPathOld());
            this.requestBuildDTO.setFailedPath(StringUtils.isEmpty(requestBuildDTO.getFailedPathOld())
                    ? StringUtils.EMPTY : requestBuildDTO.getFailedPathOld());
            this.requestBuildDTO.setPath(StringUtils.isEmpty(requestBuildDTO.getCurrentPathOld())
                    ? StringUtils.EMPTY : requestBuildDTO.getCurrentPathOld());
        } else {
            this.requestBuildDTO.setSpecialOfferName(StringUtils.EMPTY);
        }
    }

    public void onChangeCheckboxCat() {
        if (!disableBLCat) {
            if (requestBuildDTO.getCategoryIdOld() >= 0) {
                this.requestBuildDTO.setCategoryId(requestBuildDTO.getCategoryIdOld());
            }
        }
    }

    public void onSelectBlacklist() {
        if (Objects.nonNull(selectedNodeBlacklist)
                && selectedNodeBlacklist.getData() instanceof BlackList) {
            this.displayDetailBL = true;
            this.displayBtnNext = false;
            BlackList blacklist = (BlackList) selectedNodeBlacklist.getData();
            this.requestBuildDTO.setBlacklistName(StringUtils.isEmpty(blacklist.getName()) ? StringUtils.EMPTY : blacklist.getName());
            this.requestBuildDTO.setBlacklistId(Objects.isNull(blacklist.getBlacklistId()) ? null : blacklist.getBlacklistId());
            this.requestBuildDTO.setTempPath(StringUtils.isEmpty(blacklist.getTempPath()) ? StringUtils.EMPTY : blacklist.getTempPath());
            this.requestBuildDTO.setFailedPath(StringUtils.isEmpty(blacklist.getFailedPath()) ? StringUtils.EMPTY : blacklist.getFailedPath());
            this.requestBuildDTO.setHistPath(StringUtils.isEmpty(blacklist.getHistPath()) ? StringUtils.EMPTY : blacklist.getHistPath());
            this.requestBuildDTO.setPath(StringUtils.isEmpty(blacklist.getPath()) ? StringUtils.EMPTY : blacklist.getPath());
            this.requestBuildDTO.setCurrentPath(StringUtils.isEmpty(blacklist.getPath()) ? StringUtils.EMPTY : blacklist.getPath());
            this.requestBuildDTO.setCategoryId(blacklist.getCategoryId());
            this.requestBuildDTO.setCategoryIdOld(blacklist.getCategoryId());
            this.requestBuildDTO.setBlacklistNameOld(StringUtils.isEmpty(blacklist.getName()) ? StringUtils.EMPTY : blacklist.getName());

            this.requestBuildDTO.setTempPathOld(StringUtils.isEmpty(blacklist.getTempPath()) ? StringUtils.EMPTY : blacklist.getTempPath());
            this.requestBuildDTO.setFailedPathOld(StringUtils.isEmpty(blacklist.getFailedPath()) ? StringUtils.EMPTY : blacklist.getFailedPath());
            this.requestBuildDTO.setHistPathOld(StringUtils.isEmpty(blacklist.getHistPath()) ? StringUtils.EMPTY : blacklist.getHistPath());
            this.requestBuildDTO.setCurrentPathOld(StringUtils.isEmpty(blacklist.getPath()) ? StringUtils.EMPTY : blacklist.getPath());

            this.disableBLName = false;
            this.disableBLCat = false;
        } else {
            this.requestBuildDTO = new RequestBuildDTO(requestBuildDTO.getObject(), requestBuildDTO.getAction());
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void onSelectSpecialOffer() {
        if (Objects.nonNull(selectedNodeSpecialOffer)
                && selectedNodeSpecialOffer.getData() instanceof SpecialProm) {
            this.displayDetailSO = true;
            this.displayDetailBL = true;
            this.displayBtnNext = false;
            SpecialProm specialprom = (SpecialProm) selectedNodeSpecialOffer.getData();
            this.requestBuildDTO.setSpecialOfferName(StringUtils.isEmpty(specialprom.getName()) ? StringUtils.EMPTY : specialprom.getName());
            this.requestBuildDTO.setSpecialOfferId(Objects.isNull(specialprom.getSpecialPromId()) ? null : specialprom.getSpecialPromId());
            this.requestBuildDTO.setTempPath(StringUtils.isEmpty(specialprom.getTempPath()) ? StringUtils.EMPTY : specialprom.getTempPath());
            this.requestBuildDTO.setFailedPath(StringUtils.isEmpty(specialprom.getFailedPath()) ? StringUtils.EMPTY : specialprom.getFailedPath());
            this.requestBuildDTO.setHistPath(StringUtils.isEmpty(specialprom.getHistPath()) ? StringUtils.EMPTY : specialprom.getHistPath());
            this.requestBuildDTO.setPath(StringUtils.isEmpty(specialprom.getPath()) ? StringUtils.EMPTY : specialprom.getPath());
            this.requestBuildDTO.setCurrentPath(StringUtils.isEmpty(specialprom.getPath()) ? StringUtils.EMPTY : specialprom.getPath());
            this.requestBuildDTO.setCategoryId(specialprom.getCategoryId());
            this.requestBuildDTO.setCategoryIdOld(specialprom.getCategoryId());
            this.requestBuildDTO.setSpecialOfferNameOld(StringUtils.isEmpty(specialprom.getName()) ? StringUtils.EMPTY : specialprom.getName());

            this.requestBuildDTO.setTempPathOld(StringUtils.isEmpty(specialprom.getTempPath()) ? StringUtils.EMPTY : specialprom.getTempPath());
            this.requestBuildDTO.setFailedPathOld(StringUtils.isEmpty(specialprom.getFailedPath()) ? StringUtils.EMPTY : specialprom.getFailedPath());
            this.requestBuildDTO.setHistPathOld(StringUtils.isEmpty(specialprom.getHistPath()) ? StringUtils.EMPTY : specialprom.getHistPath());
            this.requestBuildDTO.setCurrentPathOld(StringUtils.isEmpty(specialprom.getPath()) ? StringUtils.EMPTY : specialprom.getPath());

            this.disableOSName = false;
            this.disableOSCat = false;
        } else {
            this.requestBuildDTO = new RequestBuildDTO(requestBuildDTO.getObject(), requestBuildDTO.getAction());
            this.displayDetailSO = false;
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void onChangeObject(SelectEvent event) {
        this.displayBtnNext = true;
        displayDetailBL = false;
        String objSelect = (String) event.getObject();
        this.actions = requestService.generateActions(objSelect);
        if (StringUtils.isEmpty(objSelect)) {

            this.requestBuildDTO = new RequestBuildDTO(null, null);
        } else {
            this.requestBuildDTO = new RequestBuildDTO(objSelect, null);
        }
        serviceId = findServiceId();
    }

    public void onChangeAction(SelectEvent event) {
        displayDetailBL = false;
        String actionSelect = (String) event.getObject();
        this.displayBtnNext = !(ObjectUtils.isNotEmpty(actionSelect) && ObjectUtils.isNotEmpty(requestBuildDTO.getObject()));

        if (StringUtils.isEmpty(actionSelect)) {
            this.requestBuildDTO = new RequestBuildDTO(requestBuildDTO.getObject(), null);
        } else {
            this.requestBuildDTO = new RequestBuildDTO(requestBuildDTO.getObject(), actionSelect);
        }
        serviceId = findServiceId();
    }

    public int findServiceId() {
        String action = requestBuildDTO.getAction();
        String object = requestBuildDTO.getObject();
        if (action == null || action.isEmpty() || object == null || object.isEmpty()) {
            return -1;
        }
        switch (object) {
            //segment
            case Constants.ObjectType.SEGMENT: {
                switch (action) {
                    case Constants.ActionValue.UPLOAD:
                        return Constants.ServiceId.UPLOAD_SEGMENT;
                    case Constants.ActionValue.DELETE:
                        return Constants.ServiceId.DELETE_SEGMENT;
                    default:
                        return -1;
                }
            }
            //blacklist
            case Constants.ObjectType.BLACK_LIST: {
                switch (action) {
                    case Constants.ActionValue.UPLOAD:
                        return Constants.ServiceId.UPLOAD_BLACKLIST;
                    case Constants.ActionValue.DELETE:
                        return Constants.ServiceId.DELETE_BLACKLIST;
                    case Constants.ActionValue.UPDATE:
                        return Constants.ServiceId.UPDATE_BLACKLIST;
                    default:
                        return -1;
                }
            }
            //special offer
            case Constants.ObjectType.SPECIAL_OFFER: {
                switch (action) {
                    case Constants.ActionValue.UPLOAD:
                        return Constants.ServiceId.UPLOAD_SPECIAL_OFFER;
                    case Constants.ActionValue.DELETE:
                        return Constants.ServiceId.DELETE_SPECIAL_OFFER;
                    case Constants.ActionValue.UPDATE:
                        return Constants.ServiceId.UPDATE_SPECIAL_OFFER;
                    default:
                        return -1;
                }
            }
            default:
                return -1;
        }
    }

    public void onViewRequrst(Long idRequest) {
        if (Objects.nonNull(idRequest)) {
            this.request = requestService.findRequestById(idRequest);
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void onDeleteRequrst(RequestDTO requestDTO) {
        if (Objects.nonNull(requestDTO)) {
            int errorCode = requestDTO.getErrorCode();
            switch (errorCode) {
                case Constants.ErrorCode.ACCEPT:
                case Constants.ErrorCode.RUNNING: {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("error.request.not.allowed"));
                    return;
                }
                case Constants.ErrorCode.INIT: {
                    if (System.currentTimeMillis() - requestDTO.getRequestTime().getTime() < UtilsServiceImpl.ALLOW_TIME_REQUEST) {
                        errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("error.request.not.allowed"));
                        return;
                    }
                }
            }
            try {
                requestService.deleteRequest(requestDTO);
                this.requests.remove(requestDTO);
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
            } catch (Exception ex) {
                Logger.getLogger(CreationRequestController.class.getName()).log(Level.SEVERE, null, ex);
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.fail"));
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void initTreeNodeBacklist() {
        List<Category> catLst = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_BLACLIST_TYPE, false);
        List<Long> integers = new ArrayList<>();
        if (!catLst.isEmpty()) {
            catLst.forEach(item -> integers.add(item.getCategoryId()));
        }
        List<BlackList> backlistLst = blacklistService.findBacklistByCategory(integers);
        List<Object> objectLst = new ArrayList<>(backlistLst);
        this.rootNodeBlacklist = treeService.createTreeCategoryAndComponentObj(catLst, objectLst);
        treeService.expandAll(rootNodeBlacklist);
    }

    public void initTreeNodeSpecialOffer() {
        List<Category> catLst = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_SPECIALPROM_TYPE, false);
        List<Long> integers = new ArrayList<>();
        if (!catLst.isEmpty()) {
            catLst.forEach(item -> integers.add(item.getCategoryId()));
        }
        List<SpecialProm> specialproms = specialPromService.findSpecialpromByCategory(integers);
        List<Object> objectLst = new ArrayList<>(specialproms);
        this.rootNodeSpecialOffer = treeService.createTreeCategoryAndComponentObj(catLst, objectLst);
        treeService.expandAll(rootNodeSpecialOffer);
    }

    public void doNext() {
        if (Objects.nonNull(requestBuildDTO.getObject()) && Objects.nonNull(requestBuildDTO.getAction())) {
            if (Constants.ObjectType.SEGMENT.equals(requestBuildDTO.getObject())
                    && Constants.ActionValue.UPLOAD.equals(requestBuildDTO.getAction())) {
                this.titlePopup = utilsService.getTex("title.popup.create.request.segment.upload");
                this.chooseFileStep3 = 1;
                this.disableBtnValidate = true;
                this.disableBtnBuild = true;
                createFileDTOs = new ArrayList<>();
                requestBuildDTO.setPathFile(StringUtils.EMPTY);
                if (!doSegmentUpload()) {
                    return;
                }
            } else if (Constants.ObjectType.SEGMENT.equals(requestBuildDTO.getObject())
                    && Constants.ActionValue.DELETE.equals(requestBuildDTO.getAction())) {
                this.titlePopup = utilsService.getTex("title.popup.create.request.segment.delete");
                initTreeNodeSegment();
                displayDetailSeg = false;
                if (requestBuildDTO.getSegmentId() == null) {
                    this.displayBtnNext = true;
                }
                this.object_action = 2;
            } else if (Constants.ObjectType.BLACK_LIST.equals(requestBuildDTO.getObject())
                    && Constants.ActionValue.UPLOAD.equals(requestBuildDTO.getAction())) {
                this.titlePopup = utilsService.getTex("title.popup.create.request.blacklist.upload");
                this.chooseFileStep3 = 1;
                this.disableBtnValidate = true;
                this.disableBtnBuild = true;
                createFileDTOs = new ArrayList<>();
                requestBuildDTO.setPathFile(StringUtils.EMPTY);
                if (!doBlackListUpload()) {
                    return;
                }
            } else if (Constants.ObjectType.BLACK_LIST.equals(requestBuildDTO.getObject())
                    && Constants.ActionValue.DELETE.equals(requestBuildDTO.getAction())) {
                this.titlePopup = utilsService.getTex("title.popup.create.request.blacklist.delete");
                initTreeNodeBacklist();
                displayDetailBL = false;
                if (requestBuildDTO.getBlacklistId() == null) {
                    this.displayBtnNext = true;
                }
                this.object_action = 4;
            } else if (Constants.ObjectType.BLACK_LIST.equals(requestBuildDTO.getObject())
                    && Constants.ActionValue.UPDATE.equals(requestBuildDTO.getAction())) {
                this.titlePopup = utilsService.getTex("title.popup.create.request.blacklist.update");
                this.chooseFileStep3 = 1;
                this.disableBtnValidate = true;
                this.disableBtnBuild = true;
                createFileDTOs = new ArrayList<>();
                requestBuildDTO.setPathFile(StringUtils.EMPTY);
                if (!doBlackListUpdate()) {
                    return;
                }
                displayDetailBL = false;
                if (requestBuildDTO.getBlacklistId() == null) {
                    this.displayBtnNext = true;
                }
            } else if (Constants.ObjectType.SPECIAL_OFFER.equals(requestBuildDTO.getObject())
                    && Constants.ActionValue.UPLOAD.equals(requestBuildDTO.getAction())) {
                this.titlePopup = utilsService.getTex("title.popup.create.request.special.upload");
                this.chooseFileStep3 = 1;
                this.disableBtnValidate = true;
                this.disableBtnBuild = true;
                createFileDTOs = new ArrayList<>();
                requestBuildDTO.setPathFile(StringUtils.EMPTY);
                if (!doSpecialOfferUpload()) {
                    return;
                }
            } else if (Constants.ObjectType.SPECIAL_OFFER.equals(requestBuildDTO.getObject())
                    && Constants.ActionValue.DELETE.equals(requestBuildDTO.getAction())) {
                this.titlePopup = utilsService.getTex("title.popup.create.request.special.delete");
                initTreeNodeSpecialOffer();
                displayDetailSO = false;
                if (requestBuildDTO.getSpecialOfferId() == null) {
                    this.displayBtnNext = true;
                }
                this.displayDetailSO = !Objects.isNull(requestBuildDTO.getSpecialOfferId());
                this.object_action = 7;
            } else if (Constants.ObjectType.SPECIAL_OFFER.equals(requestBuildDTO.getObject())
                    && Constants.ActionValue.UPDATE.equals(requestBuildDTO.getAction())) {
                this.titlePopup = utilsService.getTex("title.popup.create.request.special.update");
                this.chooseFileStep3 = 1;
                this.disableBtnValidate = true;
                this.disableBtnBuild = true;
                createFileDTOs = new ArrayList<>();
                requestBuildDTO.setPathFile(StringUtils.EMPTY);

                displayDetailSO = false;

                if (requestBuildDTO.getSpecialOfferId() == null) {
                    this.displayBtnNext = true;
                }

                if (!doSpecialOfferUpdate()) {
                    return;
                }
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("object.or.action.empty"));
        }
        this.stepsIndex++;
    }

    private boolean doSegmentUpload() {
        this.object_action = 1;
        this.categorys = categoryService.getCategoryByType(Constants.SEGMENT, false);
        if (categorys == null || categorys.isEmpty()) {
            errorMsg(Constants.REMOTE_GROWL, NOT_FOUND_ROOT_CATEGORY);
            return false;
        }
        long currentCategory = getCategoryRoot(categorys);
        if (currentCategory >= 0) {
            this.requestBuildDTO.setCategoryId(currentCategory);
        } else {
            this.requestBuildDTO.setCategoryId(categorys.get(0).getCategoryId());
        }

        List<Segment> lstSegs = segmentService.getAll();
        if (!lstSegs.isEmpty()) {
            lstSegs.sort(Comparator.comparing(Segment::getSegmentName));
        }
        this.segments = lstSegs;

        if (stepsIndex == 1) {
            if (StringUtils.isNoneBlank(requestBuildDTO.getSegmentName())
                    && requestBuildDTO.getSegmentName().length() > 225) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("maxlength.segment.name"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getSegmentName())
                    && !segmentService.checkDuplicate(requestBuildDTO.getSegmentName(), null)) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.segment.name"));
                return false;
            } else if (requestBuildDTO.getDescription() != null
                    && requestBuildDTO.getDescription().trim().length() > 255) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.segment.description"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getPath())
                    && !segmentService.checkDuplicatePath(requestBuildDTO.getPath(), null)) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.segment.path"));
                return false;
            } else if (Objects.nonNull(requestBuildDTO.getEffectDate())
                    && DateUtils.compareDate(new Date(), requestBuildDTO.getEffectDate()) == 1) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("invalid.effectDate.request.segment.upload"));
                return false;
            } else if (Objects.nonNull(requestBuildDTO.getEffectDate())
                    && Objects.nonNull(requestBuildDTO.getExpireDate())
                    && DateUtils.compareDate(requestBuildDTO.getEffectDate(), requestBuildDTO.getExpireDate()) == 1) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("invalid.expireDate.request.segment.upload"));
                return false;
            }
//            if (isExistHdfs(requestBuildDTO.getPath(), requestBuildDTO.getHistPath(), requestBuildDTO.getTempPath(), requestBuildDTO.getFailedPath())) {
//                PrimeFaces primeFaces = PrimeFaces.current();
//                primeFaces.executeScript("PF('dlgConfirmHdfs').show()");
//                return false;
//            }
        }
        return true;
    }

    private boolean doBlackListUpload() {
        this.categorys = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_BLACLIST_TYPE, false);
        if (categorys == null || categorys.isEmpty()) {
            errorMsg(Constants.REMOTE_GROWL, NOT_FOUND_ROOT_CATEGORY);
            return false;
        }
        long currentCategory = getCategoryRoot(categorys);
        if (currentCategory >= 0) {
            this.requestBuildDTO.setCategoryId(currentCategory);
        } else {
            this.requestBuildDTO.setCategoryId(categorys.get(0).getCategoryId());
        }
        this.object_action = 3;
        if (stepsIndex == 1) {
            if (StringUtils.isNoneBlank(requestBuildDTO.getBlacklistName())
                    && requestBuildDTO.getBlacklistName().length() > 255) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("maxlength.blacklist.name"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getBlacklistName())
                    && !blacklistService.checkDuplicate(requestBuildDTO.getBlacklistName(), null)) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.blacklist.name"));
                return false;
            } else if (requestBuildDTO.getDescription() != null
                    && requestBuildDTO.getDescription().trim().length() > 255) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.blacklist.description"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getPath())
                    && !blacklistService.checkDuplicatePath(requestBuildDTO.getPath(), null)) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.blacklist.Path"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getTempPath())
                    && !blacklistService.checkDuplicatePath(requestBuildDTO.getTempPath(), null)) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.blacklist.TempPath"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getHistPath())
                    && !blacklistService.checkDuplicatePath(requestBuildDTO.getHistPath(), null)) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.blacklist.HistPath"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getFailedPath())
                    && !blacklistService.checkDuplicatePath(requestBuildDTO.getFailedPath(), null)) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.blacklist.FailedPath"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getCurrentPath())
                    && !blacklistService.checkDuplicatePath(requestBuildDTO.getCurrentPath(), null)) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.blacklist.CurrentPath"));
                return false;
            }
//            if (isExistHdfs(requestBuildDTO.getPath(), requestBuildDTO.getCurrentPath(), requestBuildDTO.getHistPath(), requestBuildDTO.getTempPath(), requestBuildDTO.getFailedPath())) {
//                PrimeFaces primeFaces = PrimeFaces.current();
//                primeFaces.executeScript("PF('dlgConfirmHdfs').show()");
//                return false;
//            }
        }
        return true;
    }

    private boolean doBlackListUpdate() {
        this.categorys = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_BLACLIST_TYPE, false);
        if (categorys == null || categorys.isEmpty()) {
            errorMsg(Constants.REMOTE_GROWL, NOT_FOUND_ROOT_CATEGORY);
            return false;
        }
        long currentCategory = getCategoryRoot(categorys);
        if (currentCategory >= 0) {
            this.requestBuildDTO.setCategoryId(currentCategory);
        } else {
            this.requestBuildDTO.setCategoryId(categorys.get(0).getCategoryId());
        }
        initTreeNodeBacklist();
        this.object_action = 5;
        if (stepsIndex == 1) {
            if (StringUtils.isNoneBlank(requestBuildDTO.getBlacklistName())
                    && requestBuildDTO.getBlacklistName().length() > 255) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("maxlength.blacklist.name"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getBlacklistName())
                    && !blacklistService.checkDuplicate(requestBuildDTO.getBlacklistName(), requestBuildDTO.getBlacklistId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.blacklist.name"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getTempPath())
                    && !blacklistService.checkDuplicatePath(requestBuildDTO.getTempPath(), requestBuildDTO.getBlacklistId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.blacklist.TempPath"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getHistPath())
                    && !blacklistService.checkDuplicatePath(requestBuildDTO.getHistPath(), requestBuildDTO.getBlacklistId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.blacklist.HistPath"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getFailedPath())
                    && !blacklistService.checkDuplicatePath(requestBuildDTO.getFailedPath(), requestBuildDTO.getBlacklistId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.blacklist.FailedPath"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getCurrentPath())
                    && !blacklistService.checkDuplicatePath(requestBuildDTO.getCurrentPath(), requestBuildDTO.getBlacklistId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.blacklist.CurrentPath"));
                return false;
            }
//            if (isExistHdfs(requestBuildDTO.getPath(), requestBuildDTO.getCurrentPath(), requestBuildDTO.getHistPath(), requestBuildDTO.getTempPath(), requestBuildDTO.getFailedPath())) {
//                PrimeFaces primeFaces = PrimeFaces.current();
//                primeFaces.executeScript("PF('dlgConfirmHdfs').show()");
//                return false;
//            }
        }

        return true;
    }

    private boolean doSpecialOfferUpload() {
        this.categorys = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_SPECIALPROM_TYPE, false);
        if (categorys == null || categorys.isEmpty()) {
            errorMsg(Constants.REMOTE_GROWL, NOT_FOUND_ROOT_CATEGORY);
            return false;
        }
        long currentCategory = getCategoryRoot(categorys);
        if (currentCategory >= 0) {
            this.requestBuildDTO.setCategoryId(currentCategory);
        } else {
            this.requestBuildDTO.setCategoryId(categorys.get(0).getCategoryId());
        }
        this.object_action = 6;

        if (stepsIndex == 1) {
            if (StringUtils.isNoneBlank(requestBuildDTO.getSpecialOfferName())
                    && requestBuildDTO.getSpecialOfferName().length() > 225) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("maxlength.special.name"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getSpecialOfferName())
                    && !specialPromService.checkDuplicate(requestBuildDTO.getSpecialOfferName(), null)) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.special.name"));
                return false;
            } else if (requestBuildDTO.getDescription() != null
                    && requestBuildDTO.getDescription().trim().length() > 225) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.special.description"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getTempPath())
                    && !specialPromService.checkDuplicatePath(requestBuildDTO.getTempPath(), null)) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.special.Path"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getHistPath())
                    && !specialPromService.checkDuplicatePath(requestBuildDTO.getHistPath(), null)) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.special.HistPath"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getFailedPath())
                    && !specialPromService.checkDuplicatePath(requestBuildDTO.getFailedPath(), null)) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.special.FailedPath"));
                return false;
            }
//            if (isExistHdfs(requestBuildDTO.getPath(), requestBuildDTO.getCurrentPath(), requestBuildDTO.getHistPath(), requestBuildDTO.getTempPath(), requestBuildDTO.getFailedPath())) {
//                PrimeFaces primeFaces = PrimeFaces.current();
//                primeFaces.executeScript("PF('dlgConfirmHdfs').show()");
//                return false;
//            }
        }
        return true;
    }

    private boolean doSpecialOfferUpdate() {
        this.displayDetailSO = !Objects.isNull(requestBuildDTO.getSpecialOfferId());
        this.categorys = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_SPECIALPROM_TYPE, false);
        if (categorys == null || categorys.isEmpty()) {
            errorMsg(Constants.REMOTE_GROWL, NOT_FOUND_ROOT_CATEGORY);
            return false;
        }
        long currentCategory = getCategoryRoot(categorys);
        if (currentCategory >= 0) {
            this.requestBuildDTO.setCategoryId(currentCategory);
        } else {
            this.requestBuildDTO.setCategoryId(categorys.get(0).getCategoryId());
        }
        initTreeNodeSpecialOffer();
        this.object_action = 8;

        if (stepsIndex == 1) {
            if (StringUtils.isNoneBlank(requestBuildDTO.getSpecialOfferName())
                    && requestBuildDTO.getSpecialOfferName().length() > 225) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("maxlength.special.name"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getSpecialOfferName())
                    && !specialPromService.checkDuplicate(requestBuildDTO.getSpecialOfferName(), requestBuildDTO.getSpecialOfferId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.special.name"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getTempPath())
                    && !specialPromService.checkDuplicatePath(requestBuildDTO.getTempPath(), requestBuildDTO.getSpecialOfferId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.special.Path"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getHistPath())
                    && !specialPromService.checkDuplicatePath(requestBuildDTO.getHistPath(), requestBuildDTO.getSpecialOfferId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.special.HistPath"));
                return false;
            } else if (StringUtils.isNoneBlank(requestBuildDTO.getFailedPath())
                    && !specialPromService.checkDuplicatePath(requestBuildDTO.getFailedPath(), requestBuildDTO.getSpecialOfferId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.special.FailedPath"));
                return false;
            }
//            if (isExistHdfs(requestBuildDTO.getHistPath(), requestBuildDTO.getTempPath(), requestBuildDTO.getFailedPath())) {
//                PrimeFaces primeFaces = PrimeFaces.current();
//                primeFaces.executeScript("PF('dlgConfirmHdfs').show()");
//                return false;
//            }
        }
        return true;
    }

    public void doReplaceHdfs() {
        this.stepsIndex++;
    }

    private long getCategoryRoot(List<Category> lstCategory) {
        if (!lstCategory.isEmpty()) {
            for (Category category : lstCategory) {
                if (Objects.isNull(category.getParentId())) {
                    return category.getCategoryId();
                }
            }
        }
        return -1;
    }

    public void doPrevious() {
        this.stepsIndex--;
        if (stepsIndex == 0) {
            displayBtnNext = false;
            this.titlePopup = utilsService.getTex("title.popup.create.request");
        }
    }

    public void handleFileUpload(FileUploadEvent event) {
        if (event != null && event.getFile() != null) {
            UploadedFile uploadedFile = event.getFile();
            String filePath = UtilsServiceImpl.getProperty(Constants.SpringProperty.UPLOAD_PATH);
            if (filePath.isEmpty()) {
                return;
            }
            filePath += DateUtils.dateToYYYYMMDDHHMMSSStr(Calendar.getInstance().getTime()) + "_" + uploadedFile.getFileName();
            String contenType = uploadedFile.getContentType();
            byte[] contents = uploadedFile.getContents();
            try {
                requestBuildDTO.setPathFile(filePath);
                requestBuildDTO.setFileUpload(true);
                disableBtnValidate = false;
                disableBtnBuild = true;
                int saveFile = requestService.saveFileToDir(filePath, contenType, contents);
                if (saveFile == 1) {
                    successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
                    if (serviceId == Constants.ServiceId.UPLOAD_SEGMENT) {
                        long countLine = RequestServiceImpl.getLineFile(new File(filePath));
                        requestBuildDTO.setSegmentSize(countLine);
                    }
                } else {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.fail"));
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.fail"));
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.fail"));
        }
    }

    public void doSend() {
        try {
            switch (serviceId) {
                case Constants.ServiceId.UPLOAD_SEGMENT:
                    requestBuildDTO.setCategoryId(categoryId);
                    requestService.uploadSegment(requestBuildDTO);
                    break;
                case Constants.ServiceId.DELETE_SEGMENT:
                    requestService.deleteSegment(requestBuildDTO);
                    break;
                case Constants.ServiceId.UPLOAD_BLACKLIST:
                    requestBuildDTO.setCategoryId(categoryId);
                    requestService.uploadBlacklist(requestBuildDTO);
                    break;
                case Constants.ServiceId.DELETE_BLACKLIST:
                    requestService.deleteBlacklist(requestBuildDTO);
                    break;
                case Constants.ServiceId.UPDATE_BLACKLIST:
                    requestBuildDTO.setCategoryId(categoryId);
                    requestService.updateBlacklist(requestBuildDTO);
                    break;
                case Constants.ServiceId.UPLOAD_SPECIAL_OFFER:
                    requestBuildDTO.setCategoryId(categoryId);
                    requestService.uploadSpecialOffer(requestBuildDTO);
                    break;
                case Constants.ServiceId.DELETE_SPECIAL_OFFER:
                    requestService.deleteSpecialOffer(requestBuildDTO);
                    break;
                case Constants.ServiceId.UPDATE_SPECIAL_OFFER:
                    requestBuildDTO.setCategoryId(categoryId);
                    requestService.updateSpecialOffer(requestBuildDTO);
                    break;
            }

            doReload();
            successMsg(Constants.REMOTE_GROWL, SEND_SUCCESS);
        } catch (Exception e) {
            errorMsg(Constants.REMOTE_GROWL, SEND_ERROR);
        }
    }

//    public void doSend() {
//        RequestFuture future = null;
//        switch (serviceId) {
//            case Constants.ServiceId.UPLOAD_SEGMENT:
//                future = sendSegmentUpload();
//                break;
//            case Constants.ServiceId.DELETE_SEGMENT:
//                future = sendSegmentDelete();
//                break;
//            case Constants.ServiceId.UPLOAD_BLACKLIST:
//                future = sendBlacklistUpload();
//                break;
//            case Constants.ServiceId.DELETE_BLACKLIST:
//                future = sendBlacklistDelete();
//                break;
//            case Constants.ServiceId.UPDATE_BLACKLIST:
//                future = sendBlacklistUpdate();
//                break;
//            case Constants.ServiceId.UPLOAD_SPECIAL_OFFER:
//                future = sendSpecialOfferUpload();
//                break;
//            case Constants.ServiceId.DELETE_SPECIAL_OFFER:
//                future = sendSpecialOfferDelete();
//                break;
//            case Constants.ServiceId.UPDATE_SPECIAL_OFFER:
//                future = sendSpecialOfferUpdate();
//                break;
//        }
//        if (future != null) {
//            if (future.isSent()) {
//                successMsg(Constants.REMOTE_GROWL, SUCCESS);
//            } else {
//                switch (serviceId) {
//                    case Constants.ServiceId.UPLOAD_SEGMENT:
//                        segmentService.deleteById(requestBuildDTO.getSegmentId());
//                        break;
//                    case Constants.ServiceId.UPLOAD_BLACKLIST:
//                        blacklistService.deleteById(requestBuildDTO.getBlacklistId());
//                        break;
//                    case Constants.ServiceId.UPLOAD_SPECIAL_OFFER:
//                        specialPromService.deleteById(requestBuildDTO.getSpecialOfferId());
//                        break;
//                    default:
//                }
//                String tranactionId = future.getRequestTransaction();
//                if (tranactionId != null && !tranactionId.isEmpty()) {
//                    requestService.deleteRequestByTransaction(tranactionId);
//                }
//                errorMsg(Constants.REMOTE_GROWL, SEND_TIMEOUT);
//            }
//        } else {
//            errorMsg(Constants.REMOTE_GROWL, SEND_ERROR);
//        }
//        String uploadFile = requestBuildDTO.getPathFile();
//        if (uploadFile != null && !uploadFile.isEmpty()) {
//            RequestServiceImpl.removeFile(uploadFile);
//        }
//    }
//    public boolean isExistHdfs(String... paths) {
//        boolean isExistHdfs = false;
//        try {
//            for (String path : paths) {
//                isExistHdfs |= HDFSProcessCommon.getInstance().isExist(path);
//            }
//        } catch (Exception e) {
//
//        }
//        return isExistHdfs;
//    }
//    public RequestFuture sendSegmentDelete() {
//        RequestBuilder builder = RequestBuilder.newBuilder();
//        SegmentBuilder segmentBuilder = new SegmentBuilder();
//        segmentBuilder.setSegmentId(requestBuildDTO.getSegmentId())
//                .setSegmentName(requestBuildDTO.getSegmentName())
//                .setPath(requestBuildDTO.getPath())
//                .setFailedPath(requestBuildDTO.getFailedPath())
//                .setHistPath(requestBuildDTO.getHistPath());
//        builder.setRequestType(Constant.RequestType.REQUEST_SERVICE)
//                .setServiceId(Constant.ServiceId.DELETE_SEGMENT)
//                .setObjectBuilder(segmentBuilder);
//        RequestMessage request = builder.build();
//        RequestFuture future = RequestProducer.getInstance().sendRequestFuture(request, false, Constant.Factor.WEB_FACTOR);
//        return future;
//    }
//
//    public RequestFuture sendSegmentUpload() {
//        Segment segment = new Segment();
//        segment.setSegmentId(null);
//        segment.setSegmentName(requestBuildDTO.getSegmentName());
//        segment.setEffDate(requestBuildDTO.getEffectDate());
//        segment.setExpDate(requestBuildDTO.getExpireDate());
//        segment.setPath(requestBuildDTO.getPath());
//        segment.setParentId(Long.valueOf(requestBuildDTO.getSegmentParent()));
//        segment.setTempPath(requestBuildDTO.getTempPath());
//        segment.setHistPath(requestBuildDTO.getHistPath());
//        segment.setFailedPath(requestBuildDTO.getFailedPath());
//        segment.setIsActive(0L);
//        segment.setControlPercentage(0L);
//        segment.setSegmentSize(requestBuildDTO.getSegmentSize());
//        segment.setCategoryId(requestBuildDTO.getCategoryId());
//        long segmentId = segmentService.onSaveSegment(segment);
//        if (segmentId >= 0) {
//            requestBuildDTO.setSegmentId(segmentId);
//            RequestBuilder builder = RequestBuilder.newBuilder();
//            SegmentBuilder segmentBuilder = new SegmentBuilder();
//            segmentBuilder.setSegmentId(segmentId)
//                    .setSegmentName(segment.getSegmentName())
//                    .setEffDate(segment.getEffDate().getTime())
//                    .setExpDate(segment.getExpDate().getTime())
//                    .setIsActive(false)
//                    .setParentId(segment.getParentId())
//                    .setPath(segment.getPath())
//                    .setTempPath(segment.getTempPath())
//                    .setFailedPath(segment.getFailedPath())
//                    .setHistPath(segment.getHistPath())
//                    .setSegmentSize(segment.getSegmentSize());
//            builder.setRequestType(Constant.RequestType.REQUEST_SERVICE)
//                    .setServiceId(Constant.ServiceId.UPLOAD_SEGMENT)
//                    .setObjectBuilder(segmentBuilder);
//            RequestMessage request = builder.build();
//            RequestFuture future = RequestProducer.getInstance().sendRequestFuture(request, false, Constant.Factor.WEB_FACTOR, requestBuildDTO.getPathFile(), segment.getTempPath());
//            return future;
//        }
//        return null;
//    }
//
//    public RequestFuture sendBlacklistUpload() {
//        BlackList blacklist = new BlackList();
//        blacklist.setBlacklistId(null);
//        blacklist.setName(requestBuildDTO.getBlacklistName());
//        blacklist.setPath(requestBuildDTO.getPath());
//        blacklist.setTempPath(requestBuildDTO.getTempPath());
//        blacklist.setHistPath(requestBuildDTO.getHistPath());
//        blacklist.setFailedPath(requestBuildDTO.getFailedPath());
//        blacklist.setCategoryId(requestBuildDTO.getCategoryId());
//        long blacklistId = blacklistService.onSaveBlacklist(blacklist);
//        if (blacklistId >= 0) {
//            requestBuildDTO.setBlacklistId(blacklistId);
//            RequestBuilder builder = RequestBuilder.newBuilder();
//            BlacklistBuilder uploadBuilder = new BlacklistBuilder();
//            uploadBuilder.setBlacklistId(blacklistId)
//                    .setBlacklistName(blacklist.getName())
//                    .setPath(blacklist.getPath())
//                    .setTempPath(blacklist.getTempPath())
//                    .setFailedPath(blacklist.getFailedPath())
//                    .setHistPath(blacklist.getHistPath());
//            builder.setRequestType(Constant.RequestType.REQUEST_SERVICE)
//                    .setServiceId(Constant.ServiceId.UPLOAD_BLACKLIST)
//                    .setObjectBuilder(uploadBuilder);
//            RequestFuture future = RequestProducer.getInstance().sendRequestFuture(builder.build(), false, Constant.Factor.WEB_FACTOR, requestBuildDTO.getPathFile(), blacklist.getTempPath());
//            return future;
//        }
//        return null;
//    }
//
//    public RequestFuture sendBlacklistDelete() {
//        RequestBuilder builder = RequestBuilder.newBuilder();
//        BlacklistBuilder deleteBuilder = new BlacklistBuilder();
//        deleteBuilder.setBlacklistId(requestBuildDTO.getBlacklistId())
//                .setBlacklistName(requestBuildDTO.getBlacklistName())
//                .setHistPath(requestBuildDTO.getHistPath())
//                .setPath(requestBuildDTO.getPath())
//                .setFailedPath(requestBuildDTO.getFailedPath());
//        builder.setRequestType(Constant.RequestType.REQUEST_SERVICE)
//                .setServiceId(Constant.ServiceId.DELETE_BLACKLIST)
//                .setObjectBuilder(deleteBuilder);
//        RequestFuture future = RequestProducer.getInstance().sendRequestFuture(builder.build(), false, Constant.Factor.WEB_FACTOR);
//        return future;
//    }
//
//    public RequestFuture sendBlacklistUpdate() {
//        BlackList blacklist = new BlackList();
//        blacklist.setBlacklistId(requestBuildDTO.getBlacklistId());
//        blacklist.setName(requestBuildDTO.getBlacklistName());
//        blacklist.setPath(requestBuildDTO.getPath());
//        blacklist.setTempPath(requestBuildDTO.getTempPath());
//        blacklist.setHistPath(requestBuildDTO.getHistPath());
//        blacklist.setFailedPath(requestBuildDTO.getFailedPath());
//        long blacklistId = blacklistService.onSaveBlacklist(blacklist);
//        if (blacklistId >= 0) {
//            RequestBuilder builder = RequestBuilder.newBuilder();
//            BlacklistBuilder uploadBuilder = new BlacklistBuilder();
//            uploadBuilder.setBlacklistId(blacklistId)
//                    .setBlacklistName(blacklist.getName())
//                    .setPath(blacklist.getPath())
//                    .setTempPath(blacklist.getTempPath())
//                    .setFailedPath(blacklist.getFailedPath())
//                    .setHistPath(blacklist.getHistPath());
//            builder.setRequestType(Constant.RequestType.REQUEST_SERVICE)
//                    .setServiceId(Constant.ServiceId.UPDATE_BLACKLIST)
//                    .setObjectBuilder(uploadBuilder);
//            RequestFuture future = RequestProducer.getInstance().sendRequestFuture(builder.build(), false, Constant.Factor.WEB_FACTOR, requestBuildDTO.getPathFile(), blacklist.getTempPath());
//            return future;
//        }
//        return null;
//    }
//
//    public RequestFuture sendSpecialOfferUpload() {
//        SpecialProm specialProm = new SpecialProm();
//        specialProm.setSpecialPromId(null);
//        specialProm.setName(requestBuildDTO.getSpecialOfferName());
//        specialProm.setPath(requestBuildDTO.getPath());
//        specialProm.setTempPath(requestBuildDTO.getTempPath());
//        specialProm.setHistPath(requestBuildDTO.getHistPath());
//        specialProm.setFailedPath(requestBuildDTO.getFailedPath());
//        specialProm.setCategoryId(requestBuildDTO.getCategoryId() == null ? 0L : requestBuildDTO.getCategoryId());
//        long specialPromId = specialPromService.onSaveSpecialProm(specialProm);
//        if (specialPromId >= 0) {
//            requestBuildDTO.setSpecialOfferId(specialPromId);
//            RequestBuilder builder = RequestBuilder.newBuilder();
//            SpecialPromBuilder uploadBuilder = new SpecialPromBuilder();
//            uploadBuilder.setSpecialPromId(specialPromId)
//                    .setSpecialPromName(specialProm.getName())
//                    .setTempPath(specialProm.getTempPath())
//                    .setPath(specialProm.getPath())
//                    .setFailedPath(specialProm.getFailedPath())
//                    .setHistPath(specialProm.getHistPath());
//            builder.setRequestType(Constant.RequestType.REQUEST_SERVICE)
//                    .setServiceId(Constant.ServiceId.UPLOAD_SPECIAL_PROM)
//                    .setObjectBuilder(uploadBuilder);
//            RequestFuture future = RequestProducer.getInstance().sendRequestFuture(builder.build(), false, Constant.Factor.WEB_FACTOR, requestBuildDTO.getPathFile(), specialProm.getTempPath());
//            return future;
//        }
//        return null;
//    }
//
//    public RequestFuture sendSpecialOfferDelete() {
//        RequestBuilder request = RequestBuilder.newBuilder();
//        SpecialPromBuilder deleteBuilder = new SpecialPromBuilder();
//        deleteBuilder.setSpecialPromId(requestBuildDTO.getSpecialOfferId())
//                .setSpecialPromName(requestBuildDTO.getSpecialOfferName())
//                .setFailedPath(requestBuildDTO.getFailedPath())
//                .setPath(requestBuildDTO.getPath())
//                .setHistPath(requestBuildDTO.getHistPath());
//        request.setRequestType(Constant.RequestType.REQUEST_SERVICE)
//                .setServiceId(Constant.ServiceId.DELETE_SPECIAL_PROM)
//                .setObjectBuilder(deleteBuilder);
//        RequestFuture future = RequestProducer.getInstance().sendRequestFuture(request.build(), false, Constant.Factor.WEB_FACTOR);
//        return future;
//    }
//
//    public RequestFuture sendSpecialOfferUpdate() {
//        SpecialProm specialProm = new SpecialProm();
//        specialProm.setSpecialPromId(requestBuildDTO.getSpecialOfferId());
//        specialProm.setName(requestBuildDTO.getSpecialOfferName());
//        specialProm.setPath(requestBuildDTO.getPath());
//        specialProm.setTempPath(requestBuildDTO.getTempPath());
//        specialProm.setHistPath(requestBuildDTO.getHistPath());
//        specialProm.setFailedPath(requestBuildDTO.getFailedPath());
//        long specialPromId = specialPromService.onSaveSpecialProm(specialProm);
//        if (specialPromId >= 0) {
//            RequestBuilder builder = RequestBuilder.newBuilder();
//            SpecialPromBuilder uploadBuilder = new SpecialPromBuilder();
//            uploadBuilder.setSpecialPromId(specialPromId)
//                    .setSpecialPromName(specialProm.getName())
//                    .setPath(specialProm.getPath())
//                    .setTempPath(specialProm.getTempPath())
//                    .setFailedPath(specialProm.getFailedPath())
//                    .setHistPath(specialProm.getHistPath());
//            builder.setRequestType(Constant.RequestType.REQUEST_SERVICE)
//                    .setServiceId(Constant.ServiceId.UPDATE_SPECIAL_PROM)
//                    .setObjectBuilder(uploadBuilder);
//            RequestFuture future = RequestProducer.getInstance().sendRequestFuture(builder.build(), false, Constant.Factor.WEB_FACTOR, requestBuildDTO.getPathFile(), specialProm.getTempPath());
//            return future;
//        }
//        return null;
//    }
//
//    public void doValidateStep3() {
//        ValidateResponse response = WebAPIUtils.validate(serviceId, requestBuildDTO.getPathFile());
//        switch (response.getErrorCode()) {
//            case WebAPIUtils.ERROR:
//                errorMsg(Constants.REMOTE_GROWL, response.getErrorDescription() + VALIDATE_EXCEPTION);
//                break;
//            case WebAPIUtils.DUPLICATE:
//                errorMsg(Constants.REMOTE_GROWL, response.getErrorDescription() + DUPLICATE_EXCEPTION);
//                break;
//            case WebAPIUtils.NONE:
//                disableBtnValidate = true;
//                disableBtnBuild = false;
//                successMsg(Constants.REMOTE_GROWL, response.getErrorDescription() + SUCCESS);
//                break;
//            case WebAPIUtils.NOT_MATCH:
//                errorMsg(Constants.REMOTE_GROWL, response.getErrorDescription() + VALIDATE_ERROR);
//                break;
//            case WebAPIUtils.EMPTY:
//                errorMsg(Constants.REMOTE_GROWL, response.getErrorDescription() + FILE_EMPTY);
//                break;
//        }
//    }
    public void doValidateStep3() {
        this.disableBtnValidate = true;
        this.disableBtnBuild = false;
    }

    public boolean validateFileUpload() {
        if (Constants.ObjectType.SEGMENT.equals(requestBuildDTO.getObject())
                && Constants.ActionValue.UPLOAD.equals(requestBuildDTO.getAction())
                && !requestBuildDTO.isFileUpload()) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("creation.request.fileupload.empty"));
            return false;
        } else if (Constants.ObjectType.BLACK_LIST.equals(requestBuildDTO.getObject())
                && Constants.ActionValue.UPLOAD.equals(requestBuildDTO.getAction())
                && !requestBuildDTO.isFileUpload()) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("creation.request.fileupload.empty"));
            return false;
        } else if (Constants.ObjectType.BLACK_LIST.equals(requestBuildDTO.getObject())
                && Constants.ActionValue.UPDATE.equals(requestBuildDTO.getAction())
                && !requestBuildDTO.isFileUpload()) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("creation.request.fileupload.empty"));
            return false;
        } else if (Constants.ObjectType.SPECIAL_OFFER.equals(requestBuildDTO.getObject())
                && Constants.ActionValue.UPLOAD.equals(requestBuildDTO.getAction())
                && !requestBuildDTO.isFileUpload()) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("creation.request.fileupload.empty"));
            return false;
        } else if (Constants.ObjectType.SPECIAL_OFFER.equals(requestBuildDTO.getObject())
                && Constants.ActionValue.UPDATE.equals(requestBuildDTO.getAction())
                && !requestBuildDTO.isFileUpload()) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("creation.request.fileupload.empty"));
            return false;
        }
        return true;
    }

    public void onChangeChooseFile() {
        if (chooseFileStep3 == 2) {
            resetStatusButton();
            createFileDTOs = new ArrayList<>();
        } else {
            this.disableBtnValidate = true;
            this.disableBtnBuild = true;
        }
        requestBuildDTO.setPathFile(StringUtils.EMPTY);
    }

    public void onAddRowCreateFile() {
        CreateFileDTO cfdto = new CreateFileDTO();
        cfdto.setId(generateCreateFileId());
        cfdto.setOperator(Constants.CreateRequest.OPERATOR_ADD);
        cfdto.setMsisdn(StringUtils.EMPTY);
        createFileDTOs.add(cfdto);
        resetStatusButton();
    }

    private Long generateCreateFileId() {
        if (createFileDTOs != null && !createFileDTOs.isEmpty()) {
            return (createFileDTOs.stream()
                    .filter(item -> Objects.nonNull(item.getId()))
                    .max(Comparator.comparing(CreateFileDTO::getId))
                    .get()
                    .getId()) + 1;
        }
        return 0L;
    }

    public void onkeyupMsi() {
        resetStatusButton();
    }

    private void resetStatusButton() {
        this.disableBtnCreate = false;
        this.disableBtnValidate = true;
        this.disableBtnBuild = true;
        requestBuildDTO.setPathFile(StringUtils.EMPTY);
    }

    public void onAddRowSpeialOffer() {
        SpecialOfferDTO specialOfferDTO = new SpecialOfferDTO();
        specialOfferDTO.setOffer(StringUtils.EMPTY);
        specialOfferDTO.setPriority(StringUtils.EMPTY);
        specialOfferDTOs.add(specialOfferDTO);
    }

    public void onAddRowLstSo(CreateFileDTO cfdto) {
        Pattern pattern = Pattern.compile((String) utilsService.getObject("pattern.isdn.create.file", StringUtils.EMPTY));
        if (StringUtils.isBlank(cfdto.getMsisdn())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("empty.isdn.create.file"));
            return;
        } else if (!pattern.matcher(cfdto.getMsisdn()).matches()) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("invalid.isdn.create.file"));
            return;
        }

        this.createFileDTO = cfdto;
        specialOfferDTOs = new ArrayList<>();
        resetStatusButton();
    }

    public void onAddChangeOperatorSoUpdate(CreateFileDTO cfdto) {
        createFileDTOs.stream()
                .filter((item) -> (DataUtil.safeEqual(item.getId(), cfdto.getId()))).forEach((item) -> {
                    item.setLstSpeialOffer(StringUtils.EMPTY);
                });
        resetStatusButton();
    }

    public void onClickEditLstSo(CreateFileDTO cfdto) {
        this.createFileDTO = cfdto;
        specialOfferDTOs = requestService.genListSpeialOffer(cfdto.getLstSpeialOffer());
        resetStatusButton();
    }

    public void onApplySpeialOffer() {

        if (!validateApplySpeialOffer()) {
            return;
        }

        if (specialOfferDTOs != null && !specialOfferDTOs.isEmpty()) {
            createFileDTOs.stream()
                    .filter((cfdto) -> (DataUtil.safeEqual(cfdto.getId(), createFileDTO.getId())))
                    .forEach((cfdto) -> {
                        cfdto.setLstSpeialOffer(requestService.genListSpeialOffer(specialOfferDTOs));
                    });
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    private boolean validateApplySpeialOffer() {
        boolean rs = true;
        if (specialOfferDTOs != null && !specialOfferDTOs.isEmpty()) {
            Pattern pattern = Pattern.compile("\\d*");
            List<SpecialOfferDTO> lstValidate = new ArrayList<>();
            List<CreateFileDTO> lstCreateFile = createFileDTOs.stream()
                    .filter(item -> StringUtils.isNotBlank(item.getMsisdn())
                            && item.getMsisdn().equals(this.createFileDTO.getMsisdn())
                            && !DataUtil.safeEqual(item.getId(), createFileDTO.getId()))
                    .collect(Collectors.toList());
            if (lstCreateFile != null && !lstCreateFile.isEmpty()) {
                lstCreateFile.stream().forEach((cfdto) -> {
                    lstValidate.addAll(requestService.genListSpeialOffer(cfdto.getLstSpeialOffer()));
                });
            }
            for (SpecialOfferDTO item : specialOfferDTOs) {
                if (StringUtils.isBlank(item.getOffer())) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("empty.offer.create.file"));
                    rs = false;
                    break;
                } else if (!pattern.matcher(item.getOffer()).matches()) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("invalid.offer.create.file"));
                    rs = false;
                    break;
                } else if (StringUtils.isBlank(item.getPriority())) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("empty.priority.create.file"));
                    rs = false;
                    break;
                } else if (!pattern.matcher(item.getPriority()).matches()) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("invalid.priority.create.file"));
                    rs = false;
                    break;
                } else if (specialOfferDTOs.stream()
                        .filter(item1 -> item1.getOffer().equals(item.getOffer()))
                        .collect(Collectors.toList())
                        .size() > 1) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.offer.create.file"));
                    rs = false;
                    break;
                } else if (specialOfferDTOs.stream()
                        .filter(item1 -> item1.getPriority().equals(item.getPriority()))
                        .collect(Collectors.toList())
                        .size() > 1) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.priority.create.file"));
                    rs = false;
                    break;
                }

                if (!lstValidate.isEmpty()) {
                    if (lstValidate.stream()
                            .filter(item1 -> item1.getOffer().equals(item.getOffer()))
                            .collect(Collectors.toList())
                            .size() > 0) {
                        errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.offer.create.file"));
                        rs = false;
                        break;
                    } else if (lstValidate.stream()
                            .filter(item1 -> item1.getPriority().equals(item.getPriority()))
                            .collect(Collectors.toList())
                            .size() > 0) {
                        errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("duplicate.priority.create.file"));
                        rs = false;
                        break;
                    }
                }
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
            rs = false;
        }
        return rs;
    }

    public void onCreateFile() {
        if (!validateCreateFile()) {
            return;
        }
        String filePath = UtilsServiceImpl.getProperty(Constants.SpringProperty.UPLOAD_PATH);
        if (filePath.isEmpty()) {
            return;
        }
        filePath += DateUtils.dateToYYYYMMDDHHMMSSStr(Calendar.getInstance().getTime()) + "_";
        switch (object_action) {
            case 1:
                filePath += "SegmentUpload.txt";
                break;
            case 3:
                filePath += "BlacklistUpload.txt";
                break;
            case 5:
                filePath += "BlacklistUpdate.txt";
                break;
            case 6:
                filePath += "SpecialOfferUpload.txt";
                break;
            case 8:
                filePath += "SpecialOfferUpdate.txt";
                break;
            default:

                return;
        }
        try {
            this.requestBuildDTO.setFileUpload(true);
            this.disableBtnCreate = true;
            this.disableBtnValidate = false;
            long createFile = requestService.createFileToDir(filePath, createFileDTOs, requestBuildDTO.getObject());
            if (createFile == 1) {
                requestBuildDTO.setPathFile(filePath);
                if (object_action == 1 || object_action == 2) {
                    long countLine = RequestServiceImpl.getLineFile(new File(filePath));
                    requestBuildDTO.setSegmentSize(countLine);
                }
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
            } else {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.fail"));
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.fail"));
        }
    }

    public void onDeleteCreateFile(CreateFileDTO createFileDTO) {
        if (Objects.nonNull(createFileDTO)) {
            this.createFileDTOs.remove(createFileDTO);
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
            resetStatusButton();
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void onDeleteSpeialOffer(SpecialOfferDTO specialOfferDTO) {
        if (Objects.nonNull(specialOfferDTO)) {
            this.specialOfferDTOs.remove(specialOfferDTO);
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    private boolean validateCreateFile() {
        boolean rs = true;
        if (createFileDTOs != null && !createFileDTOs.isEmpty()) {
            for (CreateFileDTO item : createFileDTOs) {
                Pattern pattern = Pattern.compile((String) utilsService.getObject("pattern.isdn.create.file", StringUtils.EMPTY));
                if (StringUtils.isBlank(item.getMsisdn())) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("empty.isdn.create.file"));
                    rs = false;
                    break;
                } else if ((object_action == 6)
                        && StringUtils.isBlank(item.getLstSpeialOffer())) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("empty.lst.speial.offer.create.file"));
                    rs = false;
                    break;
                } else if (object_action == 8
                        && StringUtils.isNotBlank(item.getOperator())
                        && "ADD".equalsIgnoreCase(item.getOperator())
                        && StringUtils.isBlank(item.getLstSpeialOffer())) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("empty.lst.speial.offer.create.file"));
                    rs = false;
                    break;
                } else if (!pattern.matcher(item.getMsisdn()).matches()) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("invalid.isdn.create.file"));
                    rs = false;
                    break;
                }
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
            rs = false;
        }
        return rs;
    }

    public RequestStatusDTO getRequestStatusDTO() {
        return requestStatusDTO;
    }

    public void setRequestStatusDTO(RequestStatusDTO requestStatusDTO) {
        this.requestStatusDTO = requestStatusDTO;
    }

    public List<RequestDTO> getRequests() {
        return requests;
    }

    public void setRequests(List<RequestDTO> requests) {
        this.requests = requests;
    }

    public Request getSelectedRequest() {
        return selectedRequest;
    }

    public void setSelectedRequest(Request selectedRequest) {
        this.selectedRequest = selectedRequest;
    }

    public int getStepsIndex() {
        return stepsIndex;
    }

    public void setStepsIndex(int stepsIndex) {
        this.stepsIndex = stepsIndex;
    }

    public List<ComboboxDTO> getActions() {
        return actions;
    }

    public void setActions(List<ComboboxDTO> actions) {
        this.actions = actions;
    }

    public boolean isDisplayBtnNext() {
        return displayBtnNext;
    }

    public void setDisplayBtnNext(boolean displayBtnNext) {
        this.displayBtnNext = displayBtnNext;
    }

    public List<ComboboxDTO> getObjects() {
        return objects;
    }

    public void setObjects(List<ComboboxDTO> objects) {
        this.objects = objects;
    }

    public int getObject_action() {
        return object_action;
    }

    public void setObject_action(int object_action) {
        this.object_action = object_action;
    }

    public RequestDTO getRequest() {
        return request;
    }

    public void setRequest(RequestDTO request) {
        this.request = request;
    }

    public List<Category> getCategorys() {
        return categorys;
    }

    public void setCategorys(List<Category> categorys) {
        this.categorys = categorys;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }

    public TreeNode getRootNodeSegment() {
        return rootNodeSegment;
    }

    public void setRootNodeSegment(TreeNode rootNodeSegment) {
        this.rootNodeSegment = rootNodeSegment;
    }

    public TreeNode getSelectedNodeSegment() {
        return selectedNodeSegment;
    }

    public void setSelectedNodeSegment(TreeNode selectedNodeSegment) {
        this.selectedNodeSegment = selectedNodeSegment;
    }

    public TreeNode getRootNodeBlacklist() {
        return rootNodeBlacklist;
    }

    public void setRootNodeBlacklist(TreeNode rootNodeBlacklist) {
        this.rootNodeBlacklist = rootNodeBlacklist;
    }

    public TreeNode getSelectedNodeBlacklist() {
        return selectedNodeBlacklist;
    }

    public void setSelectedNodeBlacklist(TreeNode selectedNodeBlacklist) {
        this.selectedNodeBlacklist = selectedNodeBlacklist;
    }

    public TreeNode getRootNodeSpecialOffer() {
        return rootNodeSpecialOffer;
    }

    public void setRootNodeSpecialOffer(TreeNode rootNodeSpecialOffer) {
        this.rootNodeSpecialOffer = rootNodeSpecialOffer;
    }

    public TreeNode getSelectedNodeSpecialOffer() {
        return selectedNodeSpecialOffer;
    }

    public void setSelectedNodeSpecialOffer(TreeNode selectedNodeSpecialOffer) {
        this.selectedNodeSpecialOffer = selectedNodeSpecialOffer;
    }

    public boolean isDisplayDetailBL() {
        return displayDetailBL;
    }

    public void setDisplayDetailBL(boolean displayDetailBL) {
        this.displayDetailBL = displayDetailBL;
    }

    public RequestBuildDTO getRequestBuildDTO() {
        return requestBuildDTO;
    }

    public void setRequestBuildDTO(RequestBuildDTO requestBuildDTO) {
        this.requestBuildDTO = requestBuildDTO;
    }

    public boolean isDisableBLName() {
        return disableBLName;
    }

    public void setDisableBLName(boolean disableBLName) {
        this.disableBLName = disableBLName;
    }

    public boolean isDisableBLCat() {
        return disableBLCat;
    }

    public void setDisableBLCat(boolean disableBLCat) {
        this.disableBLCat = disableBLCat;
    }

    public boolean isDisplayDetailSO() {
        return displayDetailSO;
    }

    public void setDisplayDetailSO(boolean displayDetailSO) {
        this.displayDetailSO = displayDetailSO;
    }

    public boolean isDisableOSName() {
        return disableOSName;
    }

    public void setDisableOSName(boolean disableOSName) {
        this.disableOSName = disableOSName;
    }

    public boolean isDisableOSCat() {
        return disableOSCat;
    }

    public void setDisableOSCat(boolean disableOSCat) {
        this.disableOSCat = disableOSCat;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public int getChooseFileStep3() {
        return chooseFileStep3;
    }

    public void setChooseFileStep3(int chooseFileStep3) {
        this.chooseFileStep3 = chooseFileStep3;
    }

    public List<CreateFileDTO> getCreateFileDTOs() {
        return createFileDTOs;
    }

    public void setCreateFileDTOs(List<CreateFileDTO> createFileDTOs) {
        this.createFileDTOs = createFileDTOs;
    }

    public boolean isDisableBtnCreate() {
        return disableBtnCreate;
    }

    public void setDisableBtnCreate(boolean disableBtnCreate) {
        this.disableBtnCreate = disableBtnCreate;
    }

    public boolean isDisableBtnValidate() {
        return disableBtnValidate;
    }

    public void setDisableBtnValidate(boolean disableBtnValidate) {
        this.disableBtnValidate = disableBtnValidate;
    }

    public boolean isDisableBtnBuild() {
        return disableBtnBuild;
    }

    public void setDisableBtnBuild(boolean disableBtnBuild) {
        this.disableBtnBuild = disableBtnBuild;
    }

    public List<SpecialOfferDTO> getSpecialOfferDTOs() {
        return specialOfferDTOs;
    }

    public void setSpecialOfferDTOs(List<SpecialOfferDTO> specialOfferDTOs) {
        this.specialOfferDTOs = specialOfferDTOs;
    }

    public CreateFileDTO getCreateFileDTO() {
        return createFileDTO;
    }

    public void setCreateFileDTO(CreateFileDTO createFileDTO) {
        this.createFileDTO = createFileDTO;
    }

    public boolean isDisplayDetailSeg() {
        return displayDetailSeg;
    }

    public void setDisplayDetailSeg(boolean displayDetailSeg) {
        this.displayDetailSeg = displayDetailSeg;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getTitlePopup() {
        return titlePopup;
    }

    public void setTitlePopup(String titlePopup) {
        this.titlePopup = titlePopup;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
}
