package vn.viettel.campaign.controller;

import lombok.Getter;
import lombok.Setter;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DualListModel;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.CampaignOnlineService;
import vn.viettel.campaign.service.RuleServiceInterface;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.*;
import javax.faces.bean.ManagedProperty;
import org.primefaces.model.DefaultTreeNode;
import vn.viettel.campaign.validate.CheckOnEdit;

/**
 * @author truongbx
 * @date 9/15/2019
 */
@ManagedBean(name = "ruleOnlineController")
@ViewScoped
@Getter
@Setter
public class RuleOnlineController extends BaseCategoryController<Rule> implements Serializable {
    
    TreeNode parentNode;

    @Autowired
    private CampaignOnlineService campaignOnlineService;
    @Autowired
    private RuleServiceInterface ruleServiceImpl;
//	1:all , 2 specific
    private long applyAllCdrToRule = 2;
    private DualListModel<CdrService> cdrServicePickingList = new DualListModel<>();
    private List<CdrService> cdrServicesTable = new ArrayList<>();
    private List<ResultTable> resultTable = new ArrayList<>();
    private List<CdrService> cdrServicesAll = new ArrayList<>();
    private List<BaseCategory> resultTableAllData = new ArrayList<>();
    public TreeNode rootResultTable;
    private TreeNode[] selectedResultTable;
    private Map<Long, TreeNode> mapResultTableNode = new HashMap<>();

    private String editObject;
    @ManagedProperty(value = "#{resultTableController}")
    ResultTableController resultTableController;

    @ManagedProperty(value = "#{conditionTableController}")
    ConditionTableController conditionTableController;

    @ManagedProperty(value = "#{preProcessController}")
    PreProcessController preProcessController;

    @ManagedProperty(value = "#{preProcessNumberController}")
    PreProcessNumberController preProcessNumberController;

    @ManagedProperty(value = "#{preProcessExistElementController}")
    PreProcessExistElementController preProcessExistElementController;

    @ManagedProperty(value = "#{preProcessTimeController}")
    PreProcessTimeController preProcessTimeController;

    @ManagedProperty(value = "#{preProcessDateController}")
    PreProcessDateController preProcessDateController;

    @ManagedProperty(value = "#{preProcessNumberRangeController}")
    PreProcessNumberRangeController preProcessNumberRangeController;

    @ManagedProperty(value = "#{preProcessCompareNumberController}")
    PreProcessCompareNumberController preProcessCompareNumberController;

    @ManagedProperty(value = "#{preProcessZoneController}")
    PreProcessZoneController preProcessZoneController;

    @ManagedProperty(value = "#{preProcessSameElementController}")
    PreProcessSameElementController preProcessSameElementController;

    @Override
    public void init() {
        this.categoryType = Constants.CatagoryType.CATEGORY_RULE_ONLINE_TYPE;
        this.currentClass = Rule.class;
        cdrServicesAll = campaignOnlineService.getAllCdrService();
        cdrServicesTable = new ArrayList<>();
        resultTable = new ArrayList<>();
        applyAllCdrToRule = 2;
        cdrServicePickingList = new DualListModel<>(cdrServicesAll, cdrServicesTable);
    }

    public void initResultTableTree() {
        List<Category> categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_RESULT_TABLE_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.resultTableAllData = categoryService.getDataFromCatagoryId(longs, ResultTable.class.getSimpleName());
        rootResultTable = treeService.createTreeCategoryAndChild(categories, resultTableAllData);
        rootResultTable.setExpanded(true);
        createMapResulTableTreeNode(rootResultTable, mapResultTableNode);
    }

    @Override
    public void initCurrentValue() {
        currentValue = ruleServiceImpl.getNextSequense();
    }

    @Override
    public boolean onValidateObject() {
        if (!validInputField(this.currentValue.getRuleName(), "Rule name", true, true, true)) {
            return false;
        }
        if (!validInputField(this.currentValue.getDescription(), "Description", false, true, true)) {
            return false;
        }
        if (campaignOnlineService.checExisRuleName(this.currentValue.getRuleName(), this.currentValue.getRuleId())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Rule name");
            return false;
        }

        if (this.currentValue.getCategoryId() == 0) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.rule.category"));
            return false;
        }
        if (this.applyAllCdrToRule == 2 && cdrServicesTable.size() == 0) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Cdr service table");
            return false;
        }
        if (resultTable.size() == 0) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Result table");
            return false;
        }
        List<CdrService> cdrServices = new ArrayList<>();
        if (this.applyAllCdrToRule == 1) {
            this.currentValue.setCdrServiceId("all");
        } else {
            cdrServices = cdrServicesTable;
            String ids = "";
            for (CdrService cdrService : cdrServices) {
                ids += "," + cdrService.getCdrServiceId();
            }
            this.currentValue.setCdrServiceId(ids.replaceFirst(",", ""));
        }

        return true;
    }

    @Override
    public boolean doDelete() {
        return ruleServiceImpl.onDeleteRule(this.currentValue.getRuleId());
    }

    @Override
    public boolean onSaveObject() {
        DataUtil.trimObject(this.currentValue);
        return ruleServiceImpl.onSaveRule(this.currentValue, resultTable);
    }

    @Override
    public void doEdit() {
        cdrServicesAll = campaignOnlineService.getAllCdrService();

        resultTable = campaignOnlineService.getListResultByRuleId(this.currentValue.getRuleId());
        initDataForCDRServiceTable();
        if (this.currentValue.getCdrServiceId().equalsIgnoreCase("all")) {
            applyAllCdrToRule = 1;
        } else {
            applyAllCdrToRule = 2;
        }
        initResultTableTree();
    }

    @Override
    public boolean onUpdateObject() {
        DataUtil.trimObject(this.currentValue);
        return ruleServiceImpl.onUpdateRule(this.currentValue, resultTable);
    }

    @Override
    public void rollbackData() {
        Rule data = ruleServiceImpl.findById(this.currentValue.getRuleId());
        this.currentValue.asMap(data);
    }

    @Override
    public void doAdd() {
        cdrServicesAll = campaignOnlineService.getAllCdrService();
        cdrServicesTable = new ArrayList<>();
        resultTable = new ArrayList<>();
        applyAllCdrToRule = 2;
        initResultTableTree();
    }

    public void prepareDataToShowCdrServicePopup() {
        List<CdrService> cdrServicesAllDup = new ArrayList<>();
        cdrServicesAllDup.addAll(cdrServicesAll);
        List<CdrService> cdrServicesTableDup = new ArrayList<>();
        cdrServicesTableDup.addAll(cdrServicesTable);
        cdrServicePickingList = new DualListModel<>(cdrServicesAllDup, cdrServicesTableDup);

    }

    public void prepareDataToShowResultTablePopup() {
        initResultTableTree();
        createSelectedTreeNode();
    }

    public void onSaveCdrService() {
        cdrServicesAll = cdrServicePickingList.getSource();
        cdrServicesTable = cdrServicePickingList.getTarget();
    }

    public void removeResultTable(ResultTable o) {
        resultTable.remove(o);
        resultTableAllData.add(o);
    }

    public void removeCdrServiceTable(CdrService o) {
        cdrServicesTable.remove(o);
        cdrServicesAll.add(o);
    }

    public void moveResulTableUp(ResultTable o) {
        int i = resultTable.indexOf(o);
        if (i > 0) {
            resultTable.remove(o);
            resultTable.add(i - 1, o);
        }
    }

    public void moveResulTableDown(ResultTable o) {
        int i = resultTable.indexOf(o);
        if (i < resultTable.size() - 1) {
            resultTable.remove(o);
            resultTable.add(i + 1, o);
        }
    }
    
    public void prepareEditWhenClick() {
        CheckOnEdit.onEdit = "rule_online";
        prepareEdit();
        this.action = false;
    }

    public void prepareEditFromContextMenu() {
        CheckOnEdit.onEdit = "rule_online";
        prepareEdit();
        this.action = true;

    }

    public void prepareEdit() {
        this.parentNode = selectedNode.getParent();
        if ("category".equals(this.selectedNode.getType())) {
            prepareEditCategory();
            PrimeFaces.current().executeScript("PF('carDialog').show();");
        }
        if ("rule_online".equals(selectedNode.getType())) {
            this.editObject = "rule_online";
            selectedNode.getChildren().clear();
            doAddResultTableToRule(selectedNode);
            onEditObject();
            selectedNode.setExpanded(true);
        }

        if ("result".equals(selectedNode.getType())) {
            this.editObject = "result";
            resultTableController.setSelectedNode(selectedNode);
            resultTableController.prepareEditObject();
        }

        if ("condition".equals(selectedNode.getType())) {
            this.editObject = "condition";
            conditionTableController.setSelectedNode(selectedNode);
            conditionTableController.prepareEditObject();
        }

        if ("ppu".equals(selectedNode.getType())) {
//            ColumnCt columnCt = (ColumnCt) selectedNode.getData();
//            PreProcessUnit ppu = preprocessUnitServiceImpl.findById(columnCt.getPreProcessId());
//            Category cat = categoryService.getCategoryById(ppu.getCategoryId());
            PreProcessUnit ppu = (PreProcessUnit) selectedNode.getData();

            if (Constants.PRE_PROCESS_UNIT_STRING == ppu.getPreProcessType()) {
                this.editObject = "ppu_string";
                preProcessController.setSelectedNode(selectedNode);
                preProcessController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_NUMBER == ppu.getPreProcessType()) {
                this.editObject = "ppu_number";
                preProcessNumberController.setSelectedNode(selectedNode);
                preProcessNumberController.onEditObject();
            }

            if (Constants.PROCESS_UNIT_EXIST_ELEMENT == ppu.getPreProcessType()) {
                this.editObject = "ppu_exist_element";
                preProcessExistElementController.setSelectedNode(selectedNode);
                preProcessExistElementController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_TIME == ppu.getPreProcessType()) {
                this.editObject = "ppu_time";
                preProcessTimeController.setSelectedNode(selectedNode);
                preProcessTimeController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_DATE == ppu.getPreProcessType()) {
                this.editObject = "ppu_date";
                preProcessDateController.setSelectedNode(selectedNode);
                preProcessDateController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_NUMBER_RANGE == ppu.getPreProcessType()) {
                this.editObject = "ppu_number_range";
                preProcessNumberRangeController.setSelectedNode(selectedNode);
                preProcessNumberRangeController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_COMPARE_NUMBER == ppu.getPreProcessType()) {
                this.editObject = "ppu_compare_nember";
                preProcessCompareNumberController.setSelectedNode(selectedNode);
                preProcessCompareNumberController.onEditObject();
            }

            if (Constants.PROCESS_UNIT_ZONE == ppu.getPreProcessType()) {
                this.editObject = "ppu_zone";
                preProcessZoneController.setSelectedNode(selectedNode);
                preProcessZoneController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_SAME_ELEMENT == ppu.getPreProcessType()) {
                this.editObject = "ppu_same_element";
                preProcessSameElementController.setSelectedNode(selectedNode);
                preProcessSameElementController.onEditObject();
            }

        }

    }

    public void doAddResultTableToRule(TreeNode ruleNote) {
        Rule rule = (Rule) ruleNote.getData();
        List<ResultTable> resultTables = campaignOnlineService.getListResultByRuleId(rule.getRuleId());
        if (resultTables != null && !resultTables.isEmpty()) {
            for (ResultTable resultTable : resultTables) {
                TreeNode resultNote = new DefaultTreeNode("result", resultTable, ruleNote);
                doAddConditionToResultTable(resultNote);
            }
        }
    }

    public void doAddConditionToResultTable(TreeNode resultNote) {
        ResultTable resultTable = (ResultTable) resultNote.getData();
        if (resultTable.getConditionTableId() == null) {
            return;
        }
        ConditionTable conditionTable = campaignOnlineService.getListConditionById(resultTable.getConditionTableId());
        if (conditionTable != null) {
            TreeNode conditionNote = new DefaultTreeNode("condition", conditionTable, resultNote);
            doAddColumnToCondition(conditionNote);
        }
    }

    public void doAddColumnToCondition(TreeNode conditionNote) {
        ConditionTable conditionTable = (ConditionTable) conditionNote.getData();
        List<PreProcessUnit> lstPPU = campaignOnlineService.getListPPUByConditionId(conditionTable.getConditionTableId());
        if (lstPPU != null && !lstPPU.isEmpty()) {
            for (PreProcessUnit preProcessUnit : lstPPU) {
                TreeNode columnNote = new DefaultTreeNode("ppu", preProcessUnit, conditionNote);
            }
        }
    }

    public void onChangeApplyCdr() {
        cdrServicesAll = campaignOnlineService.getAllCdrService();
        cdrServicesTable.clear();
        if (editMode && applyAllCdrToRule == 2){
            initDataForCDRServiceTable();
        }
    }
    public void initDataForCDRServiceTable(){
        String[] cdrServiceId = this.currentValue.getCdrServiceId().split(",");
        cdrServicesTable = new ArrayList<>();
        List<String> data = Arrays.asList(cdrServiceId);
        Map<String, String> mapObject = new HashMap<>();
        data.forEach(e -> {
            mapObject.put(e, e);
        });
        cdrServicesAll.forEach(e -> {
            String keyValue = mapObject.get(e.getCdrServiceId() + "");
            if (keyValue != null) {
                cdrServicesTable.add(e);
            }
        });
        cdrServicesAll.removeAll(cdrServicesTable);
    }

    public boolean onSaveResultTable() {
        if (selectedResultTable.length == 0) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Result table");
            return false;
        }
        List<ResultTable> tempResultTable = new ArrayList<>();
        for (TreeNode treeNode : selectedResultTable) {
            Object data = treeNode.getData();
            if (data instanceof Category && treeNode.getChildren().isEmpty()) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.resultable.category"));
                return false;
            } else if (data instanceof ResultTable) {
                ResultTable resultTable = (ResultTable) data;
                tempResultTable.add(resultTable);
            }
        }
        resultTable.clear();
        resultTable.addAll(tempResultTable);
        return true;
    }

    public void createMapResulTableTreeNode(TreeNode node, Map<Long, TreeNode> mapResultTableNode) {
        if (node == null) {
            return;
        }
        Object data = node.getData();
        if (data instanceof ResultTable) {
            ResultTable resultTable = (ResultTable) data;
            mapResultTableNode.put(resultTable.getResultTableId(), node);
            return;
        }
        for (TreeNode treeNode : node.getChildren()) {
            createMapResulTableTreeNode(treeNode, mapResultTableNode);

        }
    }

    public void createSelectedTreeNode() {
        selectedResultTable = new TreeNode[resultTable.size()];
        for (int i = 0; i < resultTable.size(); i++) {
            TreeNode treeNode = mapResultTableNode.get(resultTable.get(i).getResultTableId());
            if (treeNode !=null){
            treeNode.setSelected(true);
            expandCurrentNode(treeNode);
            selectedResultTable[i] = treeNode;
            }
        }
    }

    public TreeNode[] getSelectedResultTable() {
        return selectedResultTable;
    }

    public void setSelectedResultTable(TreeNode[] selectedResultTable) {
        this.selectedResultTable = selectedResultTable;
    }

    public void prepareAddObject() {
        this.editObject = "rule_online";
        onAddNewObject();
    }

	@Override
	public boolean validateBeforeDelete() {
		if (ruleServiceImpl.checkRuleInUse(currentValue.getRuleId())) {
			errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("rule.used.in.campaign"), "campaign online");
			return false;
		}
		return true;
	}
}
