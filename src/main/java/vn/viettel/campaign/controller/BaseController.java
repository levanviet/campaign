/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import java.text.MessageFormat;
import java.util.*;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.TreeNode;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.primefaces.context.PrimeRequestContext;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dto.NodeInfo;
import vn.viettel.campaign.entities.Category;

/**
 * @author ConKC
 */
public abstract class BaseController extends SpringBeanAutowiringSupport {

    protected final int ACTION_SAVE = 1;
    protected final int ACTION_EDIT = 2;
    protected final int ACTION_DELETE = 3;
    protected final int ACTION_SEARCH = 4;
    public Map<Long, TreeNode> treeNodeExpanded = new HashMap<>();

    public static void errorMsg(String areaId, String errorCode, String description) {
        FacesContext.getCurrentInstance().addMessage(areaId, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorCode, description));
        PrimeRequestContext.getCurrentInstance().isIgnoreAutoUpdate();
        FacesContext.getCurrentInstance().validationFailed();
    }

    public static void errorMsg(String areaId, String errorCode, String description, Object... params) {
        String msg = MessageFormat.format(description, params);
        FacesContext.getCurrentInstance().addMessage(areaId, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorCode, msg));
        PrimeRequestContext.getCurrentInstance().isIgnoreAutoUpdate();
        FacesContext.getCurrentInstance().validationFailed();
    }

    public static void errorMsg(String areaId, Exception e) {
        FacesContext.getCurrentInstance().addMessage(areaId, new FacesMessage(FacesMessage.SEVERITY_ERROR, StringUtils.EMPTY, e.toString()));
        PrimeRequestContext.getCurrentInstance().isIgnoreAutoUpdate();
        FacesContext.getCurrentInstance().validationFailed();
    }

    public static void errorMsg(String areaId, String description) {
        FacesContext.getCurrentInstance().addMessage(areaId, new FacesMessage(FacesMessage.SEVERITY_ERROR, StringUtils.EMPTY, description));
        PrimeRequestContext.getCurrentInstance().isIgnoreAutoUpdate();
        FacesContext.getCurrentInstance().validationFailed();
    }

    public static void errorMsgParams(String areaId, String description, Object... params) {
        String msg = MessageFormat.format(description, params);
        FacesContext.getCurrentInstance().addMessage(areaId, new FacesMessage(FacesMessage.SEVERITY_ERROR, StringUtils.EMPTY, msg));
        PrimeRequestContext.getCurrentInstance().isIgnoreAutoUpdate();
        FacesContext.getCurrentInstance().validationFailed();
    }

    public static void addNotice(String description) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR.toString(), description));
    }

    public static void successMsg(String areaId, String description) {
        FacesContext.getCurrentInstance().addMessage(areaId, new FacesMessage(FacesMessage.SEVERITY_INFO, StringUtils.EMPTY, description));
        PrimeRequestContext.getCurrentInstance().isIgnoreAutoUpdate();
    }

    public static void successMsg(String areaId, String description, Object... params) {
        String msg = MessageFormat.format(description, params);
        FacesContext.getCurrentInstance().addMessage(areaId, new FacesMessage(FacesMessage.SEVERITY_INFO, StringUtils.EMPTY, msg));
    }

    public TreeNode findCategoryInTree(TreeNode root, Category category) {
        if (Objects.isNull(category)) {
            return null;
        }
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote;
            if (note.getData() instanceof Category) {
                Category data = (Category) note.getData();
                if (DataUtil.safeEqual(data.getCategoryId(), category.getCategoryId())) {
                    return note;
                }
            }
            currentNote = findCategoryInTree(note, category);
            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }
    //node info
    public TreeNode findNodeInfoInTree(TreeNode root, NodeInfo nodeInfo) {
        if (Objects.isNull(nodeInfo)) {
            return null;
        }
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote;
            if (note.getData() instanceof NodeInfo) {
                NodeInfo data = (NodeInfo) note.getData();
                if (DataUtil.safeEqual(data.getId(), nodeInfo.getId())) {
                    return note;
                }
            }
            currentNote = findNodeInfoInTree(note, nodeInfo);
            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }

//    public void expandCurrentNode(TreeNode selectedNode) {
//        if (Objects.nonNull(selectedNode)) {
//            selectedNode.setExpanded(true);
//            TreeNode parent = selectedNode.getParent();
//            if (Objects.nonNull(parent)) {
//                parent.setExpanded(true);
//                parent.setSelected(false);
//                expandCurrentNode(parent);
//            }
//        }
//    }
    public void onNodeExpand(NodeExpandEvent event) {
        addExpandedNode(event.getTreeNode());
    }

    public void addExpandedNode(TreeNode node) {
        Category category = (Category) node.getData();
        if (category == null) {
            return;
        }
        node.setExpanded(true);
        treeNodeExpanded.put(category.getCategoryId(), node);
    }
    //Node info
    public void addExpandedNodeInfo(TreeNode node) {
        NodeInfo nodeInfo = (NodeInfo) node.getData();
        if (nodeInfo == null) {
            return;
        }
        node.setExpanded(true);
        treeNodeExpanded.put(nodeInfo.getId(), node);
    }

    public void onNodeCollapse(NodeCollapseEvent event) {
        removeExpandedNode(event.getTreeNode());
    }

    public void removeExpandedNode(TreeNode node) {
        Category category = (Category) node.getData();
        if (category == null) {
            return;
        }
        node.setExpanded(false);
        treeNodeExpanded.remove(category.getCategoryId());
    }
    //node info
     public void removeExpandedNodeInfo(TreeNode node) {
        NodeInfo nodeInfo = (NodeInfo) node.getData();
        if (nodeInfo == null) {
            return;
        }
        node.setExpanded(false);
        treeNodeExpanded.remove(nodeInfo.getId());
    }

    public void mapTreeStatus(TreeNode root) {
        if (root != null && root.getData() != null && root.getData() instanceof Category) {
            Category category = (Category) root.getData();
            TreeNode treeNode = treeNodeExpanded.get(category.getCategoryId());
            if (treeNode != null) {
                root.setExpanded(true);
            }
            if (root != null && treeNode != null && root.isExpanded() && root.getChildren().isEmpty()) {
                root.setExpanded(false);
                treeNodeExpanded.remove(category.getCategoryId());
            }
        }
        if (root != null) {
            List<TreeNode> lstChildrent = root.getChildren();
            for (TreeNode note : lstChildrent) {
                mapTreeStatus(note);
            }
        }
    }
    //Node info
    public void mapTreeStatusNodeInfo(TreeNode root) {
        if (root != null && root.getData() != null && root.getData() instanceof Category) {
            NodeInfo nodeInfo = (NodeInfo) root.getData();
            TreeNode treeNode = treeNodeExpanded.get(nodeInfo.getId());
            if (treeNode != null) {
                root.setExpanded(true);
            }
            if (root != null && treeNode != null && root.isExpanded() && root.getChildren().isEmpty()) {
                root.setExpanded(false);
                treeNodeExpanded.remove(nodeInfo.getId());
            }
        }
        if (root != null) {
            List<TreeNode> lstChildrent = root.getChildren();
            for (TreeNode note : lstChildrent) {
                mapTreeStatusNodeInfo(note);
            }
        }
    }

    public void expandCurrentNode(TreeNode selectedNode) {
        TreeNode parent = selectedNode.getParent();
        if (parent == null || parent.getData() == null) {
            return;
        } else {
            addExpandedNode(parent);
            expandCurrentNode(parent);
        }
    }
    //node info
     public void expandCurrentNodeInfo(TreeNode selectedNode) {
        TreeNode parent = selectedNode.getParent();
        if (parent == null || parent.getData() == null) {
            return;
        } else {
            addExpandedNodeInfo(parent);
            expandCurrentNodeInfo(parent);
        }
    }

    public void processRefreshCategory(TreeNode rootNode, TreeNode selectedNode, Category category, boolean isUpdate) {
        if (isUpdate) {
            TreeNode treeNode = findCategoryInTree(rootNode, (Category) selectedNode.getData());
            if (treeNode != null) {
                expandCurrentNode(treeNode);
            }
            if (treeNode != null && treeNode.getParent() != null && selectedNode.getParent() != null) {
                Category newNode = (Category) treeNode.getParent().getData();
                Category oldNode = (Category) selectedNode.getParent().getData();
                if (oldNode != null && newNode != null) {
                    if (oldNode.getCategoryId() != newNode.getCategoryId() && selectedNode.getParent().getChildren().size() == 1) {
                        removeExpandedNode(selectedNode.getParent());
                    }
                } else if (oldNode == null && newNode == null) {

                } else {
                    if (selectedNode.getParent() != null && selectedNode.getParent().getChildren().size() == 1) {
                        removeExpandedNode(selectedNode.getParent());
                    }
                }
            }
        } else {
            TreeNode treeNode = findCategoryInTree(rootNode, category);
            if (treeNode != null) {
                expandCurrentNode(treeNode);
            }
        }
        mapTreeStatus(rootNode);
    }
    //Node info
     public void processRefreshNodeInfo(TreeNode rootNode, TreeNode selectedNode, NodeInfo nodeInfo, boolean isUpdate) {
        if (isUpdate) {
            TreeNode treeNode = findNodeInfoInTree(rootNode, (NodeInfo) selectedNode.getData());
            if (treeNode != null) {
                expandCurrentNodeInfo(treeNode);
            }
            if (treeNode != null && treeNode.getParent() != null && selectedNode.getParent() != null) {
                NodeInfo newNode = (NodeInfo) treeNode.getParent().getData();
                NodeInfo oldNode = (NodeInfo) selectedNode.getParent().getData();
                if (oldNode != null && newNode != null) {
                    if (oldNode.getId()!= newNode.getId() && selectedNode.getParent().getChildren().size() == 1) {
                        removeExpandedNodeInfo(selectedNode.getParent());
                    }
                } else if (oldNode == null && newNode == null) {

                } else {
                    if (selectedNode.getParent() != null && selectedNode.getParent().getChildren().size() == 1) {
                        removeExpandedNodeInfo(selectedNode.getParent());
                    }
                }
            }
        } else {
            TreeNode treeNode = findNodeInfoInTree(rootNode, nodeInfo);
            if (treeNode != null) {
                expandCurrentNodeInfo(treeNode);
            }
        }
        mapTreeStatusNodeInfo(rootNode);
    }

    public void updateCategoryInform() {
        PrimeFaces.current().ajax().update("mainForm:category");
    }

    public void updateCategoryInformCampaign() {
        PrimeFaces.current().ajax().update("mainCampaignForm:category");
    }

    public void updateCategoryInforPromotion() {
        PrimeFaces.current().ajax().update("contentForm:category");
    }

    public void updateCategoryInforNotifyTemplate() {
        UIViewRoot view = FacesContext.getCurrentInstance().getViewRoot();
        UIComponent component = view.findComponent("frmNotifyTemp:category");
        if (!DataUtil.isNullObject(component)) {
            PrimeFaces.current().ajax().update("frmNotifyTemp:category");
        }
    }

    public void updateCategoryInformCondition() {
        UIViewRoot view = FacesContext.getCurrentInstance().getViewRoot();
        UIComponent component = view.findComponent("conditionForm:category");
        if (!DataUtil.isNullObject(component)) {
            PrimeFaces.current().ajax().update("conditionForm:category");
        }
    }

    public void updateCategoryInformUssd() {
        UIViewRoot view = FacesContext.getCurrentInstance().getViewRoot();
        UIComponent component = view.findComponent("ussdForm:category");
        if (!DataUtil.isNullObject(component)) {
            PrimeFaces.current().ajax().update("ussdForm:category");
        }
    }

    public void updateCategoryInformNotify() {
        UIViewRoot view = FacesContext.getCurrentInstance().getViewRoot();
        UIComponent component = view.findComponent("notifyForm:category");
        if (!DataUtil.isNullObject(component)) {
            PrimeFaces.current().ajax().update("notifyForm:category");
        }
    }
}
