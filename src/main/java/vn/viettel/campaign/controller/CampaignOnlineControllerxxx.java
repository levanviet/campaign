
package vn.viettel.campaign.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.poseidon.domain.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.entities.Campaign;
import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.entities.Invitation;
import vn.viettel.campaign.entities.Rule;
import vn.viettel.campaign.entities.Segment;
import vn.viettel.campaign.service.CategoryService;
import vn.viettel.campaign.service.impl.TreeServiceImpl;

@ManagedBean
public class CampaignOnlineControllerxxx extends SpringBeanAutowiringSupport implements Serializable{
    
    @Autowired
    private CategoryService categoryServiceImpl;
    
    @Autowired
    private TreeServiceImpl treeServiceImpl;

    private TreeNode root;
    private TreeNode rootNode;
    private TreeNode selectedNode;
    
    private TreeNode rootCata;

    private Segment segment;

    private List<Segment> lstSeg;

    private String searchSeg;

    private Rule ruleSelected;

    private List<Rule> lstRule;

    private String searchRule;

    private Invitation invitation;

    private List<Invitation> lstInvi;

    private String searchInvi;
    
    private String action;
    
    @PostConstruct
    public void init() {
        root = createDocuments();
        lstSeg = createLstSeg();
        lstRule = createLstRule();
        lstInvi = createLstInvi();
        rootCata = createCata();
        onClickMenu(1);
    }
    
    public void onClickMenu(int menuType) {
 
        List<Category> categorys = categoryServiceImpl.getCategoryByType(menuType);
        List<Long> longs = new ArrayList<>();
        if (!categorys.isEmpty()) {
            categorys.forEach(item -> longs.add(item.getCategoryId()));
        }
        List<Campaign> objects = categoryServiceImpl.getObjectByCategory(longs, Constants.CAMPAIGN);
        rootNode = treeServiceImpl.createTreeCategoryAndComponent(categorys, objects);
    }
    
    public TreeNode createCata() {
        //root
        TreeNode root1 = new DefaultTreeNode(new Document("Files", "-", "Folder"), null);

        //Segment 1
        TreeNode online = new DefaultTreeNode(new Document("Online", "-", "Folder"), root1);
        TreeNode running = new DefaultTreeNode(new Document("Running", "-", "Folder"), root1);
        //level 2
        TreeNode on1 = new DefaultTreeNode("document", new Document("online 1", "-", "Pages Document"), online);
        TreeNode on2 = new DefaultTreeNode("document", new Document("online 2", "-", "Pages Document"), online);
        //level 3
        TreeNode lstCdr = new DefaultTreeNode("document", new Document("Demo", "-", "Pages Document"), on1);
        TreeNode demoOcs = new DefaultTreeNode("document", new Document("Demo OCS", "-", "Pages Document"), on2);
        TreeNode demoVtt = new DefaultTreeNode("document", new Document("Demo VTT", "-", "Pages Document"), on2);
        //level d
        TreeNode onOcs = new DefaultTreeNode("document", new Document("online ocs", "-", "Pages Document"), demoOcs);
        TreeNode onVtt = new DefaultTreeNode("document", new Document("online vtt", "-", "Pages Document"), demoVtt);
        
        return root1;
    }

    public TreeNode createDocuments() {
        //root
        TreeNode root1 = new DefaultTreeNode(new Document("Files", "-", "Folder"), null);

        //Segment 1
        TreeNode segment1 = new DefaultTreeNode(new Document("Segment 1", "-", "Folder"), root1);
        //level 2
        TreeNode rule1 = new DefaultTreeNode("document", new Document("Rule 1", "-", "Pages Document"), segment1);
        //level 3
        TreeNode lstCdr = new DefaultTreeNode("document", new Document("List cdr", "-", "Pages Document"), rule1);
        TreeNode result1 = new DefaultTreeNode("document", new Document("Result table 1", "-", "Pages Document"), rule1);
        TreeNode result2 = new DefaultTreeNode("document", new Document("Result table 2", "-", "Pages Document"), rule1);
        //level 4
        TreeNode voice = new DefaultTreeNode("document", new Document("Voice", "-", "Pages Document"), lstCdr);
        TreeNode data = new DefaultTreeNode("document", new Document("Data", "-", "Pages Document"), lstCdr);
        TreeNode sms = new DefaultTreeNode("document", new Document("Sms", "-", "Pages Document"), lstCdr);
        TreeNode condition = new DefaultTreeNode("document", new Document("Condition 1", "-", "Pages Document"), result2);
        //level 5
        TreeNode ppu = new DefaultTreeNode("picture", new Document("PPU 1", "-", "checkbox"), condition);

        //Segment 2
        TreeNode segment2 = new DefaultTreeNode(new Document("Segment 2", "-", "Folder"), root1);
        //level 2
        TreeNode rule = new DefaultTreeNode("document", new Document("Rule 1", "-", "Pages Document"), segment2);
        TreeNode rule2 = new DefaultTreeNode("document", new Document("Rule 2", "-", "Pages Document"), segment2);
        //level 3
        TreeNode resultseg2 = new DefaultTreeNode("document", new Document("Result table 1", "-", "Pages Document"), rule);
        TreeNode result2seg2 = new DefaultTreeNode("document", new Document("Result table 2", "-", "Pages Document"), rule2);
        TreeNode result3seg2 = new DefaultTreeNode("document", new Document("Result table 3", "-", "Pages Document"), rule2);
        //level 4
        TreeNode dtion1 = new DefaultTreeNode("document", new Document("Condition 1", "-", "Pages Document"), resultseg2);
        TreeNode dtion2 = new DefaultTreeNode("document", new Document("Condition 2", "-", "Pages Document"), result2seg2);
        //level 5
        TreeNode ppu1 = new DefaultTreeNode("picture", new Document("PPU 1", "-", "checkbox"), dtion1);
        TreeNode ppu2 = new DefaultTreeNode("picture", new Document("PPU 2", "-", "checkbox"), dtion2);
        TreeNode ppu3 = new DefaultTreeNode("picture", new Document("PPU 3", "-", "checkbox"), dtion2);

        return root1;
    }

    public TreeNode getRoot() {
        return root;
    }

    private List<Segment> createLstSeg() {
        List<Segment> lst = new ArrayList();
//        Segment seg;
//
//        for (Long i = 0L; i < 35; i++) {
//            seg = new Segment();
//            seg.setId("" + i);
//            seg.setName("segment " + i);
//            seg.setEffectDate(new Date());
//            seg.setExpDatte(new Date());
//            lst.add(seg);
//        }

        return lst;
    }

    private List<Rule> createLstRule() {
        List<Rule> lst = new ArrayList();
//        Rule seg;
//
//        for (Long i = 0L; i < 35; i++) {
//            seg = new Rule();
//            seg.setId("" + i);
//            seg.setName("rule " + i);
//            lst.add(seg);
//        }
//
        return lst;
    }

    private List<Invitation> createLstInvi() {
        List<Invitation> lst = new ArrayList();
        Invitation seg;

        for (Long i = 0L; i < 35; i++) {
            seg = new Invitation();
            seg.setId("" + i);
            seg.setName("invitation " + i);
            seg.setSegment("1");
            seg.setType("sms");
            seg.setPriority("1");
            lst.add(seg);
        }

        return lst;
    }

    public List<Segment> getLstSeg() {
        return lstSeg;
    }

    public void setLstSeg(List<Segment> lstSeg) {
        this.lstSeg = lstSeg;
    }

    public Segment getSegment() {
        return segment;
    }

    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    public String getSearchSeg() {
        return searchSeg;
    }

    public void setSearchSeg(String searchSeg) {
        this.searchSeg = searchSeg;
    }

    public Rule getRuleSelected() {
        return ruleSelected;
    }

    public void setRuleSelected(Rule ruleSelected) {
        this.ruleSelected = ruleSelected;
    }

    public List<Rule> getLstRule() {
        return lstRule;
    }

    public void setLstRule(List<Rule> lstRule) {
        this.lstRule = lstRule;
    }

    public String getSearchRule() {
        return searchRule;
    }

    public void setSearchRule(String searchRule) {
        this.searchRule = searchRule;
    }

    public Invitation getInvitation() {
        return invitation;
    }

    public void setInvitation(Invitation invitation) {
        this.invitation = invitation;
    }

    public List<Invitation> getLstInvi() {
        return lstInvi;
    }

    public void setLstInvi(List<Invitation> lstInvi) {
        this.lstInvi = lstInvi;
    }

    public String getSearchInvi() {
        return searchInvi;
    }

    public void setSearchInvi(String searchInvi) {
        this.searchInvi = searchInvi;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }
    
    public TreeNode getRootCata() {
        return rootCata;
    }

    public void setRootCata(TreeNode rootCata) {
        this.rootCata = rootCata;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public TreeNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode rootNode) {
        this.rootNode = rootNode;
    }

}
