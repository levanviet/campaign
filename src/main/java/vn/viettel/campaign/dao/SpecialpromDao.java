package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.SpecialProm;

/**
 *
 * @author ConKC
 */
public interface SpecialpromDao extends BaseDAOInteface<SpecialProm> {

    List<SpecialProm> findSpecialpromByCategory(final List<Long> categoryIds);

    List<SpecialProm> findSpecialpromByName(final String specialpromByName);

    long onSaveSpecialProm(SpecialProm specialProm);

    boolean checkDuplicate(final String name, final Long id);

    boolean checkDuplicatePath(final String path, final Long id);

}
