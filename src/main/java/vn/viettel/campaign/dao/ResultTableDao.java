package vn.viettel.campaign.dao;

import java.util.List;

import vn.viettel.campaign.dto.ResultTableDTO;
import vn.viettel.campaign.entities.ResultTable;



/**
 *
 * @author ConKC
 */
public interface ResultTableDao  extends BaseDAOInteface<ResultTable>{
    List<ResultTable> finAllResultTable();
    ResultTable updateOrSave(ResultTable resultTable);
    ResultTable getResultTableById(Long resultTableId);
    List<ResultTable> getListToCheckDuplicate(String resultTableName, Long resultTableId);
    Long checkUseRule(Long resultTableId);
    List<ResultTableDTO> getListResultTableDTO(Long conditionTableId, Long rowIndex);
}
