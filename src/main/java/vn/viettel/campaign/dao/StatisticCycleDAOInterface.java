package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.NotifyStatisticMap;
import vn.viettel.campaign.entities.StatisticCycle;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface StatisticCycleDAOInterface extends BaseDAOInteface<StatisticCycle> {
    public List<StatisticCycle> findByNotifyTriggerId(long id);
    public List<StatisticCycle> findById(long id);
    public void deleteByNotifyTriggerId(long id);
}
