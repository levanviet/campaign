package vn.viettel.campaign.dao;

import org.hibernate.Session;
import vn.viettel.campaign.entities.BlackList;
import vn.viettel.campaign.entities.OcsBehaviourTemplate;
import vn.viettel.campaign.entities.OcsBehaviourTemplateExtendFields;
import vn.viettel.campaign.entities.OcsBehaviourTemplateFields;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface OCSTemplateDaoInterface extends BaseDAOInteface<OcsBehaviourTemplate> {

    public OcsBehaviourTemplate getNextSequense();

    public String onSaveBehaviourTemplate(OcsBehaviourTemplate ocsBehaviourTemplate,
            List<OcsBehaviourTemplateFields> lstField, List<OcsBehaviourTemplateExtendFields> lstExtendFields);

    public String onUpdateBehaviourTemplate(OcsBehaviourTemplate ocsBehaviourTemplate,
            List<OcsBehaviourTemplateFields> lstField, List<OcsBehaviourTemplateExtendFields> lstExtendFields);

    public boolean deleteTemplate(Long templateId);

    public boolean checkTemplateName(String name, Long id);

    public boolean checkOcsBehaviourTemplateInUse(Long templateId);
}
