package vn.viettel.campaign.dao;


import org.hibernate.Session;
import vn.viettel.campaign.entities.RuleResultTableMap;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface RuleResultTableMapInterface extends BaseDAOInteface<RuleResultTableMap>{
	public void deleteResultTableMap(Session session, Long ruleId);
}
