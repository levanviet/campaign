/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.EvaluationInputObject;

/**
 *
 * @author SON
 */
public interface EvaluationInputObjectDAO {
    
    List<EvaluationInputObject> getLstEvaluationInputObjectHaveParentIdIsNull();
    List<EvaluationInputObject> getLstEvaluationInputObject();
    List<EvaluationInputObject> getLstEvaluationInputObjectByParentId(Long parentId);
    void onSaveOrUpdateEvaluationInputObject(EvaluationInputObject evaluationInputObject);
    void onDeleteEvaluationInputObject(EvaluationInputObject evaluationInputObject);
    EvaluationInputObject findOneByName(String name);
    EvaluationInputObject findOneById(Long id);
    List<EvaluationInputObject> getListCondition(Long id);
}
