/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.RuleEvaluationInfo;

/**
 *
 * @author ABC
 */
public interface RuleEvaluationInfoDAOInterface {

    public List<RuleEvaluationInfo> getListRuleEvaluationInfoByOwnerId(Long campaignEvaluationInfoId);
    
    public List<RuleEvaluationInfo> getListRuleEvaluationInfoOfSegment(Long segmentEvaluationInfoId);
    
    RuleEvaluationInfo getNextSequenceRuleEvaluation();
    
    public RuleEvaluationInfo doCreateOrUpdateRuleEvaluationInfo(RuleEvaluationInfo ruleEvaluationInfo);
   
    public boolean deleteRuleEvalautionInfo(Long id);
    
    public List<RuleEvaluationInfo> findRuleEvaluationInfoById(Long assessmentRuleId );
}
