/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.ResultClass;

/**
 *
 * @author ABC
 */
public interface ResultClassDAOInfterface {

    public List<ResultClass> getListResultClassByOwnerIdAndCampaignEvaluationInfoId(Long campaignId, Long campaignEvaluationInfoId);
    
    public List<ResultClass> getListResultClassByAssessmentRuleId(Long assessmentRuleId);    
    
    public List<ResultClass> getListResultClassOfSegment(Long segmentId,Long campaignEvaluationInfoId);

    ResultClass getNextSequenceResultClass();
    
    public ResultClass getNameByClassId(Long classId);
    
    public ResultClass doCreateOrUpdateResultClass(ResultClass resultClass);
    
    public boolean deleteResultClass(Long id);
    
   
}
