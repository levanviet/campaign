package vn.viettel.campaign.dao;

import org.hibernate.Session;
import vn.viettel.campaign.entities.OcsBehaviourTemplate;
import vn.viettel.campaign.entities.OcsBehaviourTemplateExtendFields;
import vn.viettel.campaign.entities.OcsBehaviourTemplateFields;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface OCSTemplateExtendFieldDaoInterface extends BaseDAOInteface<OcsBehaviourTemplateExtendFields>{
	public List<OcsBehaviourTemplateExtendFields> getLstExtendFieldOfTemplate(long templateId) ;
	public List<OcsBehaviourTemplateExtendFields> getRemainExtendFieldOfTemplate(long templateId, List<Long> lstExtendIds) ;
	public void deleteTemplateExtendFieldByTemplateId(Session session, Long templateId);
}

