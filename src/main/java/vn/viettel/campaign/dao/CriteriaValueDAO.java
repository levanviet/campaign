/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.CriteriaValue;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.KpiProcessorCriteriaValueMap;

/**
 *
 * @author SON
 */
public interface CriteriaValueDAO {

    List<CriteriaValue> getLstCriteriaValueByKpiProcessorId(Long id);

    void onSave(CriteriaValue criteriaValue);

    void onUpdate(CriteriaValue criteriaValue);

    void onDelete(Long id);

    CriteriaValue getNextSequense();

    CriteriaValue findOneById(Long id);

    void onSaveOrUpdateCriteriaValue(CriteriaValue criteriaValue);

    void onSaveKpiCriteriaValueMap(KpiProcessorCriteriaValueMap kpiProcessorCriteriaValueMap);

    CriteriaValue checkExitsCriteriaName(String name, Long id, Long processorId);

    List<KpiProcessorCriteriaValueMap> getLstKpiProcessorCriteriaValueMap();

    void onDeleteCriteriaValue(Long processorId, Long CriteriaValueId);

    public List<CriteriaValue> getListCriteriaValueById(List<Long> longs);

    List<CriteriaValue> checkDeleteCriteriaValue(Long kpiId, String name);

    public CriteriaValue chekCriteriaValueName(String name,Long processId);
    
    List<FuzzyRule> getListFuzzyForCriteriaValue(Long kpiId);
}
