package vn.viettel.campaign.dao;

import org.hibernate.Session;
import vn.viettel.campaign.entities.Function;
import vn.viettel.campaign.entities.InvitationPriority;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface FunctionDAOInterface extends BaseDAOInteface<Function> {
	List<Function> getListFunction();
	public List<Function> getListFunctionTypeOne();
}
