package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.ConditionTableRowMap;

/**
 *
 */
public interface ConditionTableRowMapDAOInterface extends BaseDAOInteface<ConditionTableRowMap> {
    public void deleteByConditionTableId(Long conditionTableId);
}
