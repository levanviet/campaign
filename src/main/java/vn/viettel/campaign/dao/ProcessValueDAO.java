/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.PpuEvaluation;
import vn.viettel.campaign.entities.ProcessValue;

/**
 *
 * @author admin
 */
public interface ProcessValueDAO {

    void onSaveOrUpdateProcessValue(ProcessValue processValue);

    void onDeleteProcessValue(PpuEvaluation ppuEvaluation);

    List<ProcessValue> getLstProcessValue();

    List<ProcessValue> getLstProcessValue(Long id);
    
    ProcessValue getNextSequence();
    
    ProcessValue checkSavePreProcessValueMap(Long processValueId, Long ppuId);
    
    void onDeleteProcessValue(Long  processValueId, Long ppuId);
    
    List<ProcessValue> checkDeleteProcessValue(Long ppuId, String name);
    
    List<FuzzyRule> getListFuzzyForProcessValue(Long ppuId);
}
