package vn.viettel.campaign.dao.notifytemplate;

import vn.viettel.campaign.entities.NotifyTemplate;

import java.util.List;

public interface NotifyTemplateDAO {

    List<NotifyTemplate> getTemplateByCategory(final List<Long> categoryIds);

    NotifyTemplate getOneById(Long id);

    void removeById(Long id);

    NotifyTemplate save(NotifyTemplate template);

    NotifyTemplate findOnByName(String name, Long id);
}
