package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.CdrService;
import vn.viettel.campaign.entities.Rule;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface CdrServiceDAOInterface extends BaseDAOInteface<CdrService>{
}
