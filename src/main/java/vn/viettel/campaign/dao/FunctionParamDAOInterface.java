package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.Function;
import vn.viettel.campaign.entities.FunctionParam;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface FunctionParamDAOInterface extends BaseDAOInteface<FunctionParam> {
	List<FunctionParam> getListFunctionParam(long functionId);
}
