package vn.viettel.campaign.dao;

import org.hibernate.Session;
import vn.viettel.campaign.entities.OcsBehaviourTemplate;
import vn.viettel.campaign.entities.OcsBehaviourTemplateFields;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface OCSTemplateFieldDaoInterface extends BaseDAOInteface<OcsBehaviourTemplateFields>{
	public List<OcsBehaviourTemplateFields> getLstFieldOfTemplate(long templateId) ;
	public List<OcsBehaviourTemplateFields> getRemainFieldOfTemplate(long templateId, List<Long> ids) ;
	public void deleteTemplateFieldByTemplateId(Session session, Long templateId);
}
