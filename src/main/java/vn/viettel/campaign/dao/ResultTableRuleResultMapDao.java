package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.Result;
import vn.viettel.campaign.entities.ResultTableRuleResultMap;
import vn.viettel.campaign.entities.RuleResult;


/**
 *
 * @author ConKC
 */
public interface ResultTableRuleResultMapDao {
    List<ResultTableRuleResultMap> finAllResultTableRuleResultMap();
    ResultTableRuleResultMap updateOrSave(ResultTableRuleResultMap resultTableRuleResultMap);
    ResultTableRuleResultMap getResultTableRuleResultMapById(Long resultTableRuleResultId);
    void deleteByResultTableId(long id);
}
