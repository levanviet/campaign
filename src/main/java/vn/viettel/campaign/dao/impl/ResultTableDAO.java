package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.ResultTableDAOInterface;
import vn.viettel.campaign.dao.RuleDAOInterface;
import vn.viettel.campaign.dto.ResultTableDTO;
import vn.viettel.campaign.entities.ResultTable;
import vn.viettel.campaign.entities.Rule;

import java.util.ArrayList;
import java.util.List;

/**
 * @author truongbx
 * @date 09/01/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class ResultTableDAO extends BaseDAOImpl<ResultTable> implements ResultTableDAOInterface{

	@Override
	public List<ResultTable> getLstRuleResultTableByRuleIds(Long ruleId) {
		String sql = "SELECT b.RESULT_TABLE_ID resultTableId,b.RESULT_TABLE_NAME resultTableName, " +
				"b.CONDITION_TABLE_ID conditionTableId , a.RULE_ID ruleId " +
				"FROM rule_result_table_map  a " +
				"INNER JOIN result_table b " +
				"on a.RESULT_TABLE_ID = b.RESULT_TABLE_ID " +
				"WHERE a.RULE_ID =:ruleId"+
				" order by a.RESULT_TABLE_INDEX ASC ";

		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("resultTableId",StandardBasicTypes.LONG);
		query.addScalar("resultTableName",StandardBasicTypes.STRING);
		query.addScalar("ruleId",StandardBasicTypes.LONG);
		query.addScalar("conditionTableId",StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(ResultTable.class));
		query.setParameter("ruleId",ruleId);
		return query.list();
	}

	@Override
	public List<ResultTable> getLstRuleResultTableByConditionTableIds(Long conditionTableId) {
		try {
			final String sql = "from ResultTable where conditionTableId = :conditionTableId";
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(sql);
			query.setParameter("conditionTableId", conditionTableId);
			return query.list();
		} catch (Exception ex) {
//			log.error("Exception " + ex);
		}
		return new ArrayList<>();
	}

	@Override
	public List<ResultTable> getAllResultTable() {
		String sql = "SELECT a.RESULT_TABLE_ID resultTableId,a.RESULT_TABLE_NAME resultTableName , " +
				"a.CONDITION_TABLE_ID conditionTableId , b.RESULT_TABLE_INDEX resultTableIndex " +
				"FROM result_table  a " +
				"LEFT JOIN rule_result_table_map b " +
				"on a.RESULT_TABLE_ID = b.RESULT_TABLE_ID ";
		SQLQuery query = getSession().createSQLQuery(sql);

		query.addScalar("resultTableId",StandardBasicTypes.LONG);
		query.addScalar("resultTableName",StandardBasicTypes.STRING);
		query.addScalar("conditionTableId",StandardBasicTypes.LONG);
		query.addScalar("resultTableIndex",StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(ResultTable.class));
		return query.list();
	}

	@Override
	public List<ResultTableDTO> getListResultTableDTO(Long conditionTableId, Long rowIndex) {
		String sql = "select a.RESULT_TABLE_ID resultTableId, a.CONDITION_TABLE_ID conditionTableId,\n" +
				"b.RESULT_TABLE_RULE_RESULT_ID resultTableRuleResultId, b.RULE_RESULT_ID ruleResultId, \n" +
				"c.RESULT_ID resultId, c.ROW_INDEX rowIndex\n" +
				"from result_table a, result_table_rule_result_map b, rule_result c, result d where \n" +
				"a.RESULT_TABLE_ID = b.RESULT_TABLE_ID\n" +
				"and b.RULE_RESULT_ID = c.RULE_RESULT_ID\n" +
				"and c.RESULT_ID = d.RESULT_ID\n" +
				"and a.CONDITION_TABLE_ID = :conditionTableId";
		if (!DataUtil.isNullObject(rowIndex)) {
			sql += " and c.ROW_INDEX = :rowIndex";
		}
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("resultTableId", StandardBasicTypes.LONG);
		query.addScalar("conditionTableId", StandardBasicTypes.LONG);
		query.addScalar("resultTableRuleResultId", StandardBasicTypes.LONG);
		query.addScalar("ruleResultId", StandardBasicTypes.LONG);
		query.addScalar("resultId", StandardBasicTypes.LONG);
		query.addScalar("rowIndex", StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(ResultTableDTO.class));
		query.setParameter("conditionTableId", conditionTableId);
		if (!DataUtil.isNullObject(rowIndex)) {
			query.setParameter("rowIndex", rowIndex);
		}
		return query.list();
	}

	@Override
	public int updateByCoditionTableId(Long conditionTableId, List<Long> ids, Long index) {
		if (DataUtil.isNullOrEmpty(ids)) {
			return 0;
		}
		String sql = "update rule_result set rule_result.ROW_INDEX = :index where rule_result.RULE_RESULT_ID in (:ids)";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.setParameter("index", index);
		query.setParameterList("ids", ids);
		return query.executeUpdate();
	}

	@Override
	public int deleteByCoditionTableId() {
		String sql = "delete from rule_result where rule_result.ROW_INDEX = -1";
		SQLQuery query = getSession().createSQLQuery(sql);
		return query.executeUpdate();
	}
}
