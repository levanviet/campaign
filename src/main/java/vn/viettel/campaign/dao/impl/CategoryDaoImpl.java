/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.BaseAbstract;
import vn.viettel.campaign.dao.CategoryDao;
import vn.viettel.campaign.entities.Category;

@Repository
@Transactional(rollbackFor = Exception.class)
public class CategoryDaoImpl extends BaseAbstract implements CategoryDao {

    private static final Logger log = Logger.getLogger(CategoryDaoImpl.class.getName());

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(Category category) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            session.saveOrUpdate(category);
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
    }

    @Override
    public List<Category> getCategory() {
        try {
            final String sql = "from Category";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            addParameters(query, null);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public List<Category> getCategoryByType(final Integer categoryType, final boolean parentId) {
        try {
            String sql = "from Category where categoryType = :categoryType ";

            if (parentId) {
                sql += "and parentId is null ";
            }
            sql += " order by name";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameter("categoryType", categoryType);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public Category getCategoryById(final Long categoryId) {
        try {
            final String sql = "from Category where categoryId = :categoryId";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameter("categoryId", categoryId);
            query.setCacheable(true);
            if (query.list() != null && !query.list().isEmpty()) {
                return (Category) query.list().get(0);
            }
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return null;
    }

    @Override
    public Boolean deleteCategory(final Category category) {
        try {
            Session session = sessionFactory.getCurrentSession();
            session.delete(category);
            return true;
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return false;
    }

    @Override
    public <T> List<T> getObjectByCategory(final List<Long> categoryIds, final String objectName) {
        try {
            final String sql = "from " + objectName + " where categoryId in (:categoryIds)";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameterList("categoryIds", categoryIds);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    public <T> List<T> getDataByCategory(final List<Long> categoryIds, final String objectName) {
        try {
            final String sql = "from " + objectName + " where categoryId in (:categoryIds)";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameterList("categoryIds", categoryIds);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public boolean checkDuplicate(final String categoryName, final Long categoryId) {
        try {
            String sql = "from Category where name = :categoryName ";
            if (Objects.nonNull(categoryId)) {
                sql += " and categoryId <> :categoryId ";
            }
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameter("categoryName", categoryName);
            if (Objects.nonNull(categoryId)) {
                query.setParameter("categoryId", categoryId);
            }
            query.setCacheable(true);
            if (query.list() != null && !query.list().isEmpty()) {
                return false;
            }
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return true;
    }

    @Override
    public boolean checkDuplicate(final String categoryName, final Long categoryId, Integer categoryType) {
        try {
            String sql = "from Category where name = :categoryName and categoryType = :categoryType ";
            if (Objects.nonNull(categoryId)) {
                sql += " and categoryId <> :categoryId ";
            }
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameter("categoryName", categoryName);
            query.setParameter("categoryType", categoryType);
            if (Objects.nonNull(categoryId)) {
                query.setParameter("categoryId", categoryId);
            }
            query.setCacheable(true);
            if (query.list() != null && !query.list().isEmpty()) {
                return false;
            }
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return true;
    }

    @Override
    public boolean checkDeleteCategory(final Long categoryId) {
        if (Objects.isNull(categoryId)) {
            return false;
        }
        try {
            Session session = sessionFactory.getCurrentSession();
            String sql = "from Category where parentId = :categoryId ";
            List lstCategory = session.createQuery(sql).setParameter("categoryId", categoryId).list();
            if ((lstCategory != null && !lstCategory.isEmpty())) {
                return false;
            }
            String sql1 = "from Campaign where categoryId = :categoryId ";
            List lstCampaign = session.createQuery(sql1).setParameter("categoryId", categoryId).list();
            if ((lstCampaign != null && !lstCampaign.isEmpty())) {
                return false;
            }
            String sql2 = "from PromotionBlock where categoryId = :categoryId ";
            List lstPromotionBlock = session.createQuery(sql2).setParameter("categoryId", categoryId).list();
            if ((lstPromotionBlock != null && !lstPromotionBlock.isEmpty())) {
                return false;
            }
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return true;
    }

    @Override
    public boolean checkDeleteCategory(Long categoryId, String className) {
        if (Objects.isNull(categoryId)) {
            return false;
        }
        try {
            Session session = sessionFactory.getCurrentSession();
            String sql = "from Category where parentId = :categoryId ";
            List lstCategory = session.createQuery(sql).setParameter("categoryId", categoryId).list();
            if ((lstCategory != null && !lstCategory.isEmpty())) {
                return false;
            }
            String sql2 = "from " + className + " where categoryId = :categoryId ";
            List lstPromotionBlock = session.createQuery(sql2).setParameter("categoryId", categoryId).list();
            if ((lstPromotionBlock != null && !lstPromotionBlock.isEmpty())) {
                return false;
            }
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return true;
    }

    @Override
    public List<Category> getCategoryByType(final Integer categoryType) {
        try {
            final String sql = "from Category where categoryType = :categoryType order by name";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameter("categoryType", categoryType);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public List<Category> findCategoryByIds(final List<Long> categoryIds) {
        try {
            final String sql = "from Category where categoryId in (:categoryIds)";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameterList("categoryIds", categoryIds);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public List<Category> findCategoryByTypes(List<Integer> types) {
        try {
            final String sql = "from Category where categoryType in (:types)";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameterList("types", types);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public Object findOne(Class clazz, long id) {
        return super.findOne(clazz, id); //To change body of generated methods, choose Tools | Templates.
    }

}
