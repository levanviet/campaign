package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.OCSTemplateDaoInterface;
import vn.viettel.campaign.dao.OCSTemplateFieldDaoInterface;
import vn.viettel.campaign.entities.OcsBehaviourTemplate;
import vn.viettel.campaign.entities.OcsBehaviourTemplateFields;
import vn.viettel.campaign.entities.Segment;

import javax.persistence.Query;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class OCSTemplateFieldDAO extends BaseDAOImpl<OcsBehaviourTemplateFields> implements OCSTemplateFieldDaoInterface {

	@Override
	public List<OcsBehaviourTemplateFields> getLstFieldOfTemplate(long templateId) {
		String sql = "SELECT a.TEMPLATE_FIELD_ID templateFieldId , a.BEHAVIOUR_TEMPLATE_ID behaviourTemplateId , " +
				"a.CRA_FIELD_ID craFieldId, a.DATA_TYPE dataType, a.VALUE value, a.SOURCE_TYPE sourceType, a.REMARK remark, b.NAME name " +
				"FROM ocs_behaviour_template_fields a " +
				"INNER JOIN field_id_ocs b " +
				"WHERE a.CRA_FIELD_ID = b.FIELD_ID " +
				"AND a.BEHAVIOUR_TEMPLATE_ID =:templateId" +
				" ORDER BY b.NAME";

		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("templateFieldId", StandardBasicTypes.LONG);
		query.addScalar("behaviourTemplateId", StandardBasicTypes.LONG);
		query.addScalar("craFieldId", StandardBasicTypes.LONG);
		query.addScalar("dataType", StandardBasicTypes.LONG);
		query.addScalar("value", StandardBasicTypes.STRING);
		query.addScalar("sourceType", StandardBasicTypes.LONG);
		query.addScalar("remark", StandardBasicTypes.STRING);
		query.addScalar("name", StandardBasicTypes.STRING);
		query.setResultTransformer(Transformers.aliasToBean(OcsBehaviourTemplateFields.class));
		query.setParameter("templateId", templateId);
		return query.list();
	}

	@Override
	public List<OcsBehaviourTemplateFields> getRemainFieldOfTemplate(long templateId, List<Long> ids) {
		String sql = "SELECT FIELD_ID craFieldId , NAME name , DATA_TYPE dataType , '1' sourceType , '" + templateId + "' behaviourTemplateId " +
				"FROM field_id_ocs where FIELD_ID not in (:ids) ORDER BY NAME;";

		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("craFieldId", StandardBasicTypes.LONG);
		query.addScalar("name", StandardBasicTypes.STRING);
		query.addScalar("sourceType", StandardBasicTypes.LONG);
		query.addScalar("behaviourTemplateId", StandardBasicTypes.LONG);
		query.addScalar("dataType", StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(OcsBehaviourTemplateFields.class));
		if (ids.size() == 0){
			ids.add(-1L);
		}
		query.setParameterList("ids", ids);
		return query.list();
	}

	@Override
	public void deleteTemplateFieldByTemplateId(Session session, Long templateId) {
			String hql = "delete from OcsBehaviourTemplateFields where behaviourTemplateId =:templateId";
			Query query = session.createQuery(hql);
			query.setParameter("templateId", templateId);
			Integer num = query.executeUpdate();
			getLog().info(" number row delete at " + templateId + " is " + num);

	}
}
