/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.EvaluationInputObjectDAO;
import vn.viettel.campaign.entities.EvaluationInputObject;

/**
 *
 * @author SON
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class EvaluationInputObjectDAOImpl extends BaseDAOImpl<EvaluationInputObject> implements EvaluationInputObjectDAO {

    @Override
    public List<EvaluationInputObject> getLstEvaluationInputObject() {
        String sql = "SELECT OBJECT_ID as objectId,"
                + " OBJECT_NAME as objectName,"
                + " OBJECT_TYPE as objectType,"
                + " OBJECT_DATA_TYPE as objectDataType,"
                + " OBJECT_PARENT_ID as objectParentId,"
                + " DESCRIPTION as description"
                + " FROM evaluation_input_object";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("objectId", StandardBasicTypes.LONG);
        query.addScalar("objectName", StandardBasicTypes.STRING);
        query.addScalar("objectType", StandardBasicTypes.INTEGER);
        query.addScalar("objectDataType", StandardBasicTypes.INTEGER);
        query.addScalar("objectParentId", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(EvaluationInputObject.class));

        return query.list();
    }

    @Override
    public List<EvaluationInputObject> getLstEvaluationInputObjectByParentId(Long parentId) {
        String sql = "SELECT OBJECT_ID as objectId,"
                + " OBJECT_NAME as objectName,"
                + " OBJECT_TYPE as objectType,"
                + " OBJECT_DATA_TYPE as objectDataType,"
                + " OBJECT_PARENT_ID as objectParentId,"
                + " DESCRIPTION as description"
                + " FROM evaluation_input_object"
                + " WHERE OBJECT_PARENT_ID=:parentId";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("objectId", StandardBasicTypes.LONG);
        query.addScalar("objectName", StandardBasicTypes.STRING);
        query.addScalar("objectType", StandardBasicTypes.INTEGER);
        query.addScalar("objectDataType", StandardBasicTypes.INTEGER);
        query.addScalar("objectParentId", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(EvaluationInputObject.class));

        query.setParameter("parentId", parentId);

        return query.list();
    }

    @Override
    public void onSaveOrUpdateEvaluationInputObject(EvaluationInputObject evaluationInputObject) {
        getSession().saveOrUpdate(evaluationInputObject);
    }

    @Override
    public void onDeleteEvaluationInputObject(EvaluationInputObject evaluationInputObject) {
        getSession().delete(evaluationInputObject);
    }

    @Override
    public EvaluationInputObject findOneByName(String name) {
        String sql = "SELECT OBJECT_ID as objectId,"
                + " OBJECT_NAME as objectName,"
                + " OBJECT_TYPE as objectType,"
                + " OBJECT_DATA_TYPE as objectDataType,"
                + " OBJECT_PARENT_ID as objectParentId,"
                + " DESCRIPTION as description"
                + " FROM evaluation_input_object"
                + " WHERE OBJECT_NAME=:name";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("objectId", StandardBasicTypes.LONG);
        query.addScalar("objectName", StandardBasicTypes.STRING);
        query.addScalar("objectType", StandardBasicTypes.INTEGER);
        query.addScalar("objectDataType", StandardBasicTypes.INTEGER);
        query.addScalar("objectParentId", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(EvaluationInputObject.class));

        query.setParameter("name", name);

        return (EvaluationInputObject) query.uniqueResult();
    }

    @Override
    public EvaluationInputObject findOneById(Long id) {
        String sql = "SELECT OBJECT_ID as objectId,"
                + " OBJECT_NAME as objectName,"
                + " OBJECT_TYPE as objectType,"
                + " OBJECT_DATA_TYPE as objectDataType,"
                + " OBJECT_PARENT_ID as objectParentId,"
                + " DESCRIPTION as description"
                + " FROM evaluation_input_object"
                + " WHERE OBJECT_ID=:id";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("objectId", StandardBasicTypes.LONG);
        query.addScalar("objectName", StandardBasicTypes.STRING);
        query.addScalar("objectType", StandardBasicTypes.INTEGER);
        query.addScalar("objectDataType", StandardBasicTypes.INTEGER);
        query.addScalar("objectParentId", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(EvaluationInputObject.class));

        query.setParameter("id", id);

        return (EvaluationInputObject) query.uniqueResult();
    }

    @Override
    public List<EvaluationInputObject> getLstEvaluationInputObjectHaveParentIdIsNull() {
        String sql = "SELECT OBJECT_ID as objectId,"
                + " OBJECT_NAME as objectName,"
                + " OBJECT_TYPE as objectType,"
                + " OBJECT_DATA_TYPE as objectDataType,"
                + " OBJECT_PARENT_ID as objectParentId,"
                + " DESCRIPTION as description"
                + " FROM evaluation_input_object"
                + " WHERE OBJECT_PARENT_ID IS NULL";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("objectId", StandardBasicTypes.LONG);
        query.addScalar("objectName", StandardBasicTypes.STRING);
        query.addScalar("objectType", StandardBasicTypes.INTEGER);
        query.addScalar("objectDataType", StandardBasicTypes.INTEGER);
        query.addScalar("objectParentId", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(EvaluationInputObject.class));

        return query.list();
    }

    @Override
    public List<EvaluationInputObject> getListCondition(Long id) {
        String sql = "SELECT OBJECT_ID as objectId,"
                + " OBJECT_NAME as objectName,"
                + " OBJECT_TYPE as objectType,"
                + " OBJECT_DATA_TYPE as objectDataType,"
                + " OBJECT_PARENT_ID as objectParentId,"
                + " DESCRIPTION as description"
                + " FROM evaluation_input_object"
                + " WHERE OBJECT_PARENT_ID = :id"
                + " AND OBJECT_DATA_TYPE != 1";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("objectId", StandardBasicTypes.LONG);
        query.addScalar("objectName", StandardBasicTypes.STRING);
        query.addScalar("objectType", StandardBasicTypes.INTEGER);
        query.addScalar("objectDataType", StandardBasicTypes.INTEGER);
        query.addScalar("objectParentId", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.setParameter("id", id);
        query.setResultTransformer(Transformers.aliasToBean(EvaluationInputObject.class));
        return query.list();
    }

}
