package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.ConditionTableDAOInterface;
import vn.viettel.campaign.dao.NotifyStatisticMapDAOInterface;
import vn.viettel.campaign.entities.ConditionTable;
import vn.viettel.campaign.entities.NotifyStatisticMap;
import vn.viettel.campaign.entities.StatisticCycle;

import java.util.ArrayList;
import java.util.List;

/**
 * @author truongbx
 * @date 09/01/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class NotifyStatisticMapDAO extends BaseDAOImpl<NotifyStatisticMap> implements NotifyStatisticMapDAOInterface {

    @Override
    public void deleteByNotifyTriggerId(long id) {
        String sql = "delete from notify_statistic_map where notify_id = :id";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.setParameter("id", id).executeUpdate();
    }

    @Override
    public List<NotifyStatisticMap> findByNotifyIdAndCycleId(long notifyId, long cycleId) {
        try {
            final String sql = "from NotifyStatisticMap where notifyId = :notifyId and statisticCycleId = :cycleId ";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameter("notifyId", notifyId);
            query.setParameter("cycleId", cycleId);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
//            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }
}
