package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.PreProcessParamMapDAOInterface;
import vn.viettel.campaign.entities.PreProcessParamMap;

import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.transaction.jta.TransactionFactory;

/**
 *
 */
@Repository
public class PreProcessParamMapDAO extends BaseDAOImpl<PreProcessParamMap> implements PreProcessParamMapDAOInterface {
	@Override
	public void deletePreProcessParamMapByPPUid(Session session, Long ppuid) {
		String hql = "DELETE  " +
				"FROM " +
				"process_param  " +
				"WHERE " +
				"PROCESS_PARAM_ID IN ( " +
				"SELECT a.PROCESS_PARAM_ID " +
				"FROM pre_process_param_map a " +
				"WHERE a.PRE_PROCESS_ID =:ppuid )";
		SQLQuery deleteProcessParam = session.createSQLQuery(hql);
		deleteProcessParam.setParameter("ppuid", ppuid);
		Integer num = deleteProcessParam.executeUpdate();
		getLog().info(" number row delete at " + ppuid + " is " + num);

		String hql1 = "delete from PreProcessParamMap where preProcessId  =:ppuid";
		Query query = session.createQuery(hql1);
		query.setParameter("ppuid", ppuid);
		int num1 = query.executeUpdate();
		getLog().info("delete Invitation by campaign " + ppuid + " num : " + num1);
	}
}
