package vn.viettel.campaign.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.BacklistDao;
import static vn.viettel.campaign.dao.impl.BaseDAOImpl.getLog;
import vn.viettel.campaign.entities.BlackList;

@Repository
@Transactional(rollbackFor = Exception.class)
public class BacklistDaoImpl extends BaseDAOImpl<BlackList> implements BacklistDao, Serializable {

    @Override
    public List<BlackList> findBacklistByCategory(final List<Long> categoryIds) {
        try {
            final String sql = "from BlackList where categoryId in (:categoryIds)";
            Query query = getSession().createQuery(sql);
            query.setParameterList("categoryIds", categoryIds);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public List<BlackList> findBacklistByName(String backlistName) {
        try {
            final String sql = "from BlackList where name = :backlistName";
            Query query = getSession().createQuery(sql);
            query.setParameter("backlistName", backlistName);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public boolean checkDuplicate(final String name, final Long id) {
        try {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("from BlackList where name = :name ");
            if (Objects.nonNull(id)) {
                sqlQuery.append("and blacklistId not in :blacklistId ");
            }
            Query query = getSession().createQuery(sqlQuery.toString());
            query.setParameter("name", name);
            if (Objects.nonNull(id)) {
                query.setParameter("blacklistId", id);
            }
            query.setCacheable(true);
            if (query.list() != null && !query.list().isEmpty()) {
                return false;
            }
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
            return false;
        }
        return true;
    }

    @Override
    public boolean checkDuplicatePath(final String path, final Long id) {
        try {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("from BlackList where (path = :path or tempPath = :tempPath or histPath = :histPath or failedPath = :failedPath) ");
            if (Objects.nonNull(id)) {
                sqlQuery.append("and blacklistId not in (:blacklistId) ");
            }
            Query query = getSession().createQuery(sqlQuery.toString());
            query.setParameter("path", path);
            query.setParameter("tempPath", path);
            query.setParameter("histPath", path);
            query.setParameter("failedPath", path);
            if (Objects.nonNull(id)) {
                query.setParameter("blacklistId", id);
            }
            query.setCacheable(true);
            if (query.list() != null && !query.list().isEmpty()) {
                return false;
            }
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
            return false;
        }
        return true;
    }

    @Override
    public long onSaveBlacklist(BlackList blacklist) {
        long result = -999;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            result = saveAndGetId(session, blacklist);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
}
