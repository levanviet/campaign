package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.FunctionDAOInterface;
import vn.viettel.campaign.dao.FunctionParamDAOInterface;
import vn.viettel.campaign.entities.ColumnCt;
import vn.viettel.campaign.entities.Function;
import vn.viettel.campaign.entities.FunctionParam;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class FunctionParamDAO extends BaseDAOImpl<FunctionParam> implements FunctionParamDAOInterface {
	@Override
	public List<FunctionParam> getListFunctionParam(long functionId) {
		String sql = "SELECT " +
				" b.FUNCTION_PARAM_ID functionParamId ," +
				" b.FUNCTION_PARAM_NAME functionParamName, " +
				" b.VALUE_TYPE valueType, " +
				" a.FUNCTION_PARAM_INDEX functionParamIndex, " +
				" a.FUNCTION_ID functionId  " +
				"FROM " +
				" function_container a " +
				" INNER JOIN function_param b  " +
				"WHERE " +
				" a.FUNCTION_PARAM_ID = b.FUNCTION_PARAM_ID  " +
				" AND a.FUNCTION_ID =:functionId" +
				" order by functionParamIndex asc ";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("functionParamId", StandardBasicTypes.LONG);
		query.addScalar("functionParamName", StandardBasicTypes.STRING);
		query.addScalar("valueType", StandardBasicTypes.LONG);
		query.addScalar("functionParamIndex", StandardBasicTypes.LONG);
		query.addScalar("functionId", StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(FunctionParam.class));
		query.setParameter("functionId", functionId);
		return query.list();
	}
}
