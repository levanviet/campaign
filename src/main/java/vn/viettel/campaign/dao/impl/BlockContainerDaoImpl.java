package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.BlockContainerDao;
import vn.viettel.campaign.entities.BlockContainer;

@Repository
public class BlockContainerDaoImpl extends BaseDAOImpl<BlockContainer> implements BlockContainerDao {

}
