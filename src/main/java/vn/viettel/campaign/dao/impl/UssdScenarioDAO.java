package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.UssdScenarioDAOInterface;
import vn.viettel.campaign.entities.UssdScenario;

import java.math.BigInteger;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class UssdScenarioDAO extends BaseDAOImpl<UssdScenario> implements UssdScenarioDAOInterface {
    @Override
    public synchronized long getNextSequense(String tableName) {
        String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = :tableName";
        getSession().createSQLQuery(update).setParameter("tableName", tableName).executeUpdate();
        String sql = "SELECT table_id  from SEQ_TABLE WHERE TABLE_NAME = :tableName";
        System.out.println("++++++++++++++++++++++++++++++++++++++" + tableName);
        return ((BigInteger) getSession().createSQLQuery(sql).setParameter("tableName", tableName).uniqueResult()).longValue();

    }

    @Override
    public long checkDelete(long id) {
        String sql = "select COUNT(*) from scenario_trigger where USSD_SCENARIO_ID = :id";
        return ((BigInteger) getSession().createSQLQuery(sql).setParameter("id", id).uniqueResult()).longValue();
    }

    @Override
    public UssdScenario getByName(long id, String name) {
        String sql = "from UssdScenario where scenarioName = :scenarioName and scenarioId != :scenarioId";
        List<UssdScenario> lst = getSession().createQuery(sql).setParameter("scenarioName", name).setParameter("scenarioId", id).list();
        return !DataUtil.isNullOrEmpty(lst) ? lst.get(0) : null;
    }
}
