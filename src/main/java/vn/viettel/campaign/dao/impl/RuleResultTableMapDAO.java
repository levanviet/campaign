package vn.viettel.campaign.dao.impl;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.RuleResultTableMapInterface;
import vn.viettel.campaign.entities.RuleResultTableMap;

import javax.persistence.Query;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class RuleResultTableMapDAO extends BaseDAOImpl<RuleResultTableMap> implements RuleResultTableMapInterface {
	@Override
	public void deleteResultTableMap(Session session, Long ruleId) {
		String hql = "delete from RuleResultTableMap where ruleId =:ruleId";
		Query query = session.createQuery(hql);
		query.setParameter("ruleId", ruleId);
		Integer num = query.executeUpdate();
		getLog().info(" number row delete at " + ruleId + " is " + num);
	}
}
