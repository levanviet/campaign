package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.NotifyValuesDAOInterface;
import vn.viettel.campaign.dao.StatisticCycleDAOInterface;
import vn.viettel.campaign.entities.NotifyValues;
import vn.viettel.campaign.entities.StatisticCycle;

import java.util.List;

/**
 * @author truongbx
 * @date 09/01/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class NotifyValuesDAO extends BaseDAOImpl<NotifyValues> implements NotifyValuesDAOInterface {

    @Override
    public List<NotifyValues> findByNotifyTriggerId(long id) {
        String sql = "SELECT\n" +
                "\tnotify_values.NOTIFY_VALUE_ID as notifyValueId,\n" +
                "\tnotify_values.TRIGGER_NOTIFY_ID as triggerNotifyId,\n" +
                "\tnotify_values.TEMPLATE_PARAM_ID as templateParamId,\n" +
                "\tnotify_values.SOURCE_TYPE as sourceType,\n" +
                "\tnotify_values.DATA_TYPE as dataType,\n" +
                "\tnotify_values.VALUE as value,\n" +
                "\tnotify_values.PARAM_STRING as paramString \n" +
                "FROM\n" +
                "\tnotify_values \n" +
                "WHERE\n" +
                "\ttrigger_notify_id = :id";

        SQLQuery query = getSession().createSQLQuery(sql);
        query.setParameter("id", id);
        query.addScalar("notifyValueId", StandardBasicTypes.LONG);
        query.addScalar("triggerNotifyId", StandardBasicTypes.LONG);
        query.addScalar("templateParamId", StandardBasicTypes.LONG);
        query.addScalar("sourceType", StandardBasicTypes.LONG);
        query.addScalar("dataType", StandardBasicTypes.LONG);
        query.addScalar("value", StandardBasicTypes.STRING);
        query.addScalar("paramString", StandardBasicTypes.STRING);
        query.setResultTransformer(Transformers.aliasToBean(NotifyValues.class));
        return query.list();
    }

    @Override
    public void deleteByNotifyTriggerId(long id) {
        String sql = "delete from notify_values where trigger_notify_id = :id";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.setParameter("id", id).executeUpdate();
    }
}
