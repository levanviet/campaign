/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.BaseAbstract;
import vn.viettel.campaign.dao.ResultDao;
import vn.viettel.campaign.entities.Result;
import vn.viettel.campaign.entities.RowCt;


@Repository
@Transactional(rollbackFor = Exception.class)
public class ResultDaoImpl extends BaseDAOImpl<Result> implements ResultDao {
    private static final Logger log = Logger.getLogger(ResultDaoImpl.class.getName());

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Result> getResultByRowIndexAndResultTableId(long rowIndex, long resultTableId) {
        String sql ="select result.RESULT_ID resultId, result.RESULT_TYPE resultType, result.STEP_INDEX stepIndex, result.PROMOTION_BLOCK_ID promotionBlockId\n" +
                    "from result_table_rule_result_map\n" +
                    "inner join rule_result\n" +
                    "inner join result\n" +
                    "where result_table_rule_result_map.rule_result_id = rule_result.rule_result_id\n" +
                    "and rule_result.result_id = result.result_id\n" +
                    "and result_table_rule_result_map.result_table_id= :resultTableId\n" +
                    "and rule_result.row_index = :rowIndex";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.addScalar("resultId", StandardBasicTypes.LONG);
        query.addScalar("resultType", StandardBasicTypes.INTEGER);
        query.addScalar("stepIndex", StandardBasicTypes.INTEGER);
        query.addScalar("promotionBlockId", StandardBasicTypes.BIG_INTEGER);
        query.setResultTransformer(Transformers.aliasToBean(Result.class));
        query.setParameter("rowIndex", rowIndex);
        query.setParameter("resultTableId", resultTableId);
        return query.list();     
    }

    @Override
    public List<Result> finAllResult() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Result> lstObj;
        lstObj = session.createQuery("from Result").list();
        return lstObj.size() > 0 ? lstObj : null;    
    }

    @Override
    public Object getSequence() {
        String sql ="SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = DATABASE() AND  TABLE_NAME = 'result'";
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        BigInteger  bigInteger = (BigInteger)query.uniqueResult();
        return query.getResultList().get(0);
    }

    @Override
    public Result updateOrSave(Result result) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(result);
        return result;
    } 

    @Override
    public Result getResultById(Long resultId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Result> lstObj;
        lstObj = session.createQuery("from Result where resultId = :resultId")
                .setParameter("resultId", resultId).list();
        return lstObj.size() > 0 ? lstObj.get(0) : null;
    }

    @Override
    public void deleteByResultTableId(long id) {
        String sql = "delete from result where result.RESULT_ID in \n" +
                "(\n" +
                "select rule_result.result_id\n" +
                "from result_table_rule_result_map\n" +
                "inner join rule_result\n" +
                "where result_table_rule_result_map.rule_result_id = rule_result.rule_result_id\n" +
                "and result_table_rule_result_map.result_table_id= :id\n" +
                ")";
        getSession().createSQLQuery(sql).setParameter("id", id).executeUpdate();
    }

    @Override
    public void deleteByDefaultResultId(long id) {
        String sql = "delete from result where result_id = :id";
        getSession().createSQLQuery(sql).setParameter("id", id).executeUpdate();
    }
}
