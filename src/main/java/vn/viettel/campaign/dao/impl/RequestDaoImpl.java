/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.RequestDao;
import vn.viettel.campaign.dto.RequestCountDTO;
import vn.viettel.campaign.dto.RequestDTO;
import vn.viettel.campaign.entities.Request;

@Repository
@Transactional(rollbackFor = Exception.class)
public class RequestDaoImpl extends BaseDAOImpl<Request> implements RequestDao {

    //HaBM2
    public static final String QUERY_REQUEST_BY_ID = "SELECT ID id,TRANSACTION_ID transactionId,FACTOR factor,OBJECT_ID objectId,"
            + " OBJECT_TYPE objectType,REQUEST_TYPE requestType,SERVICE_ID serviceId,ERROR_CODE errorCode,"
            + " ERROR_DESCRIPTION errorDescription,PERCENTAGE percentage,REQUEST_TIME requestTime,"
            + " FINISH_TIME finishTime,REQUEST_TIME requestTimeStr,FINISH_TIME finishTimeStr "
            + " FROM request";
    public static final String DELETE_REQUEST_BY_TRANSACTION = "DELETE FROM request WHERE TRANSACTION_ID = :TRANSACTIONID";

    private static final Logger log = Logger.getLogger(RequestDaoImpl.class.getName());

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Request> getAllRequest() {
        try {
            final String sql = "from Request order by id desc ";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            addParameters(query, null);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public RequestCountDTO countRequestUpdate(Long id) {
        RequestCountDTO countDTO = new RequestCountDTO();
        try {
            String sql = "from Request where 1=1 ";
            if (Objects.nonNull(id)) {
                sql += " and id > :id ";
            }
            sql += " order by id desc";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            if (Objects.nonNull(id)) {
                query.setParameter("id", id);
            }
            query.setCacheable(true);
            List<Request> requests = query.list();
            if (requests != null && !requests.isEmpty()) {
                countDTO.setId(requests.get(0).getId());
                countDTO.setCountRequest(Objects.nonNull(id) ? (long) requests.size() : 0);
            }
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return countDTO;
    }

    @Override
    public List<RequestDTO> getAllRequestScalar() {
        try {
            Map<String, Type> fieldMap = new HashMap<>();
            fieldMap.put("id", StandardBasicTypes.LONG);
            fieldMap.put("transactionId", StandardBasicTypes.STRING);
            fieldMap.put("factor", StandardBasicTypes.INTEGER);
            fieldMap.put("objectId", StandardBasicTypes.LONG);
            fieldMap.put("objectType", StandardBasicTypes.INTEGER);
            fieldMap.put("requestType", StandardBasicTypes.INTEGER);
            fieldMap.put("serviceId", StandardBasicTypes.INTEGER);
            fieldMap.put("errorCode", StandardBasicTypes.INTEGER);
            fieldMap.put("errorDescription", StandardBasicTypes.STRING);
            fieldMap.put("percentage", StandardBasicTypes.FLOAT);
            fieldMap.put("requestTime", StandardBasicTypes.DATE);
            fieldMap.put("finishTime", StandardBasicTypes.DATE);
            fieldMap.put("requestTimeStr", StandardBasicTypes.STRING);
            fieldMap.put("finishTimeStr", StandardBasicTypes.STRING);
            Session session = sessionFactory.getCurrentSession();
            SQLQuery query = session.createSQLQuery(QUERY_REQUEST_BY_ID);
            addScalar(query, fieldMap);
            query.setResultTransformer(Transformers.aliasToBean(RequestDTO.class));
            return query.list();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public RequestDTO getRequestById(final Long id) {
        try {
            String sql = "SELECT ID id,TRANSACTION_ID transactionId,FACTOR factor,OBJECT_ID objectId,"
                    + " OBJECT_TYPE objectType,REQUEST_TYPE requestType,SERVICE_ID serviceId,ERROR_CODE errorCode,"
                    + " ERROR_DESCRIPTION errorDescription,PERCENTAGE percentage,REQUEST_TIME requestTime,"
                    + " FINISH_TIME finishTime "
                    + " FROM request where ID = :idRs ";
            Map<String, Type> fieldMap = new HashMap<>();
            fieldMap.put("id", StandardBasicTypes.LONG);
            fieldMap.put("transactionId", StandardBasicTypes.STRING);
            fieldMap.put("factor", StandardBasicTypes.INTEGER);
            fieldMap.put("objectId", StandardBasicTypes.LONG);
            fieldMap.put("objectType", StandardBasicTypes.INTEGER);
            fieldMap.put("requestType", StandardBasicTypes.INTEGER);
            fieldMap.put("serviceId", StandardBasicTypes.INTEGER);
            fieldMap.put("errorCode", StandardBasicTypes.INTEGER);
            fieldMap.put("errorDescription", StandardBasicTypes.STRING);
            fieldMap.put("percentage", StandardBasicTypes.FLOAT);
            fieldMap.put("requestTime", StandardBasicTypes.DATE);
            fieldMap.put("finishTime", StandardBasicTypes.DATE);
            Session session = sessionFactory.getCurrentSession();
            SQLQuery query = session.createSQLQuery(sql);
            query.setParameter("idRs", id);
            addScalar(query, fieldMap);
            query.setResultTransformer(Transformers.aliasToBean(RequestDTO.class));
            if (query.list() != null && query.list().size() > 0) {
                return (RequestDTO) query.list().get(0);
            }
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new RequestDTO();
    }

    //HaBM2
    @Override
    public void deleteRequestByTransaction(String transactionId) {
        Session session = sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery(DELETE_REQUEST_BY_TRANSACTION);
        query.setParameter("TRANSACTIONID", transactionId);
        int result = query.executeUpdate();
        if (log.isDebugEnabled()) {
            log.debug("Delete creation request|Result = " + result + "|TransactionId = " + transactionId);
        }
    }
}
