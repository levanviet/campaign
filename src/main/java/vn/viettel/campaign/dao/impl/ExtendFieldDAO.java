package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.ExtendFieldDaoInterface;
import vn.viettel.campaign.dao.OCSTemplateDaoInterface;
import vn.viettel.campaign.entities.ExtendField;
import vn.viettel.campaign.entities.OcsBehaviourTemplate;
import vn.viettel.campaign.entities.Rule;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class ExtendFieldDAO extends BaseDAOImpl<ExtendField> implements ExtendFieldDaoInterface {
	@Override
	public ExtendField getNextSequense() {
		String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"EXTEND_FIELD\"";
		SQLQuery queryUpdate = getSession().createSQLQuery(update);
		queryUpdate.executeUpdate();
		String sql = "SELECT table_id id  from SEQ_TABLE WHERE TABLE_NAME = \"EXTEND_FIELD\"";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("id", StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(ExtendField.class));
		return (ExtendField) query.uniqueResult();
	}

	@Override
	public boolean checkExisExtendFieldName(String name) {
		List<Rule> lst = getSession().createQuery("from ExtendField where lower(name) = :name ")
				.setParameter("name", name.trim().toLowerCase())
				.list();
		if (lst != null && lst.size() > 0) {
			return true;
		}
		return false;
	}
}
