package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.InvitationDAOInterface;
import vn.viettel.campaign.entities.InvitationPriority;

import javax.persistence.Query;
import java.util.List;

/**
 * @author truongbx
 * @date 8/26/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class InvitationDAO extends BaseDAOImpl<InvitationPriority> implements InvitationDAOInterface {
	@Override
	public List<InvitationPriority> getNotifyTriggerByRuleId(long ruleId, String segmentName, long segmnentId) {
		String sql = " " +
				"SELECT DISTINCT  h.NOTIFY_TRIGGER_ID notifyId,h.TRIGGER_NAME triggerName,h.NOTIFY_TYPE notifyType,'" + segmentName + "' segmentName , '" + segmnentId + "' segmentId, '0' priority ,h.IS_INVITE invite " +
				"FROM rule_result_table_map a " +
				"INNER JOIN result_table b " +
				"INNER JOIN result_table_rule_result_map c " +
				"INNER JOIN rule_result d " +
				"INNER JOIN result e " +
				"INNER JOIN promotion_block f " +
				"INNER JOIN block_container g " +
				"INNER JOIN notify_trigger h " +
				"on a.RESULT_TABLE_ID = b.RESULT_TABLE_ID " +
				"AND b.RESULT_TABLE_ID = c.RESULT_TABLE_ID " +
				"  AND c.RULE_RESULT_ID =   d.RULE_RESULT_ID " +
				"  AND d.RESULT_ID = e.RESULT_ID " +
				"  AND e.PROMOTION_BLOCK_ID  = f.PROMOTION_BLOCK_ID  " +
				" AND f.PROMOTION_BLOCK_ID = g.PROMOTION_BLOCK_ID " +
				" AND g.PROM_ID = h.NOTIFY_TRIGGER_ID " +
				"  WHERE g.PROM_TYPE = '2'  " +
				"  AND a.rule_id =:ruleId  " +
				"  AND h.IS_INVITE = '1'  " +
				"UNION " +
				" " +
				"SELECT DISTINCT    h.NOTIFY_TRIGGER_ID notifyId,h.TRIGGER_NAME triggerName,h.NOTIFY_TYPE notifyType,'" + segmentName + "' segmentName  , '" + segmnentId + "' segmentId, '0' priority ,h.IS_INVITE invite  " +
				"FROM rule_result_table_map a " +
				"INNER JOIN result_table b " +
				"INNER JOIN result_table_rule_result_map c " +
				"INNER JOIN rule_result d " +
				"INNER JOIN result e " +
				"INNER JOIN promotion_block f " +
				"INNER JOIN block_container g " +
				"INNER JOIN ocs_behaviour o " +
				"INNER JOIN notify_trigger h " +
				"on a.RESULT_TABLE_ID = b.RESULT_TABLE_ID " +
				"AND b.RESULT_TABLE_ID = c.RESULT_TABLE_ID " +
				"  AND c.RULE_RESULT_ID =   d.RULE_RESULT_ID " +
				"  AND d.RESULT_ID = e.RESULT_ID " +
				"  AND e.PROMOTION_BLOCK_ID  = f.PROMOTION_BLOCK_ID  " +
				"\tAND f.PROMOTION_BLOCK_ID = g.PROMOTION_BLOCK_ID " +
				"\tAND g.PROM_ID = o.BEHAVIOUR_ID " +
				"\tAND o.NOTIFY_ID = h.NOTIFY_TRIGGER_ID " +
				"  WHERE g.PROM_TYPE = '1'  " +
				"  AND a. rule_id =:ruleId "+
				"  AND h.IS_INVITE = '1'  " +
				"UNION " +
				"SELECT DISTINCT    h.NOTIFY_TRIGGER_ID notifyId,h.TRIGGER_NAME triggerName,h.NOTIFY_TYPE notifyType,'" + segmentName + "' segmentName  , '" + segmnentId + "' segmentId, '0' priority ,h.IS_INVITE invite " +
				"FROM rule_result_table_map a " +
				"INNER JOIN result_table b " +
				"INNER JOIN result e " +
				"INNER JOIN promotion_block f " +
				"INNER JOIN block_container g " +
				"INNER JOIN notify_trigger h  " +
				"ON a.RESULT_TABLE_ID = b.RESULT_TABLE_ID " +
				"AND b.DEFAULT_RESULT_ID = e.RESULT_ID " +
				"AND e.PROMOTION_BLOCK_ID = f.PROMOTION_BLOCK_ID " +
				"AND f.PROMOTION_BLOCK_ID = g.PROMOTION_BLOCK_ID " +
				"AND g.PROM_ID = h.NOTIFY_TRIGGER_ID " +
				"WHERE g.PROM_TYPE = 2 " +
				"  AND a. rule_id =:ruleId "+
				"  AND h.IS_INVITE = '1'  " +
				"UNION " +
				"SELECT DISTINCT    h.NOTIFY_TRIGGER_ID notifyId,h.TRIGGER_NAME triggerName,h.NOTIFY_TYPE notifyType,'" + segmentName + "' segmentName  , '" + segmnentId + "' segmentId, '0' priority ,h.IS_INVITE invite " +
				"FROM rule_result_table_map a " +
				"INNER JOIN result_table b " +
				"INNER JOIN result e " +
				"INNER JOIN promotion_block f " +
				"INNER JOIN block_container g " +
				"INNER JOIN ocs_behaviour o " +
				"INNER JOIN notify_trigger h  " +
				"ON a.RESULT_TABLE_ID = b.RESULT_TABLE_ID " +
				"AND b.DEFAULT_RESULT_ID = e.RESULT_ID " +
				"AND e.PROMOTION_BLOCK_ID = f.PROMOTION_BLOCK_ID " +
				"AND f.PROMOTION_BLOCK_ID = g.PROMOTION_BLOCK_ID " +
				"AND g.PROM_ID = o.BEHAVIOUR_ID " +
				"AND o.NOTIFY_ID = h.NOTIFY_TRIGGER_ID " +
				"WHERE g.PROM_TYPE = 1 " +
				"  AND a. rule_id =:ruleId "+
				"  AND h.IS_INVITE = '1'  " ;

		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("notifyId", StandardBasicTypes.LONG);
		query.addScalar("segmentId", StandardBasicTypes.LONG);
		query.addScalar("priority", StandardBasicTypes.LONG);
		query.addScalar("notifyType", StandardBasicTypes.LONG);
		query.addScalar("invite", StandardBasicTypes.LONG);
		query.addScalar("triggerName", StandardBasicTypes.STRING);
		query.addScalar("segmentName", StandardBasicTypes.STRING);
		query.setResultTransformer(Transformers.aliasToBean(InvitationPriority.class));
		query.setParameter("ruleId", ruleId);
		return query.list();
	}

	@Override
	public List<InvitationPriority> getInvitationByCampaignId(long campaignId) {
		String sql = "SELECT a.NOTIFY_ID notifyId , a.PRIORITY priority, a.NOTIFY_TYPE notifyType, b.TRIGGER_NAME triggerName, c.SEGMENT_NAME segmentName  , a.SEGMENT_ID segmentId ,b.IS_INVITE invite " +
				"FROM invitation_priority a  " +
				"INNER JOIN notify_trigger b  " +
				"INNER JOIN segment c  " +
				"on a.NOTIFY_ID = b.NOTIFY_TRIGGER_ID  " +
				"AND a.SEGMENT_ID = c.SEGMENT_ID  " +
				"WHERE a.CAMPAIGN_ID =:campaignId" +
				" AND b.IS_INVITE= '1'";

		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("notifyId", StandardBasicTypes.LONG);
		query.addScalar("segmentId", StandardBasicTypes.LONG);
		query.addScalar("priority", StandardBasicTypes.LONG);
		query.addScalar("notifyType", StandardBasicTypes.LONG);
		query.addScalar("invite", StandardBasicTypes.LONG);
		query.addScalar("triggerName", StandardBasicTypes.STRING);
		query.addScalar("segmentName", StandardBasicTypes.STRING);
		query.setResultTransformer(Transformers.aliasToBean(InvitationPriority.class));
		query.setParameter("campaignId", campaignId);
		return query.list();
	}

	@Override
	public List<InvitationPriority> getInvitationForDefaultCampaign(long campaignId) {
		String sql = "SELECT a.NOTIFY_ID notifyId , a.PRIORITY priority, a.NOTIFY_TYPE notifyType, b.TRIGGER_NAME  triggerName, 'all' segmentName  , a.SEGMENT_ID segmentId ,b.IS_INVITE invite " +
				"FROM invitation_priority a  " +
				"INNER JOIN notify_trigger b  " +
				"on a.NOTIFY_ID = b.NOTIFY_TRIGGER_ID  " +
				"WHERE a.CAMPAIGN_ID =:campaignId" +
				" AND b.IS_INVITE= '1'";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("notifyId", StandardBasicTypes.LONG);
		query.addScalar("segmentId", StandardBasicTypes.LONG);
		query.addScalar("priority", StandardBasicTypes.LONG);
		query.addScalar("notifyType", StandardBasicTypes.LONG);
		query.addScalar("triggerName", StandardBasicTypes.STRING);
		query.addScalar("invite", StandardBasicTypes.LONG);
		query.addScalar("segmentName", StandardBasicTypes.STRING);
		query.setResultTransformer(Transformers.aliasToBean(InvitationPriority.class));
		query.setParameter("campaignId", campaignId);
		return query.list();
	}

	@Override
	public void deleteInviByCampaignId(Session session, Long campaignId) {
		String hql = "delete from InvitationPriority where campaignId  =:campaignId";
		Query query = session.createQuery(hql);
		query.setParameter("campaignId", campaignId);
		int num = query.executeUpdate();
		getLog().info("delete Invitation by campaign " + campaignId + " num : " + num);
	}
}
