package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.ProcessValueDAOInterface;
import vn.viettel.campaign.dao.RowCtDAOInterface;
import vn.viettel.campaign.entities.ColumnCt;
import vn.viettel.campaign.entities.ProcessValue;
import vn.viettel.campaign.entities.RowCt;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Repository
public class ProcessValueDAO extends BaseDAOImpl<ProcessValue> implements ProcessValueDAOInterface {

    @Override
    public ProcessValue findByValueId(long valueId) {
        String sql = "from ProcessValue where valueId = :valueId";
        return getSession().createQuery(sql, ProcessValue.class).setParameter("valueId", valueId).uniqueResult();
    }

    @Override
    public List<ProcessValue> findByValueIds(List<Long> valueIds) {
        String sql = "from ProcessValue where valueId in (:valueIds)";
        return getSession().createQuery(sql, ProcessValue.class).setParameterList("valueIds", valueIds).list();
    }

    @Override
    public List<ProcessValue> findProcessValueForDefaulTable(Long ppuId) {

        String sql = "SELECT  b.PROCESS_VALUE_ID processValueId ,b.VALUE_ID valueId , b.VALUE_NAME valueName , b.VALUE_INDEX valueIndex, b.VALUE_COLOR valueColor, b.DESCRIPTION description "
                + "FROM  pre_process_value_map a "
                + "INNER JOIN  process_value b "
                + "WHERE a.PROCESS_VALUE_ID = b.PROCESS_VALUE_ID "
                + "AND a.PRE_PROCESS_ID =:ppuId "
                + "ORDER BY b.VALUE_INDEX asc ";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("processValueId", StandardBasicTypes.LONG);
        query.addScalar("valueId", StandardBasicTypes.LONG);
        query.addScalar("valueName", StandardBasicTypes.STRING);
        query.addScalar("valueIndex", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("valueColor", StandardBasicTypes.STRING);
        query.setResultTransformer(Transformers.aliasToBean(ProcessValue.class));
        query.setParameter("ppuId", ppuId);
        return query.list();
    }

    @Override
    public List<ProcessValue> findByValuePreProcessIds(List<Long> preProcessIds) {
        if (DataUtil.isNullOrEmpty(preProcessIds)) {
            return new ArrayList<>();
        }
        String sql = "SELECT\n"
                + "\tprocess_value.PROCESS_VALUE_ID processValueId,\n"
                + "\tprocess_value.VALUE_ID valueId,\n"
                + "\tprocess_value.VALUE_NAME valueName,\n"
                + "\tprocess_value.VALUE_INDEX valueIndex,\n"
                + "\tprocess_value.VALUE_COLOR valueColor,\n"
                + "\tprocess_value.DESCRIPTION description\n"
                + "FROM\n"
                + "\tpre_process_unit\n"
                + "\tINNER JOIN pre_process_value_map ON pre_process_unit.PRE_PROCESS_ID = pre_process_value_map.PRE_PROCESS_ID\n"
                + "\tINNER JOIN process_value ON pre_process_value_map.PROCESS_VALUE_ID = process_value.PROCESS_VALUE_ID \n"
                + "WHERE\n"
                + "\tpre_process_unit.PRE_PROCESS_ID in (:preProcessIds)";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("processValueId", StandardBasicTypes.LONG);
        query.addScalar("valueId", StandardBasicTypes.LONG);
        query.addScalar("valueName", StandardBasicTypes.STRING);
        query.addScalar("valueColor", StandardBasicTypes.STRING);
        query.addScalar("valueIndex", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.setResultTransformer(Transformers.aliasToBean(ProcessValue.class));
        query.setParameterList("preProcessIds", preProcessIds);
        return query.list();
    }

    @Override
    public List<ProcessValue> findProProcessValueByPre_process_id(Long pre_process_id) {
        String sql = "select"
                + " b.PROCESS_VALUE_ID as processValueId,"
                + " b.VALUE_INDEX as valueIndex,"
                + " b.DESCRIPTION as description,"
                + " b.VALUE_COLOR as valueColor,"
                + " b.VALUE_NAME as valueName,"
                + " b.VALUE_ID as valueId"
                + " from pre_process_value_map as a"
                + " inner join process_value as b on a.PROCESS_VALUE_ID = b.PROCESS_VALUE_ID"
                + " where a.PRE_PROCESS_ID=:pre_process_id";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("processValueId", StandardBasicTypes.LONG);
        query.addScalar("valueIndex", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("valueColor", StandardBasicTypes.STRING);
        query.addScalar("valueName", StandardBasicTypes.STRING);
        query.addScalar("valueId", StandardBasicTypes.LONG);

        query.setResultTransformer(Transformers.aliasToBean(ProcessValue.class));
        query.setParameter("pre_process_id", pre_process_id);
        return query.list();
    }

    @Override
    public List<ProcessValue> getListProcessValueByListId(List<Long> longs) {
        String sql = "select"
                + " a.PROCESS_VALUE_ID as processValueId,"
                + " a.VALUE_INDEX as valueIndex,"
                + " a.DESCRIPTION as description,"
                + " a.VALUE_COLOR as valueColor,"
                + " a.VALUE_NAME as valueName,"
                + " a.VALUE_ID as valueId"
                + " from process_value as a"
                + " where a.PROCESS_VALUE_ID in (:longs)";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("processValueId", StandardBasicTypes.LONG);
        query.addScalar("valueIndex", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("valueColor", StandardBasicTypes.STRING);
        query.addScalar("valueName", StandardBasicTypes.STRING);
        query.addScalar("valueId", StandardBasicTypes.LONG);

        query.setResultTransformer(Transformers.aliasToBean(ProcessValue.class));
        query.setParameterList("longs", longs);
        return query.list();
    }

    @Override
    public ProcessValue checkValueName(String name,Long preProcessId) {
        String sql = "select"
                + " a.PROCESS_VALUE_ID as processValueId,"
                + " a.VALUE_INDEX as valueIndex,"
                + " a.DESCRIPTION as description,"
                + " a.VALUE_COLOR as valueColor,"
                + " a.VALUE_NAME as valueName,"
                + " a.VALUE_ID as valueId"
                + " from process_value as a"
                + " inner join pre_process_value_map as b on a.PROCESS_VALUE_ID = b.PROCESS_VALUE_ID"
                + " inner join pre_process_unit as c on b.PRE_PROCESS_ID = c.PRE_PROCESS_ID"
                + " where a.VALUE_NAME =:name and c.PRE_PROCESS_ID=:preProcessId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("processValueId", StandardBasicTypes.LONG);
        query.addScalar("valueIndex", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("valueColor", StandardBasicTypes.STRING);
        query.addScalar("valueName", StandardBasicTypes.STRING);
        query.addScalar("valueId", StandardBasicTypes.LONG);

        query.setResultTransformer(Transformers.aliasToBean(ProcessValue.class));
        query.setParameter("name", name);
        query.setParameter("preProcessId", preProcessId);
        return (ProcessValue) query.uniqueResult();
    }

}
