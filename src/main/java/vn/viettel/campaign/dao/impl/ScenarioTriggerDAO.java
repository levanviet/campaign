package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.ConditionTableDAOInterface;
import vn.viettel.campaign.dao.ScenarioTriggerDAOInterface;
import vn.viettel.campaign.entities.ConditionTable;
import vn.viettel.campaign.entities.ScenarioTrigger;
import vn.viettel.campaign.entities.UssdScenario;

import java.util.List;

/**
 * @author truongbx
 * @date 09/01/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class ScenarioTriggerDAO extends BaseDAOImpl<ScenarioTrigger> implements ScenarioTriggerDAOInterface {

    @Override
    public ScenarioTrigger getByName(long id, String name) {
        String sql = "from ScenarioTrigger where triggerName = :triggerName and scenarioTriggerId != :scenarioTriggerId";
        List<ScenarioTrigger> lst = getSession().createQuery(sql).setParameter("triggerName", name).setParameter("scenarioTriggerId", id).list();
        return !DataUtil.isNullOrEmpty(lst) ? lst.get(0) : null;
    }
}
