package vn.viettel.campaign.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.ScenarioNodeDAOInterface;
import vn.viettel.campaign.entities.ScenarioNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class ScenarioNodeDAO extends BaseDAOImpl<ScenarioNode> implements ScenarioNodeDAOInterface {
    private static final Logger log = Logger.getLogger(NoteContentDAO.class.getName());
    @Override
    public List<ScenarioNode> findByScenarioId(long scenarioId) {
        try {
            final String sql = "from ScenarioNode where ussdScenarioId = :ussdScenarioId";
            Query query = getSession().createQuery(sql);
            query.setParameter("ussdScenarioId", scenarioId);
//            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public List<ScenarioNode> findByParentId(long parentId) {
        try {
            final String sql = "from ScenarioNode where parentId = :parentId";
            Query query = getSession().createQuery(sql);
            query.setParameter("parentId", parentId);
//            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public void deleteByScenarioId(long scenarioId) {
        try {
            String sql = "DELETE FROM scenario_node WHERE  USSD_SCENARIO_ID = :scenarioId";
            int i = getSession().createSQLQuery(sql).setParameter("scenarioId", scenarioId).executeUpdate();
            System.out.println(i);
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
    }
}
