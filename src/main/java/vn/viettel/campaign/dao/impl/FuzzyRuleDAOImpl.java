/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.FuzzyRuleDAOInterface;
import vn.viettel.campaign.entities.FuzzyRule;

/**
 *
 * @author ABC
 */
@Repository
public class FuzzyRuleDAOImpl extends BaseDAOImpl<FuzzyRule> implements FuzzyRuleDAOInterface {

    @Override
    public List<FuzzyRule> getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(Long campaignId, Long campaignEvaluationInfoId) {
        String sql = "select"
                + " a.FUZZY_RULE_ID as fuzzyRuleId,"
                + " a.OWNER_ID as ownerId,"
                + " a.CAMPAIGN_EVALUATION_INFO_ID as campaignEvaluationInfoId,"
                + " a.CLASS_ID as classId,"
                + " a.RULE_LEVEL as ruleLevel,"
                + " a.WEIGHT as weight,"
                + " a.DISPLAY_EXPRESSION as displayExpression,"
                + " a.EXPRESSION as expression,"
                + " a.FUZZY_OPERATOR as fuzzyOperator,"
                + " a.DESCRIPTION as description,"
                + " b.CLASS_NAME as className"
                + " from fuzzy_rule as a"
                + " inner join result_class as b on a.CLASS_ID=b.CLASS_ID"
                + " where a.RULE_LEVEL=3 and b.OWNER_ID=:campaignId and a.CAMPAIGN_EVALUATION_INFO_ID=:campaignEvaluationInfoId";

        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("fuzzyRuleId", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("campaignEvaluationInfoId", StandardBasicTypes.LONG);
        query.addScalar("classId", StandardBasicTypes.LONG);
        query.addScalar("ruleLevel", StandardBasicTypes.LONG);
        query.addScalar("weight", StandardBasicTypes.DOUBLE);
        query.addScalar("displayExpression", StandardBasicTypes.STRING);
        query.addScalar("expression", StandardBasicTypes.STRING);
        query.addScalar("fuzzyOperator", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("className", StandardBasicTypes.STRING);
        query.setResultTransformer(Transformers.aliasToBean(FuzzyRule.class));

        query.setParameter("campaignId", campaignId);
        query.setParameter("campaignEvaluationInfoId", campaignEvaluationInfoId);
        return query.list();
    }

    @Override
    public FuzzyRule getNextSequenceFuzzyRule() {
        String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"fuzzy_rule\"";
        SQLQuery queryUpdate = getSession().createSQLQuery(update);
        queryUpdate.executeUpdate();
        String sql = "SELECT table_id fuzzyRuleId from SEQ_TABLE WHERE TABLE_NAME = \"fuzzy_rule\"";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("fuzzyRuleId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(FuzzyRule.class));
        return (FuzzyRule) query.uniqueResult();
    }

    @Override
    public FuzzyRule doCreateOrUpdateFuzzyRule(FuzzyRule fuzzyRule) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            doSaveOrUpdate(fuzzyRule);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return fuzzyRule;
    }

    @Override
    public boolean deleteFuzzyRule(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            deleteById(FuzzyRule.class, id);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return false;
        } finally {
            session.close();
        }
        return true;
    }

    @Override
    public List<FuzzyRule> getListFuzzyRuleOfSegment(Long segmentId, Long campaignEvaluationInfoId) {
        String sql = "select"
                + " a.FUZZY_RULE_ID as fuzzyRuleId,"
                + " a.OWNER_ID as ownerId,"
                + " a.CAMPAIGN_EVALUATION_INFO_ID as campaignEvaluationInfoId,"
                + " a.CLASS_ID as classId,"
                + " a.RULE_LEVEL as ruleLevel,"
                + " a.WEIGHT as weight,"
                + " a.DISPLAY_EXPRESSION as displayExpression,"
                + " a.EXPRESSION as expression,"
                + " a.FUZZY_OPERATOR as fuzzyOperator,"
                + " a.DESCRIPTION as description,"
                + " b.CLASS_NAME as className"
                + " from fuzzy_rule as a"
                + " inner join result_class as b on a.CLASS_ID=b.CLASS_ID"
                + " where a.RULE_LEVEL=2 and a.OWNER_ID=:segmentId and a.CAMPAIGN_EVALUATION_INFO_ID=:campaignEvaluationInfoId";

        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("fuzzyRuleId", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("campaignEvaluationInfoId", StandardBasicTypes.LONG);
        query.addScalar("classId", StandardBasicTypes.LONG);
        query.addScalar("ruleLevel", StandardBasicTypes.LONG);
        query.addScalar("weight", StandardBasicTypes.DOUBLE);
        query.addScalar("displayExpression", StandardBasicTypes.STRING);
        query.addScalar("expression", StandardBasicTypes.STRING);
        query.addScalar("fuzzyOperator", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("className", StandardBasicTypes.STRING);
        query.setResultTransformer(Transformers.aliasToBean(FuzzyRule.class));

        query.setParameter("segmentId", segmentId);
        query.setParameter("campaignEvaluationInfoId", campaignEvaluationInfoId);
        return query.list();
    }

    @Override
    public List<FuzzyRule> getListFuzzyByRuleId(Long ruleId) {
        String sql = "select"
                + " a.FUZZY_RULE_ID as fuzzyRuleId,"
                + " a.OWNER_ID as ownerId,"
                + " a.CLASS_ID as classId,"
                + " a.RULE_LEVEL as ruleLevel,"
                + " a.WEIGHT as weight,"
                + " a.DISPLAY_EXPRESSION as displayExpression,"
                + " a.EXPRESSION as expression,"
                + " a.FUZZY_OPERATOR as fuzzyOperator,"
                + " a.DESCRIPTION as description,"
                + " b.CLASS_NAME as className"
                + " from fuzzy_rule as a"
                + " inner join result_class as b on a.CLASS_ID=b.CLASS_ID"
                + " where a.RULE_LEVEL=1 and a.OWNER_ID=:ruleId";

        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("fuzzyRuleId", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("classId", StandardBasicTypes.LONG);
        query.addScalar("ruleLevel", StandardBasicTypes.LONG);
        query.addScalar("weight", StandardBasicTypes.DOUBLE);
        query.addScalar("displayExpression", StandardBasicTypes.STRING);
        query.addScalar("expression", StandardBasicTypes.STRING);
        query.addScalar("fuzzyOperator", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("className", StandardBasicTypes.STRING);
        query.setParameter("ruleId", ruleId);
        query.setResultTransformer(Transformers.aliasToBean(FuzzyRule.class));
        return query.list();
    }

    @Override
    public List<FuzzyRule> getListFuzzyRuleByCampaignEvaluationInfoId(Long campaignEvaluationInfoId) {
        String sql = "select"
                + " a.FUZZY_RULE_ID as fuzzyRuleId,"
                + " a.OWNER_ID as ownerId,"
                + " a.CAMPAIGN_EVALUATION_INFO_ID as campaignEvaluationInfoId,"
                + " a.CLASS_ID as classId,"
                + " a.RULE_LEVEL as ruleLevel,"
                + " a.WEIGHT as weight,"
                + " a.DISPLAY_EXPRESSION as displayExpression,"
                + " a.EXPRESSION as expression,"
                + " a.FUZZY_OPERATOR as fuzzyOperator,"
                + " a.DESCRIPTION as description,"
                + " b.CLASS_NAME as className"
                + " from fuzzy_rule as a"
                + " inner join result_class as b on a.CLASS_ID=b.CLASS_ID"
                + " where a.RULE_LEVEL=3 and a.CAMPAIGN_EVALUATION_INFO_ID=:campaignEvaluationInfoId";

        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("fuzzyRuleId", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("campaignEvaluationInfoId", StandardBasicTypes.LONG);
        query.addScalar("classId", StandardBasicTypes.LONG);
        query.addScalar("ruleLevel", StandardBasicTypes.LONG);
        query.addScalar("weight", StandardBasicTypes.DOUBLE);
        query.addScalar("displayExpression", StandardBasicTypes.STRING);
        query.addScalar("expression", StandardBasicTypes.STRING);
        query.addScalar("fuzzyOperator", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("className", StandardBasicTypes.STRING);
        query.setResultTransformer(Transformers.aliasToBean(FuzzyRule.class));

        query.setParameter("campaignEvaluationInfoId", campaignEvaluationInfoId);
        return query.list();
    }

   

}
