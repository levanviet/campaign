/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.PreProcessParamMap;

/**
 *
 * @author admin
 */
public interface PreProcessParamMapDAO {
    void onSaveOrUpdatePreProcessParamMap(PreProcessParamMap preProcessParamMap); 
    void onDeletePreProcessParamMap(PreProcessParamMap preProcessParamMap); 
}
