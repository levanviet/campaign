/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dto;

/**
 *
 * @author Administrator
 */
public class ResultStringDTO {
    private Long typeValue;
    private String typeName;
    private Long promotionBlockId;
    private String promotionBlockName;
    private Long step;

    public Long getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(Long typeValue) {
        this.typeValue = typeValue;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Long getPromotionBlockId() {
        return promotionBlockId;
    }

    public void setPromotionBlockId(Long promotionBlockId) {
        this.promotionBlockId = promotionBlockId;
    }

    public String getPromotionBlockName() {
        return promotionBlockName;
    }

    public void setPromotionBlockName(String promotionBlockName) {
        this.promotionBlockName = promotionBlockName;
    }

    public Long getStep() {
        return step;
    }

    public void setStep(Long step) {
        this.step = step;
    }
 
}
