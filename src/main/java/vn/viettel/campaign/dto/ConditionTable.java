/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dto;

import java.util.List;
import vn.viettel.campaign.entities.EvaluationInputObject;

/**
 *
 * @author ADMIN
 */
public class ConditionTable {
    private Long objectId;
    private String objectName;
    private String value;
    private List<EvaluationInputObject> lstInputObject;
    private int objectDataType;

    public int getObjectDataType() {
        return objectDataType;
    }

    public void setObjectDataType(int objectDataType) {
        this.objectDataType = objectDataType;
    }

    public List<EvaluationInputObject> getLstInputObject() {
        return lstInputObject;
    }

    public void setLstInputObject(List<EvaluationInputObject> lstInputObject) {
        this.lstInputObject = lstInputObject;
    }
    public ConditionTable(Long objectId) {
        this.objectId = objectId;
    }

    public ConditionTable(Long objectId, String objectName) {
        this.objectId = objectId;
        this.objectName = objectName;
    }

    public ConditionTable(Long objectId, String objectName, String value) {
        this.objectId = objectId;
        this.objectName = objectName;
        this.value = value;
    }

    public ConditionTable() {
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
