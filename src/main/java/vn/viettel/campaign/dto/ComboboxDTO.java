/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dto;

import java.io.Serializable;

/**
 *
 * @author ConKC
 */
public class ComboboxDTO implements Serializable {

    private String value;
    private String name;

    public ComboboxDTO() {
    }

    public ComboboxDTO(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
