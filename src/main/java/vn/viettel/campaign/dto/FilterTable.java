package vn.viettel.campaign.dto;

import vn.viettel.campaign.entities.Function;
import vn.viettel.campaign.entities.FunctionParam;

import java.util.Date;
import java.util.List;

/**
 * @author truongbx
 * @date 9/26/2019
 */
public class FilterTable {
	long fieldId;
	String fieldName;
	String fieldValueString;
	Long fieldValueNumber;
	Double fieldValueDouble;
        
	Date fieldValueDate;
	Long fieldType;
	int fieldValueBoolean;
	long fieldZoneId;
	long fieldZoneMapId;
	List<FunctionParam> lstFunctionParams;
	List<Function> lstFunction;
	public FilterTable() {
		this.fieldType = 2L;
	}

	public long getFieldId() {
		return fieldId;
	}

	public void setFieldId(long fieldId) {
		this.fieldId = fieldId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldValueString() {
		return fieldValueString;
	}

	public void setFieldValueString(String fieldValueString) {
		this.fieldValueString = fieldValueString;
	}

	public Long getFieldValueNumber() {
		return fieldValueNumber;
	}

	public void setFieldValueNumber(Long fieldValueNumber) {
		this.fieldValueNumber = fieldValueNumber;
	}

	public Double getFieldValueDouble() {
		return fieldValueDouble;
	}

	public void setFieldValueDouble(Double fieldValueDouble) {
		this.fieldValueDouble = fieldValueDouble;
	}

	public Date getFieldValueDate() {
		return fieldValueDate;
	}

	public void setFieldValueDate(Date fieldValueDate) {
		this.fieldValueDate = fieldValueDate;
	}

	public Long getFieldType() {
		return fieldType;
	}

	public void setFieldType(Long fieldType) {
		this.fieldType = fieldType;
	}

	public int getFieldValueBoolean() {
		return fieldValueBoolean;
	}

	public void setFieldValueBoolean(int fieldValueBoolean) {
		this.fieldValueBoolean = fieldValueBoolean;
	}

	public long getFieldZoneId() {
		return fieldZoneId;
	}

	public void setFieldZoneId(long fieldZoneId) {
		this.fieldZoneId = fieldZoneId;
	}

	public long getFieldZoneMapId() {
		return fieldZoneMapId;
	}

	public void setFieldZoneMapId(long fieldZoneMapId) {
		this.fieldZoneMapId = fieldZoneMapId;
	}

	public List<FunctionParam> getLstFunctionParams() {
		return lstFunctionParams;
	}

	public void setLstFunctionParams(List<FunctionParam> lstFunctionParams) {
		this.lstFunctionParams = lstFunctionParams;
	}

	public List<Function> getLstFunction() {
		return lstFunction;
	}

	public void setLstFunction(List<Function> lstFunction) {
		this.lstFunction = lstFunction;
	}
	public void resetValue(){
		 fieldValueString = null;
		 fieldValueNumber = null;
		 fieldValueDouble= null;
		 fieldValueDate= null;
		 fieldValueBoolean = 0;
	}
}
