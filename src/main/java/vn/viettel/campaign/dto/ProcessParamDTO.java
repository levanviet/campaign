package vn.viettel.campaign.dto;

public class ProcessParamDTO implements Comparable< ProcessParamDTO >{
    private int index;
    private String legend;
    private String sunday;
    private String monday;
    private String tuesday;
    private String wednesday;
    private String thursday;
    private String friday;
    private String saturday;
    private String sundayColor;
    private String mondayColor;
    private String tuesdayColor;
    private String wednesdayColor;
    private String thursdayColor;
    private String fridayColor;
    private String saturdayColor;
    @Override
    public int compareTo(ProcessParamDTO o) {
        return this.getLegend().compareTo(o.getLegend());
    }
    public String getMondayColor() {
        return mondayColor;
    }

    public void setMondayColor(String mondayColor) {
        this.mondayColor = mondayColor;
    }

    public String getTuesdayColor() {
        return tuesdayColor;
    }

    public void setTuesdayColor(String tuesdayColor) {
        this.tuesdayColor = tuesdayColor;
    }

    public String getWednesdayColor() {
        return wednesdayColor;
    }

    public void setWednesdayColor(String wednesdayColor) {
        this.wednesdayColor = wednesdayColor;
    }

    public String getThursdayColor() {
        return thursdayColor;
    }

    public void setThursdayColor(String thursdayColor) {
        this.thursdayColor = thursdayColor;
    }

    public String getFridayColor() {
        return fridayColor;
    }

    public void setFridayColor(String fridayColor) {
        this.fridayColor = fridayColor;
    }

    public String getSaturdayColor() {
        return saturdayColor;
    }

    public void setSaturdayColor(String saturdayColor) {
        this.saturdayColor = saturdayColor;
    }

    public String getSundayColor() {
        return sundayColor;
    }

    public void setSundayColor(String sundayColor) {
        this.sundayColor = sundayColor;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getLegend() {
        return legend;
    }

    public void setLegend(String legend) {
        this.legend = legend;
    }

    public String getSunday() {
        return sunday;
    }

    public void setSunday(String sunday) {
        this.sunday = sunday;
    }

    public String getMonday() {
        return monday;
    }

    public void setMonday(String monday) {
        this.monday = monday;
    }

    public String getTuesday() {
        return tuesday;
    }

    public void setTuesday(String tuesday) {
        this.tuesday = tuesday;
    }

    public String getWednesday() {
        return wednesday;
    }

    public void setWednesday(String wednesday) {
        this.wednesday = wednesday;
    }

    public String getThursday() {
        return thursday;
    }

    public void setThursday(String thursday) {
        this.thursday = thursday;
    }

    public String getFriday() {
        return friday;
    }

    public void setFriday(String friday) {
        this.friday = friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public void setSaturday(String saturday) {
        this.saturday = saturday;
    }
}
