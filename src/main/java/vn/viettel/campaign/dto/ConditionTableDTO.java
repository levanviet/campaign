/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dto;


import vn.viettel.campaign.entities.ColumnCt;
import vn.viettel.campaign.entities.ConditionTable;
import vn.viettel.campaign.entities.ProcessValue;
import vn.viettel.campaign.entities.RowCt;

import javax.persistence.Transient;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConditionTableDTO {

    private List<RowCt> lstRowCt;
    private List<ColumnCt> lstColumnCt;
    private List<ProcessValue> lstProcessValue;
    private ConditionTable conditionTable;
    //
    private List<String> values;
    private List<String> valueIds;
    private List<String> valueColor;
    private Map<Long, String> mapsPpuId = new HashMap<>();
    private List<Long> preProcessIds;
    private String index;
    private String isDefault;
    private String combine;
    
    private String resultString;
    private Long resultType;
    private long promotionBlockId;
    private Integer stepIndex;

    @Transient
    private List<ResultTableDTO> lstResultTableDTO;

    public List<ResultTableDTO> getLstResultTableDTO() {
        return lstResultTableDTO;
    }

    public void setLstResultTableDTO(List<ResultTableDTO> lstResultTableDTO) {
        this.lstResultTableDTO = lstResultTableDTO;
    }

    public List<String> getValueColor() {
        return valueColor;
    }

    public void setValueColor(List<String> valueColor) {
        this.valueColor = valueColor;
    }

    public Map<Long, String> getMapsPpuId() {
        return mapsPpuId;
    }

    public void setMapsPpuId(Map<Long, String> mapsPpuId) {
        this.mapsPpuId = mapsPpuId;
    }

    public Long getResultType() {
        return resultType;
    }

    public void setResultType(Long resultType) {
        this.resultType = resultType;
    }

    public long getPromotionBlockId() {
        return promotionBlockId;
    }

    public void setPromotionBlockId(long promotionBlockId) {
        this.promotionBlockId = promotionBlockId;
    }

    public Integer getStepIndex() {
        return stepIndex;
    }

    public void setStepIndex(Integer stepIndex) {
        this.stepIndex = stepIndex;
    }

    public List<Long> getPreProcessIds() {
        return preProcessIds;
    }

    public void setPreProcessIds(List<Long> preProcessIds) {
        this.preProcessIds = preProcessIds;
    }

    public List<RowCt> getLstRowCt() {
        return lstRowCt;
    }

    public void setLstRowCt(List<RowCt> lstRowCt) {
        this.lstRowCt = lstRowCt;
    }

    public List<ColumnCt> getLstColumnCt() {
        return lstColumnCt;
    }

    public void setLstColumnCt(List<ColumnCt> lstColumnCt) {
        this.lstColumnCt = lstColumnCt;
    }

    public List<ProcessValue> getLstProcessValue() {
        return lstProcessValue;
    }

    public void setLstProcessValue(List<ProcessValue> lstProcessValue) {
        this.lstProcessValue = lstProcessValue;
    }

    public ConditionTable getConditionTable() {
        return conditionTable;
    }

    public void setConditionTable(ConditionTable conditionTable) {
        this.conditionTable = conditionTable;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public List<String> getValueIds() {
        return valueIds;
    }

    public void setValueIds(List<String> valueIds) {
        this.valueIds = valueIds;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getCombine() {
        return combine;
    }

    public void setCombine(String combine) {
        this.combine = combine;
    }

    public String getResultString() {
        return resultString;
    }

    public void setResultString(String resultString) {
        this.resultString = resultString;
    }
    
}
