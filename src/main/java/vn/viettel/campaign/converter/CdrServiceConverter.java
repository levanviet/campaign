package vn.viettel.campaign.converter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;
import vn.viettel.campaign.entities.CdrService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author truongbx
 * @date 9/2/2019
 */
@FacesConverter(value = "cdrServiceConverter")
public class CdrServiceConverter implements Converter {
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
		DualListModel<CdrService> model = (DualListModel<CdrService>) ((PickList) uiComponent).getValue();
		for (CdrService cdrService : model.getSource()) {
			if (cdrService.getCdrServiceId() == Long.parseLong(s)) {
				return cdrService;
			}
		}
		for (CdrService cdrService : model.getTarget()) {
			if (cdrService.getCdrServiceId() == Long.parseLong(s)) {
				return cdrService;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
		return ((CdrService) o).getCdrServiceId()+"";
	}
}
