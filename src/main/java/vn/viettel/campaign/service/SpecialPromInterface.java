package vn.viettel.campaign.service;


import vn.viettel.campaign.entities.SpecialProm;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface SpecialPromInterface extends BaseBusinessInterface<SpecialProm>{
}
