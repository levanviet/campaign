/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.EvaluationInputObject;

/**
 *
 * @author SON
 */
public interface EvaluationInputObjectService {
    List<EvaluationInputObject> getLstEvaluationInputObject();
    List<EvaluationInputObject> getLstEvaluationInputObjectHaveParentIdIsNull();
    List<EvaluationInputObject> getLstEvaluationInputObjectByParentId(Long parentId);
    EvaluationInputObject findOneByName(String name);
    EvaluationInputObject findOneById(Long id);
    EvaluationInputObject onSaveOrUpdateEvaluationInputObject(EvaluationInputObject evaluationInputObject);
    Boolean onDeleteEvaluationInputObject(EvaluationInputObject evaluationInputObject);
    List<EvaluationInputObject> getListCondition(Long id);
    
}
