/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import vn.viettel.campaign.entities.Language;
import vn.viettel.campaign.entities.OcsBehaviour;
import vn.viettel.campaign.entities.ScenarioNode;
import vn.viettel.campaign.entities.UssdScenario;

import java.util.List;

/**
 * @author ConKC
 */
public interface UssdScenarioService {
    public UssdScenario getUssdScenarioById(long id);
    public List<OcsBehaviour> getAllOcsBehaviour();
    public List<Language> getAllLanguage();
    public long getNextSequense(String tableName);
    public void doSaveUssd(UssdScenario ussdScenario);
    public void doDeleteUssd(long id);
    public boolean validateDeleteScenarioNode(long id);
    public boolean validateDeleteUssd(long id);
    public boolean validateAddUssd(long id, String name);
    public ScenarioNode findScenarioNodeById(long id);
    public List<ScenarioNode> findScenarioNodeByParId(long parId);
}
