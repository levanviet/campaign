/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.Objects;

public interface ObjectService {

    public List<Objects> getLstObjectByName(String name, Long objectId);

    public List<Objects> getLstObjectByCode(String code, Long objectId);
    
    public Boolean saveObject(Objects obj);
    
    public Boolean updateObject(Objects obj);
    
    public List<Objects> getLstObjects();
    
    public List<Objects> getLstObjectsByRole(Long[] roleId);

}
