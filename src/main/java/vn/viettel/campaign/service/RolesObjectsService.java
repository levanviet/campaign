/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.RoleObject;

public interface RolesObjectsService {

    public Boolean saveRoleObject(RoleObject ro);

    public Boolean updateRoleObject(RoleObject ro);

    public void deleteRoleObjectByRoleId(Long roleId);

    public void deleteRoleObjectByObjectId(Long objectId);

    List<RoleObject> findByRoleId(Long roleId);
}
