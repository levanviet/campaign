package vn.viettel.campaign.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.common.Responses;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dao.*;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.PreprocessUnitInterface;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Service
public class PreprocessUnitServiceImpl extends BaseBusinessImpl<PreProcessUnit, PreprocessUnitDaoInterface> implements PreprocessUnitInterface {

    @Autowired
    PreprocessUnitDaoInterface preprocessUnitDAO;
    @Autowired
    InputObjectDAOInterface inputObjectDAO;
    @Autowired
    FunctionDAOInterface functionDAO;
    @Autowired
    FunctionParamDAOInterface functionParamDAO;
    @Autowired
    ZoneDAOInterface zoneDAO;
    @Autowired
    ZoneMapDAOInterface zoneMapDAO;
    @Autowired
    ProcessValueDAOInterface processValueDAO;
    @Autowired
    ProcessParamDAOInterface processParamDAO;

    @PostConstruct
    public void setupService() {
        this.tdao = preprocessUnitDAO;
        this.modelClass = PreProcessUnit.class;
    }

    @Override
    public PreProcessUnit getNextSequense() {
        return preprocessUnitDAO.getNextSequense();
    }

    @Override
    public boolean checkExistPreProcessName(String preProcessName, Long preProcessId) {
        return preprocessUnitDAO.checkExistPreProcessName(preProcessName, preProcessId);
    }

    @Override
    public List<InputObject> getLstInputObject() {
        return inputObjectDAO.getAll(InputObject.class);
    }

    @Override
    public List<InputObject> getLstCondition(long inputObjectId) {
        return inputObjectDAO.getLstCondition(inputObjectId);
    }

    @Override
    public List<Function> getLstFunction() {
        return functionDAO.getListFunction();
    }

    @Override
    public List<Function> getLstFunctionTypeOne() {
        return functionDAO.getListFunctionTypeOne();
    }

    @Override
    public List<FunctionParam> getLstFunctionParam(Long functionId) {
        return functionParamDAO.getListFunctionParam(functionId);
    }

    @Override
    public List<Zone> getLstZone() {
        return zoneDAO.getAll(Zone.class);
    }

    @Override
    public List<ZoneMap> getLstZoneMap() {
        return zoneMapDAO.getAll(ZoneMap.class);
    }

    @Override
    public List<ProcessValue> getLstProcessValue(Long ppId) {
        return processValueDAO.findProcessValueForDefaulTable(ppId);
    }

    @Override
    public List<ProcessParam> getLstProcessParam(Long ppid) {
        return processParamDAO.findProcessParamByPreProcessIds(ppid);
    }

    @Override
    public boolean onSavePreprocessUnit(PreProcessUnit preProcessUnit, List<ProcessParam> lstProcessParams, List<ProcessValue> lstProcessValue) {
        if (!DataUtil.isNullOrEmpty(lstProcessParams) && Constants.PRE_PROCESS_UNIT_STRING != preProcessUnit.getPreProcessType()) {
            for (ProcessParam processParam : lstProcessParams) {
                DataUtil.trimObject(processParam);
            }
        }
        if (!DataUtil.isNullOrEmpty(lstProcessValue)) {
            for (ProcessValue processValue : lstProcessValue) {
                DataUtil.trimObject(processValue);
            }
        }
        DataUtil.trimObject(preProcessUnit);

        String result = preprocessUnitDAO.onSavePreprocessUnit(preProcessUnit, lstProcessParams, lstProcessValue);
        if (!result.equalsIgnoreCase(Responses.SUCCESS.getName())) {
            return false;
        }
        return true;
    }

    @Override
    public boolean onUpdatePreprocessUnit(PreProcessUnit preProcessUnit, List<ProcessParam> lstProcessParams, List<ProcessValue> lstProcessValue) {
        if (!DataUtil.isNullOrEmpty(lstProcessParams)) {
            for (ProcessParam processParam : lstProcessParams) {
                DataUtil.trimObject(processParam);
            }
        }
        if (!DataUtil.isNullOrEmpty(lstProcessValue)) {
            for (ProcessValue processValue : lstProcessValue) {
                DataUtil.trimObject(processValue);
            }
        }
        DataUtil.trimObject(preProcessUnit);
        String result = preprocessUnitDAO.onUpdatePreprocessUnit(preProcessUnit, lstProcessParams, lstProcessValue);
        if (!result.equalsIgnoreCase(Responses.SUCCESS.getName())) {
            return false;
        }
        return true;
    }

    @Override
    public boolean onDeletePreprocessUnit(Long preProcessId) {
        return preprocessUnitDAO.onDeletePreprocessUnit(preProcessId);
    }

    @Override
    public boolean checkDeletePreprocessUnit(Long preProcessId) {
        return preprocessUnitDAO.checkDeletePreprocessUnit(preProcessId);
    }

    @Override
    public PreProcessUnit findById(long id) {
        return super.findById(id); //To change body of generated methods, choose Tools | Templates.
    }

}
