/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.el.ELContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import vn.viettel.campaign.controller.CampaignOfflineController;
import vn.viettel.campaign.controller.MenuController;
import vn.viettel.campaign.dao.LoginDao;
import vn.viettel.campaign.entities.Objects;
import vn.viettel.campaign.entities.Roles;
import vn.viettel.campaign.entities.Users;

@Service
public class LoginServiceImpl implements UserDetailsService {

    @Autowired
    private LoginDao loginDaoImpl;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        vn.viettel.campaign.entities.Users user = loginDaoImpl.loadUserByUsername(username);
        if (user == null) {
            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            HttpSession session = attr.getRequest().getSession(true);
            session.setAttribute("loginFaile", "Username or password is wrong!");
            throw new UsernameNotFoundException("Username Not Found");
        }
        //get list  role
        List<Roles> lstRole = loginDaoImpl.getListRole(user.getUserId());
        //get list Object - menu
        List<Objects> lstObjects = loginDaoImpl.getLstObjects(user.getUserId());
        //set session
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = (HttpSession) attr.getRequest().getSession(true);

        session.setAttribute("userToken", user);
        session.setAttribute("lstRole", lstRole);
        session.setAttribute("lstMenu", lstObjects);
        session.setAttribute("doLogin", true);
        session.setAttribute("username", user.getUserName());
        

        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;

        return new User(user.getUserName(), user.getPassWord(), enabled, accountNonExpired, credentialsNonExpired,
                accountNonLocked, getAuthorities(lstRole));
    }

    public List<GrantedAuthority> getAuthorities(List<Roles> lstRole) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        lstRole.forEach((role) -> {
            authorities.add(new SimpleGrantedAuthority(role.getRoleCode()));
        });
        return authorities;
    }

    public void setSessionMenu() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = (HttpSession) attr.getRequest().getSession(true);
        if (session != null) {
            Users users = (Users) session.getAttribute("userToken");
            if (users != null && users.getUserId() != null) {
                List<Objects> lstObjects = loginDaoImpl.getLstObjects(users.getUserId());
                session.setAttribute("lstMenu", lstObjects);

                ELContext elContext = FacesContext.getCurrentInstance().getELContext();
                MenuController menuController = (MenuController) elContext.getELResolver().getValue(elContext, null, "menuController");
                menuController.resetMenu(lstObjects);
            }
        }
    }
}
