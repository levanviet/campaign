/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.RolesDao;
import vn.viettel.campaign.entities.Roles;
import vn.viettel.campaign.service.RolesService;

@Service
public class RolesServiceImpl implements RolesService {

    @Autowired
    private RolesDao rolesDaoImpl;

    @Override
    public List<Roles> getLstRolesByName(String name, Long objectId) {
        return rolesDaoImpl.getLstRolesByName(name, objectId);
    }

    @Override
    public List<Roles> getLstRolesByCode(String code, Long objectId) {
        return rolesDaoImpl.getLstRolesByCode(code, objectId);
    }

    @Override
    public Boolean saveRoles(Roles obj) {
        return rolesDaoImpl.saveRoles(obj);
    }

    @Override
    public Boolean updateRoles(Roles obj) {
        return rolesDaoImpl.updateRoles(obj);
    }
    
    @Override
    public List<Roles> getLstRoles(){
        return rolesDaoImpl.getLstRoles();
    }

    @Override
    public List<Long> getRolesByUserId(Long userId) {
        return rolesDaoImpl.getRolesByUserId(userId);
    }
  
    @Override
    public Roles getRolesById(Long roleId) {
        return rolesDaoImpl.getRolesById(roleId);
    }

}
