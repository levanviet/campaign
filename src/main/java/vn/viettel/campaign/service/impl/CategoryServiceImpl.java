/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.CategoryDao;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.CategoryService;

/**
 *
 * @author ConKC
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDao categoryDaoImpl;

    @Override
    public void save(Category category) {
        DataUtil.trimObject(category);
        categoryDaoImpl.save(category);
    }

    @Override
    public List<Category> getCategory() {
        return categoryDaoImpl.getCategory();
    }

    @Override
    public List<Category> getCategoryByType(final Integer categoryType, final boolean parentId) {
        return categoryDaoImpl.getCategoryByType(categoryType, parentId);
    }

    @Override
    public Category getCategoryById(Long categoryId) {
        return categoryDaoImpl.getCategoryById(categoryId);
    }

    @Override
    public List<Campaign> getObjectByCategory(final List<Long> categoryIds, final String objectName) {
        return categoryDaoImpl.getObjectByCategory(categoryIds, objectName);
    }

    @Override
    public List<Segment> getObjectByCategory(final List<Long> categoryIds) {
            return categoryDaoImpl.getObjectByCategory(categoryIds, Segment.class.getSimpleName());
    }
    @Override
    public List<ConditionTable> getConditionTableByCategory(final List<Long> categoryIds) {
            return categoryDaoImpl.getObjectByCategory(categoryIds, ConditionTable.class.getSimpleName());
    }
    @Override
    public List<PreProcessUnit> getPreProcessUnitByCategory(final List<Long> categoryIds) {
            return categoryDaoImpl.getObjectByCategory(categoryIds, PreProcessUnit.class.getSimpleName());
    }

    @Override
    public List<NotifyTemplate> getNotifyTemplateByCategory(List<Long> categoryIds) {
        return categoryDaoImpl.getObjectByCategory(categoryIds, NotifyTemplate.class.getSimpleName());
    }

    @Override
    public List<Rule> getRuleByCategory(final List<Long> categoryIds) {
            return categoryDaoImpl.getObjectByCategory(categoryIds, Rule.class.getSimpleName());
    }

    @Override
    public <T> List<T> getDataFromCatagoryId(List<Long> categoryIds, String objectName) {
            return categoryDaoImpl.getObjectByCategory(categoryIds,objectName);
    }


    @Override
    public List<UssdScenario> getUssdScenarioByCategory(final List<Long> categoryIds) {
            return categoryDaoImpl.getObjectByCategory(categoryIds, UssdScenario.class.getSimpleName());
    }

    @Override
    public List<ScenarioTrigger> getScenarioTriggerByCategory(List<Long> categoryIds) {
        return categoryDaoImpl.getObjectByCategory(categoryIds, ScenarioTrigger.class.getSimpleName());
    }

    @Override
    public List<NotifyTrigger> getNotifyTriggerByCategory(List<Long> categoryIds) {
        return categoryDaoImpl.getObjectByCategory(categoryIds, NotifyTrigger.class.getSimpleName());
    }

    @Override
    public Boolean deleteCategory(final Category category) {
        return categoryDaoImpl.deleteCategory(category);
    }

    @Override
    public boolean checkDuplicate(final String categoryName, final Long categoryId) {
        return categoryDaoImpl.checkDuplicate(categoryName, categoryId);
    }
    @Override
    public boolean checkDuplicate(final String categoryName, final Long categoryId, Integer categoryType) {
        return categoryDaoImpl.checkDuplicate(categoryName, categoryId, categoryType);
    }

    @Override
    public boolean checkDeleteCategory(final Long categoryId) {
        return categoryDaoImpl.checkDeleteCategory(categoryId);
    }

    @Override
    public boolean checkDeleteCategory(final Long categoryId, String className) {
        return categoryDaoImpl.checkDeleteCategory(categoryId, className);
    }

    @Override
    public List<Category> getCategoryByType(Integer categoryType) {
        return categoryDaoImpl.getCategoryByType(categoryType);
    }

    @Override
    public List<ResultTable> getResultTableByCategory(List<Long> categoryIds) {
        return categoryDaoImpl.getObjectByCategory(categoryIds, ResultTable.class.getSimpleName());
    }

    @Override
    public List<PromotionBlock> getPromotionBlockByCategory(List<Long> categoryIds) {
        return categoryDaoImpl.getObjectByCategory(categoryIds, PromotionBlock.class.getSimpleName());
    }

    @Override
    public List<Category> findCategoryByIds(List<Long> categoryIds) {
        return categoryDaoImpl.findCategoryByIds(categoryIds);
    }

    @Override
    public List<Category> findCategoryByTypes(List<Integer> types) {
        return categoryDaoImpl.findCategoryByTypes(types);
    }
}
