/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.PpuEvaluationDAO;
import vn.viettel.campaign.entities.PpuEvaluation;
import vn.viettel.campaign.entities.ProcessParam;
import vn.viettel.campaign.entities.ProcessValue;
import vn.viettel.campaign.service.PpuEvaluationService;

/**
 *
 * @author admin
 */
@Service
public class PpuEvaluationServiceImpl implements PpuEvaluationService{

    @Autowired
    private PpuEvaluationDAO ppuEvaluationDAO;
    
    @Override
    public PpuEvaluation onSaveOrUpdatePpuEvaluation(PpuEvaluation ppuEvaluation) {
        ppuEvaluationDAO.onSaveOrUpdatePpuEvaluation(ppuEvaluation);
        return ppuEvaluation;
    }

    @Override
    public PpuEvaluation getNextSequense() {
        return ppuEvaluationDAO.getNextSequense();
    }

    @Override
    public boolean onDeletePpuEvaluation(PpuEvaluation ppuEvaluation) {
        if(ppuEvaluation != null){
            ppuEvaluationDAO.onDeletePpuEvaluation(ppuEvaluation);
            return true;
        }
        return false;
    }

    @Override
    public List<ProcessValue> getListProcessValue(Long id) {
       return ppuEvaluationDAO.getListProcessValue(id);
    }

    @Override
    public List<ProcessParam> getListProcessParam(Long id) {
        return ppuEvaluationDAO.getListProcessParam(id);
    }

    @Override
    public boolean checkExitsProcessorName(String name, Long id) {
        if(ppuEvaluationDAO.checkExitsProcessorName(name, id).isEmpty()){
            return false;
        }
        return true;
    }

    @Override
    public boolean checkExitsCriteriaName(String name, Long id) {
        if(!ppuEvaluationDAO.checkExitsCriteriaName(name, id).isEmpty()){
            return true;
        }
        return false;
    }

    @Override
    public boolean checkExitsValueName(Long processValue, String name, Long id) {
        if(!ppuEvaluationDAO.checkExitsValueName(processValue,name, id).isEmpty()){
            return true;
        }
        return false;
    }

    @Override
    public boolean checkExitsValueId(Long processValue, Long valueId, Long id) {
         if(!ppuEvaluationDAO.checkExitsValueId(processValue,valueId, id).isEmpty()){
            return true;
        }
        return false;
    }

    @Override
    public boolean checkDeletePPUEvaluation(Long  id) {
        if(!ppuEvaluationDAO.checkDeletePPUEvaluation(id).isEmpty()){
            return true;
        }
        return false;
    }

    @Override
    public PpuEvaluation findOneById(Long id) {
        return ppuEvaluationDAO.findOneById(id);
    }
    
}
