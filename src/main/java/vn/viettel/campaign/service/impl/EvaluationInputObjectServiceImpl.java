/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.EvaluationInputObjectDAO;
import vn.viettel.campaign.entities.EvaluationInputObject;
import vn.viettel.campaign.service.EvaluationInputObjectService;

/**
 *
 * @author SON
 */
@Service
public class EvaluationInputObjectServiceImpl implements EvaluationInputObjectService {

    @Autowired
    private EvaluationInputObjectDAO evaluationInputObjectDAO;

    @Override
    public List<EvaluationInputObject> getLstEvaluationInputObject() {
        return evaluationInputObjectDAO.getLstEvaluationInputObject();
    }

    @Override
    public List<EvaluationInputObject> getLstEvaluationInputObjectByParentId(Long parentId) {
        return evaluationInputObjectDAO.getLstEvaluationInputObjectByParentId(parentId);
    }

    @Override
    public EvaluationInputObject findOneByName(String name) {
        return evaluationInputObjectDAO.findOneByName(name);
    }

    @Override
    public EvaluationInputObject findOneById(Long id) {
        return id == null ? new EvaluationInputObject() : evaluationInputObjectDAO.findOneById(id);
    }

    @Override
    public EvaluationInputObject onSaveOrUpdateEvaluationInputObject(EvaluationInputObject evaluationInputObject) {
        if (evaluationInputObject != null) {
            evaluationInputObjectDAO.onSaveOrUpdateEvaluationInputObject(evaluationInputObject);
            return evaluationInputObjectDAO.findOneById(evaluationInputObject.getObjectId());
        }
        return new EvaluationInputObject();
    }

    @Override
    public Boolean onDeleteEvaluationInputObject(EvaluationInputObject evaluationInputObject) {
        if (evaluationInputObject != null) {
            evaluationInputObjectDAO.onDeleteEvaluationInputObject(evaluationInputObject);
            return true;
        }
        return false;
    }

    @Override
    public List<EvaluationInputObject> getLstEvaluationInputObjectHaveParentIdIsNull() {
        return evaluationInputObjectDAO.getLstEvaluationInputObjectHaveParentIdIsNull();
    }

    @Override
    public List<EvaluationInputObject> getListCondition(Long id) {
        return evaluationInputObjectDAO.getListCondition(id);
    }

}
