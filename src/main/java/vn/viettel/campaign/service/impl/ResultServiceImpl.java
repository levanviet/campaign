/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.ResultDao;
import vn.viettel.campaign.dao.RolesDao;
import vn.viettel.campaign.entities.Result;
import vn.viettel.campaign.entities.RoleObject;
import vn.viettel.campaign.entities.RoleUser;
import vn.viettel.campaign.entities.Roles;
import vn.viettel.campaign.service.ResultService;

@Service
public class ResultServiceImpl implements ResultService {

    @Autowired
    private ResultDao resultDaoImpl;

    @Override
    public List<Result> finAllResult() {
        return resultDaoImpl.finAllResult();
    }

    @Override
    public List<Result> getResultByRowIndexAndResultTableId(long rowIndex, long resultTableId) {
        return resultDaoImpl.getResultByRowIndexAndResultTableId(rowIndex, resultTableId);
    }

    @Override
    public Object getSequence() {
        return resultDaoImpl.getSequence();
    }

    @Override
    public Result updateOrSave(Result result) {
        return resultDaoImpl.updateOrSave(result);
    }

    @Override
    public Result getResultById(Long resultId) {
        return resultDaoImpl.getResultById(resultId);
    }
}
