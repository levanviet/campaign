/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.CampaignDAOInterface;
import vn.viettel.campaign.dao.CampaignEvaluationInfoDAOInterface;
import vn.viettel.campaign.dao.CampaignScheduleDAOInterface;
import vn.viettel.campaign.dao.FuzzyRuleDAOInterface;
import vn.viettel.campaign.dao.ResultClassDAOInfterface;
import vn.viettel.campaign.dao.RuleEvaluationInfoDAOInterface;
import vn.viettel.campaign.dao.SegmentDao;
import vn.viettel.campaign.dao.SegmentEvaluationInfoDAOInterface;
import vn.viettel.campaign.dao.UssdScenarioDAOInterface;
import vn.viettel.campaign.entities.Campaign;
import vn.viettel.campaign.entities.CampaignEvaluationInfo;
import vn.viettel.campaign.entities.CampaignSchedule;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.ResultClass;
import vn.viettel.campaign.entities.RuleEvaluationInfo;
import vn.viettel.campaign.entities.Segment;
import vn.viettel.campaign.entities.SegmentEvaluationInfo;
import vn.viettel.campaign.service.CampaignEvaluationInforService;

/**
 *
 * @author ABC
 */
@Service
public class CampaignEvaluationInfoServiceIpml implements CampaignEvaluationInforService {

    @Autowired
    private CampaignEvaluationInfoDAOInterface campaignEvaluationInfoDAOInterface;
    @Autowired
    private RuleEvaluationInfoDAOInterface ruleEvaluationInfoDAOInterface;
    @Autowired
    private ResultClassDAOInfterface resultClassDAOInfterface;
    @Autowired
    private FuzzyRuleDAOInterface fuzzyRuleDAOInterface;
    @Autowired
    private CampaignScheduleDAOInterface campaignScheduleDAOInterface;
    @Autowired
    private SegmentEvaluationInfoDAOInterface segmentEvaluationInfoDAOInterface;
    @Autowired
    private SegmentDao segmentDAO;
    @Autowired
    private UssdScenarioDAOInterface ussdScenarioDAO;
    @Autowired
    private CampaignDAOInterface camapaignDAOInterface;
    

    @Override
    public List<CampaignEvaluationInfo> getListCampaginEvaluationInfoByCategoryIds(List<Long> categoryIds) {
        return campaignEvaluationInfoDAOInterface.getListCampaginEvaluationInfoByCategoryIds(categoryIds);
    }

    @Override
    public List<RuleEvaluationInfo> getListRuleEvaluationInfoByOwnerId(Long campaignEvaluationIfoId) {
        return ruleEvaluationInfoDAOInterface.getListRuleEvaluationInfoByOwnerId(campaignEvaluationIfoId);
    }

    @Override
    public List<ResultClass> getListResultClassByOwnerIdAndCampaignEvaluationInfoId(Long campaignId, Long campaignEvaluationInfoId) {
        return resultClassDAOInfterface.getListResultClassByOwnerIdAndCampaignEvaluationInfoId(campaignId, campaignEvaluationInfoId);
    }

    @Override
    public List<ResultClass> getListResultClassByAssessmentRuleId(Long assessmentRuleId) {
        return resultClassDAOInfterface.getListResultClassByAssessmentRuleId(assessmentRuleId);
    }

    @Override
    public List<FuzzyRule> getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(Long campaignId, Long campaignEvaluationInfoId) {
        return fuzzyRuleDAOInterface.getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(campaignId, campaignEvaluationInfoId);
    }

    @Override
    public CampaignSchedule getCampaignScheduleByScheduleId(Long id) {
        return campaignScheduleDAOInterface.getCampaignScheduleByScheduleId(id);
    }

    @Override
    public RuleEvaluationInfo getNextSequenceRuleEvaluation() {
        return ruleEvaluationInfoDAOInterface.getNextSequenceRuleEvaluation();
    }

    @Override
    public ResultClass getNextSequenceResultClass() {
        return resultClassDAOInfterface.getNextSequenceResultClass();
    }

    @Override
    public CampaignEvaluationInfo getNextSequenceCampaignEvaluationInfo() {
        return campaignEvaluationInfoDAOInterface.getNextSequenceCampaignEvaluationInfo();
    }

    @Override
    public CampaignSchedule getNextSequenceCampaignSchedule() {
        return campaignScheduleDAOInterface.getNextSequneceCampaignSchedule();
    }

    @Override
    public FuzzyRule getNextSequenceFuzzyRule() {
        return fuzzyRuleDAOInterface.getNextSequenceFuzzyRule();
    }

    @Override
    public ResultClass getNameByClassId(Long classId) {
        return resultClassDAOInfterface.getNameByClassId(classId);
    }

    @Override
    public CampaignSchedule doCreateOrUpdateCampaignSchedule(CampaignSchedule CampaignSchedule) {
        return campaignScheduleDAOInterface.doCreateOrUpdateCampaignSchedule(CampaignSchedule);
    }

    @Override
    public CampaignEvaluationInfo doCreateOrUpdateCampaignEvaluationInfo(CampaignEvaluationInfo campaignEvaluationInfo) {
        return campaignEvaluationInfoDAOInterface.doCreateOrUpdateCampaignEvaluationInfo(campaignEvaluationInfo);
    }

    @Override
    public ResultClass doCreateOrUpdateResultClass(ResultClass resultClass) {
        return resultClassDAOInfterface.doCreateOrUpdateResultClass(resultClass);
    }

    @Override
    public RuleEvaluationInfo doCreateOrUpdateRuleEvaluationInfo(RuleEvaluationInfo ruleEvaluationInfo) {
        return ruleEvaluationInfoDAOInterface.doCreateOrUpdateRuleEvaluationInfo(ruleEvaluationInfo);
    }

    @Override
    public FuzzyRule doCreateOrUpdateFuzzyRule(FuzzyRule fuzzyRule) {
        return fuzzyRuleDAOInterface.doCreateOrUpdateFuzzyRule(fuzzyRule);
    }

    @Override
    public boolean deleteResultClass(Long id) {
        return resultClassDAOInfterface.deleteResultClass(id);
    }

    @Override
    public boolean deleteRuleEvaluationInfo(Long id) {
        return ruleEvaluationInfoDAOInterface.deleteRuleEvalautionInfo(id);
    }

    @Override
    public boolean deleteFuzzyRule(Long id) {
        return fuzzyRuleDAOInterface.deleteFuzzyRule(id);
    }

    @Override
    public List<SegmentEvaluationInfo> getListSegmentEvaluationInfo(Long campaignEvaluationIfoId) {
        return segmentEvaluationInfoDAOInterface.getListSegmentEvaluationInfo(campaignEvaluationIfoId);
    }

    @Override
    public List<RuleEvaluationInfo> getListRuleEvaluationInfoOfSegment(Long segmentEvaluationInfoId) {
        return ruleEvaluationInfoDAOInterface.getListRuleEvaluationInfoOfSegment(segmentEvaluationInfoId);
    }

    @Override
    public List<ResultClass> getListResultClassOfSegment(Long segmentId, Long campaignEvaluationIfoId) {
        return resultClassDAOInfterface.getListResultClassOfSegment(segmentId, campaignEvaluationIfoId);
    }

    @Override
    public List<FuzzyRule> getListFuzzyRuleOfSegment(Long segmentId, Long campaignEvaluationInfoId) {
        return fuzzyRuleDAOInterface.getListFuzzyRuleOfSegment(segmentId, campaignEvaluationInfoId);
    }

    @Override
    public List<Segment> getSegmentBuildTree(Long id) {
        return segmentDAO.getSegmentBuildTree(id);
    }

    @Override
    public SegmentEvaluationInfo doCreateOrUpdateSegmentEvaluationInfo(SegmentEvaluationInfo segmentEvaluationInfo) {
        return segmentEvaluationInfoDAOInterface.doCreateOrUpdateSegmentEvaluationInfo(segmentEvaluationInfo);
    }

    @Override
    public boolean deleteSegmentEvaluationInfo(Long id) {
        return segmentEvaluationInfoDAOInterface.deleteSegmentEvaluationInfo(id);
    }

    @Override
    public SegmentEvaluationInfo getNextSequenceSegmentEvaluationInFo() {
        return segmentEvaluationInfoDAOInterface.getNextSequenceSegmentEvaluationInfo();
    }

    @Override
    public boolean deleteCampaignSchedule(Long id) {
        return campaignScheduleDAOInterface.deleteCampaignSchedule(id);
    }

    @Override
    public boolean deleteCampaignEvaluationInfo(Long id) {
        return campaignEvaluationInfoDAOInterface.deleteCampaignEvaluaitonInfo(id);
    }

    @Override
    public long getNextSequense(String tableName) {
        return ussdScenarioDAO.getNextSequense(tableName);
    }

    @Override
    public Segment getSegmentByName(String segmentName) {
        return segmentDAO.getSegmentByName(segmentName);
    }

    @Override
    public List<FuzzyRule> getListFuzzyRuleByCampaignEvaluationInfoId(Long campaignEvaluationInfoId) {
        return fuzzyRuleDAOInterface.getListFuzzyRuleByCampaignEvaluationInfoId(campaignEvaluationInfoId);
    }

    @Override
    public CampaignEvaluationInfo findCampaignEvaluationInfoById(Long id) {
       return campaignEvaluationInfoDAOInterface.findCampaignEvaluationInfoById(id);
    }

    @Override
    public Campaign findCampaignById(Long campaignId) {
        return camapaignDAOInterface.findCampaignById(campaignId);
    }

    @Override
    public CampaignEvaluationInfo findOneById(Long id) {
        return campaignEvaluationInfoDAOInterface.findOneById(id);
    }

    

}
