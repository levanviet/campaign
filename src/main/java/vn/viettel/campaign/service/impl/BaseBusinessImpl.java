package vn.viettel.campaign.service.impl;

import vn.viettel.campaign.dao.BaseDAOInteface;
import vn.viettel.campaign.service.BaseBusinessInterface;

import java.util.List;
import org.hibernate.criterion.Order;

/**
 * @author truongbx
 * @param <T>
 * @date 8/25/2019
 */
public class BaseBusinessImpl<T, TDAO extends BaseDAOInteface<T>> implements BaseBusinessInterface<T> {

    public TDAO tdao;

    public Class<T> modelClass;

    @Override
    public T findById(long id) {
        return (T) tdao.findById(modelClass, id);
    }

    @Override
    public List<T> getAll() {
        return tdao.getAll(modelClass);
    }

    @Override
    public List<T> getAll(Order order) {
        return tdao.getAll(modelClass, order);
    }

    @Override
    public String saveOrUpdate(T t) {
        return tdao.saveOrUpdate(t);
    }

    @Override
    public String deleteById(long id) {
        return tdao.deleteById(modelClass, id);
    }

    @Override
    public String deleteByObject(T obj) {
        return tdao.deleteByObject(obj);
    }

    @Override
    public String update(T obj) {
        return tdao.update(obj);
    }

    @Override
    public String save(List<T> lstObj) {
        return tdao.save(lstObj);
    }

    @Override
    public String save(T obj) {
        return tdao.save(obj);
    }

    @Override
    public long saveAndGetId(T obj) throws Exception {
        return tdao.saveAndGetId(obj);
    }
}
