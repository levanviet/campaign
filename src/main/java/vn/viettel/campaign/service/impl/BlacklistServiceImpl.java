package vn.viettel.campaign.service.impl;

import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.entities.BlackList;

import javax.annotation.PostConstruct;
import vn.viettel.campaign.dao.BacklistDao;
import vn.viettel.campaign.service.BlacklistService;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Service
public class BlacklistServiceImpl extends BaseBusinessImpl<BlackList, BacklistDao> implements BlacklistService, Serializable {

    @Autowired
    private BacklistDao backlistDaoImpl;

    @PostConstruct
    public void setupService() {
        this.tdao = backlistDaoImpl;
        this.modelClass = BlackList.class;
    }

    @Override
    public List<BlackList> findBacklistByCategory(final List<Long> categoryIds) {
        return backlistDaoImpl.findBacklistByCategory(categoryIds);
    }

    @Override
    public List<BlackList> findBacklistByName(String backlistByName) {
        return backlistDaoImpl.findBacklistByName(backlistByName);
    }

    @Override
    public boolean checkDuplicate(final String name, final Long id) {
        return backlistDaoImpl.checkDuplicate(name, id);
    }

    @Override
    public boolean checkDuplicatePath(final String path, final Long id) {
        return backlistDaoImpl.checkDuplicatePath(path, id);
    }

    @Override
    public long onSaveBlacklist(BlackList blacklist) {
        return backlistDaoImpl.onSaveBlacklist(blacklist);
    }
}
