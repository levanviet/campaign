/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.RolesObjectsDao;
import vn.viettel.campaign.entities.RoleObject;
import vn.viettel.campaign.service.RolesObjectsService;

@Service
public class RolesObjectsServiceImpl implements RolesObjectsService {

    @Autowired
    private RolesObjectsDao rolesObjectsDaoImpl;

    @Override
    public Boolean saveRoleObject(RoleObject ro) {
        return rolesObjectsDaoImpl.saveRoleObject(ro);
    }

    @Override
    public Boolean updateRoleObject(RoleObject ro) {
        return rolesObjectsDaoImpl.updateRoleObject(ro);
    }

    @Override
    public void deleteRoleObjectByRoleId(Long roleId) {
        rolesObjectsDaoImpl.deleteRoleObjectByRoleId(roleId);
    }

    @Override
    public void deleteRoleObjectByObjectId(Long objectId) {
        rolesObjectsDaoImpl.deleteRoleObjectByObjectId(objectId);
    }

    @Override
    public List<RoleObject> findByRoleId(Long roleId) {
        return rolesObjectsDaoImpl.findByRoleId(roleId);
    }
}
