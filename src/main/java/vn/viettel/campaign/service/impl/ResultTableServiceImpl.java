/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.ResultDao;
import vn.viettel.campaign.dao.ResultTableDao;
import vn.viettel.campaign.dao.ResultTableRuleResultMapDao;
import vn.viettel.campaign.dao.RuleResultDao;
import vn.viettel.campaign.dao.impl.ResultTableDAO;
import vn.viettel.campaign.dao.impl.ResultTableDaoImpl;
import vn.viettel.campaign.dto.ConditionTableDTO;
import vn.viettel.campaign.entities.Result;
import vn.viettel.campaign.entities.ResultTable;
import vn.viettel.campaign.entities.ResultTableRuleResultMap;
import vn.viettel.campaign.entities.RuleResult;
import vn.viettel.campaign.service.ResultTableService;
import vn.viettel.campaign.service.RuleResultService;

@Service
public class ResultTableServiceImpl implements ResultTableService {

    @Autowired
    private ResultTableDao resultTableDaoImpl;

    @Autowired
    private ResultDao resultDao;

    @Autowired
    private RuleResultDao ruleResultDao;

    @Autowired
    private ResultTableRuleResultMapDao resultTableRuleResultMapDao;

    @Override
    public List<ResultTable> finAllResultTable() {
        return resultTableDaoImpl.finAllResultTable();
    }

    @Override
    public ResultTable updateOrSave(ResultTable resultTable) {
        return resultTableDaoImpl.updateOrSave(resultTable);
    }

    @Override
    public long checkUseRule(Long resultTableId) {
        return resultTableDaoImpl.checkUseRule(resultTableId);
    }

    @Override
    public void doDelete(ResultTable resultTable) {
        resultDao.deleteByResultTableId(resultTable.getResultTableId());
        ruleResultDao.deleteByResultTableId(resultTable.getResultTableId());
        resultTableRuleResultMapDao.deleteByResultTableId(resultTable.getResultTableId());
        if (!DataUtil.isNullObject(resultTable.getDefaultResultId())) {
            resultDao.deleteByDefaultResultId(resultTable.getDefaultResultId());
        }
        resultTableDaoImpl.deleteById(ResultTable.class, resultTable.getResultTableId());
    }

    @Override
    public ResultTable updateOrSave(ResultTable resultTable, Result defaultResult, List<ConditionTableDTO> conditionTableDTOS, ConditionTableDTO conditionTableDefault) {
        // delete old
        resultDao.deleteByResultTableId(resultTable.getResultTableId());
        ruleResultDao.deleteByResultTableId(resultTable.getResultTableId());
        resultTableRuleResultMapDao.deleteByResultTableId(resultTable.getResultTableId());
        if (!DataUtil.isNullObject(resultTable.getDefaultResultId())) {
            resultDao.deleteByDefaultResultId(resultTable.getDefaultResultId());
        }

//        if (!DataUtil.isNullObject(resultTable.getDefaultResultId())) {
        defaultResult = new Result();
        if (conditionTableDefault.getPromotionBlockId() != 0) {
            defaultResult.setPromotionBlockId(new BigInteger(conditionTableDefault.getPromotionBlockId() + ""));
        }
        if (!DataUtil.isNullObject(conditionTableDefault.getResultType())) {
            defaultResult.setResultType(Integer.parseInt(conditionTableDefault.getResultType() + ""));
            defaultResult.setStepIndex(conditionTableDefault.getStepIndex());
            Object nextSeq = resultDao.getSequence();
            defaultResult.setResultId(Long.parseLong(nextSeq.toString()));
            resultDao.updateOrSave(defaultResult);
        }
        // insert or update
        resultTable.setDefaultResultId(defaultResult.getResultId());
        resultTable = resultTableDaoImpl.updateOrSave(resultTable);
//        }
        if (!DataUtil.isNullObject(resultTable.getConditionTableId())) {
            if (!DataUtil.isNullOrEmpty(conditionTableDTOS)) {
                for (ConditionTableDTO conditionTableDTO : conditionTableDTOS) {
                    Result result = new Result();
                    if (conditionTableDTO.getPromotionBlockId() != 0) {
                        result.setPromotionBlockId(new BigInteger(conditionTableDTO.getPromotionBlockId() + ""));
                    }
                    result.setResultType(Integer.parseInt(conditionTableDTO.getResultType() + ""));
                    result.setStepIndex(conditionTableDTO.getStepIndex());
                    Object obj = resultDao.getSequence();
                    result.setResultId(Long.parseLong(obj.toString()));
                    resultDao.updateOrSave(result);

                    RuleResult ruleResult = new RuleResult();
                    ruleResult.setResultId(result.getResultId());
                    ruleResult.setRowIndex(Integer.parseInt(conditionTableDTO.getIndex()));
                    ruleResultDao.updateOrSave(ruleResult);

                    ResultTableRuleResultMap resultTableRuleResultMap = new ResultTableRuleResultMap();
                    resultTableRuleResultMap.setResultTableId(resultTable.getResultTableId());
                    Long id = ruleResult.getRuleResultId();
                    if (id != null) {

                        resultTableRuleResultMap.setRuleResultId(id);
                    }
                    resultTableRuleResultMapDao.updateOrSave(resultTableRuleResultMap);
                }
            }
        }
        return null;
    }

    @Override
    public ResultTable getResultTableById(Long resultTableId) {
        return resultTableDaoImpl.getResultTableById(resultTableId);
    }

    @Override
    public List<ResultTable> getListToCheckDuplicate(String resultTableName, Long resultTableId) {
        return resultTableDaoImpl.getListToCheckDuplicate(resultTableName, resultTableId);
    }


}
