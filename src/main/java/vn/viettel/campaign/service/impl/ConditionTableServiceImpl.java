/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.*;
import vn.viettel.campaign.dao.impl.ConditionTableRowMapDAO;
import vn.viettel.campaign.dto.ConditionTableDTO;
import vn.viettel.campaign.dto.ResultTableDTO;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.ConditionTableService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TOPICA
 */
@Service
public class ConditionTableServiceImpl implements ConditionTableService {


    @Autowired
    ConditionTableDAOInterface conditionTableDAO;
    @Autowired
    ColumnCTDAOInterface columnCTDAO;
    @Autowired
    RowCtDAOInterface rowCtDAO;
    @Autowired
    ResultTableDAOInterface resultTableDAO;
    @Autowired
    ProcessValueDAOInterface processValueDAO;
    @Autowired
    ConditionTableRowMapDAOInterface conditionTableRowMapDAO;
    @Autowired
    ConditionTableColumnMapDAOInterface conditionTableColumnMapDAO;

    @Override
    public ConditionTableDTO getDataConditionTable(long conditionTableId) {
        ConditionTableDTO conditionTableDTO = new ConditionTableDTO();
        List<ColumnCt> lstColumnCt = columnCTDAO.getLstColumnctByConditionTableId(conditionTableId);
        List<RowCt> lstRowCt = rowCtDAO.getLstRowCtByConditionTableId(conditionTableId);
        List<ProcessValue> lstProcessValue = new ArrayList<>();
        if (!DataUtil.isNullOrEmpty(lstRowCt)) {
            List<Long> valueIds = new ArrayList<Long>();
            for (RowCt rowCt : lstRowCt) {
                Long rowIndex = rowCt.getRowIndex();
                List<ResultTableDTO> lstResultTableDTO = resultTableDAO.getListResultTableDTO(conditionTableId, rowIndex);
                rowCt.setLstResultTableDTO(lstResultTableDTO);
                String value = rowCt.getValue();
                if (!DataUtil.isNullOrEmpty(value)) {
                    if (value.contains("/")) {
                        List<String> lstIds = DataUtil.splitListFile(value, "/");
                        if (!DataUtil.isNullOrEmpty(lstIds)) {
                            for (String valueId : lstIds) {
                                valueIds.add(Long.parseLong(valueId.trim()));
                            }
                        }
                    } else {
                        valueIds.add(Long.parseLong(value));
                    }
                }
            }
//            lstProcessValue = processValueDAO.findByValueIds(valueIds);
        }
        ConditionTable conditionTable = conditionTableDAO.findById(ConditionTable.class, conditionTableId);
        //
        conditionTableDTO.setLstColumnCt(lstColumnCt);
        conditionTableDTO.setLstProcessValue(lstProcessValue);
        conditionTableDTO.setLstRowCt(lstRowCt);
        conditionTableDTO.setConditionTable(conditionTable);
        return conditionTableDTO;
    }

    @Override
    public List<ProcessValue> getProcessValueByValuePreProcessIds(List<Long> preProcessIds) {
        return processValueDAO.findByValuePreProcessIds(preProcessIds);
    }

    @Override
    public void doSave(ConditionTable conditionTable, List<ConditionTableDTO> lstConditionTableDTO, List<ColumnCt> lstColumnCt) {
        if (!DataUtil.isNullOrEmpty(lstConditionTableDTO)) {
            for (ConditionTableDTO conditionTableDTO : lstConditionTableDTO) {
                if ("1".equalsIgnoreCase(conditionTableDTO.getIsDefault())) {
                    conditionTable.setDefaultResultIndex(Long.parseLong(conditionTableDTO.getIndex()));
                }
            }
        }
        // creae or update ConditionTable
//        conditionTable.setConditionTableId(2l);
        conditionTableDAO.saveOrUpdate(conditionTable);
        // delete row_ct
        rowCtDAO.deleteByConditionTableId(conditionTable.getConditionTableId());
        // delete column_ct
        columnCTDAO.deleteByConditionTableId(conditionTable.getConditionTableId());
        // delete condition_table_row_map
        conditionTableRowMapDAO.deleteByConditionTableId(conditionTable.getConditionTableId());
        // delete condition_table_column_map
        conditionTableColumnMapDAO.deleteByConditionTableId(conditionTable.getConditionTableId());
        // insert row_ct va condition_table_row_map
        if (!DataUtil.isNullOrEmpty(lstConditionTableDTO)) {
            long index = 1l;
            List<Long> idsRule = new ArrayList<>();
            List<ResultTableDTO> lstResultTableDTO = resultTableDAO.getListResultTableDTO(conditionTable.getConditionTableId(), null);
            if (!DataUtil.isNullOrEmpty(lstResultTableDTO)) {
                for (ResultTableDTO resultTableDTO : lstResultTableDTO) {
                    idsRule.add(resultTableDTO.getRuleResultId());
                }
            }
            int i = resultTableDAO.updateByCoditionTableId(conditionTable.getConditionTableId(), idsRule, -1L);
            for (ConditionTableDTO conditionTableDTO : lstConditionTableDTO) {
                List<String> valueIds = conditionTableDTO.getValueIds();
                if (!DataUtil.isNullOrEmpty(valueIds)) {
                    String ids = "";
                    for (String id : valueIds) {
                        ids += id + "/";
                    }
                    ids = ids.substring(0, ids.lastIndexOf("/"));
                    RowCt rowCt = new RowCt();
                    rowCt.setValue(ids);
                    rowCt.setRowIndex(index++);
                    rowCtDAO.saveOrUpdate(rowCt);
//                    rowCt.setRowIndex(rowCt.getRowId());
//                    rowCtDAO.saveOrUpdate(rowCt);
                    //
                    ConditionTableRowMap conditionTableRowMap = new ConditionTableRowMap();
                    conditionTableRowMap.setConditionTableId(conditionTable.getConditionTableId());
                    conditionTableRowMap.setRowId(rowCt.getRowId());
                    conditionTableRowMapDAO.saveOrUpdate(conditionTableRowMap);
                    // save
                    if (!DataUtil.isNullOrEmpty(conditionTableDTO.getLstResultTableDTO())) {
                        // update rowindex bang rule_result

                        List<Long> id = new ArrayList<>();
                        for (ResultTableDTO resultTableDTO : conditionTableDTO.getLstResultTableDTO()) {
                            id.add(resultTableDTO.getRuleResultId());
                        }
                        //
                        i = resultTableDAO.updateByCoditionTableId(conditionTable.getConditionTableId(), id, rowCt.getRowIndex());
                        System.out.println(i);
                    }
                    //
                }
            }

            // xoa nhung index da bi xoa
            resultTableDAO.deleteByCoditionTableId();
        }
        // insert column_ct va condition_table_column_map
        if (!DataUtil.isNullOrEmpty(lstConditionTableDTO)) {
            List<Long> lstPreProcessIds = lstConditionTableDTO.get(0).getPreProcessIds();
            if (!DataUtil.isNullOrEmpty(lstPreProcessIds)) {
                // luu thong tin cac column
                long columnIndex = 1;
                int i = 0;
                for (Long preProcessId : lstPreProcessIds) {
                    if (!DataUtil.isNullObject(preProcessId)) {
                        ColumnCt columnCt = new ColumnCt();
                        columnCt.setColumnIndex(columnIndex++);
                        columnCt.setColumnName(lstColumnCt.get(i++).getColumnName());
                        columnCt.setPreProcessId(preProcessId);
                        columnCTDAO.saveOrUpdate(columnCt);
                        // update lai index va name
//                        columnCt.setColumnIndex(columnCt.getColumnId());
//                        columnCt.setColumnName("PPU" + columnCt.getColumnId());
//                        columnCTDAO.saveOrUpdate(columnCt);
                        // Luu thong tin conditionTableColumnMap
                        ConditionTableColumnMap conditionTableColumnMap = new ConditionTableColumnMap();
                        conditionTableColumnMap.setColumnId(columnCt.getColumnId());
                        conditionTableColumnMap.setConditionTableId(conditionTable.getConditionTableId());
                        conditionTableColumnMapDAO.saveOrUpdate(conditionTableColumnMap);
                    }
                }
            }
        }
        // done
    }

    @Override
    public void doDelete(ConditionTable conditionTable) {
        conditionTableDAO.deleteById(ConditionTable.class, conditionTable.getConditionTableId());
        // delete row_ct
        rowCtDAO.deleteByConditionTableId(conditionTable.getConditionTableId());
        // delete column_ct
        columnCTDAO.deleteByConditionTableId(conditionTable.getConditionTableId());
        // delete condition_table_row_map
        conditionTableRowMapDAO.deleteByConditionTableId(conditionTable.getConditionTableId());
        // delete condition_table_column_map
        conditionTableColumnMapDAO.deleteByConditionTableId(conditionTable.getConditionTableId());
    }

    @Override
    public boolean checkDelete(Long conditionTableId) {
        return resultTableDAO.getLstRuleResultTableByConditionTableIds(conditionTableId).size() < 1;
    }

    @Override
    public ConditionTable getDataConditionTableByName(String name, long conditionTableId) {
        return conditionTableDAO.getDataConditionTableByName(name, conditionTableId);
    }
}
