package vn.viettel.campaign.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.common.Responses;
import vn.viettel.campaign.dao.*;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.OCSTemplateInterface;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Service
public class OCSTemplateServiceImpl extends BaseBusinessImpl<OcsBehaviourTemplate, OCSTemplateDaoInterface> implements OCSTemplateInterface {

    @Autowired
    OCSTemplateDaoInterface oCSTemplateDAO;
    @Autowired
    MapCommandDaoInterface mapCommandDAO;
    @Autowired
    OCSTemplateFieldDaoInterface oCSTemplateFieldDAO;
    @Autowired
    OCSTemplateExtendFieldDaoInterface OCSTemplateExtendFieldDAO;
    @Autowired
    ExtendFieldDaoInterface extendFieldDAO;

    @PostConstruct
    public void setupService() {
        this.tdao = oCSTemplateDAO;
        this.modelClass = OcsBehaviourTemplate.class;
    }

    @Override
    public List<MapCommand> getLstMapConmand() {
        return mapCommandDAO.getAll(MapCommand.class);
    }

    @Override
    public List<OcsBehaviourTemplateFields> getLstFieldOfTemplate(long id) {
        return oCSTemplateFieldDAO.getLstFieldOfTemplate(id);
    }

    @Override
    public List<OcsBehaviourTemplateFields> getRemainFieldOfTemplate(long id, List<Long> ids) {
        return oCSTemplateFieldDAO.getRemainFieldOfTemplate(id, ids);
    }

    @Override
    public OcsBehaviourTemplate getNextSequense() {
        return oCSTemplateDAO.getNextSequense();
    }

    @Override
    public List<OcsBehaviourTemplateExtendFields> getLstExtendFieldOfTemplate(long id) {
        return OCSTemplateExtendFieldDAO.getLstExtendFieldOfTemplate(id);
    }

    @Override
    public List<OcsBehaviourTemplateExtendFields> getRemainExtendFieldOfTemplate(long id, List<Long> lstExtendIds) {
        return OCSTemplateExtendFieldDAO.getRemainExtendFieldOfTemplate(id, lstExtendIds);
    }

    @Override
    public ExtendField getNextExtendSequense() {
        return extendFieldDAO.getNextSequense();
    }

    @Override
    public boolean checkExtendFieldName(String name) {
        return extendFieldDAO.checkExisExtendFieldName(name);
    }

    @Override
    public void insertExtendField(ExtendField extendField) {
        extendFieldDAO.save(extendField);
    }

    @Override
    public boolean onSaveBehaviourTemplate(OcsBehaviourTemplate ocsBehaviourTemplate, List<OcsBehaviourTemplateFields> lstField, List<OcsBehaviourTemplateExtendFields> lstExtendFields) {
        String result = oCSTemplateDAO.onSaveBehaviourTemplate(ocsBehaviourTemplate, lstField, lstExtendFields);
        if (!result.equalsIgnoreCase(Responses.SUCCESS.getName())) {
            return false;
        }
        return true;
    }

    @Override
    public boolean onUpdateBehaviourTemplate(OcsBehaviourTemplate ocsBehaviourTemplate, List<OcsBehaviourTemplateFields> lstField, List<OcsBehaviourTemplateExtendFields> lstExtendFields) {
        String result = oCSTemplateDAO.onUpdateBehaviourTemplate(ocsBehaviourTemplate, lstField, lstExtendFields);
        if (!result.equalsIgnoreCase(Responses.SUCCESS.getName())) {
            return false;
        }
        return true;
    }

    @Override
    public boolean checkTemplateName(String name, Long id) {
        return oCSTemplateDAO.checkTemplateName(name, id);
    }

    @Override
    public boolean onDeleteBehaviourTemplate(Long templateId) {
        return oCSTemplateDAO.deleteTemplate(templateId);
    }

    @Override
    public boolean checkOcsTemplateInUse(Long templateId) {
        return oCSTemplateDAO.checkOcsBehaviourTemplateInUse(templateId);
    }
}
