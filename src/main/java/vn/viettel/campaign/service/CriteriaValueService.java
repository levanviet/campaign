/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.CriteriaValue;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.KpiProcessorCriteriaValueMap;

/**
 *
 * @author SON
 */
public interface CriteriaValueService {
    List<CriteriaValue> getLstCriteriaValueByKpiProcessorId(Long id);
    CriteriaValue onSaveCriteriaValue(CriteriaValue criteriaValue);
    CriteriaValue onUpdateCriteriaValue(CriteriaValue criteriaValue);
    Boolean onDeleteCriteriaValue(Long id);
    Boolean onDeleteCriteriaValue(Long processorId, Long CriteriaValueId);
    CriteriaValue getNextSequense();
    CriteriaValue findOneById(Long id);
    CriteriaValue onSaveOrUpdate(CriteriaValue criteriaValue);
    void onSaveKpiCriteriaValueMap(KpiProcessorCriteriaValueMap kpiProcessorCriteriaValueMap);
    CriteriaValue checkExitsCriteriaName(String name, Long id,Long processorId);
    List<KpiProcessorCriteriaValueMap> getLstKpiProcessorCriteriaValueMap();
    Boolean checkDeleteCriteriaValue(Long kpiId, String name);
    
    List<FuzzyRule> getListFuzzyForCriteriaValue(Long kpiId);
}
