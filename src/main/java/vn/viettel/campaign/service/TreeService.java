/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;

import org.primefaces.model.TreeNode;
import vn.viettel.campaign.dto.NotifyTriggerDTO;
import vn.viettel.campaign.entities.*;

/**
 * @author HoangLB
 */

public interface TreeService {
    public TreeNode createTreeCategoryAndComponent(List<Category> listCat, List<Campaign> listCampaign);

    public TreeNode createTreeCategoryAndChild(List<Category> listCat, List<BaseCategory> baseCategories);

    public TreeNode createSegmentTree(List<Category> listCat, List<Segment> segments);

    public TreeNode createConditionTree(List<Category> listCat, List<ConditionTable> conditionTables);

    public TreeNode createZoneTree(List<Category> listCat, List<Zone> zones);

    public TreeNode createZoneZonMapTree(List<ZoneMap> listZoneMap, List<Zone> zones);

    public TreeNode createGeoHomeZoneTree(List<Category> listCat, List<GeoHomeZone> geoHomeZones);

    public TreeNode createZoneMapTree(List<Category> listCat, List<ZoneMap> zoneMaps);

    public TreeNode createUssdScenarioTree(List<Category> listCat, List<UssdScenario> ussdScenarios);

    public TreeNode createOcsBehaviourTree(List<Category> listCat, List<OcsBehaviour> items);

    public TreeNode createScenarioTriggerTree(List<Category> listCat, List<ScenarioTrigger> scenarioTriggers);

    public TreeNode createNotifyTriggerTree(List<Category> listCat, List<NotifyTrigger> notifyTriggers);

    public TreeNode createStatisticCycleTree(List<Category> listCat, List<StatisticCycle> statisticCycles);

    public TreeNode createPreProcessUnitTree(List<Category> listCat, List<PreProcessUnit> preProcessUnits);

    public TreeNode createNotifyTemplateTree(List<Category> listCat, List<NotifyTemplate> notifyTemplates);

    public TreeNode createRule(List<Category> listCat, List<Rule> rules);

    public TreeNode createTreeScenario(List<ScenarioNode> scenarioNodes);

    public TreeNode createTreePath(List<NotifyTriggerDTO> notifyTriggerDTOS);

    TreeNode createTreeCategoryAndComponentObj(List<Category> listCat, List<Object> listObject);
    
    public TreeNode createResultTableTree(List<Category> listCat, List<ResultTable> resultTables);
    
    public TreeNode createPromotionBlockTree(List<Category> listCat, List<PromotionBlock> promotionBlocks);
    
    public void collapseAll(TreeNode node);
    
    public void expandAll(TreeNode node);

}
