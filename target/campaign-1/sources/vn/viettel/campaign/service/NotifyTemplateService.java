package vn.viettel.campaign.service;

import org.primefaces.model.TreeNode;
import vn.viettel.campaign.dto.NotifyTemplateDetailDTO;
import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.entities.Language;
import vn.viettel.campaign.entities.NotifyContent;
import vn.viettel.campaign.entities.NotifyTemplate;

import java.util.List;

public interface NotifyTemplateService {
    TreeNode getTree(List<Category> categories);
    NotifyTemplateDetailDTO getDetail(Long id);
    List<Language> getLanguages();
    NotifyContent saveContent(NotifyContent content);
    Language getLanguageById(Long id);
    void removeContentById(Long contentId);
    NotifyTemplate saveTemplate(NotifyTemplate template);
    void removeTemplateById(Long id);
    Long getNextId();
    NotifyTemplate findByName(String name, Long id);
    List<NotifyContent> updateNotifyContent(Long templateIdFucus, List<NotifyContent> contents);
    boolean checkExistingTemplateByCategoryId(Long categoryId);
}
