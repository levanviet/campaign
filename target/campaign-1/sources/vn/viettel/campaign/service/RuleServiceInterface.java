package vn.viettel.campaign.service;

import vn.viettel.campaign.entities.*;

import java.util.List;

/**
 * @author truongbx
 * @date 9/15/2019
 */
public interface RuleServiceInterface extends BaseBusinessInterface<Rule> {
	Rule getNextSequense();

	boolean onSaveRule(Rule rule, List<ResultTable> resultTables);
	boolean onUpdateRule(Rule rule, List<ResultTable> resultTables);
	boolean onDeleteRule(Long ruleId);
	boolean checkRuleInUse(Long ruleId);

}
