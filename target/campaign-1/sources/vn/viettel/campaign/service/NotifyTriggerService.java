/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import vn.viettel.campaign.entities.*;

import java.util.List;

/**
 * @author TOPICA
 */

public interface NotifyTriggerService {
    public List<Language> getAllLanguage();

    public List<DataType> getAllDataType();

    public List<RetryCycle> getAllRetryCycle();

    public List<CycleUnit> getAllCycleUnit();

    List<NotifyTrigger> getByNotifyTemplateId(Long id);

    public void doSave(NotifyTrigger notifyTrigger);

    public void doDelete(long id);

    public NotifyTrigger getDetail(long id);

    public boolean validateAdd(long id, String name);

    public List<StatisticCycle> findStatisticCycleById(long id);

    public List<NotifyStatisticMap> findByNotifyIdAndCycleId(long notifyId, long cycleId);
}
