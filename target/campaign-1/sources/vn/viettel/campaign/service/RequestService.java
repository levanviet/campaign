/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.io.IOException;
import java.util.List;
import vn.viettel.campaign.dto.ComboboxDTO;
import vn.viettel.campaign.dto.CreateFileDTO;
import vn.viettel.campaign.dto.RequestBuildDTO;
import vn.viettel.campaign.dto.RequestCountDTO;
import vn.viettel.campaign.dto.RequestDTO;
import vn.viettel.campaign.dto.RequestStatusDTO;
import vn.viettel.campaign.dto.SpecialOfferDTO;
import vn.viettel.campaign.entities.Request;

/**
 *
 * @author ConKC
 */
public interface RequestService {

    List<Request> getAllRequest();

    RequestCountDTO countRequestUpdate(Long id);

    List<RequestDTO> getAllRequestScalar();

    RequestStatusDTO getRequestStatus();

    List<ComboboxDTO> generateObjects();

    List<ComboboxDTO> generateActions(final String object);

    RequestDTO findRequestById(final long id);

    void deleteRequestByTransaction(String transactionId);

    void deleteRequest(final RequestDTO requestDTO) throws Exception;

    int saveFileToDir(String name, String extension, byte[] content) throws IOException;

    int createFileToDir(String pathFile, List<CreateFileDTO> content, String objectType) throws IOException;

    String autoGenPath(String imputName, String prefixPath);

    String genListSpeialOffer(List<SpecialOfferDTO> specialOfferDTOs);

    List<SpecialOfferDTO> genListSpeialOffer(String input);

    void updateOrSave(Request request);

    void uploadSegment(RequestBuildDTO buildDTO) throws Exception;

    void deleteSegment(RequestBuildDTO buildDTO) throws Exception;

    void uploadBlacklist(RequestBuildDTO buildDTO) throws Exception;
    
    void deleteBlacklist(RequestBuildDTO buildDTO) throws Exception;
    
    void updateBlacklist(RequestBuildDTO buildDTO) throws Exception;
    
    void uploadSpecialOffer(RequestBuildDTO buildDTO) throws Exception;
    
    void deleteSpecialOffer(RequestBuildDTO buildDTO) throws Exception;
    
    void updateSpecialOffer(RequestBuildDTO buildDTO) throws Exception;
}
