/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.Campaign;
import vn.viettel.campaign.entities.CampaignEvaluationInfo;
import vn.viettel.campaign.entities.CampaignSchedule;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.ResultClass;
import vn.viettel.campaign.entities.RuleEvaluationInfo;
import vn.viettel.campaign.entities.Segment;
import vn.viettel.campaign.entities.SegmentEvaluationInfo;

/**
 *
 * @author ABC
 */
public interface CampaignEvaluationInforService {

    public List<CampaignEvaluationInfo> getListCampaginEvaluationInfoByCategoryIds(List<Long> categoryIds);
    
    public CampaignEvaluationInfo findOneById(Long id);

    public CampaignEvaluationInfo findCampaignEvaluationInfoById(Long id);

    public List<RuleEvaluationInfo> getListRuleEvaluationInfoByOwnerId(Long campaignEvaluationIfoId);

    public List<FuzzyRule> getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(Long campaignId, Long campaignEvaluationInfoId);

    public List<FuzzyRule> getListFuzzyRuleByCampaignEvaluationInfoId(Long campaignEvaluationInfoId);

    public List<ResultClass> getListResultClassByOwnerIdAndCampaignEvaluationInfoId(Long campaignId, Long campaignEvaluationInfoId);

    public List<ResultClass> getListResultClassByAssessmentRuleId(Long assessmentRuleId);

    public List<SegmentEvaluationInfo> getListSegmentEvaluationInfo(Long campaignEvaluationIfoId);

    public CampaignSchedule getCampaignScheduleByScheduleId(Long id);

    public List<RuleEvaluationInfo> getListRuleEvaluationInfoOfSegment(Long segmentEvaluationInfoId);

    public List<ResultClass> getListResultClassOfSegment(Long segmentId, Long campaignEvaluationIfoId);

    public List<FuzzyRule> getListFuzzyRuleOfSegment(Long segmentId, Long campaignEvaluationInfoId);

    public List<Segment> getSegmentBuildTree(Long id);

    public Segment getSegmentByName(String segmentName);
    
    public Campaign findCampaignById(Long campaignId);

    RuleEvaluationInfo getNextSequenceRuleEvaluation();

    ResultClass getNextSequenceResultClass();

    ResultClass getNameByClassId(Long classId);

    CampaignEvaluationInfo getNextSequenceCampaignEvaluationInfo();

    CampaignSchedule getNextSequenceCampaignSchedule();

    FuzzyRule getNextSequenceFuzzyRule();

    SegmentEvaluationInfo getNextSequenceSegmentEvaluationInFo();

    public CampaignSchedule doCreateOrUpdateCampaignSchedule(CampaignSchedule CampaignSchedule);

    public CampaignEvaluationInfo doCreateOrUpdateCampaignEvaluationInfo(CampaignEvaluationInfo campaignEvaluationInfo);

    public RuleEvaluationInfo doCreateOrUpdateRuleEvaluationInfo(RuleEvaluationInfo ruleEvaluationInfo);

    public ResultClass doCreateOrUpdateResultClass(ResultClass resultClass);

    public SegmentEvaluationInfo doCreateOrUpdateSegmentEvaluationInfo(SegmentEvaluationInfo segmentEvaluationInfo);

    public FuzzyRule doCreateOrUpdateFuzzyRule(FuzzyRule fuzzyRule);

    public boolean deleteResultClass(Long id);

    public boolean deleteRuleEvaluationInfo(Long id);

    public boolean deleteFuzzyRule(Long id);

    public boolean deleteSegmentEvaluationInfo(Long id);

    public boolean deleteCampaignSchedule(Long id);

    public boolean deleteCampaignEvaluationInfo(Long id);

    public long getNextSequense(String tableName);
   
  
}
