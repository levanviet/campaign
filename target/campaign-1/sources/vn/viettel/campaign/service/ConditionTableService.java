/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import vn.viettel.campaign.dto.ConditionTableDTO;
import vn.viettel.campaign.entities.ColumnCt;
import vn.viettel.campaign.entities.ConditionTable;
import vn.viettel.campaign.entities.ProcessValue;

import java.util.List;

/**
 * @author TOPICA
 */

public interface ConditionTableService {
    public ConditionTableDTO getDataConditionTable(long conditionTableId);

    List<ProcessValue> getProcessValueByValuePreProcessIds(List<Long> preProcessIds);

    public void doSave(ConditionTable conditionTable, List<ConditionTableDTO> lstConditionTableDTO, List<ColumnCt> lstColumnCt);

    public void doDelete(ConditionTable conditionTable);

    public boolean checkDelete(Long conditionTableId);

    public ConditionTable getDataConditionTableByName(String name, long conditionTableId);
}
