/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.Roles;

public interface RolesService {

    public List<Roles> getLstRolesByName(String name, Long objectId);

    public List<Roles> getLstRolesByCode(String code, Long objectId);

    public Boolean saveRoles(Roles obj);

    public Boolean updateRoles(Roles obj);

    public List<Roles> getLstRoles();

    public List<Long> getRolesByUserId(Long userId);

    public Roles getRolesById(Long valueOf);
}
