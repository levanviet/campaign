/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;

import vn.viettel.campaign.entities.*;

/**
 *
 * @author ConKC
 */
public interface CategoryService {

    void save(Category category);

    List<Category> getCategory();

    List<Category> getCategoryByType(final Integer categoryType);

    List<Category> getCategoryByType(final Integer categoryType, final boolean parentId);

    Category getCategoryById(final Long categoryId);

    Boolean deleteCategory(final Category category);

    List<Campaign> getObjectByCategory(final List<Long> categoryIds, final String objectName);
    <T> List<T> getDataFromCatagoryId(final List<Long> categoryIds, final String objectName);
    List<Segment> getObjectByCategory(final List<Long> categoryIds);

    List<ConditionTable> getConditionTableByCategory(final List<Long> categoryIds);
    
    List<ResultTable> getResultTableByCategory(final List<Long> categoryIds);
    
    List<PromotionBlock> getPromotionBlockByCategory(final List<Long> categoryIds);

    List<UssdScenario> getUssdScenarioByCategory(final List<Long> categoryIds);

    List<ScenarioTrigger> getScenarioTriggerByCategory(final List<Long> categoryIds);

    List<NotifyTrigger> getNotifyTriggerByCategory(final List<Long> categoryIds);

    List<PreProcessUnit> getPreProcessUnitByCategory(final List<Long> categoryIds);

    List<NotifyTemplate> getNotifyTemplateByCategory(final List<Long> categoryIds);

    List<Rule> getRuleByCategory(final List<Long> categoryIds);

    boolean checkDuplicate(final String categoryName, final Long categoryId);

    boolean checkDuplicate(final String categoryName, final Long categoryId, Integer categoryType);

    boolean checkDeleteCategory(final Long categoryId);

    boolean checkDeleteCategory(final Long categoryId, String className);

    List<Category> findCategoryByIds(final List<Long> categoryIds);

    List<Category> findCategoryByTypes(final List<Integer> types);
}
