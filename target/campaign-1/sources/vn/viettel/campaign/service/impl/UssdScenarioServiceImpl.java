/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.*;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.SegmentService;
import vn.viettel.campaign.service.UssdScenarioService;

import java.util.List;

/**
 * @author ConKC
 */
@Service
public class UssdScenarioServiceImpl implements UssdScenarioService {

    @Autowired
    private UssdScenarioDAOInterface ussdScenarioDAO;
    @Autowired
    private LanguageDAOInterface languageDAO;
    @Autowired
    private NoteContentDAOInterface noteContentDAO;
    @Autowired
    private ScenarioNodeDAOInterface scenarioNodeDAO;
    @Autowired
    private OcsBehaviourDAOInterface ocsBehaviourDAO;


    @Override
    public UssdScenario getUssdScenarioById(long id) {
        UssdScenario ussdScenario = ussdScenarioDAO.findById(UssdScenario.class, id);
        List<ScenarioNode> lstScenarioNode = scenarioNodeDAO.findByScenarioId(id);
        if (!DataUtil.isNullOrEmpty(lstScenarioNode)) {
            for (ScenarioNode scenarioNode : lstScenarioNode) {
                scenarioNode.setLstNoteContent(noteContentDAO.findByNoteId(scenarioNode.getNodeId()));
            }
        }
        if (!DataUtil.isNullObject(ussdScenario)) {
            ussdScenario.setLstScenarioNode(lstScenarioNode);
        }
        return ussdScenario;
    }

    @Override
    public List<OcsBehaviour> getAllOcsBehaviour() {
        return ocsBehaviourDAO.getAll(OcsBehaviour.class);
    }

    @Override
    public List<Language> getAllLanguage() {
        return languageDAO.getAll(Language.class);
    }

    @Override
    public long getNextSequense(String tableName) {
        return ussdScenarioDAO.getNextSequense(tableName);
    }

    @Override
    @Transactional(propagation= Propagation.REQUIRED)
    public void doSaveUssd(UssdScenario ussdScenario) {
        try {
            ussdScenarioDAO.saveOrUpdate(ussdScenario);
            // delete old
            noteContentDAO.deleteByNoteIds(ussdScenario.getScenarioId());
            scenarioNodeDAO.deleteByScenarioId(ussdScenario.getScenarioId());
            // insert new dataa
            for (ScenarioNode scenarioNode : ussdScenario.getLstScenarioNode()) {
                scenarioNode.setUssdScenarioId(ussdScenario.getScenarioId());
                scenarioNodeDAO.saveOrUpdate(scenarioNode);
                if (!DataUtil.isNullOrEmpty(scenarioNode.getLstNoteContent())) {
                    List<NoteContent> lst = scenarioNode.getLstNoteContent();
                    for (int i = 0; i < lst.size(); i++) {
                        NoteContent noteContent = lst.get(i);
                        noteContent.setNodeId(scenarioNode.getNodeId());
                        noteContentDAO.saveOrUpdate(noteContent);
                    }
                    System.out.println("1");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doDeleteUssd(long id) {
        noteContentDAO.deleteByNoteIds(id);
        scenarioNodeDAO.deleteByScenarioId(id);
        ussdScenarioDAO.deleteById(UssdScenario.class, id);
    }

    @Override
    public boolean validateDeleteScenarioNode(long id) {
        return scenarioNodeDAO.findByParentId(id).size() < 1;
    }

    @Override
    public boolean validateDeleteUssd(long id) {
        return ussdScenarioDAO.checkDelete(id) < 1;
    }

    @Override
    public boolean validateAddUssd(long id, String name) {
        // ko co ban ghi nao trung ten thi return true
        return DataUtil.isNullObject(ussdScenarioDAO.getByName(id, name));
    }

    @Override
    public ScenarioNode findScenarioNodeById(long id) {
        return scenarioNodeDAO.findById(ScenarioNode.class, id);
    }

    @Override
    public List<ScenarioNode> findScenarioNodeByParId(long parId) {
        return scenarioNodeDAO.findByParentId(parId);
    }
}
