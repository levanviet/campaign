/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.*;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.ScenarioTriggerService;
import vn.viettel.campaign.service.UssdScenarioService;

import java.util.List;

/**
 * @author ConKC
 */
@Service
public class ScenarioTriggerServiceImpl implements ScenarioTriggerService {

    @Autowired
    private UssdScenarioDAOInterface ussdScenarioDAO;
    @Autowired
    private ScenarioNodeDAOInterface scenarioNodeDAO;
    @Autowired
    private ScenarioTriggerDAOInterface scenarioTriggerDAO;

    @Override
    public ScenarioTrigger findById(long id) {
        return scenarioTriggerDAO.findById(ScenarioTrigger.class, id);
    }

    @Override
    public void doSave(ScenarioTrigger scenarioTrigger) {
        scenarioTriggerDAO.saveOrUpdate(scenarioTrigger);
    }

    @Override
    public void doDelete(long id) {
        scenarioTriggerDAO.deleteById(ScenarioTrigger.class, id);
    }

    @Override
    public List<UssdScenario> getAllUssdScenario() {
        return ussdScenarioDAO.getAll(UssdScenario.class);
    }

    @Override
    public List<ScenarioNode> getScenarioNodeByUssdId(long id) {
        return scenarioNodeDAO.findByScenarioId(id);
    }

    @Override
    public boolean validateAdd(long id, String name) {
        // ko co ban ghi nao trung ten thi return true
        return DataUtil.isNullObject(scenarioTriggerDAO.getByName(id, name));
    }
}
