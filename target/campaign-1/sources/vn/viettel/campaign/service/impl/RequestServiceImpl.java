/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.logging.Level;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.StringCommon;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dao.BacklistDao;
import vn.viettel.campaign.dao.RequestDao;
import vn.viettel.campaign.dao.SegmentDao;
import vn.viettel.campaign.dao.SpecialpromDao;
import vn.viettel.campaign.dto.ComboboxDTO;
import vn.viettel.campaign.dto.CreateFileDTO;
import vn.viettel.campaign.dto.RequestBuildDTO;
import vn.viettel.campaign.dto.RequestCountDTO;
import vn.viettel.campaign.dto.RequestDTO;
import vn.viettel.campaign.dto.RequestStatusDTO;
import vn.viettel.campaign.dto.SpecialOfferDTO;
import vn.viettel.campaign.entities.BlackList;
import vn.viettel.campaign.entities.Request;
import vn.viettel.campaign.entities.Segment;
import vn.viettel.campaign.entities.SpecialProm;
import vn.viettel.campaign.service.RequestService;
import vn.viettel.campaign.service.UtilsService;

/**
 *
 * @author ConKC
 */
@Service
public class RequestServiceImpl implements RequestService {

//    private static Logger logger = LogManager.getLogger(RequestServiceImpl.class);

    public static void removeFile(String filePath) {
        File file = new File(filePath);
        file.deleteOnExit();
    }

    @Autowired
    private RequestDao requestDaoImpl;

    @Autowired
    private UtilsService utilsService;

    @Autowired
    private SegmentDao segmentDaoImpl;

    @Autowired
    private BacklistDao backlistDaoImpl;

    @Autowired
    private SpecialpromDao specialpromDaoImpl;

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public List<Request> getAllRequest() {
        return requestDaoImpl.getAllRequest();
    }

    @Override
    public RequestCountDTO countRequestUpdate(Long id) {
        return requestDaoImpl.countRequestUpdate(id);
    }

    @Override
    public List<RequestDTO> getAllRequestScalar() {
        return requestDaoImpl.getAllRequestScalar();
    }

    @Override
    public RequestStatusDTO getRequestStatus() {
        RequestStatusDTO statusDTO = new RequestStatusDTO();
        List<Request> requests = requestDaoImpl.getAllRequest();
        if (!requests.isEmpty()) {
            for (Request request : requests) {
                switch (request.getErrorCode()) {
                    case Constants.ErrorCode.INIT:
                        long init = statusDTO.getInit() + 1;
                        statusDTO.setInit(init);
                        break;
                    case Constants.ErrorCode.ACCEPT:
                        long accept = statusDTO.getAccept() + 1;
                        statusDTO.setAccept(accept);
                        break;
                    case Constants.ErrorCode.RUNNING:
                        long running = statusDTO.getRunning() + 1;
                        statusDTO.setRunning(running);
                        break;
                    case Constants.ErrorCode.SUCCESS:
                        long success = statusDTO.getSuccess() + 1;
                        statusDTO.setSuccess(success);
                        break;
                    case Constants.ErrorCode.FAILED:
                        long failed = statusDTO.getFailed() + 1;
                        statusDTO.setFailed(failed);
                        break;
                }
            }
        }
        return statusDTO;
    }

    @Override
    public List<ComboboxDTO> generateObjects() {
        List<ComboboxDTO> comboboxDTOs = new ArrayList<>();
        comboboxDTOs.add(new ComboboxDTO(Constants.ObjectType.SEGMENT, Constants.ObjectName.SEGMENT));
        comboboxDTOs.add(new ComboboxDTO(Constants.ObjectType.BLACK_LIST, Constants.ObjectName.BLACK_LIST));
        comboboxDTOs.add(new ComboboxDTO(Constants.ObjectType.SPECIAL_OFFER, Constants.ObjectName.SPECIAL_OFFER));
        return comboboxDTOs;
    }

    @Override
    public List<ComboboxDTO> generateActions(final String objectType) {
        List<ComboboxDTO> comboboxDTOs = new ArrayList<>();
        if (ObjectUtils.isNotEmpty(objectType)) {
            if (Constants.ObjectType.SEGMENT.equals(objectType)) {
                comboboxDTOs.add(new ComboboxDTO(Constants.ActionValue.UPLOAD, Constants.ActionName.UPLOAD));
                comboboxDTOs.add(new ComboboxDTO(Constants.ActionValue.DELETE, Constants.ActionName.DELETE));
            } else {
                comboboxDTOs.add(new ComboboxDTO(Constants.ActionValue.UPLOAD, Constants.ActionName.UPLOAD));
                comboboxDTOs.add(new ComboboxDTO(Constants.ActionValue.DELETE, Constants.ActionName.DELETE));
                comboboxDTOs.add(new ComboboxDTO(Constants.ActionValue.UPDATE, Constants.ActionName.UPDATE));
            }
        }
        return comboboxDTOs;
    }

    @Override
    public RequestDTO findRequestById(final long id) {
        return requestDaoImpl.getRequestById(id);
    }

    @Override
    public void deleteRequestByTransaction(String transactionId) {
        requestDaoImpl.deleteRequestByTransaction(transactionId);
    }

    @Override
    public void deleteRequest(final RequestDTO requestDTO) throws Exception {
        Request request = modelMapper.map(requestDTO, Request.class);
        if (Objects.nonNull(request)) {
            requestDaoImpl.deleteById(Request.class, request.getId());
        }
    }

    @Override
    public int saveFileToDir(String pathFile, String extension, byte[] content) throws IOException {
        OutputStream out = null;
        try {
            File file = new File(pathFile);
            if (file.exists()) {
                file.mkdir();
            }
            out = new BufferedOutputStream(new FileOutputStream(file));
            out.write(content);
            return 1;
        } catch (IOException ex) {
//            logger.error(ex.getMessage(), ex);
        } finally {
            if (out != null) {
                out.close();
            }
        }
        return -1;
    }

    @Override
    public int createFileToDir(String pathFile, List<CreateFileDTO> content, String objectType) throws IOException {
        OutputStream out = null;
        try {
            File file = new File(pathFile);
            if (file.exists()) {
                file.mkdir();
            }
            out = new BufferedOutputStream(new FileOutputStream(file));
            out.write(getContentFile(content, objectType));
            return 1;
        } catch (IOException ex) {
//            logger.error(ex.getMessage(), ex);
        } finally {
            if (out != null) {
                out.close();
            }
        }
        return -1;
    }

    private byte[] getContentFile(List<CreateFileDTO> content, String objectType) {
        StringBuilder contentFile = new StringBuilder();
        if (content != null && !content.isEmpty()) {
            for (CreateFileDTO item : content) {
                contentFile.append(item.getMsisdn());
                if (objectType.equals(Constants.ObjectType.BLACK_LIST)) {
                    contentFile.append("|").append(item.getOperator());
                } else if (objectType.equals(Constants.ObjectType.SPECIAL_OFFER)) {
                    if (item.getOperator().equals("2")) {
                        contentFile.append("|").append(item.getOperator()).append("|");
                    } else {
                        contentFile.append("|").append(item.getOperator()).append("|").append(item.getLstSpeialOffer());
                    }
                }
                contentFile.append("\r\n");
            }
        }
        return contentFile.toString().getBytes();
    }

    @Override
    public String autoGenPath(String imputName, String prefixPath) {
        if (StringUtils.isBlank(imputName)) {
            return StringUtils.EMPTY;
        }
        return prefixPath + StringCommon.removeAccent(imputName.trim().toLowerCase()).replaceAll("( )+", "_") + ".txt";
    }

    @Override
    public String genListSpeialOffer(List<SpecialOfferDTO> specialOfferDTOs) {
        StringBuilder builder = new StringBuilder();
        if (specialOfferDTOs != null && !specialOfferDTOs.isEmpty()) {
            for (SpecialOfferDTO item : specialOfferDTOs) {
                builder.append(";")
                        .append(item.getOffer())
                        .append(":")
                        .append(item.getPriority());
            }
            builder.deleteCharAt(0);
        }
        return builder.toString();
    }

    @Override
    public List<SpecialOfferDTO> genListSpeialOffer(String input) {
        List<SpecialOfferDTO> rs = new ArrayList<>();
        if (StringUtils.isNotBlank(input)) {
            String[] inputSplit = input.split(";");
            for (String item : inputSplit) {
                if (StringUtils.isNotBlank(item)) {
                    String[] property = item.split(":");
                    SpecialOfferDTO dTO = new SpecialOfferDTO();
                    dTO.setOffer(property[0]);
                    dTO.setPriority(property[1]);
                    rs.add(dTO);
                }
            }
        }
        return rs;
    }

    public static long getLineFile(File file) {
        long count = 0;
        try (FileInputStream stream = new FileInputStream(file)) {
            Scanner scaner = new Scanner(stream);
            String line;
            while (scaner.hasNext()) {
                line = scaner.next().trim();
                if (line.isEmpty()) {
                    continue;
                }
                count++;
            }
        } catch (Exception ex) {
//            logger.error(ex.getMessage(), ex);
        }
//        logger.info("Segment size from file = " + count);
        return count;
    }

    @Override
    public void updateOrSave(Request request) {
        requestDaoImpl.save(request);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void uploadSegment(RequestBuildDTO buildDTO) throws Exception {
        long segId = segmentDaoImpl.saveAndGetId(builderSegment(buildDTO));
        if (segId > 0) {
            requestDaoImpl.saveAndGetId(builderRequest(segId, Integer.valueOf(Constants.ObjectType.SEGMENT), Constants.ServiceId.UPLOAD_SEGMENT));
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteSegment(RequestBuildDTO buildDTO) throws Exception {
        if (buildDTO.getSegmentId() != null) {
            Segment seg = segmentDaoImpl.findById(Segment.class, buildDTO.getSegmentId());
            if (seg != null) {
                seg.setIsActive(0L);
                long id = segmentDaoImpl.saveAndGetId(seg);
                requestDaoImpl.saveAndGetId(builderRequest(id, Integer.valueOf(Constants.ObjectType.SEGMENT), Constants.ServiceId.DELETE_SEGMENT));
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void uploadBlacklist(RequestBuildDTO buildDTO) throws Exception {
        long id = backlistDaoImpl.saveAndGetId(builderBlackList(buildDTO));
        if (id > 0) {
            requestDaoImpl.saveAndGetId(builderRequest(id, Integer.valueOf(Constants.ObjectType.BLACK_LIST), Constants.ServiceId.UPLOAD_BLACKLIST));
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteBlacklist(RequestBuildDTO buildDTO) throws Exception {
        if (buildDTO.getBlacklistId() != null) {
            backlistDaoImpl.deleteById(BlackList.class, buildDTO.getBlacklistId());
            requestDaoImpl.saveAndGetId(builderRequest(buildDTO.getBlacklistId(), Integer.valueOf(Constants.ObjectType.BLACK_LIST), Constants.ServiceId.DELETE_BLACKLIST));
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateBlacklist(RequestBuildDTO buildDTO) throws Exception {
        if (buildDTO.getBlacklistId() != null) {
            BlackList bl = backlistDaoImpl.findById(BlackList.class, buildDTO.getBlacklistId());
            bl.setName(buildDTO.getBlacklistName());
            bl.setCategoryId(buildDTO.getCategoryId());
            bl.setPath(buildDTO.getPath());
            bl.setHistPath(buildDTO.getHistPath());
            bl.setFailedPath(buildDTO.getFailedPath());
            bl.setTempPath(buildDTO.getTempPath());
            backlistDaoImpl.saveAndGetId(bl);
            requestDaoImpl.saveAndGetId(builderRequest(buildDTO.getBlacklistId(), Integer.valueOf(Constants.ObjectType.BLACK_LIST), Constants.ServiceId.UPDATE_BLACKLIST));
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void uploadSpecialOffer(RequestBuildDTO buildDTO) throws Exception {
        long id = specialpromDaoImpl.saveAndGetId(builderSpecialProm(buildDTO));
        if (id > 0) {
            requestDaoImpl.saveAndGetId(builderRequest(id, Integer.valueOf(Constants.ObjectType.SPECIAL_OFFER), Constants.ServiceId.UPLOAD_SPECIAL_OFFER));
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteSpecialOffer(RequestBuildDTO buildDTO) throws Exception {
        if (buildDTO.getSpecialOfferId() != null) {
            specialpromDaoImpl.deleteById(SpecialProm.class, buildDTO.getSpecialOfferId());
            requestDaoImpl.saveAndGetId(builderRequest(buildDTO.getSpecialOfferId(), Integer.valueOf(Constants.ObjectType.SPECIAL_OFFER), Constants.ServiceId.DELETE_SPECIAL_OFFER));
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateSpecialOffer(RequestBuildDTO buildDTO) throws Exception {
        if (buildDTO.getSpecialOfferId() != null) {
            SpecialProm specialProm = specialpromDaoImpl.findById(SpecialProm.class, buildDTO.getSpecialOfferId());
            specialProm.setName(buildDTO.getSpecialOfferName());
            specialProm.setCategoryId(buildDTO.getCategoryId());
            specialProm.setPath(buildDTO.getPath());
            specialProm.setHistPath(buildDTO.getHistPath());
            specialProm.setFailedPath(buildDTO.getFailedPath());
            specialProm.setTempPath(buildDTO.getTempPath());
            specialProm.setDescription(buildDTO.getDescription());
            specialpromDaoImpl.saveAndGetId(specialProm);
            requestDaoImpl.saveAndGetId(builderRequest(buildDTO.getSpecialOfferId(), Integer.valueOf(Constants.ObjectType.SPECIAL_OFFER), Constants.ServiceId.UPDATE_SPECIAL_OFFER));
        }
    }

    private Segment builderSegment(RequestBuildDTO buildDTO) {
        Segment seg = new Segment();
        seg.setSegmentName(buildDTO.getSegmentName());
        seg.setSegmentSize(1L);
        seg.setCategoryId(buildDTO.getCategoryId());
        seg.setParentId(buildDTO.getSegmentParent());
        seg.setPath(buildDTO.getPath());
        seg.setHistPath(buildDTO.getHistPath());
        seg.setFailedPath(buildDTO.getFailedPath());
        seg.setTempPath(buildDTO.getTempPath());
        seg.setIsActive(1L);
        seg.setEffDate(buildDTO.getEffectDate());
        seg.setExpDate(buildDTO.getExpireDate());
        seg.setDescription(buildDTO.getDescription());
        return seg;
    }

    private BlackList builderBlackList(RequestBuildDTO buildDTO) {
        BlackList blackList = new BlackList();
        blackList.setName(buildDTO.getBlacklistName());
        blackList.setCategoryId(buildDTO.getCategoryId());
        blackList.setPath(buildDTO.getPath());
        blackList.setHistPath(buildDTO.getHistPath());
        blackList.setFailedPath(buildDTO.getFailedPath());
        blackList.setTempPath(buildDTO.getTempPath());
        blackList.setDescription(buildDTO.getDescription());
        return blackList;
    }

    private SpecialProm builderSpecialProm(RequestBuildDTO buildDTO) {
        SpecialProm specialProm = new SpecialProm();
        specialProm.setName(buildDTO.getSpecialOfferName());
        specialProm.setCategoryId(buildDTO.getCategoryId());
        specialProm.setPath(buildDTO.getPath());
        specialProm.setHistPath(buildDTO.getHistPath());
        specialProm.setFailedPath(buildDTO.getFailedPath());
        specialProm.setTempPath(buildDTO.getTempPath());
        specialProm.setDescription(buildDTO.getDescription());
        return specialProm;
    }

    private Request builderRequest(Long objId, Integer objType, Integer serviceId) {
        Request req = new Request();
        req.setTransactionId("123|1581300570573");
        req.setFactor(10);
        req.setObjectId(objId);
        req.setObjectType(objType);
        req.setRequestType(Constants.RequestType.REQUEST_SERVICE);
        req.setServiceId(serviceId);
        req.setErrorCode(3);
        req.setErrorDescription("PERCENTAGE_NOT_ENOUGH");
        req.setPercentage(0F);
        req.setRequestTime(new Date());
        req.setFinishTime(new Date());
        return req;
    }
}
