/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dto.NotifyTriggerDTO;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.*;
import java.util.List;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.stereotype.Service;
import java.util.Objects;
import vn.viettel.campaign.dto.BehaviourDTO;

/**
 *
 * @author HoangLB
 */
@Service
public class TreeServiceImpl implements TreeService {

    @Override
    public TreeNode createTreeCategoryAndComponent(List<Category> listCat, List<Campaign> listCampaign) {

        // root node in framework (not show in view)
        TreeNode rootNode = new DefaultTreeNode(null, null);

        // root node off tree (parent id is null)
        if (listCat != null) {

            Category catRoot = null;
            for (Category it : listCat) {
                if (it.getParentId() == null) {
                    catRoot = it;
                }
            }
            if (catRoot != null) {
                TreeNode catRootNode = new DefaultTreeNode("category", catRoot, rootNode);

                genTreeCategoryAndComponent(catRootNode, listCat, listCampaign, catRoot);
            }
        }

        return rootNode;
    }

    public void genTreeCategoryAndComponent(TreeNode parentNodeCat, List<Category> listCat, List<Campaign> listCampaign, Category parentCat) {
        // add componentNode
        for (Campaign camp : listCampaign) {
            if (camp.getCategoryId().equals(parentCat.getCategoryId())) {
                new DefaultTreeNode("campaign", camp, parentNodeCat);
            }
        }

        for (Category cat : listCat) {
            if (parentCat.getCategoryId().equals(cat.getParentId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
                genTreeCategoryAndComponent(childNodeCat, listCat, listCampaign, cat);
            }
        }
    }

    @Override
    public TreeNode createSegmentTree(List<Category> listCat, List<Segment> segments) {

        TreeNode catRootNode = getRootNoteCatagory(listCat);
        buildCatagoryTree(catRootNode.getChildren().get(0), listCat, segments);

        return catRootNode;
    }

    @Override
    public TreeNode createConditionTree(List<Category> listCat, List<ConditionTable> conditionTables) {

        TreeNode catRootNode = getRootNoteCatagory(listCat);
//        catRootNode.getChildren().get(0).setSelectable(true);
        buildCatagoryTreeCondition(catRootNode.getChildren().get(0), listCat, conditionTables);

        return catRootNode;
    }

    @Override
    public TreeNode createZoneTree(List<Category> listCat, List<Zone> zones) {
        TreeNode catRootNode = getRootNoteCatagory(listCat);
//        catRootNode.getChildren().get(0).setSelectable(true);
        buildCatagoryTreeZone(catRootNode.getChildren().get(0), listCat, zones);

        return catRootNode;
    }

    @Override
    public TreeNode createZoneZonMapTree(List<ZoneMap> listZoneMap, List<Zone> zones) {
              // root node in framework (not show in view)
        TreeNode rootNode = new DefaultTreeNode(null, null);
        Category catRoot = null;
        for (ZoneMap it : listZoneMap) {
            TreeNode catRootNode = new DefaultTreeNode("zoneMap", it, rootNode);
            catRootNode.setExpanded(true);
            catRootNode.setSelectable(true);
            buildCatagoryTreeZoneZoneMap(catRootNode, zones);
        }
        //
        return rootNode;
    }
    //
    public void buildCatagoryTreeZoneZoneMap(TreeNode parentNodeCat, List<Zone> zones) {
        ZoneMap parentCat = (ZoneMap) parentNodeCat.getData();
        for (Zone zone : zones) {
            if (DataUtil.safeEqual(zone.getZoneMapId(), parentCat.getZoneMapId())) {
                TreeNode childNodeCat = new DefaultTreeNode("zone", zone, parentNodeCat);
//                childNodeCat.setSelectable(false);
//                buildCatagoryTreeZoneZoneMap(childNodeCat, zones);
            }
        }
    }

    @Override
    public TreeNode createGeoHomeZoneTree(List<Category> listCat, List<GeoHomeZone> geoHomeZones) {
        TreeNode catRootNode = getRootNoteCatagory(listCat);
//        catRootNode.getChildren().get(0).setSelectable(true);
        buildCatagoryTreeGeoHomeZone(catRootNode.getChildren().get(0), listCat, geoHomeZones);

        return catRootNode;
    }

    @Override
    public TreeNode createZoneMapTree(List<Category> listCat, List<ZoneMap> zoneMaps) {
        TreeNode catRootNode = getRootNoteCatagory(listCat);
//        catRootNode.getChildren().get(0).setSelectable(true);
        buildCatagoryTreeZoneMap(catRootNode.getChildren().get(0), listCat, zoneMaps);

        return catRootNode;
    }

    @Override
    public TreeNode createUssdScenarioTree(List<Category> listCat, List<UssdScenario> ussdScenarios) {

        TreeNode catRootNode = getRootNoteCatagory(listCat);
        buildCatagoryTreeUssdScenario(catRootNode.getChildren().get(0), listCat, ussdScenarios);

        return catRootNode;
    }

    @Override
    public TreeNode createOcsBehaviourTree(List<Category> listCat, List<OcsBehaviour> items) {
        TreeNode catRootNode = getRootNoteCatagory(listCat);
        buildCatagoryTreeOcs(catRootNode.getChildren().get(0), listCat, items);

        return catRootNode;
    }

    @Override
    public TreeNode createScenarioTriggerTree(List<Category> listCat, List<ScenarioTrigger> scenarioTriggers) {
        TreeNode catRootNode = getRootNoteCatagory(listCat);
        buildCatagoryTreeScenarioTrigger(catRootNode.getChildren().get(0), listCat, scenarioTriggers);

        return catRootNode;
    }

    @Override
    public TreeNode createNotifyTriggerTree(List<Category> listCat, List<NotifyTrigger> notifyTriggers) {
        TreeNode catRootNode = getRootNoteCatagory(listCat);
        buildCatagoryTreeNotifyTrigger(catRootNode.getChildren().get(0), listCat, notifyTriggers);

        return catRootNode;
    }

    @Override
    public TreeNode createStatisticCycleTree(List<Category> listCat, List<StatisticCycle> statisticCycles) {
        TreeNode catRootNode = getRootNoteCatagory(listCat);
        buildCatagoryTreeStatisticCycle(catRootNode.getChildren().get(0), listCat, statisticCycles);

        return catRootNode;
    }

    @Override
    public TreeNode createPreProcessUnitTree(List<Category> listCat, List<PreProcessUnit> preProcessUnits) {

        // root node in framework (not show in view)
        TreeNode rootNode = new DefaultTreeNode(null, null);

        Category catRoot = null;
        for (Category it : listCat) {
            if (it.getParentId() == null) {
                catRoot = it;
                TreeNode catRootNode = new DefaultTreeNode("category", catRoot, rootNode);
//                if (!DataUtil.isNullOrEmpty(catRootNode.getChildren())) {
//
//                }
            }
        }
        if (!DataUtil.isNullOrEmpty(rootNode.getChildren())) {
            for (TreeNode node : rootNode.getChildren()) {
                buildCatagoryTreePreProcessUnit(node, listCat, preProcessUnits);
            }
        }
        return rootNode;
    }

    @Override
    public TreeNode createNotifyTemplateTree(List<Category> listCat, List<NotifyTemplate> notifyTemplates) {
        TreeNode catRootNode = getRootNoteCatagory(listCat);
        buildCatagoryTreeNotifyTemplate(catRootNode.getChildren().get(0), listCat, notifyTemplates);

        return catRootNode;
    }

    @Override
    public TreeNode createRule(List<Category> listCat, List<Rule> rules) {
        TreeNode catRootNode = getRootNoteCatagory(listCat);
        buildRuleTree(catRootNode.getChildren().get(0), listCat, rules);

        return catRootNode;
    }

    @Override
    public TreeNode createTreeScenario(List<ScenarioNode> scenarioNodes) {
        ScenarioNode scenarioNodeRoot = null;
        for (ScenarioNode item : scenarioNodes) {
            if (item.getParentId() == 0) {
                scenarioNodeRoot = item;
            }
        }
        // root node in framework (not show in view)
        TreeNode rootNode = new DefaultTreeNode(null, null);
        TreeNode catRootNode = new DefaultTreeNode("scenarioNode", scenarioNodeRoot, rootNode);
        buildScenarioTree(rootNode.getChildren().get(0), scenarioNodes);
        return rootNode;
    }

    @Override
    public TreeNode createTreePath(List<NotifyTriggerDTO> notifyTriggerDTOS) {
        NotifyTriggerDTO notifyTriggerDTO = new NotifyTriggerDTO();
        notifyTriggerDTO.setId(1);
        notifyTriggerDTO.setName("Root");
        notifyTriggerDTO.setParentId(0);
        // root node in framework (not show in view)
        TreeNode rootNode = new DefaultTreeNode(null, null);
        TreeNode catRootNode = new DefaultTreeNode("notifyTriggerDTO", notifyTriggerDTO, rootNode);
        NotifyTriggerDTO notifyTriggerDTOChild = new NotifyTriggerDTO();
        notifyTriggerDTOChild.setId(2);
        notifyTriggerDTOChild.setName("Child");
        notifyTriggerDTOChild.setParentId(1);
        TreeNode childNode = new DefaultTreeNode("notifyTriggerDTO", notifyTriggerDTOChild, rootNode.getChildren().get(0));
        return rootNode;
    }

    public void buildScenarioTree(TreeNode parentNode, List<ScenarioNode> scenarioNodes) {
        ScenarioNode parentScenarioNode = (ScenarioNode) parentNode.getData();
        for (ScenarioNode item : scenarioNodes) {
            if (item.getParentId() == parentScenarioNode.getNodeId()) {
                TreeNode childNode = new DefaultTreeNode("scenarioNode", item, parentNode);
                buildScenarioTree(childNode, scenarioNodes);
            }
        }
    }

    public void buildCatagoryTree(TreeNode parentNodeCat, List<Category> listCat, List<Segment> segments) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (cat.getParentId() == parentCat.getCategoryId()) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
                childNodeCat.setSelectable(false);
                buildCatagoryTree(childNodeCat, listCat, segments);
            }
        }
        for (Segment segment : segments) {
            if (segment.getCategoryId() == parentCat.getCategoryId()) {
                new DefaultTreeNode("segment", segment, parentNodeCat);
            }
        }
    }

    public void buildCatagoryTreeCondition(TreeNode parentNodeCat, List<Category> listCat, List<ConditionTable> conditionTables) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreeCondition(childNodeCat, listCat, conditionTables);
            }
        }
        for (ConditionTable conditionTable : conditionTables) {
            if (conditionTable.getCategoryId() == parentCat.getCategoryId()) {
                new DefaultTreeNode("condition", conditionTable, parentNodeCat);
            }
        }
    }

    public void buildCatagoryTreeZoneMap(TreeNode parentNodeCat, List<Category> listCat, List<ZoneMap> zoneMaps) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreeZoneMap(childNodeCat, listCat, zoneMaps);
            }
        }
        for (ZoneMap zoneMap : zoneMaps) {
            if (DataUtil.safeEqual(zoneMap.getCategoryId(), parentCat.getCategoryId())) {
                new DefaultTreeNode("zoneMap", zoneMap, parentNodeCat);
            }
        }
    }
    public void buildCatagoryTreeGeoHomeZone(TreeNode parentNodeCat, List<Category> listCat, List<GeoHomeZone> geoHomeZones) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreeGeoHomeZone(childNodeCat, listCat, geoHomeZones);
            }
        }
        for (GeoHomeZone geoHomeZone : geoHomeZones) {
            if (DataUtil.safeEqual(geoHomeZone.getCategoryId(), parentCat.getCategoryId())) {
                new DefaultTreeNode("geoHomeZone", geoHomeZone, parentNodeCat);
            }
        }
    }
    public void buildCatagoryTreeZone(TreeNode parentNodeCat, List<Category> listCat, List<Zone> zones) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreeZone(childNodeCat, listCat, zones);
            }
        }
        for (Zone zone : zones) {
            if (DataUtil.safeEqual(zone.getCategoryId(), parentCat.getCategoryId())) {
                new DefaultTreeNode("zone", zone, parentNodeCat);
            }
        }
    }

    public void buildCatagoryTreeUssdScenario(TreeNode parentNodeCat, List<Category> listCat, List<UssdScenario> ussdScenarios) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreeUssdScenario(childNodeCat, listCat, ussdScenarios);
            }
        }
        for (UssdScenario ussdScenario : ussdScenarios) {
            if (ussdScenario.getCategoryId() == parentCat.getCategoryId()) {
                new DefaultTreeNode("ussdScenario", ussdScenario, parentNodeCat);
            }
        }
    }

    public void buildCatagoryTreeOcs(TreeNode parentNodeCat, List<Category> listCat, List<OcsBehaviour> items) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreeOcs(childNodeCat, listCat, items);
            }
        }
        for (OcsBehaviour behaviour : items) {
            if (behaviour.getCategoryId() == parentCat.getCategoryId()) {
                new DefaultTreeNode("behaviour_type", behaviour, parentNodeCat);
            }
        }
    }

    public void buildCatagoryTreeScenarioTrigger(TreeNode parentNodeCat, List<Category> listCat, List<ScenarioTrigger> scenarioTriggers) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreeScenarioTrigger(childNodeCat, listCat, scenarioTriggers);
            }
        }
        for (ScenarioTrigger scenarioTrigger : scenarioTriggers) {
            if (scenarioTrigger.getCategoryId() == parentCat.getCategoryId()) {
                new DefaultTreeNode("scenariodTrigger", scenarioTrigger, parentNodeCat);
            }
        }
    }
    public void buildCatagoryTreeNotifyTrigger(TreeNode parentNodeCat, List<Category> listCat, List<NotifyTrigger> notifyTriggers) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreeNotifyTrigger(childNodeCat, listCat, notifyTriggers);
            }
        }
        for (NotifyTrigger notifyTrigger : notifyTriggers) {
            if (DataUtil.safeEqual(notifyTrigger.getCategoryId(), parentCat.getCategoryId())) {
                new DefaultTreeNode("notifyTrigger", notifyTrigger, parentNodeCat);
            }
        }
    }
    public void buildCatagoryTreeStatisticCycle(TreeNode parentNodeCat, List<Category> listCat, List<StatisticCycle> statisticCycles) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreeStatisticCycle(childNodeCat, listCat, statisticCycles);
            }
        }
        for (StatisticCycle statisticCycle : statisticCycles) {
            if (statisticCycle.getCategoryId() == parentCat.getCategoryId()) {
                new DefaultTreeNode("statistic", statisticCycle, parentNodeCat);
            }
        }
    }

    public void buildCatagoryTreePreProcessUnit(TreeNode parentNodeCat, List<Category> listCat, List<PreProcessUnit> preProcessUnits) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreePreProcessUnit(childNodeCat, listCat, preProcessUnits);
            }
        }
        for (PreProcessUnit preProcessUnit : preProcessUnits) {
            if (DataUtil.safeEqual(preProcessUnit.getCategoryId(), parentCat.getCategoryId())) {
                new DefaultTreeNode("preprocessunit", preProcessUnit, parentNodeCat);
            }
        }
    }

    public void buildCatagoryTreeNotifyTemplate(TreeNode parentNodeCat, List<Category> listCat, List<NotifyTemplate> notifyTemplates) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreeNotifyTemplate(childNodeCat, listCat, notifyTemplates);
            }
        }
        for (NotifyTemplate notifyTemplate : notifyTemplates) {
            if (DataUtil.safeEqual(notifyTemplate.getCategoryId(), parentCat.getCategoryId())) {
                new DefaultTreeNode("notifyTemplate", notifyTemplate, parentNodeCat);
            }
        }
    }

    public void buildRuleTree(TreeNode parentNodeCat, List<Category> listCat, List<Rule> rules) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (cat.getParentId() != null && cat.getParentId().equals(parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
                childNodeCat.setSelectable(false);
                buildRuleTree(childNodeCat, listCat, rules);
            }
        }
        for (Rule rule : rules) {
            if (rule.getCategoryId() == parentCat.getCategoryId()) {
                new DefaultTreeNode("rule", rule, parentNodeCat);
            }
        }
    }

    public TreeNode getRootNoteCatagory(List<Category> listCat) {
        // root node off tree (parent id is null)
        Category catRoot = null;
        for (Category it : listCat) {
            if (it.getParentId() == null) {
                catRoot = it;
            }
        }       // root node in framework (not show in view)
        TreeNode rootNode = new DefaultTreeNode(null, null);
        TreeNode catRootNode = new DefaultTreeNode("category", catRoot, rootNode);
        catRootNode.setExpanded(true);
        catRootNode.setSelectable(true);
        return rootNode;
    }

    @Override
    public TreeNode createTreeCategoryAndComponentObj(List<Category> listCat, List<Object> listObject) {

        // root node in framework (not show in view)
        TreeNode rootNode = new DefaultTreeNode(null, null);

        // root node off tree (parent id is null)
        if (listCat != null) {

            Category catRoot = null;
            for (Category it : listCat) {
                if (it.getParentId() == null) {
                    catRoot = it;
                    break;
                }
            }

            if (Objects.isNull(catRoot) && !listCat.isEmpty()) {
                listCat.stream().forEach((category) -> {
                    TreeNode catRootNode = new DefaultTreeNode("category", category, rootNode);
                    genTreeCategoryAndComponentObj(catRootNode, listCat, listObject, category);
                });
            } else {
                TreeNode catRootNode = new DefaultTreeNode("category", catRoot, rootNode);
                genTreeCategoryAndComponentObj(catRootNode, listCat, listObject, catRoot);
            }
        }
        return rootNode;
    }

    public void genTreeCategoryAndComponentObj(TreeNode parentNodeCat, List<Category> listCat, List<Object> listObject, Category parentCat) {
        // add componentNode
        if (!listObject.isEmpty() && listObject.get(0) instanceof Campaign) {
            List<Campaign> campaigns = (List<Campaign>) (Object) listObject;
            campaigns.stream()
                    .filter((camp) -> (Objects.equals(camp.getCategoryId(), parentCat.getCategoryId())))
                    .forEach((camp) -> {
                        DefaultTreeNode defaultTreeNode = new DefaultTreeNode("campaign", camp, parentNodeCat);
                    });
        } else if (!listObject.isEmpty() && listObject.get(0) instanceof Segment) {
            List<Segment> segments = (List<Segment>) (Object) listObject;
            segments.stream()
                    .filter((segment) -> (Objects.equals(segment.getCategoryId(), parentCat.getCategoryId())))
                    .forEach((segment) -> {
                        DefaultTreeNode defaultTreeNode = new DefaultTreeNode("segment", segment, parentNodeCat);
                    });
        } else if (!listObject.isEmpty() && listObject.get(0) instanceof BlackList) {
            List<BlackList> backlists = (List<BlackList>) (Object) listObject;
            backlists.stream()
                    .filter((backlist) -> (Objects.equals(backlist.getCategoryId(), parentCat.getCategoryId())))
                    .forEach((backlist) -> {
                        DefaultTreeNode defaultTreeNode = new DefaultTreeNode("backlist", backlist, parentNodeCat);
                    });
        } else if (!listObject.isEmpty() && listObject.get(0) instanceof SpecialProm) {
            List<SpecialProm> specialproms = (List<SpecialProm>) (Object) listObject;
            specialproms.stream()
                    .filter((specialprom) -> (Objects.equals(specialprom.getCategoryId(), parentCat.getCategoryId())))
                    .forEach((specialprom) -> {
                        DefaultTreeNode defaultTreeNode = new DefaultTreeNode("specialprom", specialprom, parentNodeCat);
                    });
        } else if (!listObject.isEmpty() && listObject.get(0) instanceof PromotionBlock) {
            List<PromotionBlock> promotionBlocks = (List<PromotionBlock>) (Object) listObject;
            promotionBlocks.stream()
                    .filter((promotionBlock) -> (Objects.equals(promotionBlock.getCategoryId(), parentCat.getCategoryId())))
                    .forEach((promotionBlock) -> {
                        DefaultTreeNode defaultTreeNode = new DefaultTreeNode("promotionBlock", promotionBlock, parentNodeCat);
                    });
        } else if (!listObject.isEmpty() && listObject.get(0) instanceof BehaviourDTO) {
            List<BehaviourDTO> behaviourDTOs = (List<BehaviourDTO>) (Object) listObject;
            behaviourDTOs.stream()
                    .filter((behaviour) -> (Objects.equals(behaviour.getCategoryId(), parentCat.getCategoryId())))
                    .forEach((behaviour) -> {
                        DefaultTreeNode defaultTreeNode = new DefaultTreeNode("behaviour", behaviour, parentNodeCat);
                    });
        }

        listCat.stream()
                .filter((cat) -> (Objects.equals(cat.getParentId(), parentCat.getCategoryId())))
                .forEach((cat) -> {
                    TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
                    genTreeCategoryAndComponentObj(childNodeCat, listCat, listObject, cat);
                });
    }

    @Override
    public TreeNode createTreeCategoryAndChild(List<Category> listCat, List<BaseCategory> baseCategories) {
        TreeNode catRootNode = getRootNoteCatagory(listCat);
        buildCatagoryAndChildrentTree(catRootNode.getChildren().get(0), listCat, baseCategories);
        return catRootNode;
    }

    public void buildCatagoryAndChildrentTree(TreeNode parentNodeCat, List<Category> listCat, List<BaseCategory> baseCategories) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (parentCat.getCategoryId().equals(cat.getParentId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
                buildCatagoryAndChildrentTree(childNodeCat, listCat, baseCategories);
            }
        }
        for (BaseCategory baseCategory : baseCategories) {
            if (baseCategory.getParentId() == parentCat.getCategoryId()) {
                new DefaultTreeNode(baseCategory.getTreeType(), baseCategory, parentNodeCat);
            }
        }
    }

    @Override
    public TreeNode createResultTableTree(List<Category> listCat, List<ResultTable> resultTables) {
        TreeNode catRootNode = getRootNoteCatagory(listCat);
        buildCatagoryTreeResultTable(catRootNode.getChildren().get(0), listCat, resultTables);
        return catRootNode;
    }

    public void buildCatagoryTreeResultTable(TreeNode parentNodeCat, List<Category> listCat, List<ResultTable> resultTables) {
        Category parentCat = (Category) parentNodeCat.getData();

        for (ResultTable resultTable : resultTables) {
            if (resultTable.getCategoryId().equals(parentCat.getCategoryId())) {
                new DefaultTreeNode("result", resultTable, parentNodeCat);
            }
        }
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreeResultTable(childNodeCat, listCat, resultTables);
            }
        }

    }

    @Override
    public TreeNode createPromotionBlockTree(List<Category> listCat, List<PromotionBlock> promotionBlocks) {
        TreeNode catRootNode = getRootNoteCatagory(listCat);
        buildCatagoryTreePromotionBlock(catRootNode.getChildren().get(0), listCat, promotionBlocks);
        return catRootNode;
    }

    public void buildCatagoryTreePromotionBlock(TreeNode parentNodeCat, List<Category> listCat, List<PromotionBlock> promotionBlocks) {
        Category parentCat = (Category) parentNodeCat.getData();

        for (PromotionBlock promotionBlock : promotionBlocks) {
            if (promotionBlock.getCategoryId().equals(parentCat.getCategoryId())) {
                new DefaultTreeNode("block", promotionBlock, parentNodeCat);
            }
        }
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreePromotionBlock(childNodeCat, listCat, promotionBlocks);
            }
        }
    }

    @Override
    public void collapseAll(TreeNode node) {
        setExpandedRecursively(node, false);
    }

    @Override
    public void expandAll(TreeNode node) {
        setExpandedRecursively(node, true);
    }

    private void setExpandedRecursively(final TreeNode node, final boolean expanded) {
        node.getChildren().stream().forEach((child) -> {
            setExpandedRecursively(child, expanded);
        });
        node.setExpanded(expanded);
    }

}
