/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.CriteriaValueDAO;
import vn.viettel.campaign.entities.CriteriaValue;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.KpiProcessorCriteriaValueMap;
import vn.viettel.campaign.service.CriteriaValueService;

/**
 *
 * @author SON
 */
@Service
public class CriteriaValueServiceImpl implements CriteriaValueService {

    @Autowired
    private CriteriaValueDAO criteriaValueDAO;

    @Override
    public List<CriteriaValue> getLstCriteriaValueByKpiProcessorId(Long id) {
        return criteriaValueDAO.getLstCriteriaValueByKpiProcessorId(id);
    }

    @Override
    public CriteriaValue onSaveCriteriaValue(CriteriaValue criteriaValue) {
        criteriaValueDAO.onSave(criteriaValue);
        return criteriaValue;
    }

    @Override
    public CriteriaValue onUpdateCriteriaValue(CriteriaValue criteriaValue) {
        criteriaValueDAO.onUpdate(criteriaValue);
        return criteriaValue;
    }

    @Override
    public Boolean onDeleteCriteriaValue(Long id) {
        if (id != null) {
            criteriaValueDAO.onDelete(id);
            return true;
        }
        return false;
    }

    @Override
    public CriteriaValue getNextSequense() {
        return criteriaValueDAO.getNextSequense();
    }

    @Override
    public CriteriaValue findOneById(Long id) {
        return criteriaValueDAO.findOneById(id);
    }

    @Override
    public CriteriaValue onSaveOrUpdate(CriteriaValue criteriaValue) {
        criteriaValueDAO.onSaveOrUpdateCriteriaValue(criteriaValue);
        return criteriaValue;
    }

    @Override
    public void onSaveKpiCriteriaValueMap(KpiProcessorCriteriaValueMap kpiProcessorCriteriaValueMap) {
        criteriaValueDAO.onSaveKpiCriteriaValueMap(kpiProcessorCriteriaValueMap);
    }

    @Override
    public CriteriaValue checkExitsCriteriaName(String name, Long id,Long processorId) {
        return criteriaValueDAO.checkExitsCriteriaName(name, id, processorId);
    }

    @Override
    public List<KpiProcessorCriteriaValueMap> getLstKpiProcessorCriteriaValueMap() {
        return criteriaValueDAO.getLstKpiProcessorCriteriaValueMap().isEmpty() ? new ArrayList<>() : criteriaValueDAO.getLstKpiProcessorCriteriaValueMap();
    }

    @Override
    public Boolean onDeleteCriteriaValue(Long processorId, Long CriteriaValueId) {
       if(processorId != null && CriteriaValueId != null){
           criteriaValueDAO.onDeleteCriteriaValue(processorId, CriteriaValueId);
           return true;
       }
       return false;
    }

    @Override
    public Boolean checkDeleteCriteriaValue(Long kpiId, String name) {
        if(kpiId != null && name != null && !criteriaValueDAO.checkDeleteCriteriaValue(kpiId, name).isEmpty()){
            return true;
        }
        return false;
    }

    @Override
    public List<FuzzyRule> getListFuzzyForCriteriaValue(Long kpiId) {
       return criteriaValueDAO.getListFuzzyForCriteriaValue(kpiId).isEmpty() ? new ArrayList<>() : criteriaValueDAO.getListFuzzyForCriteriaValue(kpiId);
    }

}
