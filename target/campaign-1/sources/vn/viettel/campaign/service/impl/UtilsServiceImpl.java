/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.controller.CampaignOnlineController;
import vn.viettel.campaign.service.UtilsService;

/**
 *
 * @author ConKC
 */
@Service
public class UtilsServiceImpl implements UtilsService, Serializable {

    public static long ALLOW_TIME_REQUEST = 24 * 60 * 60 * 1000L;

    private static final String FILE_PROPERTIES = "/locale/message.properties";
    private static final String SPRING_PROPERTIES = "/spring.properties";
    private static final HashMap<String, String> SPRING_PROPERTIES_MAP = new HashMap<>();

    public static String getProperty(String key) {
        String value = SPRING_PROPERTIES_MAP.get(key);
        return value == null ? StringUtils.EMPTY : value;
    }

    @Override
    public boolean loadProperty() {
        SPRING_PROPERTIES_MAP.clear();
        try {
            Properties prop = new Properties();
            //prop.load(getClass().getClassLoader().getResourceAsStream(SPRING_PROPERTIES));
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(SPRING_PROPERTIES);
            prop.load(inputStream);
            for (String key : prop.stringPropertyNames()) {
                String property = prop.getProperty(key);
                if (StringUtils.isNotBlank(property)) {
                    if (key.contains("path")) {
                        String value = property.trim();
                        if (!value.endsWith("/")) {
                            value += "/";
                        }
                        SPRING_PROPERTIES_MAP.put(key, value);
                    } else {
                        SPRING_PROPERTIES_MAP.put(key, property.trim());
                    }
                }
            }
            String value = SPRING_PROPERTIES_MAP.get(Constants.SpringProperty.ALLOW_TIME_REQUEST);
            if (value != null && value.matches("[0-9].*")) {
                ALLOW_TIME_REQUEST = Long.valueOf(value);
            }
            inputStream.close();
        } catch (IOException ex) {
            Logger.getLogger(UtilsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    @Override
    public String getTex(final String key) {
        if (StringUtils.isEmpty(key)) {
            return StringUtils.EMPTY;
        }
        try {
            Properties prop = new Properties();
            //prop.load(getClass().getClassLoader().getResourceAsStream(FILE_PROPERTIES));
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(FILE_PROPERTIES);
            prop.load(inputStream);
            inputStream.close();
            return prop.getProperty(key);
        } catch (IOException ex) {
            Logger.getLogger(CampaignOnlineController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return StringUtils.EMPTY;
    }

    @Override
    public String getTex(final String key, final String defaultValue) {
        try {
            Properties prop = new Properties();
            //prop.load(getClass().getClassLoader().getResourceAsStream(FILE_PROPERTIES));
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(FILE_PROPERTIES);
            prop.load(inputStream);
            inputStream.close();
            return prop.getProperty(key, defaultValue);
        } catch (IOException ex) {
            Logger.getLogger(CampaignOnlineController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return StringUtils.EMPTY;
    }

    @Override
    public boolean getConfig(final String key) {
        try {
            Properties prop = new Properties();
            //prop.load(getClass().getClassLoader().getResourceAsStream(SPRING_PROPERTIES));
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(SPRING_PROPERTIES);
            prop.load(inputStream);
            String value = prop.getProperty(key, "true");
            inputStream.close();
            return Boolean.valueOf(value);
        } catch (IOException ex) {
            Logger.getLogger(CampaignOnlineController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public Object getObject(final String key, final String defaultValue) {
        try {
            Properties prop = new Properties();
            //prop.load(getClass().getClassLoader().getResourceAsStream(SPRING_PROPERTIES));
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(SPRING_PROPERTIES);
            prop.load(inputStream);
            Object value = prop.getProperty(key, defaultValue);
            inputStream.close();
            return value;
        } catch (IOException ex) {
            Logger.getLogger(CampaignOnlineController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public Object getObject(final String key) {
        try {
            Properties prop = new Properties();
            // prop.load(getClass().getClassLoader().getResourceAsStream(SPRING_PROPERTIES));

            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(SPRING_PROPERTIES);
            prop.load(inputStream);

            Object value = prop.getProperty(key);
            inputStream.close();
            return value;
        } catch (IOException ex) {
            Logger.getLogger(CampaignOnlineController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
