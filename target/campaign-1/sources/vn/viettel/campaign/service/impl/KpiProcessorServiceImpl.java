/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.KpiProcessorDAO;
import vn.viettel.campaign.entities.KpiProcessor;
import vn.viettel.campaign.service.KpiProcessorService;

/**
 *
 * @author SON
 */
@Service
public class KpiProcessorServiceImpl implements KpiProcessorService{

    @Autowired
    private KpiProcessorDAO kpiProcessorDAO;
    
    @Override
    public List<KpiProcessor> getLstKpiProcessor() {
        return kpiProcessorDAO.getLstKpiProcessor();
    }

    @Override
    public KpiProcessor findOneById(Long id) {
        return id != null ? kpiProcessorDAO.findOneById(id) : new KpiProcessor();
    }

    @Override
    public KpiProcessor onSaveOrUpdateKpiProcessor(KpiProcessor kpiProcessor) {
        if(kpiProcessor != null){
            kpiProcessorDAO.onSaveOrUpdateKpiProcessor(kpiProcessor);
            return kpiProcessorDAO.findOneById(kpiProcessor.getProcessorId());
        }
        return new KpiProcessor();
    }

    @Override
    public Boolean onDeleteKpiProcessor(KpiProcessor kpiProcessor) {
        if(kpiProcessor != null){
            kpiProcessorDAO.onDeleteKpiProcessor(kpiProcessor);
            return true;
        }
        return false;
    }

    @Override
    public KpiProcessor getNextSequense() {
        return kpiProcessorDAO.getNextSequense();
    }

    @Override
    public KpiProcessor onSaveKpiProcessor(KpiProcessor kpiProcessor) {
        if(kpiProcessor != null){
            kpiProcessorDAO.onSaveKpiProcessor(kpiProcessor);
            return kpiProcessorDAO.findOneById(kpiProcessor.getProcessorId());
        }
        return new KpiProcessor();
    }

    @Override
    public KpiProcessor onUpdateKpiProcessor(KpiProcessor kpiProcessor) {
        if(kpiProcessor != null){
            kpiProcessorDAO.onUpdateKpiProcessor(kpiProcessor);
            return kpiProcessorDAO.findOneById(kpiProcessor.getProcessorId());
        }
        return new KpiProcessor();
    }

    @Override
    public KpiProcessor checkExitsProcessName(String name, Long id) {
        return kpiProcessorDAO.checkExitsProcessName(name,id);
    }

    @Override
    public KpiProcessor checkExitsCriteriaName(String name, Long id) {
       return kpiProcessorDAO.checkExitsCriteriaName(name,id);
    }

    @Override
    public Boolean checkDeleteKpiProcessor(KpiProcessor kpiProcessor) {
        if(kpiProcessor != null && !kpiProcessorDAO.checkDeleteKpiProcessor(kpiProcessor).isEmpty()){
            return true;
        }
        return false;
    }
    
}
