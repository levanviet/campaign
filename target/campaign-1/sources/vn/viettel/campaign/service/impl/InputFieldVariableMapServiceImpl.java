/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.InputFieldVariableMapDAO;
import vn.viettel.campaign.entities.InputFieldVariableMap;
import vn.viettel.campaign.entities.KpiProcessor;
import vn.viettel.campaign.service.InputFieldVariableMapService;

/**
 *
 * @author SON
 */
@Service
public class InputFieldVariableMapServiceImpl implements InputFieldVariableMapService{

    @Autowired
    private InputFieldVariableMapDAO inputFieldVariableMapDAO;
    
    @Override
    public List<InputFieldVariableMap> getLstInputFieldVariableMapByTypeAndId(int type, Long id) {
        return inputFieldVariableMapDAO.getLstInputFieldVariableMapByTypeAndId(type, id);
    }

    @Override
    public InputFieldVariableMap onSaveOrUpdateInputFieldVariableMap(InputFieldVariableMap inputFieldVariableMap) {
        if(inputFieldVariableMap != null){
            inputFieldVariableMapDAO.onSaveOrUpdateInputFieldVariableMap(inputFieldVariableMap);
        }
        return new InputFieldVariableMap();
    }

    @Override
    public Boolean onDeleteInputFieldVariableMap(int type, Long id) {
        if(id != null){
            inputFieldVariableMapDAO.onDeleteInputFieldVariableMap(type,id);
            return true;
        }
        return false;
    }

    @Override
    public InputFieldVariableMap findOneById(Long id) {
        return inputFieldVariableMapDAO.findOneById(id);
    }

    @Override
    public InputFieldVariableMap getNextSequence() {
        return inputFieldVariableMapDAO.getNextSequence();
    }
    
}
