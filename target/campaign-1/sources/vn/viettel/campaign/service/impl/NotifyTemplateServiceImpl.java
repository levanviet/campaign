package vn.viettel.campaign.service.impl;

import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.constants.TreeNodeType;
import vn.viettel.campaign.dao.LanguageDAOInterface;
import vn.viettel.campaign.dao.UssdScenarioDAOInterface;
import vn.viettel.campaign.dao.notifytemplate.NotifyContentDAO;
import vn.viettel.campaign.dao.notifytemplate.NotifyTemplateDAO;
import vn.viettel.campaign.dto.NotifyContentDTO;
import vn.viettel.campaign.dto.NotifyTemplateDetailDTO;
import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.entities.Language;
import vn.viettel.campaign.entities.NotifyContent;
import vn.viettel.campaign.entities.NotifyTemplate;
import vn.viettel.campaign.service.NotifyTemplateService;
import vn.viettel.campaign.service.TreeBaseService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import vn.viettel.campaign.common.DataUtil;

@Service("notifyTemplateService")
public class NotifyTemplateServiceImpl extends TreeBaseService<NotifyTemplate> implements NotifyTemplateService {

    @Autowired
    NotifyTemplateDAO notifyTemplateDAO;

    @Autowired
    NotifyContentDAO notifyContentDAO;

    @Autowired
    UssdScenarioDAOInterface ussdScenarioDAOInterface;

    @Autowired
    LanguageDAOInterface languageDAOInterface;

    List<Language> languages;

    private List<NotifyTemplate> getItemByCategory(List<Long> categoryIds) {
        return notifyTemplateDAO.getTemplateByCategory(categoryIds);
    }

    @Override
    public TreeNode getTree(List<Category> categories) {
        List<Long> categoryIds = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> categoryIds.add(item.getCategoryId()));
            categories.sort(Comparator.comparing(Category::getName));
        }
        List<NotifyTemplate> items = this.getItemByCategory(categoryIds);
        TreeNode treeNode = this.createTree(categories, items, TreeNodeType.document);
        return treeNode;
    }

    @Override
    public NotifyTemplateDetailDTO getDetail(Long id) {
        NotifyTemplate notifyTemplate = notifyTemplateDAO.getOneById(id);
        List<NotifyContent> notifyContents = notifyContentDAO.getAllByTemplateId(id);
        return this.getDTO(notifyTemplate, notifyContents);
    }

    private NotifyTemplateDetailDTO getDTO(NotifyTemplate notifyTemplate, List<NotifyContent> notifyContents) {
        NotifyTemplateDetailDTO templateDetailDTO = NotifyTemplateDetailDTO.builder()
                .id(notifyTemplate.getId())
                .categoryId(notifyTemplate.getCategoryId())
                .templateType(notifyTemplate.getTemplateType())
                .description(notifyTemplate.getDescription())
                .name(notifyTemplate.getName())
                .build();

        List<NotifyContentDTO> contentsDtos = new ArrayList<>();
        for (NotifyContent content : notifyContents) {
            NotifyContentDTO contentDTO = NotifyContentDTO.builder()
                    .id(content.getId())
                    .content(content.getContent())
                    .language(content.getLanguage())
                    .notifyTemplateId(content.getNotifyTemplateId())
                    .isContentNew(Boolean.FALSE)
                    .build();

            contentsDtos.add(contentDTO);
        }
        templateDetailDTO.setContents(contentsDtos);
        return templateDetailDTO;
    }

    @Override
    public List<Language> getLanguages() {
        try {
            List<Language> list = languageDAOInterface.getAll(Language.class);
            if (!list.isEmpty()) {
                list.sort(Comparator.comparing(Language::getLanguageName));
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.EMPTY_LIST;
    }

    @Override
    public NotifyContent saveContent(NotifyContent content) {
        return notifyContentDAO.save(content);
    }

    @Override
    public Language getLanguageById(Long id) {
        try {
            return languageDAOInterface.findById(Language.class, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void removeContentById(Long contentId) {
        notifyContentDAO.removeContent(contentId);
    }

    @Override
    public NotifyTemplate saveTemplate(NotifyTemplate template) {
        DataUtil.trimObject(template);
//        template.setTemplateType(1);
        return notifyTemplateDAO.save(template);
    }

    @Override
    public void removeTemplateById(Long id) {
        notifyTemplateDAO.removeById(id);
        notifyContentDAO.deleteAllByTemplateId(id);
    }

    @Override
    public Long getNextId() {

        return null;
    }

    @Override
    public NotifyTemplate findByName(String name, Long id) {
        return notifyTemplateDAO.findOnByName(name, id);
    }

    @Override
    public List<NotifyContent> updateNotifyContent(Long templateIdFucus, List<NotifyContent> contents) {
        notifyContentDAO.deleteAllByTemplateId(templateIdFucus);
        notifyContentDAO.saveContents(contents);
        return null;
    }

    @Override
    public boolean checkExistingTemplateByCategoryId(Long categoryId) {
        List<Long> categoryIds = new ArrayList<>();
        categoryIds.add(categoryId);
        List<NotifyTemplate> templates = notifyTemplateDAO.getTemplateByCategory(categoryIds);
        if (templates == null || templates.isEmpty()) {
            return false;
        }
        return true;
    }
}
