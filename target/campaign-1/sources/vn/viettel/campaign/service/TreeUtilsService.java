/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import org.primefaces.model.TreeNode;
import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.entities.TreeItemBase;

/**
 *
 * @author ABC
 */
public interface TreeUtilsService {
    public <T extends TreeItemBase> TreeNode createTreeCategoryAndComponentCampaignEvaluation(List<Category> listCat, List<T> listT);
	public <T extends TreeItemBase> TreeNode createTreeCategoryAndComponentObject(List<Category> listCat, List<T> listT);
}
