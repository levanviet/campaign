package vn.viettel.campaign.service;

import vn.viettel.campaign.entities.*;

import java.util.List;

/**
 * @author truongbx
 * @date 9/15/2019
 */
public interface OCSBehaviourInterface extends BaseBusinessInterface<OcsBehaviour> {
	OcsBehaviour getNextSequense();
//	for main screen notification
	<T> List<T> getLstNotifyTrigger(List<Long> lstCategory );
	List<PostNotifyTrigger> getLstPostNotifyTrigger(long behaviourId);
	public boolean onSaveOCSBehaviour(OcsBehaviour ocsBehaviour , List<OcsBehaviourFields> lstField,
									  List<OcsBehaviourExtendFields> lstExtendField,List<PostNotifyTrigger> lstNotifyTrigger);
	public boolean onUpdateOCSBehaviour(OcsBehaviour ocsBehaviour , List<OcsBehaviourFields> lstField,
									  List<OcsBehaviourExtendFields> lstExtendField,List<PostNotifyTrigger> lstNotifyTrigger);
	public boolean onDeleteOcsBehaviour(Long behaviourId);
	List<OcsBehaviourFields> getLstBehaviourField(Long behaviourId,Long templateId);
	List<OcsBehaviourTemplateFields> getLstBehaviourTemplateField(Long templateId);
	List<OcsBehaviourExtendFields> getLstBehaviourExtendField(Long behaviourId,Long templateId);
	List<OcsBehaviourTemplateExtendFields> getLstBehaviourTemplateExtendField(Long templateId);
	public  boolean checkExisOCSBehaviourName(String BehaviourName,Long behaviourId);
	public  boolean validateDeleteNotifyTrigger(Long notifyId);
	public List<NotifyContent> getNotifyContentByNotifyId(Long notifyId);
	boolean checkOcsUseInPromotionBlock(Long ocsId);
	boolean checkOcsUseInUSSDScenario(Long ocsId);
//	boolean checkExtendFieldName(String name);
//	public void insertExtendField(ExtendField extendField);
//	public boolean onSaveBehaviourTemplate(OcsBehaviourTemplate ocsBehaviourTemplate,
//										   List<OcsBehaviourTemplateFields> lstField, List<OcsBehaviourTemplateExtendFields> lstExtendFields);
//	public boolean onUpdateBehaviourTemplate(OcsBehaviourTemplate ocsBehaviourTemplate,
//											 List<OcsBehaviourTemplateFields> lstField, List<OcsBehaviourTemplateExtendFields> lstExtendFields);
//	public boolean onDeleteBehaviourTemplate(Long templateId);
}
