/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.Segment;

/**
 *
 * @author ConKC
 */
public interface SegmentService extends BaseBusinessInterface<Segment> {

    List<Segment> findSegmentByCategory(final List<Long> categoryIds);

    List<Segment> findSegmentByName(final String segmentName);

    long onSaveSegment(Segment segment);

    boolean checkDuplicate(final String name, final Long id);

    boolean checkDuplicatePath(final String path, final Long id);
}
