/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.PpuEvaluation;
import vn.viettel.campaign.entities.ProcessValue;

/**
 *
 * @author admin
 */
public interface ProcessValueService {

    ProcessValue onSaveOrUpdateProcessValue(ProcessValue processValue);

    boolean onDeleteProcessValue(PpuEvaluation ppuEvaluation);

    boolean onDeleteProcessValue(Long processValueId, Long ppuId);

    List<ProcessValue> getLstProcessValue();

    List<ProcessValue> getLstProcessValue(Long id);

    ProcessValue getNextSequence();

    Boolean checkSavePreProcessValueMap(Long processValueId, Long kpiId);

    Boolean checkDeleteProcessValue(Long ppuId, String name);
    
    List<FuzzyRule> getListFuzzyForProcessValue(Long ppuId);
}
