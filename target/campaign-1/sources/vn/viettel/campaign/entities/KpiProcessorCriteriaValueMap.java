/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SON
 */
@Entity
@Table(name = "kpi_processor_criteria_value_map")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KpiProcessorCriteriaValueMap.findAll", query = "SELECT k FROM KpiProcessorCriteriaValueMap k")
    , @NamedQuery(name = "KpiProcessorCriteriaValueMap.findById", query = "SELECT k FROM KpiProcessorCriteriaValueMap k WHERE k.id = :id")
    , @NamedQuery(name = "KpiProcessorCriteriaValueMap.findByProcessorId", query = "SELECT k FROM KpiProcessorCriteriaValueMap k WHERE k.processorId = :processorId")
    , @NamedQuery(name = "KpiProcessorCriteriaValueMap.findByCriteriaValueId", query = "SELECT k FROM KpiProcessorCriteriaValueMap k WHERE k.criteriaValueId = :criteriaValueId")})
public class KpiProcessorCriteriaValueMap implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "PROCESSOR_ID")
    private long processorId;
    @Basic(optional = false)
    @Column(name = "CRITERIA_VALUE_ID")
    private long criteriaValueId;

    public KpiProcessorCriteriaValueMap() {
    }

    public KpiProcessorCriteriaValueMap(Long id) {
        this.id = id;
    }

    public KpiProcessorCriteriaValueMap(Long id, long processorId, long criteriaValueId) {
        this.id = id;
        this.processorId = processorId;
        this.criteriaValueId = criteriaValueId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getProcessorId() {
        return processorId;
    }

    public void setProcessorId(long processorId) {
        this.processorId = processorId;
    }

    public long getCriteriaValueId() {
        return criteriaValueId;
    }

    public void setCriteriaValueId(long criteriaValueId) {
        this.criteriaValueId = criteriaValueId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KpiProcessorCriteriaValueMap)) {
            return false;
        }
        KpiProcessorCriteriaValueMap other = (KpiProcessorCriteriaValueMap) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vn.viettel.campaign.entities.KpiProcessorCriteriaValueMap[ id=" + id + " ]";
    }
    
}
