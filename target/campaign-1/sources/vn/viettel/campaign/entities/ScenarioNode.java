package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author truongbx
 */
@Entity
@Table(name = "scenario_node")
public class ScenarioNode {

    @Id
    @Column(name = "NODE_ID")
    private long nodeId;
    @Column(name = "NODE_NAME")
    private String nodeName;
    @Column(name = "IS_ROOT")
    private long isRoot;
    @Column(name = "PARENT_ID")
    private long parentId;
    @Column(name = "SYNTAX")
    private String syntax;
    @Column(name = "OCS_BEHAVIOUR_ID")
    private long ocsBehaviourId;
    @Column(name = "USSD_SCENARIO_ID")
    private long ussdScenarioId;
    @Column(name = "IS_BACK")
    private long isBack;
    @Column(name = "IS_BACK_ROOT")
    private long isBackRoot;
    @Transient
    private String name;
    @Transient
    private String ocsBehaviourName;
    @Transient
    private String parName;
    @Transient
    private List<NoteContent> lstNoteContent;

    @Transient
    private boolean bBackRoot;
    @Transient
    private boolean bBack;
    @Transient
    private boolean bRoot;

    public boolean isbBackRoot() {
        return isBackRoot == 1;
    }

    public void setbBackRoot(boolean bBackRoot) {
        isBackRoot = bBackRoot ? 1 : 0;
        this.bBackRoot = bBackRoot;
    }

    public boolean isbBack() {
        return isBack == 1;
    }

    public void setbBack(boolean bBack) {
        isBack = bBack ? 1 : 0;
        this.bBack = bBack;
    }

    public boolean isbRoot() {
        return isRoot == 1;
    }

    public void setbRoot(boolean bRoot) {
        isRoot = bRoot ? 1 : 0;
        this.bRoot = bRoot;
    }

    public List<NoteContent> getLstNoteContent() {
        return lstNoteContent;
    }

    public void setLstNoteContent(List<NoteContent> lstNoteContent) {
        this.lstNoteContent = lstNoteContent;
    }

    public String getName() {
        return this.nodeName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getNodeId() {
        return nodeId;
    }

    public void setNodeId(long nodeId) {
        this.nodeId = nodeId;
    }


    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }


    public long getIsRoot() {
        return isRoot;
    }

    public void setIsRoot(long isRoot) {
        this.isRoot = isRoot;
    }


    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }


    public String getSyntax() {
        return syntax;
    }

    public void setSyntax(String syntax) {
        this.syntax = syntax;
    }


    public long getOcsBehaviourId() {
        return ocsBehaviourId;
    }

    public void setOcsBehaviourId(long ocsBehaviourId) {
        this.ocsBehaviourId = ocsBehaviourId;
    }


    public long getUssdScenarioId() {
        return ussdScenarioId;
    }

    public void setUssdScenarioId(long ussdScenarioId) {
        this.ussdScenarioId = ussdScenarioId;
    }


    public long getIsBack() {
        return isBack;
    }

    public void setIsBack(long isBack) {
        this.isBack = isBack;
    }


    public long getIsBackRoot() {
        return isBackRoot;
    }

    public void setIsBackRoot(long isBackRoot) {
        this.isBackRoot = isBackRoot;
    }

    public String getOcsBehaviourName() {
        return ocsBehaviourName;
    }

    public void setOcsBehaviourName(String ocsBehaviourName) {
        this.ocsBehaviourName = ocsBehaviourName;
    }

    public String getParName() {
        return parName;
    }

    public void setParName(String parName) {
        this.parName = parName;
    }
    public String getFilter() {
        return getName();
    }
}
