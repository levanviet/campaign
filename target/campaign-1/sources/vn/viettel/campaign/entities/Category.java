/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author ConKC
 */
@Entity
@Table(name = "CATEGORY")
public class Category implements Serializable {

    @Id
    @Column(name = "CATEGORY_ID")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long categoryId;
    @Column(name = "CATEGORY_NAME")
    private String name;
    @Column(name = "CATEGORY_TYPE")
    private Integer categoryType;
    @Column(name = "PARENT_ID")
    private Long parentId;

    public Category() {
    }

    public Category(Long categoryId, String name, Integer categoryType, Long parentId) {
        this.categoryId = categoryId;
        this.name = name;
        this.categoryType = categoryType;
        this.parentId = parentId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(Integer categoryType) {
        this.categoryType = categoryType;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getFilter() {
        return "";
    }
}
