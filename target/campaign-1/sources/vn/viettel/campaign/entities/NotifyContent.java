package vn.viettel.campaign.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "notify_content")
@Getter
@Setter
public class NotifyContent {

    @Id
    @Column(name = "NOTIFY_CONTENT_ID")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NOTIFY_TEMPLATE_ID")
    private Long notifyTemplateId;

//    @Column(name = "LANGUAGE_ID")
//    private Integer languageId;

    @Column(name = "CONTENT")
    private String content;


    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "LANGUAGE_ID")
    private Language language;
    @Transient
    private Integer notifyLanguageId;
    @Transient
    private long contentId ;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNotifyTemplateId() {
        return notifyTemplateId;
    }

    public void setNotifyTemplateId(Long notifyTemplateId) {
        this.notifyTemplateId = notifyTemplateId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Integer getNotifyLanguageId() {
        return notifyLanguageId;
    }

    public void setNotifyLanguageId(Integer notifyLanguageId) {
        this.notifyLanguageId = notifyLanguageId;
    }

    public long getContentId() {
        return contentId;
    }

    public void setContentId(long contentId) {
        this.contentId = contentId;
    }
}
