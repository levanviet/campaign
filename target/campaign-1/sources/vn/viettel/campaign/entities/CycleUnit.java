package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "cycle_unit")
public class CycleUnit {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private long id;
  @Column(name = "NAME")
  private String name;
  @Column(name = "NOTE")
  private String note;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }
  public String getFilter() {
    return getName();
  }
}
