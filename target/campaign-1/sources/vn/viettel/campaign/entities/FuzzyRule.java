/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author ABC
 */
@Entity
@Table(name = "fuzzy_rule")
public class FuzzyRule implements Serializable{
    private static final Long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "FUZZY_RULE_ID")
    private Long fuzzyRuleId;
    @Basic(optional = false)
    @Column(name = "OWNER_ID")
    private Long ownerId;
    @Column(name = "CAMPAIGN_EVALUATION_INFO_ID")
    private Long campaignEvaluationInfoId;
    @Basic(optional = false)
    @Column(name = "CLASS_ID")
    private Long classId;
    @Basic(optional = false)
    @Column(name = "RULE_LEVEL")
    private Long ruleLevel;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "WEIGHT")
    private Double weight;
    @Basic(optional = false)
    @Column(name = "DISPLAY_EXPRESSION")
    private String displayExpression;
    @Basic(optional = false)
    @Column(name = "EXPRESSION")
    private String expression;
    @Basic(optional = false)
    @Column(name = "FUZZY_OPERATOR")
    private Long fuzzyOperator;
    @Column(name = "DESCRIPTION")
    private String description;

    @Transient
    private String className;
 
    
    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
    
    
    public FuzzyRule() {
    }

    public FuzzyRule(Long fuzzyRuleId, Long ownerId, Long campaignEvaluationInfoId, Long classId, Long ruleLevel, Double weight, String displayExpression, String expression, Long fuzzyOperator, String description) {
        this.fuzzyRuleId = fuzzyRuleId;
        this.ownerId = ownerId;
        this.campaignEvaluationInfoId = campaignEvaluationInfoId;
        this.classId = classId;
        this.ruleLevel = ruleLevel;
        this.weight = weight;
        this.displayExpression = displayExpression;
        this.expression = expression;
        this.fuzzyOperator = fuzzyOperator;
        this.description = description;
    }

    public Long getFuzzyRuleId() {
        return fuzzyRuleId;
    }

    public void setFuzzyRuleId(Long fuzzyRuleId) {
        this.fuzzyRuleId = fuzzyRuleId;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Long getCampaignEvaluationInfoId() {
        return campaignEvaluationInfoId;
    }

    public void setCampaignEvaluationInfoId(Long campaignEvaluationInfoId) {
        this.campaignEvaluationInfoId = campaignEvaluationInfoId;
    }

    public Long getClassId() {
        return classId;
    }

    public void setClassId(Long classId) {
        this.classId = classId;
    }

    public Long getRuleLevel() {
        return ruleLevel;
    }

    public void setRuleLevel(Long ruleLevel) {
        this.ruleLevel = ruleLevel;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getDisplayExpression() {
        return displayExpression;
    }

    public void setDisplayExpression(String displayExpression) {
        this.displayExpression = displayExpression;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public Long getFuzzyOperator() {
        return fuzzyOperator;
    }

    public void setFuzzyOperator(Long fuzzyOperator) {
        this.fuzzyOperator = fuzzyOperator;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
