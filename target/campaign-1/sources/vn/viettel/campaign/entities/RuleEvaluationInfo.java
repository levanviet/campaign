/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author ABC
 */
@Entity
@Table(name = "rule_evaluation_info")
public class RuleEvaluationInfo implements Serializable {

    private static final Long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "RULE_EVALUATION_INFO_ID")
    private Long ruleEvaluationInfoId;
    @Basic(optional = false)
    @Column(name = "OWNER_ID")
    private Long ownerId;
    @Basic(optional = false)
    @Column(name = "ASSESSMENT_RULE_ID")
    private Long assessmentRuleId;
    @Basic(optional = false)
    @Column(name = "RULE_REASIONING_METHOD")
    private Long ruleReasioningMethod;
    @Basic(optional = false)
    @Column(name = "RULE_WEIGHT")
    private Double ruleWeight;
    @Basic(optional = false)
    @Column(name = "RULE_LEVEL")
    private Long ruleLevel;
    @Column(name = "DESCRIPTION")
    private String description;

    @Transient
    private String campaignEvaluationInfoName;
    @Transient
    private String ruleName;

    public String getCampaignEvaluationInfoName() {
        return campaignEvaluationInfoName;
    }

    public void setCampaignEvaluationInfoName(String campaignEvaluationInfoName) {
        this.campaignEvaluationInfoName = campaignEvaluationInfoName;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public RuleEvaluationInfo(Long ruleEvaluationInfoId, Long ownerId, Long assessmentRuleId, Long ruleReasioningMethod, Double ruleWeight, Long ruleLevel, String description) {
        this.ruleEvaluationInfoId = ruleEvaluationInfoId;
        this.ownerId = ownerId;
        this.assessmentRuleId = assessmentRuleId;
        this.ruleReasioningMethod = ruleReasioningMethod;
        this.ruleWeight = ruleWeight;
        this.ruleLevel = ruleLevel;
        this.description = description;
    }

    public RuleEvaluationInfo() {
    }

    public Long getRuleEvaluationInfoId() {
        return ruleEvaluationInfoId;
    }

    public void setRuleEvaluationInfoId(Long ruleEvaluationInfoId) {
        this.ruleEvaluationInfoId = ruleEvaluationInfoId;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Long getAssessmentRuleId() {
        return assessmentRuleId;
    }

    public void setAssessmentRuleId(Long assessmentRuleId) {
        this.assessmentRuleId = assessmentRuleId;
    }

    public Long getRuleReasioningMethod() {
        return ruleReasioningMethod;
    }

    public void setRuleReasioningMethod(Long ruleReasioningMethod) {
        this.ruleReasioningMethod = ruleReasioningMethod;
    }

    public Double getRuleWeight() {
        return ruleWeight;
    }

    public void setRuleWeight(Double ruleWeight) {
        this.ruleWeight = ruleWeight;
    }

    public Long getRuleLevel() {
        return ruleLevel;
    }

    public void setRuleLevel(Long ruleLevel) {
        this.ruleLevel = ruleLevel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
