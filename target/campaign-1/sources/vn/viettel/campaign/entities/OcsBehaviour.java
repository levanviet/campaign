package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "ocs_behaviour")
public class OcsBehaviour extends BaseCategory {

	@Id
	@Column(name = "BEHAVIOUR_ID")
	private long behaviourId;
	@Column(name = "BEHAVIOUR_NAME")
	private String behaviourName;
	@Column(name = "DESCRIPTION")
	private String description;
	@Column(name = "CATEGORY_ID")
	private long categoryId;
	@Column(name = "TEMPLATE_ID")
	private long templateId;
	@Column(name = "NOTIFY_ID")
	private Long notifyId;
	@Transient
	private String templateName;
	@Transient
	private String notifyName;
	public long getBehaviourId() {
		return behaviourId;
	}

	public void setBehaviourId(long behaviourId) {
		this.behaviourId = behaviourId;
	}


	public String getBehaviourName() {
		return behaviourName;
	}

	public void setBehaviourName(String behaviourName) {
		this.behaviourName = behaviourName;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}


	public long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(long templateId) {
		this.templateId = templateId;
	}


	public Long getNotifyId() {
		return notifyId == null ? 0L : notifyId;
	}

	public void setNotifyId(Long notifyId) {
		this.notifyId = notifyId;
	}

	@Override
	public long getParentId() {
		return getCategoryId();
	}

	@Override
	public void setParentId(Long id) {
		setCategoryId(id);
	}

	@Override
	public String getTreeType() {
		return "ocs_behaviour";
	}

	@Override
	public String getName() {
		return this.getBehaviourName();
	}

	@Override
	public Class getParentType() {
		return Category.class;
	}

	public void asMap(OcsBehaviour data) {
		this.behaviourId = data.getBehaviourId();
		this.behaviourName = data.getBehaviourName();
		this.description = data.getDescription();
		this.categoryId = data.getCategoryId();
		this.templateId = data.getTemplateId();
		this.notifyId = data.getNotifyId();
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getNotifyName() {
		return notifyName;
	}

	public void setNotifyName(String notifyName) {
		this.notifyName = notifyName;
	}
	public String getFilter() {
		return getBehaviourName();
	}
}
