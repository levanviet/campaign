package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "cdr_service")
public class CdrService {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CDR_SERVICE_ID")
	private long cdrServiceId;
	@Column(name = "CDR_SERVICE_CODE")
	private String cdrServiceCode;
	@Column(name = "NAME")
	private String name;
	@Column(name = "REMARK")
	private String remark;


	public long getCdrServiceId() {
		return cdrServiceId;
	}

	public void setCdrServiceId(long cdrServiceId) {
		this.cdrServiceId = cdrServiceId;
	}


	public String getCdrServiceCode() {
		return cdrServiceCode;
	}

	public void setCdrServiceCode(String cdrServiceCode) {
		this.cdrServiceCode = cdrServiceCode;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getFilter() {
		return getName();
	}
}
