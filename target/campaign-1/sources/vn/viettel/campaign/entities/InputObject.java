package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "input_object")
public class InputObject {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "OBJECT_ID")
  private long objectId;
  @Column(name = "OBJECT_NAME")
  private String objectName;
  @Column(name = "OBJECT_TYPE")
  private Long objectType;
  @Column(name = "OBJECT_DATA_TYPE")
  private Long objectDataType;
  @Column(name = "OBJECT_PARENT_ID")
  private Long objectParentId;
  @Column(name = "DESCRIPTION")
  private String description;


  public long getObjectId() {
    return objectId;
  }

  public void setObjectId(long objectId) {
    this.objectId = objectId;
  }


  public String getObjectName() {
    return objectName;
  }

  public void setObjectName(String objectName) {
    this.objectName = objectName;
  }


  public Long getObjectType() {
    return objectType;
  }

  public void setObjectType(Long objectType) {
    this.objectType = objectType;
  }


  public Long getObjectDataType() {
    return objectDataType;
  }

  public void setObjectDataType(Long objectDataType) {
    this.objectDataType = objectDataType;
  }


  public Long getObjectParentId() {
    return objectParentId;
  }

  public void setObjectParentId(Long objectParentId) {
    this.objectParentId = objectParentId;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

}
