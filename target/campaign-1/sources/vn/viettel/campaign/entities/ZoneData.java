package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "zone_data")
public class ZoneData {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ZONE_DATA_ID")
  private long zoneDataId;
  @Column(name = "ZONE_DATA_VALUE")
  private String zoneDataValue;
  @Column(name = "ZONE_ID")
  private Long zoneId;
  @Column(name = "UPDATE_DATE")
  @Temporal(javax.persistence.TemporalType.DATE)
  private Date updateDate;


  public long getZoneDataId() {
    return zoneDataId;
  }

  public void setZoneDataId(long zoneDataId) {
    this.zoneDataId = zoneDataId;
  }


  public String getZoneDataValue() {
    return zoneDataValue;
  }

  public void setZoneDataValue(String zoneDataValue) {
    this.zoneDataValue = zoneDataValue;
  }


  public Long getZoneId() {
    return zoneId;
  }

  public void setZoneId(Long zoneId) {
    this.zoneId = zoneId;
  }


  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

}
