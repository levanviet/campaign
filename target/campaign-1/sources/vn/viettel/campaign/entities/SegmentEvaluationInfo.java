/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author ABC
 */
@Entity
@Table(name = "segment_evaluation_info")
public class SegmentEvaluationInfo implements Serializable {

    private static final Long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "SEGMENT_EVALUATION_INFO_ID")
    private Long segmentEvaluationInfoId;
    @Basic(optional = false)
    @Column(name = "CAMPAIGN_EVALUATION_INFO_ID")
    private Long campaignEvaluationInfoId;
    @Basic(optional = false)
    @Column(name = "SEGMENT_ID")
    private Long segmentId;
    @Basic(optional = false)
    @Column(name = "SEGMENT_REASIONING_METHOD")
    private short segmentReasioningMethod;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SEGMENT_WEIGHT")
    private Double segmentWeight;
    @Column(name = "DESCRIPTION")
    private String description;

    @Transient
    private String segmentName;
    @Transient
    private String campaignEvaluationInfoName;
    
    @Transient
    private List<RuleEvaluationInfo> listRuleEvaluationInfo;
    @Transient
    private List<ResultClass> listResultClass;
    @Transient
    private List<FuzzyRule> listFuzzyRule;
    
    @Transient
    private List<RuleEvaluationInfo> listRuleEvaluationInfoBefor;
    @Transient
    private List<ResultClass> listResultClassBefor;
    @Transient
    private List<FuzzyRule> listFuzzyRuleBefor;

    public List<RuleEvaluationInfo> getListRuleEvaluationInfoBefor() {
        return listRuleEvaluationInfoBefor;
    }

    public void setListRuleEvaluationInfoBefor(List<RuleEvaluationInfo> listRuleEvaluationInfoBefor) {
        this.listRuleEvaluationInfoBefor = listRuleEvaluationInfoBefor;
    }

    public List<ResultClass> getListResultClassBefor() {
        return listResultClassBefor;
    }

    public void setListResultClassBefor(List<ResultClass> listResultClassBefor) {
        this.listResultClassBefor = listResultClassBefor;
    }

    public List<FuzzyRule> getListFuzzyRuleBefor() {
        return listFuzzyRuleBefor;
    }

    public void setListFuzzyRuleBefor(List<FuzzyRule> listFuzzyRuleBefor) {
        this.listFuzzyRuleBefor = listFuzzyRuleBefor;
    }
    
    

    public List<RuleEvaluationInfo> getListRuleEvaluationInfo() {
        return listRuleEvaluationInfo;
    }

    public void setListRuleEvaluationInfo(List<RuleEvaluationInfo> listRuleEvaluationInfo) {
        this.listRuleEvaluationInfo = listRuleEvaluationInfo;
    }

    public List<ResultClass> getListResultClass() {
        return listResultClass;
    }

    public void setListResultClass(List<ResultClass> listResultClass) {
        this.listResultClass = listResultClass;
    }

    public List<FuzzyRule> getListFuzzyRule() {
        return listFuzzyRule;
    }

    public void setListFuzzyRule(List<FuzzyRule> listFuzzyRule) {
        this.listFuzzyRule = listFuzzyRule;
    }
    
    

    public String getCampaignEvaluationInfoName() {
        return campaignEvaluationInfoName;
    }

    public void setCampaignEvaluationInfoName(String campaignEvaluationInfoName) {
        this.campaignEvaluationInfoName = campaignEvaluationInfoName;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public SegmentEvaluationInfo() {
    }

    public SegmentEvaluationInfo(Long segmentEvaluationInfoId, Long campaignEvaluationInfoId, Long segmentId, short segmentReasioningMethod, Double segmentWeight, String description, String segmentName, String campaignEvaluationInfoName) {
        this.segmentEvaluationInfoId = segmentEvaluationInfoId;
        this.campaignEvaluationInfoId = campaignEvaluationInfoId;
        this.segmentId = segmentId;
        this.segmentReasioningMethod = segmentReasioningMethod;
        this.segmentWeight = segmentWeight;
        this.description = description;
        this.segmentName = segmentName;
        this.campaignEvaluationInfoName = campaignEvaluationInfoName;
    }
    public Long getSegmentEvaluationInfoId() {
        return segmentEvaluationInfoId;
    }

    public void setSegmentEvaluationInfoId(Long segmentEvaluationInfoId) {
        this.segmentEvaluationInfoId = segmentEvaluationInfoId;
    }

    public Long getCampaignEvaluationInfoId() {
        return campaignEvaluationInfoId;
    }

    public void setCampaignEvaluationInfoId(Long campaignEvaluationInfoId) {
        this.campaignEvaluationInfoId = campaignEvaluationInfoId;
    }

    public Long getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(Long segmentId) {
        this.segmentId = segmentId;
    }

    public short getSegmentReasioningMethod() {
        return segmentReasioningMethod;
    }

    public void setSegmentReasioningMethod(short segmentReasioningMethod) {
        this.segmentReasioningMethod = segmentReasioningMethod;
    }

    public Double getSegmentWeight() {
        return segmentWeight;
    }

    public void setSegmentWeight(Double segmentWeight) {
        this.segmentWeight = segmentWeight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
