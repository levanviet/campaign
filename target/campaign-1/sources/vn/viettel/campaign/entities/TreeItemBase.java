package vn.viettel.campaign.entities;

public interface TreeItemBase {
    Long getCategoryId();
    void setCategoryId(Long category_id);
}
