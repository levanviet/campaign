package vn.viettel.campaign.entities;
import java.io.Serializable;
import javax.persistence.*;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "process_value")
public class ProcessValue implements Serializable {
    
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "PROCESS_VALUE_ID")
  private long processValueId;
  @Column(name = "VALUE_ID")
  private long valueId;
  @Column(name = "VALUE_NAME")
  private String valueName;
  @Column(name = "VALUE_INDEX")
  private long valueIndex;
  @Column(name = "DESCRIPTION")
  private String description;
  @Column(name = "VALUE_COLOR")
  private String valueColor;
  @Transient
  private long preprocessId;

  public long getProcessValueId() {
    return processValueId;
  }

  public void setProcessValueId(long processValueId) {
    this.processValueId = processValueId;
  }


  public long getValueId() {
    return valueId;
  }

  public void setValueId(long valueId) {
    this.valueId = valueId;
  }


  public String getValueName() {
    return valueName;
  }

  public void setValueName(String valueName) {
    this.valueName = valueName;
  }


  public long getValueIndex() {
    return valueIndex;
  }

  public void setValueIndex(long valueIndex) {
    this.valueIndex = valueIndex;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public long getPreprocessId() {
    return preprocessId;
  }

  public void setPreprocessId(long preprocessId) {
    this.preprocessId = preprocessId;
  }

  public String getValueColor() {
    return valueColor;
  }

  public void setValueColor(String valueColor) {
    this.valueColor = valueColor;
  }
}
