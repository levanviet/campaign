package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "ocs_behaviour_template_fields")
public class OcsBehaviourTemplateFields {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TEMPLATE_FIELD_ID")
	private long templateFieldId;
	@Column(name = "BEHAVIOUR_TEMPLATE_ID")
	private Long behaviourTemplateId;
	@Column(name = "CRA_FIELD_ID")
	private Long craFieldId;
	@Column(name = "DATA_TYPE")
	private Long dataType;
	@Column(name = "VALUE")
	private String value;
	@Column(name = "SOURCE_TYPE")
	private Long sourceType;
	@Column(name = "REMARK")
	private String remark;
	@Transient
	private String name;

	public long getTemplateFieldId() {
		return templateFieldId;
	}

	public void setTemplateFieldId(long templateFieldId) {
		this.templateFieldId = templateFieldId;
	}


	public Long getBehaviourTemplateId() {
		return behaviourTemplateId;
	}

	public void setBehaviourTemplateId(Long behaviourTemplateId) {
		this.behaviourTemplateId = behaviourTemplateId;
	}


	public Long getCraFieldId() {
		return craFieldId;
	}

	public void setCraFieldId(Long craFieldId) {
		this.craFieldId = craFieldId;
	}


	public Long getDataType() {
		return dataType;
	}

	public void setDataType(Long dataType) {
		this.dataType = dataType;
	}


	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}


	public Long getSourceType() {
		return sourceType;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}


	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public OcsBehaviourFields toBehaviourField(Long fieldId) {
		OcsBehaviourFields ocsBehaviourFields = new OcsBehaviourFields();
		ocsBehaviourFields.setCraFieldId(this.craFieldId);
		ocsBehaviourFields.setDataType(this.dataType);
		ocsBehaviourFields.setValue(this.value);
		ocsBehaviourFields.setTemplateType(this.sourceType);
		ocsBehaviourFields.setOcsBehaviourId(fieldId);
		ocsBehaviourFields.setName(this.getName());
//		ocsBehaviourFields.setSourceType(1L);
		return ocsBehaviourFields;
	}
	public String getFilter() {
		return getName();
	}

}
