package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "condition_table_row_map")
public class ConditionTableRowMap {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "CONDITION_TABLE_ROW_ID")
  private long conditionTableRowId;
  @Column(name = "CONDITION_TABLE_ID")
  private long conditionTableId;
  @Column(name = "ROW_ID")
  private long rowId;


  public long getConditionTableRowId() {
    return conditionTableRowId;
  }

  public void setConditionTableRowId(long conditionTableRowId) {
    this.conditionTableRowId = conditionTableRowId;
  }


  public long getConditionTableId() {
    return conditionTableId;
  }

  public void setConditionTableId(long conditionTableId) {
    this.conditionTableId = conditionTableId;
  }


  public long getRowId() {
    return rowId;
  }

  public void setRowId(long rowId) {
    this.rowId = rowId;
  }

}
