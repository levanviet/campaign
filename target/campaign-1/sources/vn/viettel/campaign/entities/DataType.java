package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "data_type")
public class DataType {
  @Id
  @Column(name = "data_type_id")
  private long dataTypeId;
  @Column(name = "data_type_name")
  private String dataTypeName;
  @Column(name = "category_id")
  private Long categoryId;
  @Column(name = "description")
  private String description;


  public long getDataTypeId() {
    return dataTypeId;
  }

  public void setDataTypeId(long dataTypeId) {
    this.dataTypeId = dataTypeId;
  }


  public String getDataTypeName() {
    return dataTypeName;
  }

  public void setDataTypeName(String dataTypeName) {
    this.dataTypeName = dataTypeName;
  }


  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

}
