package vn.viettel.campaign.entities;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "notify_template")
@Getter
@Setter
public class NotifyTemplate implements TreeItemBase{
    @Id
    @Column(name = "TEMPLATE_ID")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "TEMPLATE_NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Column(name = "TEMPLATE_TYPE")
    private Integer templateType;

    @Transient
    private String filter;

    public String getFilter() {
        return name;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public Long getCategoryId() {
        return categoryId;
    }

    @Override
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
}
