package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "pre_process_value_map")
public class PreProcessValueMap {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "PRE_PROCESS_VALUE_ID")
  private long preProcessValueId;
  @Column(name = "PRE_PROCESS_ID")
  private long preProcessId;
  @Column(name = "PROCESS_VALUE_ID")
  private long processValueId;


  public long getPreProcessValueId() {
    return preProcessValueId;
  }

  public void setPreProcessValueId(long preProcessValueId) {
    this.preProcessValueId = preProcessValueId;
  }


  public long getPreProcessId() {
    return preProcessId;
  }

  public void setPreProcessId(long preProcessId) {
    this.preProcessId = preProcessId;
  }


  public long getProcessValueId() {
    return processValueId;
  }

  public void setProcessValueId(long processValueId) {
    this.processValueId = processValueId;
  }

}
