package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "extend_field")
public class ExtendField implements Serializable {
	@Id
	@Column(name = "ID")
	private long id;
	@Column(name = "NAME")
	private String name;
	@Column(name = "DESCRIPTION")
	private String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
