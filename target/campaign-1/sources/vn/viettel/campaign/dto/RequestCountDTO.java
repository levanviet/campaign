/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dto;

/**
 *
 * @author ConKC
 */
public class RequestCountDTO {

    private Long id;
    private Long countRequest;

    public RequestCountDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCountRequest() {
        return countRequest;
    }

    public void setCountRequest(Long countRequest) {
        this.countRequest = countRequest;
    }

}
