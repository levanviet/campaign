package vn.viettel.campaign.dto;

import lombok.Builder;
import lombok.Data;
import vn.viettel.campaign.entities.Language;

@Builder
@Data
public class NotifyContentDTO {
    private Long id;
    private Long notifyTemplateId;
    private String content;
    private Long languageId;
    private Language language;
    private Boolean isContentNew;
}
