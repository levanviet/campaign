package vn.viettel.campaign.dto;

public class CreateFileDTO {

    private Long id;
    private String msisdn;
    private String operator;
    private String lstSpeialOffer;

    public CreateFileDTO() {
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getLstSpeialOffer() {
        return lstSpeialOffer;
    }

    public void setLstSpeialOffer(String lstSpeialOffer) {
        this.lstSpeialOffer = lstSpeialOffer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
