package vn.viettel.campaign.dto;

import java.util.Date;
import vn.viettel.campaign.common.DateUtils;

/**
 *
 * @author ConKC
 */
public class RequestBuildDTO {

    private String object;
    private String action;
    private String segmentName;
    private Long segmentId;
    private Long segmentSize;
    private Long segmentParent;
    private Long categoryId;
    private Long categoryIdOld;
    private String description;
    private Date effectDate;
    private Date expireDate;
    private String path;
    private String blacklistName;
    private String blacklistNameOld;
    private Long blacklistId;

    private String tempPath;
    private String failedPath;
    private String histPath;
    private String currentPath;

    private String tempPathOld;
    private String failedPathOld;
    private String histPathOld;
    private String currentPathOld;

    private String specialOfferName;
    private String specialOfferNameOld;
    private Long specialOfferId;
    private boolean fileUpload;
    private String pathFile;
    private String pathFileOld;

    public RequestBuildDTO() {
        this.effectDate = new Date();
    }

    public RequestBuildDTO(String object, String action) {
        this.object = object;
        this.action = action;
        this.effectDate = new Date();
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public Long getSegmentParent() {
        return segmentParent;
    }

    public void setSegmentParent(Long segmentParent) {
        this.segmentParent = segmentParent;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEffectDate() {
        return effectDate;
    }

    public void setEffectDate(Date effectDate) {
        this.effectDate = effectDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(Long segmentId) {
        this.segmentId = segmentId;
    }

    public String getBlacklistName() {
        return blacklistName;
    }

    public void setBlacklistName(String blacklistName) {
        this.blacklistName = blacklistName;
    }

    public Long getBlacklistId() {
        return blacklistId;
    }

    public void setBlacklistId(Long blacklistId) {
        this.blacklistId = blacklistId;
    }

    public String getTempPath() {
        return tempPath;
    }

    public void setTempPath(String tempPath) {
        this.tempPath = tempPath;
    }

    public String getFailedPath() {
        return failedPath;
    }

    public void setFailedPath(String failedPath) {
        this.failedPath = failedPath;
    }

    public String getHistPath() {
        return histPath;
    }

    public void setHistPath(String histPath) {
        this.histPath = histPath;
    }

    public String getCurrentPath() {
        return currentPath;
    }

    public void setCurrentPath(String currentPath) {
        this.currentPath = currentPath;
    }

    public Long getCategoryIdOld() {
        return categoryIdOld;
    }

    public void setCategoryIdOld(Long categoryIdOld) {
        this.categoryIdOld = categoryIdOld;
    }

    public String getBlacklistNameOld() {
        return blacklistNameOld;
    }

    public void setBlacklistNameOld(String blacklistNameOld) {
        this.blacklistNameOld = blacklistNameOld;
    }

    public String getSpecialOfferName() {
        return specialOfferName;
    }

    public void setSpecialOfferName(String specialOfferName) {
        this.specialOfferName = specialOfferName;
    }

    public Long getSpecialOfferId() {
        return specialOfferId;
    }

    public void setSpecialOfferId(Long specialOfferId) {
        this.specialOfferId = specialOfferId;
    }

    public String getSpecialOfferNameOld() {
        return specialOfferNameOld;
    }

    public void setSpecialOfferNameOld(String specialOfferNameOld) {
        this.specialOfferNameOld = specialOfferNameOld;
    }

    public Date getCurrentDate() {
        return DateUtils.addDays(new Date(), -1);
    }

    public String getTempPathOld() {
        return tempPathOld;
    }

    public void setTempPathOld(String tempPathOld) {
        this.tempPathOld = tempPathOld;
    }

    public String getFailedPathOld() {
        return failedPathOld;
    }

    public void setFailedPathOld(String failedPathOld) {
        this.failedPathOld = failedPathOld;
    }

    public String getHistPathOld() {
        return histPathOld;
    }

    public void setHistPathOld(String histPathOld) {
        this.histPathOld = histPathOld;
    }

    public String getCurrentPathOld() {
        return currentPathOld;
    }

    public void setCurrentPathOld(String currentPathOld) {
        this.currentPathOld = currentPathOld;
    }

    public boolean isFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(boolean fileUpload) {
        this.fileUpload = fileUpload;
    }

    public Long getSegmentSize() {
        return segmentSize;
    }

    public void setSegmentSize(Long segmentSize) {
        this.segmentSize = segmentSize;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getPathFileOld() {
        return pathFileOld;
    }

    public void setPathFileOld(String pathFileOld) {
        this.pathFileOld = pathFileOld;
    }

}
