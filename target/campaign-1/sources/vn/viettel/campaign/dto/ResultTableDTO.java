package vn.viettel.campaign.dto;

/**
 *
 * @author ConKC
 */
public class ResultTableDTO {

    private Long resultTableId;
    private Long conditionTableId;
    private Long resultTableRuleResultId;
    private Long ruleResultId;
    private Long resultId;
    private Long rowIndex;

    public Long getResultTableId() {
        return resultTableId;
    }

    public void setResultTableId(Long resultTableId) {
        this.resultTableId = resultTableId;
    }

    public Long getConditionTableId() {
        return conditionTableId;
    }

    public void setConditionTableId(Long conditionTableId) {
        this.conditionTableId = conditionTableId;
    }

    public Long getResultTableRuleResultId() {
        return resultTableRuleResultId;
    }

    public void setResultTableRuleResultId(Long resultTableRuleResultId) {
        this.resultTableRuleResultId = resultTableRuleResultId;
    }

    public Long getRuleResultId() {
        return ruleResultId;
    }

    public void setRuleResultId(Long ruleResultId) {
        this.ruleResultId = ruleResultId;
    }

    public Long getResultId() {
        return resultId;
    }

    public void setResultId(Long resultId) {
        this.resultId = resultId;
    }

    public Long getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(Long rowIndex) {
        this.rowIndex = rowIndex;
    }
}
