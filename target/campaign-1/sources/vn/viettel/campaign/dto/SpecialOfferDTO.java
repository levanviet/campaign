package vn.viettel.campaign.dto;

public class SpecialOfferDTO {

    private String offer;
    private String priority;

    public SpecialOfferDTO() {
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

}
