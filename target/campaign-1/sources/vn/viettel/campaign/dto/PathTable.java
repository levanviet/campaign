package vn.viettel.campaign.dto;

import java.io.Serializable;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.common.DateUtils;
import vn.viettel.campaign.entities.InputObject;

import org.primefaces.model.*;

import java.util.List;

/**
 * @author truongbx
 * @date 9/25/2019
 */
public class PathTable{
	long objectId;
	String objectName;
	List<InputObject> lstInput;
	TreeNode node;
	String dataCondition;
	String dataFunction;
	String data;

	public PathTable(long objectId, List<InputObject> lstInput) {
		this.objectId = objectId;
		this.lstInput = lstInput;
	}

	public PathTable() {
	}

	public long getObjectId() {
		return objectId;
	}

	public void setObjectId(long objectId) {
		this.objectId = objectId;
	}

	public List<InputObject> getLstInput() {
		return lstInput;
	}

	public void setLstInput(List<InputObject> lstInput) {
		this.lstInput = lstInput;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public TreeNode getNode() {
		return node;
	}

	public void setNode(TreeNode node) {
		this.node = node;
	}

	public String getDataCondition() {
		if (DataUtil.isStringNullOrEmpty(this.dataCondition)) {
			this.dataCondition = "";
			String[] result = getData().split(";");
			if (result.length == 2) {
				dataCondition = result[0];
			}
		}
		return dataCondition;
	}

	public void setDataCondition(String dataCondition) {
		this.dataCondition = dataCondition;
	}
	public void setDataByCondition(String dataCondition) {
		if (dataCondition == null){
			dataCondition = "";
		}
		if (!DataUtil.isStringNullOrEmpty(data)) {
			this.data = dataCondition + ";" + data.substring(data.indexOf(";") + 1, data.length());
		}
	}

	public String getDataFunction() {
		if (DataUtil.isStringNullOrEmpty(this.dataFunction)) {
			this.dataFunction = "";
			String[] result = getData().split(";");
			if (result.length == 2) {
				dataFunction = result[1];
			}
		}
		return dataFunction;
	}

	public void setDataFunction(String dataFunction) {
		this.dataFunction = dataFunction;
	}

	public void setDataByFunction(String dataFunction) {
		if (dataFunction == null){
			dataFunction = "";
		}
		if (!DataUtil.isStringNullOrEmpty(data)) {
			this.data = data.substring(0, data.indexOf(";")) + ";" + dataFunction;
		}
	}


	public String getData() {
		if (data == null) {
			return "";
		}
		return data;
	}

	public void setData(String data) {
		this.data = data;
		if (DataUtil.isStringNullOrEmpty(data)){
			this.dataFunction = "";
			this.dataCondition = "";
		}else {
			dataCondition = data.substring(0, data.indexOf(";"));
			dataFunction = data.substring(data.indexOf(";") + 1, data.length());
		}
	}
	public void cloneData(PathTable pathTable){
		this.objectId = pathTable.getObjectId();
		this.objectName = pathTable.getObjectName();
		this.lstInput = pathTable.getLstInput();
		this.node = pathTable.getNode();
		this.dataCondition = pathTable.getDataCondition();
		this.dataFunction = pathTable.getDataFunction();
		this.data = pathTable.getData();
	}
}
