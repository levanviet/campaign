package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.*;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface PreprocessUnitDaoInterface extends BaseDAOInteface<PreProcessUnit>{
	public PreProcessUnit getNextSequense();
	public List<PreProcessUnit> getLstPPUByConditionId(Long conditionId);
	public boolean checkExistPreProcessName(String preProcessName, Long preProcessId);
	public String onSavePreprocessUnit(PreProcessUnit preProcessUnit ,
										List<ProcessParam> lstProcessParams , List<ProcessValue> lstProcessValue);
	public String onUpdatePreprocessUnit(PreProcessUnit preProcessUnit ,
										  List<ProcessParam> lstProcessParams , List<ProcessValue> lstProcessValue);
	public boolean onDeletePreprocessUnit(Long preProcessId);

	public boolean checkDeletePreprocessUnit(Long preProcessId);
}
