/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.RoleUser;


public interface RolesUsersDao {
    public Boolean saveRoleUser(RoleUser ru);
    public Boolean updateRoleUser(RoleUser ru);
    public void deleteRoleUserByUserId(Long userId);
    public void deleteRoleUserByRoleId(Long roleId);
    List<RoleUser> findByUserId(final Long usersId);
    
}
