package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.ResultTable;
import vn.viettel.campaign.entities.Rule;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface RuleDAOInterface extends BaseDAOInteface<Rule> {
	List<Rule> getLstRuleByCampaignId(long campaignId);

	List<Rule> getLstRuleForDefaultCampaign(long campaignId);
	boolean checkExisRuleName(String ruleName,Long ruleId);
	Rule getNextSequense();
	String onSaveRule(Rule rule, List<ResultTable> resultTables);
	String onUpdateRule(Rule rule, List<ResultTable> resultTables);
	boolean checkRuleInUse(Long ruleId);
	boolean onDeleteRule(Long ruleId);
}
