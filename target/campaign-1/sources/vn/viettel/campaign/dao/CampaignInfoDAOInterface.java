package vn.viettel.campaign.dao;

import org.hibernate.Session;
import vn.viettel.campaign.entities.BlackList;
import vn.viettel.campaign.entities.CampaignInfo;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface CampaignInfoDAOInterface extends BaseDAOInteface<CampaignInfo>{
	void deleteCampaignInforByCampaignId(Session session , Long campaignId);
}
