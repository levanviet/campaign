package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.Function;
import vn.viettel.campaign.entities.ZoneMap;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface ZoneMapDAOInterface extends BaseDAOInteface<ZoneMap> {
}
