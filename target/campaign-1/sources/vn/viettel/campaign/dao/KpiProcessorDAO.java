/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.AssessmentRuleKpiProcessorMap;
import vn.viettel.campaign.entities.KpiProcessor;

/**
 *
 * @author SON
 */
public interface KpiProcessorDAO {

    List<KpiProcessor> getLstKpiProcessor();

    KpiProcessor findOneById(Long id);
    
    public List<KpiProcessor> getListKpiProcessorByRuleId(Long ruleId);
    
    public List<KpiProcessor> findAllKpiProcessor(List<Long> categoryIds);

    void onSaveOrUpdateKpiProcessor(KpiProcessor kpiProcessor);

    void onDeleteKpiProcessor(KpiProcessor kpiProcessor);

    KpiProcessor getNextSequense();

    void onSaveKpiProcessor(KpiProcessor kpiProcessor);

    void onUpdateKpiProcessor(KpiProcessor kpiProcessor);

    KpiProcessor checkExitsProcessName(String name, Long id);

    KpiProcessor checkExitsCriteriaName(String name, Long id);

    List<AssessmentRuleKpiProcessorMap> checkDeleteKpiProcessor(KpiProcessor kpiProcessor);
}
