package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.SegmentDAOInterface;
import vn.viettel.campaign.entities.Segment;

import java.util.List;

/**
 * @author truongbx
 * @date 8/26/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class SegmentDAO extends BaseDAOImpl<Segment> implements SegmentDAOInterface {
	@Override
	public List<Segment> getLstSegment(long campaignId) {
		String sql = "SELECT DISTINCT A.*\n" +
				"FROM\n" +
				"  ( " +
				"SELECT a.PRIORITY_GROUP priorityGroup,b.SEGMENT_ID segmentId ,b.SEGMENT_NAME segmentName " +
				",b.EFF_DATE effDate, b.EXP_DATE expDate ,b.SEGMENT_SIZE segmentSize , b.IS_ACTIVE isActive " +
				",b.DESCRIPTION description , b.CATEGORY_ID categoryId , b.PARENT_ID parentId " +
				"FROM campaign_info a " +
				"INNER JOIN segment b " +
				"on a.SEGMENT_ID = b.SEGMENT_ID " +
				"  AND a.CAMPAIGN_ID = :campaignId " +
				"  )A ";

		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("priorityGroup",StandardBasicTypes.INTEGER);
		query.addScalar("segmentId",StandardBasicTypes.LONG);
		query.addScalar("segmentName",StandardBasicTypes.STRING);
		query.addScalar("effDate",StandardBasicTypes.DATE);
		query.addScalar("expDate",StandardBasicTypes.DATE);
		query.addScalar("segmentSize",StandardBasicTypes.LONG);
		query.addScalar("isActive",StandardBasicTypes.LONG);
		query.addScalar("description",StandardBasicTypes.STRING);
		query.addScalar("categoryId",StandardBasicTypes.LONG);
		query.addScalar("parentId",StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(Segment.class));
		query.setParameter("campaignId",campaignId);
		return query.list();
	}
}
