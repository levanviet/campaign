package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.OCSTemplateExtendFieldDaoInterface;
import vn.viettel.campaign.dao.OCSTemplateFieldDaoInterface;
import vn.viettel.campaign.entities.OcsBehaviourTemplateExtendFields;
import vn.viettel.campaign.entities.OcsBehaviourTemplateFields;

import javax.persistence.Query;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class OCSTemplateExtendFieldDAO extends BaseDAOImpl<OcsBehaviourTemplateExtendFields> implements OCSTemplateExtendFieldDaoInterface {
	@Override
	public List<OcsBehaviourTemplateExtendFields> getLstExtendFieldOfTemplate(long templateId) {
		String sql = "SELECT a.ID id , b.NAME name , b.ID extendFieldId ,a.BEHAVIOUR_TEMPLATE_ID behaviourTemplateId " +
				"FROM ocs_behaviour_template_extend_fields a " +
				"INNER JOIN extend_field b " +
				"WHERE a.EXTEND_FIELD_ID = b.ID " +
				"AND a.BEHAVIOUR_TEMPLATE_ID =:templateId"+
				" ORDER BY b.NAME";

		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("name", StandardBasicTypes.STRING);
		query.addScalar("id", StandardBasicTypes.LONG);
		query.addScalar("extendFieldId", StandardBasicTypes.LONG);
		query.addScalar("behaviourTemplateId", StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(OcsBehaviourTemplateExtendFields.class));
		query.setParameter("templateId", templateId);
		return query.list();
	}

	@Override
	public List<OcsBehaviourTemplateExtendFields> getRemainExtendFieldOfTemplate(long templateId, List<Long> lstExtendIds) {
		String sql = "Select ID extendFieldId,NAME name , '"+templateId+"' behaviourTemplateId from extend_field where ID not in (:lstExtendIds) ORDER BY NAME";

		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("extendFieldId", StandardBasicTypes.LONG);
		query.addScalar("name", StandardBasicTypes.STRING);
		query.addScalar("behaviourTemplateId", StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(OcsBehaviourTemplateExtendFields.class));
		if (lstExtendIds.size() == 0){
			lstExtendIds.add(-1L);
		}
		query.setParameterList("lstExtendIds", lstExtendIds);
		return query.list();
	}
	@Override
	public void deleteTemplateExtendFieldByTemplateId(Session session, Long templateId) {
		String hql = "delete from OcsBehaviourTemplateExtendFields where behaviourTemplateId =:templateId";
		Query query = session.createQuery(hql);
		query.setParameter("templateId", templateId);
		Integer num = query.executeUpdate();
		getLog().info(" number row delete at " + templateId + " is " + num);
	}

}
