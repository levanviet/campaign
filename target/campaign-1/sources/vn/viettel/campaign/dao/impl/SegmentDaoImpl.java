/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.SegmentDao;
import static vn.viettel.campaign.dao.impl.BaseDAOImpl.getLog;
import vn.viettel.campaign.entities.Segment;

@Repository
@Transactional(rollbackFor = Exception.class)
public class SegmentDaoImpl extends BaseDAOImpl<Segment> implements SegmentDao {

    @Override
    public List<Segment> findSegmentByCategory(final List<Long> categoryIds) {
        try {
            final String sql = "from Segment where categoryId in (:categoryIds)";
            Query query = getSession().createQuery(sql);
            query.setParameterList("categoryIds", categoryIds);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public List<Segment> findSegmentByName(String segmentName) {
        try {
            final String sql = "from Segment where segmentName = :segmentName";
            Query query = getSession().createQuery(sql);
            query.setParameter("segmentName", segmentName);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public boolean checkDuplicate(final String name, final Long id) {
        try {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("from Segment where segmentName = :segmentName ");
            if (Objects.nonNull(id)) {
                sqlQuery.append("and segmentId not in :segmentId ");
            }
            Query query = getSession().createQuery(sqlQuery.toString());
            query.setParameter("segmentName", name);
            if (Objects.nonNull(id)) {
                query.setParameter("segmentId", id);
            }
            query.setCacheable(true);
            if (query.list() != null && !query.list().isEmpty()) {
                return false;
            }
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
            return false;
        }
        return true;
    }

    @Override
    public boolean checkDuplicatePath(final String path, final Long id) {
        try {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("from Segment where path = :path ");
            if (Objects.nonNull(id)) {
                sqlQuery.append("and segmentId not in :segmentId ");
            }
            Query query = getSession().createQuery(sqlQuery.toString());
            query.setParameter("path", path);
            if (Objects.nonNull(id)) {
                query.setParameter("segmentId", id);
            }
            query.setCacheable(true);
            if (query.list() != null && !query.list().isEmpty()) {
                return false;
            }
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
            return false;
        }
        return true;
    }

    @Override
    public long onSaveSegment(Segment segment) {
        long result = -999;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            result = saveAndGetId(session, segment);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }

    @Override
    public List<Segment> getSegmentBuildTree(Long id) {
        String sql = "select"
                + " a.SEGMENT_ID as segmentId,"
                + " a.SEGMENT_NAME as segmentName,"
                + " a.PARENT_ID as parentId,"
                + " a.CATEGORY_ID as categoryId"
                + " from segment as a"
                + " inner join campaign_info as b on a.SEGMENT_ID=b.SEGMENT_ID"
                + " where b.CAMPAIGN_ID=:id";

        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("segmentId", StandardBasicTypes.LONG);
        query.addScalar("segmentName", StandardBasicTypes.STRING);
        query.addScalar("parentId", StandardBasicTypes.LONG);
        query.addScalar("categoryId", StandardBasicTypes.LONG);
        query.setParameter("id",id);
        query.setResultTransformer(Transformers.aliasToBean(Segment.class));
        return query.list();
    }

    @Override
    public Segment getSegmentByName(String segmentName) {
        String sql = "select"
                + " a.SEGMENT_ID as segmentId,"
                + " a.SEGMENT_NAME as segmentName,"
                + " a.PARENT_ID as parentId,"
                + " a.CATEGORY_ID as categoryId"
                + " from segment as a"
                + " where a.SEGMENT_NAME=:segmentName";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("segmentId", StandardBasicTypes.LONG);
        query.addScalar("segmentName", StandardBasicTypes.STRING);
        query.addScalar("parentId", StandardBasicTypes.LONG);
        query.addScalar("categoryId", StandardBasicTypes.LONG);
        query.setParameter("segmentName", segmentName);
        query.setResultTransformer(Transformers.aliasToBean(Segment.class));
        return (Segment) query.uniqueResult();
    }
}
