package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.NotifyStatisticMapDAOInterface;
import vn.viettel.campaign.dao.StatisticCycleDAOInterface;
import vn.viettel.campaign.entities.NotifyStatisticMap;
import vn.viettel.campaign.entities.Rule;
import vn.viettel.campaign.entities.StatisticCycle;

import java.util.ArrayList;
import java.util.List;

/**
 * @author truongbx
 * @date 09/01/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class StatisticCycleDAO extends BaseDAOImpl<StatisticCycle> implements StatisticCycleDAOInterface {

    @Override
    public List<StatisticCycle> findByNotifyTriggerId(long id) {
        String sql = "SELECT\n" +
                "\tstatistic_cycle.id as id,\n" +
                "\tstatistic_cycle.NAME as name,\n" +
                "\tstatistic_cycle.CYCLE_UNIT_ID as cycleUnitId,\n" +
                "\tstatistic_cycle.CYCLE_UNIT_TIMES cycleUnitTimes,\n" +
                "\tstatistic_cycle.MAX_NOTIFY as maxNotify \n" +
                "FROM\n" +
                "\tnotify_trigger\n" +
                "\tINNER JOIN notify_statistic_map\n" +
                "\tINNER JOIN statistic_cycle \n" +
                "WHERE\n" +
                "\tnotify_trigger.notify_trigger_id = notify_statistic_map.notify_id \n" +
                "\tAND notify_statistic_map.statistic_cycle_id = statistic_cycle.id \n" +
                "\tAND notify_trigger_id = :id";

        SQLQuery query = getSession().createSQLQuery(sql);
        query.setParameter("id", id);
        query.addScalar("id", StandardBasicTypes.LONG);
        query.addScalar("name", StandardBasicTypes.STRING);
        query.addScalar("cycleUnitId", StandardBasicTypes.LONG);
        query.addScalar("cycleUnitTimes", StandardBasicTypes.LONG);
        query.addScalar("maxNotify", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(StatisticCycle.class));
        return query.list();
    }

    @Override
    public List<StatisticCycle> findById(long id) {
        try {
            final String sql = "from StatisticCycle where id = :id";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameter("id", id);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
//            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public void deleteByNotifyTriggerId(long id) {
        String sql = "DELETE FROM\n" +
                "\tstatistic_cycle \n" +
                "WHERE\n" +
                "\tid IN ( SELECT statistic_cycle_id FROM notify_statistic_map WHERE notify_statistic_map.notify_id = :id )";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.setParameter("id", id).executeUpdate();
    }
}
