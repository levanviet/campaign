package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.ProcessParamDAOInterface;
import vn.viettel.campaign.dao.ProcessValueDAOInterface;
import vn.viettel.campaign.entities.ProcessParam;
import vn.viettel.campaign.entities.ProcessValue;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Repository
public class ProcessParamDAO extends BaseDAOImpl<ProcessParam> implements ProcessParamDAOInterface {
	@Override
	public List<ProcessParam> findProcessParamByPreProcessIds(Long ppuId) {
		String sql = "SELECT b.CONFIG_INPUT configInput , b.PARAM_INDEX paramIndex " +
				"FROM pre_process_param_map a " +
				"INNER JOIN process_param b " +
				"WHERE   a.PROCESS_PARAM_ID = b.PROCESS_PARAM_ID " +
				"AND a.PRE_PROCESS_ID =:ppuId ";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("paramIndex", StandardBasicTypes.LONG);
		query.addScalar("configInput", StandardBasicTypes.STRING);
		query.setResultTransformer(Transformers.aliasToBean(ProcessParam.class));
		query.setParameter("ppuId", ppuId);
		return query.list();
	}

    @Override
    public List<ProcessParam> findProcessParamByPreProcessId(Long pre_process_id) {
         String sql="select"
                + " b.PARAM_INDEX as paramIndex,"
                + " b.CONFIG_INPUT as configInput"
                + " from pre_process_param_map as a"
                + " inner join process_param as b on a.PROCESS_PARAM_ID = b.PROCESS_PARAM_ID"
                + " where a.PRE_PROCESS_ID=:pre_process_id";
        SQLQuery query=getSession().createSQLQuery(sql);
        query.addScalar("paramIndex", StandardBasicTypes.LONG);
        query.addScalar("configInput", StandardBasicTypes.STRING);
        query.setResultTransformer(Transformers.aliasToBean(ProcessParam.class));
        query.setParameter("pre_process_id", pre_process_id);
        return query.list();
    }
}
