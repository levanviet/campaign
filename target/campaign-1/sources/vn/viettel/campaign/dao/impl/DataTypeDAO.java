package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.common.Responses;
import vn.viettel.campaign.dao.*;
import vn.viettel.campaign.entities.*;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class DataTypeDAO extends BaseDAOImpl<DataType> implements DataTypeDAOInterface {

}
