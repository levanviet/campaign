package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.CycleUnitDAOInterface;
import vn.viettel.campaign.dao.NotifyStatisticMapDAOInterface;
import vn.viettel.campaign.entities.CycleUnit;
import vn.viettel.campaign.entities.NotifyStatisticMap;

/**
 * @author truongbx
 * @date 09/01/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class CycleUnitDAO extends BaseDAOImpl<CycleUnit> implements CycleUnitDAOInterface {


}
