package vn.viettel.campaign.dao.impl;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.common.Responses;
import vn.viettel.campaign.dao.BaseDAOInteface;

import java.util.List;
import java.util.Map;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.query.Query;
import org.hibernate.type.Type;

/**
 * @author truongbx
 * @date 8/24/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class BaseDAOImpl<T extends Object> implements BaseDAOInteface<T> {

    private static final Logger log = Logger.getLogger(BaseDAOImpl.class.getName());

    @Autowired
    public SessionFactory sessionFactory;

    @Override
    public T findById(Class<T> modelClass, long id) {
        return (T) getSession().get(modelClass, id);
    }

    @Override
    public List<T> findByIds(Class<T> modelClass, List<Long> ids) {
        List<T> result = getSession().createQuery("from " + modelClass.getSimpleName() + " where id in :ids ")
                .setParameter("ids", ids)
                .list();
        return result;
    }

    @Override
    public List<T> getAll(Class<T> modelClass) {
        return (List<T>) getSession().createCriteria(modelClass).list();
    }

    @Override
    public String saveOrUpdate(T o) {
        try {
            if (!DataUtil.isNullObject(0) && getSession().contains(o)) {
                getSession().refresh(o);
            }
            getSession().saveOrUpdate(o);
            return Responses.SUCCESS.getName();
        } catch (ConstraintViolationException e) {
            log.info(e.toString());
            return e.getConstraintName();
        } catch (Exception e) {
            log.info(e.toString());
            return Responses.ERROR.getName();
        }
    }

    @Override
    public String deleteById(Class<T> modelClass, long id) {
        return deleteById(modelClass, id, getSession());
    }

    @Override
    public String deleteById(Class<T> modelClass, long id, Session session) {
        T object = session.get(modelClass, id);
        if (object == null) {
            return Responses.NOT_FOUND.getName();
        }
        return deleteByObject(session, object);
    }

    @Override
    public String deleteByListIds(Session session, List<Long> ids, Class<T> modelClass) {
        try {
            for (Long id : ids) {
                T object = (T) session.get(modelClass, id);
                if (object == null) {
                    return Responses.NOT_FOUND.getName();
                }
                session.delete(object);
            }
            return Responses.SUCCESS.getName();
        } catch (Exception e) {
            log.info(e.toString());
            return Responses.ERROR.getName();
        }

    }

    @Override
    public String deleteByObject(T obj) {
        return deleteByObject(getSession(), obj);
    }

    @Override
    public String deleteByObject(Session session, T obj) {
        try {
            session.delete(obj);
            return Responses.SUCCESS.getName();
        } catch (Exception e) {
            log.info(e.toString());
            return Responses.ERROR.getName();
        }
    }

    @Override
    public String deleteListByObject(Session session, List<T> lstObj) {
        try {
            for (T obj : lstObj) {
                session.delete(obj);
            }
            return Responses.SUCCESS.getName();
        } catch (Exception e) {
            log.info(e.toString());
            return Responses.ERROR.getName();
        }
    }

    @Override
    public String update(T obj) {
        return update(getSession(), obj);
    }

    @Override
    public String update(Session session, T obj) {
        try {
            session.update(obj);
            return Responses.SUCCESS.getName();
        } catch (ConstraintViolationException e) {
            log.info(e.toString());
            return e.getConstraintName();
        } catch (Exception e) {
            log.info(e.toString());
            return Responses.ERROR.getName();
        }
    }

    @Override
    public String update(Session session, List<T> lstObj) {
        try {
            for (T obj : lstObj) {
                session.update(obj);
            }
            return Responses.SUCCESS.getName();
        } catch (ConstraintViolationException e) {
            log.info(e.toString());
            return e.getConstraintName();
        } catch (Exception e) {
            log.info(e.toString());
            e.printStackTrace();
            return Responses.ERROR.getName();
        }
    }

    @Override
    public String save(List<T> lstObj) {
        return save(getSession(), lstObj);
    }

    @Override
    public String save(T obj) {
        return save(getSession(), obj);
    }

    @Override
    public String save(Session session, List<T> lstObj) {
        try {
            for (T obj : lstObj) {
                session.save(obj);
            }
            return Responses.SUCCESS.getName();
        } catch (ConstraintViolationException e) {
            log.info(e.toString());
            return e.getConstraintName();
        } catch (Exception e) {
            log.info(e.toString());
            e.printStackTrace();
            return Responses.ERROR.getName();
        }
    }

    @Override
    public String save(Session session, T obj) {
        try {
            long savedObjectId = (long) session.save(obj);
            return savedObjectId + "";
        } catch (ConstraintViolationException e) {
            log.info(e.toString());
            return e.getConstraintName();
        } catch (Exception e) {
            log.info(e.toString());
            e.printStackTrace();
            return Responses.ERROR.getName();
        }
    }

    @Override
    public long saveAndGetId(Session session, T obj) {
        try {
            return (long) session.save(obj);
        } catch (Exception e) {
            log.error(e.toString());
            return -1;
        }
    }

    @Override
    public long saveAndGetId(T obj) throws Exception {
        return (long) getSession().save(obj);
    }

    public Session getSession() {
        return sessionFactory.getCurrentSession();

    }

    public static Logger getLog() {
        return log;
    }

    @Override
    public List<T> getAll(Class<T> modelClass, Order order) {
        return (List<T>) getSession().createCriteria(modelClass).addOrder(order).list();
    }

    protected void addParameters(Query query, List<Object> params) {
        if (params != null) {
            int i = 0;
            for (Object p : params) {
                if (p instanceof ArrayList) {
                    query.setParameterList("list", (ArrayList) p);
                } else {
                    query.setParameter(i, p);
                    i++;
                }
            }
        }
    }

    protected void addScalar(SQLQuery query, Map<String, Type> fieldMap) {
        if (fieldMap == null) {
            return;
        }
        fieldMap.entrySet().stream().forEach((field) -> {
            query.addScalar(field.getKey(), field.getValue());
        });
    }

    @Override
    public T doSaveOrUpdate(T o) {
        try {
            if (!DataUtil.isNullObject(0) && getSession().contains(o)) {
                getSession().refresh(o);
            }
            getSession().saveOrUpdate(o);
            return o;
        } catch (ConstraintViolationException e) {
            log.info(e.toString());
            return o;
        } catch (Exception e) {
            log.info(e.toString());
            return o;
        }
    }
}
