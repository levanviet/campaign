/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.SegmentEvaluationInfoDAOInterface;
import vn.viettel.campaign.entities.SegmentEvaluationInfo;

/**
 *
 * @author ABC
 */
@Repository
public class SegmentEvaluationInfoDAOImpl extends BaseDAOImpl<SegmentEvaluationInfo> implements SegmentEvaluationInfoDAOInterface {

    @Override
    public List<SegmentEvaluationInfo> getListSegmentEvaluationInfo(Long campaignEvaluationInfoId) {
        String sql = "select"
                + " a.SEGMENT_EVALUATION_INFO_ID as segmentEvaluationInfoId,"
                + " a.CAMPAIGN_EVALUATION_INFO_ID as campaignEvaluationInfoId,"
                + " a.SEGMENT_ID as segmentId,"
                + " a.SEGMENT_REASIONING_METHOD as segmentReasioningMethod,"
                + " a.SEGMENT_WEIGHT as segmentWeight,"
                + " a.DESCRIPTION as description,"
                + " b.SEGMENT_NAME as segmentName,"
                + " c.CAMPAIGN_EVALUATION_INFO_NAME as campaignEvaluationInfoName"
                + " from segment_evaluation_info as a"
                + " inner join segment as b on a.SEGMENT_ID=b.SEGMENT_ID"
                + " inner join campaign_evaluation_info as c on a.CAMPAIGN_EVALUATION_INFO_ID=c.CAMPAIGN_EVALUATION_INFO_ID"
                + " where a.CAMPAIGN_EVALUATION_INFO_ID =:campaignEvaluationInfoId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("segmentEvaluationInfoId", StandardBasicTypes.LONG);
        query.addScalar("campaignEvaluationInfoId", StandardBasicTypes.LONG);
        query.addScalar("segmentId", StandardBasicTypes.LONG);
        query.addScalar("segmentReasioningMethod", StandardBasicTypes.SHORT);
        query.addScalar("segmentWeight", StandardBasicTypes.DOUBLE);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("segmentName", StandardBasicTypes.STRING);
        query.addScalar("campaignEvaluationInfoName", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(SegmentEvaluationInfo.class));
        query.setParameter("campaignEvaluationInfoId", campaignEvaluationInfoId);
        return query.list();
    }

    @Override
    public SegmentEvaluationInfo doCreateOrUpdateSegmentEvaluationInfo(SegmentEvaluationInfo segmentEvaluationInfo) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            doSaveOrUpdate(segmentEvaluationInfo);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
        return segmentEvaluationInfo;
    }

    @Override
    public boolean deleteSegmentEvaluationInfo(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            deleteById(SegmentEvaluationInfo.class, id);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            ex.printStackTrace();
            return false;
        } finally {
            session.close();
        }
        return true;
    }

    @Override
    public SegmentEvaluationInfo getNextSequenceSegmentEvaluationInfo() {
        String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"segment_evaluation_info\"";
        SQLQuery queryUpdate = getSession().createSQLQuery(update);
        queryUpdate.executeUpdate();
        String sql = "SELECT table_id segmentEvaluationInfoId from SEQ_TABLE WHERE TABLE_NAME = \"segment_evaluation_info\"";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("segmentEvaluationInfoId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(SegmentEvaluationInfo.class));
        return (SegmentEvaluationInfo) query.uniqueResult();
    }

    @Override
    public List<SegmentEvaluationInfo> getListSegmentEvaluationInfoMapAssessment(Long assessmentRuleId) {
          String sql = "select"
                + " a.SEGMENT_EVALUATION_INFO_ID as segmentEvaluationInfoId,"
                + " a.CAMPAIGN_EVALUATION_INFO_ID as campaignEvaluationInfoId,"
                + " a.SEGMENT_ID as segmentId,"
                + " a.SEGMENT_REASIONING_METHOD as segmentReasioningMethod,"
                + " a.SEGMENT_WEIGHT as segmentWeight,"
                + " a.DESCRIPTION as description"
                + " from segment_evaluation_info as a"
                + " inner join rule_evaluation_info as b on a.segment_evaluation_info_id = b.OWNER_ID"
                + " where b.ASSESSMENT_RULE_ID =:assessmentRuleId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("segmentEvaluationInfoId", StandardBasicTypes.LONG);
        query.addScalar("campaignEvaluationInfoId", StandardBasicTypes.LONG);
        query.addScalar("segmentId", StandardBasicTypes.LONG);
        query.addScalar("segmentReasioningMethod", StandardBasicTypes.SHORT);
        query.addScalar("segmentWeight", StandardBasicTypes.DOUBLE);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.setResultTransformer(Transformers.aliasToBean(SegmentEvaluationInfo.class));
        query.setParameter("assessmentRuleId", assessmentRuleId);
        return query.list();
    }

}
