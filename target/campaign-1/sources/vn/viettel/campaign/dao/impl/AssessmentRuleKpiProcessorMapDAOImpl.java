/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.AssessmentRuleKpiProcessorMapDAOInterfae;
import vn.viettel.campaign.entities.AssessmentRuleKpiProcessorMap;
import vn.viettel.campaign.entities.AssessmentRulePreProcessUnitMap;

/**
 *
 * @author ABC
 */
@Repository
public class AssessmentRuleKpiProcessorMapDAOImpl extends BaseDAOImpl<AssessmentRuleKpiProcessorMap> implements AssessmentRuleKpiProcessorMapDAOInterfae {

    @Override
    public AssessmentRuleKpiProcessorMap createOrUpdateAssessmentRuleKpiProcessorMap(AssessmentRuleKpiProcessorMap assessmentRuleKpiProcessorMap) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            doSaveOrUpdate(assessmentRuleKpiProcessorMap);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return assessmentRuleKpiProcessorMap;
    }

    @Override
    public boolean deleteAssessmentRuleKpiProcessorMap(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            deleteById(AssessmentRuleKpiProcessorMap.class, id);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return false;
        } finally {
            session.close();
        }
        return true;
    }

    @Override
    public List<AssessmentRuleKpiProcessorMap> getAlllistAssessmentRuleKpiProcessorMapById(Long id) {
        String sql = "select"
                + " a.ID as id,"
                + " a.RULE_ID as ruleId,"
                + " a.PROCESSOR_ID as processorId"
                + " from assessment_rule_kpi_processor_map as a"
                + " where a.RULE_ID=:id";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("id", StandardBasicTypes.LONG);
        query.addScalar("ruleId", StandardBasicTypes.LONG);
        query.addScalar("processorId", StandardBasicTypes.LONG);
        query.setParameter("id", id);
        query.setResultTransformer(Transformers.aliasToBean(AssessmentRuleKpiProcessorMap.class));
        return query.list();
    }

    @Override
    public AssessmentRuleKpiProcessorMap getNextAssessmentRuleKpiProcessorMap() {
        String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"ASSESSMENT_RULE_KPI_PROCESSOR_MAP\"";
        SQLQuery queryUpdate = getSession().createSQLQuery(update);
        queryUpdate.executeUpdate();
        String sql = "SELECT table_id Id from SEQ_TABLE WHERE TABLE_NAME = \"ASSESSMENT_RULE_KPI_PROCESSOR_MAP\"";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("Id", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(AssessmentRuleKpiProcessorMap.class));
        return (AssessmentRuleKpiProcessorMap) query.uniqueResult();
    }

    @Override
    public AssessmentRuleKpiProcessorMap findAssessmentRuleKpiProcessorMapById(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            findById(AssessmentRuleKpiProcessorMap.class, id);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return findById(AssessmentRuleKpiProcessorMap.class, id);
    }

}
