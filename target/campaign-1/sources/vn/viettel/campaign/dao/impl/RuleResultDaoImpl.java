/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.BaseAbstract;
import vn.viettel.campaign.dao.ResultDao;
import vn.viettel.campaign.dao.RuleResultDao;
import vn.viettel.campaign.entities.Result;
import vn.viettel.campaign.entities.RowCt;
import vn.viettel.campaign.entities.RuleResult;


@Repository
@Transactional(rollbackFor = Exception.class)
public class RuleResultDaoImpl extends BaseDAOImpl<Result> implements RuleResultDao {
    private static final Logger log = Logger.getLogger(RuleResultDaoImpl.class.getName());

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<RuleResult> finAllRuleResult() {
        Session session = this.sessionFactory.getCurrentSession();
        List<RuleResult> lstObj;
        lstObj = session.createQuery("from rule_result").list();
        return lstObj.size() > 0 ? lstObj : null;    
    }

    @Override
    public RuleResult updateOrSave(RuleResult ruleResult) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(ruleResult);
        return ruleResult;
    } 

    @Override
    public RuleResult getRuleResultById(Long ruleResultId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<RuleResult> lstObj;
        lstObj = session.createQuery("from rule_result where ruleResultId = :ruleResultId")
                .setParameter("ruleResultId", ruleResultId).list();
        return lstObj.size() > 0 ? lstObj.get(0) : null;
    }

    @Override
    public void deleteByResultTableId(long id) {
        String sql = "delete from rule_result where rule_result.rule_result_id\n" +
                "in\n" +
                "(\n" +
                "select result_table_rule_result_map.rule_result_id\n" +
                "from result_table_rule_result_map\n" +
                "where result_table_rule_result_map.result_table_id= :id\n" +
                ")";
        getSession().createSQLQuery(sql).setParameter("id", id).executeUpdate();
    }
}
