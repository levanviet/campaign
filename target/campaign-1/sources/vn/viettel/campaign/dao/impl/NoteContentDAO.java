package vn.viettel.campaign.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.NoteContentDAOInterface;
import vn.viettel.campaign.entities.NoteContent;

import java.util.ArrayList;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class NoteContentDAO extends BaseDAOImpl<NoteContent> implements NoteContentDAOInterface {
    private static final Logger log = Logger.getLogger(NoteContentDAO.class.getName());
    @Override
    public List<NoteContent> findByNoteId(long nodeId) {
        try {
            final String sql = "from NoteContent where nodeId = :nodeId";
            Query query = getSession().createQuery(sql);
            query.setParameter("nodeId", nodeId);
            return query.list();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public List<NoteContent> findByNoteIds(List<Long> nodeIds) {
        try {
            final String sql = "from NoteContent where nodeId in (:nodeIds)";
            Query query = getSession().createQuery(sql);
            query.setParameterList("nodeIds", nodeIds);
            return query.list();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public void deleteByNoteIds(long scenarioId) {
        try {
            String sql = "DELETE FROM note_content WHERE NODE_ID in (SELECT sn.NODE_ID FROM  scenario_node sn WHERE  sn.USSD_SCENARIO_ID = :scenarioId)";
            Query query = getSession().createSQLQuery(sql);
            query.setParameter("scenarioId", scenarioId);
            int i = query.executeUpdate();
            System.out.println(i);
//            getSession().clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Exception " + ex);
        }
    }
}
