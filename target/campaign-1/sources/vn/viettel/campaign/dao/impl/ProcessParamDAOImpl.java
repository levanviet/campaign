/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.entities.PpuEvaluation;
import vn.viettel.campaign.entities.PreProcessParamMap;
import vn.viettel.campaign.entities.ProcessParam;
import vn.viettel.campaign.entities.ProcessValue;

/**
 *
 * @author admin
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class ProcessParamDAOImpl extends BaseDAOImpl implements vn.viettel.campaign.dao.ProcessParamDAO {

    @Override
    public void onSaveOrUpdateProcessParam(ProcessParam processParam) {
        getSession().saveOrUpdate(processParam);
    }

    @Override
    public void onDeleteProcessParam(PpuEvaluation ppuEvaluation) {
        String sql = "Delete from process_param where process_param_id in (select process_param_id from pre_process_param_map where pre_process_id =:id)";
        getSession().createSQLQuery(sql).setParameter("id", ppuEvaluation.getPreProcessId()).executeUpdate();
        String sql1 = "Delete from pre_process_param_map where pre_process_id =:id1";
        getSession().createSQLQuery(sql1).setParameter("id1", ppuEvaluation.getPreProcessId()).executeUpdate();
    }

    @Override
    public ProcessParam checkOnSavePreProcessParamMap(Long processParamId, Long ppuId) {
        String sql = "select PROCESS_PARAM_ID as processParamId from pre_process_param_map WHERE PROCESS_PARAM_ID=:processParamId AND PRE_PROCESS_ID=:ppuId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("processParamId", StandardBasicTypes.LONG);
        query.setParameter("processParamId", processParamId);
        query.setParameter("ppuId", ppuId);
        query.setResultTransformer(Transformers.aliasToBean(ProcessParam.class));
        return (ProcessParam) query.uniqueResult();
    }

    @Override
    public void onDeleteProcessParam(Long processParamId, Long ppuId) {
        String sql = "Delete from process_param where process_param_id=:processParamId";
        getSession().createSQLQuery(sql).setParameter("processParamId", processParamId).executeUpdate();
        getSession().createSQLQuery("SET SQL_SAFE_UPDATES=0;");
        String sql1 = "Delete from pre_process_param_map where pre_process_id =:ppuId and process_param_id=:processParamId";
        getSession().createSQLQuery(sql1).setParameter("ppuId", ppuId).setParameter("processParamId", processParamId).executeUpdate();
    }

}
