/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.CampaignEvaluationInfoDAOInterface;
import vn.viettel.campaign.entities.CampaignEvaluationInfo;

/**
 *
 * @author ABC
 */
@Repository
public class CampaignEvaluationInfoDAOImpl extends BaseDAOImpl<CampaignEvaluationInfo> implements CampaignEvaluationInfoDAOInterface {

    @Override
    public List<CampaignEvaluationInfo> getListCampaginEvaluationInfoByCategoryIds(List<Long> categoryIds) {
        String sql = "select"
                + " a.CAMPAIGN_EVALUATION_INFO_NAME as campaignEvaluationInfoName,"
                + " a.CAMPAIGN_EVALUATION_INFO_ID as campaignEvaluationInfoId,"
                + " a.CAMPAIGN_ID as campaignId,"
                + " a.SCHEDULE_ID as scheduleId,"
                + " a.REASIONING_METHOD as reasioningMethod,"
                + " a.IS_DEFAULT as isDefault,"
                + " a.DESCRIPTION as description,"
                + " a.CATEGORY_ID as categoryId,"
                + " c.SCHEDULE_NAME as scheduleName,"
                + " b.CAMPAIGN_NAME as name,"
                + " b.IS_DEFAULT as isDefaultCampaign"
                + " from campaign_evaluation_info as a"
                + " inner join CAMPAIGN as b on a.CAMPAIGN_ID=b.CAMPAIGN_ID"
                + " inner join campaign_schedule as c on a.SCHEDULE_ID=c.SCHEDULE_ID"
                + " where a.CATEGORY_ID in (:categoryIds)";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("campaignEvaluationInfoId", StandardBasicTypes.LONG);
        query.addScalar("campaignEvaluationInfoName", StandardBasicTypes.STRING);
        query.addScalar("campaignId", StandardBasicTypes.LONG);
        query.addScalar("scheduleId", StandardBasicTypes.LONG);
        query.addScalar("reasioningMethod", StandardBasicTypes.INTEGER);
        query.addScalar("isDefault", StandardBasicTypes.BOOLEAN);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("categoryId", StandardBasicTypes.LONG);
        query.addScalar("scheduleName", StandardBasicTypes.STRING);
        query.addScalar("name", StandardBasicTypes.STRING);
        query.addScalar("isDefaultCampaign", StandardBasicTypes.INTEGER);
        query.setParameterList("categoryIds", categoryIds);
        query.setResultTransformer(Transformers.aliasToBean(CampaignEvaluationInfo.class));
        return query.list();
    }

    @Override
    public CampaignEvaluationInfo getNextSequenceCampaignEvaluationInfo() {
        String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"campaign_evaluation_info\"";
        SQLQuery queryUpdate = getSession().createSQLQuery(update);
        queryUpdate.executeUpdate();
        String sql = "SELECT table_id campaignEvaluationInfoId from SEQ_TABLE WHERE TABLE_NAME = \"campaign_evaluation_info\"";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("campaignEvaluationInfoId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(CampaignEvaluationInfo.class));
        return (CampaignEvaluationInfo) query.uniqueResult();
    }

    @Override
    public CampaignEvaluationInfo doCreateOrUpdateCampaignEvaluationInfo(CampaignEvaluationInfo campaignEvaluationInfo) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            doSaveOrUpdate(campaignEvaluationInfo);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
        return campaignEvaluationInfo;
    }

    @Override
    public boolean deleteCampaignEvaluaitonInfo(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            deleteById(CampaignEvaluationInfo.class, id);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            ex.printStackTrace();
            return false;
        } finally {
            session.close();
        }
        return true;
    }

    @Override
    public CampaignEvaluationInfo findCampaignEvaluationInfoById(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            findById(CampaignEvaluationInfo.class, id);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
        return findById(CampaignEvaluationInfo.class, id);
    }

    @Override
    public List<CampaignEvaluationInfo> findAllCampaignEvaluationInfo() {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            getAll(CampaignEvaluationInfo.class);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
        return getAll(CampaignEvaluationInfo.class);
    }

    @Override
    public List<CampaignEvaluationInfo> getListCampaignEvalurationMapAssessment(Long assessmentRuleId) {
        String sql = "select"
                + " a.CAMPAIGN_EVALUATION_INFO_NAME as campaignEvaluationInfoName,"
                + " a.CAMPAIGN_EVALUATION_INFO_ID as campaignEvaluationInfoId,"
                + " a.CAMPAIGN_ID as campaignId,"
                + " a.SCHEDULE_ID as scheduleId,"
                + " a.REASIONING_METHOD as reasioningMethod,"
                + " a.IS_DEFAULT as isDefault,"
                + " a.DESCRIPTION as description,"
                + " a.CATEGORY_ID as categoryId"
                + " from campaign_evaluation_info as a"
                + " inner join rule_evaluation_info as b on a.CAMPAIGN_EVALUATION_INFO_ID=b.OWNER_ID"
                + " where  b.ASSESSMENT_RULE_ID = :assessmentRuleId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("campaignEvaluationInfoId", StandardBasicTypes.LONG);
        query.addScalar("campaignEvaluationInfoName", StandardBasicTypes.STRING);
        query.addScalar("campaignId", StandardBasicTypes.LONG);
        query.addScalar("scheduleId", StandardBasicTypes.LONG);
        query.addScalar("reasioningMethod", StandardBasicTypes.INTEGER);
        query.addScalar("isDefault", StandardBasicTypes.BOOLEAN);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("categoryId", StandardBasicTypes.LONG);
        query.setParameter("assessmentRuleId", assessmentRuleId);
        query.setResultTransformer(Transformers.aliasToBean(CampaignEvaluationInfo.class));
        return query.list();
    }

    @Override
    public CampaignEvaluationInfo findOneById(Long id) {
        String sql ="select"
                + " a.CAMPAIGN_EVALUATION_INFO_NAME as campaignEvaluationInfoName,"
                + " a.CAMPAIGN_EVALUATION_INFO_ID as campaignEvaluationInfoId,"
                + " a.CAMPAIGN_ID as campaignId,"
                + " a.SCHEDULE_ID as scheduleId,"
                + " a.REASIONING_METHOD as reasioningMethod,"
                + " a.IS_DEFAULT as isDefault,"
                + " a.DESCRIPTION as description,"
                + " a.CATEGORY_ID as categoryId,"
                + " c.SCHEDULE_NAME as scheduleName,"
                + " b.CAMPAIGN_NAME as name,"
                + " b.IS_DEFAULT as isDefaultCampaign"
                + " from campaign_evaluation_info as a"
                + " inner join CAMPAIGN as b on a.CAMPAIGN_ID=b.CAMPAIGN_ID"
                + " inner join campaign_schedule as c on a.SCHEDULE_ID=c.SCHEDULE_ID"
                + " where a.CAMPAIGN_EVALUATION_INFO_ID =:id";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("campaignEvaluationInfoId", StandardBasicTypes.LONG);
        query.addScalar("campaignEvaluationInfoName", StandardBasicTypes.STRING);
        query.addScalar("campaignId", StandardBasicTypes.LONG);
        query.addScalar("scheduleId", StandardBasicTypes.LONG);
        query.addScalar("reasioningMethod", StandardBasicTypes.INTEGER);
        query.addScalar("isDefault", StandardBasicTypes.BOOLEAN);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("categoryId", StandardBasicTypes.LONG);
        query.addScalar("scheduleName", StandardBasicTypes.STRING);
        query.addScalar("name", StandardBasicTypes.STRING);
        query.addScalar("isDefaultCampaign", StandardBasicTypes.INTEGER);
        query.setParameter("id", id);
        query.setResultTransformer(Transformers.aliasToBean(CampaignEvaluationInfo.class));
        return (CampaignEvaluationInfo) query.uniqueResult();
    }

}
