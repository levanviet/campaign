/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.io.Serializable;
import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.ResultClassDAOInfterface;
import vn.viettel.campaign.entities.ResultClass;

/**
 *
 * @author ABC
 */
@Repository
public class ResultClassDAOImpl extends BaseDAOImpl<ResultClass> implements ResultClassDAOInfterface {

    @Override
    public List<ResultClass> getListResultClassByOwnerIdAndCampaignEvaluationInfoId(Long campaignId, Long campaignEvaluationInfoId) {
        String sql = "Select"
                + " a.CLASS_ID as classId,"
                + " a.OWNER_ID as ownerId,"
                + " a.CLASS_NAME as className,"
                + " a.DESCRIPTION as description,"
                + " a.CLASS_LEVEL as classLevel,"
                + " a.CAMPAIGN_EVALUATION_INFO_ID as campaignEvaluationInfoId "
                + " FROM result_class as a"
                + " WHERE a.CLASS_LEVEL = 3 AND a.OWNER_ID=:campaignId AND a.CAMPAIGN_EVALUATION_INFO_ID =:campaignEvaluationInfoId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("classId", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("className", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("classLevel", StandardBasicTypes.INTEGER);
        query.addScalar("campaignEvaluationInfoId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(ResultClass.class));

        query.setParameter("campaignId", campaignId);
        query.setParameter("campaignEvaluationInfoId", campaignEvaluationInfoId);
        return query.list();
    }

    @Override
    public ResultClass getNextSequenceResultClass() {
        String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"result_class\"";
        SQLQuery queryUpdate = getSession().createSQLQuery(update);
        queryUpdate.executeUpdate();
        String sql = "SELECT table_id classId from SEQ_TABLE WHERE TABLE_NAME = \"result_class\"";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("classId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(ResultClass.class));
        return (ResultClass) query.uniqueResult();
    }

    @Override
    public ResultClass getNameByClassId(Long classId) {
        String sql = "Select"
                + " a.CLASS_NAME as className,"
                + " a.CLASS_ID as classId"
                + " FROM result_class as a"
                + " WHERE a.CLASS_ID =:classId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("className", StandardBasicTypes.STRING);
        query.addScalar("classId", StandardBasicTypes.LONG);

        query.setResultTransformer(Transformers.aliasToBean(ResultClass.class));
        query.setParameter("classId", classId);
        return (ResultClass) query.uniqueResult();
    }

    @Override
    public ResultClass doCreateOrUpdateResultClass(ResultClass resultClass) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            doSaveOrUpdate(resultClass);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return resultClass;
    }

   
    @Override
    public boolean deleteResultClass(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            deleteById(ResultClass.class, id);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();;
            tx.rollback();
            return false;
        } finally {
            session.close();
        }
        return true;
    }

    @Override
    public List<ResultClass> getListResultClassByAssessmentRuleId(Long assessmentRuleId) {
        String sql = "Select"
                + " a.CLASS_ID as classId,"
                + " a.OWNER_ID as ownerId,"
                + " a.CLASS_NAME as className,"
                + " a.DESCRIPTION as description,"
                + " a.CLASS_LEVEL as classLevel,"
                + " a.CAMPAIGN_EVALUATION_INFO_ID as campaignEvaluationInfoId "
                + " FROM result_class as a"
                + " WHERE a.CLASS_LEVEL = 1 AND a.OWNER_ID=:assessmentRuleId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("classId", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("className", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("classLevel", StandardBasicTypes.INTEGER);
        query.addScalar("campaignEvaluationInfoId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(ResultClass.class));

        query.setParameter("assessmentRuleId", assessmentRuleId);
        return query.list();
    }

    @Override
    public List<ResultClass> getListResultClassOfSegment(Long segmentId, Long campaignEvaluationInfoId) {
        String sql = "Select"
                + " a.CLASS_ID as classId,"
                + " a.OWNER_ID as ownerId,"
                + " a.CLASS_NAME as className,"
                + " a.DESCRIPTION as description,"
                + " a.CLASS_LEVEL as classLevel,"
                + " a.CAMPAIGN_EVALUATION_INFO_ID as campaignEvaluationInfoId "
                + " FROM result_class as a"
                + " WHERE a.CLASS_LEVEL = 2 AND a.OWNER_ID=:segmentId AND a.CAMPAIGN_EVALUATION_INFO_ID =:campaignEvaluationInfoId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("classId", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("className", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("classLevel", StandardBasicTypes.INTEGER);
        query.addScalar("campaignEvaluationInfoId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(ResultClass.class));

        query.setParameter("segmentId", segmentId);
        query.setParameter("campaignEvaluationInfoId", campaignEvaluationInfoId);
        return query.list();
    }

}
