/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.TemporalType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.UsersDao;
import vn.viettel.campaign.entities.Users;

@Repository
@Transactional(rollbackFor = Exception.class)
public class UsersDaoImpl implements UsersDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Users> getLstUsersByName(String name, Long userId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Users> lstObj;
        if (userId != null) {
            lstObj = session.createQuery("from Users where lower(userName) = :userName and status = 1 and userId <> :userId")
                    .setParameter("userName", name.toLowerCase())
                    .setParameter("userId", userId)
                    .list();
        } else {
            lstObj = session.createQuery("from Users where lower(userName) = :userName and status = 1").setParameter("userName", name.toLowerCase()).list();
        }
        return lstObj.size() > 0 ? lstObj : null;
    }

    @Override
    public Boolean saveUsers(Users obj) {
        Session session = this.sessionFactory.getCurrentSession();
        DataUtil.trimObject(obj);
        session.save(obj);
        return true;
    }

    @Override
    public Boolean updateUsers(Users obj) {
        Session session = this.sessionFactory.getCurrentSession();
        DataUtil.trimObject(obj);
        session.update(obj);
        return true;
    }

    @Override
    public List<Users> getLstUsers() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Users> lstObj = session.createQuery("from Users where status = 1").list();
        return lstObj.size() > 0 ? lstObj : null;
    }

    @Override
    public void changePassword(Long userId, String newpass) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("update Users set passWord = :passWord,lastChangePassword = :lastPass,updateDate = :updateDate where userId = :userId");
        //HaBM2-20012020: Fix not change updateTime
        query.setParameter("passWord", newpass);
        query.setParameter("lastPass", newpass);
        query.setParameter("updateDate", new Date());
        query.setParameter("userId", userId);
        query.executeUpdate();
    }
}
