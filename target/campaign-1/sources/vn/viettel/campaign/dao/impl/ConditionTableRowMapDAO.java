package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.ConditionTableRowMapDAOInterface;
import vn.viettel.campaign.dao.ProcessValueDAOInterface;
import vn.viettel.campaign.entities.ConditionTableRowMap;
import vn.viettel.campaign.entities.ProcessValue;

/**
 *
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class ConditionTableRowMapDAO extends BaseDAOImpl<ConditionTableRowMap> implements ConditionTableRowMapDAOInterface {

    @Override
    public void deleteByConditionTableId(Long conditionTableId) {
        try {
            if (!DataUtil.isNullObject(conditionTableId)) {
                String sql = "DELETE FROM condition_table_row_map \n" +
                        "WHERE\n" +
                        "\tcondition_table_row_map.CONDITION_TABLE_ID = :conditionTableId";
                getSession().createSQLQuery(sql).setParameter("conditionTableId", conditionTableId).executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
