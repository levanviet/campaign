package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.CampaignScheduleDAOInterface;
import vn.viettel.campaign.dao.CdrServiceDAOInterface;
import vn.viettel.campaign.entities.CampaignSchedule;
import vn.viettel.campaign.entities.CdrService;
import vn.viettel.campaign.entities.Rule;

import javax.persistence.Query;
import java.util.List;
import static jdk.nashorn.internal.objects.NativeError.printStackTrace;
import org.hibernate.Transaction;

/**
 * @author truongbx
 * @date 09/01/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class CampaignScheduleDAO extends BaseDAOImpl<CampaignSchedule> implements CampaignScheduleDAOInterface {

	@Override
	public List<CampaignSchedule> getLstCampaignSchedule(Long campaignId) {
		List<CampaignSchedule> lstObj = getSession().createQuery("from CampaignSchedule where campaignId = :campaignId")
				.setParameter("campaignId", campaignId)
				.list();
		return lstObj;
	}

	@Override
	public CampaignSchedule getNextSchedule() {
			String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"SCHEDULE\"";
			SQLQuery queryUpdate = getSession().createSQLQuery(update);
			queryUpdate.executeUpdate();
			String sql = "SELECT table_id scheduleId  from SEQ_TABLE WHERE TABLE_NAME = \"SCHEDULE\"";
			SQLQuery query = getSession().createSQLQuery(sql);
			query.addScalar("scheduleId", StandardBasicTypes.LONG);
			query.setResultTransformer(Transformers.aliasToBean(CampaignSchedule.class));
			return (CampaignSchedule)query.uniqueResult();

	}
	@Override
	public void deleteCampaignScheduleByCampaignId(Session session, Long campaignId) {
		String hql = "delete from CampaignSchedule where campaignId =:campaignId";
		Query query = session.createQuery(hql);
		query.setParameter("campaignId", campaignId);
		Integer num = query.executeUpdate();
		getLog().info(" number row delete at " + campaignId + " is " + num);
	}

    @Override
    public boolean checkExisScheduleName(String scheduleName) {
        List<Rule> lst = getSession().createQuery("from CampaignSchedule where lower(scheduleName) = :scheduleName ")
                .setParameter("scheduleName", scheduleName.trim().toLowerCase())
                .list();
        if (lst != null && lst.size() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public CampaignSchedule getCampaignScheduleByScheduleId(Long id) {
        String sql = "select"
                + " a.SCHEDULE_ID as scheduleId,"
                + " a.SCHEDULE_NAME as scheduleName,"
                + " a.DESCRIPTION as description,"
                + " a.SCHEDULE_TYPE as scheduleType,"
                + " a.CAMPAIGN_ID as campaignId,"
                + " a.SCHEDULE_PATTERN as schedulePattern,"
                + " a.MODE as mode"
                + " from campaign_schedule as a"
                + " where SCHEDULE_ID=:id";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("scheduleId", StandardBasicTypes.LONG);
        query.addScalar("scheduleName", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("scheduleType", StandardBasicTypes.LONG);
        query.addScalar("campaignId", StandardBasicTypes.LONG);
        query.addScalar("schedulePattern", StandardBasicTypes.STRING);
        query.addScalar("mode", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(CampaignSchedule.class));
        query.setParameter("id", id);

        return (CampaignSchedule) query.uniqueResult();

    }

    @Override
    public CampaignSchedule getNextSequneceCampaignSchedule() {
        String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"campaign_schedule\"";
        SQLQuery queryUpdate = getSession().createSQLQuery(update);
        queryUpdate.executeUpdate();
        String sql = "SELECT table_id scheduleId from SEQ_TABLE WHERE TABLE_NAME = \"campaign_schedule\"";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("scheduleId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(CampaignSchedule.class));
        return (CampaignSchedule) query.uniqueResult();
    }

    @Override
    public CampaignSchedule doCreateOrUpdateCampaignSchedule(CampaignSchedule campaignSchedule) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            doSaveOrUpdate(campaignSchedule);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return campaignSchedule;
    }

    @Override
    public boolean deleteCampaignSchedule(Long id) {
         Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            deleteById(CampaignSchedule.class, id);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            ex.printStackTrace();
            return false;
        } finally {
            session.close();
        }
        return true;
    }
}
