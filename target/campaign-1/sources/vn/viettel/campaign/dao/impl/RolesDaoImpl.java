/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.RolesDao;
import vn.viettel.campaign.entities.RoleObject;
import vn.viettel.campaign.entities.RoleUser;
import vn.viettel.campaign.entities.Roles;

@Repository
@Transactional(rollbackFor = Exception.class)
public class RolesDaoImpl implements RolesDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Roles> getLstRolesByName(String name, Long roleId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Roles> lstObj;
        if (roleId != null) {
            lstObj = session.createQuery("from Roles where lower(roleName) = :roleName and status = 1 and roleId <> :roleId")
                    .setParameter("roleName", name.toLowerCase())
                    .setParameter("roleId", roleId)
                    .list();
        } else {
            lstObj = session.createQuery("from Roles where lower(roleName) = :roleName and status = 1").setParameter("roleName", name.toLowerCase()).list();
        }
        return lstObj.size() > 0 ? lstObj : null;
    }

    @Override
    public List<Roles> getLstRolesByCode(String code, Long roleId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Roles> lstObj;
        if (roleId != null) {
            lstObj = session.createQuery("from Roles where lower(roleCode) = :roleCode and status = 1 and roleId <> :roleId")
                    .setParameter("roleCode", code.toLowerCase())
                    .setParameter("roleId", roleId)
                    .list();
        } else {
            lstObj = session.createQuery("from Roles where lower(roleCode) = :roleCode and status = 1").setParameter("roleCode", code.toLowerCase()).list();
        }
        return lstObj.size() > 0 ? lstObj : null;
    }

    @Override
    public Boolean saveRoles(Roles obj) {
        Session session = this.sessionFactory.getCurrentSession();
        DataUtil.trimObject(obj);
        session.save(obj);
        return true;
    }

    @Override
    public Boolean updateRoles(Roles obj) {
        Session session = this.sessionFactory.getCurrentSession();
        DataUtil.trimObject(obj);
        session.update(obj);
        return true;
    }

    @Override
    public List<Roles> getLstRoles() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Roles> lstObj;
        lstObj = session.createQuery("from Roles where status = 1").list();
        return lstObj.size() > 0 ? lstObj : null;
    }

    @Override
    public List<Long> getRolesByUserId(Long userId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Long> lstObj;
        lstObj = session.createQuery("select roleId from Roles where roleId in (select roleId from RoleUser where userId = :userId and isActive = 1) and status = 1")
                .setParameter("userId", userId).list();
        return lstObj.size() > 0 ? lstObj : null;
    }

    @Override
    public Roles getRolesById(Long roleId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Roles> lstObj;
        lstObj = session.createQuery("from Roles where roleId = :roleId and status = 1")
                .setParameter("roleId", roleId).list();
        return lstObj.size() > 0 ? lstObj.get(0) : null;
    }

}
