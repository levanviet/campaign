package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.ConditionTableColumnMapDAOInterface;
import vn.viettel.campaign.dao.ConditionTableRowMapDAOInterface;
import vn.viettel.campaign.entities.ConditionTableColumnMap;
import vn.viettel.campaign.entities.ConditionTableRowMap;

/**
 *
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class ConditionTableColumnMapDAO extends BaseDAOImpl<ConditionTableColumnMap> implements ConditionTableColumnMapDAOInterface {
    @Override
    public void deleteByConditionTableId(Long conditionTableId) {
        try {
            if (!DataUtil.isNullObject(conditionTableId)) {
                String sql = "DELETE FROM condition_table_column_map \n" +
                        "WHERE\n" +
                        "\tcondition_table_column_map.CONDITION_TABLE_ID = :conditionTableId";
                getSession().createSQLQuery(sql).setParameter("conditionTableId", conditionTableId).executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
