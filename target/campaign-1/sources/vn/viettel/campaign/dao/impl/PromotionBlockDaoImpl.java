package vn.viettel.campaign.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.hibernate.SQLQuery;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.PromotionBlockDao;
import vn.viettel.campaign.dto.BehaviourDTO;
import vn.viettel.campaign.entities.PromotionBlock;
import vn.viettel.campaign.entities.Result;

@Repository
public class PromotionBlockDaoImpl extends BaseDAOImpl<PromotionBlock> implements PromotionBlockDao {

    @Override
    public List<PromotionBlock> findByCategory(final List<Long> categoryIds) {
        try {
            final String sql = "from PromotionBlock where categoryId in (:categoryIds) order by id desc ";
            Query query = getSession().createQuery(sql);
            query.setParameterList("categoryIds", categoryIds);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public List<BehaviourDTO> findBehaviourByPromotionBlockId(final Long promotionBlockId) {
        if (Objects.isNull(promotionBlockId)) {
            return new ArrayList<>();
        }
        try {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("select * from ( ");
            sqlQuery.append("SELECT bc.PROM_TYPE promotionType,ob.BEHAVIOUR_ID promotionId, ");
            sqlQuery.append("ob.BEHAVIOUR_NAME promotionName,bc.PROMOTION_BLOCK_ID promotionBlockId, ");
            sqlQuery.append("ob.CATEGORY_ID categoryId,bc.OCS_PARENT_ID ocsParentId ");
            sqlQuery.append("FROM block_container bc ");
            sqlQuery.append("INNER JOIN ocs_behaviour ob ON bc.PROM_ID = ob.BEHAVIOUR_ID ");
            sqlQuery.append("WHERE bc.PROM_TYPE = 1 ");
            sqlQuery.append("union all ");
            sqlQuery.append("SELECT bc.PROM_TYPE promotionType,nt.NOTIFY_TRIGGER_ID promotionId, ");
            sqlQuery.append("nt.TRIGGER_NAME promotionName,bc.PROMOTION_BLOCK_ID promotionBlockId, ");
            sqlQuery.append("nt.CATEGORY_ID categoryId,bc.OCS_PARENT_ID ocsParentId ");
            sqlQuery.append("FROM block_container bc ");
            sqlQuery.append("INNER JOIN notify_trigger nt ON bc.PROM_ID = nt.NOTIFY_TRIGGER_ID ");
            sqlQuery.append("WHERE bc.PROM_TYPE = 2 ");
            sqlQuery.append("union all ");
            sqlQuery.append("SELECT  bc.PROM_TYPE promotionType,st.SCENARIO_TRIGGER_ID promotionId, ");
            sqlQuery.append("st.TRIGGER_NAME promotionName,bc.PROMOTION_BLOCK_ID promotionBlockId, ");
            sqlQuery.append("st.CATEGORY_ID categoryId,bc.OCS_PARENT_ID ocsParentId ");
            sqlQuery.append("FROM block_container bc ");
            sqlQuery.append("INNER JOIN scenario_trigger st ON bc.PROM_ID = st.SCENARIO_TRIGGER_ID ");
            sqlQuery.append("WHERE  bc.PROM_TYPE = 3 ");
            sqlQuery.append(") behaviour where behaviour.promotionBlockId = :promotionBlockId ");

            SQLQuery query = getSession().createSQLQuery(sqlQuery.toString());
            query.setParameter("promotionBlockId", promotionBlockId);
            query.addScalar("promotionType", StandardBasicTypes.LONG);
            query.addScalar("promotionId", StandardBasicTypes.LONG);
            query.addScalar("promotionName", StandardBasicTypes.STRING);
            query.addScalar("promotionBlockId", StandardBasicTypes.LONG);
            query.addScalar("categoryId", StandardBasicTypes.LONG);
            query.addScalar("ocsParentId", StandardBasicTypes.LONG);
            query.setResultTransformer(Transformers.aliasToBean(BehaviourDTO.class));
            return query.list();
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public List<BehaviourDTO> findOcsBehaviour(final List<Long> ids) {
        try {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("SELECT 1 promotionType,ob.BEHAVIOUR_ID promotionId, ");
            sqlQuery.append("ob.BEHAVIOUR_NAME promotionName,ob.CATEGORY_ID categoryId, ");
            sqlQuery.append("ob.NOTIFY_ID notifyId ");
            sqlQuery.append("FROM ocs_behaviour ob ");
            if (ids != null && !ids.isEmpty()) {
                sqlQuery.append("WHERE ob.OCS_BEHAVIOUR_ID NOT IN (:ocsBehaviourIds) ");
            }
            SQLQuery query = getSession().createSQLQuery(sqlQuery.toString());
            if (ids != null && !ids.isEmpty()) {
                query.setParameter("ocsBehaviourIds", ids);
            }
            query.addScalar("promotionType", StandardBasicTypes.LONG);
            query.addScalar("promotionId", StandardBasicTypes.LONG);
            query.addScalar("promotionName", StandardBasicTypes.STRING);
            query.addScalar("categoryId", StandardBasicTypes.LONG);
            query.addScalar("notifyId", StandardBasicTypes.LONG);
            query.setResultTransformer(Transformers.aliasToBean(BehaviourDTO.class));
            return query.list();
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public List<BehaviourDTO> findNotifyTrigger(final List<Long> ids) {
        try {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("SELECT 2 promotionType,nt.NOTIFY_TRIGGER_ID promotionId, ");
            sqlQuery.append("nt.TRIGGER_NAME promotionName,nt.CATEGORY_ID categoryId ");
            sqlQuery.append("FROM notify_trigger nt ");
            sqlQuery.append("WHERE nt.IS_INVITE = 1 ");
            if (ids != null && !ids.isEmpty()) {
                sqlQuery.append("and nt.NOTIFY_TRIGGER_ID IN (:notifyTriggers) ");
            }
            SQLQuery query = getSession().createSQLQuery(sqlQuery.toString());
            if (ids != null && !ids.isEmpty()) {
                query.setParameter("notifyTriggers", ids);
            }
            query.addScalar("promotionType", StandardBasicTypes.LONG);
            query.addScalar("promotionId", StandardBasicTypes.LONG);
            query.addScalar("promotionName", StandardBasicTypes.STRING);
            query.addScalar("categoryId", StandardBasicTypes.LONG);
            query.setResultTransformer(Transformers.aliasToBean(BehaviourDTO.class));
            return query.list();
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public List<BehaviourDTO> findScenarioTrigger(final List<Long> ids) {
        try {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("SELECT 3 promotionType,st.SCENARIO_TRIGGER_ID promotionId, ");
            sqlQuery.append("st.TRIGGER_NAME promotionName,st.CATEGORY_ID categoryId ");
            sqlQuery.append("FROM scenario_trigger st ");
            if (ids != null && !ids.isEmpty()) {
                sqlQuery.append("WHERE st.SCENARIO_TRIGGER_ID IN (:scenarioTriggers) ");
            }
            SQLQuery query = getSession().createSQLQuery(sqlQuery.toString());
            if (ids != null && !ids.isEmpty()) {
                query.setParameter("scenarioTriggers", ids);
            }
            query.addScalar("promotionType", StandardBasicTypes.LONG);
            query.addScalar("promotionId", StandardBasicTypes.LONG);
            query.addScalar("promotionName", StandardBasicTypes.STRING);
            query.addScalar("categoryId", StandardBasicTypes.LONG);
            query.setResultTransformer(Transformers.aliasToBean(BehaviourDTO.class));
            return query.list();
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public boolean checkDuplicate(final String name, final Long id) {
        try {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("from PromotionBlock where name = :name ");
            if (Objects.nonNull(id)) {
                sqlQuery.append("and id not in :id ");
            }
            Query query = getSession().createQuery(sqlQuery.toString());
            query.setParameter("name", name);
            if (Objects.nonNull(id)) {
                query.setParameter("id", id);
            }
            query.setCacheable(true);
            if (query.list() != null && !query.list().isEmpty()) {
                return false;
            }
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
            return false;
        }
        return true;
    }

    @Override
    public boolean checkInUseResult(Long promotionBlockId) {
        List<Result> lstObj = getSession().createQuery("from Result where  promotionBlockId =:promotionBlockId")
                .setParameter("promotionBlockId", BigInteger.valueOf(promotionBlockId))
                .list();
        if (lstObj == null || lstObj.isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public void deleteBlockContainerByPrmId(final Long prmId) throws Exception {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("delete from block_container where PROMOTION_BLOCK_ID = :prmId ");
        SQLQuery query = getSession().createSQLQuery(sqlQuery.toString());
        query.setParameter("prmId", prmId);
        query.executeUpdate();
    }

    @Override
    public Long genPrmBlockId() throws Exception {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("SELECT AUTO_INCREMENT FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'promotion_block' ");
        SQLQuery query = getSession().createSQLQuery(sqlQuery.toString());
        return ((Number) query.uniqueResult()).longValue();
    }

    @Override
    public Long checkUsePromotionBlock(Long promType, Long promId) throws Exception {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("select COUNT(*) from block_container where prom_id = :promId and prom_type = :promType");
        SQLQuery query = getSession().createSQLQuery(sqlQuery.toString()).setParameter("promId", promId).setParameter("promType", promType);
        return ((Number) query.uniqueResult()).longValue();
    }
}
