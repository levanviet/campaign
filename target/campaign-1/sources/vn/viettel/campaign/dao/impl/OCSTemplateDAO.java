package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.common.Responses;
import vn.viettel.campaign.dao.OCSTemplateDaoInterface;
import vn.viettel.campaign.dao.OCSTemplateExtendFieldDaoInterface;
import vn.viettel.campaign.dao.OCSTemplateFieldDaoInterface;
import vn.viettel.campaign.entities.*;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class OCSTemplateDAO extends BaseDAOImpl<OcsBehaviourTemplate> implements OCSTemplateDaoInterface {
	@Autowired
	OCSTemplateFieldDaoInterface oCSTemplateFieldDAO;
	@Autowired
	OCSTemplateExtendFieldDaoInterface oCSTemplateExtendFieldDAO;

	public OcsBehaviourTemplate getNextSequense() {
		String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"OCS_TEMPLATE\"";
		SQLQuery queryUpdate = getSession().createSQLQuery(update);
		queryUpdate.executeUpdate();
		String sql = "SELECT table_id templateId  from SEQ_TABLE WHERE TABLE_NAME = \"OCS_TEMPLATE\"";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("templateId", StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(OcsBehaviourTemplate.class));
		return (OcsBehaviourTemplate) query.uniqueResult();
	}

	@Override
	public String onSaveBehaviourTemplate(OcsBehaviourTemplate ocsBehaviourTemplate,
										  List<OcsBehaviourTemplateFields> lstField, List<OcsBehaviourTemplateExtendFields> lstExtendFields) {
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			save(session, ocsBehaviourTemplate);

			String resultField = oCSTemplateFieldDAO.save(session, lstField);
			if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultField)) {
				throw new Exception();
			}
			if (lstExtendFields != null && !lstExtendFields.isEmpty()) {
				String resultExtendField = oCSTemplateExtendFieldDAO.save(session, lstExtendFields);
				if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultExtendField)) {
					throw new Exception();
				}
			}
			tx.commit();

		} catch (Exception ex) {
			ex.printStackTrace();
			if (tx != null){
			tx.rollback();
			}
			return Responses.ERROR.getName();
		} finally {
			if (session !=null){
			session.close();
			}
		}
		return Responses.SUCCESS.getName();
	}

	@Override
	public String onUpdateBehaviourTemplate(OcsBehaviourTemplate ocsBehaviourTemplate, List<OcsBehaviourTemplateFields> lstField,
											List<OcsBehaviourTemplateExtendFields> lstExtendFields) {
		Session session = null;
		Transaction tx = null;

		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			update(session, ocsBehaviourTemplate);

			oCSTemplateFieldDAO.deleteTemplateFieldByTemplateId(session, ocsBehaviourTemplate.getTemplateId());
			String result = oCSTemplateFieldDAO.save(session, lstField);
			if (!Responses.SUCCESS.getName().equalsIgnoreCase(result)) {
				throw new Exception();
			}
			oCSTemplateExtendFieldDAO.deleteTemplateExtendFieldByTemplateId(session, ocsBehaviourTemplate.getTemplateId());
			if (lstExtendFields != null && !lstExtendFields.isEmpty()) {
				String resultExtend = oCSTemplateExtendFieldDAO.save(session, lstExtendFields);
				if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultExtend)) {
					throw new Exception();
				}
			}
			tx.commit();

		} catch (Exception ex) {
			ex.printStackTrace();
			if (tx !=null){
			tx.rollback();
			}
			return Responses.ERROR.getName();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return Responses.SUCCESS.getName();
	}

	@Override
	public boolean deleteTemplate(Long templateId) {
		Session session = null;
		Transaction tx = null;

		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			deleteById(OcsBehaviourTemplate.class, templateId, session);
			oCSTemplateFieldDAO.deleteTemplateFieldByTemplateId(session, templateId);
			oCSTemplateExtendFieldDAO.deleteTemplateExtendFieldByTemplateId(session, templateId);
			tx.commit();

		} catch (Exception ex) {
			ex.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return true;
	}

	@Override
	public boolean checkTemplateName(String templateName, Long templateId) {
		List<Campaign> lst = getSession().createQuery("from OcsBehaviourTemplate where lower(templateName) = :templateName and templateId <> :templateId ")
				.setParameter("templateName", templateName.trim().toLowerCase())
				.setParameter("templateId", templateId)
				.list();
		if (lst != null && lst.size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean checkOcsBehaviourTemplateInUse(Long templateId) {
			List<OcsBehaviour> lstObj = getSession().createQuery("from OcsBehaviour where  templateId =:templateId")
					.setParameter("templateId", templateId)
					.list();
			if (lstObj == null || lstObj.isEmpty()) {
				return false;
			}
			return true;
	}
}
