/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.InputFieldVariableMapDAO;
import vn.viettel.campaign.entities.InputFieldVariableMap;
import vn.viettel.campaign.entities.KpiProcessor;

/**
 *
 * @author SON
 */
@Repository
public class InputFieldVariableMapDAOImpl extends BaseDAOImpl<InputFieldVariableMap> implements InputFieldVariableMapDAO {

    @Override
    public List<InputFieldVariableMap> getLstInputFieldVariableMapByTypeAndId(int type, Long id) {
        String sql = "SELECT ID as id,"
                + " OWNER_ID as ownerId,"
                + " VARIABLE_NAME as variableName,"
                + " TYPE as type,"
                + " VARIABLE_VALUE as variableValue"
                + " FROM input_field_variable_map"
                + " WHERE TYPE=:type AND OWNER_ID =:id";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("id", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("variableName", StandardBasicTypes.STRING);
        query.addScalar("type", StandardBasicTypes.INTEGER);
        query.addScalar("variableValue", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(InputFieldVariableMap.class));

        query.setParameter("type", type);
        query.setParameter("id", id);

        return query.list();
    }

    @Override
    public void onSaveOrUpdateInputFieldVariableMap(InputFieldVariableMap inputFieldVariableMap) {
        getSession().saveOrUpdate(inputFieldVariableMap);
    }

    @Override
    public void onDeleteInputFieldVariableMap(int type, Long id) {
        String sql = "Delete from input_field_variable_map where type =:type and owner_id = :pre_processor_id";
        getSession().createSQLQuery(sql).setParameter("type", type).setParameter("pre_processor_id",id).executeUpdate();
    }

    @Override
    public InputFieldVariableMap findOneById(Long id) {
        String sql = "SELECT ID as id,"
                + " OWNER_ID as ownerId,"
                + " VARIABLE_NAME as variableName,"
                + " TYPE as type,"
                + " VARIABLE_VALUE as variableValue"
                + " FROM input_field_variable_map"
                + " WHERE ID=:id";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("id", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("variableName", StandardBasicTypes.STRING);
        query.addScalar("type", StandardBasicTypes.INTEGER);
        query.addScalar("variableValue", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(InputFieldVariableMap.class));

        query.setParameter("id", id);

        return (InputFieldVariableMap) query.uniqueResult();
    }

    @Override
    public InputFieldVariableMap getNextSequence() {
       String update = "UPDATE seq_table set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = 'INPUT_FIELD_VARIABLE_MAP' ";
       getSession().createSQLQuery(update).executeUpdate();
       String sql = "SELECT TABLE_ID as id FROM seq_table where TABLE_NAME = 'INPUT_FIELD_VARIABLE_MAP' ";
       SQLQuery query = getSession().createSQLQuery(sql);
       query.addScalar("id",StandardBasicTypes.LONG);
       query.setResultTransformer(Transformers.aliasToBean(InputFieldVariableMap.class));
       return (InputFieldVariableMap) query.uniqueResult();
    }

}
