package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.PreprocessUnitDaoInterface;
import vn.viettel.campaign.dao.ZoneDAOInterface;
import vn.viettel.campaign.entities.Campaign;
import vn.viettel.campaign.entities.PreProcessUnit;
import vn.viettel.campaign.entities.Zone;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class ZoneDAO extends BaseDAOImpl<Zone> implements ZoneDAOInterface {

}
