/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.LoginDao;
import vn.viettel.campaign.entities.Objects;
import vn.viettel.campaign.entities.Roles;
import vn.viettel.campaign.entities.Users;

@Repository
@Transactional(rollbackFor = Exception.class)
public class LoginDaoImpl implements LoginDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Users loadUserByUsername(String username) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Users> users;
        users = session.createQuery("from Users where status = 1 and userName = :userName")
                .setParameter("userName", username).list();
        return users.size() > 0 ? users.get(0) : null;
    }

    @Override
    public List<Roles> getListRole(Long userId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Roles> lstRole;
        lstRole = session.createQuery("from Roles where status = 1 "
                + "and roleId in (select roleId from RoleUser where isActive = 1 and userId = :userId)")
                .setParameter("userId", userId)
                .list();
        return lstRole.size() > 0 ? lstRole : null;
    }

    @Override
    public List<Objects> getLstObjects(Long userId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Objects> lstObj;
        lstObj = session.createQuery("select distinct o from Objects o where o.objectId in "
                + "(select ro.objectId from RoleObject ro where ro.isActive = 1 and ro.roleId in "
                + "(select roleId from RoleUser where isActive = 1 and userId = :userId) )"
                + " and o.status = 1 order by o.parentId, o.ord")
                .setParameter("userId", userId).list();
        return lstObj.size() > 0 ? arrangeLstObject(lstObj) : null;
    }

    private List<Objects> arrangeLstObject(List<Objects> lstObj) {
        List<Objects> subObj = new ArrayList();
        lstObj.forEach((obj) -> {
            if (obj.getParentId() != null) {
                subObj.add(obj);
            }
        });
        lstObj.removeAll(subObj);
        lstObj.forEach((obj) -> {
            subObj.forEach((sub) -> {
                if (obj.getObjectId().longValue() == sub.getParentId()) {
                    obj.getLstSub().add(sub);
                }
            });
        });
        return lstObj;
    }

}
