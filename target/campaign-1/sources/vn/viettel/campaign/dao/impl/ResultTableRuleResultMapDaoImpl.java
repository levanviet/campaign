/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.BaseAbstract;
import vn.viettel.campaign.dao.ResultDao;
import vn.viettel.campaign.dao.RuleResultDao;
import vn.viettel.campaign.entities.Result;
import vn.viettel.campaign.entities.ResultTableRuleResultMap;
import vn.viettel.campaign.entities.RowCt;
import vn.viettel.campaign.entities.RuleResult;
import vn.viettel.campaign.dao.ResultTableRuleResultMapDao;


@Repository
@Transactional(rollbackFor = Exception.class)
public class ResultTableRuleResultMapDaoImpl extends BaseDAOImpl<Result> implements ResultTableRuleResultMapDao {
    private static final Logger log = Logger.getLogger(ResultTableRuleResultMapDaoImpl.class.getName());

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<ResultTableRuleResultMap> finAllResultTableRuleResultMap() {
        Session session = this.sessionFactory.getCurrentSession();
        List<ResultTableRuleResultMap> lstObj;
        lstObj = session.createQuery("from result_table_rule_result_map").list();
        return lstObj.size() > 0 ? lstObj : null;    
    }

    @Override
    public ResultTableRuleResultMap updateOrSave(ResultTableRuleResultMap resultTableRuleResultMap) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(resultTableRuleResultMap);
        return resultTableRuleResultMap;
    } 

    @Override
    public ResultTableRuleResultMap getResultTableRuleResultMapById(Long resultTableRuleResultId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<ResultTableRuleResultMap> lstObj;
        lstObj = session.createQuery("from result_table_rule_result_map where resultTableRuleResultId = :resultTableRuleResultId")
                .setParameter("resultTableRuleResultId", resultTableRuleResultId).list();
        return lstObj.size() > 0 ? lstObj.get(0) : null;
    }


    @Override
    public void deleteByResultTableId(long id) {
        String sql = "delete from result_table_rule_result_map where result_table_id = :id";
        getSession().createSQLQuery(sql).setParameter("id", id).executeUpdate();
    }
}
