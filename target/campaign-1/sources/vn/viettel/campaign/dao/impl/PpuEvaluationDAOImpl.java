/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.PpuEvaluationDAO;
import vn.viettel.campaign.entities.PpuEvaluation;
import vn.viettel.campaign.entities.ProcessParam;
import vn.viettel.campaign.entities.ProcessValue;

/**
 *
 * @author admin
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class PpuEvaluationDAOImpl extends BaseDAOImpl<PpuEvaluation> implements PpuEvaluationDAO {

    @Override
    public void onSaveOrUpdatePpuEvaluation(PpuEvaluation ppuEvaluation) {
        getSession().saveOrUpdate(ppuEvaluation);
    }

    @Override
    public PpuEvaluation getNextSequense() {
        String update = "UPDATE seq_table set TABLE_ID = TABLE_ID + 1"
                + " WHERE TABLE_NAME = 'PRE_PROCESS_UNIT'  ";

        getSession().createSQLQuery(update).executeUpdate();

        String sql = "SELECT TABLE_ID as preProcessId "
                + " FROM seq_table "
                + " WHERE TABLE_NAME = 'PRE_PROCESS_UNIT' ";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("preProcessId", StandardBasicTypes.LONG);

        query.setResultTransformer(Transformers.aliasToBean(PpuEvaluation.class));
        return (PpuEvaluation) query.uniqueResult();
    }

    @Override
    public void onDeletePpuEvaluation(PpuEvaluation ppuEvaluation) {
        getSession().delete(ppuEvaluation);
    }

    @Override
    public List<ProcessValue> getListProcessValue(Long id) {
        String sql = "SELECT process_value.PROCESS_VALUE_ID as processValueId,"
                + " VALUE_ID as valueId,"
                + " VALUE_NAME as valueName,"
                + " VALUE_INDEX as valueIndex,"
                + " DESCRIPTION as description,"
                + " VALUE_COLOR as valueColor"
                + " FROM process_value inner join pre_process_value_map"
                + " ON process_value.PROCESS_VALUE_ID = pre_process_value_map.PROCESS_VALUE_ID"
                + " WHERE pre_process_value_map.pre_process_id =:id";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("processValueId", StandardBasicTypes.LONG);
        query.addScalar("valueId", StandardBasicTypes.LONG);
        query.addScalar("valueName", StandardBasicTypes.STRING);
        query.addScalar("valueIndex", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("valueColor", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(ProcessValue.class));

        query.setParameter("id", id);

        return query.list();
    }

    @Override
    public List<ProcessParam> getListProcessParam(Long id) {
        String sql = "SELECT process_param.PROCESS_PARAM_ID as processParamId,"
                + " PARAM_INDEX as paramIndex,"
                + " CONFIG_INPUT as configInput"
                + " FROM process_param inner join pre_process_param_map"
                + " ON process_param.PROCESS_PARAM_ID = pre_process_param_map.PROCESS_PARAM_ID"
                + " WHERE pre_process_param_map.pre_process_id =:id";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("processParamId", StandardBasicTypes.LONG);
        query.addScalar("paramIndex", StandardBasicTypes.LONG);
        query.addScalar("configInput", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(ProcessParam.class));

        query.setParameter("id", id);

        return query.list();
    }

    @Override
    public List<PpuEvaluation> checkExitsProcessorName(String name, Long id) {
        String sql = "select *from pre_process_unit where PRE_PROCESS_NAME = :name AND PRE_PROCESS_ID != :id";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.setParameter("name", name);
        query.setParameter("id", id);
        return query.list();
    }

    @Override
    public List<PpuEvaluation> checkExitsCriteriaName(String name, Long id) {
        String sql = "select *from pre_process_unit where CRITERIA_NAME = :name AND PRE_PROCESS_ID != :id";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.setParameter("name", name);
        query.setParameter("id", id);
        return query.list();
    }

    @Override
    public List<PpuEvaluation> checkExitsValueName(Long processValue, String name, Long id) {
        String sql = "SELECT process_value.PROCESS_VALUE_ID as processValueId,"
                + " VALUE_ID as valueId,"
                + " VALUE_NAME as valueName,"
                + " VALUE_INDEX as valueIndex,"
                + " DESCRIPTION as description,"
                + " VALUE_COLOR as valueColor"
                + " FROM process_value inner join pre_process_value_map"
                + " ON process_value.PROCESS_VALUE_ID = pre_process_value_map.PROCESS_VALUE_ID"
                + " WHERE pre_process_value_map.pre_process_id =:id AND VALUE_NAME =:name and process_value.PROCESS_VALUE_ID != :processValue";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("processValueId", StandardBasicTypes.LONG);
        query.addScalar("valueId", StandardBasicTypes.LONG);
        query.addScalar("valueName", StandardBasicTypes.STRING);
        query.addScalar("valueIndex", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("valueColor", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(ProcessValue.class));

        query.setParameter("id", id);
        query.setParameter("name", name);
        query.setParameter("processValue", processValue);
        return query.list();
    }

    @Override
    public List<PpuEvaluation> checkExitsValueId(Long processValue, Long valueId, Long id) {
        String sql = "SELECT process_value.PROCESS_VALUE_ID as processValueId,"
                + " VALUE_ID as valueId,"
                + " VALUE_NAME as valueName,"
                + " VALUE_INDEX as valueIndex,"
                + " DESCRIPTION as description,"
                + " VALUE_COLOR as valueColor"
                + " FROM process_value inner join pre_process_value_map"
                + " ON process_value.PROCESS_VALUE_ID = pre_process_value_map.PROCESS_VALUE_ID"
                + " WHERE pre_process_value_map.pre_process_id =:id AND VALUE_ID =:valueId and process_value.PROCESS_VALUE_ID != :processValue";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("processValueId", StandardBasicTypes.LONG);
        query.addScalar("valueId", StandardBasicTypes.LONG);
        query.addScalar("valueName", StandardBasicTypes.STRING);
        query.addScalar("valueIndex", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("valueColor", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(ProcessValue.class));

        query.setParameter("id", id);
        query.setParameter("valueId", valueId);
        query.setParameter("processValue", processValue);
        return query.list();
    }

    @Override
    public List<PpuEvaluation> checkDeletePPUEvaluation(Long id) {
        String sql = "select * from assessment_rule_pre_process_unit_map where pre_process_unit_id = :id";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.setParameter("id", id);
        return query.list();
    }

    @Override
    public PpuEvaluation findOneById(Long id) {
        String sql = "SELECT PRE_PROCESS_ID as preProcessId,"
                + " PRE_PROCESS_NAME as preProcessName,"
                + " PRE_PROCESS_TYPE as preProcessType,"
                + " DEFAULT_VALUE as defaultValue,"
                + " INPUT_FIELDS as inputFields,"
                + " SPECIAL_FIELDS as specialFields,"
                + " DESCRIPTION as description,"
                + " CATEGORY_ID as categoryId,"
                + " TYPE as type,"
                + " CRITERIA_NAME as criteriaName"
                + " FROM  pre_process_unit WHERE PRE_PROCESS_ID=:id";
        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("preProcessId", StandardBasicTypes.LONG);
        query.addScalar("preProcessName", StandardBasicTypes.STRING);
        query.addScalar("preProcessType", StandardBasicTypes.INTEGER);
        query.addScalar("defaultValue", StandardBasicTypes.INTEGER);
        query.addScalar("inputFields", StandardBasicTypes.STRING);
        query.addScalar("specialFields", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("categoryId", StandardBasicTypes.LONG);
        query.addScalar("type", StandardBasicTypes.INTEGER);
        query.addScalar("criteriaName", StandardBasicTypes.STRING);

        query.setParameter("id", id);

        query.setResultTransformer(Transformers.aliasToBean(PpuEvaluation.class));

        return (PpuEvaluation) query.uniqueResult();

    }

    @Override
    public List<PpuEvaluation> getListPpuEvaluationPPUByRuleId(Long idd) {
        String sql = "select"
                + " a.PRE_PROCESS_ID as preProcessId,"
                + " a.PRE_PROCESS_NAME as preProcessName,"
                + " a.PRE_PROCESS_TYPE as preProcessType,"
                + " a.DEFAULT_VALUE as defaultValue,"
                + " a.INPUT_FIELDS as inputFields,"
                + " a.SPECIAL_FIELDS as specialFields,"
                + " a.DESCRIPTION as description,"
                + " a.CATEGORY_ID as categoryId,"
                + " a.TYPE as type,"
                + " a.CRITERIA_NAME as criteriaName"
                + " from pre_process_unit as a"
                + " inner join assessment_rule_pre_process_unit_map as b on a.PRE_PROCESS_ID=b.PRE_PROCESS_UNIT_ID"
                + " where a.TYPE=2 and b.RULE_ID=:idd";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("preProcessId", StandardBasicTypes.LONG);
        query.addScalar("preProcessName", StandardBasicTypes.STRING);
        query.addScalar("preProcessType", StandardBasicTypes.INTEGER);
        query.addScalar("defaultValue", StandardBasicTypes.INTEGER);
        query.addScalar("inputFields", StandardBasicTypes.STRING);
        query.addScalar("specialFields", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("categoryId", StandardBasicTypes.LONG);
        query.addScalar("type", StandardBasicTypes.INTEGER);
        query.addScalar("criteriaName", StandardBasicTypes.STRING);
        query.setParameter("idd", idd);
        query.setResultTransformer(Transformers.aliasToBean(PpuEvaluation.class));
        return query.list();
    }

    @Override
    public List<PpuEvaluation> getListPpuEvaluationTree(List<Long> categoryIds) {
        String sql = "select"
                + " a.PRE_PROCESS_ID as preProcessId,"
                + " a.PRE_PROCESS_NAME as preProcessName,"
                + " a.PRE_PROCESS_TYPE as preProcessType,"
                + " a.DEFAULT_VALUE as defaultValue,"
                + " a.INPUT_FIELDS as inputFields,"
                + " a.SPECIAL_FIELDS as specialFields,"
                + " a.DESCRIPTION as description,"
                + " a.CATEGORY_ID as categoryId,"
                + " a.TYPE as type,"
                + " a.CRITERIA_NAME as criteriaName"
                + " from pre_process_unit as a"
                + " where a.CATEGORY_ID in (:categoryIds)";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("preProcessId", StandardBasicTypes.LONG);
        query.addScalar("preProcessName", StandardBasicTypes.STRING);
        query.addScalar("preProcessType", StandardBasicTypes.INTEGER);
        query.addScalar("defaultValue", StandardBasicTypes.INTEGER);
        query.addScalar("inputFields", StandardBasicTypes.STRING);
        query.addScalar("specialFields", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("categoryId", StandardBasicTypes.LONG);
        query.addScalar("type", StandardBasicTypes.INTEGER);
        query.addScalar("criteriaName", StandardBasicTypes.STRING);
        query.setParameter("categoryIds", categoryIds);
        query.setResultTransformer(Transformers.aliasToBean(PpuEvaluation.class));
        return query.list();
    }

}
