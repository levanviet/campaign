package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.LanguageDAOInterface;
import vn.viettel.campaign.entities.Language;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class LanguageDAO extends BaseDAOImpl<Language> implements LanguageDAOInterface {
}
