package vn.viettel.campaign.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.SpecialpromDao;
import vn.viettel.campaign.entities.SpecialProm;

@Repository
@Transactional(rollbackFor = Exception.class)
public class SpecialpromDaoImpl extends BaseDAOImpl<SpecialProm> implements SpecialpromDao {

    @Override
    public List<SpecialProm> findSpecialpromByCategory(final List<Long> categoryIds) {
        try {
            final String sql = "from SpecialProm where categoryId in (:categoryIds)";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameterList("categoryIds", categoryIds);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public List<SpecialProm> findSpecialpromByName(String specialpromByName) {
        try {
            final String sql = "from SpecialProm where specialpromByName = :specialpromByName";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameter("specialpromByName", specialpromByName);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
        }
        return new ArrayList<>();
    }

    @Override
    public boolean checkDuplicate(final String name, final Long id) {
        try {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("from SpecialProm where name = :name ");
            if (Objects.nonNull(id)) {
                sqlQuery.append("and specialPromId not in :specialPromId ");
            }
            Query query = getSession().createQuery(sqlQuery.toString());
            query.setParameter("name", name);
            if (Objects.nonNull(id)) {
                query.setParameter("specialPromId", id);
            }
            query.setCacheable(true);
            if (query.list() != null && !query.list().isEmpty()) {
                return false;
            }
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
            return false;
        }
        return true;
    }

    @Override
    public boolean checkDuplicatePath(final String path, final Long id) {
        try {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("from SpecialProm where (path = :path or histPath = :histPath or failedPath = :failedPath) ");
            if (Objects.nonNull(id)) {
                sqlQuery.append("and specialPromId not in (:specialPromId) ");
            }
            Query query = getSession().createQuery(sqlQuery.toString());
            query.setParameter("path", path);
            query.setParameter("histPath", path);
            query.setParameter("failedPath", path);
            if (Objects.nonNull(id)) {
                query.setParameter("specialPromId", id);
            }
            query.setCacheable(true);
            if (query.list() != null && !query.list().isEmpty()) {
                return false;
            }
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
            return false;
        }
        return true;
    }

    @Override
    public long onSaveSpecialProm(SpecialProm specialProm) {
        long result = -999;
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            result = saveAndGetId(session, specialProm);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
}
