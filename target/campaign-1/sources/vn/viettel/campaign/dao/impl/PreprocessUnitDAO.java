package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.common.Responses;
import vn.viettel.campaign.dao.*;
import vn.viettel.campaign.entities.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class PreprocessUnitDAO extends BaseDAOImpl<PreProcessUnit> implements PreprocessUnitDaoInterface {

    @Autowired
    ProcessParamDAOInterface processParamDAO;
    @Autowired
    ProcessValueDAOInterface processValueDAO;
    @Autowired
    PreProcessValueMapDAOInterface preProcessValueMapDAO;
    @Autowired
    PreProcessParamMapDAOInterface preProcessParamMapDAO;
    @Autowired
    OCSTemplateExtendFieldDaoInterface oCSTemplateExtendFieldDAO;

    @Override
    public PreProcessUnit getNextSequense() {
        String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"PRE_PROCESS_UNIT\"";
        SQLQuery queryUpdate = getSession().createSQLQuery(update);
        queryUpdate.executeUpdate();
        String sql = "SELECT table_id preProcessId  from SEQ_TABLE WHERE TABLE_NAME = \"PRE_PROCESS_UNIT\"";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("preProcessId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(PreProcessUnit.class));
        return (PreProcessUnit) query.uniqueResult();
    }

    @Override
    public boolean checkExistPreProcessName(String preProcessName, Long preProcessId) {
        List<Campaign> lst = getSession().createQuery("from PreProcessUnit where lower(preProcessName) = :preProcessName and preProcessId <> :preProcessId ")
                .setParameter("preProcessName", preProcessName.trim().toLowerCase())
                .setParameter("preProcessId", preProcessId)
                .list();
        if (lst != null && lst.size() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public String onSavePreprocessUnit(PreProcessUnit preProcessUnit, List<ProcessParam> lstProcessParams,
            List<ProcessValue> lstProcessValue) {
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            save(session, preProcessUnit);
            Long ppuId = preProcessUnit.getPreProcessId();
            // tiepnv: check khac null moi insert
            if (!DataUtil.isNullOrEmpty(lstProcessParams)) {
                String resultField = processParamDAO.save(session, lstProcessParams);
                if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultField)) {
                    throw new Exception();
                }
                List<PreProcessParamMap> lstParamsMap = new ArrayList<>();
                for (ProcessParam processParam : lstProcessParams) {
                    PreProcessParamMap preProcessParamMap = new PreProcessParamMap();
                    preProcessParamMap.setPreProcessId(ppuId);
                    preProcessParamMap.setProcessParamId(processParam.getProcessParamId());
                    lstParamsMap.add(preProcessParamMap);
                }
                String resultParamMap = preProcessParamMapDAO.save(session, lstParamsMap);
                if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultParamMap)) {
                    throw new Exception();
                }
            }
            // tiepnv: check khac null moi insert
            if (!DataUtil.isNullOrEmpty(lstProcessValue)) {
                String resultProcessValue = processValueDAO.save(session, lstProcessValue);
                if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultProcessValue)) {
                    throw new Exception();
                }
                List<PreProcessValueMap> lstPreProcessValueMaps = new ArrayList<>();
                for (ProcessValue processValue : lstProcessValue) {
                    PreProcessValueMap preProcessValueMap = new PreProcessValueMap();
                    preProcessValueMap.setPreProcessId(ppuId);
                    preProcessValueMap.setProcessValueId(processValue.getProcessValueId());
                    lstPreProcessValueMaps.add(preProcessValueMap);
                }
                String resultProcessValueMap = preProcessValueMapDAO.save(session, lstPreProcessValueMaps);
                if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultProcessValueMap)) {
                    throw new Exception();
                }
            }
            tx.commit();

        } catch (Exception ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return Responses.ERROR.getName();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return Responses.SUCCESS.getName();
    }

    @Override
    public String onUpdatePreprocessUnit(PreProcessUnit preProcessUnit, List<ProcessParam> lstProcessParams,
            List<ProcessValue> lstProcessValue) {
        Session session = null;
        Transaction tx = null;

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            update(session, preProcessUnit);

            // tiepnv: check khac null moi insert
            if (!DataUtil.isNullOrEmpty(lstProcessParams)) {
                preProcessParamMapDAO.deletePreProcessParamMapByPPUid(session, preProcessUnit.getPreProcessId());
                String resultField = processParamDAO.save(session, lstProcessParams);
                if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultField)) {
                    throw new Exception();
                }
                List<PreProcessParamMap> lstParamsMap = new ArrayList<>();
                for (ProcessParam processParam : lstProcessParams) {
                    PreProcessParamMap preProcessParamMap = new PreProcessParamMap();
                    preProcessParamMap.setPreProcessId(preProcessUnit.getPreProcessId());
                    preProcessParamMap.setProcessParamId(processParam.getProcessParamId());
                    lstParamsMap.add(preProcessParamMap);
                }
                String resultParamMap = preProcessParamMapDAO.save(session, lstParamsMap);
                if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultParamMap)) {
                    throw new Exception();
                }
            }
            // tiepnv: check khac null moi insert
            if (!DataUtil.isNullOrEmpty(lstProcessValue)) {
                // tiepnv: check neu list la 2 doi tuong true va false thi ko update
                boolean notUpdate = false;
                if (lstProcessValue.size() == 2) {
                    long valId1 = lstProcessValue.get(0).getValueId();
                    long valId2 = lstProcessValue.get(1).getValueId();
                    notUpdate = (valId1 == 0 || valId1 == 1) && (valId2 == 0 || valId2 == 1);
                }
                if (notUpdate) {
                    // TODO
//					preProcessValueMapDAO.deletePreProcessValueMapByPPUid(session, preProcessUnit.getPreProcessId());
                    String resultProcessValue = processValueDAO.update(session, lstProcessValue);
                } else {
                    preProcessValueMapDAO.deletePreProcessValueMapByPPUid(session, preProcessUnit.getPreProcessId());
                    String resultProcessValue = processValueDAO.save(session, lstProcessValue);
                    if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultProcessValue)) {
                        throw new Exception();
                    }
                    List<PreProcessValueMap> lstPreProcessValueMaps = new ArrayList<>();
                    for (ProcessValue processValue : lstProcessValue) {
                        PreProcessValueMap preProcessValueMap = new PreProcessValueMap();
                        preProcessValueMap.setPreProcessId(preProcessUnit.getPreProcessId());
                        preProcessValueMap.setProcessValueId(processValue.getProcessValueId());
                        lstPreProcessValueMaps.add(preProcessValueMap);
                    }
                    String resultProcessValueMap = preProcessValueMapDAO.save(session, lstPreProcessValueMaps);
                    if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultProcessValueMap)) {
                        throw new Exception();
                    }
                }

            }

            tx.commit();

        } catch (Exception ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return Responses.ERROR.getName();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return Responses.SUCCESS.getName();
    }

    @Override
    public boolean onDeletePreprocessUnit(Long preProcessId) {
        Session session = null;
        Transaction tx = null;

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            deleteById(PreProcessUnit.class, preProcessId, session);
            preProcessValueMapDAO.deletePreProcessValueMapByPPUid(session, preProcessId);
            preProcessParamMapDAO.deletePreProcessParamMapByPPUid(session, preProcessId);
            tx.commit();

        } catch (Exception ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return false;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return true;
    }

    @Override
    public boolean checkDeletePreprocessUnit(Long preProcessId) {
        String sql = "select count(*) from condition_table_column_map \n"
                + "join column_ct on condition_table_column_map.COLUMN_ID = column_ct.COLUMN_ID\n"
                + "where column_ct.PRE_PROCESS_ID = :preProcessId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.setParameter("preProcessId", preProcessId);
        BigInteger rs = (BigInteger) query.uniqueResult();
        return rs.intValue() == 0;
    }

    @Override
    public List<PreProcessUnit> getLstPPUByConditionId(Long conditionId) {
        String sql = "SELECT "
                + "a.PRE_PROCESS_ID preProcessId , a.PRE_PROCESS_NAME preProcessName , a.PRE_PROCESS_TYPE preProcessType, "
                + "a.DEFAULT_VALUE defaultValue , a.INPUT_FIELDS inputFields , a.SPECIAL_FIELDS specialFields , "
                + "a.DESCRIPTION description , a.CATEGORY_ID categoryId , a.TYPE type "
                + "FROM "
                + "pre_process_unit a "
                + "INNER JOIN column_ct b "
                + "INNER JOIN condition_table_column_map c "
                + "INNER JOIN condition_table d  "
                + "WHERE "
                + "a.PRE_PROCESS_ID = b.PRE_PROCESS_ID  "
                + "AND b.COLUMN_ID = c.COLUMN_ID  "
                + "AND c.CONDITION_TABLE_ID = d.CONDITION_TABLE_ID  "
                + "AND d.CONDITION_TABLE_ID =:conditionId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("preProcessId", StandardBasicTypes.LONG);
        query.addScalar("preProcessName", StandardBasicTypes.STRING);
        query.addScalar("preProcessType", StandardBasicTypes.INTEGER);
        query.addScalar("defaultValue", StandardBasicTypes.LONG);
        query.addScalar("inputFields", StandardBasicTypes.STRING);
        query.addScalar("specialFields", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("categoryId", StandardBasicTypes.LONG);
        query.addScalar("type", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(PreProcessUnit.class));
        query.setParameter("conditionId", conditionId);
        return query.list();
    }
}
