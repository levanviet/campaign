package vn.viettel.campaign.dao;

import java.util.List;
import org.hibernate.Session;
import vn.viettel.campaign.entities.PreProcessParamMap;
import vn.viettel.campaign.entities.PreProcessValueMap;

/**
 *
 */
public interface PreProcessValueMapDAOInterface extends BaseDAOInteface<PreProcessValueMap> {

    public void deletePreProcessValueMapByPPUid(Session session, Long ppuid);
    
    public List<PreProcessValueMap> getListPreProcessValueMapByPreProcessId(Long preProcessId);

}
