package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.BlackList;
import vn.viettel.campaign.entities.UssdScenario;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface UssdScenarioDAOInterface extends BaseDAOInteface<UssdScenario>{
    public long getNextSequense(String tableName);
    public long checkDelete(long id);

    public UssdScenario getByName(long id, String name);
}
