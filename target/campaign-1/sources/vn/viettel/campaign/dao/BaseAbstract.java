/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ConKC
 * @param <T>
 */
public abstract class BaseAbstract<T> {

    @Autowired
    SessionFactory sessionFactory;

    public T findOne(Class<T> clazz, long id) {
        return (T) getCurrentSession().get(clazz, id);
    }

    public List findAll(Class<T> clazz) {
        return getCurrentSession().createQuery(("from " + clazz.getName())).list();
    }

    public T create(T entity) {
        getCurrentSession().saveOrUpdate(entity);
        return entity;
    }

    public T update(T entity) {
        return (T) getCurrentSession().merge(entity);
    }

    public void delete(T entity) {
        getCurrentSession().delete(entity);
    }

    public void deleteById(Class<T> clazz, long entityId) {
        T entity = findOne(clazz, entityId);
        delete(entity);
    }

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    protected void addParameters(Query query, List<Object> params) {
        if (params != null) {
            int i = 0;
            for (Object p : params) {
                if (p instanceof ArrayList) {
                    query.setParameterList("list", (ArrayList) p);
                } else {
                    query.setParameter(i, p);
                    i++;
                }
            }
        }
    }

    protected void addScalar(SQLQuery query, Map<String, Type> fieldMap) {
        if (fieldMap == null) {
            return;
        }
        fieldMap.entrySet().stream().forEach((field) -> {
            query.addScalar(field.getKey(), field.getValue());
        });
    }
}
