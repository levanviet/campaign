/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.Objects;
import vn.viettel.campaign.entities.Roles;
import vn.viettel.campaign.entities.Users;

public interface LoginDao {

    public Users loadUserByUsername(String username);

    public List<Roles> getListRole(Long userId);

    public List<Objects> getLstObjects(Long userId);
}
