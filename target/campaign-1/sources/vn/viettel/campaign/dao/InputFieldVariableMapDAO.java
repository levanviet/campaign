/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.InputFieldVariableMap;
import vn.viettel.campaign.entities.KpiProcessor;

/**
 *
 * @author SON
 */
public interface InputFieldVariableMapDAO {
    List<InputFieldVariableMap> getLstInputFieldVariableMapByTypeAndId(int type, Long id);
    void onSaveOrUpdateInputFieldVariableMap(InputFieldVariableMap inputFieldVariableMap);
    void onDeleteInputFieldVariableMap(int type, Long id);
    InputFieldVariableMap findOneById(Long id);
    InputFieldVariableMap getNextSequence();
}
