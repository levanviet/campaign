package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.*;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface OcsBehaviourDAOInterface extends BaseDAOInteface<OcsBehaviour> {
	OcsBehaviour getNextSequence();

	String onSaveOCSBehaviour(OcsBehaviour ocsBehaviour, List<OcsBehaviourFields> lstField,
							  List<OcsBehaviourExtendFields> lstExtendField,List<PostNotifyTrigger> lstNotifyTrigger);
	String onUpdateOCSBehaviour(OcsBehaviour ocsBehaviour, List<OcsBehaviourFields> lstField,
											List<OcsBehaviourExtendFields> lstExtendFields,List<PostNotifyTrigger> lstNotifyTrigger);
	boolean onDeleteOcsBehaviour(Long ocsBehaviourId);
	List<OcsBehaviourFields> getLstBehaviourField(Long behaviourId,Long templateId);
	List<OcsBehaviourTemplateFields> getLstBehaviourTemplateField(Long templateId);
	List<OcsBehaviourExtendFields> getLstBehaviourExtendField(Long behaviourId,Long templateId);
	List<OcsBehaviourTemplateExtendFields> getLstBehaviourTemplateExtendField(Long templateId);
	boolean checkExistOCSBehaviourName(String behaviourName, Long behaviourId);

	List<OcsBehaviour> getLstBehaviourByNotifyId(Long notifyId);
	boolean checkOcsUseInPromotionBlock(Long ocsId);
	boolean checkOcsUseInUSSDScenario(Long ocsId);
}
