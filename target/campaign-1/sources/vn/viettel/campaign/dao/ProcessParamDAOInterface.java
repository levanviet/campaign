package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.ProcessParam;
import vn.viettel.campaign.entities.ProcessValue;

import java.util.List;

/**
 *
 */
public interface ProcessParamDAOInterface extends BaseDAOInteface<ProcessParam> {
    List<ProcessParam> findProcessParamByPreProcessIds(Long ppuId);
    
    List<ProcessParam> findProcessParamByPreProcessId(Long pre_process_id);
}
