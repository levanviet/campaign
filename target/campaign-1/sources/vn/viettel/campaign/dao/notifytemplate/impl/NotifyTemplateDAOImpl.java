package vn.viettel.campaign.dao.notifytemplate.impl;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.BaseAbstract;
import vn.viettel.campaign.dao.notifytemplate.NotifyTemplateDAO;
import vn.viettel.campaign.entities.NotifyTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository("notifyTemplateDAO")
@Transactional(rollbackFor = Exception.class)
public class NotifyTemplateDAOImpl extends BaseAbstract<NotifyTemplate> implements NotifyTemplateDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger log = Logger.getLogger(NotifyTemplateDAOImpl.class.getName());

    @Override
    public List<NotifyTemplate> getTemplateByCategory(List<Long> categoryIds) {
        try {
            final String sql = "from NotifyTemplate where categoryId in (:categoryIds)";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameterList("categoryIds", categoryIds);
            query.setCacheable(true);
            List<NotifyTemplate> templates = query.list();
            return templates;
        } catch (Exception ex) {
            log.error("Exception " + ex);
            System.out.println("Exception " + ex);
        }
        return Collections.EMPTY_LIST;
    }

    @Override
    public NotifyTemplate getOneById(Long id) {
        try {
            return this.findOne(NotifyTemplate.class, id);
        } catch (Exception e) {
            log.error("Exception " + e.getMessage());
            System.out.println("Exception " + e.getMessage());
        }
        return null;
    }

    @Override
    public void removeById(Long id) {
        this.deleteById(NotifyTemplate.class, id);
    }

    @Override
    public NotifyTemplate save(NotifyTemplate template) {
        try {
            return this.create(template);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public NotifyTemplate findOnByName(String name, Long id) {
        try {
            String sql = "from NotifyTemplate where name = :name ";
            if (Objects.nonNull(id)) {
                sql += "and id not in :id ";
            }
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameter("name", name);
            if (Objects.nonNull(id)) {
                query.setParameter("id", id);
            }
            query.setCacheable(true);
            List<NotifyTemplate> templates = query.list();
            if (templates == null || templates.size() < 1) {
                return null;
            }
            return templates.get(0);
        } catch (Exception ex) {
            log.error("Exception " + ex);
            System.out.println("Exception " + ex);
        }
        return null;
    }
}
