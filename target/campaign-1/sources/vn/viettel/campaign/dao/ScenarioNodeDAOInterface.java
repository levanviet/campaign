package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.BlackList;
import vn.viettel.campaign.entities.ScenarioNode;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface ScenarioNodeDAOInterface extends BaseDAOInteface<ScenarioNode>{
    public List<ScenarioNode> findByScenarioId(long scenarioId);
    public List<ScenarioNode> findByParentId(long parentId);
    public void deleteByScenarioId(long scenarioId);
}
