package vn.viettel.campaign.dao;


import vn.viettel.campaign.entities.Segment;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface SegmentDAOInterface extends BaseDAOInteface<Segment>{
	List<Segment> getLstSegment(long campaignId);
}
