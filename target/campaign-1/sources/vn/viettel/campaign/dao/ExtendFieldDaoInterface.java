package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.ExtendField;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface ExtendFieldDaoInterface extends BaseDAOInteface<ExtendField>{
	public ExtendField getNextSequense();
	public boolean checkExisExtendFieldName(String name);
}
