package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.dto.RequestCountDTO;
import vn.viettel.campaign.dto.RequestDTO;
import vn.viettel.campaign.entities.Request;

/**
 *
 * @author ConKC
 */
public interface RequestDao extends BaseDAOInteface<Request> {

    List<Request> getAllRequest();

    RequestCountDTO countRequestUpdate(Long id);

    List<RequestDTO> getAllRequestScalar();

    void deleteRequestByTransaction(String transactionId);

    RequestDTO getRequestById(final Long id);
}
