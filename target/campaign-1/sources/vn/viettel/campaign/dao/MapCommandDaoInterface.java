package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.MapCommand;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface MapCommandDaoInterface extends BaseDAOInteface<MapCommand>{
}
