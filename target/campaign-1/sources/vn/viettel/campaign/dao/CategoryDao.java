/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.Category;

/**
 *
 * @author ConKC
 */
public interface CategoryDao {

    void save(Category category);

    List<Category> getCategory();

    List<Category> getCategoryByType(final Integer categoryType, final boolean parentId);

    List<Category> getCategoryByType(final Integer categoryType);

    Category getCategoryById(final Long categoryId);

    Boolean deleteCategory(final Category category);

    <T> List<T> getObjectByCategory(final List<Long> categoryIds, final String objectName);

    boolean checkDuplicate(final String categoryName, final Long categoryId);

    boolean checkDuplicate(final String categoryName, final Long categoryId, Integer categoryType);

    boolean checkDeleteCategory(final Long categoryId);

    boolean checkDeleteCategory(final Long categoryId, final String className);
    
    List<Category> findCategoryByIds(final List<Long> categoryIds);

    List<Category> findCategoryByTypes(final List<Integer> types);
}
