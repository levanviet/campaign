package vn.viettel.campaign.dao;

import org.hibernate.Session;
import vn.viettel.campaign.entities.OcsBehaviourFields;
import vn.viettel.campaign.entities.OcsBehaviourTemplateFields;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface OCSBehaviourFieldDaoInterface extends BaseDAOInteface<OcsBehaviourFields>{
	public List<OcsBehaviourFields> getLstFieldOfOCSBehaviour(long ocsBehaviourId,long templateId) ;
	public void deleteOCSBehaviourFieldByTemplateId(Session session, Long ocsBehaviourId);
}
