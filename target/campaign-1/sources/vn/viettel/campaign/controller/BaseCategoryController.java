package vn.viettel.campaign.controller;

//import com.sun.org.apache.xml.internal.resolver.helpers.PublicId;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dto.FilterTable;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.*;
import vn.viettel.campaign.service.impl.TreeServiceImpl;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.Objects;
import javax.el.ELContext;
import javax.faces.context.FacesContext;

import org.primefaces.PrimeFaces;
import vn.viettel.campaign.validate.CheckOnEdit;

/**
 * @author truongbx
 * @date 9/15/2019
 */
@Getter
@Setter
public abstract class BaseCategoryController<T extends BaseCategory> extends BaseController {

    public boolean editMode;
    public boolean display;
    public boolean updateCategory;
    public boolean action = false;
    public TreeNode rootNode;
    public TreeNode selectedNode;
    public List<Category> categories;
    public T currentValue;
    public List<BaseCategory> lstData;
    public Class<T> currentClass;
    public Integer categoryType;
    public Category category;
    public Long maxValue = 9223372036854775807L;
    public Long minValue = -9223372036854775808L;
    @Autowired
    public CategoryService categoryService;

    @Autowired
    public UtilsService utilsService;
    @Autowired
    public TreeServiceImpl treeService;
    @Autowired
    PreprocessUnitInterface preprocessUnitService;
    @Autowired
    RuleServiceInterface ruleServiceImpl;
    @Autowired
    private UssdScenarioService ussdScenarioService;

    @PostConstruct
    public void initBaseCategory() {
        this.display = false;
        this.categories = new ArrayList<>();
        this.category = new Category();
        this.rootNode = new DefaultTreeNode(null, null);
//		action = false;
        init();
        initCategories();
//		onAddNewObject();
    }

    abstract void init();

    public void initCategories() {
        this.categories = categoryService.getCategoryByType(categoryType);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.lstData = categoryService.getDataFromCatagoryId(longs, currentClass.getSimpleName());
        rootNode = treeService.createTreeCategoryAndChild(categories, lstData);
        rootNode.setExpanded(true);
        addExpandedNode(rootNode.getChildren().get(0));
    }

    public void prepareCreateCategory() {
        Long id = ussdScenarioService.getNextSequense(Constants.TableName.CATEGORY);
        this.updateCategory = false;
        this.category = new Category();
        this.category.setCategoryType(categoryType);
        category.setCategoryId(id);
        Category parentCat = (Category) selectedNode.getData();

        this.category.setParentId(parentCat.getCategoryId());
    }

    public void prepareEditCategory() {
        this.updateCategory = true;
        if (Objects.nonNull(selectedNode)) {
            Category cat = (Category) selectedNode.getData();
            this.category = categoryService.getCategoryById(cat.getCategoryId());
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void doDeleteCategory() {
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();
            if (DataUtil.isNullObject(category.getParentId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.parent.category"));
                return;
            }
            if (categoryService.checkDeleteCategory(this.category.getCategoryId(), currentClass.getSimpleName())) {
                categoryService.deleteCategory(category);
                selectedNode.getChildren().clear();
                TreeNode parNode = selectedNode.getParent();
                selectedNode.getParent().getChildren().remove(selectedNode);
                if (parNode.getChildren().isEmpty()) {
                    removeExpandedNode(parNode);
                }
                selectedNode.setParent(null);
                selectedNode = null;
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
                categories.remove(category);
                updateCategoryInform();
            } else {
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void onAddNewObject() {
        Category category = (Category) selectedNode.getData();
        this.editMode = false;
        this.display = true;
        this.action = true;
        initCurrentValue();
        currentValue.setParentId(category.getCategoryId());
        doAdd();
    }

    public void onEditObject() {
        this.editMode = true;
        this.display = true;
//		this.action = true;
        action = true;
        this.currentValue = (T) selectedNode.getData();
        rollbackData();
        doEdit();
    }

    public void onEditObject(PreProcessUnit ppu) {
        this.editMode = true;
        this.display = true;
//		this.action = true;
        action = true;
        this.currentValue = (T) ppu;
        rollbackData();
        doEdit();
    }

    public boolean onDeleteObject() {
        //
        this.currentValue = (T) selectedNode.getData();
        boolean isValid = validateBeforeDelete();
        if (!isValid) {
            return false;
        }
        if (currentValue instanceof PreProcessUnit) {
            PreProcessUnit unit = (PreProcessUnit) currentValue;
            if (!preprocessUnitService.checkDeletePreprocessUnit(unit.getPreProcessId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("ppu.used.in.condition"));
                return false;
            }
        }
        //
        this.display = false;
        boolean result = doDelete();
        TreeNode parentNode = selectedNode.getParent();
        if (result) {
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
            if (parentNode.getChildren().size() == 1) {
                removeExpandedNode(parentNode);
            }
            parentNode.getChildren().remove(selectedNode);
            lstData.remove(currentValue);
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.fail"));
        }
        return result;
    }

    public void onReset() {
        if (this.editMode) {
            onEditObject();
        } else {
            //add new
            onAddNewObject();
        }
    }

    public boolean onSave() {
        DataUtil.trimObject(this.currentValue);
        boolean result;
        if (!onValidateObject()) {
//            if (this.editMode) {
//                rollbackData();
//            }
            return false;
        }
        if (this.editMode) {
            result = onUpdateObject();
            if (result) {
                // INTO HA
                PreProcessUnit ppu;
                String editOb = "";
                T t = (T) selectedNode.getData();
                if ("vn.viettel.campaign.entities.Rule".equals(t.getClass().getName())) {
                    editOb = "rule";
                }
                if ("vn.viettel.campaign.entities.PreProcessUnit".equals(t.getClass().getName())) {
                    ppu = (PreProcessUnit) selectedNode.getData();
                    if (Constants.PRE_PROCESS_UNIT_STRING == ppu.getPreProcessType()) {
                        editOb = "ppu_string";
                    }
                    if (Constants.PRE_PROCESS_UNIT_NUMBER == ppu.getPreProcessType()) {
                        editOb = "ppu_number";
                    }
                    if (Constants.PROCESS_UNIT_EXIST_ELEMENT == ppu.getPreProcessType()) {
                        editOb = "ppu_exist_element";
                    }
                    if (Constants.PRE_PROCESS_UNIT_TIME == ppu.getPreProcessType()) {
                        editOb = "ppu_time";
                    }
                    if (Constants.PRE_PROCESS_UNIT_DATE == ppu.getPreProcessType()) {
                        editOb = "ppu_date";
                    }
                    if (Constants.PRE_PROCESS_UNIT_NUMBER_RANGE == ppu.getPreProcessType()) {
                        editOb = "ppu_number_range";
                    }
                    if (Constants.PRE_PROCESS_UNIT_COMPARE_NUMBER == ppu.getPreProcessType()) {
                        editOb = "ppu_compare_nember";
                    }
                    if (Constants.PROCESS_UNIT_ZONE == ppu.getPreProcessType()) {
                        editOb = "ppu_zone";
                    }
                    if (Constants.PRE_PROCESS_UNIT_SAME_ELEMENT == ppu.getPreProcessType()) {
                        editOb = "ppu_same_element";
                    }
                }
                if ("condition".equals(CheckOnEdit.onEdit)) {
                    ELContext elContext = FacesContext.getCurrentInstance().getELContext();
                    ConditionTableController conditionTableController = (ConditionTableController) elContext.getELResolver().getValue(elContext, null, "conditionTableController");
                    conditionTableController.getParentNode().getChildren().remove(conditionTableController.getSelectedNode());
                    if ("rule".equals(editOb)) {
                        conditionTableController.setSelectedNode(new DefaultTreeNode(editOb, this.currentValue, conditionTableController.getParentNode()));
                    } else {
                        conditionTableController.setSelectedNode(new DefaultTreeNode("ppu", this.currentValue, conditionTableController.getParentNode()));
                    }
                    conditionTableController.getSelectedNode().setSelected(true);
                    conditionTableController.setEditObject(editOb);
                    showMessage(result);
                    return result;
                }
                if ("result".equals(CheckOnEdit.onEdit)) {
                    ELContext elContext = FacesContext.getCurrentInstance().getELContext();
                    ResultTableController resultTableController = (ResultTableController) elContext.getELResolver().getValue(elContext, null, "resultTableController");
                    resultTableController.getParentNode().getChildren().remove(resultTableController.getSelectedNode());
                    if ("rule".equals(editOb)) {
                        resultTableController.setSelectedNode(new DefaultTreeNode(editOb, this.currentValue, resultTableController.getParentNode()));
                    } else {
                        resultTableController.setSelectedNode(new DefaultTreeNode("ppu", this.currentValue, resultTableController.getParentNode()));
                    }
                    resultTableController.getSelectedNode().setSelected(true);
                    resultTableController.setEditObject(editOb);
                    showMessage(result);
                    return result;
                }
                if ("rule_online".equals(CheckOnEdit.onEdit) && !"rule".equals(editOb)) {
                    ELContext elContext = FacesContext.getCurrentInstance().getELContext();
                    RuleOnlineController ruleOnlineController = (RuleOnlineController) elContext.getELResolver().getValue(elContext, null, "ruleOnlineController");
                    ruleOnlineController.getParentNode().getChildren().remove(ruleOnlineController.getSelectedNode());
                    
                    if ("rule".equals(editOb)) {
                        ruleOnlineController.setSelectedNode(new DefaultTreeNode(editOb, this.currentValue, ruleOnlineController.getParentNode()));
                    } else {
                        ruleOnlineController.setSelectedNode(new DefaultTreeNode("ppu", this.currentValue, ruleOnlineController.getParentNode()));
                    }
                    
                    ruleOnlineController.getSelectedNode().setSelected(true);
                    ruleOnlineController.setEditObject(editOb);
                    showMessage(result);
                    return result;
                }
                if ("rule_offline".equals(CheckOnEdit.onEdit) && !"rule".equals(editOb)) {
                    ELContext elContext = FacesContext.getCurrentInstance().getELContext();
                    RuleOfflineController ruleOfflineController = (RuleOfflineController) elContext.getELResolver().getValue(elContext, null, "ruleOfflineController");
                    ruleOfflineController.getParentNode().getChildren().remove(ruleOfflineController.getSelectedNode());
                    
                    if ("rule".equals(editOb)) {
                        ruleOfflineController.setSelectedNode(new DefaultTreeNode(editOb, this.currentValue, ruleOfflineController.getParentNode()));
                    } else {
                        ruleOfflineController.setSelectedNode(new DefaultTreeNode("ppu", this.currentValue, ruleOfflineController.getParentNode()));
                    }
                    
                    ruleOfflineController.getSelectedNode().setSelected(true);
                    ruleOfflineController.setEditObject(editOb);
                    showMessage(result);
                    return result;
                }

                if ("cam_online".equals(CheckOnEdit.onEdit)) {
                    ELContext elContext = FacesContext.getCurrentInstance().getELContext();
                    CampaignOnlineController campaignOnlineController = (CampaignOnlineController) elContext.getELResolver().getValue(elContext, null, "campaignOnlineController");
                    campaignOnlineController.getParentNode().getChildren().remove(campaignOnlineController.getSelectedNode());
                    
                    if ("rule".equals(editOb)) {
                        campaignOnlineController.setSelectedNode(new DefaultTreeNode(editOb, this.currentValue, campaignOnlineController.getParentNode()));
                    } else {
                        campaignOnlineController.setSelectedNode(new DefaultTreeNode("ppu", this.currentValue, campaignOnlineController.getParentNode()));
                    }
                    
                    campaignOnlineController.getSelectedNode().setSelected(true);
                    campaignOnlineController.setEditObject(editOb);
                    if ("rule".equals(editOb)) {
                        campaignOnlineController.prepareEdit();
                    }
                    showMessage(result);
                    return result;
                }

                if ("cam_offline".equals(CheckOnEdit.onEdit)) {
                    ELContext elContext = FacesContext.getCurrentInstance().getELContext();
                    CampaignOfflineController campaignOfflineController = (CampaignOfflineController) elContext.getELResolver().getValue(elContext, null, "campaignOfflineController");
                    campaignOfflineController.getParentNode().getChildren().remove(campaignOfflineController.getSelectedNode());
                    
                    if ("rule".equals(editOb)) {
                        campaignOfflineController.setSelectedNode(new DefaultTreeNode(editOb, this.currentValue, campaignOfflineController.getParentNode()));
                    } else {
                        campaignOfflineController.setSelectedNode(new DefaultTreeNode("ppu", this.currentValue, campaignOfflineController.getParentNode()));
                    }
                    
                    campaignOfflineController.getSelectedNode().setSelected(true);
                    campaignOfflineController.setEditObject(editOb);
                    if ("rule".equals(editOb)) {
                        campaignOfflineController.prepareEdit();
                    }
                    showMessage(result);
                    return result;
                }
                // OUT OF HA

                TreeNode parentNote = selectedNode.getParent();
                parentNote.getChildren().remove(selectedNode);
                if (parentNote.getChildren().size() == 0) {
                    removeExpandedNode(parentNote);
                }
                showMessage(result);
            }

        } else {
            result = onSaveObject();
            showMessage(result);
            lstData.add(this.currentValue);
        }
        if (result) {
            this.editMode = true;
            action = false;
            TreeNode treeNode = addNoteToCategoryTree(rootNode, this.currentValue);
            if (treeNode !=null){
                treeNode.setSelected(true);
            }

        }
        this.action = false;
        return result;
    }

    public boolean validateBeforeDelete() {
        return true;
    }

    public boolean validateCategory() {
        if (DataUtil.isStringNullOrEmpty(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Category name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Category name");
            return false;
        }
        boolean rs = true;
        if (!categoryService.checkDuplicate(category.getName(), category.getCategoryId(), category.getCategoryType())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Category name");
            rs = false;
        }
        return rs;
    }

    public void rollbackData() {
    }

    public void doSaveOrUpdateCategory() {
        categoryService.save(category);
        refreshTree();
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    private void refreshTree() {
        if (updateCategory) {
            int i = categories.indexOf(selectedNode.getData());
            categories.remove(selectedNode.getData());
            categories.add(i, this.category);
        } else {
            categories.add(this.category);
        }
        Collections.sort(categories,new SortCategory());
        rootNode = treeService.createTreeCategoryAndChild(categories, lstData);
        processRefreshCategory(rootNode, selectedNode, category, updateCategory);
        updateCategoryInform();
    }

    public void doAdd() {
    }

    public void initCurrentValue() {
        try {
            currentValue = currentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doEdit() {
    }

    public boolean onValidateObject() {
        return true;
    }

    public boolean doDelete() {
        return true;
    }

    public boolean onUpdateObject() {
        return true;
    }

    public boolean onSaveObject() {
        return true;
    }

    public void showMessage(boolean result) {
        if (result) {
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.fail"));
        }
    }

    public void actionSuccess() {
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
    }

    public void duplidateMessage(String fieldName) {
        errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), fieldName);
    }

    public void notCompleteMessage(String fieldName) {
        errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notComplete"), fieldName);
    }

    public boolean validInputField(String value, String fieldName, boolean isRequire, boolean isCheckMaxlength, boolean isCheckPercent) {
        if (isRequire && !validRequireField(value, fieldName)) {
            return false;
        }
        if (isCheckMaxlength && !DataUtil.checkMaxlength(value)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), fieldName);
            return false;
        }
        if (isCheckPercent && !DataUtil.checkNotContainPercentage(value)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), fieldName);
            return false;
        }
        return true;
    }

    public boolean validRequireField(String value, String fieldName) {
        if (DataUtil.isStringNullOrEmpty(value)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), fieldName);
            return false;
        }
        return true;
    }

    public boolean validRequireField(Long value, String fieldName) {
        if (value == null || value == 0) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), fieldName);
            return false;
        }
        return true;
    }

    public boolean validRequireField(Integer value, String fieldName) {
        if (value == null || value == 0) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), fieldName);
            return false;
        }
        return true;
    }

    public boolean validRequireFieldList(List<?> value, String fieldName) {
        if (value == null || value.isEmpty()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), fieldName);
            return false;
        }
        return true;
    }

    public void exitsMessage(String fieldName) {
        errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), fieldName);
    }

    public TreeNode addNoteToCategoryTree(TreeNode rootNode, BaseCategory baseCategory) {
        TreeNode note = findParentNoteInTree(rootNode, baseCategory);
        if (note != null) {
            TreeNode newNote = new DefaultTreeNode(baseCategory.getTreeType(), baseCategory, note);
            expandCurrentNode(newNote);
            this.selectedNode = newNote;
            return newNote;
        }
        return null;
    }

    public TreeNode findParentNoteInTree(TreeNode root, BaseCategory baseCategory) {
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (baseCategory.getParentType().isInstance(note.getData())) {
                Category data = (Category) note.getData();
                if (data.getCategoryId() == baseCategory.getParentId()) {
                    return note;
                }
            }
            currentNote = findParentNoteInTree(note, baseCategory);
            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }

    public TreeNode findParameterNoteInTree(TreeNode root, Long parameterId) {
        if (parameterId == null){
            return null;
        }
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (note.getData() instanceof Parameter) {
                Parameter data = (Parameter) note.getData();
                if (data.getParameterId() == parameterId) {
                    return note;
                }
            }
            currentNote = findParameterNoteInTree(note, parameterId);
            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }
    public TreeServiceImpl getTreeService() {
        return treeService;
    }

    public void prepareEdit() {
        if ("category".equals(selectedNode.getType())) {
            prepareEditCategory();
            PrimeFaces.current().executeScript("PF('carDialog').show();");
        }
        if ("preprocess_string".equals(selectedNode.getType())) {
            onEditObject();
        }
    }

    public void onViewObject(OcsBehaviour ocsBehaviour) {
        this.editMode = true;
        this.display = true;
//		this.action = true;
        action = false;
        this.currentValue = (T) ocsBehaviour;
        rollbackData();
        doEdit();
    }
    class SortCategory implements Comparator<Category> {
        public int compare(Category a, Category b)
        {
            return a.getName().compareTo( b.getName());
        }
    }
    
    public List<ProcessValue> cloneListProcessValue(List<ProcessValue> listOld){
	List<ProcessValue> newLst = new ArrayList<>();
	for (ProcessValue processValue : listOld) {
		ProcessValue pv = new ProcessValue();
		
		pv.setDescription(processValue.getDescription());
		pv.setPreprocessId(processValue.getPreprocessId());
		pv.setProcessValueId(processValue.getProcessValueId());
		pv.setValueColor(processValue.getValueColor());
		pv.setValueIndex(processValue.getValueIndex());
		pv.setValueName(processValue.getValueName());
		pv.setValueId(processValue.getValueId());
		
		newLst.add(pv);

	}
	return newLst;
}
    public boolean validateInputFunction(List<FilterTable> lstFunctionTable){
        for (FilterTable filterTable : lstFunctionTable) {
            for (FunctionParam functionParam : filterTable.getLstFunctionParams()) {
                if(functionParam.getValueType().intValue() == 1) {
                    String valuetemp = functionParam.getFieldValueString() == null ? "":
                            functionParam.getFieldValueString().trim();
                    return DataUtil.validateInputFilter(valuetemp);
                }
            }
        }
        return true;
    }
    public boolean validateInputCondition(List<FilterTable> lstConditionTable,Map<Long, InputObject> mapInputObject) {
        for (FilterTable filterTable : lstConditionTable) {
            InputObject input = mapInputObject.get(filterTable.getFieldId());
            if (input == null) {
                return true;
            }
            if (input.getObjectDataType().intValue() == 2) {
                String value = filterTable.getFieldValueString() == null ? "":
                        filterTable.getFieldValueString().trim();
                return DataUtil.validateInputFilter(value);
            }
        }
        return true;
    }

    public Long getMaxValueId(List<ProcessValue> lstProcessValueTemp ){

        Long i = 0L ;
        for (ProcessValue processValue :lstProcessValueTemp){
            if (processValue.getValueId() > i){
                i = processValue.getValueId();
            }
        }
        return i + 1 ;
    }
}
