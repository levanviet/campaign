/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@ManagedBean
public class RedirectController implements Serializable {

    @PostConstruct
    public void init() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = (HttpSession) attr.getRequest().getSession(true);
        if (session.getAttribute("lstMenu") != null) {
            if (session.getAttribute("doLogin") != null) {
                session.removeAttribute("doLogin");
            }
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("/campaign/dashboard/home.jsf");
            } catch (IOException e) {
            }
        } else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("/campaign/login.jsf");
            } catch (IOException e) {

            }
        }

    }

}
