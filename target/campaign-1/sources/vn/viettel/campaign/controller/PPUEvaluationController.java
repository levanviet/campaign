/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.common.DateUtils;
import static vn.viettel.campaign.common.DateUtils.DATETIME_PPU_SRING;
import static vn.viettel.campaign.common.DateUtils.DATETIME_PPU_ZONE;
import vn.viettel.campaign.constants.Constants;
import static vn.viettel.campaign.constants.Constants.CatagoryType.CATEORY_KPI_PROCESSOR_TYPE;
import static vn.viettel.campaign.constants.Constants.CatagoryType.CATEORY_PPU_EVALUATION_TYPE;
import static vn.viettel.campaign.controller.BaseController.errorMsg;
import static vn.viettel.campaign.controller.BaseController.errorMsgParams;
import static vn.viettel.campaign.controller.BaseController.successMsg;
import vn.viettel.campaign.dao.PreProcessParamMapDAO;
import vn.viettel.campaign.dao.PreProcessValueMapDAO;
import vn.viettel.campaign.dao.ProcessParamDAO;
import vn.viettel.campaign.dto.Path;
import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.dto.ConditionTable;
import vn.viettel.campaign.dto.FilterTable;
import vn.viettel.campaign.dto.FunctionTable;
import vn.viettel.campaign.dto.NodeInfo;
import vn.viettel.campaign.dto.PathTable;
import vn.viettel.campaign.entities.BaseCategory;
import vn.viettel.campaign.entities.EvaluationInputObject;
import vn.viettel.campaign.entities.Function;
import vn.viettel.campaign.entities.FunctionParam;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.InputFieldVariableMap;
import vn.viettel.campaign.entities.InputObject;
import vn.viettel.campaign.entities.Parameter;
import vn.viettel.campaign.entities.PpuEvaluation;
import vn.viettel.campaign.entities.PreProcessParamMap;
import vn.viettel.campaign.entities.PreProcessValueMap;
import vn.viettel.campaign.entities.ProcessParam;
import vn.viettel.campaign.entities.ProcessValue;
import vn.viettel.campaign.entities.Zone;
import vn.viettel.campaign.entities.ZoneMap;
import vn.viettel.campaign.service.CategoryService;
import vn.viettel.campaign.service.EvaluationInputObjectService;
import vn.viettel.campaign.service.InputFieldVariableMapService;
import vn.viettel.campaign.service.PpuEvaluationService;
import vn.viettel.campaign.service.PreprocessUnitInterface;
import vn.viettel.campaign.service.ProcessValueService;
import vn.viettel.campaign.service.TreeUtilsService;
import vn.viettel.campaign.service.UssdScenarioService;
import vn.viettel.campaign.service.UtilsService;
import vn.viettel.campaign.service.impl.TreeServiceImpl;

/**
 *
 * @author SON
 */
@ManagedBean(name = "ppuEvaluationController")
@ViewScoped
@Getter
@Setter
public class PPUEvaluationController extends BaseController implements Serializable {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private UssdScenarioService ussdScenarioService;

    @Autowired
    private TreeUtilsService treeUtilsService;

    @Autowired
    private InputFieldVariableMapService inputFieldVariableMapService;

    @Autowired
    private EvaluationInputObjectService evaluationInputObjectService;

    @Autowired
    private UtilsService utilsService;

    @Autowired
    private PpuEvaluationService ppuEvaluationService;

    @Autowired
    private ProcessValueService processValueService;

    @Autowired
    private PreProcessValueMapDAO preProcessValueMapDAO;

    @Autowired
    private PreProcessParamMapDAO preProcessParamMapDAO;

    @Autowired
    private ProcessParamDAO processParamDAO;

    @Autowired
    PreprocessUnitInterface preprocessUnitServiceImpl;

    public List<Path> lstPath = new ArrayList<>();
    public Path path;
    public ConditionTable condition;
    private List<Category> lstCategory = new ArrayList<>();
    private List<InputFieldVariableMap> lstInputFieldVariableMap = new ArrayList<>();
    private List<InputFieldVariableMap> lstInputFieldVariableMapToDelete = new ArrayList<>();
    private List<EvaluationInputObject> lstEvaluationInputObject = new ArrayList<>();
    private List<PpuEvaluation> lstPpuEvaluation = new ArrayList<>();
    private List<ProcessValue> lstProcessValue = new ArrayList<>();
    private List<ProcessValue> lstProcessValueToDelete = new ArrayList<>();
    private List<PreProcessValueMap> lstPreProcessValueMapToDelete = new ArrayList<>();
    private List<ProcessParam> lstProcessParam = new ArrayList<>();
    private List<ProcessParam> lstProcessParamToDelete = new ArrayList<>();
    private List<PreProcessParamMap> lstPreProcessParamMapToDelete = new ArrayList<>();
    private List<ConditionTable> lstCondition = new ArrayList<>();
    private List<FunctionTable> lstFunction = new ArrayList<>();
    private List<ZoneMap> lstZoneMap = new ArrayList<>();
    private List<Category> lstCategoryParameter = new ArrayList<>();
    private List<Parameter> lstParameter = new ArrayList<>();
    private Category category = new Category();
    private PpuEvaluation ppuEvaluation = new PpuEvaluation();
    private InputFieldVariableMap inputFieldVariableMap;
    private Map<Long, Parameter> mapParameter = new HashMap<>();
    private ProcessParam processParam;
    private boolean disableConditiontable = true;
    private List<FilterTable> lstConditionTable = new ArrayList<>();
    private List<FilterTable> lstFunctionTable = new ArrayList<>();
    private List<EvaluationInputObject> lstConditionData = new ArrayList<>();
    private List<Function> lstFunctionData = new ArrayList<>();
    private List<Function> lstFunctionDataTypeOne = new ArrayList<>();
    private Map<Long, Function> mapFunction = new HashMap<>();
    private Map<Long, PpuEvaluation> mapPpuEvaluation = new HashMap<>();
    private Map<Long, EvaluationInputObject> mapInputObject = new HashMap<>();
    private Map<String, EvaluationInputObject> mapInputNameObject = new HashMap<>();
    private PpuEvaluation currentValue = new PpuEvaluation();
    private ProcessParam currentProcessParam;
    Map<Long, Zone> mapZone = new HashMap<>();
    List<Zone> lstZone = new ArrayList<>();
    List<FunctionTable> functions = new ArrayList<>();
    Map<String, Function> mapFunctionsType2 = new HashMap<>();
    Map<String, Function> mapFunctionsType1 = new HashMap<>();
    @Autowired
    public TreeServiceImpl treeService;
    private TreeNode rootNode;
    private TreeNode selectedNode;
    private TreeNode backUpSelectNode;
    private TreeNode rootNodeInput;
    private TreeNode selectedNodeInput;
    private TreeNode parameterSelectedNode;
    private TreeNode parameterNode;
    private FunctionParam currentFunctionParam;
    private boolean display;
    private boolean action;
    private boolean changedInputField = false;
    private boolean editMode;
    private boolean editModeCriteriaValue;
    private boolean update;
    private boolean view;
    private boolean viewFilter;

    private TreeNode zoneRootNode;
    private TreeNode selectedZoneNode;

    @PostConstruct
    public void init() {
        initPathTree();
        initTreeNode();
        lstProcessParamToDelete = new ArrayList<>();
        lstEvaluationInputObject = evaluationInputObjectService.getLstEvaluationInputObject();
        lstFunctionData = preprocessUnitServiceImpl.getLstFunction();
        lstFunctionDataTypeOne = preprocessUnitServiceImpl.getLstFunctionTypeOne();
        lstZoneMap = preprocessUnitServiceImpl.getLstZoneMap();
        lstFunctionData.forEach((e) -> mapFunction.put(e.getFunctionId(), e));
        lstFunctionDataTypeOne.forEach((e) -> mapFunction.put(e.getFunctionId(), e));
        for (EvaluationInputObject inputObject : lstEvaluationInputObject) {
            mapInputObject.put(inputObject.getObjectId(), inputObject);
            mapInputNameObject.put(inputObject.getObjectName() + "_" + inputObject.getObjectParentId(), inputObject);
        }
        lstZoneMap = preprocessUnitServiceImpl.getLstZoneMap();
        initZoneTree();

    }

    public void initTreeParameter() {
        lstCategoryParameter = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_PARAMETER_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!lstCategoryParameter.isEmpty()) {
            lstCategoryParameter.forEach(item -> longs.add(item.getCategoryId()));
        }
        lstParameter = categoryService.getDataFromCatagoryId(longs, Parameter.class.getSimpleName());
        mapParameter = new HashMap<>();
        lstParameter.forEach((e) -> {
            mapParameter.put(e.getParameterId(), e);
        });
        initCategoryTree();
    }

    public TreeNode getRootParameterNote() {
        // root node off tree (parent id is null)
        Category rootObject = null;
        for (Category it : lstCategoryParameter) {
            if (it.getParentId() == null) {
                rootObject = it;
            }
        }       // root node in framework (not show in view)
        parameterNode = new DefaultTreeNode(null, null);
        TreeNode note = new DefaultTreeNode("category", rootObject, parameterNode);
        note.setSelectable(false);
        return note;
    }

    public void initCategoryTree() {
        TreeNode node = getRootParameterNote();
        buildCatagoryAndChildrentTree(node, lstCategoryParameter, lstParameter);
        parameterNode.getChildren().get(0).setExpanded(true);
    }

    public void buildCatagoryAndChildrentTree(TreeNode parentNodeCat, List<Category> listCat, List<Parameter> data) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (parentCat.getCategoryId().equals(cat.getParentId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
                buildCatagoryAndChildrentTree(childNodeCat, listCat, data);
            }
        }
        for (Parameter baseCategory : data) {
            if (baseCategory.getCategoryId().equals(parentCat.getCategoryId())) {
                new DefaultTreeNode("parameter", baseCategory, parentNodeCat);
            }
        }
    }

    public void initTreeNode() {
        this.lstCategory = categoryService.getCategoryByType(CATEORY_PPU_EVALUATION_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!lstCategory.isEmpty()) {
            lstCategory.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.lstPpuEvaluation = categoryService.getDataFromCatagoryId(longs, Constants.PPU_EVALUATION);
        for (PpuEvaluation obj : lstPpuEvaluation) {
            mapPpuEvaluation.put(obj.getPreProcessId(), obj);
        }
        //rootNode = treeUtilsService.createTreeCategoryAndComponentObject(lstCategory, lstPpuEvaluation);
        rootNode = genCategoryAndComponent(lstCategory, lstPpuEvaluation);
        rootNode.getChildren().get(0).setExpanded(true);
        addExpandedNode(rootNode.getChildren().get(0));
    }

    public void initZoneTree() {
        List<ZoneMap> lstZoneMap = preprocessUnitServiceImpl.getLstZoneMap();
        List<Zone> lstZone = preprocessUnitServiceImpl.getLstZone();
        mapZone = new HashMap<>();
        lstZone.forEach((e) -> {
            mapZone.put(e.getZoneId(), e);
        });
        zoneRootNode = treeService.createZoneZonMapTree(lstZoneMap, lstZone);
    }

    public void prepareChooseZone(FunctionParam functionParam) {
        initZoneTree();
        currentFunctionParam = functionParam;
        selectedZoneNode = null;
    }

    public boolean validateChooseParameter() {
        if (parameterSelectedNode == null || parameterSelectedNode.getType().equalsIgnoreCase("category")) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.parameter.choose"));
            return false;
        }
        Parameter parameter = (Parameter) parameterSelectedNode.getData();
        currentProcessParam.setValue(parameter.getParameterName());
        currentProcessParam.setParameterId(parameter.getParameterId());
        return true;
    }

    public void prepareToShowParameterDialog(ProcessParam processParam) {
        this.currentProcessParam = processParam;
        parameterSelectedNode = null;
    }

    public boolean validateChooseZone() {
        if (selectedZoneNode == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("not.valid.choose.zone"));
            return false;
        }
        if (selectedZoneNode.getData() instanceof Category) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("not.valid.choose.cat"));
            return false;
        }
        Zone zone = (Zone) selectedZoneNode.getData();
        currentFunctionParam.setFieldZoneId(zone.getZoneId());
        currentFunctionParam.setFieldZoneName(zone.getZoneName());
        if (inputFieldVariableMap.getIsInputMode() == 2) {
            buildDataFunctionForInputModeIsFunction(functions);
        }
        if (inputFieldVariableMap.getIsInputMode() == 1) {
            buildDataFunction(lstFunctionTable, 2);
        }
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        return true;
    }

    TreeNode selectedNodeClone = new DefaultTreeNode();

    public void prepareEditWhenClick() {
        if ("category".equals(selectedNode.getType())) {
            PrimeFaces.current().executeScript("PF('carDialog').show();");
            prepareEditCategory();

        }
        if ("object".equals(selectedNode.getType()) || "ppu".equals(selectedNode.getType())) {
            selectedNodeClone.setParent(selectedNode.getParent());
            selectedNodeClone.setType("object");

            this.backUpSelectNode = this.selectedNode;

            this.ppuEvaluation = new PpuEvaluation();
            this.editMode = true;
            this.display = true;
            this.action = false;
            this.ppuEvaluation = (PpuEvaluation) selectedNode.getData();
            lstProcessParam = new ArrayList<>();
            lstProcessParamToDelete = new ArrayList<>();
            lstProcessValueNew = new ArrayList<>();
            processValueToDelete = new ProcessValue();
            lstProcessValueToDelete = new ArrayList<>();
            lstInputFieldVariableMap = inputFieldVariableMapService.getLstInputFieldVariableMapByTypeAndId(2, ppuEvaluation.getPreProcessId());
            lstInputFieldVariableMapToDelete = inputFieldVariableMapService.getLstInputFieldVariableMapByTypeAndId(2, ppuEvaluation.getPreProcessId());
            lstProcessValue = ppuEvaluationService.getListProcessValue(ppuEvaluation.getPreProcessId());
            lstProcessParam = ppuEvaluationService.getListProcessParam(ppuEvaluation.getPreProcessId());
            for (ProcessParam obj : lstProcessParam) {
                obj.getParam();
            }
        }

    }

    public void prepareEdit() {
        if ("category".equals(selectedNode.getType())) {
            PrimeFaces.current().executeScript("PF('carDialog').show();");
            prepareEditCategory();

        }
        if ("object".equals(selectedNode.getType())) {
            this.ppuEvaluation = new PpuEvaluation();
            this.editMode = true;
            this.display = true;
            this.action = true;
            this.ppuEvaluation = (PpuEvaluation) selectedNode.getData();
            this.backUpSelectNode = this.selectedNode;

            lstProcessParam = new ArrayList<>();
            lstProcessParamToDelete = new ArrayList<>();
            lstProcessValueNew = new ArrayList<>();
            processValueToDelete = new ProcessValue();
            lstProcessValueToDelete = new ArrayList<>();
            lstInputFieldVariableMap = inputFieldVariableMapService.getLstInputFieldVariableMapByTypeAndId(2, ppuEvaluation.getPreProcessId());
            lstInputFieldVariableMapToDelete = inputFieldVariableMapService.getLstInputFieldVariableMapByTypeAndId(2, ppuEvaluation.getPreProcessId());
            lstProcessValue = ppuEvaluationService.getListProcessValue(ppuEvaluation.getPreProcessId());
            lstProcessParam = ppuEvaluationService.getListProcessParam(ppuEvaluation.getPreProcessId());
            for (ProcessParam obj : lstProcessParam) {
                obj.getParam();
            }
        }

    }

    public void prepareAddPPUEvaluation() {
        this.editMode = false;
        this.display = true;
        this.action = true;
        lstInputFieldVariableMap = new ArrayList<>();
        Category car = (Category) selectedNode.getData();

        this.ppuEvaluation = ppuEvaluationService.getNextSequense();
        this.ppuEvaluation.setSpecialFields("isUseParameter:false");
        this.ppuEvaluation.setCategoryId(car.getCategoryId());
        lstProcessParam = new ArrayList<>();
        lstProcessValue = new ArrayList<>();
        lstProcessParamToDelete = new ArrayList<>();
        lstProcessValueNew = new ArrayList<>();
        processValueToDelete = new ProcessValue();
        lstProcessValueToDelete = new ArrayList<>();
        processValueToDelete = new ProcessValue();

    }

    public boolean validateDeletePPU() {
        PpuEvaluation PpuEvaluationDelete = (PpuEvaluation) selectedNode.getData();
        if (ppuEvaluationService.checkDeletePPUEvaluation(PpuEvaluationDelete.getPreProcessId())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("ppu.used.in.assessmentRule"));
            return false;
        }
        return true;
    }

    public void prepareDeletePPUEvaluation() {
        this.ppuEvaluation = (PpuEvaluation) selectedNode.getData();
        ppuEvaluationService.onDeletePpuEvaluation(ppuEvaluation);
        for (int i = 0; i < lstProcessValue.size(); i++) {
            processValueService.onDeleteProcessValue(ppuEvaluation);
        }
        for (int i = 0; i < lstProcessParam.size(); i++) {
            processParamDAO.onDeleteProcessParam(ppuEvaluation);
        }
        for (InputFieldVariableMap obj : lstInputFieldVariableMap) {
            inputFieldVariableMapService.onDeleteInputFieldVariableMap(2, ppuEvaluation.getPreProcessId());
        }
        this.display = false;
        TreeNode parentNode = selectedNode.getParent();
        if (parentNode.getChildren().size() == 1) {
            removeExpandedNode(parentNode);
        }
        parentNode.getChildren().remove(selectedNode);
        lstPpuEvaluation.remove(this.ppuEvaluation);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));

    }

    public boolean validateSpecialCharacter(String value) {
        String regex = "$ & + , : ; = ? @ # | ' < > . - ^ * ( ) % !";
        for (String item : regex.split(" ")) {
            if (value.contains(item)) {
                return false;
            }
        }
        return true;
    }

    public boolean validationPPUEvaluation() {
        if (!validInputField(ppuEvaluation.getPreProcessName(), "PPU Name", true, true, true)) {
            return false;
        }
        if (ppuEvaluationService.checkExitsProcessorName(ppuEvaluation.getPreProcessName(), ppuEvaluation.getPreProcessId())) {
            duplidateMessage("PPU Name");
            return false;
        }
        if (!validInputField(ppuEvaluation.getCriteriaName(), "Criteria Name", true, false, true)) {
            return false;
        }
        if (!validateSpecialCharacter(ppuEvaluation.getCriteriaName())) {

            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.special.character"));
            return false;
        }
        if (ppuEvaluationService.checkExitsCriteriaName(ppuEvaluation.getCriteriaName(), ppuEvaluation.getPreProcessId())) {
            duplidateMessage("Criteria Name");
            return false;
        }
        if (!DataUtil.checkMaxlength(ppuEvaluation.getCriteriaName(), 45)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength.2"), "Criteria Name");
            return false;
        }
        if (!validInputField(ppuEvaluation.getDescription(), "Description", false, true, true)) {
            return false;
        }
        if (!validInputField(ppuEvaluation.getInputFields(), "Input field", true, true, true)) {
            return false;
        }
        if (!checkInputField(this.ppuEvaluation.getInputFields())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("invalid.inputField"));
            return false;
        }
        if (!validInputField(ppuEvaluation.getSpecialFields(), "Special field", true, false, false)) {
            return false;
        }
        if (ppuEvaluation.getDefaultValue() == -1) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Default value");
            return false;
        }
        if (checkValueNotUseInDefaultValueOrNomalizer(lstProcessValue, lstProcessParam)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.ppu.processValue"));
            return false;
        }
        for (ProcessParam param : lstProcessParam) {
            if (param != null) {
                if (param.getParamIndex() == -1) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("priority.value.name"));
                    return false;

                }
                if (!validInputField(param.getValue(), "Parameter Value", true, true, true)) {
                    return false;
                }
                if (param.getPriority() == null || ("").equalsIgnoreCase(param.getPriority().toString())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Priority");
                    return false;
                }
            }
        }
        if (checkDuplicatePriority(lstProcessParam)) {
            duplidateMessage("Priority");
            return false;
        }
        if (lstInputFieldVariableMap.isEmpty()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Variable Table");
            return false;
        }
        if (lstProcessParam.isEmpty()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Normalizer");
            return false;
        }

        return true;
    }

    private boolean checkExitValueId(Long valueId, List<ProcessParam> lst) {
        for (ProcessParam obj : lst) {
            if (obj.getParamIndex().equals(valueId)) {
                return false;
            }
        }
        return true;
    }

    private boolean checkValueNotUseInDefaultValueOrNomalizer(List<ProcessValue> lstValue, List<ProcessParam> lstParam) {
        for (ProcessValue obj : lstValue) {
            if (checkExitValueId(obj.getValueId(), lstParam) && obj.getValueId() != ppuEvaluation.getDefaultValue()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDuplicatePriority(List<ProcessParam> lstParam) {
        for (int i = 0; i < lstParam.size(); i++) {
            for (int j = i + 1; j < lstParam.size(); j++) {
                if (lstParam.get(i) != null) {
                    if (lstParam.get(i).getPriority().equals(lstParam.get(j).getPriority())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean checkDuplicateValueName(String value) {
        for (ProcessValue obj : lstProcessValueNew) {
            if (obj.getValueName().equals(value)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDuplicateValueName(List<ProcessValue> lstParam) {
        for (int i = 0; i < lstParam.size(); i++) {
            for (int j = i + 1; j < lstParam.size(); j++) {
                if (lstParam.get(i).getValueName().equals(lstParam.get(j).getValueName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean validationProcessValue() {
        for (ProcessValue value : lstProcessValueNew) {
            if (String.valueOf(value.getValueId()) == null || String.valueOf(value.getValueId()).equalsIgnoreCase("")) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Value Id");
                return false;
            }
            if (ppuEvaluationService.checkExitsValueId(value.getProcessValueId(), value.getValueId(), ppuEvaluation.getPreProcessId())) {
                duplidateMessage("Value Id");
                return false;
            }
            if (value.getValueId() <= 0) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.integer"), "Value Id");
                return false;
            }

            if (!validInputField(value.getValueName(), "Value Name", true, false, true)) {
                return false;
            }
            if (!DataUtil.checkMaxlength(value.getValueName(), 45)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength.2"), "Value Name");
                return false;
            }
            if (!validInputField(value.getValueColor(), "Value Color", true, false, false)) {
                return false;
            }

            if (!validInputField(value.getDescription(), "Description", false, true, true)) {
                return false;
            }
        }
        if (checkDuplicateValueName(lstProcessValueNew)) {
            duplidateMessage("Value Name");
        }
        return true;
    }

    public static boolean checkMaxlengthValueName(String input, int length) {
        if (input == null) {
            return true;
        }
        try {
            if (input.trim().length() > length) {
                return false;
            }
        } catch (Exception e) {
            return true;
        }
        return true;
    }

    public class ProcessValueComparator implements Comparator<ProcessValue> {

        @Override
        public int compare(ProcessValue o1, ProcessValue o2) {
            return o1.getValueName().compareTo(o2.getValueName());
        }
    }

    class SortTable implements Comparator<FilterTable> {

        public int compare(FilterTable a, FilterTable b) {
            return a.getFieldId() - b.getFieldId() > 0 ? 1 : -1;
        }
    }

    class SortFunctions implements Comparator<FunctionTable> {

        @Override
        public int compare(FunctionTable o1, FunctionTable o2) {
            return o1.getFunctionId() - o2.getFunctionId() > 0 ? 1 : -1;
        }

    }

    class ProcessParamsComparator implements Comparator<ProcessParam> {

        public int compare(ProcessParam a, ProcessParam b) {
            if (a.getPriority() == null && b.getPriority() == null) {
                return 0;
            } else if (a.getPriority() == null && b.getPriority() != null) {
                return -1;
            } else if (a.getPriority() != null && b.getPriority() == null) {
                return 1;
            } else {
                return a.getPriority() - b.getPriority();
            }
        }
    }

    public boolean checkDuplicate(List<FilterTable> lstFilterTable) {
        List<FilterTable> newList = new ArrayList<>();
        newList.addAll(lstFilterTable);
        Collections.sort(newList, new SortTable());
        long previousId = -1;
        for (FilterTable filterTable : newList) {
            if (previousId == filterTable.getFieldId()) {
                return false;
            }
            previousId = filterTable.getFieldId();
        }
        return true;
    }

    public void onSaveOrUpdatePPUEvaluation() {
        if (validationPPUEvaluation()) {
            ppuEvaluation.setPreProcessType(2);
            ppuEvaluation.setType(2);
            ppuEvaluationService.onSaveOrUpdatePpuEvaluation(ppuEvaluation);
            if (changedInputField) {
                for (InputFieldVariableMap obj : lstInputFieldVariableMapToDelete) {
                    inputFieldVariableMapService.onDeleteInputFieldVariableMap(2, ppuEvaluation.getPreProcessId());
                }
                for (InputFieldVariableMap obj : lstInputFieldVariableMap) {
                    obj.setType(2);
                    obj.setId(null);
                    inputFieldVariableMapService.onSaveOrUpdateInputFieldVariableMap(obj);
                }

            }
            if (!changedInputField) {
                for (InputFieldVariableMap obj : lstInputFieldVariableMap) {
                    obj.setType(2);
                    inputFieldVariableMapService.onSaveOrUpdateInputFieldVariableMap(obj);
                }
            }

            onSaveOrUpdateProcessValue();
            onSaveOrUpdateProcessParam();
            if (this.editMode) {
                this.selectedNode = this.backUpSelectNode;
                initTreeNode();
                PpuEvaluation object = (PpuEvaluation) selectedNode.getData();
                mapTreeStatus(rootNode);
                //searchNode(rootNode, object);

            } else {
                rootNode.getChildren().get(0).setExpanded(true);
                TreeNode treeNode = addNoteToCategoryTree(rootNode, this.ppuEvaluation);
                this.backUpSelectNode = treeNode;
                if (treeNode != null) {
                    treeNode.setSelected(true);
                }
            }

            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
        action = false;
    }

    public void searchNode(TreeNode root, PpuEvaluation object) {
        for (TreeNode node : root.getChildren()) {
            if ("category".equals(node.getType())) {
                Category car = (Category) node.getData();
                if (object.getCategoryId().equals(car.getCategoryId())) {
                    node.setExpanded(true);
                    expandNodeParent(node);
                }
            }
            searchNode(node, object);

        }
    }

    public void expandNodeParent(TreeNode node) {
        if (node.getParent() != null && node != null) {
            node.getParent().setExpanded(true);
            expandNodeParent(node.getParent());
        }
    }

    public TreeNode addNoteToCategoryTree(TreeNode rootNode, BaseCategory baseCategory) {
        TreeNode note = findParentNoteInTree(rootNode, baseCategory);
        if (note != null) {
            TreeNode newNote = new DefaultTreeNode(baseCategory.getTreeType(), baseCategory, note);
            expandCurrentNode(newNote);
            this.selectedNode = newNote;
            this.backUpSelectNode = newNote;
            return newNote;
        }
        return null;
    }

    @Override
    public void expandCurrentNode(TreeNode selectedNode) {
        TreeNode parent = selectedNode.getParent();
        if (parent == null || parent.getData() == null) {
            return;
        } else {
            addExpandedNode(parent);
            expandCurrentNode(parent);
        }
    }

    public TreeNode findParentNoteInTree(TreeNode root, BaseCategory baseCategory) {
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (baseCategory.getParentType().isInstance(note.getData())) {
                Category data = (Category) note.getData();
                if (data.getCategoryId() == baseCategory.getParentId()) {
                    return note;
                }
            }
            currentNote = findParentNoteInTree(note, baseCategory);
            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }

    @Override
    public void onNodeExpand(NodeExpandEvent event) {
        addExpandedNode(event.getTreeNode());
    }

    @Override
    public void addExpandedNode(TreeNode node) {
        Category category = (Category) node.getData();
        if (category == null) {
            return;
        }
        node.setExpanded(true);
        treeNodeExpanded.put(category.getCategoryId(), node);
    }

    @Override
    public void onNodeCollapse(NodeCollapseEvent event) {
        removeExpandedNode(event.getTreeNode());
    }

    @Override
    public void removeExpandedNode(TreeNode node) {
        Category category = (Category) node.getData();
        if (category == null) {
            return;
        }
        node.setExpanded(false);
        treeNodeExpanded.remove(category.getCategoryId());
    }

    public String changeSpecialFields(ProcessParam processParams) {
        processParams.setConfigInput("");
        processParams.setValue(null);
        for (ProcessParam obj : lstProcessParam) {
            if (obj.isParam() == true) {
                ppuEvaluation.setSpecialFields("isUseParameter:true");
                return ppuEvaluation.getSpecialFields();
            }
        }
        ppuEvaluation.setSpecialFields("isUseParameter:false");
        return ppuEvaluation.getSpecialFields();
    }
    List<ProcessValue> lstProcessValueNew = new ArrayList<>();

    public void prepareAddRowValueTable() {
        ProcessValue processValue = new ProcessValue();
        processValue.setValueColor("fffcfb");
        long max = 0;
        if (lstProcessValueNew.isEmpty()) {
            processValue.setValueId(max);
        }
        for (int i = 0; i < lstProcessValueNew.size(); i++) {
            if (lstProcessValueNew.get(i).getValueId() > max) {
                max = lstProcessValueNew.get(i).getValueId();
            }
        }
        processValue.setValueId(max + 1);
        processValue.setProcessValueId(processValueService.getNextSequence().getProcessValueId());
        lstProcessValueNew.add(processValue);
    }

    private boolean checkUseProcessValueinNormalTable(Long value_id, List<ProcessParam> lstParam) {
        for (ProcessParam param : lstParam) {
            if (value_id.equals(param.getParamIndex())) {
                return true;
            }

        }
        return false;
    }

    private boolean validateValue(Long id, List<ProcessParam> lst) {
        if (checkUseProcessValueinNormalTable(id, lst)) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("value.used.in.nomalizer"));
            return false;
        }
        if (ppuEvaluation.getDefaultValue() == id) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("value.used.in.defaultValue"));
            return false;
        }
        return true;
    }

    public void validate() {

    }
    int flag = 1;
    ProcessValue processValueToDelete = new ProcessValue();

    private Long nodeInfoId = 0L;

    public List<NodeInfo> getListNodeInfoFromDisplayExpress(String displayExpression) {

        List<NodeInfo> listNodeInfo = new ArrayList<>();
        this.nodeInfoId = 1L;

        NodeInfo rootNodeInfo = new NodeInfo();

        rootNodeInfo.setId(this.nodeInfoId);
        rootNodeInfo.setValue(displayExpression);
        rootNodeInfo.setType("group");
        rootNodeInfo.setDisplay(null);
        listNodeInfo.add(rootNodeInfo);

        getListNode(rootNodeInfo, listNodeInfo);

        return listNodeInfo;

    }

    public void getListNode(NodeInfo nodeInfo, List<NodeInfo> listNodeInfo) {
        // lấy chuỗi displayExpression of group
        String valueStr = nodeInfo.getValue();

        // Ký tự ngoặc đầu tiên (tính cả ban đầu hoăc sau khi reset nếu đủ bộ ngoặc)
        String startChar = "";

        // vị trí ngoặc mở đầu tiên
        int startIndex = 0;

        // để lưu số lượng ngoặc mở
        int numberOfStartChar = 0;

        // vị trí ngoặc đóng cuối cùng
        int endIndex = 0;

        // để lưu số lượng ngoặc dong
        int numberOfEndChar = 0;

//        ArrayList<String> listString = new ArrayList();
        ArrayList<String> listStringToSplit = new ArrayList();
        List<NodeInfo> listTodequy = new ArrayList();

        for (int i = 0; i < valueStr.length(); i++) {
            if ("".equals(startChar)) {

                if ("[".equals(Character.toString(valueStr.charAt(i))) || "(".equals(Character.toString(valueStr.charAt(i)))) {
                    startChar = Character.toString(valueStr.charAt(i));
                    numberOfStartChar = 1;
                    startIndex = i;
                }
            } else {

                if ("(".equals(startChar)) {

                    if (")".equals(Character.toString(valueStr.charAt(i)))) {
                        numberOfEndChar = numberOfEndChar + 1;

                        if (numberOfEndChar == numberOfStartChar) {
                            endIndex = i;
//                            listString.add(valueStr.substring(startIndex + 1, endIndex));
                            listStringToSplit.add(valueStr.substring(startIndex, endIndex + 1));

                            NodeInfo n = new NodeInfo();
                            this.nodeInfoId = this.nodeInfoId + 1;

                            n.setId(this.nodeInfoId);
                            n.setParentId(nodeInfo.getId());
                            n.setValue(valueStr.substring(startIndex + 1, endIndex));
                            n.setType("group");
//                            n.setDisplay(valueStr.substring(startIndex + 1, endIndex));

                            listNodeInfo.add(n);
                            listTodequy.add(n);

                            startChar = "";
                            numberOfStartChar = 0;
                            startIndex = 0;
                            numberOfEndChar = 0;
                            endIndex = 0;
//                            getListNode(n, nodeInfoId, listNodeInfo);
                        }

                    }
                    if ("(".equals(Character.toString(valueStr.charAt(i)))) {
                        numberOfStartChar = numberOfStartChar + 1;
                    }
                }

                if ("[".equals(startChar)) {
                    if ("]".equals(Character.toString(valueStr.charAt(i)))) {
                        numberOfEndChar = numberOfEndChar + 1;
                        if (numberOfEndChar == numberOfStartChar) {
                            endIndex = i;
//                            listString.add(valueStr.substring(startIndex + 1, endIndex));
                            listStringToSplit.add(valueStr.substring(startIndex, endIndex + 1));

                            NodeInfo n = new NodeInfo();
                            this.nodeInfoId = this.nodeInfoId + 1;

                            n.setId(this.nodeInfoId);
                            n.setParentId(nodeInfo.getId());
                            n.setValue(valueStr.substring(startIndex + 1, endIndex));
                            n.setType("object");
                            n.setDisplay(valueStr.substring(startIndex + 1, endIndex));

                            listNodeInfo.add(n);

                            startChar = "";
                            numberOfStartChar = 0;
                            numberOfEndChar = 0;
                            startIndex = 0;
                            endIndex = 0;
                        }
                    }
                    if ("[".equals(Character.toString(valueStr.charAt(i)))) {
                        numberOfStartChar = numberOfStartChar + 1;
                    }
                }

            }
        }

        String operator = valueStr.replace(listStringToSplit.get(0), "").trim().split(" ")[0];
        nodeInfo.setOperator(operator);
        nodeInfo.setDisplay("Group - " + operator);
        for (NodeInfo itm : listTodequy) {
            getListNode(itm, listNodeInfo);
        }
    }

    public Boolean checkResultClassNameNotIsUsedWithValueExInExpressionOfListFuzzyRule(String name, List<NodeInfo> allListNodeInfor) {
        if (!"".equals(name)) {
            for (NodeInfo item : allListNodeInfor) {
                String[] text = item.getDisplay().split(" ");
                for (int i = 0; i < text.length; i++) {
                    if ("is".equals(text[i])) {
                        if (item.getDisplay().substring(item.getDisplay().indexOf(text[i]) + 2, item.getDisplay().length()).trim().equals(name)) {
                            return false;
                        }
                    }
                    if ("not".equals(text[i])) {
                        if (item.getDisplay().substring(item.getDisplay().indexOf(text[i]) + 3, item.getDisplay().length()).trim().equals(name)) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public void prepareDeleteRowValueTable(ProcessValue processValue) {
        flag = 1;
        processValueToDelete.setProcessValueId(processValue.getProcessValueId());
        for (FuzzyRule obj : processValueService.getListFuzzyForProcessValue(ppuEvaluation.getPreProcessId())) {
            if (!checkResultClassNameNotIsUsedWithValueExInExpressionOfListFuzzyRule(processValue.getValueName(), getListNodeInfoFromDisplayExpress(obj.getDisplayExpression()))) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("value.used.in.assessmentRule"));
                flag = 0;
                return;
            }
        }
        if (!validateValue(processValue.getValueId(), lstProcessParam)) {

            flag = 0;
        }
    }

    public void deleteValueTable() {
        if (flag == 1) {
            for (ProcessValue obj : lstProcessValueNew) {
                if (obj.getProcessValueId() == processValueToDelete.getProcessValueId()) {
                    lstProcessValueNew.remove(obj);
                    lstProcessValueToDelete.add(obj);
                    return;
                }
            }

            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public void onSaveOrUpdateProcessValue() {
        for (ProcessValue obj : lstProcessValue) {
            if (processValueService.checkSavePreProcessValueMap(obj.getProcessValueId(), ppuEvaluation.getPreProcessId())) {
                processValueService.onSaveOrUpdateProcessValue(obj);
            }
            if (!processValueService.checkSavePreProcessValueMap(obj.getProcessValueId(), ppuEvaluation.getPreProcessId())) {
                PreProcessValueMap preProcessValueMap = new PreProcessValueMap();
                obj.setProcessValueId(0);
                processValueService.onSaveOrUpdateProcessValue(obj);
                preProcessValueMap.setProcessValueId(obj.getProcessValueId());
                preProcessValueMap.setPreProcessId(this.ppuEvaluation.getPreProcessId());
                preProcessValueMapDAO.onSaveOrUpdatePreProcessValueMap(preProcessValueMap);
            }

        }
        for (ProcessValue obj : lstProcessValueToDelete) {
            processValueService.onDeleteProcessValue(obj.getProcessValueId(), ppuEvaluation.getPreProcessId());
        }
    }

    public void showValueDefault() {
        this.lstProcessValueNew = new ArrayList<>();
        for (ProcessValue obj : lstProcessValue) {
            ProcessValue objNew = new ProcessValue();
            objNew.setValueId(obj.getValueId());
            objNew.setValueName(obj.getValueName());
            objNew.setValueColor(obj.getValueColor());
            objNew.setDescription(obj.getDescription());
            objNew.setPreprocessId(obj.getPreprocessId());
            objNew.setValueIndex(obj.getValueIndex());
            objNew.setProcessValueId(obj.getProcessValueId());
            lstProcessValueNew.add(objNew);
        }
    }

    public void onSaveValueDefault() {
        lstProcessValue = new ArrayList<>();
        for (ProcessValue obj : lstProcessValueNew) {
            ProcessValue objNew = new ProcessValue();
            objNew.setValueId(obj.getValueId());
            objNew.setValueName(obj.getValueName());
            objNew.setValueColor(obj.getValueColor());
            objNew.setDescription(obj.getDescription());
            objNew.setPreprocessId(obj.getPreprocessId());
            objNew.setValueIndex(obj.getValueIndex());
            objNew.setProcessValueId(obj.getProcessValueId());
            lstProcessValue.add(objNew);
        }
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public void prepareAddProcessParam() {
        ProcessParam obj = new ProcessParam();

        Integer max = 0;
        lstProcessParam.add(obj);
        obj.setPriority(0);
        if (lstProcessParam.size() == 1) {
            lstProcessParam.get(0).setPriority(1);
        } else {
            max = lstProcessParam.get(0).getPriority();
            for (int i = 0; i < lstProcessParam.size(); i++) {
                if (lstProcessParam.get(i).getPriority() > max) {
                    max = lstProcessParam.get(i).getPriority();
                }
            }
            obj.setPriority(max + 1);
        }

    }

    public void prepareDeleteProcessParam(ProcessParam processParam) {
        lstProcessParam.remove(processParam);
        lstProcessParamToDelete.add(processParam);
    }

    public void onSaveOrUpdateProcessParam() {
        for (ProcessParam obj : lstProcessParam) {
            if (processParamDAO.checkOnSavePreProcessParamMap(obj.getProcessParamId(), ppuEvaluation.getPreProcessId()) == null) {
                obj.setConfigInput("inputNumber:" + obj.getValue() + ";isUseParameter:" + obj.isParam()
                        + ";comparisionType:" + (obj.getType() == null ? "" : obj.getType()) + ";priority:" + (obj.getPriority() == null ? "" : obj.getPriority()));
                processParamDAO.onSaveOrUpdateProcessParam(obj);
                PreProcessParamMap p = new PreProcessParamMap();
                p.setPreProcessId(ppuEvaluation.getPreProcessId());
                p.setProcessParamId(obj.getProcessParamId());
                preProcessParamMapDAO.onSaveOrUpdatePreProcessParamMap(p);
            } else {
                obj.setConfigInput("inputNumber:" + obj.getValue() + ";isUseParameter:" + obj.isParam()
                        + ";comparisionType:" + (obj.getType() == null ? "" : obj.getType()) + ";priority:" + (obj.getPriority() == null ? "" : obj.getPriority()));
                processParamDAO.onSaveOrUpdateProcessParam(obj);
            }
        }
        for (ProcessParam obj : lstProcessParamToDelete) {
            processParamDAO.onDeleteProcessParam(obj.getProcessParamId(), ppuEvaluation.getPreProcessId());
        }

    }

    public String removeSpaces(String str) {
        str = str.replaceAll(" +", " ");
        return str;
    }

    private boolean checkExitString(String str, List<String> lst) {
        for (String itt : lst) {
            if (str.equals(itt)) {
                return true;
            }
        }
        return false;
    }

    public void genVariableNameTable() {
        if (!checkInputField(this.ppuEvaluation.getInputFields())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("invalid.inputField"));
            try {
                if (this.ppuEvaluation.getCloneInputFiled() == null && ppuEvaluationService.findOneById(ppuEvaluation.getPreProcessId()).getInputFields() != null) {
                    this.ppuEvaluation.setInputFields(ppuEvaluationService.findOneById(ppuEvaluation.getPreProcessId()).getInputFields());
                    return;
                }
            } catch (NullPointerException e) {
                this.ppuEvaluation.setInputFields("");
                e.printStackTrace();
                return;
            }
            this.ppuEvaluation.setInputFields(this.ppuEvaluation.getCloneInputFiled());

        } else {
            this.ppuEvaluation.setCloneInputFiled(this.ppuEvaluation.getInputFields());
            this.changedInputField = true;
            lstInputFieldVariableMap = new ArrayList<>();
            String[] variableArray = this.ppuEvaluation.getInputFields()
                    .replace("+", " ")
                    .replace("-", " ")
                    .replace("*", " ")
                    .replace("/", " ")
                    .replace("(", " ")
                    .replace(")", " ").replaceAll(" +", " ").trim().split(" ");
            List<String> variables = new ArrayList<>();
            for (int i = 0; i < variableArray.length; i++) {
                if (!checkIsNumber(variableArray[i]) && !checkExitString(variableArray[i], variables)) {
                    variables.add(variableArray[i]);
                }
            }
            for (String item : variables) {
                if (!item.equals("")) {
                    inputFieldVariableMap = new InputFieldVariableMap();
                    inputFieldVariableMap.setId(inputFieldVariableMapService.getNextSequence().getId());
                    inputFieldVariableMap.setOwnerId(this.ppuEvaluation.getPreProcessId());
                    inputFieldVariableMap.setType(2);
                    inputFieldVariableMap.setVariableName(item);
                    inputFieldVariableMap.setVariableValue("");
                    lstInputFieldVariableMap.add(inputFieldVariableMap);
                }
            }
        }
    }

    public Boolean checkInputField(String inputField) {
        String pattern = "[-+]?[0-9]*\\.?[0-9]*[a-zA-Z]*[()+*/-]*";
        String patternNumber = "[-+]?[0-9]*\\.?[0-9]*";
        String patternVariable = "[a-z]*";
        String[] variableArray = inputField
                .replace("+", " ")
                .replace("-", " ")
                .replace("*", " ")
                .replace("/", " ")
                .replace("(", " ")
                .replace(")", " ").replaceAll(" +", " ").trim().split(" ");
        for (String item : variableArray) {
            if (!Pattern.matches(pattern, item)) {
                return false;
            }
            if (!Pattern.matches(patternNumber, item) && !Pattern.matches(patternVariable, item)) {
                return false;
            }
        }
        class shortvariableArray implements Comparator<String> {

            @Override
            public int compare(String o1, String o2) {
                if (o1.length() > o2.length()) {
                    return 1;
                }
                if (o1.length() < o2.length()) {
                    return -1;
                }
                if (o1.length() == o2.length()) {
                    return 0;
                }
                return 0;
            }

        }
        List<String> newVariableArray = new ArrayList<>();
        List<String> newVariableArray1 = new ArrayList<>();
        for (String item : variableArray) {
            newVariableArray.add(item);
            newVariableArray1.add(item);
        }

        Collections.sort(newVariableArray, new shortvariableArray());

        for (int i = newVariableArray.size() - 1; i >= 0; i--) {
            inputField = inputField.replace(variableArray[newVariableArray1.indexOf(newVariableArray.get(i))], "1");
        }
        try {
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("javascript");
            engine.eval(inputField);
            return true;

        } catch (ScriptException ex) {
            return false;
        }
    }

    public void prepareEditInputFieldVariableMapView(InputFieldVariableMap inputFieldVariableMap) {
        this.view = false;
        this.viewFilter = true;
        if (inputFieldVariableMap.getVariableValue().equals("")) {
            inputFieldVariableMap.setIsInputMode(1);
        }
        this.inputFieldVariableMap = new InputFieldVariableMap();
        this.inputFieldVariableMap.setId(inputFieldVariableMap.getId());
        this.inputFieldVariableMap.setOwnerId(inputFieldVariableMap.getOwnerId());
        this.inputFieldVariableMap.setType(inputFieldVariableMap.getType());
        this.inputFieldVariableMap.setVariableName(inputFieldVariableMap.getVariableName());
        this.inputFieldVariableMap.setVariableValue(inputFieldVariableMap.getVariableValue());
        int type = inputFieldVariableMap.getInputMode();
        if (type == 1) {
            this.inputFieldVariableMap.setIsInputMode(1);
            genPathTable(inputFieldVariableMap.getVariableValue());
        } else {
            this.inputFieldVariableMap.setIsInputMode(2);
            genFunctionTable(inputFieldVariableMap.getVariableValue());
        }
    }
    boolean isObject = false;

    public void prepareEditInputFieldVariableMap(InputFieldVariableMap inputFieldVariableMap) {
        this.view = true;
        this.viewFilter = false;
        if (inputFieldVariableMap.getVariableValue().equals("")) {
            inputFieldVariableMap.setIsInputMode(1);
        }
        this.inputFieldVariableMap = new InputFieldVariableMap();
        this.inputFieldVariableMap.setId(inputFieldVariableMap.getId());
        this.inputFieldVariableMap.setOwnerId(inputFieldVariableMap.getOwnerId());
        this.inputFieldVariableMap.setType(inputFieldVariableMap.getType());
        this.inputFieldVariableMap.setVariableName(inputFieldVariableMap.getVariableName());
        this.inputFieldVariableMap.setVariableValue(inputFieldVariableMap.getVariableValue());
        if (this.inputFieldVariableMap.getVariableValue().equals("")) {
            this.inputFieldVariableMap.setVariableValue("type:1|");
            this.inputFieldVariableMap.setIsInputMode(1);
            lstPath = new ArrayList<>();
            functions = new ArrayList<>();
        }
        int type = inputFieldVariableMap.getInputMode();
        if (type == 1) {
            isObject = true;
            this.inputFieldVariableMap.setIsInputMode(1);
            genPathTable(inputFieldVariableMap.getVariableValue());
        } else {
            isObject = false;
            this.inputFieldVariableMap.setIsInputMode(2);
            genFunctionTable(inputFieldVariableMap.getVariableValue());
        }

    }

    public void genPathTable(String value) {
        lstPath = new ArrayList<>();
        if (!value.equals("type:1|") && !value.equals("") && !value.equals("type:2|")) {
            String[] paths = value.trim()
                    .split("type:[0-9]+\\|")[1]
                    .replace("{", "$").split("[}\\.]+");
            Long parentId = null;
            for (int i = 0; i < paths.length; i++) {
                path = new Path();

                if (i == 0) {
                    if (paths[i].split("\\$")[0].equals("null")) {
                        lstPath.add(path);
                        StringBuilder stringBuilder = new StringBuilder();
                        for (Path p : lstPath) {
                            if (p.getId() != -1) {
                                if (!p.getDataCondition().trim().equals("") && !p.getDataFunction().trim().equals("")) {
                                    stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
                                }
                                if (p.getDataCondition().trim().equals("") && !p.getDataFunction().trim().equals("")) {
                                    stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
                                }
                                if (!p.getDataCondition().trim().equals("") && p.getDataFunction().trim().equals("")) {
                                    stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
                                }
                                if (p.getDataCondition().trim().equals("") && p.getDataFunction().trim().equals("")) {

                                    stringBuilder.append(".").append(p.getObjectName());
                                }
                            }

                        }
                        StringBuilder pathNew = new StringBuilder("type:" + inputFieldVariableMap.getInputMode() + "|");
                        String replace = stringBuilder.toString().replaceFirst(".", "");
                        pathNew.append(replace);
                        this.inputFieldVariableMap.setVariableValue(pathNew.toString());
                        return;
                    } else {
                        path.setLstEvaluationInputObject(evaluationInputObjectService.getLstEvaluationInputObjectHaveParentIdIsNull());
                        parentId = evaluationInputObjectService.findOneByName(paths[i].split("\\$")[0]).getObjectId();
                    }
                }
                if (i != 0) {
                    if (paths[i].split("\\$")[0].equals("null")) {
                        Long parent = evaluationInputObjectService.findOneByName(paths[i - 1].split("\\$")[0]).getObjectId();
                        if (!evaluationInputObjectService.getLstEvaluationInputObjectByParentId(parentId).isEmpty()) {
                            List<EvaluationInputObject> lstChoose = evaluationInputObjectService.getLstEvaluationInputObjectByParentId(parent);
                            path.setLstEvaluationInputObject(lstChoose);
                            lstPath.add(path);
                        }
                        path.setId(-1);
                        StringBuilder stringBuilder = new StringBuilder();
                        for (Path p : lstPath) {
                            if (p.getId() != -1) {
                                if (!p.getDataCondition().trim().equals("") && !p.getDataFunction().trim().equals("")) {
                                    stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
                                }
                                if (p.getDataCondition().trim().equals("") && !p.getDataFunction().trim().equals("")) {
                                    stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
                                }
                                if (!p.getDataCondition().trim().equals("") && p.getDataFunction().trim().equals("")) {
                                    stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
                                }
                                if (p.getDataCondition().trim().equals("") && p.getDataFunction().trim().equals("")) {

                                    stringBuilder.append(".").append(p.getObjectName());
                                }
                            }

                        }
                        StringBuilder pathNew = new StringBuilder("type:" + inputFieldVariableMap.getInputMode() + "|");
                        String replace = stringBuilder.toString().replaceFirst(".", "");
                        pathNew.append(replace);
                        this.inputFieldVariableMap.setVariableValue(pathNew.toString());
                        return;
                    } else {
                        path.setLstEvaluationInputObject(evaluationInputObjectService.getLstEvaluationInputObjectByParentId(parentId));
                        parentId = evaluationInputObjectService.findOneByName(paths[i].split("\\$")[0]).getObjectId();
                    }
                }
                path.setInputObject(evaluationInputObjectService.findOneByName(paths[i].split("\\$")[0]));
                path.setId(i);
                if (!paths[i].contains("$")) {
                    path.setFilter("");
                }
                if (paths[i].contains("$")) {
                    path.setFilter(paths[i].split("\\$")[1]);
                }
                path.setObjectName(paths[i].split("\\$")[0]);
                lstPath.add(path);

            }
        }
    }

    public void changePathRowInTable(Path path, int index) {
        StringBuilder pathNew = new StringBuilder("type:" + inputFieldVariableMap.getInputMode() + "|");
        if (lstPath.isEmpty()) {
            this.inputFieldVariableMap.setVariableValue(pathNew.toString());
        } else if (lstPath.get(index).getInputObject().getObjectId() == -1) {
            return;
        } else if (index == 0 && lstPath.get(lstPath.size() - 1).getInputObject().getObjectId() == -1) {
            this.inputFieldVariableMap.setVariableValue(pathNew.toString());
        } else {
            int i = 0;
            while (i <= index) {
                lstPath.get(index).setObjectName(evaluationInputObjectService.findOneById(path.getInputObject().getObjectId()).getObjectName());
                if (i == index) {
                    pathNew.append(lstPath.get(i).getObjectName());
                }
                if (i < index && !lstPath.get(i).getFilter().contains("")) {
                    pathNew.append(lstPath.get(i).getObjectName()).append("{").append(lstPath.get(i).getFilter()).append("}.");
                }
                if (i < index && lstPath.get(i).getFilter().contains("")) {
                    pathNew.append(lstPath.get(i).getObjectName()).append(".");
                }
                i++;
            }
            this.inputFieldVariableMap.setVariableValue(pathNew.toString());
            genPathTable(this.inputFieldVariableMap.getVariableValue());
        }

    }

    public void changePathRowTable() {
        if (validateFilter()) {
            StringBuilder stringBuilder = new StringBuilder();
            for (Path p : lstPath) {
                if (p.getId() == path.getId()) {
                    p.setDataCondition(path.getDataCondition());
                    p.setDataFunction(path.getDataFunction());
                    p.setLstEvaluationInputObject(path.getLstEvaluationInputObject());
                    p.setFilter(path.getFilter());
                    p.setObjectName(path.getObjectName());
                    path.setId(-111);
                }
                if (!p.getDataCondition().trim().equals("") && !p.getDataFunction().trim().equals("")) {
                    stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
                }
                if (p.getDataCondition().trim().equals("") && !p.getDataFunction().trim().equals("")) {
                    stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
                }
                if (!p.getDataCondition().trim().equals("") && p.getDataFunction().trim().equals("")) {
                    stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
                }
                if (p.getDataCondition().trim().equals("") && p.getDataFunction().trim().equals("")) {
                    if (p.getObjectName() == null) {
                        stringBuilder.append(".").append("");
                    }
                    if (p.getObjectName() != null) {
                        stringBuilder.append(".").append(p.getObjectName().trim());

                    }
                }

            }
            if (lstPath.get(lstPath.size() - 1).getInputObject().getObjectId() == -1) {
                StringBuilder pathNew = new StringBuilder("type:" + inputFieldVariableMap.getInputMode() + "|");
                String replace = stringBuilder.toString().replaceFirst(".", "");
                pathNew.append(replace);
                pathNew.append("null");
                this.inputFieldVariableMap.setVariableValue(pathNew.toString());
                genPathTable(inputFieldVariableMap.getVariableValue());
                return;
            }
            StringBuilder pathNew = new StringBuilder("type:" + inputFieldVariableMap.getInputMode() + "|");
            String replace = stringBuilder.toString().replaceFirst(".", "");
            pathNew.append(replace);
            this.inputFieldVariableMap.setVariableValue(pathNew.toString());
            genPathTable(inputFieldVariableMap.getVariableValue());
        }

    }

    public void changePathRowOnTable(int index) {
        for (int i = index + 1; i < lstPath.size();) {
            lstPath.remove(i);
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (Path p : lstPath) {
            if (lstPath.indexOf(p) == index && p.getInputObject().getObjectId() != -1) {
                lstPath.get(index).setObjectName(evaluationInputObjectService.findOneById(p.getInputObject().getObjectId()).getObjectName());
                stringBuilder.append(".").append(p.getObjectName());
                StringBuilder pathNew = new StringBuilder("type:" + inputFieldVariableMap.getInputMode() + "|");
                String replace = stringBuilder.toString().replaceFirst(".", "");
                pathNew.append(replace);
                this.inputFieldVariableMap.setVariableValue(pathNew.toString());
                genPathTable(this.inputFieldVariableMap.getVariableValue());
                return;
            }
            if (lstPath.indexOf(p) >= 0 && p.getInputObject().getObjectId() == -1) {
                lstPath.get(index).setObjectName("");
                StringBuilder pathNew = new StringBuilder("type:" + inputFieldVariableMap.getInputMode() + "|");
                String replace = stringBuilder.toString().replaceFirst(".", "");
                pathNew.append(replace);
                addChildPath();
                PrimeFaces.current().ajax().update("addVeriablePopup:pathTable");
                this.inputFieldVariableMap.setVariableValue(pathNew.toString());
                return;
            }

            if (!p.getDataCondition().equals("") && !p.getDataFunction().equals("")) {
                stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
            }
            if (p.getDataCondition().equals("") && !p.getDataFunction().equals("")) {
                stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
            }
            if (p.getDataCondition().equals("") && p.getDataFunction().equals("")) {

                stringBuilder.append(".").append(p.getObjectName());
            }

        }
    }
    Boolean checkDeDeletePathRow = false;
    Boolean checkFilter = false;

    public void prepareDeletePathRow(Path path, int index) {
        checkDeDeletePathRow = true;
        for (int i = index; i < lstPath.size();) {
            lstPath.remove(i);
        }
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder pathNew = new StringBuilder("type:" + inputFieldVariableMap.getInputMode() + "|");
        for (Path p : lstPath) {
            if (!p.getDataCondition().equals("") && !p.getDataFunction().equals("")) {
                stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
            }
            if (p.getDataCondition().equals("") && !p.getDataFunction().equals("")) {
                stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
            }
            if (!p.getDataCondition().equals("") && p.getDataFunction().equals("")) {
                stringBuilder.append(".").append(p.getObjectName()).append("{").append(p.getDataCondition()).append(";").append(p.getDataFunction()).append("}");
            }
            if (p.getDataCondition().equals("") && p.getDataFunction().equals("")) {

                stringBuilder.append(".").append(p.getObjectName());
            }

        }
        String replace = stringBuilder.toString().replaceFirst(".", "");
        pathNew.append(replace);

        this.inputFieldVariableMap.setVariableValue(pathNew.toString());
        if (index == 0) {
            this.inputFieldVariableMap.setVariableValue("type:1|");
        }
        genPathTable(this.inputFieldVariableMap.getVariableValue());

    }

    public void prepareCreateCategory() {
        Long id = ussdScenarioService.getNextSequense(Constants.TableName.CATEGORY);
        this.update = false;
        this.category = new Category();
        this.category.setCategoryType(CATEORY_PPU_EVALUATION_TYPE);
        category.setCategoryId(id);
        Category parentCat = (Category) selectedNode.getData();
        this.category.setParentId(parentCat.getCategoryId());
        updateCategoryInformCampaign();
    }

    public void prepareEditCategory() {
        this.update = true;
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void doSaveOrUpdateCategory() {
        category.setCategoryType(34);
        categoryService.save(category);
        refreshTree();
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));

    }

    class SortCategory implements Comparator<Category> {

        public int compare(Category a, Category b) {
            return a.getName().compareTo(b.getName());
        }
    }

    private void refreshTree() {
        if (update) {
            int i = lstCategory.indexOf(selectedNode.getData());
            lstCategory.remove((Category) selectedNode.getData());
            lstCategory.add(i, this.category);
        } else {
            lstCategory.add(this.category);
        }

        Collections.sort(lstCategory, new SortCategory());
        initTreeNode();
        processRefreshCategory(rootNode, selectedNode, category, update);
        updateCategoryInform();
    }

    public void doDeleteCategory() {
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();
            if (inValidCategoryNode()) {
                return;
            }
            if (categoryService.checkDeleteCategory(this.category.getCategoryId())) {
                categoryService.deleteCategory(category);
                selectedNode.getChildren().clear();
                TreeNode parNode = selectedNode.getParent();
                selectedNode.getParent().getChildren().remove(selectedNode);
                if (parNode.getChildren().isEmpty()) {
                    removeExpandedNode(parNode);
                }
                selectedNode.setParent(null);
                selectedNode = null;
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
                lstCategory.remove(category);
//                parNode.setExpanded(true);
                updateCategoryInformCampaign();
            } else {
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    private boolean checkIsNumber(String item) {
        try {
            Double.parseDouble(item);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean validateCategory() {
        boolean rs = true;
        if (!categoryService.checkDuplicate(category.getName(), category.getCategoryId(), category.getCategoryType())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Category name");
            rs = false;
        }
        if (!validInputField(category.getName(), "Category name", true, true, true)) {
            rs = false;
        }
        if (category.getName().equalsIgnoreCase(categoryService.getCategoryById(category.getParentId()).getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.parent.category"));
            return false;
        }
        return rs;
    }

    public boolean inValidCategoryNode() {
        Category car = (Category) selectedNode.getData();
        if (DataUtil.isNullObject(car.getParentId())) {
            errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.parent.category"));
            return true;
        }
        if (!selectedNode.getChildren().isEmpty()) {
            errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), car.getName());
            return true;
        }
        return false;
    }

    public TreeNode genRootNode() {
        TreeNode root_ = new DefaultTreeNode(null);
        for (EvaluationInputObject obj : this.lstEvaluationInputObject) {
            if (obj.getObjectParentId() == null) {
                TreeNode node = new DefaultTreeNode("parent", obj, root_);
                genChildNode(obj.getObjectId(), node, this.lstEvaluationInputObject);
            }

        }
        return root_;
    }
    TreeNode chooseNode;

    private void genChildNode(Long parentId, TreeNode nodeParent, List<EvaluationInputObject> lstEvaluationInputObject) {
        for (EvaluationInputObject obj : lstEvaluationInputObject) {
            if (obj.getObjectParentId() != null && obj.getObjectParentId().equals(parentId)) {
                TreeNode childNode = new DefaultTreeNode("child", obj, nodeParent);
                genChildNode(obj.getObjectId(), childNode, lstEvaluationInputObject);
            }
        }
    }

    public void showTreeNode() {
        chooseNode = genRootNode();
    }

    public void genExpanded(TreeNode nodeParent) {
        for (TreeNode node : nodeParent.getChildren()) {
            node.setExpanded(true);
            genExpanded(node);
        }
    }

    public void genPathTable() {
        this.lstPath = new ArrayList<>();
        List<TreeNode> lstTreeNodeParent = new ArrayList<>();
        lstTreeNodeParent = getLstParentNodeSelected(selectedNodeInput, lstTreeNodeParent);
        for (TreeNode node : lstTreeNodeParent) {
            if (node != null) {
                Path p = new Path();
                EvaluationInputObject object = (EvaluationInputObject) node.getData();
                p.setInputObject(object);
                p.setObjectName(object.getObjectName());
                if (node.getParent() == null) {
                    p.setLstEvaluationInputObject(evaluationInputObjectService.getLstEvaluationInputObjectHaveParentIdIsNull());
                } else {
                    p.setLstEvaluationInputObject(getLstChildNode(node.getParent()));
                }
                lstPath.add(p);
            }
        }
    }

    public TreeNode genCategoryAndComponent(List<Category> lstCategory, List<PpuEvaluation> lstObject) {
        TreeNode root_ = new DefaultTreeNode(null);
        for (Category car : lstCategory) {
            if (car.getParentId() == null) {
                TreeNode nodeChild = new DefaultTreeNode("category", car, root_);
                genChildCategoryAndObject(car, nodeChild, lstCategory, lstObject);
            }
        }
        return root_;
    }

    private void genChildCategoryAndObject(Category parentCar, TreeNode parentNode, List<Category> lstCategory, List<PpuEvaluation> lstObject) {
        for (Category car : lstCategory) {
            if (car.getParentId() != null && car.getParentId().equals(parentCar.getCategoryId())) {
                TreeNode nodeChildCar = new DefaultTreeNode("category", car, parentNode);
                genChildCategoryAndObject(car, nodeChildCar, lstCategory, lstObject);
            }
        }
        for (PpuEvaluation obj : lstObject) {
            if (obj.getCategoryId().equals(parentCar.getCategoryId())) {
                DefaultTreeNode defaultTreeNode = new DefaultTreeNode("object", obj, parentNode);

            }
        }
    }

    private List<TreeNode> getLstParentNodeSelected(TreeNode nodeSelected, List<TreeNode> lstTreeNodeParent) {
        EvaluationInputObject e = (EvaluationInputObject) nodeSelected.getData();
        lstTreeNodeParent.add(0, nodeSelected);
        if (e.getObjectParentId() == null) {
            return null;
        }
        getLstParentNodeSelected(nodeSelected.getParent(), lstTreeNodeParent);
        return lstTreeNodeParent;
    }

    private List<EvaluationInputObject> getLstChildNode(TreeNode parent) {
        List<EvaluationInputObject> lstChildEvaluation = new ArrayList<>();
        List<TreeNode> lstChildNode = parent.getChildren();
        for (TreeNode node : lstChildNode) {
            EvaluationInputObject obj = (EvaluationInputObject) node.getData();
            lstChildEvaluation.add(obj);
        }
        return lstChildEvaluation;
    }

    public void initPathTree() {
        getRootNote();

    }

    public void buildTree(TreeNode node, List<EvaluationInputObject> lstInputObject, EvaluationInputObject parent) {
        for (EvaluationInputObject input : lstInputObject) {
            if (input.getObjectParentId() != null && input.getObjectParentId().equals(parent.getObjectId())) {
                TreeNode childNode = new DefaultTreeNode("object", input, node);
                buildTree(childNode, lstInputObject, input);
            }
        }
    }

    public TreeNode getRootNote() {
        rootNodeInput = new DefaultTreeNode(null, null);
        for (EvaluationInputObject it : lstEvaluationInputObject) {
            if (it.getObjectParentId() == null) {
                EvaluationInputObject rootObject = it;
                TreeNode note = new DefaultTreeNode("root", rootObject, rootNodeInput);
                buildTree(note, lstEvaluationInputObject, rootObject);
            }
        }
        rootNodeInput.setSelectable(false);
        rootNodeInput.setExpanded(true);
        return rootNodeInput;
    }

    public void validateChoosePath() {
        if (selectedNodeInput == null) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.object.choose"));
        } else {
            List<Path> data = new ArrayList<>();
            //getDataToPathTable(data, selectedNodeInput);
            data = getLstDataToTree(data, selectedNodeInput);
            lstPath = new ArrayList<>();
            lstPath.addAll(data);

            StringBuilder pathNew = new StringBuilder("type:" + inputFieldVariableMap.getInputMode() + "|");
            for (int i = 0; i < lstPath.size(); i++) {
                if (i == lstPath.size() - 1) {
                    pathNew.append(lstPath.get(i).getObjectName());
                } else {
                    pathNew.append(lstPath.get(i).getObjectName()).append(".");
                }
            }
            this.inputFieldVariableMap.setVariableValue(pathNew.toString());
            genPathTable(this.inputFieldVariableMap.getVariableValue());
        }

    }

    public void getDataToPathTable(List<Path> data, TreeNode selectedNodeInput) {
        EvaluationInputObject inputObject = (EvaluationInputObject) selectedNodeInput.getData();

        Path pathTable = new Path();
        pathTable.setObjectName(inputObject.getObjectName());
        List<EvaluationInputObject> lstInputObject = getChildDataOfNode(selectedNodeInput.getParent());
        pathTable.setLstEvaluationInputObject(lstInputObject);
        data.add(0, pathTable);

        if (inputObject.getObjectParentId() == null) {
            return;
        }
        getDataToPathTable(data, selectedNodeInput.getParent());

    }

    public List<EvaluationInputObject> getChildDataOfNode(TreeNode node) {
        List<TreeNode> lstTreeNote = node.getChildren();
        List<EvaluationInputObject> lstInputObject = new ArrayList<>();
        for (TreeNode treeNode : lstTreeNote) {
            lstInputObject.add((EvaluationInputObject) treeNode.getData());
        }
        return lstInputObject;
    }

    public List<Path> getLstDataToTree(List<Path> lstResult, TreeNode node) {
        List<EvaluationInputObject> lstResultInput = new ArrayList<>();
        lstResultInput = getLstChildNode(lstResultInput, node);
        for (EvaluationInputObject obj : lstResultInput) {
            Path pathTable = new Path();
            pathTable.setObjectName(obj.getObjectName());
            pathTable.setLstEvaluationInputObject(lstResultInput);
            lstResult.add(pathTable);
        }
        return lstResult;
    }

    public List<EvaluationInputObject> getLstChildNode(List<EvaluationInputObject> lstResult, TreeNode node) {
        if (node.getParent() == null) {
            return lstResult;
        }
        lstResult.add(0, (EvaluationInputObject) node.getData());
        getLstChildNode(lstResult, node.getParent());
        return lstResult;
    }

    public void addChildPath() {
        if (lstPath.isEmpty()) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.dont.parent"));
            return;
        } else if (lstPath.get(lstPath.size() - 1).getInputObject().getObjectId() == -1) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.dont.child"));
            return;
        } else {
            Long parentId = evaluationInputObjectService.findOneByName(lstPath.get(lstPath.size() - 1).getObjectName()).getObjectId();
            if (evaluationInputObjectService.getLstEvaluationInputObjectByParentId(parentId).isEmpty()) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.dont.child"));
                return;
            }
            if (!evaluationInputObjectService.getLstEvaluationInputObjectByParentId(parentId).isEmpty()) {
                List<EvaluationInputObject> lstChoose = evaluationInputObjectService.getLstEvaluationInputObjectByParentId(parentId);
                Path pathNews = new Path();
                pathNews.setLstEvaluationInputObject(lstChoose);
                lstPath.add(pathNews);
            }
        }
    }

    public void prepareEditFilter(Path paths) {
        lstCondition = new ArrayList<>();
        lstFunction = new ArrayList<>();
        this.path = new Path();
        this.path = paths;
        Long parentId = evaluationInputObjectService.findOneByName(paths.getObjectName()).getObjectId();
        String[] lstDataCondition;
        lstDataCondition = path.getDataCondition().split("&");
        for (String item : lstDataCondition) {
            condition = new ConditionTable();
            condition.setObjectDataType(evaluationInputObjectService.findOneByName(item.split("=")[0]).getObjectDataType());
            condition.setObjectId(evaluationInputObjectService.findOneByName(item.split("=")[0]).getObjectId());
            condition.setObjectName(item.split("=")[0]);
            condition.setValue(item.split("=")[1]);
            condition.setLstInputObject(evaluationInputObjectService.getListCondition(parentId));
            this.lstCondition.add(condition);
        }

        String[] lstDataFunction = path.getDataFunction().split(":");

        for (String item : lstDataFunction) {
            String functionName = item.split("\\(")[0];
            String[] lstParam;
            lstParam = item.substring(item.indexOf("(") + 1, item.indexOf(")")).split(",");
            FunctionTable function = new FunctionTable();
            function.setLstParam(lstParam);
            function.setFunctionName(functionName);
            lstFunction.add(function);

        }

    }
    boolean checkSizeLstCondition = false;

    public void prepareDataToShowFilterTable(Path pathTable) {
        this.path = new Path();
        if (pathTable.getInputObject().getObjectId() < 1) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.path"));
            return;
        }

        this.path.setDataCondition(pathTable.getDataCondition());
        this.path.setDataFunction(pathTable.getDataFunction());
        this.path.setLstEvaluationInputObject(pathTable.getLstEvaluationInputObject());
        this.path.setFilter(pathTable.getFilter());
        this.path.setObjectName(pathTable.getObjectName());
        this.path.setId(pathTable.getId());
        this.path.setInputObject(pathTable.getInputObject());

        Long parentId = evaluationInputObjectService.findOneByName(pathTable.getObjectName()).getObjectId();
        lstConditionData = evaluationInputObjectService.getListCondition(parentId);
        EvaluationInputObject object = evaluationInputObjectService.findOneByName(pathTable.getObjectName());
        lstConditionData = evaluationInputObjectService.getListCondition(parentId);
        if ((object.getObjectType() == 2 || object.getObjectType() == 3) && object.getObjectDataType() == 1 && !lstConditionData.isEmpty()) {
            checkSizeLstCondition = true;
        } else {
            checkSizeLstCondition = false;
        }
        lstConditionTable = convertConditionFilterToList(path.getDataCondition());
        lstFunctionTable = convertFunctionFilterToList(path.getDataFunction(), 2);

    }

    public boolean validateInputCondition(List<FilterTable> lstConditionTable, Map<Long, EvaluationInputObject> mapInputObject) {
        for (FilterTable filterTable : lstConditionTable) {
            EvaluationInputObject input = mapInputObject.get(filterTable.getFieldId());
            if (input == null) {
                return true;
            }
            if (input.getObjectDataType() != 1) {
                String value = filterTable.getFieldValueString() == null ? ""
                        : filterTable.getFieldValueString().trim();
                return DataUtil.validateInputFilter(value);
            }
        }
        return true;
    }

    public boolean validateInputFunction(List<FilterTable> lstFunctionTable) {
        for (FilterTable filterTable : lstFunctionTable) {
            for (FunctionParam functionParam : filterTable.getLstFunctionParams()) {
                if (functionParam.getValueType().intValue() == 1) {
                    String valuetemp = functionParam.getFieldValueString() == null ? ""
                            : functionParam.getFieldValueString().trim();
                    return DataUtil.validateInputFilter(valuetemp);
                }
            }
        }
        return true;
    }

    public boolean validateFilter() {

        if (!changeConditionFieldName()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notComplete"), "Condition");
            return false;
        }
        if (!checkDuplicate(lstConditionTable)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.table"), "field name", "condition");
            return false;
        }
        if (!validateInputCondition(lstConditionTable, mapInputObject)) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("valid.parameter"));
            return false;
        }
        if (!buildDataFunction(lstFunctionTable, 2)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notComplete"), "Function");
            return false;
        }
        if (!checkDuplicate(lstFunctionTable)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.table"), "function name", "Function");
            return false;
        }
        if (!validateInputFunction(lstFunctionTable)) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("valid.parameter"));
            return false;
        }
        return true;

    }

    public List<FilterTable> convertConditionFilterToList(String condition) {

        List<FilterTable> lstDataCondition = new ArrayList<>();
        if (!DataUtil.isStringNullOrEmpty(condition)) {
            Map<String, EvaluationInputObject> map = new HashMap<>();
            for (EvaluationInputObject inputObject : lstConditionData) {
                map.put(inputObject.getObjectName(), inputObject);
            }
            String[] rows = condition.split("&");
            for (int i = 0; i < rows.length; i++) {
                String row = rows[i];
                String[] fieldAndData = row.split("=");
                EvaluationInputObject inputObject = map.get(fieldAndData[0]);
                if (inputObject != null) {
                    FilterTable filterTable = new FilterTable();
                    filterTable.setFieldId(inputObject.getObjectId());
                    filterTable.setFieldName(inputObject.getObjectName());
                    filterTable.setFieldType(Integer.toUnsignedLong(inputObject.getObjectDataType()));
                    String value = fieldAndData[1];
                    switch (inputObject.getObjectDataType()) {
                        case 2:
                            filterTable.setFieldValueString(value);
                            break;
                        case 3:
                        case 4:
                            filterTable.setFieldValueNumber(Long.parseLong(value));
                            break;
                        case 5:
                            filterTable.setFieldValueDouble(Double.parseDouble(value));
                            break;
                        case 6:
                            if (value.equalsIgnoreCase("true")) {
                                filterTable.setFieldValueBoolean(2);
                            } else {
                                filterTable.setFieldValueBoolean(1);
                            }
                            break;

                    }

                    lstDataCondition.add(filterTable);
                }
            }

        }
        return lstDataCondition;
    }

    public List<FilterTable> convertFunctionFilterToList(String functionData, Integer type) {
        List<FilterTable> lstDataFunction = new ArrayList<>();
        if (!DataUtil.isStringNullOrEmpty(functionData)) {
            Map<String, Function> map = new HashMap<>();
            for (Function function : lstFunctionData) {
                map.put(function.getFunctionDisplay().substring(0, function.getFunctionDisplay().indexOf("(")) + "_" + function.getNumberParameter(), function);
            }

            String[] rows = functionData.split(":");
            for (int i = 0; i < rows.length; i++) {
                String row = rows[i];
                String functionName = row.split("\\(")[0];
                String argumentStr = DataUtil.getDataBetweenParenthesis(row);
                int paramNum = 0;
                String[] arg = argumentStr.split(",");
                if (!DataUtil.isStringNullOrEmpty(argumentStr)) {
                    paramNum = argumentStr.split(",").length;
                }
                Function function = map.get(functionName + "_" + paramNum);
                if (function == null) {
                    return lstDataFunction;
                }
                FilterTable filterTable = new FilterTable();
                filterTable.setFieldId(function.getFunctionId());
                filterTable.setFieldName(function.getFunctionDisplay());

                List<FunctionParam> lstParams = preprocessUnitServiceImpl.getLstFunctionParam(function.getFunctionId());
                for (FunctionParam functionParam : lstParams) {
                    String value = arg[lstParams.indexOf(functionParam)];
                    switch (functionParam.getValueType().intValue()) {
                        case 1:
                            functionParam.setFieldValueString(value);
                            break;
                        case 2:
                            functionParam.setFieldValueNumber(Long.parseLong(value));
                            break;
                        case 3:
                            functionParam.setFieldValueDate(DateUtils.stringToDate(value, DATETIME_PPU_ZONE));
                            break;
                        case 4:
                            functionParam.setFieldZoneId(Long.parseLong(value));
                            Zone zone = mapZone.get(functionParam.getFieldZoneId());
                            if (zone != null) {
                                functionParam.setFieldZoneName(zone.getZoneName());
                            }
                            break;
                        case 5:
                            functionParam.setFieldZoneMapId(Long.parseLong(value));
                            break;
                        case 6:
                            if (type == 2) {
                                functionParam.setFieldValueNumber(Long.parseLong(value));
                            } else if (type == 1) {
                                functionParam.setFieldValueDouble(Double.parseDouble(value));
                            }
                            break;
                    }
                }
                filterTable.setLstFunctionParams(lstParams);
                lstDataFunction.add(filterTable);
            }
        }
        return lstDataFunction;
    }

    public void onFieldConditionChange(FilterTable filterTable) {
        EvaluationInputObject input = evaluationInputObjectService.findOneById(filterTable.getFieldId());
        if (input != null) {
            filterTable.setFieldType(Integer.toUnsignedLong(input.getObjectDataType()));
            filterTable.setFieldName(input.getObjectName());
            switch (filterTable.getFieldType().intValue()) {
                case 2:
                    filterTable.setFieldValueString("");
                    break;
                case 3:
                case 4:
                    filterTable.setFieldValueNumber(null);
                    break;
                case 5:
                    filterTable.setFieldValueNumber(null);
                    break;
            }
            changeConditionFieldName();
        }
    }

    public boolean changeConditionFieldName() {
        StringBuilder conditionNew = new StringBuilder();
        for (FilterTable filter : lstConditionTable) {
            EvaluationInputObject input = mapInputObject.get(filter.getFieldId());
            if (input == null) {
                return false;
            }
            conditionNew.append("&").append(filter.getFieldName()).append("=");
            switch (filter.getFieldType().intValue()) {
                case 2:
                    if (DataUtil.isStringNullOrEmpty(filter.getFieldValueString())) {
                        return false;
                    }
                    conditionNew.append(filter.getFieldValueString() == null ? "" : filter.getFieldValueString());

                    break;
                case 3:
                case 4:
                    if (filter.getFieldValueNumber() == null) {
                        return false;
                    }
                    conditionNew.append(filter.getFieldValueNumber() == null ? "" : filter.getFieldValueNumber());

                    break;
                case 5:
                    if (filter.getFieldValueNumber() == null) {
                        return false;
                    }
                    conditionNew.append(filter.getFieldValueNumber() == null ? "" : filter.getFieldValueNumber());

                    break;
                default:
                    if (filter.getFieldValueBoolean() == -1) {
                        return false;
                    }
                    if (filter.getFieldValueBoolean() == 1) {
                        conditionNew.append("false");
                    }
                    if (filter.getFieldValueBoolean() == 2) {
                        conditionNew.append("true");
                    }

            }

        }
        path.setFilter("");
        path.setDataCondition(conditionNew.toString().replaceFirst("&", ""));
        return true;
    }

    public void prepareAddCondition() {
        FilterTable filterTable = new FilterTable();
        lstConditionTable.add(filterTable);
        changeConditionFieldName();

    }

    public void prepareDelateCondition(FilterTable filterTable) {

        lstConditionTable.remove(filterTable);
        changeConditionFieldName();
        if (lstConditionTable.isEmpty()) {
            path.setDataCondition("");
            path.setFilter("");
        }
    }

    public void changeFieldFunctionName(FilterTable filterTable) {
        Function input = mapFunction.get(filterTable.getFieldId());
        if (input == null) {
            filterTable.setLstFunctionParams(new ArrayList<>());
            return;
        }
        filterTable.setFieldType(input.getType());
        filterTable.setFieldName(input.getFunctionDisplay());
        filterTable.resetValue();
        filterTable.setLstFunctionParams(preprocessUnitServiceImpl.getLstFunctionParam(input.getFunctionId()));
        buildDataFunction(lstFunctionTable, 2);
    }

    public void changedFunctionTableName(FunctionTable functionTable) {
        Function input = mapFunction.get(functionTable.getFunctionId());
        if (input == null) {
            functionTable.setLstFunctionParam(new ArrayList<>());
            return;
        }
        functionTable.setFunctionId(input.getFunctionId());
        functionTable.setFunctionName(input.getFunctionDisplay());
        functionTable.setLstFunctionParam(preprocessUnitServiceImpl.getLstFunctionParam(input.getFunctionId()));
        buildDataFunctionForInputModeIsFunction(functions);
    }

    public void preapareDeleteFunctionTable(FunctionTable functionTable, int index) {
        if (functions.size() == 1) {
            functions.remove(functionTable);
            this.inputFieldVariableMap.setVariableValue("type:2|");
        }
        if (functions.size() != 1 && index == 0) {
            functions.removeAll(functions);
            this.inputFieldVariableMap.setVariableValue("type:2|");
        }
        if (functions.size() > 1) {
            functions.remove(functionTable);
            buildDataFunctionForInputModeIsFunction(functions);
        }

    }

    public void onFunctionChange() {
        if (inputFieldVariableMap.getIsInputMode() == 2) {
            buildDataFunctionForInputModeIsFunction(functions);
        } else {
            buildDataFunction(lstFunctionTable, 2);
        }

    }

    public void preapareAddFunction() {
        if (inputFieldVariableMap.getIsInputMode() == 1) {
            FilterTable filterTable = new FilterTable();
            filterTable.setLstFunction(lstFunctionData);
            lstFunctionTable.add(filterTable);
        } else {
            FunctionTable functionTable = new FunctionTable();
            functions.add(functionTable);

        }

    }

    public void preapareDeleteFunction(FilterTable filterTable) {

        lstFunctionTable.remove(filterTable);
        buildDataFunction(lstFunctionTable, 2);
        if (lstFunctionTable.isEmpty()) {
            path.setDataFunction("");
            path.setFilter("");
        }

    }

    public boolean validateFunctions(List<FunctionTable> lstFunctionTable) {
        for (FunctionTable filterTable : lstFunctionTable) {
            try {
                filterTable.getLstFunctionParam().isEmpty();
            } catch (Exception e) {
                return false;
            }
            for (FunctionParam functionParam : filterTable.getLstFunctionParam()) {
                if (functionParam.getValueType().intValue() == 1) {
                    String valuetemp = functionParam.getFieldValueString() == null ? ""
                            : functionParam.getFieldValueString().trim();
                    return DataUtil.validateInputFilter(valuetemp);
                }
            }
        }
        return true;
    }

    public boolean buildDataFunction(List<FilterTable> lstFunctionTable, long type) {
        String str = "";
        for (FilterTable filterTable : lstFunctionTable) {
            Function input = mapFunction.get(filterTable.getFieldId());
            if (input == null) {
                return false;
            }
            String value = "";
            for (FunctionParam functionParam : filterTable.getLstFunctionParams()) {
                switch (functionParam.getValueType().intValue()) {
                    case 1:
                        String valuetemp = functionParam.getFieldValueString() == null ? ""
                                : functionParam.getFieldValueString().trim();
                        if (DataUtil.isStringNullOrEmpty(valuetemp)) {
                            return false;
                        }
                        value += "," + valuetemp;
                        break;
                    case 2:
                        Long valuetemp2 = functionParam.getFieldValueNumber();
                        if (valuetemp2 == null) {
                            return false;
                        }
                        value += "," + valuetemp2;
                        break;
                    case 3:
                        String valuetempDate = DateUtils.formatDatetoString(functionParam.getFieldValueDate(), DATETIME_PPU_ZONE);
                        if (DataUtil.isStringNullOrEmpty(valuetempDate)) {
                            return false;
                        }
                        value += "," + valuetempDate;
                        break;
                    case 4:
                        Long zone = functionParam.getFieldZoneId();
                        if (zone == null || zone == -1L || zone == 0) {
                            return false;
                        }
                        value += "," + zone;
                        break;
                    case 5:
                        Long zoneMapId = functionParam.getFieldZoneMapId();
                        if (zoneMapId == null || zoneMapId == -1L || zoneMapId == 0) {
                            return false;
                        }
                        value += "," + zoneMapId;
                        break;
                    case 6:
                        Long valuetemp3 = functionParam.getFieldValueNumber();
                        if (valuetemp3 == null) {
                            return false;
                        }
                        value += "," + valuetemp3;

                        break;
                }
            }
            str += ":" + filterTable.getFieldName().split("\\(")[0] + "(" + value.replaceFirst(",", "") + ")";
        }
        if (type == 2) {
            this.path.setDataFunction(str.replaceFirst(":", ""));
        }
        return true;
    }

    public void changePath(Path path, int index) {
        changePathRowInTable(path, 1);

    }

    public void changeFunctionName() {
        StringBuilder str = new StringBuilder();
        for (FilterTable filter : lstFunctionTable) {
            str.append(":").append(filter.getFieldName());
            str.append("(");
            for (FunctionParam functionParam : filter.getLstFunctionParams()) {
                switch (functionParam.getValueType().intValue()) {
                    case 1:
                        str.append(functionParam.getFieldValueString()).append(",");
                        break;
                    case 2:
                        str.append(functionParam.getFieldValueNumber()).append(",");
                        break;
                    case 3:
                        str.append(functionParam.getFieldValueDate().toString()).append(",");
                        break;
                    case 4:
                        str.append(functionParam.getFieldZoneId()).append(",");
                        break;
                    case 5:
                    case 6:
                        str.append(functionParam.getFieldValueNumber()).append(",");
                        break;
                }
            }
            str.append(")");

        }
        path.setDataFunction(str.toString().replaceFirst(":", ""));
    }

    public void genFunctionTable(String value1) {
        functions = new ArrayList<>();
        String[] lstFunctionPath = value1.split("\\|")[1].split(":");

        for (Function function : lstFunctionData) {
            mapFunctionsType2.put(function.getFunctionDisplay().substring(0, function.getFunctionDisplay().indexOf("(")) + "_" + function.getNumberParameter(), function);
        }
        for (Function function : lstFunctionDataTypeOne) {
            mapFunctionsType1.put(function.getFunctionDisplay().substring(0, function.getFunctionDisplay().indexOf("(")) + "_" + function.getNumberParameter(), function);
        }
        for (int i = 0; i < lstFunctionPath.length; i++) {
            String argumentStr = DataUtil.getDataBetweenParenthesis(lstFunctionPath[i]);
            String arg[] = argumentStr.split(",");
            int paramNum = 0;
            if (!DataUtil.isStringNullOrEmpty(argumentStr)) {
                paramNum = argumentStr.split(",").length;
            }
            Function function = new Function();
            if (i == 0) {
                function = mapFunctionsType1.get(lstFunctionPath[i].split("\\(")[0] + "_" + paramNum);
            }
            if (i > 0) {
                function = mapFunctionsType2.get(lstFunctionPath[i].split("\\(")[0] + "_" + paramNum);
            }
            FunctionTable functionTable = new FunctionTable();
            List<FunctionParam> lstParams = new ArrayList<>();
            if (function != null) {
                functionTable.setFunctionName(function.getFunctionDisplay());
                functionTable.setFunctionId(function.getFunctionId());
                lstParams = preprocessUnitServiceImpl.getLstFunctionParam(function.getFunctionId());
            }
            for (FunctionParam functionParam : lstParams) {
                String value = arg[lstParams.indexOf(functionParam)];
                switch (functionParam.getValueType().intValue()) {
                    case 1:
                        functionParam.setFieldValueString(value);
                        break;
                    case 2:
                        functionParam.setFieldValueNumber(Long.parseLong(value));
                        break;
                    case 3:
                        functionParam.setFieldValueDate(DateUtils.stringToDate(value, DATETIME_PPU_ZONE));
                        break;
                    case 4:
                        functionParam.setFieldZoneId(Long.parseLong(value));
                        Zone zone = mapZone.get(functionParam.getFieldZoneId());
                        if (zone != null) {
                            functionParam.setFieldZoneName(zone.getZoneName());
                        }
                        break;
                    case 5:
                        functionParam.setFieldZoneMapId(Long.parseLong(value));
                        break;
                    case 6:
                        functionParam.setFieldValueDouble(Double.parseDouble(value));
                        break;
                }
            }
            functionTable.setLstFunctionParam(lstParams);
            functions.add(functionTable);

        }
    }

    public boolean buildDataFunctionForInputModeIsFunction(List<FunctionTable> lstFunctionTable) {
        String str = "";
        for (FunctionTable function : lstFunctionTable) {
            Function input = mapFunction.get(function.getFunctionId());
            if (input == null) {
                return false;
            }
            String value = "";
            for (FunctionParam functionParam : function.getLstFunctionParam()) {
                switch (functionParam.getValueType().intValue()) {
                    case 1:
                        String valuetemp = functionParam.getFieldValueString() == null ? ""
                                : functionParam.getFieldValueString().trim();
                        if (DataUtil.isStringNullOrEmpty(valuetemp)) {
                            return false;
                        }
                        value += "," + valuetemp;
                        break;
                    case 2:
                        Long valuetemp2 = functionParam.getFieldValueNumber();
                        if (valuetemp2 == null) {
                            return false;
                        }
                        value += "," + valuetemp2;
                        break;
                    case 3:
                        String valuetempDate = DateUtils.formatDatetoString(functionParam.getFieldValueDate(), DATETIME_PPU_ZONE);
                        if (DataUtil.isStringNullOrEmpty(valuetempDate)) {
                            return false;
                        }
                        value += "," + valuetempDate;
                        break;
                    case 4:
                        Long zone = functionParam.getFieldZoneId();
                        if (zone == null || zone == -1 || zone == 0) {
                            return false;
                        }
                        value += "," + zone;
                        break;
                    case 5:
                        Long zoneMapId = functionParam.getFieldZoneMapId();
                        if (zoneMapId == null || zoneMapId == -1 || zoneMapId == 0) {
                            return false;
                        }
                        value += "," + zoneMapId;
                        break;
                    case 6:
                        Double valuetemp3 = functionParam.getFieldValueDouble();
                        if (valuetemp3 == null) {
                            return false;
                        }
                        value += "," + valuetemp3;
                        break;
                }
            }
            str += ":" + function.getFunctionName().split("\\(")[0] + "(" + value.replaceFirst(",", "") + ")";
        }
        String replace = str.replaceFirst(":", "");
        StringBuilder news = new StringBuilder("type:2|");
        news.append(replace);
        this.inputFieldVariableMap.setVariableValue(news.toString());
        return true;
    }

    public void ChangeDataFunctionForInputModeIsFunction() {
        StringBuilder str = new StringBuilder();
        str.append("type:2|");
        for (FunctionTable obj : functions) {
            obj.getFunctionName();
            for (FunctionParam param : obj.getLstFunctionParam()) {

            }
        }
    }

    public void reset() {
        if (editMode) {
            lstProcessParamToDelete = new ArrayList<>();
            lstProcessValueNew = new ArrayList<>();
            processValueToDelete = new ProcessValue();
            lstProcessValueToDelete = new ArrayList<>();
            this.ppuEvaluation = ppuEvaluationService.findOneById(ppuEvaluation.getPreProcessId());
            lstInputFieldVariableMap = inputFieldVariableMapService.getLstInputFieldVariableMapByTypeAndId(2, ppuEvaluation.getPreProcessId());
            lstProcessValue = processValueService.getLstProcessValue(ppuEvaluation.getPreProcessId());
            lstProcessParam = ppuEvaluationService.getListProcessParam(ppuEvaluation.getPreProcessId());
            for (ProcessParam obj : lstProcessParam) {
                obj.getParam();
            }
            initTreeNode();
            mapTreeStatus(rootNode);
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
        } else {
            prepareAddPPUEvaluation();
        }

    }

    public void showMessage(boolean result) {
        if (result) {
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.fail"));
        }
    }

    public void actionSuccess() {
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
    }

    public void duplidateMessage(String fieldName) {
        errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), fieldName);
    }

    public void notCompleteMessage(String fieldName) {
        errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notComplete"), fieldName);
    }

    public boolean validInputField(String value, String fieldName, boolean isRequire, boolean isCheckMaxlength, boolean isCheckPercent) {
        if (isRequire && !validRequireField(value, fieldName)) {
            return false;
        }
        if (isCheckMaxlength && !DataUtil.checkMaxlength(value)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), fieldName);
            return false;
        }
        if (isCheckPercent && !DataUtil.checkNotContainPercentage(value)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), fieldName);
            return false;
        }
        return true;
    }

    public boolean validRequireField(String value, String fieldName) {
        if (DataUtil.isStringNullOrEmpty(value)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), fieldName);
            return false;
        }
        return true;
    }

    public boolean validRequireField(Long value, String fieldName) {
        if (value == null || value == 0) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), fieldName);
            return false;
        }
        return true;
    }

    public boolean validRequireField(Integer value, String fieldName) {
        if (value == null || value == 0) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), fieldName);
            return false;
        }
        return true;
    }

    public boolean validRequireFieldList(List<?> value, String fieldName) {
        if (value == null || value.isEmpty()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), fieldName);
            return false;
        }
        return true;
    }

    public void exitsMessage(String fieldName) {
        errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), fieldName);
    }

    public void changeInputMode() {
        if (isObject) {
            for (InputFieldVariableMap item : lstInputFieldVariableMap) {
                if (item != null) {
                    if (item.getId().equals(inputFieldVariableMap.getId())) {
                        inputFieldVariableMap.setVariableValue(item.getVariableValue());
                        inputFieldVariableMap.setInputMode(item.getInputMode());
                        genPathTable(item.getVariableValue());
                    }
                    if (item.getId().equals(inputFieldVariableMap.getId()) && inputFieldVariableMap.getIsInputMode() == 2) {
                        inputFieldVariableMap.setVariableValue("type:2|");
                    }
                    if (item.getId().equals(inputFieldVariableMap.getId()) && inputFieldVariableMap.getIsInputMode() == 1 && "".equals(item.getVariableValue())) {
                        inputFieldVariableMap.setVariableValue("type:1|");
                    }
                }
            }
            functions = new ArrayList<>();
        }
        if (!isObject) {
            for (InputFieldVariableMap item : lstInputFieldVariableMap) {
                if (item != null) {
                    if (item.getId().equals(inputFieldVariableMap.getId())) {
                        inputFieldVariableMap.setVariableValue(item.getVariableValue());
                        inputFieldVariableMap.setInputMode(item.getInputMode());
                        genFunctionTable(item.getVariableValue());
                    }
                    if (item.getId().equals(inputFieldVariableMap.getId()) && inputFieldVariableMap.getIsInputMode() == 1) {
                        inputFieldVariableMap.setVariableValue("type:1|");
                    }
                    if (item.getId().equals(inputFieldVariableMap.getId()) && inputFieldVariableMap.getIsInputMode() == 2 && "".equals(item.getVariableValue())) {
                        inputFieldVariableMap.setVariableValue("type:2|");
                    }
                }
            }
            lstPath = new ArrayList<>();
        }
    }

    public void onsaveVariableDefine() {
        if (this.lstPath.isEmpty() && inputFieldVariableMap.getIsInputMode() == 1) {
            inputFieldVariableMap.setVariableValue("");
        }
        if (this.functions.isEmpty() && inputFieldVariableMap.getIsInputMode() == 2) {
            inputFieldVariableMap.setVariableValue("");
        }

        for (InputFieldVariableMap item : lstInputFieldVariableMap) {
            if (item.getId().equals(inputFieldVariableMap.getId())) {
                item.setId(inputFieldVariableMap.getId());
                item.setOwnerId(inputFieldVariableMap.getOwnerId());
                item.setType(inputFieldVariableMap.getType());
                item.setVariableName(inputFieldVariableMap.getVariableName());
                item.setVariableValue(inputFieldVariableMap.getVariableValue());
            }
        }
    }

    public boolean onSaveInputFieldVariable() {
        if (lstPath.isEmpty() && inputFieldVariableMap.getIsInputMode() == 1) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Path Table");
            return false;
        }
        if (functions.isEmpty() && inputFieldVariableMap.getIsInputMode() == 2) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Function Table");
            return false;
        }
        if (!validInputField(inputFieldVariableMap.getVariableName(), "Variable Name", true, true, true)) {
            return false;
        }
        if (!validInputField(inputFieldVariableMap.getVariableValue(), "Path", true, true, false)) {
            return false;
        }
        if (inputFieldVariableMap.getIsInputMode() == 1) {
            for (Path obj : lstPath) {
                if (obj.getInputObject().getObjectId() == null || obj.getInputObject().getObjectId() == -1) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notComplete"), "Path Table");
                    return false;
                }
            }
        }
        if (inputFieldVariableMap.getIsInputMode() == 2) {
            for (FunctionTable obj : functions) {
                if (obj.getFunctionId() == null || obj.getFunctionId() == -1) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notComplete"), "Function");
                    return false;
                }
            }
        }
        if (!validateFunctions(functions)) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("valid.parameter"));
            return false;
        }
        if (inputFieldVariableMap.getIsInputMode() == 2) {
            if (!validateFunctions()) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notComplete"), "Function");
                return false;
            }
            if (isDuplicateFunctions()) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.table"), "function name", "Function");
                return false;
            }
        }

        return true;
    }

    public boolean isDuplicateFunctions() {
        List<FunctionTable> newList = new ArrayList<>();
        newList.addAll(functions);
        Collections.sort(newList, new SortFunctions());
        long id = -1;
        for (FunctionTable obj : newList) {
            if (id == obj.getFunctionId()) {
                return true;
            }
            id = obj.getFunctionId();
        }
        return false;
    }

    public boolean validateFunctions() {
        if (!buildDataFunctionForInputModeIsFunction(functions)) {
            return false;
        }
        return true;
    }

}
