package vn.viettel.campaign.controller;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dto.NotifyContentDTO;
import vn.viettel.campaign.dto.NotifyTemplateDetailDTO;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.*;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.Objects;
import java.util.*;

import vn.viettel.campaign.dao.LanguageDAOInterface;
import vn.viettel.campaign.service.impl.TreeServiceImpl;
import vn.viettel.campaign.validate.CheckOnEdit;

/**
 * @author ConKC
 */
@ManagedBean
@ViewScoped
public class NotifyTemplateController extends BaseController implements Serializable {

    private TreeNode rootNode;
    private TreeNode selectedNode;
    private Boolean isUpdate;
    private boolean isDisplay;
    private boolean actonSwitch;
    private List<Category> categories;
    public Map<Long, Category> mapCategory;
    private Category category;
    private Boolean isUpdateContent;
    private NotifyTemplateDetailDTO templateDetail;
    private NotifyContentDTO contentRequest;
    private List<Language> langSelectItems;
    private boolean selectOnCategory;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private NotifyTemplateService notifyTemplateService;

    @Autowired
    private UtilsService utilsService;

    @Autowired
    private UssdScenarioService ussdScenarioService;

    @Autowired
    private NotifyTriggerService notifyTriggerService;

    @Autowired
    private RolesObjectsService rolesObjectsService;

    @Autowired
    private LanguageDAOInterface languageDAOInterface;

    @Autowired
    private TreeServiceImpl treeService;

    @PostConstruct
    public void init() {
        this.actonSwitch = true;
        this.isDisplay = false;
        this.category = new Category();
        this.categories = new ArrayList<>();
        this.isUpdate = false;
        this.isUpdateContent = false;
        templateDetail = NotifyTemplateDetailDTO.builder().build();
        contentRequest = NotifyContentDTO.builder().build();
        langSelectItems = new ArrayList<>();
        mapCategory = new HashMap<>();
        initTreeNode();
    }

    public void initTreeNode() {
        mapCategory = new HashMap<>();
        categories.forEach(e -> {
            mapCategory.put(e.getCategoryId(), e);
        });
        List<Category> categoryTemp = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_NOTIFY_TEMPLATE_TYPE);
        categories.clear();
        categoryTemp.forEach(e -> {
            if (mapCategory.get(e.getCategoryId()) != null) {
                categories.add(mapCategory.get(e.getCategoryId()));
            } else {
                categories.add(e);
            }
        });
        rootNode = notifyTemplateService.getTree(categories);
        addExpandedNode(rootNode.getChildren().get(0));
    }

    public void prepareCreateCategory() {
        this.isUpdate = false;
        this.category = new Category();
        this.category.setCategoryType(Constants.CatagoryType.CATEGORY_NOTIFY_TEMPLATE_TYPE);
        Category parentCat = (Category) selectedNode.getData();
        this.category.setParentId(parentCat.getCategoryId());
        Long id = ussdScenarioService.getNextSequense(Constants.TableName.CATEGORY);
        category.setCategoryId(id);
    }

    public void prepareEditCategory() {
        this.isUpdate = true;
        if (Objects.nonNull(selectedNode)) {
            Category cat = (Category) selectedNode.getData();
            this.category = categoryService.getCategoryById(cat.getCategoryId());
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void doSaveOrUpdateCategory() {
        if (validateCategory()) {
            categoryService.save(category);
            refreshTree();
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public boolean validateCategory() {
        
        if (DataUtil.isStringNullOrEmpty(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Category name");
            return false;
        }
        
        if (!DataUtil.checkNotContainPercentage(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Category name");
            return false;
        }
        boolean rs = true;
        if (!categoryService.checkDuplicate(category.getName(), category.getCategoryId(), category.getCategoryType())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.category.exist"));
            rs = false;
        }
        return rs;
//        if (DataUtil.isStringNullOrEmpty(this.category.getName())) {
//            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category name");
//            return false;
//        }
//        if (!DataUtil.checkMaxlength(this.category.getName())) {
//            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Category name");
//            return false;
//        }
//        if (!categoryService.checkDuplicate(category.getName(), category.getCategoryId(), category.getCategoryType())) {
//            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.category.exist"));
//            return false;
//        }
//        return true;
    }

    private void refreshTree() {
        this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_NOTIFY_TEMPLATE_TYPE);
        this.initTreeNode();
        updateCategoryInforNotifyTemplate();
        processRefreshCategory(rootNode, selectedNode, category, isUpdate);
    }

    public void doDeleteCategory() {
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();

            if (Objects.isNull(category) || Objects.isNull(category.getParentId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.delete.category.root"));
                return;
            }

            if (notifyTemplateService.checkExistingTemplateByCategoryId(category.getCategoryId())) {
//                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.category.children"));
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
                return;
            }

            if (categoryService.checkDeleteCategory(category.getCategoryId())) {
                categoryService.deleteCategory(category);
                rolesObjectsService.deleteRoleObjectByObjectId(category.getCategoryId());
                selectedNode.getChildren().clear();
                TreeNode parNode = selectedNode.getParent();

                selectedNode.getParent().getChildren().remove(selectedNode);
                if (parNode.getChildren().isEmpty()) {
                    removeExpandedNode(parNode);
                }
                selectedNode.setParent(null);
                selectedNode = null;
                categories.remove(category);
                initTreeNode();
                mapTreeStatus(rootNode);
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
                updateCategoryInforNotifyTemplate();
            } else {
//                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.category.children"));
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void prepareAddObject() {
        try {
            this.actonSwitch = true;
            this.isDisplay = true;
            this.isUpdate = false;
            Category cat = (Category) this.selectedNode.getData();
            Long nextId = ussdScenarioService.getNextSequense(Constants.TableName.NOTIFY_TEMPLATE);
            this.templateDetail = NotifyTemplateDetailDTO.builder()
                    .id(nextId)
                    .categoryId(cat.getCategoryId())
                    .contents(new ArrayList<>())
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void prepareAddContent() {
        try {
            this.isUpdateContent = false;
            Long nextId = ussdScenarioService.getNextSequense(Constants.TableName.NOTIFY_CONTENT);
            langSelectItems = notifyTemplateService.getLanguages();
            this.contentRequest = NotifyContentDTO.builder().id(nextId).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean validateContent() {
        if (DataUtil.isStringNullOrEmpty(this.contentRequest.getContent())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Content");
            return false;
        }

        if (!DataUtil.checkMaxlength(this.contentRequest.getContent(), 255)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Content");
            return false;
        }

        if (!DataUtil.checkNotContainPercentage(this.contentRequest.getContent())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Content");
            return false;
        }

        if (Objects.isNull(this.contentRequest.getLanguageId())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Language");
            return false;
        }

        if (!templateDetail.getContents().isEmpty()
                && !isUpdateContent
                && templateDetail.getContents()
                .stream()
                .anyMatch(item -> DataUtil.safeEqual(item.getLanguageId(), contentRequest.getLanguageId()))) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.in.list"), "Language");
            return false;
        }

        if (!templateDetail.getContents().isEmpty()
                && isUpdateContent
                && templateDetail.getContents()
                .stream()
                .filter(item -> !DataUtil.safeEqual(item.getId(), contentRequest.getId()))
                .anyMatch(item -> DataUtil.safeEqual(item.getLanguageId(), contentRequest.getLanguageId()))) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.in.list"), "Language");
            return false;
        }
        return true;
    }

    public void saveContent() {
        Language language = languageDAOInterface.findById(Language.class, contentRequest.getLanguageId());
        if (isUpdateContent) {
            templateDetail.getContents().forEach(item -> {
                if (DataUtil.safeEqual(item.getId(), contentRequest.getId())) {
                    item.setContent(contentRequest.getContent());
                    item.setLanguageId(contentRequest.getLanguageId());
                    item.setLanguage(language);
                }
            });
        } else {
            NotifyContentDTO contentDTO = NotifyContentDTO.builder()
                    .id(contentRequest.getId())
                    .content(contentRequest.getContent())
                    .notifyTemplateId(templateDetail.getId())
                    .languageId(contentRequest.getLanguageId())
                    .language(language)
                    .isContentNew(Boolean.TRUE)
                    .build();
            templateDetail.getContents().add(contentDTO);
        }
    }

    public void onDeleteContent(NotifyContentDTO contentDTO) {
        if (Objects.nonNull(contentDTO)) {
            this.templateDetail.getContents().remove(contentDTO);
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void prepareEditContent(NotifyContentDTO contentDTO) {
        this.isUpdateContent = true;
        langSelectItems = notifyTemplateService.getLanguages();
        if (Objects.nonNull(contentDTO)) {
            this.contentRequest = contentDTO;
        } else {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public boolean validateTemplate() {
        if (DataUtil.isStringNullOrEmpty(templateDetail.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Template name");
            return false;
        }
        if (!DataUtil.checkMaxlength(templateDetail.getName(), 255)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Template name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(templateDetail.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Template name");
            return false;
        }

        if (isUpdate && notifyTemplateService.findByName(templateDetail.getName(), templateDetail.getId()) != null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Template name");
            return false;
        }

        if (!isUpdate && notifyTemplateService.findByName(templateDetail.getName(), null) != null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Template name");
            return false;
        }

        if (Objects.isNull(templateDetail.getCategoryId())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category");
            return false;
        }

        if (!DataUtil.checkMaxlength(templateDetail.getDescription(), 255)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(templateDetail.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Description");
            return false;
        }
        if (templateDetail.getContents() == null || templateDetail.getContents().size() < 1) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Content");
            return false;
        } else {
            for (NotifyContentDTO notifyContentDTO : templateDetail.getContents()) {
                //
                List<String> lstParam = splitContent(notifyContentDTO);
                if (!DataUtil.isNullOrEmpty(lstParam)) {
                    for (String param : lstParam) {
                        if (!DataUtil.checkMaxlength(param, 45)) {
                            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength.1"), "Param content", "45");
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    public List<String> splitContent(NotifyContentDTO notifyContentDTO) {
        List<String> lstParamString = new ArrayList<>();
        String content = notifyContentDTO.getContent();
        content = content.replaceAll("\\r\\n", " ");
        if (!DataUtil.isNullOrEmpty(content) && content.contains("{") && content.contains("}")) {
//                    List<String> strings = Arrays.asList(content.replaceAll("^.*?\\{", "").split("}.*?(\\{|$)"));
            List<String> strings = getListParam(content);
            if (!DataUtil.isNullOrEmpty(strings)) {
                for (String item : strings) {
//                            item = "" + item + "";
                    if (!lstParamString.contains(item)) {
                        lstParamString.add(item);
                    }
                }

            }
        }
        return lstParamString;
    }

    public List<String> getListParam(String chuoi) {
        List<String> listParam = new ArrayList();
        try {
            byte ptext[] = chuoi.getBytes("ISO-8859-1");
            chuoi = new String(ptext, "UTF-8");

            byte bStart[] = "{".getBytes("ISO-8859-1");
            String strStart = new String(bStart, "UTF-8");

            byte bEnd[] = "}".getBytes("ISO-8859-1");
            String strEnd = new String(bEnd, "UTF-8");

            Integer start = null;
            Integer end = null;
            for (int i = 0; i < chuoi.length(); i++) {
                String str = chuoi.substring(i, i + 1);
                if (strStart.equals(str)) {
                    start = i;
                }

                if (start != null && strEnd.equals(str)) {
                    end = i;
                    listParam.add(chuoi.substring(start + 1, end));
                    start = null;
                    end = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listParam;
    }

    public void prepareEditTemp() {
        if (Objects.nonNull(this.selectedNode)) {
            NotifyTemplate template = (NotifyTemplate) this.selectedNode.getData();
            if (Objects.nonNull(template)) {
                this.isDisplay = true;
                this.actonSwitch = false;
                this.isUpdate = true;
                this.templateDetail = notifyTemplateService.getDetail(template.getId());
            } else {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void saveTemplate() {
        NotifyTemplate template = new NotifyTemplate();
        template.setId(templateDetail.getId());
        template.setName(templateDetail.getName().trim());
        template.setCategoryId(templateDetail.getCategoryId());
        template.setDescription(templateDetail.getDescription());
        notifyTemplateService.saveTemplate(template);
        updateContent(templateDetail.getContents());
        this.isUpdate = true;
        initTreeNode();
        Category cat = categoryService.getCategoryById(templateDetail.getCategoryId());
        TreeNode note = findCategoryInTree(rootNode, cat);
        if (note != null) {

            expandCurrentNode(note);
            mapTreeStatus(rootNode);
        }
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
        
        this.actonSwitch = false;
    }

    public void prepareEditWhenClick() {
        CheckOnEdit.onEdit = "notify_template";
        onNodeSelect();
        this.actonSwitch = false;
    }

    public void prepareEditFromContextMenu() {
        CheckOnEdit.onEdit = "notify_template";
        onNodeSelect();
        this.actonSwitch = true;

    }

    public void onNodeSelect() {
        this.selectOnCategory = false;
        this.isUpdate = true;
        this.actonSwitch = false;
        if ("category".equals(selectedNode.getType())) {
            prepareEditCategory();
            this.selectOnCategory = true;
            PrimeFaces.current().executeScript("PF('carDialog').show();");
        } else {
            NotifyTemplate template = (NotifyTemplate) selectedNode.getData();
            this.isDisplay = true;
            this.templateDetail = notifyTemplateService.getDetail(template.getId());
        }
    }

    public void onDeleteTemp() {
        if (Objects.nonNull(selectedNode)) {
            NotifyTemplate template = (NotifyTemplate) selectedNode.getData();
            if (Objects.nonNull(template)) {
                List<NotifyTrigger> triggers = notifyTriggerService.getByNotifyTemplateId(template.getId());
                if (!triggers.isEmpty()) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notify.trigger"));
                    return;
                }
                this.isDisplay = false;
                if (deleteObject(template.getId())) {
                    successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
                    TreeNode parentNode = selectedNode.getParent();
                    parentNode.getChildren().remove(selectedNode);
                    if (parentNode.getChildren().size() == 0) {
                        removeExpandedNode(parentNode);
                    }
                    initTreeNode();
                    mapTreeStatus(rootNode);
                } else {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.fail"));
                }
            } else {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public boolean deleteObject(Long templateId) {
        try {
            notifyTemplateService.removeTemplateById(templateId);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void revertTemplate() {
        if (isUpdate && Objects.nonNull(this.selectedNode)) {
            NotifyTemplate template = (NotifyTemplate) this.selectedNode.getData();
            if (Objects.nonNull(template)) {
                this.templateDetail = notifyTemplateService.getDetail(template.getId());
            }
        } else {
            prepareAddObject();
        }
    }

    private void updateContent(List<NotifyContentDTO> contentDTOS) {
        if (contentDTOS.isEmpty()) {
            return;
        }
        List<NotifyContent> contents = new ArrayList<>();
        contentDTOS.stream().map((contentDTO) -> {
            NotifyContent content = new NotifyContent();
            content.setId(contentDTO.getId());
            content.setContent(contentDTO.getContent());
            content.setLanguage(contentDTO.getLanguage());
            content.setNotifyTemplateId(contentDTO.getNotifyTemplateId());
            return content;
        }).forEach((content) -> {
            contents.add(content);
        });
        notifyTemplateService.updateNotifyContent(templateDetail.getId(), contents);
    }

    public List<Language> getLangSelectItems() {
        return langSelectItems;
    }

    public void setLangSelectItems(List<Language> langSelectItems) {
        this.langSelectItems = langSelectItems;
    }

    public NotifyTemplateDetailDTO getTemplateDetail() {
        return templateDetail;
    }

    public void setTemplateDetail(NotifyTemplateDetailDTO templateDetail) {
        this.templateDetail = templateDetail;
    }

    public NotifyContentDTO getContentRequest() {
        return contentRequest;
    }

    public void setContentRequest(NotifyContentDTO contentRequest) {
        this.contentRequest = contentRequest;
    }

    public boolean isIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(boolean isDisplay) {
        this.isDisplay = isDisplay;
    }

    public boolean isActonSwitch() {
        return actonSwitch;
    }

    public void setActonSwitch(boolean actonSwitch) {
        this.actonSwitch = actonSwitch;
    }

    public Boolean getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(Boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    public TreeNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode rootNode) {
        this.rootNode = rootNode;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean getIsUpdateContent() {
        return isUpdateContent;
    }

    public void setIsUpdateContent(Boolean isUpdateContent) {
        this.isUpdateContent = isUpdateContent;
    }

    public boolean isSelectOnCategory() {
        return selectOnCategory;
    }

    public void setSelectOnCategory(boolean selectOnCategory) {
        this.selectOnCategory = selectOnCategory;
    }
}
