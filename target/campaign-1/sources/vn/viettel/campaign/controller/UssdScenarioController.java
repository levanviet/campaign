/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dao.NoteContentDAOInterface;
import vn.viettel.campaign.dao.ScenarioNodeDAOInterface;
import vn.viettel.campaign.dto.ConditionTableDTO;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.*;
import vn.viettel.campaign.service.impl.TreeServiceImpl;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import java.io.Serializable;
import java.util.Objects;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import org.primefaces.PrimeFaces;
import vn.viettel.campaign.validate.CheckOnEdit;

/**
 * @author ConKC
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
public class UssdScenarioController extends BaseController implements Serializable {

    private boolean isDisplay;
    private boolean showDetail;
    private boolean actonSwitch = false;
    private boolean addRoot = true;
    private TreeNode rootNode;
    private TreeNode rootOcsNode;
    private TreeNode rootScenarioNode;
    private TreeNode preprocessUnitRootNode;
    private TreeNode ruleRootNode;
    private TreeNode selectedNode;
    private TreeNode selectedOcsNode;
    private TreeNode selectedScenarioNode;
    private TreeNode selectedNodeTmp;
    private TreeNode selectedPreprocessUnitNode;
    private TreeNode selectedRuleNode;
    private List<Category> categories;
    private List<UssdScenario> ussdScenarios;
    private List<OcsBehaviour> ocsBehaviours;
    private List<Language> languages;
    private Category category;
    private UssdScenario ussdScenario;
    private ScenarioNode scenarioNode;
    private boolean isAdd;
    private ScenarioNode scenarioNodeTmp;
    private NoteContent noteContent;
    private List<ScenarioNode> lstScenarioNodeDelete;
    private Boolean isUpdate;
    private Boolean addContent;
    private TreeNode root;
    private Segment segment;
    private List<Segment> lstSeg;
    private List<Segment> lstSegFiltered;
    private String searchSeg;
    private Rule ruleSelected;
    private List<Rule> lstRule;
    private String searchRule;
    private Invitation invitation;
    private List<Invitation> lstInvi;
    private List<BlackList> lstBlackList;
    private List<SpecialProm> lstSpecialProm;
    private String searchInvi;
    private String action;
    private String messageConfirm = "Are you sure to create this Category?";
    private String datePattern = "dd/MM/yyyy";
    private Map<Long, Segment> mapKeySegment = new HashMap<>();
    private List<Segment> lstTreeSegment = new ArrayList<>();
    private Rule createNewRule = new Rule();
    private List<CdrService> cdrServicesTable = new ArrayList<>();
    private List<ResultTable> resultTable = new ArrayList<>();
    private List<CdrService> cdrServicesAll = new ArrayList<>();
    private List<ResultTable> resultTableAllData = new ArrayList<>();
    private DualListModel<CdrService> cdrServicePickingList = new DualListModel<>();
    private DualListModel<ResultTable> resultTablePickingList = new DualListModel<>();
    private boolean applyAllCdrToRule = false;
    List<Category> ruleCategories = new ArrayList<>();
    public Map<Long, TreeNode> treeNodeExpandedScenario = new HashMap<>();

    private UploadedFile file;
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private TreeServiceImpl treeService;

    @Autowired
    private CampaignOnlineService campaignOnlineService;

    @Autowired
    SpecialPromService specialPromServiceImpl;

    @Autowired
    private UtilsService utilsService;
    @Autowired
    private ScenarioNodeDAOInterface scenarioNodeDAO;

    @Autowired
    private UssdScenarioService ussdScenarioService;

    @Autowired
    OCSBehaviourInterface oCSBehaviourServiceImpl;

    @Autowired
    private NoteContentDAOInterface noteContentDAO;

    @PostConstruct
    public void init() {
        this.isDisplay = false;
        this.category = new Category();
        this.noteContent = new NoteContent();
        this.categories = new ArrayList<>();
        this.isUpdate = false;
//        messageConfirm = "Are you sure to create this Category?";
        this.root = new DefaultTreeNode(null, null);
        this.ocsBehaviours = ussdScenarioService.getAllOcsBehaviour();
        this.languages = ussdScenarioService.getAllLanguage();
        Collections.sort(languages);
        initTreeNode();

    }

    public void initTreeNode() {
        this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_USSD_SCENARIO_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.ussdScenarios = categoryService.getUssdScenarioByCategory(longs);
        rootNode = treeService.createUssdScenarioTree(categories, ussdScenarios);
        addExpandedNode(rootNode.getChildren().get(0));
    }

    public void prepareCreateCategory() {
        this.isUpdate = false;
//        messageConfirm = "Are you sure to create this Category?";
        this.category = new Category();
        this.category.setCategoryType(Constants.CatagoryType.CATEGORY_USSD_SCENARIO_TYPE);
        Category parentCat = (Category) selectedNode.getData();
        this.category.setParentId(parentCat.getCategoryId());
        Long id = ussdScenarioService.getNextSequense(Constants.TableName.CATEGORY);
        category.setCategoryId(id);
    }

    public String getLanguageName() {
        String rs = "";

        return rs;
    }

    public void prepareEditCategory() {
        this.isUpdate = true;
//        messageConfirm = "Are you sure to edit this Category?";
        if (Objects.nonNull(selectedNode)) {
            Category cat = (Category) selectedNode.getData();
            this.category = categoryService.getCategoryById(cat.getCategoryId());
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void doSaveOrUpdateCategory() {
        if (validateCategory()) {
            categoryService.save(category);
            refreshTree();
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public boolean validateCategory() {
        
        if (DataUtil.isStringNullOrEmpty(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Category name");
            return false;
        }
        
        if (!DataUtil.checkNotContainPercentage(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Category name");
            return false;
        }
        boolean rs = true;
        if (!categoryService.checkDuplicate(category.getName(), category.getCategoryId(), category.getCategoryType())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.category.exist"));
            rs = false;
        }
        return rs;

//        if (DataUtil.isStringNullOrEmpty(this.category.getName())) {
//            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category name");
//            return false;
//        }
//        if (!DataUtil.checkMaxlength(this.category.getName())) {
//            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Category name");
//            return false;
//        }
//        boolean rs = true;
//        if (!categoryService.checkDuplicate(category.getName(), category.getCategoryId(), category.getCategoryType())) {
//            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.category.exist"));
//            rs = false;
//        }
//        return rs;
    }

    public boolean validateUssd() {
        System.out.println("validateUssd");
        if (DataUtil.isStringNullOrEmpty(this.ussdScenario.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Scenario name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.ussdScenario.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Scenario name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.ussdScenario.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Scenario name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.ussdScenario.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.ussdScenario.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Description");
            return false;
        }
        if (DataUtil.isNullOrEmpty(this.ussdScenario.getLstScenarioNode())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.ussd.scenario.node"));
            return false;
        } else {
            Map<String, Long> mapNameAndPar = new HashMap<>();
            Map<String, Long> mapSyntaxAndPar = new HashMap<>();
            for (ScenarioNode item : this.ussdScenario.getLstScenarioNode()) {
                if (mapNameAndPar.containsKey(item.getNodeName() + item.getParentId())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.node.duplicate"));
                    return false;
                } else {
                    mapNameAndPar.put(item.getNodeName() + item.getParentId(), item.getParentId());
                }
                if (mapSyntaxAndPar.containsKey(item.getSyntax() + item.getParentId())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.syntax.duplicate"));
                    return false;
                } else {
                    mapSyntaxAndPar.put(item.getSyntax() + item.getParentId(), item.getParentId());
                }
                if (DataUtil.isStringNullOrEmpty(item.getName())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Node name");
                    return false;
                }
                if (!DataUtil.checkMaxlength(item.getName())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Node name");
                    return false;
                }
                if (!DataUtil.checkNotContainPercentage(item.getName())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Node name");
                    return false;
                }
                if (DataUtil.isStringNullOrEmpty(item.getSyntax()) && item.getParentId() != 0) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Syntax");
                    return false;
                }
                if (!DataUtil.checkMaxlength(item.getSyntax())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Syntax");
                    return false;
                }
                if (!DataUtil.checkNotContainPercentage(item.getSyntax())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Syntax");
                    return false;
                }
                if (DataUtil.isNullOrEmpty(item.getLstNoteContent())) {
//                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.ussd.node.content"));
//                    return false;
                } else {
                    Map<Long, Long> mapLanguage = new HashMap<>();
                    int numOfNull = 0;
                    for (NoteContent content : item.getLstNoteContent()) {
                        if (mapLanguage.containsKey(content.getLanguageId())) {
                            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.language"));
                            return false;
                        } else {
                            mapLanguage.put(content.getLanguageId(), content.getLanguageId());
                        }
                        if (DataUtil.isStringNullOrEmpty(content.getContent())) {
                            numOfNull++;
                        }
                        if (DataUtil.isStringNullOrEmpty(content.getContent()) && numOfNull > 1) {
                            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Message content");
                            return false;
                        }
                        if (!DataUtil.checkMaxlength(content.getContent())) {

                            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Message content");
                            return false;
                        }
                        if (!DataUtil.checkNotContainPercentage(content.getContent())) {
                            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Message content");
                            return false;
                        }
                    }
                }
            }
        }

        if (!ussdScenarioService.validateAddUssd(ussdScenario.getScenarioId(), ussdScenario.getScenarioName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.scenario.name"));
            return false;
        }
        //
        if (!validateNoeEmpty()) {
            return false;
        }
        return true;
    }

    public boolean validateNoeEmpty() {
        if (!DataUtil.isNullOrEmpty(this.lstScenarioNodeDelete)) {
            for (ScenarioNode scenarioNode : lstScenarioNodeDelete) {
                List<ScenarioNode> lstDb = ussdScenarioService.findScenarioNodeByParId(scenarioNode.getNodeId());
                List<ScenarioNode> lst = getAllChildDelete(scenarioNode.getNodeId());
                if (!DataUtil.isNullOrEmpty(lstDb)) {
                    int sizedb = lstDb.size();
                    int size = lst.size();
                    if (sizedb > size) {
                        errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.ussd.node.content.change"));
                        return false;
                    }
                }
//                if (!DataUtil.isNullOrEmpty(ussdScenarioService.findScenarioNodeByParId(scenarioNode.getNodeId()))) {
//                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.ussd.node.content.notempty"));
//                    return false;
//                }
            }
        } else {
            List<ScenarioNode> lstScenarioNode = this.ussdScenario.getLstScenarioNode();
            if (!DataUtil.isNullOrEmpty(lstScenarioNode)) {
                int size = 0;
                for (ScenarioNode item : lstScenarioNode) {
                    if (!DataUtil.isNullObject(ussdScenarioService.findScenarioNodeById(item.getNodeId()))) {
                        size++;
                    }
                }
//                    List<ScenarioNode> lstDb = ussdScenarioService.findScenarioNodeByParId(root.getNodeId());
                List<ScenarioNode> lstDb = scenarioNodeDAO.findByScenarioId(this.ussdScenario.getScenarioId());
                if (!DataUtil.isNullOrEmpty(lstDb)) {
                    int sizedb = lstDb.size();
                    if (sizedb > size) {
                        errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.ussd.node.content.change"));
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void onSelectScenario() {
        this.scenarioNode = (ScenarioNode) this.selectedScenarioNode.getData();
        this.scenarioNodeTmp = DataUtil.cloneBean(scenarioNode);
        if (!DataUtil.isNullObject(selectedScenarioNode.getParent())) {
            ScenarioNode parent = (ScenarioNode) selectedScenarioNode.getParent().getData();
            if (!DataUtil.isNullObject(parent)) {
                scenarioNode.setParName(parent.getNodeName());
            }
        }
        isAdd = false;
        this.showDetail = true;
    }

    public void prepareCreateSubNode() {
        isAdd = true;
        System.out.println("prepareCreateSubNode");
        long nodeId = ussdScenarioService.getNextSequense(Constants.TableName.SCENARIO_NOTE);
        this.scenarioNode = new ScenarioNode();
        scenarioNode.setNodeId(nodeId);
        this.showDetail = true;
        ScenarioNode selected = (ScenarioNode) this.selectedScenarioNode.getData();
        scenarioNode.setParentId(selected.getNodeId());
        scenarioNode.setParName(selected.getNodeName());
        scenarioNodeTmp = DataUtil.cloneBean(scenarioNode);
    }

    public void clearNode() {
        try {
            this.ussdScenario = (UssdScenario) selectedNode.getData();
        } catch (Exception e) {

        }
//        if (!this.isUpdate) {
//            this.rootScenarioNode = null;
//            this.ussdScenario.setLstScenarioNode(new ArrayList<>());
//        } else {
//            this.ussdScenario = ussdScenarioService.getUssdScenarioById(ussdScenario.getScenarioId());
//            this.rootScenarioNode = treeService.createTreeScenario(ussdScenario.getLstScenarioNode());
//        }
        this.showDetail = false;
        this.rootScenarioNode = null;
        this.ussdScenario.setLstScenarioNode(new ArrayList<>());
    }

    public void onAddRootNode() {
        long nodeId = ussdScenarioService.getNextSequense(Constants.TableName.SCENARIO_NOTE);
        this.scenarioNode = new ScenarioNode();
        scenarioNode.setNodeId(nodeId);
        this.showDetail = true;
        scenarioNodeTmp = DataUtil.cloneBean(scenarioNode);
    }

    public void doAddNode() {
        System.out.println("doAddNode");
        List<ScenarioNode> lstScenarioNode = this.ussdScenario.getLstScenarioNode();
        lstScenarioNode = DataUtil.isNullOrEmpty(lstScenarioNode) ? new ArrayList<>() : lstScenarioNode;
        boolean isExist = false;
        for (ScenarioNode item : lstScenarioNode) {
            if (item.getNodeId() == this.scenarioNode.getNodeId()) {
                isExist = true;
                break;
            }
        }
        if (!isExist) {
            lstScenarioNode.add(this.scenarioNode);
        }

        if (DataUtil.isNullOrEmpty(scenarioNode.getLstNoteContent())) {
//            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.scenario.node"));
//            return;
        }
        if (!DataUtil.isNullObject(selectedScenarioNode) && !DataUtil.isNullObject(selectedScenarioNode.getData())) {
            Map<String, String> mapNodeName = new HashMap<>();
            Map<String, String> mapSyntax = new HashMap<>();
            for (TreeNode child : selectedScenarioNode.getChildren()) {
                ScenarioNode item = (ScenarioNode) child.getData();
                if (mapSyntax.containsKey(item.getSyntax())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.syntax.duplicate"));
                    return;
                } else {
                    mapSyntax.put(item.getSyntax(), item.getSyntax());
                }
                if (mapNodeName.containsKey(item.getNodeName())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.node.duplicate"));
                    return;
                } else {
                    mapNodeName.put(item.getNodeName(), item.getNodeName());
                }
            }
        }
        if (!DataUtil.isNullObject(selectedScenarioNode)) {
            TreeNode parent = selectedScenarioNode.getParent();
            if (!DataUtil.isNullObject(parent) && !DataUtil.isNullObject(parent.getData())) {
                Map<String, String> mapNodeName = new HashMap<>();
                Map<String, String> mapSyntax = new HashMap<>();
                for (TreeNode child : parent.getChildren()) {
                    ScenarioNode item = (ScenarioNode) child.getData();
                    if (mapSyntax.containsKey(item.getSyntax())) {
                        errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.syntax.duplicate"));
                        return;
                    } else {
                        mapSyntax.put(item.getSyntax(), item.getSyntax());
                    }
                    if (mapNodeName.containsKey(item.getNodeName())) {
                        errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.node.duplicate"));
                        return;
                    } else {
                        mapNodeName.put(item.getNodeName(), item.getNodeName());
                    }
                }
            }
        }

        this.ussdScenario.setLstScenarioNode(lstScenarioNode);
        this.rootScenarioNode = treeService.createTreeScenario(lstScenarioNode);

        this.showDetail = true;
//        long nodeId = ussdScenarioService.getNextSequense(Constants.TableName.SCENARIO_NOTE);
//        this.scenarioNode = new ScenarioNode();
//        scenarioNode.setNodeId(nodeId);
//        if (!DataUtil.isNullObject(rootScenarioNode)) {
//            ScenarioNode root = (ScenarioNode) rootScenarioNode.getChildren().get(0).getData();
//            scenarioNode.setParentId(root.getNodeId());
//        }
//        scenarioNodeTmp = DataUtil.cloneBean(scenarioNode);
        try {
            selectedScenarioNode = DataUtil.isNullObject(selectedScenarioNode) ? rootScenarioNode.getChildren().get(0) : selectedScenarioNode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!DataUtil.isNullObject(selectedScenarioNode)) {
            TreeNode note = findScenarioNodeInTree(rootScenarioNode, (ScenarioNode) selectedScenarioNode.getData());
            if (note != null) {
                expandCurrentNodeScenario(note);
                note.setExpanded(true);
            } else {
                expandCurrentNodeScenario(rootScenarioNode);
                rootScenarioNode.setExpanded(true);
            }
        }
        refreshTreeContentScenario();
    }

    public void expandCurrentNodeScenario(TreeNode selectedNode) {
        TreeNode parent = selectedNode.getParent();
        if (parent == null || parent.getData() == null) {
            return;
        } else {
            addExpandedNodeScenario(parent);
            expandCurrentNodeScenario(parent);
        }
    }

    private void refreshTreeContentScenario() {
        processRefreshScenario(rootScenarioNode, selectedScenarioNode);
    }

    public void processRefreshScenario(TreeNode rootNode, TreeNode selectedNode) {
        if (DataUtil.isNullObject(selectedNode) || DataUtil.isNullObject(rootNode)) {
            return;
        }
        TreeNode treeNode = findScenarioNodeInTree(rootNode, (ScenarioNode) selectedNode.getData());
        treeNode = DataUtil.isNullObject(treeNode) ? selectedScenarioNode : treeNode;
        if (DataUtil.isNullObject(treeNode)) {
            return;
        }
        addExpandedNodeScenario(treeNode.getParent());
        if (treeNode.getParent() != null && selectedNode.getParent() != null) {
            ScenarioNode newNode = (ScenarioNode) treeNode.getParent().getData();
            ScenarioNode oldNode = (ScenarioNode) selectedNode.getParent().getData();
            if (!DataUtil.isNullObject(newNode)
                    && !DataUtil.isNullObject(oldNode)
                    && oldNode.getNodeId() != newNode.getNodeId()
                    && selectedNode.getParent().getChildren().size() == 1) {
                removeExpandedNode(selectedNode.getParent());
            }

        }
        mapTreeStatusScenario(rootNode);
    }

    public void mapTreeStatusScenario(TreeNode root) {
        if (root != null && root.getData() != null && root.getData() instanceof ScenarioNode) {
            Object obj = root.getData();
            ScenarioNode category = (ScenarioNode) obj;
            TreeNode treeNode = treeNodeExpandedScenario.get(category.getNodeId());
            if (treeNode != null) {
                root.setExpanded(true);
            }
        }
        if (root != null) {

            List<TreeNode> lstChildrent = root.getChildren();
            for (TreeNode note : lstChildrent) {
                mapTreeStatusScenario(note);
            }
        }
    }

    public void onNodeExpandScenario(NodeExpandEvent event) {
        addExpandedNodeScenario(event.getTreeNode());
    }

    public void addExpandedNodeScenario(TreeNode node) {
        ScenarioNode category = (ScenarioNode) node.getData();
        if (category == null) {
            return;
        }
        node.setExpanded(true);
        treeNodeExpandedScenario.put(category.getNodeId(), node);
    }

    public void onNodeCollapseScenario(NodeCollapseEvent event) {
        removeExpandedNodeScenario(event.getTreeNode());
    }

    public void removeExpandedNodeScenario(TreeNode node) {
        ScenarioNode category = (ScenarioNode) node.getData();
        if (category == null) {
            return;
        }
        node.setExpanded(false);
        treeNodeExpandedScenario.remove(category.getNodeId());
    }

    public TreeNode findScenarioNodeInTree(TreeNode root, ScenarioNode scenarioNode) {
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (note.getData() instanceof ScenarioNode) {
                ScenarioNode data = (ScenarioNode) note.getData();
                if (data.getNodeId() == scenarioNode.getNodeId()) {
                    return note;
                }
            }
            currentNote = findScenarioNodeInTree(note, scenarioNode);
            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }

    public boolean validateAddNode() {
        System.out.println("validateAddNode");
        String nodeName = scenarioNode.getNodeName();
        String syntax = scenarioNode.getSyntax();
        scenarioNode.setNodeName(scenarioNodeTmp.getNodeName());
        scenarioNode.setSyntax(scenarioNodeTmp.getSyntax());
        if (!isAdd && !DataUtil.isNullObject(selectedScenarioNode)) {
            Map<String, Long> mapNameAndPar = new HashMap<>();
            Map<String, Long> mapSyntaxAndPar = new HashMap<>();
//            mapNameAndPar.put(scenarioNode.getNodeName() + scenarioNode.getParentId(), scenarioNode.getParentId());
//            mapSyntaxAndPar.put(scenarioNode.getSyntax() + scenarioNode.getParentId(), scenarioNode.getParentId());
            TreeNode parent = selectedScenarioNode.getParent();
            if (!DataUtil.isNullObject(parent)) {
                List<TreeNode> child = parent.getChildren();
                if (!DataUtil.isNullOrEmpty(child)) {

                    for (TreeNode item : child) {
                        ScenarioNode node = (ScenarioNode) item.getData();
                        if (mapNameAndPar.containsKey(node.getNodeName() + node.getParentId())) {
                            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.node.duplicate"));
                            scenarioNode.setNodeName(nodeName);
                            return false;
                        } else {
                            mapNameAndPar.put(node.getNodeName() + node.getParentId(), node.getParentId());
                        }
                        if (mapSyntaxAndPar.containsKey(node.getSyntax() + node.getParentId())) {
                            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.syntax.duplicate"));
                            scenarioNode.setSyntax(syntax);
                            return false;
                        } else {
                            mapSyntaxAndPar.put(node.getSyntax() + node.getParentId(), node.getParentId());
                        }
                    }
                }
            }
        }
        if (isAdd && !DataUtil.isNullObject(selectedScenarioNode)) {
            Map<String, Long> mapNameAndPar = new HashMap<>();
            Map<String, Long> mapSyntaxAndPar = new HashMap<>();
            mapNameAndPar.put(scenarioNode.getNodeName() + scenarioNode.getParentId(), scenarioNode.getParentId());
            mapSyntaxAndPar.put(scenarioNode.getSyntax() + scenarioNode.getParentId(), scenarioNode.getParentId());
//            TreeNode parent = selectedScenarioNode.getParent();
            if (!DataUtil.isNullObject(selectedScenarioNode)) {
                List<TreeNode> child = selectedScenarioNode.getChildren();
                if (!DataUtil.isNullOrEmpty(child)) {

                    for (TreeNode item : child) {
                        ScenarioNode node = (ScenarioNode) item.getData();
                        if (mapNameAndPar.containsKey(node.getNodeName() + node.getParentId())) {
                            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.node.duplicate"));
                            scenarioNode.setNodeName(nodeName);
                            return false;
                        } else {
                            mapNameAndPar.put(node.getNodeName() + node.getParentId(), node.getParentId());
                        }
                        if (mapSyntaxAndPar.containsKey(node.getSyntax() + node.getParentId())) {
                            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.syntax.duplicate"));
                            scenarioNode.setSyntax(syntax);
                            return false;
                        } else {
                            mapSyntaxAndPar.put(node.getSyntax() + node.getParentId(), node.getParentId());
                        }
                    }
                }
            }
        }
        if (DataUtil.isStringNullOrEmpty(scenarioNode.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Node name");
            return false;
        }
        if (!DataUtil.checkMaxlength(scenarioNode.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Node name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(scenarioNode.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Node name");
            return false;
        }
        if (DataUtil.isStringNullOrEmpty(scenarioNode.getSyntax()) && scenarioNode.getParentId() != 0) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Syntax");
            return false;
        }
        if (!DataUtil.checkMaxlength(scenarioNode.getSyntax())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Syntax");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(scenarioNode.getSyntax())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Syntax");
            return false;
        }

        if (DataUtil.isNullOrEmpty(scenarioNode.getLstNoteContent())) {
            // root node bat buoc nhap node content
            if (scenarioNode.getParentId() == 0) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.scenario.node"));
                return false;
            }

        } else {
            Map<Long, Long> mapLanguage = new HashMap<>();
            int numOfNull = 0;
            for (NoteContent content : scenarioNode.getLstNoteContent()) {
                if (mapLanguage.containsKey(content.getLanguageId())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.language"));
                    return false;
                } else {
                    mapLanguage.put(content.getLanguageId(), content.getLanguageId());
                }
                if (DataUtil.isStringNullOrEmpty(content.getContent())) {
                    numOfNull++;
                }
                if (DataUtil.isStringNullOrEmpty(content.getContent()) && numOfNull > 1) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Message content");
                    return false;
                }
                if (!DataUtil.checkMaxlength(content.getContent())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Message content");
                    return false;
                }
                if (!DataUtil.checkNotContainPercentage(content.getContent())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Message content");
                    return false;
                }
            }
        }
        return true;
    }

    private void refreshTree() {
//        this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_USSD_SCENARIO_TYPE);
//        rootNode = treeService.createUssdScenarioTree(categories, ussdScenarios);
//        rootNode.setSelected(true);

        if (isUpdate) {
//            categories.remove((Category) selectedNode.getData());
//            categories.add(this.category);

            int i = categories.indexOf(selectedNode.getData());
            categories.remove(selectedNode.getData());
            categories.add(i, this.category);
        } else {
            categories.add(this.category);
        }

        rootNode = treeService.createUssdScenarioTree(categories, ussdScenarios);
        processRefreshCategory(rootNode, selectedNode, category, isUpdate);
        updateCategoryInformUssd();
    }

    public void onSelectOcs() {
        if (!DataUtil.isNullObject(selectedOcsNode)) {
            if (selectedOcsNode.getData() instanceof Category) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.ocs"));
                return;
            } else if (selectedOcsNode.getData() instanceof OcsBehaviour) {
                OcsBehaviour behaviour = (OcsBehaviour) selectedOcsNode.getData();
                this.scenarioNode.setOcsBehaviourId(behaviour.getBehaviourId());
                this.scenarioNode.setOcsBehaviourName(behaviour.getBehaviourName());
            }
        }
    }

    public void refreshData() {
        System.out.println("refreshData");
        TreeNode treeNode = this.selectedNode;
        if (treeNode != null) {
            this.selectedNodeTmp = this.selectedNode;
        }
        if (treeNode != null && treeNode.getData() != null && treeNode.getData() instanceof Category) {
            prepareAddObject();
        } else {
//            initTreeNode();
            prepareEditObject();
        }
        this.actonSwitch = false;
    }

    public void doDeleteCategory() {
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();
            if (DataUtil.isNullObject(category.getParentId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.parent.category"));
                return;
            }
            if (categoryService.checkDeleteCategory(this.category.getCategoryId(), UssdScenario.class.getSimpleName())) {
                categoryService.deleteCategory(category);
                selectedNode.getChildren().clear();
                TreeNode parNode = selectedNode.getParent();
                selectedNode.getParent().getChildren().remove(selectedNode);
                if (parNode.getChildren().isEmpty()) {
                    removeExpandedNode(parNode);
                }
                selectedNode.setParent(null);
                selectedNode = null;
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
                categories.remove(category);
//                parNode.setExpanded(true);
                updateCategoryInformUssd();
            } else {
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public List<ScenarioNode> getAllChildDelete(long nodeId) {
        List<ScenarioNode> lst = new ArrayList<>();
        if (!DataUtil.isNullOrEmpty(lstScenarioNodeDelete)) {
            for (ScenarioNode scenarioNode : lstScenarioNodeDelete) {
                if (nodeId == scenarioNode.getParentId()) {
                    lst.add(scenarioNode);
                }
            }
        }
        return lst;
    }

    public void doDeleteNode() {
        System.out.println("doDeleteNode");
        List<ScenarioNode> lstScenarioNode = ussdScenario.getLstScenarioNode();
        List<TreeNode> lstChild = selectedScenarioNode.getChildren();
        if (!DataUtil.isNullOrEmpty(lstChild)) {
            for (TreeNode child : lstChild) {
                ScenarioNode itemChild = (ScenarioNode) child.getData();
                if (!DataUtil.isNullObject(ussdScenarioService.findScenarioNodeById(itemChild.getNodeId()))) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.ussd.node.content.notempty"));
                    return;
                }
            }

        }
        ScenarioNode scenarioNode = (ScenarioNode) selectedScenarioNode.getData();
        //

        boolean isRemove = true;
        boolean isConfirm = false;
        List<ScenarioNode> lstDb = ussdScenarioService.findScenarioNodeByParId(scenarioNode.getNodeId());
        List<ScenarioNode> lst = getAllChildDelete(scenarioNode.getNodeId());
        if (!DataUtil.isNullOrEmpty(lstDb)) {
            int sizedb = lstDb.size();
            int size = lst.size();
            if (sizedb > size) {
                isRemove = false;
            }
        }
        if (!isRemove) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.ussd.node.content.change"));
            return;
        }

        // check confirm
        if (!DataUtil.isNullObject(ussdScenarioService.findScenarioNodeById(scenarioNode.getNodeId()))) {
            isConfirm = true;
        }
        if (!isConfirm) {
            // xoa luon ko can confirm

//            if (!DataUtil.isNullOrEmpty(lstScenarioNode)) {
//                for (ScenarioNode item : lstScenarioNode) {
//                    if (item.getParentId() == scenarioNode.getNodeId()) {
//                        errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("node.not.empty"));
//                        return;
//                    }
//                }
//            }
//        if (!ussdScenarioService.validateDeleteScenarioNode(scenarioNode.getNodeId())) {
//            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("node.not.empty"));
//            return;
//        }
            // do delete
//        this.ussdScenario = (UssdScenario) selectedNode.getData();
//            lstScenarioNode.remove(scenarioNode);
            List<ScenarioNode> lstScenarioNode1 = new CopyOnWriteArrayList<>();
            lstScenarioNode1.addAll(lstScenarioNode);
            removeInlist(lstScenarioNode1, scenarioNode);
            lstScenarioNode.clear();
            lstScenarioNode.addAll(lstScenarioNode1);
            if (!DataUtil.isNullOrEmpty(ussdScenario.getLstScenarioNode())) {
                this.rootScenarioNode = treeService.createTreeScenario(lstScenarioNode);
            } else {
                this.rootScenarioNode = null;
                this.addRoot = true;
            }

//
//            if (scenarioNode.getParentId() == 0) {
//                this.rootScenarioNode = null;
//                this.addRoot = true;
//            } else {
//                if (!DataUtil.isNullOrEmpty(ussdScenario.getLstScenarioNode())) {
//                    this.rootScenarioNode = treeService.createTreeScenario(lstScenarioNode);
//                } else {
//                    this.rootScenarioNode = null;
//                    this.addRoot = true;
//                }
//            }
            this.showDetail = false;
            refreshTreeContentScenario();
        } else {
            PrimeFaces current = PrimeFaces.current();
            current.executeScript("PF('dlgConfirmDelete').show();");
        }

    }

    public void removeInlist(List<ScenarioNode> lstScenarioNode, ScenarioNode itemRemove) {
        if (!DataUtil.isNullOrEmpty(lstScenarioNode) && !DataUtil.isNullObject(itemRemove)) {
            // lstScenarioNode.remove(itemRemove);
            Iterator<ScenarioNode> itr = lstScenarioNode.iterator();
            while (itr.hasNext()) {
                ScenarioNode item = itr.next();
                if (item.getNodeId() == itemRemove.getNodeId()) {
                    lstScenarioNode.remove(item);
                }
            }
//            lstScenarioNode.removeIf((ScenarioNode a) -> a.getNodeId() == itemRemove.getNodeId());
            if (!DataUtil.isNullOrEmpty(lstScenarioNode)) {
                for (ScenarioNode item : lstScenarioNode) {
                    if (itemRemove.getNodeId() == item.getParentId()) {
                        removeInlist(lstScenarioNode, item);
                    }
                }
            }
        }
    }

    public void doDeleteNodeConfirm() {
        System.out.println("doDeleteNodeConfirm");
        List<ScenarioNode> lstScenarioNode = ussdScenario.getLstScenarioNode();
        ScenarioNode scenarioNode = (ScenarioNode) selectedScenarioNode.getData();
//        if (!DataUtil.isNullOrEmpty(lstScenarioNode)) {
//            for (ScenarioNode item : lstScenarioNode) {
//                if (item.getParentId() == scenarioNode.getNodeId()) {
//                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("node.not.empty"));
//                    return;
//                }
//            }
//        }
//        if (!ussdScenarioService.validateDeleteScenarioNode(scenarioNode.getNodeId())) {
//            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("node.not.empty"));
//            return;
//        }
        // do delete
//        this.ussdScenario = (UssdScenario) selectedNode.getData();
//        lstScenarioNode.remove(scenarioNode);
        List<ScenarioNode> lstScenarioNode1 = new CopyOnWriteArrayList<>();
        lstScenarioNode1.addAll(lstScenarioNode);
        removeInlist(lstScenarioNode1, scenarioNode);
        lstScenarioNode.clear();
        lstScenarioNode.addAll(lstScenarioNode1);
        if (!DataUtil.isNullOrEmpty(ussdScenario.getLstScenarioNode())) {
            this.rootScenarioNode = treeService.createTreeScenario(lstScenarioNode);
        } else {
            this.rootScenarioNode = null;
            this.addRoot = true;
        }
        this.showDetail = false;
        refreshTreeContentScenario();
        this.lstScenarioNodeDelete.add(scenarioNode);
    }

    public void prepareEditObject() {
        this.isDisplay = true;
        this.isUpdate = true;
        actonSwitch = false;
        this.lstScenarioNodeDelete = new ArrayList<>();
        try {
            this.ussdScenario = (UssdScenario) selectedNode.getData();
            this.ussdScenario = ussdScenarioService.getUssdScenarioById(ussdScenario.getScenarioId());
            if (!DataUtil.isNullOrEmpty(ussdScenario.getLstScenarioNode())) {
                for (ScenarioNode item : ussdScenario.getLstScenarioNode()) {
                    OcsBehaviour behaviour = oCSBehaviourServiceImpl.findById(item.getOcsBehaviourId());
                    if (!DataUtil.isNullObject(behaviour)) {
                        item.setOcsBehaviourName(behaviour.getBehaviourName());
                    }
                    if (!DataUtil.isNullOrEmpty(item.getLstNoteContent())) {
                        for (NoteContent noteContent : item.getLstNoteContent()) {
                            if (!DataUtil.isNullOrEmpty(languages)) {
                                for (Language language : languages) {
                                    if (noteContent.getLanguageId() == language.getLanguageId()) {
                                        noteContent.setLanguageName(language.getLanguageName());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            this.rootScenarioNode = treeService.createTreeScenario(ussdScenario.getLstScenarioNode());
            rootScenarioNode.setExpanded(true);
//            this.scenarioNode = (ScenarioNode) this.selectedScenarioNode.getData();
            this.showDetail = false;
        } catch (Exception e) {
            this.ussdScenario = (UssdScenario) this.selectedNodeTmp.getData();
        }

    }

    public void initDataUssdScenario() {

    }

    public void doSaveUssdScenario() {
//        noteContentDAO.deleteByNoteIds(ussdScenario.getScenarioId());
        if (!validateNoeEmpty()) {
            return;
        }
        DataUtil.trimObject(ussdScenario);
        if (!DataUtil.isNullOrEmpty(ussdScenario.getLstScenarioNode())) {
            for (ScenarioNode node : ussdScenario.getLstScenarioNode()) {
                if (node.getParentId() == 0) {
                    node.setIsRoot(1L);
                }
                DataUtil.trimObject(node);
                if (!DataUtil.isNullOrEmpty(node.getLstNoteContent())) {
                    for (NoteContent noteContent : node.getLstNoteContent()) {
                        DataUtil.trimObject(noteContent);
                    }
                }
            }
        }
        ussdScenarioService.doSaveUssd(this.ussdScenario);
//        initTreeNode();
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        if (isUpdate) {
            TreeNode parentNote = selectedNode.getParent();
            if (parentNote != null) {
                parentNote.getChildren().remove(selectedNode);
                if (parentNote.getChildren().size() == 0) {
                    removeExpandedNode(parentNote);
                }
//                new DefaultTreeNode("ussdScenario", ussdScenario, parentNote).setSelected(true);
                parentNote.setExpanded(true);
            }
//            paretNode.getChildren().remove(selectedNode);

        }
        addNoteToCategoryTree(rootNode, ussdScenario);
        if (!isUpdate) {
            ussdScenarios = DataUtil.isNullOrEmpty(ussdScenarios) ? new ArrayList<>() : ussdScenarios;
            ussdScenarios.add(this.ussdScenario);
        }
        mapTreeStatus(rootNode);
        isUpdate = true;
        this.lstScenarioNodeDelete = new ArrayList<>();
        this.actonSwitch = false;
    }

    public TreeNode addNoteToCategoryTree(TreeNode rootNode, UssdScenario conditionTable) {
//        if (isUpdate) {
//            return null;
//        }
        TreeNode note = findParentNoteInTree(rootNode, conditionTable);
        if (note != null) {
            TreeNode newNote = new DefaultTreeNode("ussdScenario", conditionTable, note);
            expandCurrentNode(newNote);
            this.selectedNode = newNote;
            return newNote;
        }
        return null;
    }

    public TreeNode findParentNoteInTree(TreeNode root, UssdScenario conditionTable) {
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (Category.class.isInstance(note.getData())) {
                Category data = (Category) note.getData();
                if (DataUtil.safeEqual(data.getCategoryId(), conditionTable.getCategoryId())) {
                    return note;
                }
            }
            currentNote = findParentNoteInTree(note, conditionTable);
            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }

    public void onSelectRow() {

    }

    public boolean validateAddRow() {
        //
        if (DataUtil.isStringNullOrEmpty(noteContent.getContent())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Message content");
            return false;
        }
        if (!DataUtil.checkMaxlength(noteContent.getContent())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Message content");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(noteContent.getContent())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Message content");
            return false;
        }

        List<NoteContent> lstNoteContent = this.scenarioNode.getLstNoteContent();
        lstNoteContent = DataUtil.isNullOrEmpty(lstNoteContent) ? new ArrayList<>() : lstNoteContent;
        Map<Long, Long> mapLanguage = new HashMap<>();
        NoteContent tmp = DataUtil.cloneBean(noteContent);
        List<NoteContent> lstNoteContent1 = new ArrayList<>(lstNoteContent);
        lstNoteContent1.add(tmp);
        for (NoteContent content : lstNoteContent1) {
            if (mapLanguage.containsKey(content.getLanguageId())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.language"));
                return false;
            } else {
                mapLanguage.put(content.getLanguageId(), content.getLanguageId());
            }

        }

        boolean isAdd = true;
        NoteContent itemAdd = null;
        for (NoteContent item : lstNoteContent) {
            if (item.getNodeContentId() == noteContent.getNodeContentId()) {
                isAdd = false;
                itemAdd = item;
                break;
            }
        }
        if (!DataUtil.isNullOrEmpty(this.languages)) {
            for (Language language : languages) {
                if (noteContent.getLanguageId() == language.getLanguageId()) {
                    noteContent.setLanguageName(language.getLanguageName());
                    break;
                }
            }
        }
        if (isAdd) {
            lstNoteContent.add(noteContent);
        } else {
            itemAdd.setContent(noteContent.getContent());
            itemAdd.setLanguageId(noteContent.getLanguageId());
            itemAdd.setLanguageName(noteContent.getLanguageName());
        }
        this.scenarioNode.setLstNoteContent(lstNoteContent);

        return true;
    }

    public void onAddRow() {
        System.out.println("onAddRow");
//        List<NoteContent> lstNoteContent = this.scenarioNode.getLstNoteContent();
//        lstNoteContent = DataUtil.isNullOrEmpty(lstNoteContent) ? new ArrayList<>() : lstNoteContent;
        Long id = ussdScenarioService.getNextSequense(Constants.TableName.NODE_CONTENT);
        addContent = true;
        noteContent = new NoteContent();
//        noteContent.setNodeId(id);
        noteContent.setNodeContentId(id);
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('addContentTreePopup').show();");
//        lstNoteContent.add(noteContent);
//        this.scenarioNode.setLstNoteContent(lstNoteContent);
    }

    public void onAddColumn() {
//        System.out.println("onAddColumn:");
//        PrimeFaces current = PrimeFaces.current();
//        current.executeScript("PF('preprocessUnitTreePopup').show();");
    }

    public boolean validatePreprocessUnit() {

        return true;
    }

    public void moveUpRow(ConditionTableDTO conditionTableDTO) {
        System.out.println("moveUpRow : " + conditionTableDTO);

    }

    public void moveDownRow(ConditionTableDTO conditionTableDTO) {
        System.out.println("moveDownRow : " + conditionTableDTO);

    }

    public void removeRow(NoteContent noteContent) {
        System.out.println("removeRow : ");
        try {
            if (this.isUpdate) {
                PrimeFaces current = PrimeFaces.current();
                current.executeScript("PF('dlgRemoveNodeContent').show();");
                this.noteContent = noteContent;
            } else {
                this.scenarioNode.getLstNoteContent().remove(noteContent);
            }
        } catch (Exception e) {
            System.out.println("err removeRow");
        }
    }

    public void removeRowAfterConfirm() {
        System.out.println("removeRow : ");
        try {
            this.scenarioNode.getLstNoteContent().remove(this.noteContent);
        } catch (Exception e) {
            System.out.println("err removeRow");
        }
    }

    public void editRow(NoteContent noteContent) {
        System.out.println("editRow : ");
        this.noteContent = noteContent;
        PrimeFaces current = PrimeFaces.current();
        addContent = false;
        current.executeScript("PF('addContentTreePopup').show();");
    }

    public void prepareAddObject() {
        this.lstScenarioNodeDelete = new ArrayList<>();
        this.isDisplay = true;
        this.isUpdate = false;
        this.showDetail = false;
        actonSwitch = true;
        this.ussdScenario = new UssdScenario();
        long id = ussdScenarioService.getNextSequense(Constants.TableName.USSD_SCENARIO);
        rootScenarioNode = null;
        Category category = (Category) this.selectedNode.getData();
        ussdScenario.setCategoryId(category.getCategoryId());
        ussdScenario.setScenarioId(id);
    }

    public void doDeleteObject() {
        Long id = ((UssdScenario) this.selectedNode.getData()).getScenarioId();
        ussdScenarioService.doDeleteUssd(id);
        TreeNode parentNode = selectedNode.getParent();
        parentNode.getChildren().remove(selectedNode);
        if (parentNode.getChildren().size() == 0) {
            removeExpandedNode(parentNode);
        }
        initTreeNode();
        mapTreeStatus(rootNode);
        this.isDisplay = false;
        this.showDetail = false;
        ussdScenarios.remove(selectedNode.getData());
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public void onChooseOcs() {
//        ussdScenarios;
        List<Category> categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_OCS_BEHAVIOUR_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.ocsBehaviours = categoryService.getDataFromCatagoryId(longs, OcsBehaviour.class.getSimpleName());
        rootOcsNode = treeService.createOcsBehaviourTree(categories, ocsBehaviours);

        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('ocsTreePopup').show();");
    }

    public boolean validateDeleteUssd() {
        Long id = ((UssdScenario) this.selectedNode.getData()).getScenarioId();
        boolean rs = ussdScenarioService.validateDeleteUssd(id);
        if (!rs) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.ussd.used"));
            rs = false;
        }
        return rs;
    }

    public TreeNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode rootNode) {
        this.rootNode = rootNode;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(Boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public Segment getSegment() {
        return segment;
    }

    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    public List<Segment> getLstSeg() {
        return lstSeg;
    }

    public void setLstSeg(List<Segment> lstSeg) {
        this.lstSeg = lstSeg;
    }

    public String getSearchSeg() {
        return searchSeg;
    }

    public void setSearchSeg(String searchSeg) {
        this.searchSeg = searchSeg;
    }

    public Rule getRuleSelected() {
        return ruleSelected;
    }

    public void setRuleSelected(Rule ruleSelected) {
        this.ruleSelected = ruleSelected;
    }

    public List<Rule> getLstRule() {
        return lstRule;
    }

    public void setLstRule(List<Rule> lstRule) {
        this.lstRule = lstRule;
    }

    public String getSearchRule() {
        return searchRule;
    }

    public void setSearchRule(String searchRule) {
        this.searchRule = searchRule;
    }

    public Invitation getInvitation() {
        return invitation;
    }

    public void setInvitation(Invitation invitation) {
        this.invitation = invitation;
    }

    public List<Invitation> getLstInvi() {
        return lstInvi;
    }

    public void setLstInvi(List<Invitation> lstInvi) {
        this.lstInvi = lstInvi;
    }

    public String getSearchInvi() {
        return searchInvi;
    }

    public void setSearchInvi(String searchInvi) {
        this.searchInvi = searchInvi;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<UssdScenario> getUssdScenarios() {
        return ussdScenarios;
    }

    public void setUssdScenarios(List<UssdScenario> ussdScenarios) {
        this.ussdScenarios = ussdScenarios;
    }

    public UssdScenario getUssdScenario() {
        return ussdScenario;
    }

    public void setUssdScenario(UssdScenario ussdScenario) {
        this.ussdScenario = ussdScenario;
    }

    public TreeNode getRoot() {
        return root;
    }

    public boolean getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(boolean isDisplay) {
        this.isDisplay = isDisplay;
    }

    public DualListModel<CdrService> getCdrServicePickingList() {
        return cdrServicePickingList;
    }

    public void setCdrServicePickingList(DualListModel<CdrService> cdrServicePickingList) {
        this.cdrServicePickingList = cdrServicePickingList;
    }

    public List<CdrService> getCdrServicesTable() {
        return cdrServicesTable;
    }

    public void setCdrServicesTable(List<CdrService> cdrServicesTable) {
        this.cdrServicesTable = cdrServicesTable;
    }

    public DualListModel<ResultTable> getResultTablePickingList() {
        return resultTablePickingList;
    }

    public void setResultTablePickingList(DualListModel<ResultTable> resultTablePickingList) {
        this.resultTablePickingList = resultTablePickingList;
    }

    public List<ResultTable> getResultTable() {
        return resultTable;
    }

    public void setResultTable(List<ResultTable> resultTable) {
        this.resultTable = resultTable;
    }

    public boolean isApplyAllCdrToRule() {
        return this.applyAllCdrToRule;
    }

    public void setApplyAllCdrToRule(boolean value) {
        this.applyAllCdrToRule = value;
    }

    public void changeCdrValue() {
        this.applyAllCdrToRule = applyAllCdrToRule;
    }

    public String getDatePattern() {
        return datePattern;
    }

    public void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
    }

    public void eventListener(ValueChangeEvent event) {
        System.out.println(event.toString());
    }

    public void valueChangeMethod(ValueChangeEvent e) {
        System.out.println(e.getNewValue().toString());
    }

    public String getMessageConfirm() {
        return messageConfirm;
    }

    public void setMessageConfirm(String messageConfirm) {
        this.messageConfirm = messageConfirm;
    }

    public TreeNode getPreprocessUnitRootNode() {
        return preprocessUnitRootNode;
    }

    public void setPreprocessUnitRootNode(TreeNode preprocessUnitRootNode) {
        this.preprocessUnitRootNode = preprocessUnitRootNode;
    }

    public TreeNode getSelectedPreprocessUnitNode() {
        return selectedPreprocessUnitNode;
    }

    public void setSelectedPreprocessUnitNode(TreeNode selectedPreprocessUnitNode) {
        this.selectedPreprocessUnitNode = selectedPreprocessUnitNode;
    }

    public boolean getActonSwitch() {
//        System.out.println(actonSwitch);
        return actonSwitch;
    }

    public TreeNode getSelectedNodeTmp() {
        return selectedNodeTmp;
    }

    public void setSelectedNodeTmp(TreeNode selectedNodeTmp) {
        this.selectedNodeTmp = selectedNodeTmp;
    }

    public void setActonSwitch(boolean actonSwitch) {
        this.actonSwitch = actonSwitch;
    }

    public boolean isAddRoot() {
        return DataUtil.isNullObject(this.ussdScenario) || DataUtil.isNullOrEmpty(this.ussdScenario.getLstScenarioNode());
    }

    public void setAddRoot(boolean addRoot) {
        this.addRoot = addRoot;
    }

    public TreeNode getRootScenarioNode() {
        return rootScenarioNode;
    }

    public void setRootScenarioNode(TreeNode rootScenarioNode) {
        this.rootScenarioNode = rootScenarioNode;
    }

    public TreeNode getSelectedScenarioNode() {
        return selectedScenarioNode;
    }

    public void setSelectedScenarioNode(TreeNode selectedScenarioNode) {
        this.selectedScenarioNode = selectedScenarioNode;
    }

    public boolean isShowDetail() {
        return showDetail;
    }

    public void setShowDetail(boolean showDetail) {
        this.showDetail = showDetail;
    }

    public ScenarioNode getScenarioNode() {
        return scenarioNode;
    }

    public void setScenarioNode(ScenarioNode scenarioNode) {
        this.scenarioNode = scenarioNode;
    }

    public List<OcsBehaviour> getOcsBehaviours() {
        return ocsBehaviours;
    }

    public void setOcsBehaviours(List<OcsBehaviour> ocsBehaviours) {
        this.ocsBehaviours = ocsBehaviours;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public void prepareEditWhenClick() {
        CheckOnEdit.onEdit = "ussd_template";
        prepareEdit();
        this.actonSwitch = false;
    }

    public void prepareEditFromContextMenu() {
        CheckOnEdit.onEdit = "ussd_template";
        prepareEdit();
        this.actonSwitch = true;

    }

    public void prepareEdit() {
        if ("category".equals(selectedNode.getType())) {
            prepareEditCategory();
            PrimeFaces.current().executeScript("PF('carDialog').show();");
        }
        if ("ussdScenario".equals(selectedNode.getType())) {
            prepareEditObject();
        }

    }
}
