/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.entities.Users;
import vn.viettel.campaign.service.UsersService;
import vn.viettel.campaign.service.UtilsService;

@ManagedBean
@ViewScoped
public class ChangePasswordController extends BaseController implements Serializable {

    private String oldPassword;
    private String newPassword;
    private String reNewPassword;
    private String action;
    private Boolean actonSwitch;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UsersService usersServiceImpl;

    @Autowired
    private UtilsService utilsService;

    private Users userToken;

    @PostConstruct
    public void init() {
        this.actonSwitch = true;
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = (HttpSession) attr.getRequest().getSession(true);
        userToken = (Users) session.getAttribute("userToken");
    }

    public boolean validatePassword() {
        boolean rs = true;
        if (!bCryptPasswordEncoder.matches(oldPassword, userToken.getPassWord())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("incorrect.change.password"));
            rs = false;
        } else if (newPassword.length() < 8 || newPassword.length() > 255) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("length.change.password"));
            rs = false;
        } else if (!newPassword.equals(reNewPassword)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("match.change.password"));
            rs = false;
        }
        return rs;
    }

    public void changePassword() throws IOException {
        if (!validatePassword()) {
            return;
        }
        usersServiceImpl.changePassword(userToken.getUserId(), bCryptPasswordEncoder.encode(newPassword), bCryptPasswordEncoder.encode(oldPassword));
        setOldPassword(StringUtils.EMPTY);
        setNewPassword(StringUtils.EMPTY);
        setReNewPassword(StringUtils.EMPTY);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("success.change.password"));
        FacesContext.getCurrentInstance().getExternalContext().redirect("/campaign/login.jsf");
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getReNewPassword() {
        return reNewPassword;
    }

    public void setReNewPassword(String reNewPassword) {
        this.reNewPassword = reNewPassword;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Boolean getActonSwitch() {
        return actonSwitch;
    }

    public void setActonSwitch(Boolean actonSwitch) {
        this.actonSwitch = actonSwitch;
    }
}
