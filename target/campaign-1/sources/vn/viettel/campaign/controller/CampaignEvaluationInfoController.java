/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.common.DateUtils;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.constants.Constants.CatagoryType;
import static vn.viettel.campaign.controller.BaseController.errorMsg;
import static vn.viettel.campaign.controller.BaseController.errorMsgParams;
import static vn.viettel.campaign.controller.BaseController.successMsg;
import vn.viettel.campaign.dto.Expression;
import vn.viettel.campaign.dto.NodeDefineDTO;
import vn.viettel.campaign.dto.NodeInfo;
import vn.viettel.campaign.entities.AssessmentRule;
import vn.viettel.campaign.entities.BaseCategory;
import vn.viettel.campaign.entities.Campaign;
import vn.viettel.campaign.entities.CampaignEvaluationInfo;
import vn.viettel.campaign.entities.CampaignSchedule;
import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.ResultClass;
import vn.viettel.campaign.entities.RuleEvaluationInfo;
import vn.viettel.campaign.entities.Segment;
import vn.viettel.campaign.entities.SegmentEvaluationInfo;
import vn.viettel.campaign.service.AssessmentRuleService;
import vn.viettel.campaign.service.CampaignEvaluationInforService;
import vn.viettel.campaign.service.CategoryService;
import vn.viettel.campaign.service.TreeUtilsService;
import vn.viettel.campaign.service.UtilsService;

/**
 *
 * @author ABC
 */
@ManagedBean(name = "campaignEvaluationInfoController")
@ViewScoped
@Getter
@Setter
public class CampaignEvaluationInfoController extends BaseController implements Serializable {

    private TreeNode rootNode;
    private TreeNode rootNodeCampaign;
    private TreeNode rootNodeAssessment;
    private TreeNode rootNodeFuzzy;
    private TreeNode rootNodeSegment;
    private TreeNode selectedNode;
    private TreeNode currentRootNote;

    private CampaignEvaluationInfo backupCampaignEvaluationInfo;
    private CampaignEvaluationInfo campaignEvaluationInfo;
    private RuleEvaluationInfo ruleEvaluationInfo;
    private RuleEvaluationInfo ruleEvaluationInfoEdit;
    private ResultClass resultClass;
    private FuzzyRule fuzzyRule;
    private FuzzyRule fuzzyRuleEdit;
    private Campaign campaign;
    private AssessmentRule assessmentRule;
    private CampaignSchedule campaignSchedule;
    private CampaignSchedule campaignScheduleEdit;
    private SegmentEvaluationInfo segmentEvaluationInfo;
    private SegmentEvaluationInfo segmentEvaluationInfoEdit;
    private Category category;
    private Category categoryBackUp;
    private Expression expression;
    private NodeDefineDTO nodeDefineDTO;
    private NodeInfo nodeInfo;
    private Segment segment;

    private List<Category> categories;
    private List<Category> categoriess;
    private List<Category> categorieCamapign = new ArrayList<>();
    private List<Category> categorieSegment = new ArrayList<>();
    private List<Category> categorieAssessment = new ArrayList<>();
    private List<Category> listCategoryToDelete = new ArrayList<>();
    private List<CampaignEvaluationInfo> listCampaignEvaluationInfo;
    private List<CampaignEvaluationInfo> listCampaignEvaluationInfoToDelete = new ArrayList<>();
    private List<RuleEvaluationInfo> listRuleEvaluationInfo;
    private List<RuleEvaluationInfo> listRuleEvaluationInfoOfSegment;
    private List<RuleEvaluationInfo> listRuleEvaluationInfoMapToDefault = new ArrayList<>();
    private List<RuleEvaluationInfo> listRuleEvaluationInfoToDelete;
    private List<ResultClass> listResultClass;
    private List<ResultClass> listResultClassOfSement;
    private List<ResultClass> listResultClassOfSementMap = new ArrayList<>();
    private List<ResultClass> listResultClassMap = new ArrayList<>();
    private List<ResultClass> listResultClassToDelete;
    private List<ResultClass> chooseResultClass = new ArrayList<>();
    private List<ResultClass> listResultClassOfSegmentMap = new ArrayList<>();
    private List<FuzzyRule> listFuzzyRule;
    private List<FuzzyRule> listFuzzyRuleOfSegment;
    private List<FuzzyRule> listFuzzyRuleToDelete;
    private List<FuzzyRule> listFuzzyRuleMapToDefault = new ArrayList<>();
    private List<FuzzyRule> listFuzzyRuleMapToNotDefault = new ArrayList<>();
    private List<SegmentEvaluationInfo> listSegmentEvaluationInfo;
    private List<SegmentEvaluationInfo> listSegmentEvaluationInfoToDelete = new ArrayList<>();
    private List<Campaign> listCampaign;
    private List<AssessmentRule> listAssessmentRule;
    private List<Segment> listSegment;
    private List<CampaignSchedule> listCampaignSchedulesToDelete = new ArrayList<>();
    private List<NodeInfo> listNodeInfoOfSegment = new ArrayList<>();
    private List<NodeInfo> listNodeInfoToDelete = new ArrayList<>();
    private List<NodeInfo> listNodeInfo = new ArrayList<>();

    private List<RuleEvaluationInfo> listRuleEvaluationInfoOfSegmentAfter = new ArrayList<>();
    private List<ResultClass> listResultClassOfSegmentAfter = new ArrayList<>();
    private List<FuzzyRule> listFuzzyRuleOfSegmentAfter = new ArrayList<>();

    private List<RuleEvaluationInfo> listRuleEvaluationInfoOfSegmentBefor = new ArrayList<>();
    private List<ResultClass> listResultClassOfSegmentBefor = new ArrayList<>();
    private List<FuzzyRule> listFuzzyRuleOfSegmentBefor = new ArrayList<>();

    private boolean action;
    private boolean disPlay;
    private boolean ruleEditMode;
    private boolean disabledRuleMode;
    private boolean campaignScheduleEditMode;
    private boolean fuzzyEditMode;
    private boolean expressionEditMode;
    private boolean expressionEditModeGroup;
    private boolean nodeInfoEditMode;
    private boolean nodeInfoEditModeGroup;
    private boolean segmentEvaluationInfoEditMode;
    private boolean ruleSegmentEditMode;
    private boolean disabledRuleSegment;
    private boolean disableSegmentEvaluationInfo;
    private boolean disableFuzzy;
    private boolean disableFuzzySegment;
    private boolean fuuzyRuleSegmentEditMode;
    private boolean categoryEditMode;
    private boolean chekIsDeFault;
    private boolean renderedCbb;
    private boolean renderedCbbSegment;
    private boolean editMode;
    private boolean focusComboboxResult = false;
    private boolean focusCombobox = false;
    private boolean focusValuEx = false;

    private boolean focusComboboxSegment = false;
    private boolean focusValuExSegment = false;
    private boolean focusComboboxResultSegment = false;

    private int count;
    private int modeSchedule;
    private String pattenSchedule;
    private String segmentNameMap;
    private String ruleNameMap;
    private Double dem;

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private TreeUtilsService treeUtilsService;
    @Autowired
    private CampaignEvaluationInforService campaignEvaluationInforService;
    @Autowired
    private UtilsService utilsService;
    @Autowired
    private AssessmentRuleService assessmentRuleService;

    @PostConstruct
    public void init() {
        this.disPlay = false;
        this.categories = new ArrayList<>();
        this.categoriess = new ArrayList<>();
        this.listCampaignEvaluationInfo = new ArrayList<>();
        this.listRuleEvaluationInfo = new ArrayList<>();
        this.listResultClass = new ArrayList<>();
        this.listFuzzyRule = new ArrayList<>();
        this.listCampaign = new ArrayList<>();
        this.listAssessmentRule = new ArrayList<>();
        this.listSegmentEvaluationInfo = new ArrayList<>();
        this.listResultClassOfSement = new ArrayList<>();
        this.listFuzzyRuleOfSegment = new ArrayList<>();
        this.listSegment = new ArrayList<>();
        this.listRuleEvaluationInfoOfSegment = new ArrayList<>();
        this.listRuleEvaluationInfoToDelete = new ArrayList<>();
        this.listResultClassToDelete = new ArrayList<>();
        this.listFuzzyRuleToDelete = new ArrayList<>();
        this.listSegmentEvaluationInfoToDelete = new ArrayList<>();
        initTreeNode();
    }

    public void initTreeNode() {
        this.categories = categoryService.getCategoryByType(CatagoryType.CATEGORY_CAMPAIGN_EVALUATION_INFO_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.listCampaignEvaluationInfo = campaignEvaluationInforService.getListCampaginEvaluationInfoByCategoryIds(longs);
        // this.listCampaignEvaluationInfo = categoryService.getDataFromCatagoryId(longs, Constants.CAMPAIGN_EVALUATION_INFO);
        rootNode = treeUtilsService.createTreeCategoryAndComponentCampaignEvaluation(categories, listCampaignEvaluationInfo);
        rootNode.getChildren().get(0).setExpanded(true);
        addExpandedNode(rootNode.getChildren().get(0));
    }

    public void buildCampaignTree() {
        this.categorieCamapign = categoryService.getCategoryByType(CatagoryType.CATEGORY_CAMPAIGN_ONLINE_TYPE);
        this.categoriess = categoryService.getCategoryByType(CatagoryType.CATEGORY_CAMPAIGN_OFFLINE_TYPE);
        this.categorieCamapign.addAll(this.categoriess);
        List<Long> longs = new ArrayList<>();
        if (!categorieCamapign.isEmpty()) {
            categorieCamapign.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.listCampaign = categoryService.getObjectByCategory(longs, Constants.CAMPAIGN);
        rootNodeCampaign = treeUtilsService.createTreeCategoryAndComponentCampaignEvaluation(categorieCamapign, listCampaign);

    }

//          CATEGORY
    public void preapreCreateCategory() {
        this.categoryEditMode = false;
        Category parentCat = (Category) selectedNode.getData();
        this.category = new Category();
        this.categoryBackUp = new Category();
        category.setCategoryType(parentCat.getCategoryType());
        Long id = campaignEvaluationInforService.getNextSequense(Constants.TableName.CATEGORY);
        category.setCategoryId(id);
        this.category.setParentId(parentCat.getCategoryId());
        cloneCategory(categoryBackUp, category);
    }

    public void cloneCategory(Category a, Category b) {
        a.setCategoryId(b.getCategoryId());
        a.setCategoryType(b.getCategoryType());
        a.setName(b.getName());
        a.setParentId(b.getParentId());
    }

    public void preapreUpdateCategory() {
        this.categoryEditMode = true;
        this.categoryBackUp = new Category();
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();
            cloneCategory(categoryBackUp, category);
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void saveCategory() {
        cloneCategory(category, categoryBackUp);
        categoryService.save(category);
        refreshTree();
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    private void refreshTree() {
        if (categoryEditMode) {
            categories.remove((Category) selectedNode.getData());
            categories.add(this.category);
        } else {
            categories.add(this.category);
        }

        rootNode = treeUtilsService.createTreeCategoryAndComponentCampaignEvaluation(categories, listCampaignEvaluationInfo);
        processRefreshCategory(rootNode, selectedNode, category, categoryEditMode);
//        updateCategoryInformCampaign();
    }

    public void deleteCategory() {
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();
            if (inValidCategoryNode()) {
                return;
            }
            if (categoryService.checkDeleteCategory(this.category.getCategoryId())) {
                categoryService.deleteCategory(category);
                selectedNode.getChildren().clear();
                TreeNode parNode = selectedNode.getParent();

                selectedNode.getParent().getChildren().remove(selectedNode);
                if (parNode.getChildren().isEmpty()) {
                    removeExpandedNode(parNode);
                }
                selectedNode.setParent(null);
                selectedNode = null;
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
                categories.remove(category);
                parNode.setExpanded(true);
//                updateCategoryInformCampaign();
                updateCategoryInforNotifyTemplate();
            } else {
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public boolean inValidCategoryNode() {
        if (DataUtil.isNullObject(category.getParentId())) {
            errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.parent.category"));
            return true;
        }
        if (!selectedNode.getChildren().isEmpty()) {
            errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            return true;
        }
        return false;
    }

    public boolean validateCategory() {
        if (DataUtil.isStringNullOrEmpty(this.categoryBackUp.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.categoryBackUp.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Category name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.categoryBackUp.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Category name");
            return false;
        }
        if (Objects.equals(this.categoryBackUp.getCategoryId(), this.categoryBackUp.getParentId())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.category"));
            return false;
        }

        boolean rs = true;
        if (!categoryService.checkDuplicate(categoryBackUp.getName(), categoryBackUp.getCategoryId(), categoryBackUp.getCategoryType())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Category name");
            rs = false;
        }
        return rs;
    }

//          CAMPAIGN_EVALUAITON_IFNO
    public void prepareEditCampaignEvaluationInfo() {
        //initTreeNode();
        CampaignEvaluationInfo campaignE = (CampaignEvaluationInfo) selectedNode.getData();
        this.currentRootNote = this.selectedNode;
        this.campaignEvaluationInfo = campaignEvaluationInforService.findOneById(campaignE.getCampaignEvaluationInfoId());
        this.backupCampaignEvaluationInfo = new CampaignEvaluationInfo();
        cloneCampaignEvaluationInfo(backupCampaignEvaluationInfo, campaignEvaluationInfo);
        this.listResultClassToDelete = new ArrayList<>();
        this.listFuzzyRuleToDelete = new ArrayList<>();
        this.listRuleEvaluationInfoToDelete = new ArrayList<>();
        this.listSegmentEvaluationInfo = new ArrayList<>();

        this.editMode = true;
        if (this.campaignEvaluationInfo.getIsDefault() == true) {
            this.campaign = campaignEvaluationInforService.findCampaignById(this.campaignEvaluationInfo.getCampaignId());
            this.listRuleEvaluationInfo = campaignEvaluationInforService.getListRuleEvaluationInfoByOwnerId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.listResultClass = campaignEvaluationInforService.getListResultClassByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.listFuzzyRule = campaignEvaluationInforService.getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.campaignSchedule = campaignEvaluationInforService.getCampaignScheduleByScheduleId(campaignEvaluationInfo.getScheduleId());

            this.disPlay = true;
            this.action = true;

            this.campaignScheduleEditMode = true;
            this.chekIsDeFault = true;
        }
        if (this.campaignEvaluationInfo.getIsDefault() == false) {
            this.campaign = campaignEvaluationInforService.findCampaignById(this.campaignEvaluationInfo.getCampaignId());
            this.listResultClass = campaignEvaluationInforService.getListResultClassByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.listFuzzyRule = campaignEvaluationInforService.getListFuzzyRuleByCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.listSegmentEvaluationInfo = campaignEvaluationInforService.getListSegmentEvaluationInfo(campaignEvaluationInfo.getCampaignEvaluationInfoId());

            //Duyệt list segment và load hết các list của nó
            for (SegmentEvaluationInfo item : listSegmentEvaluationInfo) {

                item.setListRuleEvaluationInfo(campaignEvaluationInforService.getListRuleEvaluationInfoOfSegment(item.getSegmentEvaluationInfoId()));
                item.setListResultClass(campaignEvaluationInforService.getListResultClassOfSegment(item.getSegmentId(), campaignEvaluationInfo.getCampaignEvaluationInfoId()));
                item.setListFuzzyRule(campaignEvaluationInforService.getListFuzzyRuleOfSegment(item.getSegmentId(), campaignEvaluationInfo.getCampaignEvaluationInfoId()));

//                listRuleEvaluationInfoOfSegmentBefor.addAll(item.getListRuleEvaluationInfo());
//                listResultClassOfSegmentBefor.addAll(item.getListResultClass());
//                listFuzzyRuleOfSegmentBefor.addAll(item.getListFuzzyRule());
                item.setListRuleEvaluationInfoBefor(item.getListRuleEvaluationInfo());
                item.setListResultClassBefor(item.getListResultClass());
                item.setListFuzzyRuleBefor(item.getListFuzzyRule());

            }
            this.listSegmentEvaluationInfoToDelete = new ArrayList<>();
            this.campaignSchedule = campaignEvaluationInforService.getCampaignScheduleByScheduleId(campaignEvaluationInfo.getScheduleId());
            this.disPlay = true;
            this.action = true;

            this.campaignScheduleEditMode = true;
            this.chekIsDeFault = false;
        }
    }

    public void onResetCampaignEvalautionInfo() {

//        if (this.editMode == true) {
//            this.campaignEvaluationInfo = this.backupCampaignEvaluationInfo;
//            this.campaign = campaignEvaluationInforService.findCampaignById(this.campaignEvaluationInfo.getCampaignId());
//            if (this.campaignEvaluationInfo.getIsDefault() == true) {
//
//                this.listRuleEvaluationInfo = campaignEvaluationInforService.getListRuleEvaluationInfoByOwnerId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
//                this.listResultClass = campaignEvaluationInforService.getListResultClassByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
//                this.listFuzzyRule = campaignEvaluationInforService.getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
//                this.campaignSchedule = campaignEvaluationInforService.getCampaignScheduleByScheduleId(campaignEvaluationInfo.getScheduleId());
//                this.disPlay = true;
//                this.action = true;
//
//                this.listResultClassToDelete = new ArrayList<>();
//                this.listFuzzyRuleToDelete = new ArrayList<>();
//                this.listRuleEvaluationInfoToDelete = new ArrayList<>();
//                this.campaignScheduleEditMode = true;
//                this.chekIsDeFault = true;
//                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
//            }
//            if (this.campaignEvaluationInfo.getIsDefault() == false) {
//
//                this.listResultClass = campaignEvaluationInforService.getListResultClassByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
//                this.listFuzzyRule = campaignEvaluationInforService.getListFuzzyRuleByCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
//                this.listSegmentEvaluationInfo = campaignEvaluationInforService.getListSegmentEvaluationInfo(campaignEvaluationInfo.getCampaignEvaluationInfoId());
//
//                //Duyệt list segment và load hết các list của nó
//                for (SegmentEvaluationInfo item : listSegmentEvaluationInfo) {
//
//                    item.setListRuleEvaluationInfo(campaignEvaluationInforService.getListRuleEvaluationInfoOfSegment(item.getSegmentEvaluationInfoId()));
//                    item.setListResultClass(campaignEvaluationInforService.getListResultClassOfSegment(item.getSegmentId(), campaignEvaluationInfo.getCampaignEvaluationInfoId()));
//                    item.setListFuzzyRule(campaignEvaluationInforService.getListFuzzyRuleOfSegment(item.getSegmentId(), campaignEvaluationInfo.getCampaignEvaluationInfoId()));
//
////                    listRuleEvaluationInfoOfSegmentBefor.addAll(item.getListRuleEvaluationInfo());
////                    listResultClassOfSegmentBefor.addAll(item.getListResultClass());
////                    listFuzzyRuleOfSegmentBefor.addAll(item.getListFuzzyRule());
//                    item.setListRuleEvaluationInfoBefor(item.getListRuleEvaluationInfo());
//                    item.setListResultClassBefor(item.getListResultClass());
//                    item.setListFuzzyRuleBefor(item.getListFuzzyRule());
//                }
//
//                this.campaignSchedule = campaignEvaluationInforService.getCampaignScheduleByScheduleId(campaignEvaluationInfo.getScheduleId());
//                this.disPlay = true;
//                this.action = true;
//
//                this.listResultClassToDelete = new ArrayList<>();
//                this.listFuzzyRuleToDelete = new ArrayList<>();
//                this.listSegmentEvaluationInfoToDelete = new ArrayList<>();
//                this.listRuleEvaluationInfoToDelete = new ArrayList<>();
//
//                this.campaignScheduleEditMode = true;
//                this.chekIsDeFault = false;
//                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
//            }
//        } else {
//            cloneCampaignEvaluationInfo(campaignEvaluationInfo, backupCampaignEvaluationInfo);
//            if (this.campaignEvaluationInfo.getIsDefault() == true) {
//                this.campaign = new Campaign();
//                this.listRuleEvaluationInfo = new ArrayList<>();
//                this.listResultClass = new ArrayList<>();
//                this.listFuzzyRule = new ArrayList<>();
//                this.campaignSchedule = new CampaignSchedule();
//                this.listResultClassToDelete = new ArrayList<>();
//                this.listFuzzyRuleToDelete = new ArrayList<>();
//                this.listRuleEvaluationInfoToDelete = new ArrayList<>();
//            }
//            if (this.campaignEvaluationInfo.getIsDefault() == false) {
//                this.campaign = new Campaign();
//                this.listSegmentEvaluationInfo = new ArrayList<>();
//                this.listResultClass = new ArrayList<>();
//                this.listFuzzyRule = new ArrayList<>();
//                this.campaignSchedule = new CampaignSchedule();
//                this.listResultClassToDelete = new ArrayList<>();
//                this.listFuzzyRuleToDelete = new ArrayList<>();
//                this.listRuleEvaluationInfoToDelete = new ArrayList<>();
//                this.listSegmentEvaluationInfoToDelete = new ArrayList<>();
//            }
//            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
//        }
        //this.selectedNode = this.currentRootNote;
        if (this.editMode == true) {
            if (chekIsDeFault == false) {

                //prepareEditCampaignEvaluationInfo();
                this.listResultClass = campaignEvaluationInforService.getListResultClassByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
                this.listFuzzyRule = campaignEvaluationInforService.getListFuzzyRuleByCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
                this.listSegmentEvaluationInfo = campaignEvaluationInforService.getListSegmentEvaluationInfo(campaignEvaluationInfo.getCampaignEvaluationInfoId());

                //Duyệt list segment và load hết các list của nó
                for (SegmentEvaluationInfo item : listSegmentEvaluationInfo) {

                    item.setListRuleEvaluationInfo(campaignEvaluationInforService.getListRuleEvaluationInfoOfSegment(item.getSegmentEvaluationInfoId()));
                    item.setListResultClass(campaignEvaluationInforService.getListResultClassOfSegment(item.getSegmentId(), campaignEvaluationInfo.getCampaignEvaluationInfoId()));
                    item.setListFuzzyRule(campaignEvaluationInforService.getListFuzzyRuleOfSegment(item.getSegmentId(), campaignEvaluationInfo.getCampaignEvaluationInfoId()));

//                    listRuleEvaluationInfoOfSegmentBefor.addAll(item.getListRuleEvaluationInfo());
//                    listResultClassOfSegmentBefor.addAll(item.getListResultClass());
//                    listFuzzyRuleOfSegmentBefor.addAll(item.getListFuzzyRule());
                    item.setListRuleEvaluationInfoBefor(item.getListRuleEvaluationInfo());
                    item.setListResultClassBefor(item.getListResultClass());
                    item.setListFuzzyRuleBefor(item.getListFuzzyRule());
                }

                this.campaignSchedule = campaignEvaluationInforService.getCampaignScheduleByScheduleId(campaignEvaluationInfo.getScheduleId());
                this.listRuleEvaluationInfoToDelete = new ArrayList<>();
                this.listSegmentEvaluationInfoToDelete = new ArrayList<>();
                this.listFuzzyRuleToDelete = new ArrayList<>();
                this.listResultClassToDelete = new ArrayList<>();
                cloneCampaignEvaluationInfo(campaignEvaluationInfo, backupCampaignEvaluationInfo);
                this.campaignEvaluationInfo.setIsDefault(false);
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
            }
            if (chekIsDeFault == true) {

                // prepareEditCampaignEvaluationInfo();
                this.campaign = campaignEvaluationInforService.findCampaignById(this.campaignEvaluationInfo.getCampaignId());
                this.listRuleEvaluationInfo = campaignEvaluationInforService.getListRuleEvaluationInfoByOwnerId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
                this.listResultClass = campaignEvaluationInforService.getListResultClassByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
                this.listFuzzyRule = campaignEvaluationInforService.getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
                this.campaignSchedule = campaignEvaluationInforService.getCampaignScheduleByScheduleId(campaignEvaluationInfo.getScheduleId());

                this.listRuleEvaluationInfoToDelete = new ArrayList<>();
                this.listFuzzyRuleToDelete = new ArrayList<>();
                this.listResultClassToDelete = new ArrayList<>();
                this.listSegmentEvaluationInfoToDelete = new ArrayList<>();
                cloneCampaignEvaluationInfo(campaignEvaluationInfo, backupCampaignEvaluationInfo);
                this.campaignEvaluationInfo.setIsDefault(true);
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
            }
        } else {
            cloneCampaignEvaluationInfo(campaignEvaluationInfo, backupCampaignEvaluationInfo);
            if (this.campaignEvaluationInfo.getIsDefault() == true) {
                this.campaign = new Campaign();
                this.listRuleEvaluationInfo = new ArrayList<>();
                this.listResultClass = new ArrayList<>();
                this.listFuzzyRule = new ArrayList<>();
                this.campaignSchedule = new CampaignSchedule();
                this.listResultClassToDelete = new ArrayList<>();
                this.listFuzzyRuleToDelete = new ArrayList<>();
                this.listRuleEvaluationInfoToDelete = new ArrayList<>();
            }
            if (this.campaignEvaluationInfo.getIsDefault() == false) {
                this.campaign = new Campaign();
                this.listSegmentEvaluationInfo = new ArrayList<>();
                this.listResultClass = new ArrayList<>();
                this.listFuzzyRule = new ArrayList<>();
                this.campaignSchedule = new CampaignSchedule();
                this.listResultClassToDelete = new ArrayList<>();
                this.listFuzzyRuleToDelete = new ArrayList<>();
                this.listRuleEvaluationInfoToDelete = new ArrayList<>();
                this.listSegmentEvaluationInfoToDelete = new ArrayList<>();
            }
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public void prepareCreateCampaignEvaluationInfo() {
        this.campaignEvaluationInfo = new CampaignEvaluationInfo();
        this.editMode = false;
        this.currentRootNote = null;
        this.category = (Category) selectedNode.getData();
        this.campaignEvaluationInfo = campaignEvaluationInforService.getNextSequenceCampaignEvaluationInfo();
        campaignEvaluationInfo.setCategoryId(category.getCategoryId());
        campaignEvaluationInfo.setIsDefault(false);
        this.campaign = new Campaign();
        this.listRuleEvaluationInfo = new ArrayList<>();
        this.listSegmentEvaluationInfo = new ArrayList<>();
        this.listResultClass = new ArrayList<>();
        this.listFuzzyRule = new ArrayList<>();
        this.disPlay = true;
        this.action = true;
        this.campaignScheduleEditMode = false;
        this.backupCampaignEvaluationInfo = new CampaignEvaluationInfo();
        cloneCampaignEvaluationInfo(backupCampaignEvaluationInfo, campaignEvaluationInfo);
        this.listResultClassToDelete = new ArrayList<>();
        this.listFuzzyRuleToDelete = new ArrayList<>();
        this.listRuleEvaluationInfoToDelete = new ArrayList<>();
        this.listSegmentEvaluationInfoToDelete = new ArrayList<>();

    }

    public void cloneCampaignEvaluationInfo(CampaignEvaluationInfo a, CampaignEvaluationInfo b) {
        a.setCampaignEvaluationInfoId(b.getCampaignEvaluationInfoId());
        a.setCampaignEvaluationInfoName(b.getCampaignEvaluationInfoName());
        a.setCampaignId(b.getCampaignId());
        a.setCategoryId(b.getCategoryId());
        a.setDescription(b.getDescription());
        a.setIsDefault(b.getIsDefault());
        a.setIsDefaultCampaign(b.getIsDefaultCampaign());
        a.setName(b.getName());
        a.setReasioningMethod(b.getReasioningMethod());
        a.setRuleName(b.getRuleName());
        a.setScheduleId(b.getScheduleId());
        a.setScheduleName(b.getScheduleName());
    }

    //VALIDATE CampaignEvaluationInfo
    public boolean validateCampaignEvaluationInfo() {
        CampaignEvaluationInfo campaignEvaluationInfo = this.campaignEvaluationInfo;
        if (DataUtil.isStringNullOrEmpty(campaignEvaluationInfo.getCampaignEvaluationInfoName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Name");
            return false;
        }
        for (CampaignEvaluationInfo items : listCampaignEvaluationInfo) {
            if (Objects.equals(campaignEvaluationInfo.getCampaignEvaluationInfoId(), items.getCampaignEvaluationInfoId())) {
                continue;
            }
            if (campaignEvaluationInfo.getCampaignEvaluationInfoName().equals(items.getCampaignEvaluationInfoName())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.campaignEvaluation.name"));
                return false;
            }
        }

        if (!DataUtil.checkMaxlength(campaignEvaluationInfo.getCampaignEvaluationInfoName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(campaignEvaluationInfo.getCampaignEvaluationInfoName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.special.characters"), "Campaign Evaluation Name");
            return false;
        }
        if (DataUtil.isStringNullOrEmpty(campaignEvaluationInfo.getScheduleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Schedule");
            return false;
        }
        if (this.campaign.getCampaignId() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Campaign");
            return false;
        }
        if (!DataUtil.checkMaxlength(campaignEvaluationInfo.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(campaignEvaluationInfo.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.special.characters"), "Description");
            return false;
        }
        validateSaveAll();
        return true;

    }

    public boolean validateSaveAll() {
        if (this.campaignEvaluationInfo.getIsDefault() == true) {
            this.count = 0;
            for (RuleEvaluationInfo item : listRuleEvaluationInfo) {
                if (item != null) {
                    Double value = item.getRuleWeight() * 100;
                    count = count + value.intValue();
                }
            }
            if (count != 100) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.count.weight"), "Rule");
                return false;
            }
        }
        if (this.campaignEvaluationInfo.getIsDefault() == false) {
            this.count = 0;
            for (SegmentEvaluationInfo item : listSegmentEvaluationInfo) {
                if (item != null) {
                    Double value = item.getSegmentWeight() * 100;
                    count = count + value.intValue();
                }
            }
            if (count != 100) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.count.weight"), "Segment");
                return false;
            }

        }
        if (validateResultClass() == false) {
            return false;
        }

        this.count = 0;
        for (FuzzyRule item : listFuzzyRule) {
            if (item != null) {
                Double value = item.getWeight() * 100;
                count = count + value.intValue();
            }
        }
        if (count != 100) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.count.weight"), "fuzzy");
            return false;
        }

        return true;
    }

    public void chooseCampaignForObject() {

        this.campaign = (Campaign) selectedNode.getData();

        this.listFuzzyRuleToDelete = new ArrayList<>();
        this.listResultClassToDelete = new ArrayList<>();
        if (this.campaignEvaluationInfo.getIsDefault() == true) {
            for (FuzzyRule item : listFuzzyRule) {
                this.listFuzzyRuleToDelete.add(item);
            }
            this.listFuzzyRule = new ArrayList<>();
            for (ResultClass item : listResultClass) {
                this.listResultClassToDelete.add(item);
            }
            this.listResultClass = new ArrayList<>();
        }
        if (this.campaignEvaluationInfo.getIsDefault() == false) {
            this.listSegmentEvaluationInfoToDelete = new ArrayList<>();
            for (SegmentEvaluationInfo item : listSegmentEvaluationInfo) {
                this.listSegmentEvaluationInfoToDelete.add(item);
            }
            this.listSegmentEvaluationInfo = new ArrayList<>();
            for (FuzzyRule item : listFuzzyRule) {
                this.listFuzzyRuleToDelete.add(item);
            }
            this.listFuzzyRule = new ArrayList<>();
            for (ResultClass item : listResultClass) {
                this.listResultClassToDelete.add(item);
            }
            this.listResultClass = new ArrayList<>();
        }
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public boolean validateChooseCampaign() {
        if (!Objects.nonNull(selectedNode)) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.campaign"));
            return false;
        }
        if (selectedNode.getType().equals("category")) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
            return false;
        } else {
            Campaign campaignMap = new Campaign();
            campaignMap = (Campaign) selectedNode.getData();
            if (Objects.equals(this.campaign.getCampaignId(), campaignMap.getCampaignId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.campaignOld"));
                return false;
            }

        }

        return true;
    }

    public void prepareEditOrCreateCampaignSchedule() {
        if (this.campaignEvaluationInfo.getScheduleName() != null) {
            this.campaignScheduleEdit = new CampaignSchedule();
            setCampaignSchedule(campaignScheduleEdit, campaignSchedule);
            this.modeSchedule = (int) this.campaignScheduleEdit.getMode();
            this.pattenSchedule = campaignScheduleEdit.getSchedulePattern();
            if (campaignScheduleEdit.getMode() == 2) {
                String[] date = campaignScheduleEdit.getSchedulePattern().split(" ");
                String dateString = date[3] + "/" + date[4] + "/" + date[5];
                campaignScheduleEdit.setChooseDate(DateUtils.parseVINDate(dateString));
                campaignScheduleEdit.setSecond(Integer.parseInt(date[0]));
                campaignScheduleEdit.setMinute(Integer.parseInt(date[1]));
                campaignScheduleEdit.setHour(Integer.parseInt(date[2]));
            }

        } else {
            this.campaignSchedule = new CampaignSchedule();
            this.campaignScheduleEdit = new CampaignSchedule();
            this.campaignSchedule = campaignEvaluationInforService.getNextSequenceCampaignSchedule();
            this.campaignSchedule.setMode(2L);
            setCampaignSchedule(campaignScheduleEdit, campaignSchedule);
        }
    }

    public void setCampaignSchedule(CampaignSchedule a, CampaignSchedule b) {
        a.setScheduleId(b.getScheduleId());
        a.setScheduleName(b.getScheduleName());
        a.setDescription(b.getDescription());
        a.setScheduleType(b.getScheduleType());
        a.setCampaignId(b.getCampaignId());
        a.setSchedulePattern(b.getSchedulePattern());
        a.setMode(b.getMode());
        a.setSecond(b.getSecond());
        a.setMinute(b.getMinute());
        a.setMode(b.getMode());
        a.setChooseDate(b.getChooseDate());
    }

    public void chooseDefaultOrNotDefault() {
        //this.selectedNode = this.currentRootNote;
        if (chekIsDeFault == false) {
            if (this.campaignEvaluationInfo.getIsDefault() == false) {
                //prepareEditCampaignEvaluationInfo();
                this.listResultClass = campaignEvaluationInforService.getListResultClassByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
                this.listFuzzyRule = campaignEvaluationInforService.getListFuzzyRuleByCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
                this.listSegmentEvaluationInfo = campaignEvaluationInforService.getListSegmentEvaluationInfo(campaignEvaluationInfo.getCampaignEvaluationInfoId());

                //Duyệt list segment và load hết các list của nó
                for (SegmentEvaluationInfo item : listSegmentEvaluationInfo) {

                    item.setListRuleEvaluationInfo(campaignEvaluationInforService.getListRuleEvaluationInfoOfSegment(item.getSegmentEvaluationInfoId()));
                    item.setListResultClass(campaignEvaluationInforService.getListResultClassOfSegment(item.getSegmentId(), campaignEvaluationInfo.getCampaignEvaluationInfoId()));
                    item.setListFuzzyRule(campaignEvaluationInforService.getListFuzzyRuleOfSegment(item.getSegmentId(), campaignEvaluationInfo.getCampaignEvaluationInfoId()));

//                    listRuleEvaluationInfoOfSegmentBefor.addAll(item.getListRuleEvaluationInfo());
//                    listResultClassOfSegmentBefor.addAll(item.getListResultClass());
//                    listFuzzyRuleOfSegmentBefor.addAll(item.getListFuzzyRule());
                    item.setListRuleEvaluationInfoBefor(item.getListRuleEvaluationInfo());
                    item.setListResultClassBefor(item.getListResultClass());
                    item.setListFuzzyRuleBefor(item.getListFuzzyRule());
                }

                this.campaignSchedule = campaignEvaluationInforService.getCampaignScheduleByScheduleId(campaignEvaluationInfo.getScheduleId());
                this.listRuleEvaluationInfoToDelete = new ArrayList<>();
                this.listSegmentEvaluationInfoToDelete = new ArrayList<>();
                this.listFuzzyRuleToDelete = new ArrayList<>();
            }
            if (this.campaignEvaluationInfo.getIsDefault() == true) {
                this.listSegmentEvaluationInfoToDelete = new ArrayList<>();
                this.listSegmentEvaluationInfoToDelete.addAll(this.listSegmentEvaluationInfo);
                this.listSegmentEvaluationInfo = new ArrayList<>();

                this.listRuleEvaluationInfoToDelete = new ArrayList<>();
//                this.listRuleEvaluationInfoToDelete.addAll(this.listRuleEvaluationInfo);
                this.listRuleEvaluationInfo = new ArrayList<>();

                this.listFuzzyRuleToDelete = new ArrayList<>();
                this.listFuzzyRuleToDelete.addAll(this.listFuzzyRule);
                this.listFuzzyRule = new ArrayList<>();
            }
        }
        if (chekIsDeFault == true) {

            if (this.campaignEvaluationInfo.getIsDefault() == true) {
                // prepareEditCampaignEvaluationInfo();
                this.campaign = campaignEvaluationInforService.findCampaignById(this.campaignEvaluationInfo.getCampaignId());
                this.listRuleEvaluationInfo = campaignEvaluationInforService.getListRuleEvaluationInfoByOwnerId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
                this.listResultClass = campaignEvaluationInforService.getListResultClassByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
                this.listFuzzyRule = campaignEvaluationInforService.getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
                this.campaignSchedule = campaignEvaluationInforService.getCampaignScheduleByScheduleId(campaignEvaluationInfo.getScheduleId());

                this.listRuleEvaluationInfoToDelete = new ArrayList<>();
                this.listFuzzyRuleToDelete = new ArrayList<>();
                this.listSegmentEvaluationInfoToDelete = new ArrayList<>();
            }
            if (this.campaignEvaluationInfo.getIsDefault() == false) {

                this.listSegmentEvaluationInfoToDelete = new ArrayList<>();
                this.listSegmentEvaluationInfo = new ArrayList<>();
                //rule
                this.listRuleEvaluationInfoToDelete = new ArrayList<>();
                this.listRuleEvaluationInfoToDelete.addAll(this.listRuleEvaluationInfo);
                this.listRuleEvaluationInfo = new ArrayList<>();
                //fuzzy
                this.listFuzzyRuleToDelete = new ArrayList<>();
                this.listFuzzyRuleToDelete.addAll(this.listFuzzyRule);
                this.listFuzzyRule = new ArrayList<>();
            }
        }
    }

    public boolean saveCampainSchedule() {

        DataUtil.trimObject(campaignScheduleEdit);
        if (DataUtil.isStringNullOrEmpty(campaignScheduleEdit.getScheduleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Schedule name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(campaignScheduleEdit.getScheduleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Schedule name");
            return false;
        }
        if (!DataUtil.checkMaxlength(campaignScheduleEdit.getScheduleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Schedule name");
            return false;
        }

        if (!DataUtil.checkNotContainPercentage(campaignScheduleEdit.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Description");
            return false;
        }
        if (!DataUtil.checkMaxlength(campaignScheduleEdit.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }

        if (campaignScheduleEdit.getMode() == 2 && campaignScheduleEdit.getChooseDate() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Choose date");
            return false;
        }

        if (campaignScheduleEdit.getMode() == 1 && DataUtil.isStringNullOrEmpty(campaignScheduleEdit.getSchedulePattern())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Pattern");
            return false;
        }
        String partten = "";
        if (campaignScheduleEdit.getMode() == 2) {

            Calendar cal = Calendar.getInstance();
            cal.setTime(campaignScheduleEdit.getChooseDate());
            cal.set(Calendar.SECOND, campaignScheduleEdit.getSecond());
            cal.set(Calendar.MINUTE, campaignScheduleEdit.getMinute());
            cal.set(Calendar.HOUR, campaignScheduleEdit.getHour());
            Calendar now = Calendar.getInstance();
            if (cal.before(now)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.date"), "Choose date time", "current "
                        + "date time");
                return false;
            }
            int month = cal.get(Calendar.MONTH) + 1;
            partten = +campaignScheduleEdit.getSecond() + " " + campaignScheduleEdit.getMinute() + " " + campaignScheduleEdit.getHour()
                    + " " + cal.get(Calendar.DAY_OF_MONTH) + " " + month + " " + cal.get(Calendar.YEAR);
            campaignScheduleEdit.setSchedulePattern(partten);
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        } else {
            String[] element = campaignScheduleEdit.getSchedulePattern().split(" ");
            if (element.length < 6 || element.length > 7) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.pattern"), "Pattern");
                return false;
            }
            if (!element[0].matches(DateUtils.SECOND_REGEX)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Second");
                return false;
            }
            if (!element[1].matches(DateUtils.MINUTE_REGEX_CAMPAIGN)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Minute");
                return false;
            }
            if (!element[2].matches(DateUtils.HOUR_REGEX)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Hour");
                return false;
            }
            if (!element[3].matches(DateUtils.DAY_OF_MONTH_REGEX)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Day of month");
                return false;
            }
            if (!element[4].matches(DateUtils.MONTH_REGEX)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Month");
                return false;
            }
            if (!element[5].matches(DateUtils.DAY_OF_WEEK_REGEX)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Day of week");
                return false;
            }
            if (element.length == 7 && !element[6].matches(DateUtils.YEAR_REGEX)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Year");
                return false;
            }
            if (element.length == 7) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.SECOND, Integer.parseInt(element[0]));
                cal.set(Calendar.MINUTE, Integer.parseInt(element[1]));
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(element[2]));
                cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(element[3]));
                cal.set(Calendar.MONTH, Integer.parseInt(element[4]) - 1);
                cal.set(Calendar.YEAR, Integer.parseInt(element[6]));
                Calendar now = Calendar.getInstance();
                if (cal.before(now)) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.date"), "Choose date time", "current "
                            + "date time");
                    return false;
                }
            }
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
        campaignEvaluationInfo.setScheduleName(campaignScheduleEdit.getScheduleName());
        campaignScheduleEdit.setScheduleType(1);
        setCampaignSchedule(campaignSchedule, campaignScheduleEdit);
        return true;

    }

    public void chooseNodeCampaignSchedule() {

        if (this.modeSchedule == 1) {
            this.campaignScheduleEdit = campaignScheduleEdit;
            if (this.campaignScheduleEdit.getMode() == 2) {
                this.campaignScheduleEdit.setSchedulePattern(null);
            }
            if (this.campaignScheduleEdit.getMode() == 1) {
                this.campaignScheduleEdit.setSchedulePattern(pattenSchedule);
            }
        }
        if (this.modeSchedule == 2) {
            if (this.campaignScheduleEdit.getMode() == 1) {
                this.campaignScheduleEdit.setSchedulePattern(null);
                campaignScheduleEdit.setChooseDate(null);
                campaignScheduleEdit.setSecond(0);
                campaignScheduleEdit.setMinute(0);
                campaignScheduleEdit.setHour(0);
            }
            if (this.campaignScheduleEdit.getMode() == 2) {
                String[] date = pattenSchedule.split(" ");
                String dateString = date[3] + "/" + date[4] + "/" + date[5];
                campaignScheduleEdit.setChooseDate(DateUtils.parseVINDate(dateString));
                campaignScheduleEdit.setSecond(Integer.parseInt(date[0]));
                campaignScheduleEdit.setMinute(Integer.parseInt(date[1]));
                campaignScheduleEdit.setHour(Integer.parseInt(date[2]));
            }
        }

//        this.campaignSchedule.setSchedulePattern("");
//        this.campaignSchedule.setChooseDate(null);
    }

    public void editNameCampaignEvaluationInfo() {
        this.campaignEvaluationInfo.setCampaignEvaluationInfoName(campaignEvaluationInfo.getCampaignEvaluationInfoName().replaceAll(" +", " "));
        for (RuleEvaluationInfo item : listRuleEvaluationInfo) {
            item.setCampaignEvaluationInfoName(campaignEvaluationInfo.getCampaignEvaluationInfoName());
        }

    }

    public void editDescriptionCampaignEvaluationInfo() {
        this.campaignEvaluationInfo.setDescription(campaignEvaluationInfo.getDescription().replaceAll(" +", " "));
    }

    public void prepareEditWhenClick() {
        if ("object".equals(selectedNode.getType())) {
            prepareEditCampaignEvaluationInfo();
            this.action = false;
        }
        if ("category".equals(selectedNode.getType())) {
            preapreUpdateCategory();
            PrimeFaces.current().executeScript("PF('carDialog').show();");
        }
    }

//        RULE_EVALUATION_INFO
    public void buildAssessmentTree() {
        this.categorieAssessment = categoryService.getCategoryByType(CatagoryType.CATEGORY_ASSESSMENT_RULE_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categorieAssessment.isEmpty()) {
            categorieAssessment.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.listAssessmentRule = categoryService.getDataFromCatagoryId(longs, Constants.ASSESSMENT_RULE);
        rootNodeAssessment = treeUtilsService.createTreeCategoryAndComponentCampaignEvaluation(categorieAssessment, listAssessmentRule);
        rootNodeAssessment.getChildren().get(0).setExpanded(true);
        addExpandedNode(rootNodeAssessment.getChildren().get(0));
    }

    public void prepareCreateRuleEvaluationInfo() {
        if ("".equals(this.campaignEvaluationInfo.getCampaignEvaluationInfoName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.emptyNumber"), "Campaign EvaluationInfo Name");
        } else {
            this.ruleEditMode = false;
            this.disabledRuleMode = false;
            this.assessmentRule = new AssessmentRule();
            this.ruleNameMap = null;
            this.ruleEvaluationInfo = new RuleEvaluationInfo();
            this.ruleEvaluationInfoEdit = new RuleEvaluationInfo();
            this.ruleEvaluationInfo = campaignEvaluationInforService.getNextSequenceRuleEvaluation();
            this.ruleEvaluationInfo.setRuleLevel(3L);
            this.ruleEvaluationInfo.setOwnerId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.ruleEvaluationInfo.setCampaignEvaluationInfoName(campaignEvaluationInfo.getCampaignEvaluationInfoName());
            setRuleEvaluationInfo(ruleEvaluationInfoEdit, ruleEvaluationInfo);
        }
    }

    public void prepareEditRuleEvaluationInfo(RuleEvaluationInfo ruleEvaluationInfo) {
        if ("".equals(this.campaignEvaluationInfo.getCampaignEvaluationInfoName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.emptyNumber"), "Campaign EvaluationInfo Name");
        } else {
            this.ruleEditMode = true;
            this.disabledRuleMode = false;
//        this.ruleEvaluationInfo = ruleEvaluationInfo;
            this.ruleEvaluationInfoEdit = new RuleEvaluationInfo();
            setRuleEvaluationInfo(ruleEvaluationInfoEdit, ruleEvaluationInfo);
            this.ruleNameMap = ruleEvaluationInfoEdit.getRuleName();
            this.assessmentRule = assessmentRuleService.findAssessmentRuleById(this.ruleEvaluationInfoEdit.getAssessmentRuleId());
        }
    }

    public void setRuleEvaluationInfo(RuleEvaluationInfo a, RuleEvaluationInfo b) {
        a.setAssessmentRuleId(b.getAssessmentRuleId());
        a.setOwnerId(b.getOwnerId());
        a.setRuleEvaluationInfoId(b.getRuleEvaluationInfoId());
        a.setRuleReasioningMethod(b.getRuleReasioningMethod());
        a.setRuleWeight(b.getRuleWeight());
        a.setRuleLevel(b.getRuleLevel());
        a.setDescription(b.getDescription());
        a.setCampaignEvaluationInfoName(b.getCampaignEvaluationInfoName());
        a.setRuleName(b.getRuleName());
    }

    public void editAssessmentDescription() {
        this.ruleEvaluationInfo = ruleEvaluationInfo;
        this.ruleEvaluationInfo.setDescription(this.ruleEvaluationInfo.getDescription().replaceAll(" +", " "));
    }

    public void deleteRuleEvaluationInfo(RuleEvaluationInfo ruleEvaluationInfo) {
        this.ruleEvaluationInfo = ruleEvaluationInfo;
        if (validateDeleteRuleEvaluationOfCampaignEvaluaiton()) {
            this.listRuleEvaluationInfo.remove(ruleEvaluationInfo);
            if (ruleEvaluationInfo != null) {
                this.listRuleEvaluationInfoToDelete.add(ruleEvaluationInfo);
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.delete.assessment"));
        }
    }

    public void doSaveRuleEvaluationInfo() {
        if (this.ruleEditMode == false) {
            setRuleEvaluationInfo(this.ruleEvaluationInfo, ruleEvaluationInfoEdit);
            this.listRuleEvaluationInfo.add(ruleEvaluationInfo);
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        } else {
            for (RuleEvaluationInfo item : listRuleEvaluationInfo) {
                if (Objects.equals(item.getRuleEvaluationInfoId(), ruleEvaluationInfoEdit.getRuleEvaluationInfoId())) {
                    setRuleEvaluationInfo(item, ruleEvaluationInfoEdit);
                }
            }
            if (!this.ruleEvaluationInfoEdit.getRuleName().equals(this.ruleNameMap)) {
                if (this.ruleNameMap != null) {
                    if (!this.listFuzzyRule.isEmpty()) {
                        List<Long> longs = checkChooseNameInFuzzyRule(ruleNameMap, this.listFuzzyRule);
                        for (Long id : longs) {
                            for (FuzzyRule item : listFuzzyRule) {
                                if (Objects.equals(id, item.getFuzzyRuleId())) {
                                    this.listFuzzyRuleToDelete.add(item);
                                    this.listFuzzyRule.remove(item);
                                    if (this.listFuzzyRule.isEmpty()) {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

            }

            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }

    }

    public void showRuleEvaluationInfo(RuleEvaluationInfo ruleEvaluationInfo) {
        this.disabledRuleMode = true;
        this.ruleEvaluationInfoEdit = ruleEvaluationInfo;
        this.assessmentRule = assessmentRuleService.findAssessmentRuleById(this.ruleEvaluationInfoEdit.getAssessmentRuleId());
    }

    public void chooseAssessmentRule() {
        assessmentRule = (AssessmentRule) selectedNode.getData();
        this.ruleEvaluationInfoEdit.setAssessmentRuleId(assessmentRule.getRuleId());
        this.ruleEvaluationInfoEdit.setRuleName(assessmentRule.getRuleName());

        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    //VALIDATE ASSESMENTRULE
    public boolean validateRuleEvaluation() {
        RuleEvaluationInfo ruleEvaluationInfo = this.ruleEvaluationInfoEdit;
        if (DataUtil.isStringNullOrEmpty(ruleEvaluationInfo.getCampaignEvaluationInfoName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Owner Name");
            return false;
        }
        if (ruleEvaluationInfo.getAssessmentRuleId() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Rule Name");
            return false;
        }
        if (ruleEvaluationInfo.getRuleWeight() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.emptyNumber"), "Rule Weight");
            return false;
        }
        if (ruleEvaluationInfo.getRuleWeight() < 0 || ruleEvaluationInfo.getRuleWeight() > 1) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.weight"), "Rule Weight");
            return false;
        }
        if (!DataUtil.checkMaxlength(ruleEvaluationInfo.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(ruleEvaluationInfo.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.special.characters"), "Description");
            return false;
        }
        return true;
    }
    //validate delete AssessmentRule

    public boolean validateDeleteRuleEvaluationOfCampaignEvaluaiton() {
        List<NodeInfo> allListNodeInfor = getAllNodeInfoOfListfuzzyRuleOfCampaignEvaluation();
        Boolean validate = true;
        // RuleEvaluationInfo ruleEvaluationInfo = this.ruleEvaluationInfo;
        validate = checkNameNotIsUsedWithContentInExpressionOfListFuzzyRule(this.ruleEvaluationInfo.getRuleName(), allListNodeInfor);
        return validate;
    }

    public List<NodeInfo> getAllNodeInfoOfListfuzzyRuleOfCampaignEvaluation() {
        List<NodeInfo> allListNodeInfor = new ArrayList<>();
        for (FuzzyRule item : listFuzzyRule) {
            allListNodeInfor.addAll(getListNodeInfoFromDisplayExpress(item.getDisplayExpression()));
        }
        return allListNodeInfor;
    }

    //validate delete AssessmentRule Of Segment
    public boolean validateDeleteRuleEvaluationOfSegment() {
        List<NodeInfo> allListNodeInfor = getAllNodeInfoOfListfuzzyRuleOfSegment();
        Boolean validate = true;
        // RuleEvaluationInfo ruleEvaluationInfo = this.ruleEvaluationInfo;
        validate = checkNameNotIsUsedWithContentInExpressionOfListFuzzyRule(this.ruleEvaluationInfo.getRuleName(), allListNodeInfor);
        return validate;
    }

    public List<NodeInfo> getAllNodeInfoOfListfuzzyRuleOfSegment() {
        List<NodeInfo> allListNodeInfor = new ArrayList<>();
        for (FuzzyRule item : listFuzzyRuleOfSegment) {
            allListNodeInfor.addAll(getListNodeInfoFromDisplayExpress(item.getDisplayExpression()));
        }
        return allListNodeInfor;
    }

    public Boolean checkNameNotIsUsedWithContentInExpressionOfListFuzzyRule(String name, List<NodeInfo> allListNodeInfor) {
        for (NodeInfo item : allListNodeInfor) {
            String[] text = item.getDisplay().split(" ");
            for (int i = 0; i < text.length; i++) {
                if ("is".equals(text[i])) {
                    if (item.getDisplay().substring(0, item.getDisplay().indexOf(text[i]) - 1).trim().equals(name)) {
                        return false;
                    }
                }
                if ("not".equals(text[i])) {
                    if (item.getDisplay().substring(0, item.getDisplay().indexOf(text[i]) - 1).trim().equals(name)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public boolean validateDeleteResultClassOfSegment() {
        if (validateDeleteResultNameInFuzzyRuleSegment() == false) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.delete.resultNamesegment"));
            return false;
        }
        if (validateDeleteResultNameInFuzzySegment() == false) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.result.name"));
            return false;
        }
        return true;

    }

    //validate delete ResultClass Of Segment
    public boolean validateDeleteResultNameInFuzzySegment() {
        List<NodeInfo> allListNodeInfor = getAllNodeInfoOfListfuzzyRuleOfCampaignEvaluationNotDefault();
        boolean validadsas = true;
        validadsas = checkResultClassNameNotIsUsedWithValueExInExpressionOfListFuzzyRule(this.resultClass.getClassName(), allListNodeInfor);
        return validadsas;
    }

    public boolean validateDeleteResultNameInFuzzyRuleSegment() {
        this.dem = 0d;
        for (FuzzyRule item : listFuzzyRuleOfSegment) {
            if (Objects.equals(item.getClassId(), resultClass.getClassId())) {
                dem++;
            }
        }
        if (dem != 0) {

            return false;
        }
        return true;
    }

    public List<NodeInfo> getAllNodeInfoOfListfuzzyRuleOfCampaignEvaluationNotDefault() {
        List<NodeInfo> allListNodeInfor = new ArrayList<>();
        for (FuzzyRule item : this.listFuzzyRule) {
            allListNodeInfor.addAll(getListNodeInfoFromDisplayExpress(item.getDisplayExpression()));
        }
        return allListNodeInfor;
    }

    public Boolean checkResultClassNameNotIsUsedWithValueExInExpressionOfListFuzzyRule(String name, List<NodeInfo> allListNodeInfor) {
        if (!"".equals(name)) {
            for (NodeInfo item : allListNodeInfor) {
                String[] text = item.getDisplay().split(" ");
                for (int i = 0; i < text.length; i++) {
                    if ("is".equals(text[i])) {
                        if (item.getDisplay().substring(item.getDisplay().indexOf(text[i]) + 2, item.getDisplay().length()).trim().equals(name)) {
                            return false;
                        }
                    }
                    if ("not".equals(text[i])) {
                        if (item.getDisplay().substring(item.getDisplay().indexOf(text[i]) + 3, item.getDisplay().length()).trim().equals(name)) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    //validate delete SegmentEvaluationInfo
    public boolean validateDeleteSegmentEvaluationInfo() {
        List<NodeInfo> allListNodeInfor = getAllNodeInfoOfListfuzzyRuleOfCampaignEvaluationNotDefault();
        Boolean validate = true;
        validate = checkSegmentNameNotIsUsedWithValueExInExpressionOfListFuzzyRule(this.segmentEvaluationInfo.getSegmentName(), allListNodeInfor);
        return validate;
    }

    public Boolean checkSegmentNameNotIsUsedWithValueExInExpressionOfListFuzzyRule(String name, List<NodeInfo> allListNodeInfor) {
        for (NodeInfo item : allListNodeInfor) {
            String[] text = item.getDisplay().split(" ");
            for (int i = 0; i < text.length; i++) {
                if ("is".equals(text[i])) {
                    if (item.getDisplay().substring(0, item.getDisplay().indexOf(text[i]) - 1).trim().equals(name)) {
                        return false;
                    }
                }
                if ("not".equals(text[i])) {
                    if (item.getDisplay().substring(0, item.getDisplay().indexOf(text[i]) - 1).trim().equals(name)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    //       RESULT_CLASS
    public void prepareCreateResultClass() {
        if (this.campaign.getCampaignId() == null) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.campaign"));
        } else {
            this.resultClass = new ResultClass();
            this.resultClass = campaignEvaluationInforService.getNextSequenceResultClass();
            this.resultClass.setOwnerId(this.campaign.getCampaignId());
            this.resultClass.setCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.resultClass.setClassLevel(3);
            this.listResultClass.add(this.resultClass);
        }
    }

    public void editResultClassName() {
        for (ResultClass item : listResultClass) {
            if (item != null) {
                item.setClassName(item.getClassName().replaceAll(" +", " "));
                item.setDescription(item.getDescription().replaceAll(" +", " "));
            }
        }
        for (FuzzyRule item : listFuzzyRule) {
            for (ResultClass items : listResultClass) {
                if (Objects.equals(item.getClassId(), items.getClassId())) {
                    item.setClassName(items.getClassName());
                }
            }
        }
    }

    public void deleteResultClass(ResultClass resultClass) {
        this.resultClass = resultClass;
        if (validateDeleteResultClass()) {
            if (resultClass != null) {
                this.listResultClass.remove(resultClass);
                this.listResultClassToDelete.add(resultClass);
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
            }
        }
    }

    public boolean validateDeleteResultClass() {
        this.dem = 0d;
        for (FuzzyRule item : listFuzzyRule) {
            if (Objects.equals(item.getClassId(), resultClass.getClassId())) {
                dem++;
            }
        }
        if (dem != 0) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.result.name"));
            return false;
        }
        return true;
    }

    public boolean validateResultClass() {
        if (listResultClass.isEmpty()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "List Result Class");
            return false;
        }
        for (ResultClass item : listResultClass) {
            if (!DataUtil.checkMaxlength(item.getDescription())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description Result class");
                return false;
            }
            if (!DataUtil.checkNotContainPercentage(item.getDescription())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.special.characters"), "Description Result class");
                return false;
            }
            if (DataUtil.isStringNullOrEmpty(item.getClassName())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Result Class Name");
                return false;
            }
            if (!DataUtil.checkMaxlength(item.getClassName())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Result Class Name");
                return false;
            }
            if (!DataUtil.checkNotContainPercentage(item.getClassName())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.special.characters"), "Result Class Name");
                return false;
            }
        }
        for (ResultClass item : listResultClass) {
            for (ResultClass items : listResultClass) {
                if (Objects.equals(item.getClassId(), items.getClassId())) {
                    continue;
                }
                if (item.getClassName().equals(items.getClassName())) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.resultName"));
                    return false;
                }
            }
        }
        return true;
    }

//      FUZZY_RULE
    public void prepareCreateFuzzyRule() {
        if (this.campaign.getCampaignId() == null) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.campaign"));

        } else {
            if (validateResultClass()) {
                this.fuzzyEditMode = false;
                this.focusComboboxResult = false;
                this.disableFuzzy = false;
                this.fuzzyRuleEdit = new FuzzyRule();
                this.fuzzyRule = new FuzzyRule();
                this.fuzzyRule = campaignEvaluationInforService.getNextSequenceFuzzyRule();
                this.fuzzyRule.setRuleLevel(3L);
                this.fuzzyRule.setOwnerId(this.campaign.getCampaignId());
                this.fuzzyRule.setCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
                this.fuzzyRule.setDisplayExpression("");
                setFuzzyRule(fuzzyRuleEdit, fuzzyRule);
                this.chooseResultClass = new ArrayList<>();
                this.chooseResultClass.addAll(this.listResultClass);
                this.listNodeInfo = new ArrayList<>();
                NodeInfo item = new NodeInfo();
                item.setId(1L);
                item.setParentId(null);
                item.setType("group");
                item.setValue("");
                this.renderedCbb = false;
                this.listNodeInfo.add(item);
                nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT("and");
                item.setOperator(nodeDefineDTO.getOperatorDT());
                rootNodeFuzzy = genTreeFuzzy(this.listNodeInfo);
            }
        }
    }

    public void prepareEditFuzzyRule(FuzzyRule fuzzyRule) {
        if (this.campaign.getCampaignId() == null) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.campaign"));

        } else {
            if (validateResultClass()) {
                this.fuzzyEditMode = true;
                this.disableFuzzy = false;
                this.focusComboboxResult = false;
                //this.fuzzyRule = fuzzyRule;
                this.fuzzyRuleEdit = new FuzzyRule();
                setFuzzyRule(fuzzyRuleEdit, fuzzyRule);
                this.chooseResultClass = new ArrayList<>();
                this.chooseResultClass.addAll(this.listResultClass);
                listNodeInfo = getListNodeInfoFromDisplayExpress(this.fuzzyRuleEdit.getDisplayExpression());
                rootNodeFuzzy = genTreeFuzzy(this.listNodeInfo);
                this.dem = 0d;
                for (NodeInfo item : listNodeInfo) {
                    if (item.getParentId() == null) {
                        this.nodeDefineDTO = new NodeDefineDTO();
                        nodeDefineDTO.setOperatorDT(item.getOperator());
                        for (NodeInfo items : listNodeInfo) {
                            if (Objects.equals(items.getParentId(), item.getId())) {
                                dem++;
                            }
                        }
                        if (dem >= 2) {
                            this.renderedCbb = true;
                        } else {
                            this.renderedCbb = false;
                        }

                    }
                }
            }
        }

    }

    public void setFuzzyRule(FuzzyRule a, FuzzyRule b) {
        a.setCampaignEvaluationInfoId(b.getCampaignEvaluationInfoId());
        a.setClassId(b.getClassId());
        a.setClassName(b.getClassName());
        a.setDescription(b.getDescription());
        a.setDisplayExpression(b.getDisplayExpression());
        a.setFuzzyRuleId(b.getFuzzyRuleId());
        a.setFuzzyOperator(b.getFuzzyOperator());
        a.setExpression(b.getExpression());
        a.setOwnerId(b.getOwnerId());
        a.setWeight(b.getWeight());
        a.setRuleLevel(b.getRuleLevel());
    }

    public void showFuzzyRule(FuzzyRule fuzzyRule) {
        this.disableFuzzy = true;
        this.fuzzyRuleEdit = new FuzzyRule();
        setFuzzyRule(fuzzyRuleEdit, fuzzyRule);
        this.chooseResultClass = new ArrayList<>();
        this.chooseResultClass.addAll(this.listResultClass);
        listNodeInfo = getListNodeInfoFromDisplayExpress(this.fuzzyRuleEdit.getDisplayExpression());
        rootNodeFuzzy = genTreeFuzzy(this.listNodeInfo);
        TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfo);
        genField(togenfield);
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                this.dem = 0d;
                this.nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT(item.getOperator());
                for (NodeInfo items : listNodeInfo) {
                    if (Objects.equals(items.getParentId(), item.getId())) {
                        dem++;
                    }
                }
                if (dem >= 2) {
                    this.renderedCbb = true;
                } else {
                    this.renderedCbb = false;
                }

            }
        }
    }

    public void editDescriptionFuzzyRule() {
        this.fuzzyRuleEdit = fuzzyRuleEdit;
        this.fuzzyRuleEdit.setDescription(this.fuzzyRuleEdit.getDescription().replaceAll(" +", " "));
    }

    public boolean validateTreeFuzzy() {
        Boolean check = true;
        for (NodeInfo it : listNodeInfo) {
            if ("group".equals(it.getType())) {
                if (countChild(it, listNodeInfo) < 2) {
                    check = false;
                }
            }
        }
        return check;
    }

    public boolean validateTreeFuzzySegment() {
        Boolean check = true;
        for (NodeInfo it : listNodeInfoOfSegment) {
            if ("group".equals(it.getType())) {
                if (countChild(it, listNodeInfoOfSegment) < 2) {
                    check = false;
                }
            }
        }
        return check;
    }

    public int countChild(NodeInfo nodeParent, List<NodeInfo> listNodeInfo) {
        int count = 0;
        for (NodeInfo item : listNodeInfo) {
            if (Objects.equals(item.getParentId(), nodeParent.getId())) {
                count += 1;
            }
        }
        return count;
    }

    public boolean validateFuzzyRule() {
        FuzzyRule fuzzyRule = this.fuzzyRuleEdit;

        if (fuzzyRule.getWeight() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.expresstion"), "Fuzzy Weight");
            return false;
        }
        if (fuzzyRule.getWeight() < 0 || fuzzyRule.getWeight() > 1) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.weight"), "Fuzzy Weight");
            return false;
        }
        if (!DataUtil.checkMaxlength(fuzzyRule.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (fuzzyRule.getClassId() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Result class");
            this.focusComboboxResult = true;
            return false;
        }
        if (!DataUtil.checkMaxlength(fuzzyRule.getDisplayExpression())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Display Expression");
            return false;
        }
        if (DataUtil.isStringNullOrEmpty(fuzzyRule.getDisplayExpression())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Display Expression");
            return false;
        }
        if (!validateTreeFuzzy()) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.group.tree"));
            return false;
        }
        return true;
    }

    public boolean validateExpresstion() {
        Expression expression = this.expression;
        if ("".equals(expression.getContentEx())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.expresstion"), "ContextEx");
            this.focusCombobox = true;
            this.focusComboboxSegment = true;
            return false;
        }
        if ("".equals(expression.getValueEx())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.valueEx"), "ValueEx");
            this.focusValuEx = true;
            this.focusValuExSegment = true;
            return false;
        }
        return true;
    }

    public void chooseSegmentName() {
        expression = this.expression;
//        this.segment = campaignEvaluationInforService.getSegmentByName(expression.getContentEx());
        this.listResultClassOfSementMap = new ArrayList<>();
        for (SegmentEvaluationInfo item : listSegmentEvaluationInfo) {
            if (item.getSegmentName().equals(expression.getContentEx())) {
                this.listResultClassOfSementMap = item.getListResultClass();
            }
        }
    }

    public void deleteFuzzy(FuzzyRule fuzzyRule) {
        this.listFuzzyRule.remove(fuzzyRule);
        if (this.fuzzyRule != null) {
            this.listFuzzyRuleToDelete.add(fuzzyRule);
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public void saveFuzzyRule() {
        if (this.fuzzyEditMode == false) {
            for (ResultClass item : listResultClass) {
                if (Objects.equals(fuzzyRuleEdit.getClassId(), item.getClassId())) {
                    this.fuzzyRuleEdit.setClassName(item.getClassName());
                }
            }

            setFuzzyRule(this.fuzzyRule, fuzzyRuleEdit);

            this.listFuzzyRule.add(this.fuzzyRule);
            for (FuzzyRule item : listFuzzyRule) {
                item.setExpression(item.getDisplayExpression().trim().toLowerCase()
                        .replace("and", "&")
                        .replace("not", "#")
                        .replace("or", "||")
                        .replace("is", "_").replaceAll(" +", "_"));

            }
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
        if (this.fuzzyEditMode == true) {
            for (ResultClass item : listResultClass) {
                if (Objects.equals(fuzzyRuleEdit.getClassId(), item.getClassId())) {
                    this.fuzzyRuleEdit.setClassName(item.getClassName());
                }
            }
            for (FuzzyRule item : listFuzzyRule) {
                if (Objects.equals(item.getFuzzyRuleId(), fuzzyRuleEdit.getFuzzyRuleId())) {
                    setFuzzyRule(item, fuzzyRuleEdit);
                }
                item.setExpression(item.getDisplayExpression().trim().toLowerCase()
                        .replace("and", "&")
                        .replace("not", "#")
                        .replace("or", "||")
                        .replace("is", "_").replaceAll(" +", "_"));
            }
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    // ren cay
    public TreeNode genTreeFuzzy(List<NodeInfo> listNodeInfo) {
        // root node of framework
        TreeNode fuzzyRootNode = new DefaultTreeNode(null, null);

//         lấy ra nodeInfo gốc nhưng khong gan vao goc framework
        NodeInfo nodeInfoRoot = new NodeInfo();
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                nodeInfoRoot = item;
            }
        }

        for (NodeInfo item : listNodeInfo) {
            if (Objects.equals(item.getParentId(), nodeInfoRoot.getId())) {
                if ("group".equals(item.getType())) {
                    TreeNode node = new DefaultTreeNode("group", item, fuzzyRootNode);
                    recursiveTreeFuzzy(node, item, listNodeInfo);
                }
                if ("object".equals(item.getType())) {
                    TreeNode node = new DefaultTreeNode("object", item, fuzzyRootNode);
                }
            }
        }

        fuzzyRootNode.setExpanded(true);

        return fuzzyRootNode;
    }

    public TreeNode genTreeFuzzyToGenDisplayExpression(List<NodeInfo> listNodeInfo) {
        // root node of framework
        TreeNode fuzzyRootNode = new DefaultTreeNode(null, null);

//        //Tạo root node của dữ liệu
        NodeInfo firstNodeInfo = new NodeInfo();
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                firstNodeInfo = item;
            }
        }
        TreeNode rootNode = new DefaultTreeNode("group", firstNodeInfo, fuzzyRootNode);

//        // Đệ quy để tạo các node con tiếp theo
        recursiveTreeFuzzy(rootNode, firstNodeInfo, listNodeInfo);
        fuzzyRootNode.getChildren().get(0).setExpanded(true);

        return fuzzyRootNode;
    }

    public void recursiveTreeFuzzy(TreeNode rootNode, NodeInfo firstNodeInfo, List<NodeInfo> listNodeInfo) {
        for (NodeInfo item : listNodeInfo) {

            if (Objects.equals(item.getParentId(), firstNodeInfo.getId())) {
                if ("group".equals(item.getType())) {
                    TreeNode node = new DefaultTreeNode("group", item, rootNode);
                    recursiveTreeFuzzy(node, item, listNodeInfo);
                }

                if ("object".equals(item.getType())) {
                    TreeNode node = new DefaultTreeNode("object", item, rootNode);
                }
            }
        }
    }

    private Long nodeInfoId = 0L;

    public List<NodeInfo> getListNodeInfoFromDisplayExpress(String displayExpression) {

        //Khởi tạo listNodeInfo rỗng 
        List<NodeInfo> listNodeInfo = new ArrayList<>();

        //Khởi tạo group gốc
        this.nodeInfoId = 1L;
        NodeInfo rootNodeInfo = new NodeInfo();
        rootNodeInfo.setId(this.nodeInfoId);
        rootNodeInfo.setValue(displayExpression);
        rootNodeInfo.setType("group");
        rootNodeInfo.setDisplay(null);
        // thiếu setOperatorn để set sau

        //Gán vào list
        listNodeInfo.add(rootNodeInfo);

        //Đệ quy
        getListNode(rootNodeInfo, listNodeInfo);

        // trả về list
        return listNodeInfo;

    }

    public void getListNode(NodeInfo nodeInfo, List<NodeInfo> listNodeInfo) {

        // lấy chuỗi displayExpression of group
        String valueStr = nodeInfo.getValue();

        // Ký tự ngoặc đầu tiên (tính cả ban đầu hoăc sau khi reset nếu đủ bộ ngoặc)
        String startChar = "";

        // vị trí ngoặc mở đầu tiên
        int startIndex = 0;

        // để lưu số lượng ngoặc mở
        int numberOfStartChar = 0;

        // vị trí ngoặc đóng cuối cùng
        int endIndex = 0;

        // để lưu số lượng ngoặc dong
        int numberOfEndChar = 0;

//        ArrayList<String> listString = new ArrayList();
        ArrayList<String> listStringToSplit = new ArrayList();
        List<NodeInfo> listTodequy = new ArrayList();

        for (int i = 0; i < valueStr.length(); i++) {
            if ("".equals(startChar)) {

                if ("[".equals(Character.toString(valueStr.charAt(i))) || "(".equals(Character.toString(valueStr.charAt(i)))) {
                    startChar = Character.toString(valueStr.charAt(i));
                    numberOfStartChar = 1;
                    startIndex = i;
                }
            } else {

                if ("(".equals(startChar)) {

                    if (")".equals(Character.toString(valueStr.charAt(i)))) {
                        numberOfEndChar = numberOfEndChar + 1;

                        if (numberOfEndChar == numberOfStartChar) {
                            endIndex = i;
//                            listString.add(valueStr.substring(startIndex + 1, endIndex));
                            listStringToSplit.add(valueStr.substring(startIndex, endIndex + 1));

                            NodeInfo n = new NodeInfo();
                            this.nodeInfoId = this.nodeInfoId + 1;

                            n.setId(this.nodeInfoId);
                            n.setParentId(nodeInfo.getId());
                            n.setValue(valueStr.substring(startIndex + 1, endIndex));
                            n.setType("group");
//                            n.setDisplay(valueStr.substring(startIndex + 1, endIndex));

                            listNodeInfo.add(n);
                            listTodequy.add(n);

                            startChar = "";
                            numberOfStartChar = 0;
                            startIndex = 0;
                            numberOfEndChar = 0;
                            endIndex = 0;
//                            getListNode(n, nodeInfoId, listNodeInfo);
                        }

                    }
                    if ("(".equals(Character.toString(valueStr.charAt(i)))) {
                        numberOfStartChar = numberOfStartChar + 1;
                    }
                }

                if ("[".equals(startChar)) {
                    if ("]".equals(Character.toString(valueStr.charAt(i)))) {
                        numberOfEndChar = numberOfEndChar + 1;
                        if (numberOfEndChar == numberOfStartChar) {
                            endIndex = i;
//                            listString.add(valueStr.substring(startIndex + 1, endIndex));
                            listStringToSplit.add(valueStr.substring(startIndex, endIndex + 1));

                            NodeInfo n = new NodeInfo();
                            this.nodeInfoId = this.nodeInfoId + 1;

                            n.setId(this.nodeInfoId);
                            n.setParentId(nodeInfo.getId());
                            n.setValue(valueStr.substring(startIndex + 1, endIndex));
                            n.setType("object");
                            n.setDisplay(valueStr.substring(startIndex + 1, endIndex));

                            listNodeInfo.add(n);

                            startChar = "";
                            numberOfStartChar = 0;
                            numberOfEndChar = 0;
                            startIndex = 0;
                            endIndex = 0;
                        }
                    }
                    if ("[".equals(Character.toString(valueStr.charAt(i)))) {
                        numberOfStartChar = numberOfStartChar + 1;
                    }
                }

            }
        }

        String operator = valueStr.replace(listStringToSplit.get(0), "").trim().split(" ")[0];
        nodeInfo.setOperator(operator);
        nodeInfo.setDisplay("Group - " + operator);
        for (NodeInfo itm : listTodequy) {
            getListNode(itm, listNodeInfo);
        }
    }

    //ren tu cay len field
    public void genField(TreeNode tree) {
        String valueStr = "";
        if (genTreeFuzzyToGenDisplayExpression(listNodeInfo).getChildren().size() > 0) {
            TreeNode nodeTogen = genTreeFuzzyToGenDisplayExpression(listNodeInfo).getChildren().get(0);
            valueStr = genToStringTree(nodeTogen);
            valueStr = valueStr.substring(1, valueStr.length() - 1);
        }

        // set valueString vào trường field
        fuzzyRuleEdit.setDisplayExpression(valueStr);
    }

    public void genFieldOfSegment(TreeNode tree) {
        String valueStr = "";
        if (genTreeFuzzyToGenDisplayExpression(listNodeInfoOfSegment).getChildren().size() > 0) {
            TreeNode nodeTogen = genTreeFuzzyToGenDisplayExpression(listNodeInfoOfSegment).getChildren().get(0);
            valueStr = genToStringTree(nodeTogen);
            valueStr = valueStr.substring(1, valueStr.length() - 1);
        }

        // set valueString vào trường field
        fuzzyRuleEdit.setDisplayExpression(valueStr);
    }

    public String genToStringTree(TreeNode treeNode) {
        String valueString = "(";

        NodeInfo nodeInfo = (NodeInfo) treeNode.getData();

        List<TreeNode> listChildren = treeNode.getChildren();
        if (listChildren.size() > 0) {
            NodeInfo fistChildNodeInfo = (NodeInfo) listChildren.get(0).getData();
            if ("object".equals(fistChildNodeInfo.getType())) {
                valueString = valueString + "[" + fistChildNodeInfo.getDisplay() + "]";
            }
            if ("group".equals(fistChildNodeInfo.getType())) {
                valueString = valueString + genToStringTree(listChildren.get(0));
            }

            for (int i = 1; i < listChildren.size(); i++) {
                NodeInfo nodeInfos = (NodeInfo) listChildren.get(i).getData();
                if ("object".equals(nodeInfos.getType())) {
                    valueString = valueString + " " + nodeInfo.getOperator() + " " + "[" + nodeInfos.getDisplay() + "]";
                }
                if ("group".equals(nodeInfos.getType())) {
                    valueString = valueString + " " + nodeInfo.getOperator() + " " + genToStringTree(listChildren.get(i));
                }
            }
        }
        valueString = valueString + ")";
        return valueString;
    }

// Object
    public void prepareEditExpression() {
        this.focusComboboxSegment = false;
        this.focusValuExSegment = false;
        this.expressionEditMode = true;
        this.focusCombobox = false;
        this.focusValuEx = false;
        this.listResultClassMap = new ArrayList<>();
        this.listResultClassOfSementMap = new ArrayList<>();
        expression = new Expression();
        NodeInfo nodeInfo = (NodeInfo) selectedNode.getData();

        String[] text = nodeInfo.getDisplay().split(" ");

        for (int i = 0; i < text.length; i++) {
            if ("is".equals(text[i])) {
                expression.setContentEx(nodeInfo.getDisplay().substring(0, nodeInfo.getDisplay().indexOf(text[i]) - 1).trim());
                expression.setOperatorEx("is");
                expression.setValueEx(nodeInfo.getDisplay().substring(nodeInfo.getDisplay().indexOf(text[i]) + 2, nodeInfo.getDisplay().length()).trim());
            }
            if ("not".equals(text[i])) {
                expression.setContentEx(nodeInfo.getDisplay().substring(0, nodeInfo.getDisplay().indexOf(text[i]) - 1).trim());
                expression.setOperatorEx("not");
                expression.setValueEx(nodeInfo.getDisplay().substring(nodeInfo.getDisplay().indexOf(text[i]) + 3, nodeInfo.getDisplay().length()).trim());
            }
        }

        if (this.campaignEvaluationInfo.getIsDefault() == true) {
            for (RuleEvaluationInfo item : listRuleEvaluationInfo) {
                if (item.getRuleName() != null) {
                    if (item.getRuleName().equals(expression.getContentEx())) {
                        this.listResultClassMap = campaignEvaluationInforService.getListResultClassByAssessmentRuleId(item.getAssessmentRuleId());
                    } else {

                    }
                }
            }
        }
        if (this.campaignEvaluationInfo.getIsDefault() == false) {
            this.segment = campaignEvaluationInforService.getSegmentByName(expression.getContentEx());
            if (campaignEvaluationInforService.getSegmentByName(expression.getContentEx()) != null) {
                this.listResultClassOfSement = campaignEvaluationInforService.getListResultClassOfSegment(this.segment.getSegmentId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
                this.listResultClassOfSementMap = new ArrayList<>();
                this.listResultClassOfSementMap = this.listResultClassOfSement;
            } else {

            }
        }

    }

    public void prepareEditExpressionSegment() {
        this.focusComboboxSegment = false;
        this.focusValuExSegment = false;

        this.expressionEditMode = true;

        this.listResultClassMap = new ArrayList<>();

        this.listResultClassOfSementMap = new ArrayList<>();
        expression = new Expression();
        NodeInfo nodeInfo = (NodeInfo) selectedNode.getData();

        String[] text = nodeInfo.getDisplay().split(" ");

        for (int i = 0;
                i < text.length;
                i++) {
            if ("is".equals(text[i])) {
                expression.setContentEx(nodeInfo.getDisplay().substring(0, nodeInfo.getDisplay().indexOf(text[i]) - 1).trim());
                expression.setOperatorEx("is");
                expression.setValueEx(nodeInfo.getDisplay().substring(nodeInfo.getDisplay().indexOf(text[i]) + 2, nodeInfo.getDisplay().length()).trim());
            }
            if ("not".equals(text[i])) {
                expression.setContentEx(nodeInfo.getDisplay().substring(0, nodeInfo.getDisplay().indexOf(text[i]) - 1).trim());
                expression.setOperatorEx("not");
                expression.setValueEx(nodeInfo.getDisplay().substring(nodeInfo.getDisplay().indexOf(text[i]) + 3, nodeInfo.getDisplay().length()).trim());
            }
        }

        for (RuleEvaluationInfo item : listRuleEvaluationInfoOfSegment) {
            if (item.getRuleName() != null) {
                if (item.getRuleName().equals(expression.getContentEx())) {
                    this.listResultClassOfSegmentMap = campaignEvaluationInforService.getListResultClassByAssessmentRuleId(item.getAssessmentRuleId());
                } else {

                }
            }
        }

    }

    public void prepareCreateExpression() {
        this.focusComboboxSegment = false;
        this.focusValuExSegment = false;
        this.focusCombobox = false;
        this.focusValuEx = false;
        this.expressionEditMode = false;
        this.expressionEditModeGroup = false;
        expression = new Expression();
        this.listResultClassMap = new ArrayList<>();
        this.listResultClassOfSementMap = new ArrayList<>();
        this.listResultClassOfSegmentMap = new ArrayList<>();
    }

    public void chooseContexntEx() {
        expression = this.expression;
        this.listResultClassMap = new ArrayList<>();
        for (RuleEvaluationInfo item : listRuleEvaluationInfo) {
            if (item.getRuleName() != null) {
                if (item.getRuleName().equals(expression.getContentEx())) {
                    this.listResultClassMap = campaignEvaluationInforService.getListResultClassByAssessmentRuleId(item.getAssessmentRuleId());
                }
            }
        }
    }

    public void prepareCreateExpressionForGroup() {
        this.focusComboboxSegment = false;
        this.focusValuExSegment = false;
        this.focusCombobox = false;
        this.focusValuEx = false;
        this.expressionEditMode = false;
        this.expressionEditModeGroup = true;
        expression = new Expression();
        this.listResultClassMap = new ArrayList<>();
        this.listResultClassOfSementMap = new ArrayList<>();
        this.listResultClassOfSegmentMap = new ArrayList<>();
    }

    public void deleteEpressionOrGroup() {
        NodeInfo nodeInfo = (NodeInfo) selectedNode.getData();
        this.count = 0;
        for (NodeInfo item : listNodeInfo) {
            if (Objects.equals(item.getParentId(), nodeInfo.getId())) {
                this.count++;
            }
        }
        if (count >= 1) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.delete.group"));
        } else {
            this.listNodeInfo.remove(nodeInfo);
            rootNodeFuzzy = genTreeFuzzy(this.listNodeInfo);
            for (NodeInfo item : listNodeInfo) {
                if (item.getParentId() == null) {
                    this.nodeDefineDTO = new NodeDefineDTO();
                    nodeDefineDTO.setOperatorDT(item.getOperator());
                    this.dem = 0d;
                    for (NodeInfo items : listNodeInfo) {
                        if (Objects.equals(items.getParentId(), item.getId())) {
                            dem++;
                        }
                    }
                    if (dem >= 2) {
                        this.renderedCbb = true;
                    } else {
                        this.renderedCbb = false;
                    }
                }
            }
            TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfo);
            genField(togenfield);
        }
    }

    public boolean validateDeleteGroup() {
        NodeInfo nodeInfo = (NodeInfo) selectedNode.getData();
        this.count = 0;
        for (NodeInfo item : listNodeInfo) {
            if (Objects.equals(item.getParentId(), nodeInfo.getId())) {
                this.count++;
            }
        }
        if (count >= 1) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.delete.group"));
            return false;
        }
        return true;
    }

    public void saveOrEditExpression() {
        if (this.expressionEditMode == true) {

            this.nodeInfo = (NodeInfo) selectedNode.getData();
            for (NodeInfo item : listNodeInfo) {
                if (Objects.equals(item.getId(), this.nodeInfo.getId())) {
                    item.setDisplay(expression.getContentEx() + " " + expression.getOperatorEx() + " " + expression.getValueEx());
                }
            }

        }
        if (this.expressionEditMode == false) {
            if (this.expressionEditModeGroup == false) {
                NodeInfo nodeSelect = (NodeInfo) selectedNode.getData();
                this.nodeInfo = new NodeInfo();

                Long maxId = 1L;
                for (NodeInfo item : listNodeInfo) {
                    if (item.getId() > maxId) {
                        maxId = item.getId();
                    }
                }

                this.nodeInfo.setId(maxId + 1);
                this.nodeInfo.setParentId(nodeSelect.getId());
                this.nodeInfo.setDisplay(expression.getContentEx() + " " + expression.getOperatorEx() + " " + expression.getValueEx());
                this.nodeInfo.setValue(nodeInfo.getDisplay());
                this.nodeInfo.setOperator(expression.getOperatorEx());
                this.nodeInfo.setType("object");

                //listNodeInfo.add(nodeInfo);
            }
            if (this.expressionEditModeGroup == true) {

                this.nodeInfo = new NodeInfo();

                Long maxId = 1L;
                for (NodeInfo item : listNodeInfo) {
                    if (item.getId() > maxId) {
                        maxId = item.getId();
                    }
                }

                Long minId = 1L;
                for (NodeInfo item : listNodeInfo) {
                    if (item.getId() < minId) {
                        minId = item.getId();
                    }
                }

                this.nodeInfo.setId(maxId + 1);
                this.nodeInfo.setParentId(minId);
                this.nodeInfo.setDisplay(expression.getContentEx() + " " + expression.getOperatorEx() + " " + expression.getValueEx());
                this.nodeInfo.setValue(nodeInfo.getDisplay());
                this.nodeInfo.setOperator(expression.getOperatorEx());
                this.nodeInfo.setType("object");

                //listNodeInfo.add(nodeInfo);
            }

        }
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                this.nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT(item.getOperator());
                this.dem = 0d;
                for (NodeInfo items : listNodeInfo) {
                    if (Objects.equals(items.getParentId(), item.getId())) {
                        dem++;
                    }
                }
                if (dem >= 2) {
                    this.renderedCbb = true;
                } else {
                    this.renderedCbb = false;
                }
            }
        }
//        rootNodeFuzzy = genTreeFuzzy(this.listNodeInfo);
        refreshTreeNodeInfo();
        TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfo);
        genField(togenfield);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));

    }

    //    xoa tree fuzzy
    public void clearTree() {
        for (int i = 0; i < listNodeInfo.size(); i++) {
            if (listNodeInfo.get(i).getParentId() != null) {
                listNodeInfoToDelete.add(listNodeInfo.get(i));
            }
        }
        for (NodeInfo item : listNodeInfoToDelete) {
            if (item != null) {
                listNodeInfo.remove(item);
            }
        }
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                this.nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT(item.getOperator());
                this.dem = 0d;
                for (NodeInfo items : listNodeInfo) {
                    if (Objects.equals(items.getParentId(), item.getId())) {
                        dem++;
                    }
                }
                if (dem >= 2) {
                    this.renderedCbb = true;
                } else {
                    this.renderedCbb = false;
                }
            }
        }
        rootNodeFuzzy = genTreeFuzzy(this.listNodeInfo);
        TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfo);
        genField(togenfield);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));

    }

// Group
    public void prepareEditNodeInfo() {
        this.nodeInfoEditMode = true;
        NodeInfo nodeInfo = (NodeInfo) selectedNode.getData();
        nodeDefineDTO = new NodeDefineDTO();
        nodeDefineDTO.setOperatorDT(nodeInfo.getDisplay().split(" ")[2]);
    }

    public void prepareCreateNodeInfo() {
        this.nodeInfoEditMode = false;
        this.nodeInfoEditModeGroup = false;
        nodeDefineDTO = new NodeDefineDTO();
    }

    public void prepareCreateNodeInfoGroup() {
        this.nodeInfoEditMode = false;
        this.nodeInfoEditModeGroup = true;
        nodeDefineDTO = new NodeDefineDTO();
    }

    public void chooseOperratorGroup() {
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                item.setOperator(this.nodeDefineDTO.getOperatorDT());
                item.setDisplay("Group" + " " + "-" + " " + this.nodeDefineDTO.getOperatorDT());
            }
        }
        rootNodeFuzzy = genTreeFuzzy(this.listNodeInfo);
        TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfo);
        genField(togenfield);

    }

    public void saveOrEditGroup() {
        if (this.nodeInfoEditMode == false) {
            if (this.nodeInfoEditModeGroup == false) {
                NodeInfo nodeInfoSelect = (NodeInfo) selectedNode.getData();
                this.nodeInfo = new NodeInfo();

                Long maxId = 1L;
                for (NodeInfo item : listNodeInfo) {
                    if (item.getId() > maxId) {
                        maxId = item.getId();
                    }
                }
                this.nodeInfo.setId(maxId + 1);
                this.nodeInfo.setParentId(nodeInfoSelect.getId());
                this.nodeInfo.setDisplay("Group" + " " + "-" + " " + nodeDefineDTO.getOperatorDT());;
                this.nodeInfo.setOperator(nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setType("group");

                //this.listNodeInfo.add(nodeInfo);
            }
            if (this.nodeInfoEditModeGroup == true) {
                this.nodeInfo = new NodeInfo();

                Long maxId = 1L;
                for (NodeInfo item : listNodeInfo) {
                    if (item.getId() > maxId) {
                        maxId = item.getId();
                    }
                }
                Long minId = 1L;
                for (NodeInfo item : listNodeInfo) {
                    if (item.getId() < minId) {
                        minId = item.getId();
                    }
                }
                this.nodeInfo.setParentId(minId);
                this.nodeInfo.setId(maxId + 1);
                this.nodeInfo.setDisplay("Group" + " " + "-" + " " + nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setValue(nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setOperator(nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setType("group");

                // listNodeInfo.add(nodeInfo);
            }
            //rootNodeFuzzy = genTreeFuzzy(listNodeInfo);
        }
        if (this.nodeInfoEditMode == true) {
            NodeInfo nodeInfoSelect = (NodeInfo) selectedNode.getData();
            for (NodeInfo item : listNodeInfo) {
                if (Objects.equals(item.getId(), nodeInfoSelect.getId())) {
                    item.setDisplay("Group" + " " + "-" + " " + nodeDefineDTO.getOperatorDT());
                    item.setOperator(nodeDefineDTO.getOperatorDT());
                }
            }
            //rootNodeFuzzy = genTreeFuzzy(listNodeInfo);
        }

//        rootNodeFuzzy = genTreeFuzzy(this.listNodeInfo);
        refreshTreeNodeInfoGroup();
        TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfo);
        genField(togenfield);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    private void refreshTreeNodeInfo() {
        if (expressionEditMode) {

        } else {
            listNodeInfo.add(this.nodeInfo);
        }
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                this.nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT(item.getOperator());
                this.dem = 0d;
                for (NodeInfo items : listNodeInfo) {
                    if (Objects.equals(items.getParentId(), item.getId())) {
                        dem++;
                    }
                }
                if (dem >= 2) {
                    this.renderedCbb = true;
                } else {
                    this.renderedCbb = false;
                }
            }
        }
        rootNodeFuzzy = genTreeFuzzy(listNodeInfo);
        processRefreshNodeInfo(rootNodeFuzzy, selectedNode, nodeInfo, expressionEditMode);
    }

    private void refreshTreeNodeInfoGroup() {
        if (nodeInfoEditMode) {

        } else {
            listNodeInfo.add(this.nodeInfo);
        }
        for (NodeInfo item : listNodeInfo) {
            if (item.getParentId() == null) {
                this.nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT(item.getOperator());
                this.dem = 0d;
                for (NodeInfo items : listNodeInfo) {
                    if (Objects.equals(items.getParentId(), item.getId())) {
                        dem++;
                    }
                }
                if (dem >= 2) {
                    this.renderedCbb = true;
                } else {
                    this.renderedCbb = false;
                }
            }
        }
        rootNodeFuzzy = genTreeFuzzy(listNodeInfo);
        processRefreshNodeInfo(rootNodeFuzzy, selectedNode, nodeInfo, nodeInfoEditMode);
    }

    public void prepareEditSegmentEvaluationInfo(SegmentEvaluationInfo segmentEvaluationInfo) {
        if ("".equals(this.campaignEvaluationInfo.getCampaignEvaluationInfoName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.emptyNumber"), "Campaign EvaluationInfo Name");
        } else {
            this.segmentEvaluationInfoEditMode = true;
            this.disableSegmentEvaluationInfo = false;
            this.segmentEvaluationInfoEdit = new SegmentEvaluationInfo();
            this.segment = campaignEvaluationInforService.getSegmentByName(segmentEvaluationInfo.getSegmentName());
            this.segmentNameMap = this.segment.getName();
            setSegmentEvaluationInfo(segmentEvaluationInfoEdit, segmentEvaluationInfo);
            segmentEvaluationInfoEdit.setListRuleEvaluationInfo(cloneRuleEvaluationInfo(segmentEvaluationInfo.getListRuleEvaluationInfo()));
            segmentEvaluationInfoEdit.setListResultClass(cloneResultClass(segmentEvaluationInfo.getListResultClass()));
            segmentEvaluationInfoEdit.setListFuzzyRule(cloneFuzzyRule(segmentEvaluationInfo.getListFuzzyRule()));

            this.listRuleEvaluationInfoOfSegment = segmentEvaluationInfoEdit.getListRuleEvaluationInfo();
            this.listResultClassOfSement = segmentEvaluationInfoEdit.getListResultClass();
            this.listFuzzyRuleOfSegment = segmentEvaluationInfoEdit.getListFuzzyRule();

            List<ResultClass> listClassId = new ArrayList<>();
            for (ResultClass item : this.listResultClassToDelete) {
                for (ResultClass items : this.listResultClassOfSement) {
                    if (Objects.equals(item.getClassId(), items.getClassId())) {
                        listClassId.add(item);
                    }
                }
            }
            listResultClassToDelete.removeAll(listClassId);

            List<FuzzyRule> listRuleId = new ArrayList<>();
            for (FuzzyRule item : this.listFuzzyRuleToDelete) {
                for (FuzzyRule items : this.listFuzzyRuleOfSegment) {
                    if (Objects.equals(item.getFuzzyRuleId(), items.getFuzzyRuleId())) {
                        listRuleId.add(item);
                    }
                }
            }
            listFuzzyRuleToDelete.removeAll(listRuleId);

        }
    }

    public List<RuleEvaluationInfo> cloneRuleEvaluationInfo(List<RuleEvaluationInfo> list) {
        List<RuleEvaluationInfo> listRuleEvaluationInfoClone = new ArrayList<>();
        for (RuleEvaluationInfo item : list) {
            RuleEvaluationInfo ruleEvaluationInfo = new RuleEvaluationInfo();
            setRuleEvaluationInfo(ruleEvaluationInfo, item);
            listRuleEvaluationInfoClone.add(ruleEvaluationInfo);
        }
        return listRuleEvaluationInfoClone;
    }

    public List<ResultClass> cloneResultClass(List<ResultClass> list) {
        List<ResultClass> listResultClassClone = new ArrayList<>();
        for (ResultClass item : list) {
            ResultClass resultClass = new ResultClass();
            resultClass.setClassId(item.getClassId());
            resultClass.setOwnerId(item.getOwnerId());
            resultClass.setCampaignEvaluationInfoId(item.getCampaignEvaluationInfoId());
            resultClass.setClassName(item.getClassName());
            resultClass.setDescription(item.getDescription());
            resultClass.setClassLevel(item.getClassLevel());
            listResultClassClone.add(resultClass);
        }
        return listResultClassClone;
    }

    public List<FuzzyRule> cloneFuzzyRule(List<FuzzyRule> list) {
        List<FuzzyRule> listFuzzyRuleClone = new ArrayList<>();
        for (FuzzyRule item : list) {
            FuzzyRule fuzzyRule = new FuzzyRule();
            setFuzzyRule(fuzzyRule, item);
            listFuzzyRuleClone.add(fuzzyRule);
        }
        return listFuzzyRuleClone;
    }

    public void prepareCreateSegmentEvaluationInfo() {
        if ("".equals(this.campaignEvaluationInfo.getCampaignEvaluationInfoName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.emptyNumber"), "Campaign EvaluationInfo Name");
        } else if (this.campaign.getCampaignId() != null) {
            this.disableSegmentEvaluationInfo = false;
            this.segmentEvaluationInfoEditMode = false;

            this.segmentEvaluationInfoEdit = new SegmentEvaluationInfo();
            this.segmentEvaluationInfoEdit = campaignEvaluationInforService.getNextSequenceSegmentEvaluationInFo();
            this.segmentEvaluationInfoEdit.setCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.segmentEvaluationInfoEdit.setCampaignEvaluationInfoName(campaignEvaluationInfo.getCampaignEvaluationInfoName());
            this.segment = new Segment();
            this.segmentNameMap = null;

            this.segmentEvaluationInfoEdit.setListRuleEvaluationInfo(new ArrayList<>());
            this.segmentEvaluationInfoEdit.setListResultClass(new ArrayList<>());
            this.segmentEvaluationInfoEdit.setListFuzzyRule(new ArrayList<>());

            this.segmentEvaluationInfoEdit.setListRuleEvaluationInfoBefor(new ArrayList<>());
            this.segmentEvaluationInfoEdit.setListResultClassBefor(new ArrayList<>());
            this.segmentEvaluationInfoEdit.setListFuzzyRuleBefor(new ArrayList<>());

            this.listRuleEvaluationInfoOfSegment = segmentEvaluationInfoEdit.getListRuleEvaluationInfo();
            this.listResultClassOfSement = segmentEvaluationInfoEdit.getListResultClass();
            this.listFuzzyRuleOfSegment = segmentEvaluationInfoEdit.getListFuzzyRule();
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.create.segment"));
        }
    }

    public void showSegmentEvaluationInfo(SegmentEvaluationInfo segmentEvaluationInfo) {
        this.disableSegmentEvaluationInfo = true;
        this.segmentEvaluationInfo = segmentEvaluationInfo;
        this.segmentEvaluationInfoEdit = new SegmentEvaluationInfo();
        setSegmentEvaluationInfo(segmentEvaluationInfoEdit, segmentEvaluationInfo);
        this.segment = campaignEvaluationInforService.getSegmentByName(segmentEvaluationInfoEdit.getSegmentName());

        segmentEvaluationInfoEdit.setListRuleEvaluationInfo(cloneRuleEvaluationInfo(segmentEvaluationInfo.getListRuleEvaluationInfo()));
        segmentEvaluationInfoEdit.setListResultClass(cloneResultClass(segmentEvaluationInfo.getListResultClass()));
        segmentEvaluationInfoEdit.setListFuzzyRule(cloneFuzzyRule(segmentEvaluationInfo.getListFuzzyRule()));

        this.listRuleEvaluationInfoOfSegment = segmentEvaluationInfoEdit.getListRuleEvaluationInfo();
        this.listResultClassOfSement = segmentEvaluationInfoEdit.getListResultClass();
        this.listFuzzyRuleOfSegment = segmentEvaluationInfoEdit.getListFuzzyRule();
    }

    public void setSegmentEvaluationInfo(SegmentEvaluationInfo a, SegmentEvaluationInfo b) {
        a.setSegmentEvaluationInfoId(b.getSegmentEvaluationInfoId());
        a.setCampaignEvaluationInfoId(b.getCampaignEvaluationInfoId());
        a.setSegmentId(b.getSegmentId());
        a.setSegmentReasioningMethod(b.getSegmentReasioningMethod());
        a.setSegmentWeight(b.getSegmentWeight());
        a.setDescription(b.getDescription());
        a.setCampaignEvaluationInfoName(b.getCampaignEvaluationInfoName());
        a.setSegmentName(b.getSegmentName());
        a.setListFuzzyRule(b.getListFuzzyRule());
        a.setListResultClass(b.getListResultClass());
        a.setListRuleEvaluationInfo(b.getListRuleEvaluationInfo());
    }

    public void deleteSegmentEvaluation(SegmentEvaluationInfo segmentEvaluationInfo) {
        this.segmentEvaluationInfo = segmentEvaluationInfo;
        if (validateDeleteSegmentEvaluationInfo()) {
            if (this.segmentEvaluationInfo != null) {
                this.listSegmentEvaluationInfo.remove(this.segmentEvaluationInfo);
                this.listSegmentEvaluationInfoToDelete.add(this.segmentEvaluationInfo);
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.delete.segment"));
        }

    }

    public boolean validateSegmentEvaluationInfo() {
        SegmentEvaluationInfo segmentEvaluationInfo = this.segmentEvaluationInfoEdit;

        if (DataUtil.isStringNullOrEmpty(segmentEvaluationInfo.getSegmentName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Segment Name");
            return false;
        }
        if (segmentEvaluationInfo.getSegmentWeight() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.weight"), "Segment Weight");
            return false;
        }
        if (segmentEvaluationInfo.getSegmentWeight() < 0 || segmentEvaluationInfo.getSegmentWeight() > 1) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.weight"), "Segment Weight");
            return false;
        }
        if (!DataUtil.checkMaxlength(segmentEvaluationInfo.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(segmentEvaluationInfo.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.special.characters"), "Description");
            return false;
        }
        this.count = 0;
        for (RuleEvaluationInfo item : listRuleEvaluationInfoOfSegment) {
            if (item != null) {
                Double value = item.getRuleWeight() * 100;
                count = count + value.intValue();
            }
        }
        if (count != 100) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.count.weight"), "Rule");
            return false;
        }
        if (validateResultClassSegment() == false) {
            return false;
        }

        this.count = 0;
        for (FuzzyRule item : listFuzzyRuleOfSegment) {
            if (item != null) {
                Double value = item.getWeight() * 100;
                count = count + value.intValue();
            }
        }
        if (count != 100) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.count.weight"), "fuzzy");
            return false;
        }
        return true;
    }

    public boolean validateResultClassSegment() {
        if (listResultClassOfSement.isEmpty()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "List Result Class");
            return false;
        } else {
            for (ResultClass item : listResultClassOfSement) {
                if (!DataUtil.checkMaxlength(item.getDescription())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description Result class");
                    return false;
                }
                if (!DataUtil.checkNotContainPercentage(item.getDescription())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.special.characters"), "Description Result class");
                    return false;
                }
                if (DataUtil.isStringNullOrEmpty(item.getClassName())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Result Class Name");
                    return false;
                }
                if (!DataUtil.checkMaxlength(item.getClassName())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Result Class Name");
                    return false;
                }
                if (!DataUtil.checkNotContainPercentage(item.getClassName())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.special.characters"), "Result Class Name");
                    return false;
                }
            }
            for (ResultClass item : listResultClassOfSement) {
                for (ResultClass items : listResultClassOfSement) {
                    if (Objects.equals(item.getClassId(), items.getClassId())) {
                        continue;
                    }
                    if (item.getClassName().equals(items.getClassName())) {
                        errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.resultName"));
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void saveSegmentEvaluationInfo() {
        if (this.segmentEvaluationInfoEditMode == false) {
            segmentEvaluationInfoEdit.setListResultClass(this.listResultClassOfSement);
            segmentEvaluationInfoEdit.setListFuzzyRule(this.listFuzzyRuleOfSegment);
            segmentEvaluationInfoEdit.setListRuleEvaluationInfo(this.listRuleEvaluationInfoOfSegment);
            segmentEvaluationInfoEdit.setCampaignEvaluationInfoName(this.campaignEvaluationInfo.getCampaignEvaluationInfoName());
            this.listSegmentEvaluationInfo.add(segmentEvaluationInfoEdit);

            segmentEvaluationInfoEdit.setListFuzzyRule(this.listFuzzyRuleOfSegment);
            segmentEvaluationInfoEdit.setListResultClass(this.listResultClassOfSement);
            segmentEvaluationInfoEdit.setListRuleEvaluationInfo(this.listRuleEvaluationInfoOfSegment);

            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));

        } else {
            for (SegmentEvaluationInfo item : listSegmentEvaluationInfo) {
                if (Objects.equals(item.getSegmentEvaluationInfoId(), segmentEvaluationInfoEdit.getSegmentEvaluationInfoId())) {
                    segmentEvaluationInfoEdit.setListResultClass(this.listResultClassOfSement);
                    segmentEvaluationInfoEdit.setListFuzzyRule(this.listFuzzyRuleOfSegment);
                    segmentEvaluationInfoEdit.setListRuleEvaluationInfo(this.listRuleEvaluationInfoOfSegment);
                    setSegmentEvaluationInfo(item, segmentEvaluationInfoEdit);
                    if (!item.getSegmentName().equals(segmentNameMap)) {
                        if (this.segmentNameMap != null) {
                            if (!this.listFuzzyRule.isEmpty()) {
                                List<Long> longs = checkChooseNameInFuzzyRule(segmentNameMap, this.listFuzzyRule);
                                for (Long id : longs) {
                                    for (FuzzyRule items : listFuzzyRule) {
                                        if (Objects.equals(id, items.getFuzzyRuleId())) {
                                            this.listFuzzyRuleToDelete.add(items);
                                            this.listFuzzyRule.remove(items);
                                            if (this.listFuzzyRule.isEmpty()) {
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public void buildCampaignForSegment() {
        if (this.campaign.getIsDefault() == 1) {

            this.categorieSegment = categoryService.getCategoryByType(CatagoryType.CATEGORY_SEGMENT_TYPE);
            List<Long> longs = new ArrayList<>();
            if (!categorieSegment.isEmpty()) {
                categorieSegment.forEach(item -> longs.add(item.getCategoryId()));
            }
            this.listSegment = categoryService.getDataFromCatagoryId(longs, Constants.SEGMENT_TABLE);
            rootNodeSegment = treeUtilsService.createTreeCategoryAndComponentCampaignEvaluation(categorieSegment, listSegment);
            rootNodeSegment.getChildren().get(0).setExpanded(true);
            addExpandedNode(rootNodeSegment.getChildren().get(0));

        }
        if (this.campaign.getIsDefault() == 0) {

            rootNodeSegment = new DefaultTreeNode(null, null);
            this.listSegment = campaignEvaluationInforService.getSegmentBuildTree(this.campaign.getCampaignId()); // lay list segment tuong ung
            for (Segment itt : listSegment) {
                new DefaultTreeNode("object", itt, rootNodeSegment);
            }

        }
    }

    public void chooseSegmentForSegmentEvaluation() {

        this.segment = (Segment) selectedNode.getData();
        segmentEvaluationInfoEdit.setSegmentName(segment.getSegmentName());
        segmentEvaluationInfoEdit.setSegmentId(segment.getSegmentId());

        for (ResultClass item : listResultClassOfSement) {
            this.listResultClassToDelete.add(item);
        }
        this.listResultClassOfSement = new ArrayList<>();
        for (FuzzyRule item : listFuzzyRuleOfSegment) {
            this.listFuzzyRuleToDelete.add(item);
        }
        this.listFuzzyRuleOfSegment = new ArrayList<>();

        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));

    }

    public List<Long> checkChooseNameInFuzzyRule(String name, List<FuzzyRule> lst) {
        List<Long> longs = new ArrayList<>();
        for (FuzzyRule item : lst) {
            List<NodeInfo> listDisplayExpression = new ArrayList<>();
            listDisplayExpression.addAll(getListNodeInfoFromDisplayExpress(item.getDisplayExpression()));
            for (NodeInfo items : listDisplayExpression) {
                String[] text = items.getDisplay().split(" ");
                for (int i = 0; i < text.length; i++) {
                    if ("is".equals(text[i])) {
                        if (items.getDisplay().substring(0, items.getDisplay().indexOf(text[i]) - 1).trim().equals(name)) {
                            longs.add(item.getFuzzyRuleId());
                        }
                    }
                    if ("not".equals(text[i])) {
                        if (items.getDisplay().substring(0, items.getDisplay().indexOf(text[i]) - 1).trim().equals(name)) {
                            longs.add(item.getFuzzyRuleId());
                        }
                    }
                }
            }
        }
        return longs;
    }

    public boolean validateChooseCampaignForSegment() {
        if (!Objects.nonNull(selectedNode)) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.segment"));
            return false;
        }
        if (selectedNode.getType().equals("category")) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.segment"));
            return false;
        } else {
            Segment segmentMap = new Segment();
            segmentMap = (Segment) selectedNode.getData();
            if (Objects.equals(this.segment.getSegmentId(), segmentMap.getSegmentId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.segment"));
                return false;
            }

            if (selectedNode.getType().equals("category")) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.segment"));
                return false;
            }
            if (Objects.nonNull(selectedNode)) {
                this.segment = (Segment) selectedNode.getData();
                this.dem = 0d;
                for (SegmentEvaluationInfo item : listSegmentEvaluationInfo) {
                    if (Objects.equals(item.getSegmentId(), segment.getSegmentId())) {
                        dem++;
                    }
                }
                if (dem != 0) {
                    errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.segment.name"));
                    return false;
                }
            }
        }
        return true;
    }
    //RuleOFSegment

    public void prepareEditRuleSegment(RuleEvaluationInfo ruleEvaluationInfo) {
        this.ruleSegmentEditMode = true;
        this.disabledRuleSegment = false;
        this.ruleEvaluationInfoEdit = new RuleEvaluationInfo();
        setRuleEvaluationInfo(ruleEvaluationInfoEdit, ruleEvaluationInfo);
        this.ruleNameMap = ruleEvaluationInfoEdit.getRuleName();
        this.assessmentRule = assessmentRuleService.findAssessmentRuleById(this.ruleEvaluationInfoEdit.getAssessmentRuleId());
    }

    public void prepareCreateRuleSegment() {
        if (this.segment.getName() == null) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.segment"));
        } else {
            this.ruleSegmentEditMode = false;
            this.disabledRuleSegment = false;
            this.ruleNameMap = null;
            this.assessmentRule = new AssessmentRule();
            this.ruleEvaluationInfo = new RuleEvaluationInfo();
            this.ruleEvaluationInfoEdit = new RuleEvaluationInfo();
            this.ruleEvaluationInfo = campaignEvaluationInforService.getNextSequenceRuleEvaluation();
            this.ruleEvaluationInfo.setRuleLevel(2L);
            this.ruleEvaluationInfo.setOwnerId(segmentEvaluationInfoEdit.getSegmentEvaluationInfoId());
            setRuleEvaluationInfo(ruleEvaluationInfoEdit, ruleEvaluationInfo);
        }
    }

    public void doSaveRuleSegment() {
        if (ruleSegmentEditMode == false) {
            setRuleEvaluationInfo(this.ruleEvaluationInfo, ruleEvaluationInfoEdit);
            this.listRuleEvaluationInfoOfSegment.add(ruleEvaluationInfo);
//            this.listALLRuleEvaluationInfoOfSegment.add(ruleEvaluationInfo);

        }
        if (ruleSegmentEditMode == true) {
            {
                for (RuleEvaluationInfo item : listRuleEvaluationInfoOfSegment) {
                    if (Objects.equals(item.getRuleEvaluationInfoId(), this.ruleEvaluationInfoEdit.getRuleEvaluationInfoId())) {
                        setRuleEvaluationInfo(item, ruleEvaluationInfoEdit);
                    }
                }
                if (!this.ruleEvaluationInfoEdit.getRuleName().equals(this.ruleNameMap)) {
                    if (this.ruleNameMap != null) {
                        if (!this.listFuzzyRuleOfSegment.isEmpty()) {
                            List<Long> longs = checkChooseNameInFuzzyRule(ruleNameMap, this.listFuzzyRuleOfSegment);
                            for (Long id : longs) {
                                for (FuzzyRule item : listFuzzyRuleOfSegment) {
                                    if (Objects.equals(id, item.getFuzzyRuleId())) {
                                        this.listFuzzyRuleToDelete.add(item);
                                        this.listFuzzyRuleOfSegment.remove(item);
                                        if (this.listFuzzyRuleOfSegment.isEmpty()) {
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public void deleteRuleSegment(RuleEvaluationInfo ruleEvaluationInfo) {
        this.ruleEvaluationInfo = ruleEvaluationInfo;
        if (validateDeleteRuleEvaluationOfSegment()) {
            if (ruleEvaluationInfo != null) {
                this.listRuleEvaluationInfoOfSegment.remove(ruleEvaluationInfo);
//                this.listALLRuleEvaluationInfoOfSegment.remove(ruleEvaluationInfo);
                this.listRuleEvaluationInfoToDelete.add(ruleEvaluationInfo);
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.delete.assessmentSegment"));
        }
    }

    public void chooseAssessmentRuleSegment() {
        this.assessmentRule = (AssessmentRule) selectedNode.getData();
        this.ruleEvaluationInfoEdit.setAssessmentRuleId(this.assessmentRule.getRuleId());
        this.ruleEvaluationInfoEdit.setRuleName(this.assessmentRule.getRuleName());

        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public boolean validateAssessmentRule() {
        if (!Objects.nonNull(selectedNode)) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.assessment"));
            return false;
        }
        if (selectedNode.getType().equals("category")) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
            return false;
        }
        return true;
    }

    public void showRuleSegment(RuleEvaluationInfo ruleEvaluationInfo) {
        this.disabledRuleSegment = true;
        this.ruleEvaluationInfoEdit = ruleEvaluationInfo;
        this.assessmentRule = assessmentRuleService.findAssessmentRuleById(this.ruleEvaluationInfoEdit.getAssessmentRuleId());
    }

    public boolean validateAssessmentRuleOfSegment() {
        RuleEvaluationInfo ruleEvaluationInfo = this.ruleEvaluationInfoEdit;
        if (DataUtil.isStringNullOrEmpty(ruleEvaluationInfo.getRuleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Rule Name");
            return false;
        }
        if (ruleEvaluationInfo.getRuleWeight() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.emptyNumber"), "Rule Weight");
            return false;
        }
        if (segmentEvaluationInfoEdit.getSegmentId() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Owner Name");
            return false;
        }
        if (ruleEvaluationInfo.getRuleWeight() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.weight"), "Rule Weight");
            return false;
        }
        if (ruleEvaluationInfo.getRuleWeight() < 0 || ruleEvaluationInfo.getRuleWeight() > 1) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.weight"), "Rule Weight");
            return false;
        }
        if (!DataUtil.checkMaxlength(ruleEvaluationInfo.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(ruleEvaluationInfo.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.special.characters"), "Description");
            return false;
        }
        return true;
    }

    //Result Segment
    public void prepareCreateResultClassSegmemnt() {
        if (this.segmentEvaluationInfoEdit.getSegmentId() == null) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.segment"));
        } else {
            this.resultClass = new ResultClass();
            this.resultClass = campaignEvaluationInforService.getNextSequenceResultClass();
            this.resultClass.setClassLevel(2);
            this.resultClass.setCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.resultClass.setOwnerId(segmentEvaluationInfoEdit.getSegmentId());
            this.listResultClassOfSement.add(this.resultClass);
//        this.listALLResultClassOfSement.add(this.resultClass);
        }
    }

    public void editResultClassNameSegment() {
        for (ResultClass item : listResultClassOfSement) {
            if (item != null) {
                item.setClassName(item.getClassName().replaceAll(" +", " "));
            }
        }
        for (FuzzyRule item : listFuzzyRuleOfSegment) {
            for (ResultClass items : listResultClassOfSement) {
                if (Objects.equals(item.getClassId(), items.getClassId())) {
                    item.setClassName(items.getClassName());
                }
            }
        }

    }

    public void editResultClassDecriptionSegment() {
        for (ResultClass item : listResultClassOfSement) {
            if (item != null) {
                item.setDescription(item.getDescription().replaceAll(" +", " "));
            }
        }

    }

    public void deleteResultClassSegment(ResultClass resultClass) {
        this.resultClass = resultClass;
        if (validateDeleteResultClassOfSegment()) {
            if (resultClass != null) {
                this.listResultClassOfSement.remove(resultClass);
//                this.listALLResultClassOfSement.remove(resultClass);
                this.listResultClassToDelete.add(resultClass);
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
            }
        }
    }

    //Fuzzy Segment
    public void prepareEditFuzzySegment(FuzzyRule fuzzyRule) {
        if (this.segment.getName() == null) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.segment"));

        } else {
            if (validateResultClassSegment()) {
                this.fuuzyRuleSegmentEditMode = true;
                this.focusComboboxResultSegment = false;
                this.disableFuzzySegment = false;
                this.fuzzyRuleEdit = new FuzzyRule();
                setFuzzyRule(fuzzyRuleEdit, fuzzyRule);
                this.chooseResultClass = new ArrayList<>();
                this.chooseResultClass = this.listResultClassOfSement;
                listNodeInfoOfSegment = getListNodeInfoFromDisplayExpress(fuzzyRuleEdit.getDisplayExpression());
                rootNodeFuzzy = genTreeFuzzy(this.listNodeInfoOfSegment);
                for (NodeInfo item : listNodeInfoOfSegment) {
                    if (item.getParentId() == null) {
                        this.nodeDefineDTO = new NodeDefineDTO();
                        nodeDefineDTO.setOperatorDT(item.getOperator());
                        this.dem = 0d;
                        for (NodeInfo items : listNodeInfoOfSegment) {
                            if (Objects.equals(items.getParentId(), item.getId())) {
                                dem++;
                            }
                        }
                        if (dem >= 2) {
                            this.renderedCbbSegment = true;
                        } else {
                            this.renderedCbbSegment = false;
                        }
                    }
                }
            }
        }
    }

    public void prepareCreateFuzzySegment() {
        if (this.segment.getName() == null) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.segment"));
        } else {
            if (validateResultClassSegment()) {
                this.fuuzyRuleSegmentEditMode = false;
                this.focusComboboxResultSegment = false;
                this.disableFuzzySegment = false;
                this.fuzzyRuleEdit = new FuzzyRule();
                this.fuzzyRule = new FuzzyRule();
                this.fuzzyRule = campaignEvaluationInforService.getNextSequenceFuzzyRule();
                this.chooseResultClass = new ArrayList<>();
                this.chooseResultClass.addAll(this.listResultClassOfSement);
                this.fuzzyRule.setRuleLevel(2L);
                this.fuzzyRule.setCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
                this.fuzzyRule.setOwnerId(segmentEvaluationInfoEdit.getSegmentId());
                this.fuzzyRule.setDisplayExpression("");
                setFuzzyRule(fuzzyRuleEdit, fuzzyRule);
                this.listNodeInfoOfSegment = new ArrayList<>();
                NodeInfo item = new NodeInfo();
                item.setId(1L);
                item.setParentId(null);
                item.setType("group");
                this.renderedCbbSegment = false;
                item.setValue("");
                this.listNodeInfoOfSegment.add(item);
                nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT("and");
                item.setOperator(nodeDefineDTO.getOperatorDT());
                rootNodeFuzzy = genTreeFuzzy(this.listNodeInfoOfSegment);
            }
        }
    }

    public void showFuzzySegment(FuzzyRule fuzzyRule) {
        this.disableFuzzySegment = true;
        this.fuzzyRuleEdit = new FuzzyRule();
        setFuzzyRule(fuzzyRuleEdit, fuzzyRule);
        this.chooseResultClass = new ArrayList<>();
        this.chooseResultClass.addAll(this.listResultClassOfSement);
        listNodeInfoOfSegment = getListNodeInfoFromDisplayExpress(fuzzyRuleEdit.getDisplayExpression());
        rootNodeFuzzy = genTreeFuzzy(this.listNodeInfoOfSegment);
        for (NodeInfo item : listNodeInfoOfSegment) {
            if (item.getParentId() == null) {
                this.nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT(item.getOperator());
                this.dem = 0d;
                for (NodeInfo items : listNodeInfoOfSegment) {
                    if (Objects.equals(items.getParentId(), item.getId())) {
                        dem++;
                    }
                }
                if (dem >= 2) {
                    this.renderedCbbSegment = true;
                } else {
                    this.renderedCbbSegment = false;
                }
            }
        }
    }

    public void deleteFuzzyRuleSegment(FuzzyRule fuzzyRule) {
        if (fuzzyRule != null) {
            this.listFuzzyRuleOfSegment.remove(fuzzyRule);
//            this.listALLFuzzyRuleOfSegment.remove(fuzzyRule);
            this.listFuzzyRuleToDelete.add(fuzzyRule);
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public void saveFuzzySegment() {
        if (this.fuuzyRuleSegmentEditMode == false) {
            for (ResultClass item : listResultClassOfSement) {
                if (Objects.equals(fuzzyRuleEdit.getClassId(), item.getClassId())) {
                    this.fuzzyRuleEdit.setClassName(item.getClassName());
                }
            }
            setFuzzyRule(this.fuzzyRule, fuzzyRuleEdit);
            this.listFuzzyRuleOfSegment.add(fuzzyRule);
            for (FuzzyRule item : listFuzzyRuleOfSegment) {
                item.setExpression(item.getDisplayExpression().trim().toLowerCase()
                        .replace("and", "&")
                        .replace("not", "#")
                        .replace("or", "||")
                        .replace("is", "_").replaceAll(" +", "_"));
                item.setCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
            }

//            this.listALLFuzzyRuleOfSegment.add(fuzzyRule);
        }
        if (this.fuuzyRuleSegmentEditMode == true) {
            for (ResultClass item : listResultClassOfSement) {
                if (Objects.equals(fuzzyRuleEdit.getClassId(), item.getClassId())) {
                    this.fuzzyRuleEdit.setClassName(item.getClassName());
                }
            }
            for (FuzzyRule item : listFuzzyRuleOfSegment) {
                if (Objects.equals(item.getFuzzyRuleId(), fuzzyRuleEdit.getFuzzyRuleId())) {
                    setFuzzyRule(item, fuzzyRuleEdit);
                }
                item.setExpression(item.getDisplayExpression().trim().toLowerCase()
                        .replace("and", "&")
                        .replace("not", "#")
                        .replace("or", "||")
                        .replace("is", "_").replaceAll(" +", "_"));
                item.setCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
            }

        }
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public void chooseRuleNameForFuzzyInSegment() {
        Expression expression = this.expression;
        this.listResultClassOfSegmentMap = new ArrayList<>();
        for (RuleEvaluationInfo item : listRuleEvaluationInfoOfSegment) {
            if (item.getRuleName() != null) {
                if (item.getRuleName().equals(expression.getContentEx())) {
                    this.listResultClassOfSegmentMap = campaignEvaluationInforService.getListResultClassByAssessmentRuleId(item.getAssessmentRuleId());
                }
            }
        }
    }

    public boolean validateFuzzyOfegment() {
        FuzzyRule fuzzyRule = this.fuzzyRuleEdit;

        if (fuzzyRule.getWeight() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.expresstion"), "Fuzzy weight");
            return false;
        }
        if (fuzzyRule.getWeight() < 0 || fuzzyRule.getWeight() > 1) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.weight"), "Fuzzy weight");
            return false;
        }
        if (!DataUtil.checkMaxlength(fuzzyRule.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (fuzzyRule.getClassId() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Result class");
            this.focusComboboxResultSegment = true;
            return false;
        }
        if (!DataUtil.checkMaxlength(fuzzyRule.getDisplayExpression())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Display Expression");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(fuzzyRule.getDisplayExpression())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Display Expression");
            return false;
        }
        if (!validateTreeFuzzySegment()) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.group.tree"));
            return false;
        }
        return true;
    }

    public void deleteEpressionOfSegment() {
        NodeInfo nodeInfo = (NodeInfo) selectedNode.getData();
        this.count = 0;
        for (NodeInfo item : listNodeInfoOfSegment) {
            if (Objects.equals(item.getParentId(), nodeInfo.getId())) {
                this.count++;
            }
        }
        if (count >= 1) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.delete.group"));
        } else {
            listNodeInfoOfSegment.remove(nodeInfo);

            rootNodeFuzzy = genTreeFuzzy(this.listNodeInfoOfSegment);
            for (NodeInfo item : listNodeInfoOfSegment) {
                if (item.getParentId() == null) {
                    this.nodeDefineDTO = new NodeDefineDTO();
                    nodeDefineDTO.setOperatorDT(item.getOperator());
                    this.dem = 0d;
                    for (NodeInfo items : listNodeInfoOfSegment) {
                        if (Objects.equals(items.getParentId(), item.getId())) {
                            dem++;
                        }
                    }
                    if (dem >= 2) {
                        this.renderedCbbSegment = true;
                    } else {
                        this.renderedCbbSegment = false;
                    }
                }
            }
            TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfoOfSegment);
            genFieldOfSegment(togenfield);
        }
    }

    public void saveOrEditExpressionSegment() {
        if (this.expressionEditMode == true) {
            NodeInfo nodeInfo = (NodeInfo) selectedNode.getData();
            for (NodeInfo item : listNodeInfoOfSegment) {
                if (Objects.equals(item.getId(), nodeInfo.getId())) {
                    item.setDisplay(expression.getContentEx() + " " + expression.getOperatorEx() + " " + expression.getValueEx());
                }
            }

        }
        if (this.expressionEditMode == false) {
            if (this.expressionEditModeGroup == false) {
                NodeInfo nodeSelect = (NodeInfo) selectedNode.getData();
                this.nodeInfo = new NodeInfo();

                Long maxId = 1L;
                for (NodeInfo item : listNodeInfoOfSegment) {
                    if (item.getId() > maxId) {
                        maxId = item.getId();
                    }
                }

                this.nodeInfo.setId(maxId + 1);
                this.nodeInfo.setParentId(nodeSelect.getId());
                this.nodeInfo.setDisplay(expression.getContentEx() + " " + expression.getOperatorEx() + " " + expression.getValueEx());
                this.nodeInfo.setValue(this.nodeInfo.getDisplay());
                this.nodeInfo.setType("object");

            }
            if (this.expressionEditModeGroup == true) {

                this.nodeInfo = new NodeInfo();

                Long maxId = 1L;
                for (NodeInfo item : listNodeInfoOfSegment) {
                    if (item.getId() > maxId) {
                        maxId = item.getId();
                    }
                }

                Long minId = 1L;
                for (NodeInfo item : listNodeInfoOfSegment) {
                    if (item.getId() < minId) {
                        minId = item.getId();
                    }
                }

                this.nodeInfo.setId(maxId + 1);
                this.nodeInfo.setParentId(minId);
                this.nodeInfo.setDisplay(expression.getContentEx() + " " + expression.getOperatorEx() + " " + expression.getValueEx());
                this.nodeInfo.setValue(this.nodeInfo.getDisplay());
                this.nodeInfo.setType("object");

            }

        }

        refreshTreeNodeInfoSegment();
        TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfoOfSegment);
        genFieldOfSegment(togenfield);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public void saveOrEditGroupSegment() {
        if (this.nodeInfoEditMode == false) {
            if (this.nodeInfoEditModeGroup == false) {
                NodeInfo nodeInfoSelect = (NodeInfo) selectedNode.getData();
                this.nodeInfo = new NodeInfo();

                Long maxId = 1L;
                for (NodeInfo item : listNodeInfoOfSegment) {
                    if (item.getId() > maxId) {
                        maxId = item.getId();
                    }
                }
                this.nodeInfo.setId(maxId + 1);
                this.nodeInfo.setParentId(nodeInfoSelect.getId());
                this.nodeInfo.setDisplay("Group" + " " + "-" + " " + nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setValue(this.nodeInfo.getDisplay());
                this.nodeInfo.setOperator(nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setType("group");

            }
            if (this.nodeInfoEditModeGroup == true) {
                this.nodeInfo = new NodeInfo();

                Long maxId = 1L;
                for (NodeInfo item : listNodeInfoOfSegment) {
                    if (item.getId() > maxId) {
                        maxId = item.getId();
                    }
                }
                Long minId = 1L;
                for (NodeInfo item : listNodeInfoOfSegment) {
                    if (item.getId() < minId) {
                        minId = item.getId();
                    }
                }
                this.nodeInfo.setParentId(minId);
                this.nodeInfo.setId(maxId + 1);
                this.nodeInfo.setDisplay("Group" + " " + "-" + " " + nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setValue(this.nodeInfo.getDisplay());
                this.nodeInfo.setOperator(nodeDefineDTO.getOperatorDT());
                this.nodeInfo.setType("group");

            }
            //rootNodeFuzzy = genTreeFuzzy(listNodeInfo);
        }
        if (this.nodeInfoEditMode == true) {
            NodeInfo nodeInfoSelect = (NodeInfo) selectedNode.getData();
            for (NodeInfo item : listNodeInfoOfSegment) {
                if (Objects.equals(item.getId(), nodeInfoSelect.getId())) {
                    item.setDisplay("Group" + " " + "-" + " " + nodeDefineDTO.getOperatorDT());
                    item.setOperator(nodeDefineDTO.getOperatorDT());
                }
            }
            //rootNodeFuzzy = genTreeFuzzy(listNodeInfo);
        }

        refreshTreeNodeInfoGroupSegment();
        TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfoOfSegment);
        genFieldOfSegment(togenfield);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    private void refreshTreeNodeInfoSegment() {
        if (expressionEditMode) {

        } else {
            listNodeInfoOfSegment.add(this.nodeInfo);
        }
        for (NodeInfo item : listNodeInfoOfSegment) {
            if (item.getParentId() == null) {
                this.nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT(item.getOperator());
                this.dem = 0d;
                for (NodeInfo items : listNodeInfoOfSegment) {
                    if (Objects.equals(items.getParentId(), item.getId())) {
                        dem++;
                    }
                }
                if (dem >= 2) {
                    this.renderedCbbSegment = true;
                } else {
                    this.renderedCbbSegment = false;
                }
            }
        }
        rootNodeFuzzy = genTreeFuzzy(listNodeInfoOfSegment);
        processRefreshNodeInfo(rootNodeFuzzy, selectedNode, nodeInfo, expressionEditMode);
    }

    private void refreshTreeNodeInfoGroupSegment() {
        if (nodeInfoEditMode) {

        } else {
            listNodeInfoOfSegment.add(this.nodeInfo);
        }
        for (NodeInfo item : listNodeInfoOfSegment) {
            if (item.getParentId() == null) {
                this.nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT(item.getOperator());
                this.dem = 0d;
                for (NodeInfo items : listNodeInfoOfSegment) {
                    if (Objects.equals(items.getParentId(), item.getId())) {
                        dem++;
                    }
                }
                if (dem >= 2) {
                    this.renderedCbbSegment = true;
                } else {
                    this.renderedCbbSegment = false;
                }
            }
        }
        rootNodeFuzzy = genTreeFuzzy(listNodeInfoOfSegment);
        processRefreshNodeInfo(rootNodeFuzzy, selectedNode, nodeInfo, nodeInfoEditMode);
    }

    public void deleteFuzzyExpressionSegment() {
        for (int i = 0; i < listNodeInfoOfSegment.size(); i++) {
            if (listNodeInfoOfSegment.get(i).getParentId() != null) {
                listNodeInfoToDelete.add(listNodeInfoOfSegment.get(i));
            }
        }
        for (NodeInfo item : listNodeInfoToDelete) {
            if (item != null) {
                listNodeInfoOfSegment.remove(item);
            }
        }
        for (NodeInfo item : listNodeInfoOfSegment) {
            if (item.getParentId() == null) {
                this.nodeDefineDTO = new NodeDefineDTO();
                nodeDefineDTO.setOperatorDT(item.getOperator());
                this.dem = 0d;
                for (NodeInfo items : listNodeInfoOfSegment) {
                    if (Objects.equals(items.getParentId(), item.getId())) {
                        dem++;
                    }
                }
                if (dem >= 2) {
                    this.renderedCbbSegment = true;
                } else {
                    this.renderedCbbSegment = false;
                }
            }
        }
        rootNodeFuzzy = genTreeFuzzy(this.listNodeInfoOfSegment);
        TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfo);
        genFieldOfSegment(togenfield);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public void chooseOperratorGroupSegment() {
        for (NodeInfo item : listNodeInfoOfSegment) {
            if (item.getParentId() == null) {
                item.setOperator(this.nodeDefineDTO.getOperatorDT());
                item.setDisplay("Group" + " " + "-" + " " + this.nodeDefineDTO.getOperatorDT());
            }
        }
        rootNodeFuzzy = genTreeFuzzy(this.listNodeInfoOfSegment);
        TreeNode togenfield = genTreeFuzzyToGenDisplayExpression(this.listNodeInfoOfSegment);
        genFieldOfSegment(togenfield);
    }

    //     SAVE-ALL
    public void saveAll() {
        if (this.campaignEvaluationInfo != null) {
            if (campaignEvaluationInfo.getIsDefault() == true) {
                this.chekIsDeFault = true;
                //Save Or Update CampaignSchedule
                if (this.campaignSchedule != null) {
                    campaignSchedule.setCampaignId(this.campaign.getCampaignId());
                    campaignEvaluationInforService.doCreateOrUpdateCampaignSchedule(campaignSchedule);
                }

                //Save Or Update CampaignEvaluautionInfo
                if (this.campaignEvaluationInfo != null && this.campaignSchedule != null) {
                    this.campaignEvaluationInfo.setScheduleId(this.campaignSchedule.getScheduleId());
                    this.campaignEvaluationInfo.setCampaignId(this.campaign.getCampaignId());
                    this.campaignEvaluationInfo.setName(this.campaign.getName());
                    campaignEvaluationInforService.doCreateOrUpdateCampaignEvaluationInfo(campaignEvaluationInfo);
                    //TreeNode newNode = new DefaultTreeNode("object", campaignEvaluationInfo, selectedNode);
                }
                for (CampaignEvaluationInfo item : listCampaignEvaluationInfo) {
                    if (this.campaignEvaluationInfo != null) {
                        if (Objects.equals(item.getCampaignEvaluationInfoId(), this.campaignEvaluationInfo.getCampaignEvaluationInfoId())) {
                            cloneCampaignEvaluationInfo(item, this.campaignEvaluationInfo);
                        }
                    }
                }

                //delete segment (khi chuyen notDefault qua)
                for (SegmentEvaluationInfo item : listSegmentEvaluationInfoToDelete) {
                    if (this.campaignEvaluationInfo != null) {
                        this.listFuzzyRuleOfSegment = campaignEvaluationInforService.getListFuzzyRuleOfSegment(item.getSegmentId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
                        this.listFuzzyRuleToDelete.addAll(this.listFuzzyRuleOfSegment);
                        this.listResultClassOfSement = campaignEvaluationInforService.getListResultClassOfSegment(item.getSegmentId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
                        this.listResultClassToDelete.addAll(this.listResultClassOfSement);
                        this.listRuleEvaluationInfoOfSegment = campaignEvaluationInforService.getListRuleEvaluationInfoOfSegment(item.getSegmentEvaluationInfoId());
                        this.listRuleEvaluationInfoToDelete.addAll(this.listRuleEvaluationInfoOfSegment);

                        campaignEvaluationInforService.deleteSegmentEvaluationInfo(item.getSegmentEvaluationInfoId());
                    }

                }

                //Save RuleEvaluationInfo
                for (RuleEvaluationInfo item : listRuleEvaluationInfo) {

                    campaignEvaluationInforService.doCreateOrUpdateRuleEvaluationInfo(item);

                }
                //delete RuleEvaluationInfo
                for (RuleEvaluationInfo item : listRuleEvaluationInfoToDelete) {

                    campaignEvaluationInforService.deleteRuleEvaluationInfo(item.getRuleEvaluationInfoId());

                }

                //Save Or Update ResultClass
                for (ResultClass item : listResultClass) {

                    campaignEvaluationInforService.doCreateOrUpdateResultClass(item);

                }
                //Delete ResultClass
                for (ResultClass item : listResultClassToDelete) {

                    campaignEvaluationInforService.deleteResultClass(item.getClassId());
                }

                //Save Or Update FuzzyRule
                for (FuzzyRule item : listFuzzyRule) {

                    campaignEvaluationInforService.doCreateOrUpdateFuzzyRule(item);

                }
                //Delete Fuzzy

                for (FuzzyRule item : listFuzzyRuleToDelete) {

                    campaignEvaluationInforService.deleteFuzzyRule(item.getFuzzyRuleId());

                }

            }

            if (campaignEvaluationInfo.getIsDefault() == false) {
                this.chekIsDeFault = false;
                //Save Or Update CampaignSchedule
                if (this.campaignSchedule != null) {
                    campaignSchedule.setCampaignId(this.campaign.getCampaignId());
                    campaignEvaluationInforService.doCreateOrUpdateCampaignSchedule(campaignSchedule);
                }

                //Save Or Update CampaignEvaluautionInfo
                if (this.campaignEvaluationInfo != null) {
                    if (this.campaignEvaluationInfo != null) {
                        this.campaignEvaluationInfo.setScheduleId(campaignSchedule.getScheduleId());
                        this.campaignEvaluationInfo.setName(this.campaign.getName());
                        this.campaignEvaluationInfo.setCampaignId(campaign.getCampaignId());
                        campaignEvaluationInforService.doCreateOrUpdateCampaignEvaluationInfo(campaignEvaluationInfo);
                        //TreeNode newNode = new DefaultTreeNode("object", campaignEvaluationInfo, selectedNode);
                    }
                }
                this.listCampaignEvaluationInfo.add(this.campaignEvaluationInfo);
                for (CampaignEvaluationInfo item : listCampaignEvaluationInfo) {
                    if (this.campaignEvaluationInfo != null) {
                        if (Objects.equals(item.getCampaignEvaluationInfoId(), this.campaignEvaluationInfo.getCampaignEvaluationInfoId())) {
                            cloneCampaignEvaluationInfo(item, this.campaignEvaluationInfo);
                        }
                    }
                }

                //Save Or Update SegmentEvaluationInfo
                for (SegmentEvaluationInfo segmentEvaluationInfo : listSegmentEvaluationInfo) {

                    listRuleEvaluationInfoOfSegmentAfter = new ArrayList<>();
                    listResultClassOfSegmentAfter = new ArrayList<>();
                    listFuzzyRuleOfSegmentAfter = new ArrayList<>();

                    campaignEvaluationInforService.doCreateOrUpdateSegmentEvaluationInfo(segmentEvaluationInfo);
                    listRuleEvaluationInfoOfSegmentAfter.addAll(segmentEvaluationInfo.getListRuleEvaluationInfo());
                    listResultClassOfSegmentAfter.addAll(segmentEvaluationInfo.getListResultClass());
                    listFuzzyRuleOfSegmentAfter.addAll(segmentEvaluationInfo.getListFuzzyRule());
                    if (segmentEvaluationInfo.getListRuleEvaluationInfoBefor() != null) {
                        for (RuleEvaluationInfo item : segmentEvaluationInfo.getListRuleEvaluationInfoBefor()) {
                            this.count = 0;
                            for (RuleEvaluationInfo items : listRuleEvaluationInfoOfSegmentAfter) {
                                if (Objects.equals(item.getRuleEvaluationInfoId(), items.getRuleEvaluationInfoId())) {
                                    count++;
                                }
                            }
                            if (count == 0) {
                                this.listRuleEvaluationInfoToDelete.add(item);
                            }
                        }
                    }
                    if (segmentEvaluationInfo.getListResultClassBefor() != null) {
                        for (ResultClass item : segmentEvaluationInfo.getListResultClassBefor()) {
                            this.count = 0;
                            for (ResultClass items : listResultClassOfSegmentAfter) {
                                if (Objects.equals(item.getClassId(), items.getClassId())) {
                                    count++;
                                }
                            }
                            if (count == 0) {
                                this.listResultClassToDelete.add(item);
                            }
                        }
                    }
                    if (segmentEvaluationInfo.getListFuzzyRuleBefor() != null) {
                        for (FuzzyRule item : segmentEvaluationInfo.getListFuzzyRuleBefor()) {
                            this.count = 0;
                            for (FuzzyRule items : listFuzzyRuleOfSegmentAfter) {
                                if (Objects.equals(item.getFuzzyRuleId(), items.getFuzzyRuleId())) {
                                    count++;
                                }
                            }
                            if (count == 0) {
                                this.listFuzzyRuleToDelete.add(item);
                            }
                        }
                    }

                    //Save Or Update ResultClass Segment
                    for (ResultClass item : listResultClassOfSegmentAfter) {

                        campaignEvaluationInforService.doCreateOrUpdateResultClass(item);

                    }
                    List<RuleEvaluationInfo> listRuleId = new ArrayList<>();
                    for (RuleEvaluationInfo item : this.listRuleEvaluationInfoToDelete) {
                        for (RuleEvaluationInfo items : this.listRuleEvaluationInfoOfSegmentAfter) {
                            if (Objects.equals(item.getRuleEvaluationInfoId(), items.getRuleEvaluationInfoId())) {
                                listRuleId.add(item);
                            }
                        }
                    }
                    this.listRuleEvaluationInfoToDelete.removeAll(listRuleId);

                    List<ResultClass> listClassId = new ArrayList<>();
                    for (ResultClass item : this.listResultClassToDelete) {
                        for (ResultClass items : this.listResultClassOfSegmentAfter) {
                            if (Objects.equals(item.getClassId(), items.getClassId())) {
                                listClassId.add(item);
                            }
                        }
                    }
                    this.listResultClassToDelete.removeAll(listClassId);

                    List<FuzzyRule> listFuzzyRuleId = new ArrayList<>();
                    for (FuzzyRule item : this.listFuzzyRuleToDelete) {
                        for (FuzzyRule items : this.listFuzzyRuleOfSegmentAfter) {
                            if (Objects.equals(item.getFuzzyRuleId(), items.getFuzzyRuleId())) {
                                listFuzzyRuleId.add(item);
                            }
                        }
                    }
                    this.listFuzzyRuleToDelete.removeAll(listFuzzyRuleId);
                    //Save RuleEvaluationInfo Segment
                    for (RuleEvaluationInfo item : listRuleEvaluationInfoOfSegmentAfter) {

                        campaignEvaluationInforService.doCreateOrUpdateRuleEvaluationInfo(item);

                    }
                    //Save Or Update FuzzyRule Segment
                    for (FuzzyRule item : listFuzzyRuleOfSegmentAfter) {

                        campaignEvaluationInforService.doCreateOrUpdateFuzzyRule(item);

                    }
                }

//                <------ END SAVE SEGMENT ----->
                for (SegmentEvaluationInfo item : listSegmentEvaluationInfoToDelete) {

                    campaignEvaluationInforService.deleteSegmentEvaluationInfo(item.getSegmentEvaluationInfoId());

                }

                //Save Or Update ResultClass
                for (ResultClass item : listResultClass) {

                    campaignEvaluationInforService.doCreateOrUpdateResultClass(item);

                }

                //Delete ResultClass
                for (ResultClass item : listResultClassToDelete) {

                    campaignEvaluationInforService.deleteResultClass(item.getClassId());

                }

                //delete RuleEvaluationInfo
                for (RuleEvaluationInfo item : listRuleEvaluationInfoToDelete) {

                    campaignEvaluationInforService.deleteRuleEvaluationInfo(item.getRuleEvaluationInfoId());

                }

                //Save Or Update FuzzyRule
                for (FuzzyRule item : listFuzzyRule) {

                    campaignEvaluationInforService.doCreateOrUpdateFuzzyRule(item);

                }

                //Delete Fuzzy
                for (FuzzyRule item : listFuzzyRuleToDelete) {

                    campaignEvaluationInforService.deleteFuzzyRule(item.getFuzzyRuleId());

                }

            }
        }

    }

    public void doSaveAll() {
        if (this.editMode) {
            saveAll();
            if (this.currentRootNote != null) {
                this.selectedNode = null;
                this.selectedNode = this.currentRootNote;
            }
//            this.selectedNode = this.currentRootNote;
//            TreeNode parentNote = selectedNode.getParent();
//                parentNote.getChildren().remove(selectedNode);
//                if (parentNote.getChildren().size() == 0) {
//                    removeExpandedNode(parentNote);
//                }
//                rootNode.getChildren().get(0).setExpanded(true);
//                TreeNode treeNode = addNoteToCategoryTree(rootNode, this.campaignEvaluationInfo);
//                this.currentRootNote = treeNode;
//                if (treeNode != null) {
//                    treeNode.setSelected(true);
//                }
            initTreeNode();
            Category cat = categoryService.getCategoryById(this.campaignEvaluationInfo.getCategoryId());
            TreeNode note = findCategoryInTree(rootNode, cat);
            if (note != null) {
                note.setExpanded(true);
                expandCurrentNode(note);
                mapTreeStatus(rootNode);
            }
            this.action = false;
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
        if (!this.editMode) {
            saveAll();
            rootNode.getChildren().get(0).setExpanded(true);
            TreeNode treeNode = addNoteToCategoryTree(rootNode, this.campaignEvaluationInfo);
            if (treeNode != null) {
                treeNode.setSelected(true);
            }
            this.currentRootNote = treeNode;
            this.action = false;
            this.editMode = true;
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }

    }

    public void deleteCampaignEvaluationInfo() {
        campaignEvaluationInfo = (CampaignEvaluationInfo) selectedNode.getData();
        if (this.campaignEvaluationInfo.getIsDefault() == true) {
            listCampaignEvaluationInfoToDelete.add(campaignEvaluationInfo);
            listCampaignEvaluationInfo.remove(campaignEvaluationInfo);
            //delete RuleEvaluationInfo
            this.listRuleEvaluationInfo = campaignEvaluationInforService.getListRuleEvaluationInfoByOwnerId(campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.listRuleEvaluationInfoToDelete = this.listRuleEvaluationInfo;

            for (RuleEvaluationInfo item : listRuleEvaluationInfoToDelete) {

                campaignEvaluationInforService.deleteRuleEvaluationInfo(item.getRuleEvaluationInfoId());

            }
            //delete ResultClass

            this.listResultClass = campaignEvaluationInforService.getListResultClassByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.listResultClassToDelete = this.listResultClass;

            for (ResultClass item : listResultClassToDelete) {
                campaignEvaluationInforService.deleteResultClass(item.getClassId());

            }
            //delete Fuzzy

            this.listFuzzyRule = campaignEvaluationInforService.getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.listFuzzyRuleToDelete = this.listFuzzyRule;

            for (FuzzyRule item : listFuzzyRuleToDelete) {

                campaignEvaluationInforService.deleteFuzzyRule(item.getFuzzyRuleId());

            }
            //delete campaignSchedule
            this.campaignSchedule = campaignEvaluationInforService.getCampaignScheduleByScheduleId(campaignEvaluationInfo.getScheduleId());
            if (this.campaignSchedule != null) {
                listCampaignSchedulesToDelete.add(campaignSchedule);
            }
            for (CampaignSchedule item : listCampaignSchedulesToDelete) {
                campaignEvaluationInforService.deleteCampaignSchedule(item.getScheduleId());
            }

            //delete CampaignEvaluationInfo            
            for (CampaignEvaluationInfo item : listCampaignEvaluationInfoToDelete) {
                campaignEvaluationInforService.deleteCampaignEvaluationInfo(item.getCampaignEvaluationInfoId());
            }
            TreeNode parentNode = selectedNode.getParent();
            parentNode.getChildren().remove(selectedNode);
            if (parentNode.getChildren().isEmpty()) {
                removeExpandedNode(parentNode);
            }
            //initTreeNode();
            this.disPlay = false;
        }
        if (this.campaignEvaluationInfo.getIsDefault() == false) {
            listCampaignEvaluationInfoToDelete.add(campaignEvaluationInfo);
            listCampaignEvaluationInfo.remove(campaignEvaluationInfo);

            //delete campaignSchedule
            this.campaignSchedule = campaignEvaluationInforService.getCampaignScheduleByScheduleId(campaignEvaluationInfo.getScheduleId());
            if (this.campaignSchedule != null) {
                listCampaignSchedulesToDelete.add(campaignSchedule);
            }
            for (CampaignSchedule item : listCampaignSchedulesToDelete) {
                campaignEvaluationInforService.deleteCampaignSchedule(item.getScheduleId());
            }

            //delete ResultClass
            this.listResultClass = campaignEvaluationInforService.getListResultClassByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.listResultClassToDelete = this.listResultClass;

            //delete Fuzzy
            this.listFuzzyRule = campaignEvaluationInforService.getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(campaignEvaluationInfo.getCampaignId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
            this.listFuzzyRuleToDelete = this.listFuzzyRule;

            //delete segment_Evaluation_Info
            this.listSegmentEvaluationInfo = campaignEvaluationInforService.getListSegmentEvaluationInfo(campaignEvaluationInfo.getCampaignEvaluationInfoId());
            for (SegmentEvaluationInfo item : listSegmentEvaluationInfo) {
                if (item != null) {
                    listSegmentEvaluationInfoToDelete.add(item);
                    this.listFuzzyRuleOfSegment = campaignEvaluationInforService.getListFuzzyRuleOfSegment(item.getSegmentId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
                    this.listFuzzyRuleToDelete.addAll(this.listFuzzyRuleOfSegment);
                    this.listResultClassOfSement = campaignEvaluationInforService.getListResultClassOfSegment(item.getSegmentId(), campaignEvaluationInfo.getCampaignEvaluationInfoId());
                    this.listResultClassToDelete.addAll(this.listResultClassOfSement);
                    this.listRuleEvaluationInfoOfSegment = campaignEvaluationInforService.getListRuleEvaluationInfoOfSegment(item.getSegmentEvaluationInfoId());
                    this.listRuleEvaluationInfoToDelete.addAll(this.listRuleEvaluationInfoOfSegment);
                }
            }
            for (SegmentEvaluationInfo item : listSegmentEvaluationInfoToDelete) {

                campaignEvaluationInforService.deleteSegmentEvaluationInfo(item.getSegmentEvaluationInfoId());
            }

            for (FuzzyRule item : listFuzzyRuleToDelete) {
                campaignEvaluationInforService.deleteFuzzyRule(item.getFuzzyRuleId());
            }
            for (ResultClass item : listResultClassToDelete) {

                campaignEvaluationInforService.deleteResultClass(item.getClassId());

            }

            for (RuleEvaluationInfo item : listRuleEvaluationInfoToDelete) {

                campaignEvaluationInforService.deleteRuleEvaluationInfo(item.getRuleEvaluationInfoId());

            }

            //delete CampaignEvaluationInfo            
            for (CampaignEvaluationInfo item : listCampaignEvaluationInfoToDelete) {
                listCampaignEvaluationInfo.remove(campaignEvaluationInfo);
                campaignEvaluationInforService.deleteCampaignEvaluationInfo(item.getCampaignEvaluationInfoId());
            }
            TreeNode parentNode = selectedNode.getParent();
            parentNode.getChildren().remove(selectedNode);
            if (parentNode.getChildren().isEmpty()) {
                removeExpandedNode(parentNode);
            }

            //initTreeNode();
            this.disPlay = false;
        }
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public TreeNode addNoteToCategoryTree(TreeNode rootNode, BaseCategory baseCategory) {
        TreeNode note = findParentNoteInTree(rootNode, baseCategory);
        if (note != null) {
            TreeNode newNote = new DefaultTreeNode(baseCategory.getTreeType(), baseCategory, note);
            expandCurrentNode(newNote);
            this.selectedNode = newNote;
            this.currentRootNote = newNote;
            return newNote;
        }
        return null;
    }

    public TreeNode findParentNoteInTree(TreeNode root, BaseCategory baseCategory) {
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (baseCategory.getParentType().isInstance(note.getData())) {
                Category data = (Category) note.getData();
                if (data.getCategoryId() == baseCategory.getParentId()) {
                    return note;
                }
            }
            currentNote = findParentNoteInTree(note, baseCategory);
            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }
}
