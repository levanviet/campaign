/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpSession;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import vn.viettel.campaign.entities.Objects;

@ManagedBean
@SessionScoped
public class MenuController {
    private String username;

    private MenuModel model;

    @PostConstruct
    public void init() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = (HttpSession) attr.getRequest().getSession(true);
        if (session.getAttribute("lstMenu") != null) {
            List<Objects> lstObjs = (List<Objects>) session.getAttribute("lstMenu");

            model = new DefaultMenuModel();
            //dashboard
            DefaultMenuItem item1 = new DefaultMenuItem("Dashboard");
            item1.setIcon("fa fa-fw fa-home");
            item1.setUrl("/campaign/campaign/campaignOnline");
            //change color

            lstObjs.forEach((obj) -> {
                DefaultSubMenu submenu = new DefaultSubMenu(obj.getObjectName());
                if (obj.getPathImg() != null) {
                    submenu.setIcon(obj.getPathImg());
                }
                obj.getLstSub().forEach((sub) -> {
                    DefaultMenuItem item = new DefaultMenuItem(sub.getObjectName());
                    item.setUrl(sub.getObjectUrl());
                    if (sub.getPathImg() != null) {
                        item.setIcon(sub.getPathImg());
                        item.setTitle(sub.getObjectName());
                    }
                    if (sub.getPathImg() != null) {
                        submenu.setIcon(obj.getPathImg());
                    }
                    submenu.addElement(item);
                });
                model.addElement(submenu);
            });

        }
        this.username = session.getAttribute("username").toString();
    }

    public void resetMenu(List<Objects> lstObjs) {
        this.model = new DefaultMenuModel();
        if (lstObjs != null && lstObjs.size() > 0) {
            lstObjs.forEach((obj) -> {
                DefaultSubMenu submenu = new DefaultSubMenu(obj.getObjectName());
                if (obj.getPathImg() != null) {
                    submenu.setIcon(obj.getPathImg());
                }
                obj.getLstSub().forEach((sub) -> {
                    DefaultMenuItem item = new DefaultMenuItem(sub.getObjectName());
                    item.setUrl(sub.getObjectUrl());
                    if (sub.getPathImg() != null) {
                        item.setIcon(sub.getPathImg());
                        item.setTitle(sub.getObjectName());
                    }
                    if (sub.getPathImg() != null) {
                        submenu.setIcon(obj.getPathImg());
                    }
                    submenu.addElement(item);
                });
                this.model.addElement(submenu);
            });
        }
    }

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    

}
