/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.validate;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import vn.viettel.campaign.controller.BaseController;

/**
 *
 * @author ConKC
 */
@FacesValidator("uploadValidator")
public class ValidateFile extends BaseController implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        FacesMessage message = new FacesMessage(NOT_IN_RANGE_MESSAGE_ID);
        if (value == null) {
            throw new ValidatorException(message);
        }
    }
}
