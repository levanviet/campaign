package vn.viettel.campaign.common;

public class TimeDiffirence {

    private Long day;
    private Long hour;
    private Long min;
    private Long second;

    public TimeDiffirence() {
    }

    /**
     * @return the day
     */
    public Long getDay() {
        return day;
    }

    /**
     * @param day the day to set
     */
    public void setDay(Long day) {
        this.day = day;
    }

    /**
     * @return the hour
     */
    public Long getHour() {
        return hour;
    }

    /**
     * @param hour the hour to set
     */
    public void setHour(Long hour) {
        this.hour = hour;
    }

    /**
     * @return the min
     */
    public Long getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin(Long min) {
        this.min = min;
    }

    /**
     * @return the second
     */
    public Long getSecond() {
        return second;
    }

    /**
     * @param second the second to set
     */
    public void setSecond(Long second) {
        this.second = second;
    }

}
