package vn.viettel.campaign.common;

/**
 * @author truongbx
 * @date 8/24/2019
 */
public enum Responses {
	SUCCESS("SUCCESS"),ERROR("FAIL"),NOT_FOUND("NOT_FOUND");
	public String statusName;
	Responses(String statusName){
		this.statusName = statusName;
	}
	public String getName(){
		return this.statusName;
	}

}
