package vn.viettel.campaign.common;

import java.util.HashMap;
import java.util.Map;

/**
 * @author truongbx
 * @date 9/7/2019
 */
public enum NotifyType {

	SMS(1L, "SMS"),
	USSD(2L, "USSD"),
	ZALO(3L, "ZALO"),
	FACEBOOK(4L, "FACEBOOK"),
	VIBER(5L, "VIBER"),
	MOCHA(6L, "MOCHA");

	private Long number;
	private String value;

	NotifyType(Long number, String value) {
		this.number = number;
		this.value = value;
	}
	public static Map<Long,String> asMap(){
		Map<Long,String> mapValue = new HashMap<>();
		for (NotifyType item :NotifyType.values()){
			mapValue.put(item.getNumber(),item.getValue());
		}
		return mapValue;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}}
